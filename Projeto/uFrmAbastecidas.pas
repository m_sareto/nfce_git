unit uFrmAbastecidas;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGrids, DB, DBClient, ExtCtrls, DBCtrls, StdCtrls,
  Buttons, Menus;

type
  TfrmAbastecidas = class(TForm)
    DataSource1: TDataSource;
    grdAbastecidas: TDBGrid;
    cdsAbastecidas: TClientDataSet;
    cdsAbastecidasID: TIntegerField;
    cdsAbastecidasBOMBA: TIntegerField;
    cdsAbastecidasPRODUTO: TStringField;
    cdsAbastecidasPRECO: TBCDField;
    cdsAbastecidasLITRO: TBCDField;
    cdsAbastecidasDINHEIRO: TBCDField;
    cdsAbastecidasNIVEPRECO: TIntegerField;
    Panel1: TPanel;
    CheckBox1: TCheckBox;
    cdsAbastecidasData_Hora: TDateTimeField;
    Panel2: TPanel;
    BitBtn1: TBitBtn;
    cdsAbastecidasStatus: TStringField;
    cdsAbastecidasFrentista: TStringField;
    cdsAbastecidasTemp_ECF_Item: TBooleanField;
    PopupMenu1: TPopupMenu;
    Pago1: TMenuItem;
    Pagar1: TMenuItem;
    cdsAbastecidasPago: TBooleanField;
    ppmAbastecidas: TPopupMenu;
    UsoeConsumo1: TMenuItem;
    Aferio1: TMenuItem;
    procedure FormShow(Sender: TObject);
    procedure grdAbastecidasKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure grdAbastecidasDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure BitBtn1Click(Sender: TObject);
    procedure CheckBox1Click(Sender: TObject);
    procedure Pago1Click(Sender: TObject);
    procedure Pagar1Click(Sender: TObject);
    procedure grdAbastecidasCellClick(Column: TColumn);
    procedure grdAbastecidasMouseMove(Sender: TObject; Shift: TShiftState;
      X, Y: Integer);
    procedure UsoeConsumo1Click(Sender: TObject);
    procedure Aferio1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmAbastecidas: TfrmAbastecidas;

implementation

uses uDMCupomFiscal, uFuncoes, uFrmConfiguracao, uFrmPrincipal,
  uRotinasGlobais, uFrmPosto;

{$R *.dfm}

procedure TfrmAbastecidas.FormShow(Sender: TObject);
begin
  Top:=10;
  try
    {if(POSTO.Abastecidas_Pago = True)then
    begin
      grdAbastecidas.Columns.Items[8].Visible := True;
      grdAbastecidas.Options := grdAbastecidas.Options -[dgRowSelect];
    end
    else
    begin
      grdAbastecidas.Columns.Items[8].Visible := False;
      grdAbastecidas.Options := grdAbastecidas.Options +[dgRowSelect];
    end;}
    cdsAbastecidas.CreateDataSet;
    cdsAbastecidas.First;
    While not(cdsAbastecidas.Eof) do cdsAbastecidas.Delete;

    DMCupomFiscal.dbQuery1.Active:=False;
    DMCupomFiscal.dbQuery1.SQL.Clear;
    If POSTO.Abastecidas_Exibir Then
      DMCupomFiscal.dbQuery1.SQL.Add('Select A.*, F.Nome as F_Nome, t.abastecida'+
                                     ' From Est_Abastecimentos A'+
                                     ' Left Outer Join Funcionarios F on F.CODFRENTISTA=A.Operador'+
                                     ' Left Outer Join temp_nfce_item t on t.abastecida=a.id'+
                                     ' Where A.Status<>''E'' Order By A.ID')
    Else
      DMCupomFiscal.dbQuery1.SQL.Add('Select A.*, F.Nome as F_Nome, t.abastecida'+
                                     ' From Est_Abastecimentos A'+
                                     ' Left Outer Join Funcionarios F on F.CODFRENTISTA=A.Operador'+
                                     ' Left Outer Join temp_nfce_item t on t.abastecida=a.id'+
                                     ' Where A.Status=''X'' Order By A.ID');
    DMCupomFiscal.dbQuery1.Active:=True;
    DMCupomFiscal.dbQuery1.First;
    While not(DMCupomFiscal.dbQuery1.Eof) do
    begin
      cdsAbastecidas.Insert;
      cdsAbastecidasID.Value        :=DMCupomFiscal.dbQuery1.FieldByName('Id').AsInteger;
      cdsAbastecidasBOMBA.Value     :=DMCupomFiscal.dbQuery1.FieldByName('BOMBA').AsInteger;
      cdsAbastecidasPRODUTO.Value   :=DMCupomFiscal.dbQuery1.FieldByName('MERCADORIA').AsString;
      cdsAbastecidasPRECO.Value     :=DMCupomFiscal.dbQuery1.FieldByName('PRECO').AsCurrency;
      cdsAbastecidasLITRO.Value     :=DMCupomFiscal.dbQuery1.FieldByName('LITRO').AsCurrency;
      cdsAbastecidasDINHEIRO.Value  :=DMCupomFiscal.dbQuery1.FieldByName('DINHEIRO').AsCurrency;
      cdsAbastecidasNIVEPRECO.Value :=DMCupomFiscal.dbQuery1.FieldByName('NIVEPRECO').AsInteger;
      cdsAbastecidasData_Hora.Value :=DMCupomFiscal.dbQuery1.FieldByName('DATA_HORA').AsDateTime;
      cdsAbastecidasStatus.Value    :=DMCupomFiscal.dbQuery1.FieldByName('Status').asString;
      cdsAbastecidasFrentista.Value :=DMCupomFiscal.dbQuery1.FieldByName('F_Nome').asString;
      //cdsAbastecidasPago.Value      :=DMCupomFiscal.dbQuery1.FieldByName('Pago').asString;

      if(DMCupomFiscal.dbQuery1.FieldByName('Pago').asString = 'S')then
        cdsAbastecidasPago.Value:=True
      else
        cdsAbastecidasPago.Value:=False;

      {Quando abastecida for maior que 0 indica que esta abastecida esta registrada no temp_nfce_item}
      if (DMCupomFiscal.dbQuery1.FieldByName('abastecida').AsInteger > 0) then
        cdsAbastecidasTemp_ECF_Item.Value:=True
      else
        cdsAbastecidasTemp_ECF_Item.Value:=False;
      cdsAbastecidas.Post;
      DMCupomFiscal.dbQuery1.Next;
    end;
  except
    on E:Exception do
    begin
      logErros(Self, caminhoLog,'Erro tentar carregar abastecidas', 'Erro tentar carregar abastecidas','S',E);
    end;
  end;
end;

procedure TfrmAbastecidas.grdAbastecidasKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (key = vk_Return) and not (DataSource1.DataSet.IsEmpty) then
    if (cdsAbastecidasStatus.Value<>'X') then
      msgInformacao('J� foi emitido Cupom Fiscal para essa Abastecida','Aten��o')
    else
      if (cdsAbastecidasTemp_ECF_Item.Value) then
        msgInformacao('Abastecida j� esta registrada no Cupom Fiscal atual','Aten��o')
      else
        ModalResult := mrOK;
end;

procedure TfrmAbastecidas.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case Key of
    vk_Escape: ModalResult := mrCancel;
  end;

  if (Key=vk_escape) then Close;
end;

procedure TfrmAbastecidas.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  If CheckBox1.Checked then
  begin
    try
      DMCupomFiscal.IBSQL1.Close;
      DMCupomFiscal.IBSQL1.SQL.Clear;
      DMCupomFiscal.IBSQL1.SQL.Add('Update Est_Abastecimentos set Status=''E'' Where (Id=:pId)');
      DMCupomFiscal.IBSQL1.ParamByName('pId').AsInteger:=cdsAbastecidasID.AsInteger;
      DMCupomFiscal.IBSQL1.ExecQuery;
      DMCupomFiscal.IBSQL1.Close;
    except
      on E:Exception do
      begin
        DMCupomFiscal.Transaction.Rollback;
        logErros(Self, caminhoLog,'Erro ao Tentar Cancelar Abastecidas', 'Erro ao Tentar Cancelar Abastecidas','S',E);
        Abort;
      end;
    end;
  end;
end;

procedure TfrmAbastecidas.FormCreate(Sender: TObject);
begin
  {If (POSTO.Concentrador ='CBC') then
  begin
    Panel2.Visible:=True;
    Application.ProcessMessages;
    try
      Winexec('C:\CBC\CBC.exe Ler', SW_HIDE);
    except
      on E:Exception do
      begin
        logErros(Self, caminhoLog,'Erro ao Tentar Ler Abastecidas CBC.EXE', 'Erro ao Tentar Ler Abastecidas CBC.EXE','S',E);
        Abort;
      end;
    end;
    Panel2.Visible:=False;
    Application.BringToFront;
  end;

  If (POSTO.Concentrador = 'ControlTech') then
  begin
    if not(FindWindow(nil,'Control Tech') > 0) then
    begin
      ShowMessage('O Control Tech N�O est� aberto'+#13+'N�o pode Ser Efetuada Leitura do Concentrador vWTech!');
      Close;
    end;

    Panel2.Visible:=True;
    Application.ProcessMessages;
    Panel2.Visible:=False;
  end;  }
end;

procedure TfrmAbastecidas.grdAbastecidasDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
  const
  IS_CHECK : Array[Boolean] of Integer = (DFCS_BUTTONCHECK, DFCS_BUTTONCHECK or DFCS_CHECKED);
var
  Check : Integer;
  R     : TRect;
begin
if not (cdsAbastecidas.IsEmpty) then
  begin
    If cdsAbastecidasTemp_ECF_Item.Value then
    begin
      grdAbastecidas.Canvas.Brush.Color := HexToTColor('88B8FF');
      grdAbastecidas.Canvas.FillRect(Rect);
    end;

    If cdsAbastecidasStatus.Value='F' then
    begin
      grdAbastecidas.Canvas.Font.Color:= clRed;
      grdAbastecidas.Canvas.Font.Style:=[fsBold];
    end;
    grdAbastecidas.DefaultDrawColumnCell(Rect, DataCol, Column, State);

    if AnsiLowerCase(Column.FieldName) = 'pago' then
    begin
      grdAbastecidas.Canvas.FillRect(Rect);
      if cdsAbastecidasPAGO.Value then
        frmPosto.imgListAbastecidas.Draw(grdAbastecidas.Canvas,Rect.Left+05,Rect.Top+1,2)
      else
        frmPosto.imgListAbastecidas.Draw(grdAbastecidas.Canvas,Rect.Left+05,Rect.Top+1,3);
      {Canvas.FillRect(Rect);
      Check := 0;
      if cdsAbastecidasPago.Value then
        Check := DFCS_CHECKED
      else
        Check := 0;
      R := Rect;
      InflateRect(R,-1,-1); //aqui manipula o tamanho do checkBox
      DrawFrameControl(Canvas.Handle,rect,DFC_BUTTON,DFCS_BUTTONCHECK or Check)}
    end;
  end;
end;

procedure TfrmAbastecidas.Aferio1Click(Sender: TObject);
begin
  if msgPergunta('Confirma lan�amento da abastecida  '+cdsAbastecidasID.asString+' como aferi��o ?'+sLineBreak+
                 'Litros : '+cdsAbastecidasLITRO.AsString,'.::AFERI��O::.') then
  begin
    IF(FRENTE_CAIXA.Autorizacao_Abast)then
    begin
      if not getAutorizacaoIncondicional('Exclui Abastecida', 'Autoriza��o para exclus�o de uma abastecida',
      cdsAbastecidasDINHEIRO.AsCurrency) then
      begin
        Abort;
      end;
    end;
    try
      if not dmCupomFiscal.IBSQL1.Transaction.InTransaction then
        dmCupomFiscal.IBSQL1.Transaction.StartTransaction;
      dmCupomFiscal.IBSQL1.Close;
      dmCupomFiscal.IBSQL1.SQL.Clear;
      dmCupomFiscal.IBSQL1.SQL.Add('Update est_abastecimentos Set Status=''A'' where id = :pID');
      dmCupomFiscal.IBSQL1.ParamByName('pID').AsInteger := cdsAbastecidasID.AsInteger;
      dmCupomFiscal.IBSQL1.ExecQuery;
      dmCupomFiscal.IBSQL1.Close;
      dmCupomFiscal.IBSQL1.Transaction.Commit;
      cdsAbastecidas.Delete;
    except
      on E: Exception do
      begin
        if dmCupomFiscal.IBSQL1.Transaction.InTransaction then
          dmCupomFiscal.IBSQL1.Transaction.Rollback;
        logErros(Self, caminhoLog, 'Erro ao excluir abastecida. '+e.Message, 'Erro ao excluir abastecida', 'S', e);
      end;
    end;
  end
end;

procedure TfrmAbastecidas.BitBtn1Click(Sender: TObject);
begin
  Close;
end;

procedure TfrmAbastecidas.CheckBox1Click(Sender: TObject);
begin
  if CheckBox1.Checked then
    if not (getAutorizacao('EXCLUSAO_ABASTECIDA','EFETUAR A EXCLUSAO DE ABASTECIDA',0)) then
      CheckBox1.Checked:=False;
end;

procedure TfrmAbastecidas.Pago1Click(Sender: TObject);
begin
{  try
    dmCupomFiscal.IBSQL1.Close;
    dmCupomFiscal.IBSQL1.SQL.Clear;
    dmCupomFiscal.IBSQL1.SQL.Add('Update est_abastecimentos Set Pago=''S'' where id = :pID');
    dmCupomFiscal.IBSQL1.ParamByName('pID').AsInteger := cdsAbastecidasID.Value;
    dmCupomFiscal.IBSQL1.ExecQuery;
    dmCupomFiscal.IBSQL1.Close;
    dmCupomFiscal.Transaction.CommitRetaining;
    cdsAbastecidas.Edit;
    cdsAbastecidasPago.Value:=True;
    cdsAbastecidas.Post;
  except
    on E: Exception do
    begin
      logErros(Sender,caminhoLog,'Erro ao Alterar Status' +#13+
                                 'Abastecida: '+cdsAbastecidasID.AsString+#13+
                                 'Data/Hora: '+cdsAbastecidasData_Hora.AsString+#13+
                                 'Produto: '+cdsAbastecidasPRODUTO.AsString,'Erro ao Alterar Status - Abastecida '+cdsAbastecidasID.AsString,'S', E);
      dmCupomFiscal.Transaction.RollbackRetaining;
      Abort;
    end;
  end;}
end;

procedure TfrmAbastecidas.UsoeConsumo1Click(Sender: TObject);
var
  sPlaca: String;
begin
  if(msgPergunta('Lan�ar abastecida como Uso e Consumo?'+sLineBreak+
    'N�mero: '+cdsAbastecidasID.AsString+sLineBreak+
    'Produto: '+cdsAbastecidasPRODUTO.AsString+
    'Data: '+cdsAbastecidasData_Hora.AsString+sLineBreak+
    'Valor: '+ FormatFloat('R$ ###,##0.00',cdsAbastecidasDINHEIRO.AsCurrency),Application.Title))then
  begin
    if(getAutorizacao('ABASTECIDA_USO_CONSUMO','LAN�AMENTO DE ABASTECIDA COMO USO E CONSUMO',
    cdsAbastecidasDINHEIRO.AsCurrency))Then
    begin
      try
        sPlaca := '';
        InputQuery('Vincular Placa', 'Informe a Placa:', sPlaca);
        dmCupomFiscal.IBSQL1.Close;
        dmCupomFiscal.IBSQL1.SQL.Clear;
        dmCupomFiscal.IBSQL1.SQL.Add('Update Est_Abastecimentos set Status = ''U'' ');
        if(sPlaca <> '')Then
          dmCupomFiscal.IBSQL1.SQL.Add(',Placa=:pPlaca');
        dmCupomFiscal.IBSQL1.SQL.Add(' where ID = :pID');
        if(sPlaca <> '')Then
          dmCupomFiscal.IBSQL1.ParamByName('pPlaca').AsString := UpperCase(sPlaca);
        dmCupomFiscal.IBSQL1.ParamByName('pID').AsInteger := cdsAbastecidasID.AsInteger;
        dmCupomFiscal.IBSQL1.ExecQuery;
        dmCupomFiscal.IBSQL1.Close;
        dmCupomFiscal.Transaction.CommitRetaining;
      except
        on E:Exception do
          logErros(Sender, caminhoLog,'Erro ao lan�ar abastecida como uso e consumo',
            'Erro ao lan�ar abastecida como uso e consumo','S',E);
      end;
    end;
  end;

end;

procedure TfrmAbastecidas.Pagar1Click(Sender: TObject);
begin
{  try
    dmCupomFiscal.IBSQL1.Close;
    dmCupomFiscal.IBSQL1.SQL.Clear;
    dmCupomFiscal.IBSQL1.SQL.Add('Update est_abastecimentos Set Pago=''N'' where id = :pID');
    dmCupomFiscal.IBSQL1.ParamByName('pID').AsInteger := cdsAbastecidasID.Value;
    dmCupomFiscal.IBSQL1.ExecQuery;
    dmCupomFiscal.IBSQL1.Close;
    dmCupomFiscal.Transaction.CommitRetaining;
    cdsAbastecidas.Edit;
    cdsAbastecidasPago.Value:=False;
    cdsAbastecidas.Post;
  except
    on E: Exception do
    begin
      logErros(Sender,caminhoLog,'Erro ao Alterar Status' +#13+
                                 'Abastecida: '+ cdsAbastecidasID.AsString+#13+
                                 'Data/Hora: '+cdsAbastecidasData_Hora.AsString+#13+
                                 'Produto: '+cdsAbastecidasPRODUTO.AsString,'Erro ao Alterar Status - Abastecida '+cdsAbastecidasID.AsString,'S', E);
      dmCupomFiscal.Transaction.RollbackRetaining;
      Abort;
    end;
  end;}
end;

procedure TfrmAbastecidas.grdAbastecidasCellClick(Column: TColumn);
begin
  if AnsiLowerCase(Column.FieldName) = 'pago' then
  begin
    cdsAbastecidas.Edit;
    cdsAbastecidas.FieldByName('Pago').AsBoolean := not cdsAbastecidas.FieldByName('Pago').AsBoolean;
    cdsAbastecidas.Post;

    if not dmCupomFiscal.IBSQL1.Transaction.InTransaction then
      dmCupomFiscal.IBSQL1.Transaction.StartTransaction;
    dmCupomFiscal.IBSQL1.Close;
    dmCupomFiscal.IBSQL1.SQL.Clear;
    if(cdsAbastecidas.FieldByName('Pago').AsBoolean)then
      dmCupomFiscal.IBSQL1.SQL.Add('Update est_abastecimentos Set Pago=''S'' where id = :pID')
    else
      dmCupomFiscal.IBSQL1.SQL.Add('Update est_abastecimentos Set Pago=''N'' where id = :pID');

    dmCupomFiscal.IBSQL1.ParamByName('pID').AsInteger := cdsAbastecidasID.Value;
    dmCupomFiscal.IBSQL1.ExecQuery;
    dmCupomFiscal.IBSQL1.Close;
    dmCupomFiscal.IBSQL1.Transaction.Commit;
    getTemp_NFCE_Item;
    //dmCupomFiscal.Transaction.CommitRetaining;
  end;
end;


procedure TfrmAbastecidas.grdAbastecidasMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
{Const
Coluna=8;
var
  Cell: TGridCoord;}
begin
{   with TdbGrid(sender) do
   begin
     Cell := MouseCoord(X, Y);
     case cell.X of
     Coluna:
        begin
          Cursor:=crHandPoint;
          grdAbastecidasCellClick(grdAbastecidas.Columns[8]);
        end;
     else
     Cursor:=crDefault;
     end;
   end;}
end;

end.

