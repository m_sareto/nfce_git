object frmContaCorrente: TfrmContaCorrente
  Left = 450
  Top = 279
  Caption = 'Conta Corrente (Cr'#233'dito)'
  ClientHeight = 391
  ClientWidth = 619
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDesktopCenter
  OnClose = FormClose
  OnKeyDown = FormKeyDown
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 51
    Width = 619
    Height = 36
    Align = alTop
    Color = clGray
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlue
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
    object Label9: TLabel
      Left = 405
      Top = 8
      Width = 69
      Height = 20
      Caption = 'Total R$'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object DBEdit9: TDBEdit
      Left = 478
      Top = 4
      Width = 120
      Height = 28
      TabStop = False
      DataField = 'TOTAL'
      DataSource = dsCCorrente_Total
      ReadOnly = True
      TabOrder = 0
    end
    object btNovo: TBitBtn
      Left = 5
      Top = 5
      Width = 62
      Height = 25
      Caption = 'Novo'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Glyph.Data = {
        36060000424D3606000000000000360000002800000020000000100000000100
        18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF84B094257341196B
        3725734184B094FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFF9999994A4A4A4040404A4A4A999999FFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF88B297288C5364BA8D95D2
        B264BA8D288C5381AE91FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFF9C9C9C5959598F8F8FB3B3B38F8F8F595959969696C6A18CC38E68
        C08B66BE8864BB8561B9835FB47E5CB27C5AB17B58206C3A62BA8B60BA87FFFF
        FF60B98767BC8F20703DA0A0A08989898787878484848181817F7F7F7B7B7B79
        79797878784444448D8D8D8C8C8CFFFFFF8B8B8B919191464646C8926CFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF317B4C9CD4B6FFFFFFFFFF
        FFFFFFFF95D2B2196B378E8E8EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFF545454B8B8B8FFFFFFFFFFFFFFFFFFB3B3B3404040CA946EFFFFFF
        FFFFFFFFFFFEFFFFFDFEFEFDFEFEFCFEFEFCFEFEFC4A8B6290D3B192D6B1FFFF
        FF65BC8C67BC8F20703D909090FFFFFFFFFFFFFFFFFFFEFEFEFEFEFEFDFDFDFD
        FDFDFDFDFD696969B1B1B1B3B3B3FFFFFF909090919191464646CC976FFFFFFF
        FFFFFCFFFFFDFEFEFCFEFEFCFEFEFBFDFDFAFDFDFAA7C6B161AB8195D4B4BAE6
        D06ABB8F2D8F5781AE91929292FFFFFFFEFEFEFEFEFEFDFDFDFDFDFDFDFDFDFC
        FCFCFCFCFCB5B5B5858585B4B4B4D0D0D09292925D5D5D969696D19C73FFFFFF
        FEFEFCFEFEFCFEFEFCFDFDFBFDFDFBFDFDFAFDFDF8FBFBF9ACC8B56099754F8E
        664A8A61717951FFFFFF969696FFFFFFFDFDFDFDFDFDFDFDFDFCFCFCFCFCFCFC
        FCFCFBFBFBFAFAFAB9B9B97B7B7B6D6D6D686868676767FFFFFFD49E75FFFFFF
        FEFEFCFDFDFBFDFDFCFDFDFBFDFDF9FCFCF8FBF9F7FBF9F5FBF8F4FBF7F2FBF5
        F2FFFFFFB27C5AFFFFFF989898FFFFFFFDFDFDFCFCFCFDFDFDFCFCFCFBFBFBFA
        FAFAF9F9F9F8F8F8F7F7F7F6F6F6F5F5F5FFFFFF797979FFFFFFD5A076FFFFFF
        FDFDFCFDFDFBFDFDFAFCFCF9FCFBF7FBF9F5FBF8F4FBF7F3FBF5F2FAF3EFF8F2
        ECFFFFFFB57E5CFFFFFF9A9A9AFFFFFFFDFDFDFCFCFCFCFCFCFBFBFBFAFAFAF8
        F8F8F7F7F7F6F6F6F5F5F5F3F3F3F1F1F1FFFFFF7B7B7BFFFFFFD8A279FFFFFF
        FDFDFAFCFCFAFCFBF9FBFAF6FBF8F5FBF7F4FBF6F1F8F4EEF7F2EBF7F0EAF6EC
        E8FFFFFFB7815EFFFFFF9C9C9CFFFFFFFCFCFCFBFBFBFAFAFAF9F9F9F7F7F7F7
        F7F7F5F5F5F2F2F2F0F0F0EFEFEFECECECFFFFFF7E7E7EFFFFFFD9A379FFFFFF
        FCFBF9FCFBF8FBF9F7FBF7F4FAF7F2F9F5F0F7F3EDF6EFEAF5EBE7F3EAE4F2E7
        DEFFFFFFBA8560FFFFFF9D9D9DFFFFFFFAFAFAFAFAFAF9F9F9F7F7F7F6F6F6F4
        F4F4F1F1F1EEEEEEEBEBEBE9E9E9E6E6E6FFFFFF818181FFFFFFDBA47AFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFBD8763FFFFFF9E9E9EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF838383FFFFFFDCA77BDCA77B
        DCA77BDCA77BDCA77BDCA77BDCA77BDCA77BDCA77BDCA77BDCA77BDCA77BDCA7
        7BDCA77BC08B66FFFFFFA0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0
        A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0878787FFFFFFDDAD86E8B992
        E8B992E8B992E8B992E8B992E8B992E8B992E8B992E8B992E8B992E8B992E8B9
        92E8B992C19170FFFFFFA7A7A7B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3
        B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B38E8E8EFFFFFFDBC3B6DEB492
        DCA77BDCA67ADAA47AD8A279D5A076D49E75D29D73CF9A72CE9970CB966FC994
        6CC79E80DBC3B6FFFFFFC3C3C3AFAFAFA0A0A09F9F9F9E9E9E9C9C9C9A9A9A98
        98989797979595959393939191918F8F8F9B9B9BC3C3C3FFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
      NumGlyphs = 2
      ParentFont = False
      TabOrder = 1
      OnClick = btNovoClick
    end
    object btSalva: TBitBtn
      Left = 67
      Top = 5
      Width = 65
      Height = 25
      Caption = 'Salvar'
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Glyph.Data = {
        36060000424D3606000000000000360000002800000020000000100000000100
        18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAA8C6E
        8D6D568D6D568D6D567B61517B61516D574A6D574A5F4D445F4D4456463F5646
        3F4331297B6151FFFFFFFFFFFF8686866A6A6A6A6A6A6A6A6A60606060606056
        56565656564D4D4D4D4D4D464646464646313131606060FFFFFFFFFFFF8D6D56
        FCCD9FF4C395F4C395EBBB8BE4B281DAA774CEA06FCE9962C89156BC8244BC82
        44BC8244433129FFFFFFFFFFFF6A6A6AC4C4C4BABABABABABAB1B1B1A8A8A89D
        9D9D9696968E8E8E848484757575757575757575313131FFFFFFFFFFFFAA8C6E
        FCCD9FE4B281DFAD7BDAA774DAA774CEA06FCEA06FCE9962CE9962C89156C891
        56BC824456463FFFFFFFFFFFFF868686C4C4C4A8A8A8A3A3A39D9D9D9D9D9D96
        96969696968E8E8E8E8E8E848484848484757575464646FFFFFFFFFFFFAA8C6E
        FFD3AAE8B787E4B281DFAD7BDFAD7BDAA774DAA774CEA06FCE9962CE9962C891
        56BC824456463FFFFFFFFFFFFF868686CBCBCBAEAEAEA8A8A8A3A3A3A3A3A39D
        9D9D9D9D9D9696968E8E8E8E8E8E848484757575464646FFFFFFFFFFFFCEA06F
        FFD7B4EBBB8BE8B787E4B281DFAD7BDFAD7BDAA774DAA774CEA06FCE9962C891
        56C891565F4D44FFFFFFFFFFFF969696D1D1D1B1B1B1AEAEAEA8A8A8A3A3A3A3
        A3A39D9D9D9D9D9D9696968E8E8E8484848484844D4D4DFFFFFFFFFFFFC6AC86
        FFD7B4F4C395EBBB8BE8B787E4B281E4B281DFAD7BDAA774DAA774CEA06FCE99
        62CE99625F4D44FFFFFFFFFFFFA2A2A2D1D1D1BABABAB1B1B1AEAEAEA8A8A8A8
        A8A8A3A3A39D9D9D9D9D9D9696968E8E8E8E8E8E4D4D4DFFFFFFFFFFFFC6AC86
        FFE0C5F4C395F4C395EBBB8BE8B787E4B281E4B281DFAD7BDAA774CEA06FCE99
        62CEA06F6D574AFFFFFFFFFFFFA2A2A2DBDBDBBABABABABABAB1B1B1AEAEAEA8
        A8A8A8A8A8A3A3A39D9D9D9696968E8E8E969696565656FFFFFFFFFFFFC6AC86
        FFE0C5FCCD9FF4C395F4C395EBBB8BEBBB8BE4B281DFAD7BDAA774DAA774CEA0
        6FDAA7746D574AFFFFFFFFFFFFA2A2A2DBDBDBC4C4C4BABABABABABAB1B1B1B1
        B1B1A8A8A8A3A3A39D9D9D9D9D9D9696969D9D9D565656FFFFFFFFFFFFD4BD97
        FFE0C5FCCD9FFCCD9FF4C395F4C395EBBB8BE8B787E4B281DFAD7BDAA774CEA0
        6FE4B2817B6151FFFFFFFFFFFFB2B2B2DBDBDBC4C4C4C4C4C4BABABABABABAB1
        B1B1AEAEAEA8A8A8A3A3A39D9D9D969696A8A8A8606060FFFFFFFFFFFFD4BD97
        FFE0C5FFD3AAFFEEDFFFEBDBFFEBDBFFEBDBFFEBDBFFEBDBFFEBDBFFEBDBDAA7
        74EBBB8B7B6151FFFFFFFFFFFFB2B2B2DBDBDBCBCBCBEBEBEBE9E9E9E9E9E9E9
        E9E9E9E9E9E9E9E9E9E9E9E9E9E99D9D9DB1B1B1606060FFFFFFFFFFFFDFCBA8
        FBE7D2FFD7B4FFEEDFFFF4EAFFF4EAFFF4EAFFEEDFFFEEDFE8B787FFFCF9DAA7
        74F4C3958D6D56FFFFFFFFFFFFC1C1C1E3E3E3D1D1D1EBEBEBF2F2F2F2F2F2F2
        F2F2EBEBEBEBEBEBAEAEAEFBFBFB9D9D9DBABABA6A6A6AFFFFFFFFFFFFE3D2B0
        FBE7D2FFD7B4FFEEDFFFF4EAFFF4EAFFF4EAFFEEDFFFEEDFEBBB8BFFFCF9DFAD
        7BF4C3958D6D56FFFFFFFFFFFFC8C8C8E3E3E3D1D1D1EBEBEBF2F2F2F2F2F2F2
        F2F2EBEBEBEBEBEBB1B1B1FBFBFBA3A3A3BABABA6A6A6AFFFFFFFFFFFFE5D3AE
        FFEBDBFBE7D2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD7B4FFFFFFFCCD
        9FFCCD9F7B6151FFFFFFFFFFFFC8C8C8E9E9E9E3E3E3FFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFD1D1D1FFFFFFC4C4C4C4C4C4606060FFFFFFFFFFFFFBE7D2
        D4BD97D4BD97D4BD97D4BD97C6AC86C6AC86CEA06FAA8C6EAA8C6EAA8C6EAA8C
        6E8D6D56AA8C6EFFFFFFFFFFFFE3E3E3B2B2B2B2B2B2B2B2B2B2B2B2A2A2A2A2
        A2A29696968686868686868686868686866A6A6A868686FFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
      NumGlyphs = 2
      ParentFont = False
      TabOrder = 2
      OnClick = btSalvaClick
    end
    object btExcluir: TBitBtn
      Left = 132
      Top = 5
      Width = 63
      Height = 25
      Caption = 'Excluir'
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Glyph.Data = {
        36060000424D3606000000000000360000002800000020000000100000000100
        18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF95B0E3235CC20543
        BC1F59C186A6DDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFB9B9B96E6E6E5B5B5B6B6B6BB0B0B0FFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8CABE12866CA2177E60579
        EA0164DD074FBE86A6DDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFB4B4B47575758181817878786D6D6D5F5F5FB0B0B0C6A18CC38E68
        C08B66BE8864BB8561B9835FB47E5CB27C5AB17B58164BAE639DF4187FFF0076
        F80076EE0368E11E59C0A0A0A08989898787878484848181817F7F7F7B7B7B79
        79797878785D5D5DA9A9A98989897B7B7B7777777070706B6B6BC8926CFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0543BCAECDFEFFFFFFFFFF
        FFFFFFFF187FEF0543BC8E8E8EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFF5B5B5BD4D4D4FFFFFFFFFFFFFFFFFF8383835B5B5BCA946EFFFFFF
        FFFFFFFFFFFEFFFFFDFEFEFDFEFEFCFEFEFCFEFEFC245DC28DB5F64D92FF1177
        FF2186FF408AEB245CC2909090FFFFFFFFFFFFFFFFFFFEFEFEFEFEFEFDFDFDFD
        FDFDFDFDFD6F6F6FBFBFBFA2A2A28585858E8E8E9393936E6E6ECC976FFFFFFF
        FFFFFCFFFFFDFEFEFCFEFEFCFEFEFBFDFDFAFDFDFA95B0E13D76D28DB5F7B8D6
        FE72A8F52E6BCA94AFE2929292FFFFFFFEFEFEFEFEFEFDFDFDFDFDFDFDFDFDFC
        FCFCFCFCFCB9B9B9848484BFBFBFDADADAB1B1B1797979B8B8B8D19C73FFFFFF
        FEFEFCFEFEFCFEFEFCFDFDFBFDFDFBFDFDFAFDFDF8FBFBF993AEDF2A61C60543
        BC205AC15F6186FFFFFF969696FFFFFFFDFDFDFDFDFDFDFDFDFCFCFCFCFCFCFC
        FCFCFBFBFBFAFAFAB7B7B77373735B5B5B6C6C6C6F6F6FFFFFFFD49E75FFFFFF
        FEFEFCFDFDFBFDFDFCFDFDFBFDFDF9FCFCF8FBF9F7FBF9F5FBF8F4FBF7F2FBF5
        F2FFFFFFB27C5AFFFFFF989898FFFFFFFDFDFDFCFCFCFDFDFDFCFCFCFBFBFBFA
        FAFAF9F9F9F8F8F8F7F7F7F6F6F6F5F5F5FFFFFF797979FFFFFFD5A076FFFFFF
        FDFDFCFDFDFBFDFDFAFCFCF9FCFBF7FBF9F5FBF8F4FBF7F3FBF5F2FAF3EFF8F2
        ECFFFFFFB57E5CFFFFFF9A9A9AFFFFFFFDFDFDFCFCFCFCFCFCFBFBFBFAFAFAF8
        F8F8F7F7F7F6F6F6F5F5F5F3F3F3F1F1F1FFFFFF7B7B7BFFFFFFD8A279FFFFFF
        FDFDFAFCFCFAFCFBF9FBFAF6FBF8F5FBF7F4FBF6F1F8F4EEF7F2EBF7F0EAF6EC
        E8FFFFFFB7815EFFFFFF9C9C9CFFFFFFFCFCFCFBFBFBFAFAFAF9F9F9F7F7F7F7
        F7F7F5F5F5F2F2F2F0F0F0EFEFEFECECECFFFFFF7E7E7EFFFFFFD9A379FFFFFF
        FCFBF9FCFBF8FBF9F7FBF7F4FAF7F2F9F5F0F7F3EDF6EFEAF5EBE7F3EAE4F2E7
        DEFFFFFFBA8560FFFFFF9D9D9DFFFFFFFAFAFAFAFAFAF9F9F9F7F7F7F6F6F6F4
        F4F4F1F1F1EEEEEEEBEBEBE9E9E9E6E6E6FFFFFF818181FFFFFFDBA47AFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFBD8763FFFFFF9E9E9EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF838383FFFFFFDCA77BDCA77B
        DCA77BDCA77BDCA77BDCA77BDCA77BDCA77BDCA77BDCA77BDCA77BDCA77BDCA7
        7BDCA77BC08B66FFFFFFA0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0
        A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0878787FFFFFFDDAD86E8B992
        E8B992E8B992E8B992E8B992E8B992E8B992E8B992E8B992E8B992E8B992E8B9
        92E8B992C19170FFFFFFA7A7A7B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3
        B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B38E8E8EFFFFFFDBC3B6DEB492
        DCA77BDCA67ADAA47AD8A279D5A076D49E75D29D73CF9A72CE9970CB966FC994
        6CC79E80DBC3B6FFFFFFC3C3C3AFAFAFA0A0A09F9F9F9E9E9E9C9C9C9A9A9A98
        98989797979595959393939191918F8F8F9B9B9BC3C3C3FFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
      NumGlyphs = 2
      ParentFont = False
      TabOrder = 3
      OnClick = btExcluirClick
    end
    object btCancelar: TBitBtn
      Left = 195
      Top = 5
      Width = 69
      Height = 25
      Caption = 'Cancelar'
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Glyph.Data = {
        36060000424D3606000000000000360000002800000020000000100000000100
        18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFF1313F20000F10000F10000F10000EF0000EF0000ED1212EEFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6C6C6C60606060606060
        60606060606060605F5F5F6A6A6AFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFF1313F61A20F53C4CF93A49F83847F83545F83443F73242F7141BF11717
        EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6E6E6E7373738B8B8B89898988
        88888686868585858484846E6E6E6D6D6DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        1313F81D23F94453FA2429F91212F70F0FF60C0CF50909F5161BF53343F7141B
        F11717EFFFFFFFFFFFFFFFFFFFFFFFFF6F6F6F7676769090907A7A7A6E6E6E6B
        6B6B6969696767677070708585856E6E6E6D6D6DFFFFFFFFFFFFFFFFFF1313F9
        1F25FA4A58FB4247FBC9C9FD3B3BF91313F71010F63333F7C5C5FD3035F73444
        F7141BF21717EFFFFFFFFFFFFF7070707878789494948D8D8DDEDEDE8787876E
        6E6E6C6C6C818181DBDBDB8181818585856E6E6E6D6D6DFFFFFFFFFFFF0000FB
        4F5DFD3237FBCBCBFEF2F2FFEBEBFE3B3BF93939F8EAEAFEF1F1FEC5C5FD181D
        F63343F70000EFFFFFFFFFFFFF646464979797838383DFDFDFF7F7F7F3F3F387
        8787858585F2F2F2F6F6F6DBDBDB727272858585606060FFFFFFFFFFFF0000FD
        525FFD2828FC4747FCECECFFF2F2FFECECFFECECFEF1F1FFEAEAFE3434F70B0B
        F53545F80000EFFFFFFFFFFFFF6565659999997D7D7D8F8F8FF4F4F4F7F7F7F4
        F4F4F3F3F3F7F7F7F2F2F2828282696969868686606060FFFFFFFFFFFF0000FD
        5562FE2C2CFD2929FC4848FCEDEDFFF2F2FFF2F2FFECECFE3A3AF91212F70F0F
        F63848F80000F1FFFFFFFFFFFF6565659B9B9B8080807D7D7D909090F4F4F4F7
        F7F7F7F7F7F3F3F38686866E6E6E6B6B6B888888606060FFFFFFFFFFFF0000FD
        5764FE3030FD2D2DFD4B4BFCEDEDFFF2F2FFF2F2FFECECFF3D3DF91616F81313
        F73C4BF80000F1FFFFFFFFFFFF6565659C9C9C828282808080929292F4F4F4F7
        F7F7F7F7F7F4F4F48888887070706E6E6E8A8A8A606060FFFFFFFFFFFF0000FF
        5A67FE3333FE5050FDEDEDFFF3F3FFEDEDFFEDEDFFF2F2FFECECFE3E3EFA1717
        F83F4EF90000F1FFFFFFFFFFFF6666669E9E9E848484959595F4F4F4F8F8F8F4
        F4F4F4F4F4F7F7F7F3F3F38989897171718C8C8C606060FFFFFFFFFFFF0000FF
        5B68FF4347FECFCFFFF3F3FFEDEDFF4C4CFC4A4AFCECECFFF2F2FFCACAFE2A2F
        FA4251FA0000F3FFFFFFFFFFFF6666669F9F9F8F8F8FE2E2E2F8F8F8F4F4F492
        9292919191F4F4F4F7F7F7DFDFDF7E7E7E8F8F8F616161FFFFFFFFFFFF1414FF
        262BFF5D6AFF585BFFCFCFFF5252FE2F2FFD2C2CFD4B4BFCCCCCFE484CFB4957
        FB1D23F91414F6FFFFFFFFFFFF7272727E7E7EA0A0A09B9B9BE2E2E297979781
        8181808080929292E0E0E09090909393937676766E6E6EFFFFFFFFFFFFFFFFFF
        1414FF262BFF5D6AFF4347FF3434FE3232FE3030FD2D2DFD383CFC4F5DFC1F25
        FA1414F8FFFFFFFFFFFFFFFFFFFFFFFF7272727E7E7EA0A0A08F8F8F85858584
        84848282828080808787879797977878786F6F6FFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFF1414FF262BFF5C69FF5B68FF5A67FE5865FE5663FE5461FE2227FC0D0D
        FBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7272727E7E7EA0A0A09F9F9F9E
        9E9E9D9D9D9C9C9C9B9B9B7A7A7A6C6C6CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFF1313FF0000FF0000FF0000FF0000FD0000FD0000FD1313FDFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF71717166666666666666
        6666656565656565656565707070FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
      NumGlyphs = 2
      ParentFont = False
      TabOrder = 4
      OnClick = btCancelarClick
    end
    object btFechar: TBitBtn
      Left = 264
      Top = 5
      Width = 69
      Height = 25
      Caption = 'Fechar'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Glyph.Data = {
        36060000424D3606000000000000360000002800000020000000100000000100
        18000000000000060000120B0000120B00000000000000000000F9F9F9F1F1F1
        F2F2F2F4F4F3FCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFEFE
        FEEEEEEECECFD0E2E2E3F9F9F9F1F1F1F2F2F2F4F4F4FCFCFCFCFCFCFCFCFCFC
        FCFCFCFCFCFCFCFCFCFCFCFCFCFCFEFEFEEEEEEECFCFCFE2E2E2C9C9C9989898
        9B9B9B9595957E8081807F7F7F80807F80807F80807F8080807F7F7F83847F8A
        9370889A5D8CB298A6B1C9C9C99898989B9B9B9595958080807F7F7F80808080
        80808080808080807F7F7F828282898989868686888888A5A5A5E9E9E9DFDFDF
        E2E1E1C5C6C854657876706B706F6F716F6F716F6F716F70746C66637C91528D
        BC5087B35F9BC995A5B2E9E9E9DFDFDFE1E1E1C6C6C66666666F6F6F6F6F6F6F
        6F6F6F6F6F7070706B6B6B7A7A7A888888838383959595A4A4A4FFFFFFFFFFFF
        FFFFFFF4F4F5596B7F6D66606765646765646765646765656A625B5A768D4C87
        B74C80AA6197C497A5B1FFFFFFFFFFFFFFFFFFF4F4F46C6C6C65656565656565
        65656565656565656161617474748383837C7C7C939393A4A4A4FFFFFFFFFFFF
        E9E9E9CECFD05B697D66625C636160636261636261636162665D565A758B4F8A
        B94D83AF669DCA97A5B1FFFFFFFFFFFFE9E9E9CFCFCF6B6B6B60606061616162
        62626262626262625C5C5C7373738585857F7F7F999999A4A4A4FFFFFFFFFFFF
        7A9E8D3E95785C6178675C595F5F5D605E5D605E5D605E5E62595259748B518D
        BE5084B06B9EC996A5B0FFFFFFFFFFFF8C8C8C6C6C6C6868685D5D5D5E5E5E5E
        5E5E5E5E5E5E5E5E5858587272728989898181819B9B9BA3A3A3B0BBB7649680
        27926701DB9728896A6251525E5A5B5B5B5A5C5B5A5C5C5B5C544E58748A5590
        C24E87B269A1CC97A5B0B6B6B67E7E7E5F5F5F7676765C5C5C5555555B5B5B5B
        5B5B5B5B5B5C5C5C5353537272728C8C8C8181819C9C9CA4A4A47DA2911BE3A8
        00D19900D59E00D0932984625B51525A565758575658585758504A57738A5992
        C24299CC65B2E298A3AE90909087878772727275757571717159595954545457
        57575757575858584F4F4F7171718E8E8E8B8B8BA6A6A6A3A3A3819F903EE6BF
        0ED6AB0BC89D10DCAE1AA676514F4B595154555454555555554C4757738B5B96
        C8409BCE6BAEDA98A3AD9090909B9B9B7D7D7D7474748181816464644E4E4E54
        54545454545555554C4C4C7171719292928B8B8BA5A5A5A3A3A398B3A651C79C
        42B9974CDBBF20AB8142584B574C4F52505052505052515251484357738B5D98
        CA4C97C971B7E498A3ADA6A6A68F8F8F8383839C9C9C6B6B6B4D4D4D50505050
        50505050505252524848487171719494948D8D8DADADADA3A3A3EFF4F2DBEEE3
        5F9E7D2DAD8A3F67695542444F4E4E4E4D4D4E4D4D4E4E4F4C443F55738A5C9C
        D05A91C182B3DD96A3AEF2F2F2E4E4E47E7E7E7373735858584747474E4E4E4D
        4D4D4D4D4D4E4E4E4444447070709797978E8E8EB0B0B0A2A2A2FFFFFFFFFFFF
        C3D9CEA9C6BC595D784C46424B4A4B4B4A4A4B4A4A4B4B4C49413C55728C60A0
        D45994C684B7E197A3ADFFFFFFFFFFFFCECECEB8B8B86666664646464B4B4B4A
        4A4A4A4A4A4B4B4B4141417171719B9B9B909090B3B3B3A2A2A2FFFFFFFFFFFF
        FFFFFFE9EDEC4E5B6F473E374643414643404643404644424438305170885DA0
        D65A97C989BBE597A3ACFFFFFFFFFFFFFFFFFFEBEBEB5E5E5E3D3D3D43434342
        42424242424444443737376D6D6D9B9B9B939393B8B8B8A2A2A2FFFFFFFFFFFF
        FFFFFFE7E7E8445C6F45424144464944464844464845474A3D3A39667F957AB8
        EC5495CB89BBE697A3ACFFFFFFFFFFFFFFFFFFE7E7E75A5A5A42424246464646
        46464646464747473A3A3A7E7E7EB4B4B4919191B8B8B8A2A2A2FFFFFFFFFFFF
        FFFFFFF9F9F9B4C7D6A7C0D3A8BFD3A8BFD3A8BFD3A8BFD3A5BDD1B5C8D7AFBC
        C891C0E796C6EF95A0AAFFFFFFFFFFFFFFFFFFF9F9F9C5C5C5BEBEBEBEBEBEBE
        BEBEBEBEBEBEBEBEBBBBBBC6C6C6BCBCBCBDBDBDC3C3C3A0A0A0FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFC
        FCDEE2E4C1D9ED99A5AFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFCFCE1E1E1D7D7D7A4A4A4}
      NumGlyphs = 2
      ParentFont = False
      TabOrder = 5
      OnClick = btFecharClick
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 619
    Height = 51
    Align = alTop
    ParentBackground = False
    TabOrder = 1
    object Label8: TLabel
      Left = 430
      Top = 6
      Width = 41
      Height = 13
      Caption = 'Valor R$'
    end
    object Label1: TLabel
      Left = 15
      Top = 6
      Width = 21
      Height = 13
      Caption = 'Vale'
    end
    object Label2: TLabel
      Left = 68
      Top = 6
      Width = 32
      Height = 13
      Caption = 'Cliente'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsUnderline]
      ParentFont = False
      OnClick = Label2Click
    end
    object Label3: TLabel
      Left = 516
      Top = 6
      Width = 44
      Height = 13
      Caption = 'Saldo R$'
    end
    object DBEdit4: TDBEdit
      Left = 65
      Top = 20
      Width = 45
      Height = 19
      Ctl3D = False
      DataField = 'CLIFOR'
      DataSource = dsCCorrente
      ParentCtl3D = False
      TabOrder = 1
      OnExit = DBEdit4Exit
    end
    object DBEdit8: TDBEdit
      Left = 430
      Top = 20
      Width = 80
      Height = 19
      Ctl3D = False
      DataField = 'Valor'
      DataSource = dsCCorrente
      ParentCtl3D = False
      TabOrder = 3
      OnExit = DBEdit8Exit
    end
    object DBEdit1: TDBEdit
      Left = 14
      Top = 20
      Width = 45
      Height = 19
      Ctl3D = False
      DataField = 'ID_CREDITO'
      DataSource = dsCCorrente
      ParentCtl3D = False
      TabOrder = 0
      OnExit = DBEdit1Exit
    end
    object edNome: TEdit
      Left = 110
      Top = 20
      Width = 314
      Height = 19
      TabStop = False
      Ctl3D = False
      Enabled = False
      ParentCtl3D = False
      ReadOnly = True
      TabOrder = 2
    end
    object reSaldo: TRealEdit
      Left = 516
      Top = 19
      Width = 80
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Ctl3D = False
      ParentCtl3D = False
      ReadOnly = True
      TabOrder = 4
      FormatReal = fNumber
      FormatSize = '10.2'
    end
  end
  object dbgrd1: TDBGrid
    Left = 0
    Top = 87
    Width = 619
    Height = 304
    Align = alClient
    DataSource = dsCCorrente
    DrawingStyle = gdsClassic
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'ID_CREDITO'
        Title.Caption = 'Vale'
        Width = 80
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NOME'
        Title.Caption = 'Nome'
        Width = 381
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'VALOR'
        Title.Alignment = taRightJustify
        Title.Caption = 'Valor'
        Visible = True
      end>
  end
  object dsCCorrente_Total: TDataSource
    AutoEdit = False
    DataSet = qrCCorrente_Total
    Left = 376
    Top = 236
  end
  object dsCCorrente: TDataSource
    DataSet = dmCupomFiscal.dbTemp_NFCe_FormaPag_CCorrente
    OnStateChange = dsCCorrenteStateChange
    Left = 128
    Top = 192
  end
  object qrCCorrente_Total: TIBQuery
    Database = dmCupomFiscal.DataBase
    Transaction = dmCupomFiscal.Transaction
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'Select Sum(Valor) as Total From TEMP_NFCE_FORMAPAG_CCorrente'
      'Where Caixa=:pCaixa')
    Left = 377
    Top = 183
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'pCaixa'
        ParamType = ptUnknown
      end>
    object qrCCorrente_TotalTOTAL: TIBBCDField
      FieldName = 'TOTAL'
      ProviderFlags = []
      DisplayFormat = '###,##0.00'
      EditFormat = '###,##0.00'
      currency = True
      Precision = 18
      Size = 2
    end
  end
end
