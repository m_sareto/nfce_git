object Form1: TForm1
  Left = 192
  Top = 177
  Width = 696
  Height = 480
  Caption = 'Form1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label56: TLabel
    Left = 191
    Top = 84
    Width = 41
    Height = 13
    Caption = 'Hist'#243'rico'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlue
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsUnderline]
    ParentFont = False
  end
  object Label58: TLabel
    Left = 288
    Top = 85
    Width = 142
    Height = 13
    Caption = 'Descri'#231#227'o do Hist'#243'rico...'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label61: TLabel
    Left = 136
    Top = 110
    Width = 96
    Height = 13
    Caption = 'Valor de Entrada R$'
  end
  object Label62: TLabel
    Left = 122
    Top = 135
    Width = 110
    Height = 13
    Caption = 'Restante a parcelar R$'
  end
  object Label63: TLabel
    Left = 137
    Top = 159
    Width = 95
    Height = 13
    Caption = 'N'#250'mero de parcelas'
  end
  object Label64: TLabel
    Left = 132
    Top = 213
    Width = 100
    Height = 13
    Caption = 'Data primeira parcela'
  end
  object Label65: TLabel
    Left = 128
    Top = 185
    Width = 104
    Height = 13
    Caption = 'Valor de cada parcela'
  end
  object Label17: TLabel
    Left = 203
    Top = 349
    Width = 55
    Height = 13
    Caption = 'Funcion'#225'rio'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlue
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsUnderline]
    ParentFont = False
  end
  object Label18: TLabel
    Left = 312
    Top = 349
    Width = 133
    Height = 13
    Caption = 'Nome do Funcion'#225'rio...'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object DBGrid2: TDBGrid
    Left = 321
    Top = 105
    Width = 180
    Height = 181
    Ctl3D = False
    DataSource = DsPar
    Options = [dgEditing, dgTitles, dgIndicator, dgColLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
    ParentCtl3D = False
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnExit = DBGrid2Exit
    Columns = <
      item
        Expanded = False
        FieldName = 'Data'
        Title.Caption = 'Vencimento(s)'
        Width = 70
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Valor'
        Title.Alignment = taRightJustify
        Title.Caption = 'Valor R$'
        Width = 77
        Visible = True
      end>
  end
  object ed_Cr_Historico: TEdit
    Left = 240
    Top = 81
    Width = 45
    Height = 19
    Ctl3D = False
    MaxLength = 6
    ParentCtl3D = False
    TabOrder = 1
    OnExit = ed_Cr_HistoricoExit
  end
  object Re_Vl_Entrada: TRealEdit
    Left = 241
    Top = 105
    Width = 73
    Height = 21
    Alignment = taRightJustify
    Color = clWhite
    Ctl3D = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentCtl3D = False
    ParentFont = False
    TabOrder = 2
    FormatReal = fNumber
    FormatSize = '10.2'
    OnExit = Re_Vl_EntradaExit
  end
  object re_vl_Resta_Parcela: TRealEdit
    Left = 241
    Top = 130
    Width = 73
    Height = 21
    TabStop = False
    Alignment = taRightJustify
    Color = clNavy
    Ctl3D = False
    Enabled = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentCtl3D = False
    ParentFont = False
    TabOrder = 3
    FormatReal = fNumber
    FormatSize = '10.2'
  end
  object ed_n_Parcela: TEdit
    Left = 241
    Top = 156
    Width = 28
    Height = 19
    Ctl3D = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    MaxLength = 2
    ParentCtl3D = False
    ParentFont = False
    TabOrder = 4
    Text = '1'
    OnExit = ed_n_ParcelaExit
  end
  object me_Dt_Pri_Parcela: TMaskEdit
    Left = 241
    Top = 207
    Width = 64
    Height = 19
    Ctl3D = False
    EditMask = '!99/99/00;1;_'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    MaxLength = 8
    ParentCtl3D = False
    ParentFont = False
    TabOrder = 5
    Text = '  /  /  '
  end
  object CheckBox3: TCheckBox
    Left = 119
    Top = 236
    Width = 136
    Height = 17
    Alignment = taLeftJustify
    Caption = 'Fixar dia do vencimento'
    Checked = True
    State = cbChecked
    TabOrder = 6
  end
  object re_vl_Parcela: TRealEdit
    Left = 241
    Top = 180
    Width = 73
    Height = 21
    TabStop = False
    Alignment = taRightJustify
    Color = clNavy
    Ctl3D = False
    Enabled = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentCtl3D = False
    ParentFont = False
    TabOrder = 7
    FormatReal = fNumber
    FormatSize = '10.2'
  end
  object BitBtn13: TBitBtn
    Left = 121
    Top = 288
    Width = 195
    Height = 27
    Caption = '&Simular Parcelas'
    TabOrder = 8
    OnClick = BitBtn13Click
  end
  object Panel1: TPanel
    Left = 321
    Top = 285
    Width = 180
    Height = 31
    BorderStyle = bsSingle
    Ctl3D = False
    ParentCtl3D = False
    TabOrder = 9
    object Label66: TLabel
      Left = 12
      Top = 3
      Width = 60
      Height = 13
      Caption = 'SubTotal R$'
    end
    object re_vl_T_Parcelas: TRealEdit
      Left = 82
      Top = -1
      Width = 79
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Color = clNavy
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      FormatReal = fNumber
      FormatSize = '10.2'
    end
  end
  object Panel2: TPanel
    Left = 122
    Top = 56
    Width = 383
    Height = 20
    Caption = 'Venda A Prazo'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlue
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 10
  end
  object Edit7: TEdit
    Left = 264
    Top = 345
    Width = 45
    Height = 19
    Ctl3D = False
    MaxLength = 6
    ParentCtl3D = False
    TabOrder = 11
    Text = '1'
    OnExit = Edit7Exit
  end
  object CDSPar: TClientDataSet
    Active = True
    Aggregates = <>
    Params = <>
    AfterPost = CDSParAfterPost
    Left = 392
    Top = 166
    Data = {
      490000009619E0BD010000001800000002000000000003000000490004446174
      6108000800000000000556616C6F720800040000000100075355425459504502
      00490006004D6F6E6579000000}
    object CDSParData: TDateTimeField
      FieldName = 'Data'
      EditMask = '99/99/99'
    end
    object CDSParValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '###,##0.00'
    end
  end
  object DsPar: TDataSource
    DataSet = CDSPar
    Left = 432
    Top = 166
  end
end
