unit uFrmExportaNFCe;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Mask, DBCtrls, Buttons, ExtCtrls, DB, ComCtrls,
  IBCustomDataSet, IBQuery, Grids, DBGrids;

type
  TfrmExportaNFCe = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    btnEnviar: TBitBtn;
    DBGrid1: TDBGrid;
    dsEst_ECF: TDataSource;
    btnSair: TBitBtn;
    Panel4: TPanel;
    statusBar: TStatusBar;
    procedure btnEnviarClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btnSairClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure conectarBaseDadosRemota();
    procedure desconectarBaseDadosRemota();
    procedure setNFCeEnviado(id_ecf: Integer);
    procedure getNFCeTransmissao();
    procedure setTriggerAtivaInativa(trigger:String; Ativa:Boolean);
  public
    { Public declarations }
  end;

var
  frmExportaNFCe: TfrmExportaNFCe;

implementation

uses uFrmPrincipal, uDMCupomFiscal, uFuncoes, StrUtils, uRotinasGlobais;


{$R *.dfm}

{ TfrmTransferenciaPedidos }


procedure TfrmExportaNFCe.conectarBaseDadosRemota;
begin
  try
    If Trim(FRENTE_CAIXA.DatabaseRemoto) <> ''  then
    begin
      dmCupomFiscal.DatabaseRemota.Connected := False;
      dmCupomFiscal.TransactionRemota.Active := False;
      dmCupomFiscal.DatabaseRemota.DatabaseName:=Trim(FRENTE_CAIXA.DatabaseRemoto);
      dmCupomFiscal.DatabaseRemota.Connected := true;
      dmCupomFiscal.TransactionRemota.Active := true;
    end
    else
      raise Exception.Create('Caminho da base de dados do servidor remoto n�o informado');
  except
    on e:Exception do
      raise Exception.Create('Erro ao conectar-se com a base de dados do servidor. '+e.Message);
  end;
end;


procedure TfrmExportaNFCe.btnEnviarClick(Sender: TObject);
var ID_ECF_Remoto, ID_ECF_Local, cont, contTotal:Integer;
begin
  if msgPergunta('Confirmar a transmiss�o de NFC-e(s) para o Servidor ?','') then
  begin
    try
      btnEnviar.Enabled:=False;
      btnSair.Enabled:=False;
      frmExportaNFCe.Enabled:=False;
      frmPrincipal.abreFormAguarde('Aguarde...');
      conectarBaseDadosRemota();
      try
        cont:=0;
        contTotal:=dmCupomFiscal.qurECF.RecordCount;
        if not(dmCupomFiscal.qurECF.Eof) then
        begin
          dmCupomFiscal.qurECF.First;
          while not(dmCupomFiscal.qurECF.Eof) do
          begin
            Inc(cont);
            Application.ProcessMessages;
            statusBar.Panels[0].Text:='Transmitindo '+IntToStr(cont)+' de '+IntToStr(contTotal);
            statusBar.Refresh;
            try
              if not dmCupomFiscal.dbECF_Rem.Transaction.InTransaction then
                dmCupomFiscal.dbECF_Rem.Transaction.StartTransaction;
              dmCupomFiscal.dbECF_Rem.Open;
              dmCupomFiscal.dbECF_Rem.Insert;
              dmCupomFiscal.dbECF_RemCAIXA.Value:=dmCupomFiscal.qurECFCAIXA.Value;
              dmCupomFiscal.dbECF_RemCUPOM.Value:=dmCupomFiscal.qurECFCUPOM.Value;
              dmCupomFiscal.dbECF_RemDATA.Value:=dmCupomFiscal.qurECFDATA.Value;
              dmCupomFiscal.dbECF_RemVLR_DINHEIRO.Value:=dmCupomFiscal.qurECFVLR_DINHEIRO.Value;
              dmCupomFiscal.dbECF_RemVLR_CHEQUE.Value:=dmCupomFiscal.qurECFVLR_CHEQUE.Value;
              dmCupomFiscal.dbECF_RemVLR_CARTAO.Value:=dmCupomFiscal.qurECFVLR_CARTAO.Value;
              dmCupomFiscal.dbECF_RemVLR_TIKET.Value:=dmCupomFiscal.qurECFVLR_TIKET.Value;
              dmCupomFiscal.dbECF_RemVLR_CONVENIO.Value:=dmCupomFiscal.qurECFVLR_CONVENIO.Value;
              dmCupomFiscal.dbECF_RemVLR_SUBTOTAL.Value:=dmCupomFiscal.qurECFVLR_SUBTOTAL.Value;
              dmCupomFiscal.dbECF_RemVLR_DESCONTO.Value:=dmCupomFiscal.qurECFVLR_DESCONTO.Value;
              dmCupomFiscal.dbECF_RemVLR_TOTAL.Value:=dmCupomFiscal.qurECFVLR_TOTAL.Value;
              dmCupomFiscal.dbECF_RemVLR_TOTTRIB.Value:=dmCupomFiscal.qurECFVLR_TOTTRIB.Value;
              dmCupomFiscal.dbECF_RemVLR_TOTTRIB_EST.Value:=dmCupomFiscal.qurECFVLR_TOTTRIB_EST.Value;
              dmCupomFiscal.dbECF_RemVLR_TOTTRIB_MUN.Value:=dmCupomFiscal.qurECFVLR_TOTTRIB_MUN.Value;
              dmCupomFiscal.dbECF_RemVLR_RECEBIDO.Value:=dmCupomFiscal.qurECFVLR_RECEBIDO.Value;
              dmCupomFiscal.dbECF_RemVLR_TROCO.Value:=dmCupomFiscal.qurECFVLR_TROCO.Value;
              dmCupomFiscal.dbECF_RemCCUSTO.Value:=dmCupomFiscal.qurECFCCUSTO.Value;
              dmCupomFiscal.dbECF_RemTURNO.Value:=dmCupomFiscal.qurECFTURNO.Value;
              dmCupomFiscal.dbECF_RemFUNCIONARIO.Value:=dmCupomFiscal.qurECFFUNCIONARIO.Value;
              dmCupomFiscal.dbECF_RemESTOQUE.Value:=dmCupomFiscal.qurECFESTOQUE.Value;
              dmCupomFiscal.dbECF_RemOPERACAO.Value:=dmCupomFiscal.qurECFOPERACAO.Value;
              dmCupomFiscal.dbECF_RemFORMA_PAG.Value:=dmCupomFiscal.qurECFFORMA_PAG.Value;
              dmCupomFiscal.dbECF_RemCLIFOR.Value:=dmCupomFiscal.qurECFCLIFOR.Value;
              dmCupomFiscal.dbECF_RemCF_NOME.Value:=dmCupomFiscal.qurECFCF_NOME.Value;
              dmCupomFiscal.dbECF_RemCF_ENDE.Value:=dmCupomFiscal.qurECFCF_ENDE.Value;
              dmCupomFiscal.dbECF_RemCF_MUNI.Value:=dmCupomFiscal.qurECFCF_MUNI.Value;
              dmCupomFiscal.dbECF_RemCF_CNPJ_CPF.Value:=dmCupomFiscal.qurECFCF_CNPJ_CPF.Value;
              dmCupomFiscal.dbECF_RemCF_IE.Value:=dmCupomFiscal.qurECFCF_IE.Value;
              dmCupomFiscal.dbECF_RemCF_PLACA.Value:=dmCupomFiscal.qurECFCF_PLACA.Value;
              dmCupomFiscal.dbECF_RemCF_KM.Value:=dmCupomFiscal.qurECFCF_KM.Value;
              dmCupomFiscal.dbECF_RemEL_CHAVE.Value:= gerarELChave_BaseRemota;
              dmCupomFiscal.dbECF_RemSTATUS.Value:=dmCupomFiscal.qurECFSTATUS.Value;
              dmCupomFiscal.dbECF_RemFUN_USUARIO.Value:=dmCupomFiscal.qurECFFUN_USUARIO.Value;
              if dmCupomFiscal.qurECFCONT_DT_HR.Value > 0 then
                dmCupomFiscal.dbECF_RemCONT_DT_HR.Value:=dmCupomFiscal.qurECFCONT_DT_HR.Value;
              dmCupomFiscal.dbECF_RemCONT_JUST.Value:=dmCupomFiscal.qurECFCONT_JUST.Value;
              dmCupomFiscal.dbECF_RemCOD_SIT.Value:=dmCupomFiscal.qurECFCOD_SIT.Value;
              dmCupomFiscal.dbECF_RemINDPAG.Value:=dmCupomFiscal.qurECFINDPAG.Value;
              dmCupomFiscal.dbECF_RemMODELO_DOC.Value:=dmCupomFiscal.qurECFMODELO_DOC.Value;
              dmCupomFiscal.dbECF_RemSERIE.Value:=dmCupomFiscal.qurECFSERIE.Value;
              dmCupomFiscal.dbECF_RemNUMERO.Value:=dmCupomFiscal.qurECFNUMERO.Value;
              dmCupomFiscal.dbECF_RemHORA.Value:=dmCupomFiscal.qurECFHORA.Value;
              dmCupomFiscal.dbECF_RemCHAVE_NFCE.Value:=dmCupomFiscal.qurECFCHAVE_NFCE.Value;
              dmCupomFiscal.dbECF_RemTPIMP.Value:=dmCupomFiscal.qurECFTPIMP.Value;
              dmCupomFiscal.dbECF_RemTPEMIS.Value:=dmCupomFiscal.qurECFTPEMIS.Value;
              dmCupomFiscal.dbECF_RemTPAMB.Value:=dmCupomFiscal.qurECFTPAMB.Value;
              dmCupomFiscal.dbECF_RemINDPRES.Value:=dmCupomFiscal.qurECFINDPRES.Value;
              dmCupomFiscal.dbECF_RemVERPROC.Value:=dmCupomFiscal.qurECFVERPROC.Value;
              dmCupomFiscal.dbECF_RemCF_NUMERO_END.Value:=dmCupomFiscal.qurECFCF_NUMERO_END.Value;
              dmCupomFiscal.dbECF_RemCF_MUNICIPIO.Value:=dmCupomFiscal.qurECFCF_MUNICIPIO.Value;
              dmCupomFiscal.dbECF_RemCF_BAIRRO.Value:=dmCupomFiscal.qurECFCF_BAIRRO.Value;
              dmCupomFiscal.dbECF_RemCF_CEP.Value:=dmCupomFiscal.qurECFCF_CEP.Value;
              dmCupomFiscal.dbECF_RemINDIEDEST.Value:=dmCupomFiscal.qurECFINDIEDEST.Value;
              dmCupomFiscal.dbECF_RemMODFRETE.Value:=dmCupomFiscal.qurECFMODFRETE.Value;
              dmCupomFiscal.dbECF_RemVLR_BC_ICM.Value:=dmCupomFiscal.qurECFVLR_BC_ICM.Value;
              dmCupomFiscal.dbECF_RemVLR_ICM.Value:=dmCupomFiscal.qurECFVLR_ICM.Value;
              dmCupomFiscal.dbECF_RemVLR_FRETE.Value:=dmCupomFiscal.qurECFVLR_FRETE.Value;
              dmCupomFiscal.dbECF_RemVLR_SEG.Value:=dmCupomFiscal.qurECFVLR_SEG.Value;
              dmCupomFiscal.dbECF_RemVLR_BC_PIS.Value:=dmCupomFiscal.qurECFVLR_BC_PIS.Value;
              dmCupomFiscal.dbECF_RemVLR_PIS.Value:=dmCupomFiscal.qurECFVLR_PIS.Value;
              dmCupomFiscal.dbECF_RemVLR_BC_COFINS.Value:=dmCupomFiscal.qurECFVLR_BC_COFINS.Value;
              dmCupomFiscal.dbECF_RemVLR_COFINS.Value:=dmCupomFiscal.qurECFVLR_COFINS.Value;
              dmCupomFiscal.dbECF_RemVLR_OUTRO.Value:=dmCupomFiscal.qurECFVLR_OUTRO.Value;
              dmCupomFiscal.dbECF_RemINFADFISCO.Value:=dmCupomFiscal.qurECFINFADFISCO.Value;
              dmCupomFiscal.dbECF_RemINFCPL.Value:=dmCupomFiscal.qurECFINFCPL.Value;
              dmCupomFiscal.dbECF_RemENVIADO.Value:='S';
              dmCupomFiscal.dbECF_Rem.Post;
              ID_ECF_Local:=dmCupomFiscal.qurECFID.Value;
              ID_ECF_Remoto:=dmCupomFiscal.dbECF_RemID.Value;
            except
              on e:exception do
                raise Exception.Create('[EST_ECF] - Erro ao inserir registro na base de dados. '+e.Message);
            end;

            //------ Busca e Insere Faturas do ECF
            try
              dmCupomFiscal.qurECF_Fatura.Active:=False;
              dmCupomFiscal.qurECF_Fatura.SQL.Clear;
              dmCupomFiscal.qurECF_Fatura.SQL.Add('select * from EST_ECF_FATURA where ID_ECF=:pID_ECF');
              dmCupomFiscal.qurECF_Fatura.ParamByName('pID_ECF').AsInteger:=ID_ECF_Local;
              dmCupomFiscal.qurECF_Fatura.Active:=True;
              dmCupomFiscal.qurECF_Fatura.First;
              while not(dmCupomFiscal.qurECF_Fatura.Eof) do
              begin
                if not dmCupomFiscal.dbECF_Fatura_Rem.Transaction.InTransaction then
                  dmCupomFiscal.dbECF_Fatura_Rem.Transaction.StartTransaction;
                dmCupomFiscal.dbECF_Fatura_Rem.Open;
                dmCupomFiscal.dbECF_Fatura_Rem.Insert;
                dmCupomFiscal.dbECF_Fatura_RemID_ECF.Value:=ID_ECF_Remoto;
                dmCupomFiscal.dbECF_Fatura_RemDATA.Value:=dmCupomFiscal.qurECF_FaturaDATA.Value;
                dmCupomFiscal.dbECF_Fatura_RemVALOR.Value:=dmCupomFiscal.qurECF_FaturaVALOR.Value;
                dmCupomFiscal.dbECF_Fatura_RemPARCELA.Value:=dmCupomFiscal.qurECF_FaturaPARCELA.Value;
                dmCupomFiscal.dbECF_Fatura_RemNDUP.Value:=dmCupomFiscal.qurECF_FaturaNDUP.Value;
                dmCupomFiscal.dbECF_Fatura_Rem.Post;
                dmCupomFiscal.dbECF_Fatura_Rem.Close;
                dmCupomFiscal.qurECF_Fatura.Next;
              end;
            except
              on e:exception do
                raise Exception.Create('[EST_ECF_FATURA] - Erro ao inserir registro na base de dados. '+e.Message);
            end;

            //------ Busca e Insere FormaPag ECF
            try
              dmCupomFiscal.qurECF_FormaPag.Active:=False;
              dmCupomFiscal.qurECF_FormaPag.SQL.Clear;
              dmCupomFiscal.qurECF_FormaPag.SQL.Add('select * from EST_ECF_FORMAPAG where ID_ECF=:pID_ECF');
              dmCupomFiscal.qurECF_FormaPag.ParamByName('pID_ECF').AsInteger:=ID_ECF_Local;
              dmCupomFiscal.qurECF_FormaPag.Active:=True;
              dmCupomFiscal.qurECF_FormaPag.First;
              while not(dmCupomFiscal.qurECF_FormaPag.Eof) do
              begin
                if not dmCupomFiscal.dbECF_FormaPag_Rem.Transaction.InTransaction then
                  dmCupomFiscal.dbECF_FormaPag_Rem.Transaction.StartTransaction;
                dmCupomFiscal.dbECF_FormaPag_Rem.Open;
                dmCupomFiscal.dbECF_FormaPag_Rem.Insert;
                dmCupomFiscal.dbECF_FormaPag_RemID_ECF.Value:=ID_ECF_Remoto;
                dmCupomFiscal.dbECF_FormaPag_RemID.Value:=dmCupomFiscal.qurECF_FormaPagID.Value;
                dmCupomFiscal.dbECF_FormaPag_RemTPAG.Value:=dmCupomFiscal.qurECF_FormaPagTPAG.Value;
                dmCupomFiscal.dbECF_FormaPag_RemVPAG.Value:=dmCupomFiscal.qurECF_FormaPagVPAG.Value;
                dmCupomFiscal.dbECF_FormaPag_RemCARD_CNPJ.Value:=dmCupomFiscal.qurECF_FormaPagCARD_CNPJ.Value;
                dmCupomFiscal.dbECF_FormaPag_RemCARD_TBAND.Value:=dmCupomFiscal.qurECF_FormaPagCARD_TBAND.Value;
                dmCupomFiscal.dbECF_FormaPag_RemCARD_CAUT.Value:=dmCupomFiscal.qurECF_FormaPagCARD_CAUT.Value;
                dmCupomFiscal.dbECF_FormaPag_RemFORMA_PAG.Value:=dmCupomFiscal.qurECF_FormaPagFORMA_PAG.Value;
                dmCupomFiscal.dbECF_FormaPag_RemINDICE_ECF.Value:=dmCupomFiscal.qurECF_FormaPagINDICE_ECF.Value;
                dmCupomFiscal.dbECF_FormaPag_Rem.Post;
                dmCupomFiscal.dbECF_FormaPag_Rem.Close;
                dmCupomFiscal.qurECF_FormaPag.Next;
              end;
            except
              on e:exception do
                raise Exception.Create('[EST_ECF_FORMAPAG] - Erro ao inserir registro na base de dados. '+e.Message);
            end;

            //------ Busca e Insere Itens
            try
              dmCupomFiscal.qurECF_Item.Active:=False;
              dmCupomFiscal.qurECF_Item.SQL.Clear;
              dmCupomFiscal.qurECF_Item.SQL.Add('select * from EST_ECF_ITEM where ID_ECF=:pID_ECF');
              dmCupomFiscal.qurECF_Item.ParamByName('pID_ECF').AsInteger:=ID_ECF_Local;
              dmCupomFiscal.qurECF_Item.Active:=True;
              dmCupomFiscal.qurECF_Item.First;
              while not(dmCupomFiscal.qurECF_Item.Eof) do
              begin
                if not dmCupomFiscal.dbECF_Item_Rem.Transaction.InTransaction then
                  dmCupomFiscal.dbECF_Item_Rem.Transaction.StartTransaction;
                dmCupomFiscal.dbECF_Item_Rem.Open;
                dmCupomFiscal.dbECF_Item_Rem.Insert;
                dmCupomFiscal.dbECF_Item_RemID_ECF.Value:=ID_ECF_Remoto;
                dmCupomFiscal.dbECF_Item_RemITEM.Value:=dmCupomFiscal.qurECF_ItemITEM.Value;
                dmCupomFiscal.dbECF_Item_RemMERCADORIA.Value:=dmCupomFiscal.qurECF_ItemMERCADORIA.Value;
                dmCupomFiscal.dbECF_Item_RemQTD.Value:=dmCupomFiscal.qurECF_ItemQTD.Value;
                dmCupomFiscal.dbECF_Item_RemVLR.Value:=dmCupomFiscal.qurECF_ItemVLR.Value;
                dmCupomFiscal.dbECF_Item_RemTOTAL.Value:=dmCupomFiscal.qurECF_ItemTOTAL.Value;
                dmCupomFiscal.dbECF_Item_RemICM.Value:=dmCupomFiscal.qurECF_ItemICM.Value;
                dmCupomFiscal.dbECF_Item_RemVALOR_CUSTO.Value:=dmCupomFiscal.qurECF_ItemVALOR_CUSTO.Value;
                dmCupomFiscal.dbECF_Item_RemBICO.Value:=dmCupomFiscal.qurECF_ItemBICO.Value;
                dmCupomFiscal.dbECF_Item_RemCST_PIS.Value:=dmCupomFiscal.qurECF_ItemCST_PIS.Value;
                dmCupomFiscal.dbECF_Item_RemCST_COFINS.Value:=dmCupomFiscal.qurECF_ItemCST_COFINS.Value;
                dmCupomFiscal.dbECF_Item_RemABASTECIDA.Value:=dmCupomFiscal.qurECF_ItemABASTECIDA.Value;
                dmCupomFiscal.dbECF_Item_RemVLR_DESCONTO.Value:=dmCupomFiscal.qurECF_ItemVLR_DESCONTO.Value;
                dmCupomFiscal.dbECF_Item_RemQTD_CAN.Value:=dmCupomFiscal.qurECF_ItemQTD_CAN.Value;
                dmCupomFiscal.dbECF_Item_RemVLR_TOTTRIB.Value:=dmCupomFiscal.qurECF_ItemVLR_TOTTRIB.Value;
                dmCupomFiscal.dbECF_Item_RemVLR_TOTTRIB_EST.Value:=dmCupomFiscal.qurECF_ItemVLR_TOTTRIB_EST.Value;
                dmCupomFiscal.dbECF_Item_RemVLR_TOTTRIB_MUN.Value:=dmCupomFiscal.qurECF_ItemVLR_TOTTRIB_MUN.Value;
                dmCupomFiscal.dbECF_Item_RemVLR_DESCONTO_ITEM.Value:=dmCupomFiscal.qurECF_ItemVLR_DESCONTO_ITEM.Value;
                dmCupomFiscal.dbECF_Item_RemCFOP.Value:=dmCupomFiscal.qurECF_ItemCFOP.Value;
                dmCupomFiscal.dbECF_Item_RemCST_ICM.Value:=dmCupomFiscal.qurECF_ItemCST_ICM.Value;
                dmCupomFiscal.dbECF_Item_RemNCM.Value:=dmCupomFiscal.qurECF_ItemNCM.Value;
                dmCupomFiscal.dbECF_Item_RemUNIDADE.Value:=dmCupomFiscal.qurECF_ItemUNIDADE.Value;
                dmCupomFiscal.dbECF_Item_RemINDTOT.Value:=dmCupomFiscal.qurECF_ItemINDTOT.Value;
                dmCupomFiscal.dbECF_Item_RemALI_ICM.Value:=dmCupomFiscal.qurECF_ItemALI_ICM.Value;
                dmCupomFiscal.dbECF_Item_RemVLR_BC_ICM.Value:=dmCupomFiscal.qurECF_ItemVLR_BC_ICM.Value;
                dmCupomFiscal.dbECF_Item_RemVLR_ICM.Value:=dmCupomFiscal.qurECF_ItemVLR_ICM.Value;
                dmCupomFiscal.dbECF_Item_RemVLR_FRETE.Value:=dmCupomFiscal.qurECF_ItemVLR_FRETE.Value;
                dmCupomFiscal.dbECF_Item_RemVLR_SEG.Value:=dmCupomFiscal.qurECF_ItemVLR_SEG.Value;
                dmCupomFiscal.dbECF_Item_RemVLR_OUTRO.Value:=dmCupomFiscal.qurECF_ItemVLR_OUTRO.Value;
                dmCupomFiscal.dbECF_Item_RemALI_PIS.Value:=dmCupomFiscal.qurECF_ItemALI_PIS.Value;
                dmCupomFiscal.dbECF_Item_RemALI_COFINS.Value:=dmCupomFiscal.qurECF_ItemALI_COFINS.Value;
                dmCupomFiscal.dbECF_Item_RemVLR_BC_PIS.Value:=dmCupomFiscal.qurECF_ItemVLR_BC_PIS.Value;
                dmCupomFiscal.dbECF_Item_RemVLR_PIS.Value:=dmCupomFiscal.qurECF_ItemVLR_PIS.Value;
                dmCupomFiscal.dbECF_Item_RemVLR_BC_COFINS.Value:=dmCupomFiscal.qurECF_ItemVLR_BC_COFINS.Value;
                dmCupomFiscal.dbECF_Item_RemVLR_COFINS.Value:=dmCupomFiscal.qurECF_ItemVLR_COFINS.Value;
                dmCupomFiscal.dbECF_Item_Rem.Post;
                dmCupomFiscal.dbECF_Item_Rem.Close;
                dmCupomFiscal.qurECF_Item.Next;
              end;
            except
              on e:exception do
                raise Exception.Create('[EST_ECF_ITEM] - Erro ao inserir registro na base de dados. '+e.Message);
            end;

            //------ Busca e Insere EST_ECF_XML
            try
              dmCupomFiscal.qurECF_XML.Active:=False;
              dmCupomFiscal.qurECF_XML.SQL.Clear;
              dmCupomFiscal.qurECF_XML.SQL.Add('select * from EST_ECF_XML where ID_ECF=:pID_ECF');
              dmCupomFiscal.qurECF_XML.ParamByName('pID_ECF').AsInteger:=ID_ECF_Local;
              dmCupomFiscal.qurECF_XML.Active:=True;
              dmCupomFiscal.qurECF_XML.First;
              //--Teremos que ver para desativar trigger antes de inserir
              while not(dmCupomFiscal.qurECF_XML.Eof) do
              begin
                //if not dmCupomFiscal.ibSqlComando_Rem.Transaction.InTransaction then
                  //dmCupomFiscal.ibSqlComando_Rem.Transaction.StartTransaction;
                dmCupomFiscal.ibSqlComando_Rem.Close;
                dmCupomFiscal.ibSqlComando_Rem.SQL.Clear;
                dmCupomFiscal.ibSqlComando_Rem.SQL.Add('update est_ecf_xml set cstat=:pcstat, xmotivo=:pxmotivo,'+
                                        ' nprot=:pnProt, xml_retorno=:pxml_Retorno, nprot_can=:pnProt_can, xml_retorno_can=:pxml_Retorno_can,'+
                                        ' nprot_inu=:pnProt_inu, xml_retorno_inu=:pxml_retorno_inu, nprot_den=:pnProt_den, xml_retorno_den=:pxml_retorno_den'+
                                        ' where id_ecf=:pid_ecf');
                dmCupomFiscal.ibSqlComando_Rem.ParamByName('pcstat').Value      := dmCupomFiscal.qurECF_XMLCSTAT.Value;

                if dmCupomFiscal.qurECF_XMLXMOTIVO.Value='' then
                  dmCupomFiscal.ibSqlComando_Rem.ParamByName('pxmotivo').Value    := null
                else
                  dmCupomFiscal.ibSqlComando_Rem.ParamByName('pxmotivo').Value    := dmCupomFiscal.qurECF_XMLXMOTIVO.Value;

                if dmCupomFiscal.qurECF_XMLNPROT.Value='' then
                  dmCupomFiscal.ibSqlComando_Rem.ParamByName('pnprot').Value      := null
                else
                  dmCupomFiscal.ibSqlComando_Rem.ParamByName('pnprot').Value      := dmCupomFiscal.qurECF_XMLNPROT.Value;

                if dmCupomFiscal.qurECF_XMLXML_RETORNO.Value='' then
                  dmCupomFiscal.ibSqlComando_Rem.ParamByName('pxml_retorno').Value := null
                else
                  dmCupomFiscal.ibSqlComando_Rem.ParamByName('pxml_retorno').Value := dmCupomFiscal.qurECF_XMLXML_RETORNO.Value;

                if dmCupomFiscal.qurECF_XMLNPROT_CAN.Value='' then
                  dmCupomFiscal.ibSqlComando_Rem.ParamByName('pnprot_can').Value  := null
                else
                  dmCupomFiscal.ibSqlComando_Rem.ParamByName('pnprot_can').Value  := dmCupomFiscal.qurECF_XMLNPROT_CAN.Value;

                if dmCupomFiscal.qurECF_XMLXML_RETORNO_CAN.Value='' then
                  dmCupomFiscal.ibSqlComando_Rem.ParamByName('pxml_retorno_can').Value  := null
                else
                  dmCupomFiscal.ibSqlComando_Rem.ParamByName('pxml_retorno_can').Value  := dmCupomFiscal.qurECF_XMLXML_RETORNO_CAN.Value;

                if dmCupomFiscal.qurECF_XMLNPROT_INU.Value='' then
                  dmCupomFiscal.ibSqlComando_Rem.ParamByName('pnprot_inu').Value  := null
                else
                  dmCupomFiscal.ibSqlComando_Rem.ParamByName('pnprot_inu').Value  := dmCupomFiscal.qurECF_XMLNPROT_INU.Value;

                if dmCupomFiscal.qurECF_XMLXML_RETORNO_INU.Value='' then
                  dmCupomFiscal.ibSqlComando_Rem.ParamByName('pxml_retorno_inu').Value  := null
                else
                  dmCupomFiscal.ibSqlComando_Rem.ParamByName('pxml_retorno_inu').Value  := dmCupomFiscal.qurECF_XMLXML_RETORNO_INU.Value;

                if dmCupomFiscal.qurECF_XMLNPROT_DEN.Value='' then
                  dmCupomFiscal.ibSqlComando_Rem.ParamByName('pnprot_den').Value  := null
                else
                  dmCupomFiscal.ibSqlComando_Rem.ParamByName('pnprot_den').Value  := dmCupomFiscal.qurECF_XMLNPROT_DEN.Value;

                if dmCupomFiscal.qurECF_XMLXML_RETORNO_DEN.Value='' then
                  dmCupomFiscal.ibSqlComando_Rem.ParamByName('pxml_retorno_den').Value := null
                else
                  dmCupomFiscal.ibSqlComando_Rem.ParamByName('pxml_retorno_den').Value := dmCupomFiscal.qurECF_XMLXML_RETORNO_DEN.Value;

                dmCupomFiscal.ibSqlComando_Rem.ParamByName('pid_ecf').Value:=ID_ECF_Remoto;
                dmCupomFiscal.ibSqlComando_Rem.ExecQuery;
                dmCupomFiscal.qurECF_XML.Next;
              end;
            except
              on e:exception do
                raise Exception.Create('[EST_ECF_XML] - Erro ao inserir registro na base de dados. '+e.Message);
            end;

            if not dmCupomFiscal.TransactionRemota.InTransaction then
              dmCupomFiscal.TransactionRemota.StartTransaction;
            dmCupomFiscal.TransactionRemota.Commit; //Confirma transacao no final
            setNFCeEnviado(ID_ECF_Local); //Ira atualizar na base local para Enviado "S"

            dmCupomFiscal.qurECF.Next;
          end;
          msgAviso('Transmitido '+IntToStr(cont)+' NFC-e em lote com total de '+IntToStr(contTotal)+' NFC-e(s)','');
        end
        else
          msgInformacao('N�o ha NFC-e(s) pendentes para transmiss�o ao Servidor','Aten��o');
      except
        on e:Exception do
        begin
          dmCupomFiscal.TransactionRemota.Rollback;
          logErros(Sender, caminhoLog, 'Ocorreu seguinte erro ao transmitir lote de NFC-e(s) para servidor. Transmitido '+IntToStr(cont)+' em lote com total de '+IntToStr(contTotal)+' NFC-e(s)'+
          sLineBreak+e.Message, 'Erro ao transmitir lote de NFC-e(s) para servidor', 'S', E);
        end;
      end;
    finally
      desconectarBaseDadosRemota();
      statusBar.Panels[0].Text:='';
      frmPrincipal.fechaFormAguarde;
      frmExportaNFCe.Enabled:=True;
      btnEnviar.Enabled:=True;
      btnSair.Enabled:=True;
      getNFCeTransmissao();
    end;
  end;
end;

procedure TfrmExportaNFCe.setNFCeEnviado(id_ecf: Integer);
begin
  dmCupomFiscal.IBSQL1.Close;
  dmCupomFiscal.IBSQL1.SQL.Clear;
  dmCupomFiscal.IBSQL1.SQL.Add('Update est_ecf set enviado=''S'' where id=:pID_ECF');
  dmCupomFiscal.IBSQL1.ParamByName('pID_ECF').AsInteger:=id_ecf;
  dmCupomFiscal.IBSQL1.ExecQuery;
  dmCupomFiscal.Transaction.CommitRetaining;
end;

procedure TfrmExportaNFCe.FormKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if Key=vk_escape then btnSair.Click;
end;

procedure TfrmExportaNFCe.btnSairClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmExportaNFCe.FormShow(Sender: TObject);
begin
  getNFCeTransmissao;
end;

procedure TfrmExportaNFCe.desconectarBaseDadosRemota;
begin
  dmCupomFiscal.DatabaseRemota.Connected := False;
  dmCupomFiscal.TransactionRemota.Active := False;
end;

procedure TfrmExportaNFCe.getNFCeTransmissao;
begin
  //----- Busca os pendente envio
  dmCupomFiscal.qurECF.Active:=False;
  dmCupomFiscal.qurECF.SQL.Clear;
  dmCupomFiscal.qurECF.SQL.Add('select e.* from est_ecf e'+
        ' left outer join est_ecf_xml x on x.id_ecf=e.id'+
        ' where ((enviado=''N'') or (enviado is null))  and (modelo_doc=''65'')'+
        ' and ((x.cstat=100) or (x.cstat=150)'+ //Efetivado
        ' or (x.cstat=101) or (x.cstat=135) or (x.cstat=151) or (x.cstat=155)'+ //Cancelado
        ' or (x.cstat=102)'+ //Inutilizado
        ' or (x.cstat=110) or (x.cstat=301) or (x.cstat=302) or (x.cstat=303))'); //Denegado
  dmCupomFiscal.qurECF.Active:=True;
  dmCupomFiscal.qurECF.FetchAll;
  dmCupomFiscal.qurECF.First;
  Panel4.Caption:=IntToStr(dmCupomFiscal.qurECF.RecordCount)+' NFC-e(s) a ser transmitido(s)'
end;

procedure TfrmExportaNFCe.setTriggerAtivaInativa(trigger: String;
  Ativa: Boolean);
begin
  dmCupomFiscal.ibSqlComando_Rem.Close;
  dmCupomFiscal.ibSqlComando_Rem.SQL.Clear;
  if Ativa then
    dmCupomFiscal.ibSqlComando_Rem.SQL.Add('ALTER TRIGGER '+trigger+' ACTIVE')
  else
    dmCupomFiscal.ibSqlComando_Rem.SQL.Add('ALTER TRIGGER '+trigger+' INACTIVE');
  dmCupomFiscal.ibSqlComando_Rem.ExecQuery;
  dmCupomFiscal.ibSqlComando_Rem.Transaction.CommitRetaining;
end;

end.

