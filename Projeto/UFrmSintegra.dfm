object FrmSintegra: TFrmSintegra
  Left = 270
  Top = 161
  Width = 399
  Height = 472
  Caption = 'Sintegra - Daruma'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 391
    Height = 304
    Align = alTop
    TabOrder = 0
    object Label2: TLabel
      Left = 8
      Top = 8
      Width = 50
      Height = 13
      Caption = 'Munic'#237'pio:'
    end
    object Label5: TLabel
      Left = 8
      Top = 56
      Width = 99
      Height = 13
      Caption = 'Codigo do Convenio:'
    end
    object Label6: TLabel
      Left = 8
      Top = 80
      Width = 97
      Height = 13
      Caption = 'Codigo da Natureza:'
    end
    object Label7: TLabel
      Left = 8
      Top = 104
      Width = 102
      Height = 13
      Caption = 'Codigo da Finalidade:'
    end
    object Label8: TLabel
      Left = 8
      Top = 128
      Width = 57
      Height = 13
      Caption = 'Logradouro:'
    end
    object Label9: TLabel
      Left = 8
      Top = 152
      Width = 40
      Height = 13
      Caption = 'Numero:'
    end
    object Label10: TLabel
      Left = 8
      Top = 176
      Width = 67
      Height = 13
      Caption = 'Complemento:'
    end
    object Label11: TLabel
      Left = 8
      Top = 200
      Width = 30
      Height = 13
      Caption = 'Bairro:'
    end
    object Label12: TLabel
      Left = 8
      Top = 224
      Width = 24
      Height = 13
      Caption = 'CEP:'
    end
    object Label13: TLabel
      Left = 8
      Top = 248
      Width = 71
      Height = 13
      Caption = 'Nome Contato:'
    end
    object Label14: TLabel
      Left = 8
      Top = 272
      Width = 45
      Height = 13
      Caption = 'Telefone:'
    end
    object Municipio: TEdit
      Left = 64
      Top = 8
      Width = 305
      Height = 21
      TabOrder = 0
      Text = 'Curitiba'
    end
    object Fax: TEdit
      Left = 32
      Top = 32
      Width = 337
      Height = 21
      TabOrder = 1
      Text = '(xx41) 3361-6945'
    end
    object Logradouro: TEdit
      Left = 72
      Top = 128
      Width = 297
      Height = 21
      TabOrder = 2
      Text = 'Av. Comendador Franco'
    end
    object Numero: TEdit
      Left = 56
      Top = 152
      Width = 313
      Height = 21
      TabOrder = 3
      Text = '1341'
    end
    object Complemento: TEdit
      Left = 80
      Top = 176
      Width = 289
      Height = 21
      TabOrder = 4
      Text = 'Bloco B'
    end
    object Bairro: TEdit
      Left = 40
      Top = 200
      Width = 329
      Height = 21
      TabOrder = 5
      Text = 'Jardim Bot'#226'nico'
    end
    object CEP: TEdit
      Left = 40
      Top = 224
      Width = 329
      Height = 21
      TabOrder = 6
      Text = '80215-090'
    end
    object Nome_Contato: TEdit
      Left = 80
      Top = 248
      Width = 289
      Height = 21
      TabOrder = 7
      Text = 'Jo'#227'o'
    end
    object Telefone: TEdit
      Left = 56
      Top = 272
      Width = 313
      Height = 21
      TabOrder = 8
      Text = '(XX41) 3361-6005'
    end
    object CodConvenio: TEdit
      Left = 112
      Top = 56
      Width = 257
      Height = 21
      TabOrder = 9
      Text = '3'
    end
    object CodNatureza: TEdit
      Left = 112
      Top = 80
      Width = 257
      Height = 21
      TabOrder = 10
      Text = '3'
    end
    object CodFinalidade: TEdit
      Left = 112
      Top = 104
      Width = 257
      Height = 21
      TabOrder = 11
      Text = '1'
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 304
    Width = 391
    Height = 96
    Align = alTop
    TabOrder = 1
    object PageControl1: TPageControl
      Left = 1
      Top = 1
      Width = 389
      Height = 94
      ActivePage = TabSheet1
      Align = alClient
      TabOrder = 0
      object TabSheet1: TTabSheet
        Caption = '   Daruma   '
        object Label1: TLabel
          Left = 8
          Top = 8
          Width = 124
          Height = 13
          Caption = 'Data Inicio do Movimento:'
        end
        object Label3: TLabel
          Left = 8
          Top = 32
          Width = 127
          Height = 13
          Caption = 'Data do fim do Movimento:'
        end
        object Data_Inicio_Movimento: TEdit
          Left = 136
          Top = 8
          Width = 233
          Height = 21
          TabOrder = 0
          Text = '011206'
        end
        object Data_Fim_Movimento: TEdit
          Left = 144
          Top = 32
          Width = 225
          Height = 21
          TabOrder = 1
          Text = '040107'
        end
      end
      object TabSheet2: TTabSheet
        Caption = '   Bematech   '
        ImageIndex = 1
      end
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 400
    Width = 391
    Height = 38
    Align = alClient
    TabOrder = 2
    object Enviar: TButton
      Left = 104
      Top = 6
      Width = 89
      Height = 25
      Caption = 'Enviar'
      TabOrder = 0
      OnClick = EnviarClick
    end
    object Fechar: TButton
      Left = 200
      Top = 6
      Width = 89
      Height = 25
      Caption = 'Fechar'
      TabOrder = 1
    end
  end
end
