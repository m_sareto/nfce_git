object frmClifor: TfrmClifor
  Left = 0
  Top = 0
  Align = alClient
  BorderIcons = [biMinimize, biMaximize, biHelp]
  Caption = 'Identificar Consumidor'
  ClientHeight = 605
  ClientWidth = 834
  Color = clWindow
  Ctl3D = False
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDesktopCenter
  WindowState = wsMaximized
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnlCabecalho: TPanel
    Left = 0
    Top = 0
    Width = 834
    Height = 42
    Align = alTop
    BevelOuter = bvNone
    Caption = 'Identificar Consumidor'
    Color = clHotLight
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -35
    Font.Name = 'Arial'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
  end
  object pnlCentral: TPanel
    Left = 176
    Top = 48
    Width = 498
    Height = 489
    TabOrder = 2
    object GroupBox1: TGroupBox
      Left = 1
      Top = 1
      Width = 496
      Height = 487
      Align = alClient
      TabOrder = 0
      object lbRecado: TLabel
        Left = 11
        Top = 399
        Width = 9
        Height = 20
        Caption = '?'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label12: TLabel
        Left = 11
        Top = 81
        Width = 81
        Height = 20
        Caption = 'CPF/CNPJ*'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label16: TLabel
        Left = 257
        Top = 81
        Width = 106
        Height = 20
        Caption = 'Inscr. Estadual'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lblPlaca: TLabel
        Left = 385
        Top = 81
        Width = 39
        Height = 20
        Caption = 'Placa'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlue
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsUnderline]
        ParentFont = False
        OnClick = lblPlacaClick
      end
      object lblKM: TLabel
        Left = 385
        Top = 139
        Width = 73
        Height = 20
        Caption = 'KM - Atual'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label9: TLabel
        Left = 11
        Top = 12
        Width = 41
        Height = 20
        Caption = 'Cliente'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlue
        Font.Height = -16
        Font.Name = 'Arial Narrow'
        Font.Style = [fsUnderline]
        ParentFont = False
        OnClick = Label9Click
      end
      object lblMotorista: TLabel
        Left = 11
        Top = 139
        Width = 66
        Height = 20
        Caption = 'Motorista'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlue
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsUnderline]
        ParentFont = False
        OnClick = lblMotoristaClick
      end
      object GroupBox2: TGroupBox
        Left = 11
        Top = 197
        Width = 475
        Height = 196
        Caption = '| Endere'#231'o |'
        TabOrder = 6
        object Label10: TLabel
          Left = 9
          Top = 15
          Width = 28
          Height = 20
          Caption = 'UF*'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label4: TLabel
          Left = 64
          Top = 15
          Width = 72
          Height = 20
          Caption = 'Munic'#237'pio*'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label5: TLabel
          Left = 371
          Top = 15
          Width = 38
          Height = 20
          Caption = 'CEP*'
          FocusControl = edtCEP
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label13: TLabel
          Left = 371
          Top = 76
          Width = 62
          Height = 20
          Caption = 'N'#250'mero*'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label11: TLabel
          Left = 9
          Top = 76
          Width = 75
          Height = 20
          Caption = 'Endere'#231'o*'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label6: TLabel
          Left = 9
          Top = 138
          Width = 48
          Height = 20
          Caption = 'Bairro*'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object lcmbMunicipio: TDBLookupComboBox
          Left = 64
          Top = 36
          Width = 301
          Height = 30
          DataField = 'CF_MUNICIPIO'
          DataSource = dsTempECF
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -19
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          KeyField = 'ID'
          ListField = 'DESCRICAO'
          ListSource = dmCupomFiscal.dsMunicipio
          ParentFont = False
          TabOrder = 1
        end
        object edtCEP: TDBEdit
          Left = 371
          Top = 36
          Width = 95
          Height = 30
          Color = clHotLight
          DataField = 'CF_CEP'
          DataSource = dsTempECF
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -19
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 2
          OnEnter = edtClienteNomeEnter
          OnExit = edtCEPExit
        end
        object edtEndereco: TDBEdit
          Left = 9
          Top = 97
          Width = 356
          Height = 30
          CharCase = ecUpperCase
          Color = clHotLight
          Ctl3D = False
          DataField = 'CF_ENDE'
          DataSource = dsTempECF
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -19
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 3
          OnEnter = edtClienteNomeEnter
          OnExit = edtEnderecoExit
        end
        object edtNumeroEnd: TDBEdit
          Left = 371
          Top = 97
          Width = 95
          Height = 30
          CharCase = ecUpperCase
          Color = clHotLight
          DataField = 'CF_NUMERO_END'
          DataSource = dsTempECF
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -19
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 4
          OnEnter = edtClienteNomeEnter
          OnExit = edtNumeroEndExit
        end
        object edtBairro: TDBEdit
          Left = 9
          Top = 158
          Width = 457
          Height = 30
          CharCase = ecUpperCase
          Color = clHotLight
          DataField = 'CF_BAIRRO'
          DataSource = dsTempECF
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -19
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 5
          OnEnter = edtClienteNomeEnter
          OnExit = edtBairroExit
        end
        object cmbUF: TComboBox
          Left = 9
          Top = 35
          Width = 48
          Height = 32
          CharCase = ecUpperCase
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -19
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 0
          Text = 'RS'
          OnClick = cmbUFClick
          OnExit = cmbUFExit
          Items.Strings = (
            'AC'
            'AL'
            'AM'
            'AP'
            'BA'
            'CE'
            'DF'
            'ES'
            'GO'
            'MA'
            'MT'
            'MS'
            'MG'
            'PA'
            'PB'
            'PR'
            'PE'
            'PI'
            'RJ'
            'RN'
            'RO'
            'RS'
            'RR'
            'SC'
            'SE'
            'SP'
            'TO'
            'EX')
        end
      end
      object rgIndicadorIEDest: TDBRadioGroup
        Left = 11
        Top = 428
        Width = 475
        Height = 57
        Caption = '| ICMS - Indicador da IE destinat'#225'rio* |'
        Columns = 3
        DataField = 'INDIEDEST'
        DataSource = dsTempECF
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          '1 - Contribuinte '
          '2 - Contr. Isento'
          '9 - N'#227'o contr.')
        ParentFont = False
        TabOrder = 7
        Values.Strings = (
          '1'
          '2'
          '9')
      end
      object edtCPFCNPJ: TDBEdit
        Left = 11
        Top = 103
        Width = 202
        Height = 30
        Hint = 'Pesquisa Cliente pelo CNPJ ou CPF'
        Color = clHotLight
        DataField = 'CF_CNPJ_CPF'
        DataSource = dsTempECF
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnEnter = edtClienteNomeEnter
        OnExit = edtCPFCNPJExit
      end
      object edtIE: TDBEdit
        Left = 257
        Top = 103
        Width = 122
        Height = 30
        CharCase = ecUpperCase
        Color = clHotLight
        DataField = 'CF_IE'
        DataSource = dsTempECF
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
        OnEnter = edtClienteNomeEnter
        OnExit = edtIEExit
      end
      object edtPlaca: TDBEdit
        Left = 385
        Top = 103
        Width = 101
        Height = 30
        CharCase = ecUpperCase
        Color = clHotLight
        DataField = 'CF_PLACA'
        DataSource = dsTempECF
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 3
        OnEnter = edtClienteNomeEnter
        OnExit = edtPlacaExit
      end
      object edtKM: TDBEdit
        Left = 385
        Top = 161
        Width = 101
        Height = 30
        Color = clHotLight
        DataField = 'CF_KM'
        DataSource = dsTempECF
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 5
        OnEnter = edtClienteNomeEnter
        OnExit = edtKMExit
      end
      object edtClienteNome: TDBEdit
        Left = 10
        Top = 36
        Width = 427
        Height = 38
        CharCase = ecUpperCase
        Color = clHotLight
        Ctl3D = False
        DataField = 'CF_NOME'
        DataSource = dsTempECF
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 0
        OnEnter = edtClienteNomeEnter
        OnExit = edtClienteNomeExit
      end
      object btnLimpaDados: TPngBitBtn
        Left = 443
        Top = 36
        Width = 43
        Height = 38
        Hint = 'Limpar os dados do cliente'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 8
        TabStop = False
        OnClick = btnLimpaDadosClick
        PngImage.Data = {
          89504E470D0A1A0A0000000D49484452000000240000002408040000004B0950
          130000008D4944415478DA6364A012601C9906713014003102FC609800C4241B
          C4C1B081C11D4D6C2743002EA3D00D6A60A827D22F8D40B543CA20B20136831A
          88D087A1069B41FFC971C0A841A3068D1A346AD06036E81FC102F81F03333106
          9D63302460D0590613620CB261D8C1C08DC798CF0C1E0CC78831888141862198
          41108731EF19D6303C2522D0C805C3D820001D132825770609F0000000004945
          4E44AE426082}
      end
      object edtMotorista: TDBEdit
        Left = 11
        Top = 161
        Width = 368
        Height = 30
        Hint = 'Pesquisa Cliente pelo CNPJ ou CPF'
        CharCase = ecUpperCase
        Color = clHotLight
        DataField = 'CF_MOTORISTA'
        DataSource = dsTempECF
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        MaxLength = 30
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 4
        OnEnter = edtMotoristaEnter
        OnExit = edtMotoristaExit
      end
      object btnCadastrarCNPJ: TPngBitBtn
        Left = 215
        Top = 102
        Width = 32
        Height = 32
        Hint = 'Busca os dados do cliente apartir do CNPJ informado'
        Enabled = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 9
        TabStop = False
        OnClick = btnCadastrarCNPJClick
        PngImage.Data = {
          89504E470D0A1A0A0000000D4948445200000018000000180806000000E0773D
          F80000048F4944415478DA9D95DB4F1C5518C0BF3333CB0EEC0DCAB22B0A46AA
          363418A3018C1AA37D28FE0B3646A831D1D8F40134D1D4023E2888F4D10753B1
          16D3F84250138C0FB296A64B815E58D05ADADA426DB915B2BBEC8DD99DDD9DCB
          F13BC3A54377A9D493CC9CEF5CE6FBCDF96E87C07DADA6A6061A9B0E4243C3EB
          82D7EB79CE2208B51CCF3D894B5E4AA1087B4A08A4B05FD6357D4651D5402814
          FE6BD037A8FF70EA144C4D5DD9A28F98076F3536C1D1A3ADA576BBED135C6AD4
          34CD830A28F684A2F6F5C604828DF23C4F2C1601B0BF0B94F62693A963DDC7BA
          13DF9FFC2E3FE0DCD885124F997B424EA79F50149598159AB66D8CCDF3B4A0C0
          42C402EBB550385CFFCACB2FA6F2022E5E1ADF9F55751FFE31FC9FC6711CFC73
          6BBAFEEDA6C689BC80C9C9C9064DA7BE482C6E98E5614E808D2CCECFC1DCEC6C
          5D47C7E7DB036C369BCFE170D0644A26A1F00AAC44A23493C9E4F880F53CCF11
          6B410138EC361A8DAC909999190887C3755D5D5D0F040C96949480D3E9045DD7
          21180CB28F101481A42401C2709E82285AA1A8A808F067A0B0B010161616601D
          50FF20C0B308F803010401641D40F123128D464142403A9DA6384F4451A40820
          EB008A00760215F73DD5D1D1319B17E03B7DC6E2F194FDE92D73EF65908701DC
          5D5A229AA65F4CC9F24B6F1E7883E6059C1D1ED9A7AADA19666FB77B17B8D04C
          4A360B89441C62B1D806C0301D02D04436E01C31748606D9AC0A02CF43026ED7
          1D39FCD9E49DCB4A2E2010083424A4944F4AA6B6440859971998858B91669868
          94CB92E0DE2FD87073BF924D1EF9EA9D1BDDD7CF65B677326A60D103E1950828
          8AB26DDC532E038B7B3E402DD43449DA8FBF1BEAFC7B647B800FED4F999359C2
          CD2F2CD0603044128984611E041A316FB158A868E3C8F4E3CDC0CE65D2D5D6F3
          5EA8F3C6683A2FE05504F83700FFE564D1C693AB8FB6B013DC0350DA76E250B0
          F3663EC059FF30EFF178AEBA4B77EDD929E0B2F74343ABF904BD87973BA7C7E4
          5CC0F0C8581DC7F397B04443F9235E703AEC46F4B044330136A308013051FA11
          9A4837AB699F3E9FEE4C045518FF299653EC1AE2AB920F4375F38F048137A248
          5555C3E1E8179AE1C3E4A6AB87128E920C1731970F2645F11DC7E7E39F3F5DEA
          CF71B255147DAB528A62D92558B2B77E6CAA45B3E2AF7449F4E72B884C1E5352
          EA6B270EDED6B6AD4576BB1DCD1283F9C545A3976519D8295864B154E0048029
          E7B72091E5FBA357C2B478FEB72FE76F5D3F1DCF89A2FD08F87DA751A41625C8
          30FD1A3450379391532C87662E2C1F1FEC9ACB75F2E8E868B1DBED0E2060F74E
          8BDD1D7E1C02F22F8689AC72F114BF54FA424FFB8F7246D2EE012A2A2AA0B9B9
          192A2B2B7997CBE52D2F2F6FABAAAA3A80C954BC51AEF345112BD7787F834FFA
          460B6666E34FCBFBEADDCAEEF9949456FBFAFAC0EFF7AF016A6B6BA1A5A5E531
          AC29EFE3F019B4B107AFBF623C45392AB2A1420EEF01F66051CB1A3E100441C7
          CB1E9774495223528A8B6B255AC5355C9B463D27878686AEF4F6F6AE01AAABAB
          A1B5B555C0052F0EDDB8A918651BCA2E943994AD283B370A1E8E598FCE24F2DA
          145D453989720CE515949707060632FDFDFDF02FD3602931F27894AE00000000
          49454E44AE426082}
      end
    end
  end
  object pnlRodape: TPanel
    Left = 0
    Top = 581
    Width = 834
    Height = 24
    Align = alBottom
    Color = clHotLight
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentBackground = False
    ParentFont = False
    TabOrder = 3
    DesignSize = (
      834
      24)
    object Label7: TLabel
      Left = 5
      Top = 3
      Width = 49
      Height = 19
      Caption = 'Caixa:'
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -16
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      Transparent = True
    end
    object Label8: TLabel
      Left = 699
      Top = 3
      Width = 41
      Height = 19
      Anchors = [akRight, akBottom]
      Caption = 'Data:'
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -16
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      Transparent = True
      ExplicitLeft = 741
    end
    object lblNCaixa: TDBText
      Left = 63
      Top = 4
      Width = 84
      Height = 17
      DataField = 'CAIXA'
      DataSource = dsTempECF
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -16
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblData: TDBText
      Left = 747
      Top = 4
      Width = 84
      Height = 17
      Anchors = [akRight, akBottom]
      DataField = 'DATA'
      DataSource = dsTempECF
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -16
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
  end
  object pnlFinalizar: TPanel
    Left = 0
    Top = 540
    Width = 834
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    Color = clHotLight
    ParentBackground = False
    TabOrder = 1
    object btnSalvar: TBitBtn
      Left = 774
      Top = 6
      Width = 53
      Height = 26
      Caption = 'Salvar'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      NumGlyphs = 2
      ParentFont = False
      TabOrder = 0
      Visible = False
      OnClick = btnSalvarClick
    end
    object pnlFinalizarOrganiza: TPanel
      Left = 242
      Top = 3
      Width = 366
      Height = 36
      BevelOuter = bvNone
      ParentBackground = False
      ParentColor = True
      TabOrder = 1
      object btnFinalizar: TSpeedButton
        Left = -3
        Top = -1
        Width = 88
        Height = 37
        Hint = 'Pressione F1 ou clique aqui para Finalizar o Cupom Fiscal'
        Caption = 'Prosseguir'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Glyph.Data = {
          B6040000424DB604000000000000360000002800000018000000100000000100
          18000000000080040000120B0000120B00000000000000000000E1E1E1979696
          91908F9291908A898881807F918F8F9291909291909291909291909291909291
          9092919091908F8483828281808A898893919173717068666591908F979696E1
          E1E1A5A4A4E8E6E6E0DFDEDFDEDEDFDEDEE1E0E0DFDEDEDFDEDEDFDEDEDFDEDE
          DFDEDEDFDEDEDFDEDEDFDEDEDFDEDEDFDEDEDFDEDEDFDEDEDFDEDEDFDEDEDFDE
          DEE0DFDEE7E6E6A4A4A3A3A2A2F6F6F7D7D6D6C8C8C6C9C9C8CCCCCBC9C9C8C9
          C9C8C9C9C8C9C9C8C9C9C8C9C9C8C9C9C8C9C9C8C9C9C8C9C9C8C9C9C8C9C9C8
          C9C9C8C9C9C8C8C8C6D7D6D6F6F6F6A2A1A0A1A0A0EEEEEEF2F3F3FBFBFBFAFA
          FAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFA
          FAFAFAFAFAFAFAFAFAFAFAFAFAFAFBFBFBF2F2F2EFEEEEA1A0A0A2A1A0EAEAEA
          F3F3F3FFFFFFFDFDFDFDFDFDC1C2C2868787FDFDFDFDFDFDFDFDFDFDFDFDFDFD
          FDFDFDFDDFDFDF868787DFDFDFFDFDFDFDFDFDFDFDFDFFFFFFF1F0F0EBEAE9A2
          A1A0A2A2A1E7E7E6F1F1F1FBFBFBF8F8F8F8F8F88384840F1111F9F9F9F9F9F9
          F9F9F9F9F9F9F8F8F8F8F8F8BEBFBF0F1111BEBEBEF9F8F8F9F9F9F9F9F9FCFC
          FCEFEEEEE7E6E6A2A2A1A3A2A2E5E4E3EFEEEDF8F8F7F5F4F3F7F6F78283830F
          1111F5F5F4F7F6F6F6F6F6F6F6F6F6F6F5F5F5F4BBBCBB0F1111BCBCBCF4F4F4
          F6F5F5F7F6F6FAF9F9ECECECE3E3E2A3A2A2A3A3A2E1E1E0EBEBEBF5F5F4F2F1
          F1F6F5F58282820F1111BBBABABBBBBBBBBABAD8D8D8F5F4F4F4F4F4BBBBBB0F
          1111BBBBBBF4F3F3F4F4F3F4F2F2F7F7F6EAEAEAE0E0DFA3A3A2A4A3A3DDDDDD
          E9E8E8F5F4F4F4F4F4F6F6F58282820F11110F11110F11110F1111818282F5F5
          F4F5F4F3BBBBBB0F1111BBBBBBF4F4F3F5F4F4F2F1F1F5F4F4E8E8E8DCDDDCA4
          A3A3A4A3A3DCDBDAEAE9E9F6F5F5F4F4F3F5F5F48182820F1111F4F3F3F4F4F3
          F2F2F1F3F3F2F5F5F42B2C2C5657570F1111BBBBBAF3F3F2F4F3F3F0EFEFF2F1
          F1E6E6E5DAD9D8A4A4A3A5A4A3D9D8D7E8E8E7F5F5F4F4F3F2F5F5F48182820F
          1111B9B9B9BABAB9B8B8B8C6C8C7F4F4F47273730F11110F1111BABABAF2F2F1
          F2F1F1EEEEEDEFEFEEE4E3E3D7D5D5A5A4A4A5A5A4D5D3D3E4E4E3F2F2F1EFEF
          EEF3F3F28081810F11110F11110F11110F11110F1111F2F2F2F0EFEE9B9B9A0F
          1111B8B8B8EFEFEEEEEDEDEAE9E8ECECEBE2E2E1D3D2D2A5A5A4A6A5A5CFCECD
          E1E0E0EEEEEDEDECEBEEEDEDE0DFDFB4B4B3B2B3B2B3B3B2B1B1B0C2C1C0EEEE
          EDEBEAE9EAE9E8C4C4C4DEDEDDEAEAE9E9E8E7E7E6E5ECEBEADFDFDECFCECDA6
          A5A5A6A6A5D5D4D4ECEBEBF5F5F5F2F2F2F2F1F1EFEFEFEFEFEFF1F0F0F3F3F3
          F4F4F4F1F1F2EFEFEFEFEFEFF0F0F0F0F0EFEFEFEFF0F0EFF2F1F1F4F4F4F6F6
          F6ECEBEBD5D4D4A6A6A5AFAFAEE9E9E8F0F0F0EBEBEBECEBEBEDEDEDECECECEC
          ECECECEBEBEBEBEBEBEBEBECECEBECECECECECECECEBEBECECECECECECECEBEB
          ECEBEBEBEBEBEBEBEBF0F0F0E9E9E8AFAFAEE1E1E1CECDCCD2D1D0D1D0CECDCD
          CBCDCCCAD1D0CED1D0CED1D0CED1D0CED1D0CED1D0CED1D0CED1D0CED1D0CED1
          D0CED1D0CED1D0CED1D0CEC8C7C5C5C4C2D2D1D0CECDCCE1E1E1}
        Layout = blGlyphTop
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        Spacing = 2
        OnClick = btnFinalizarClick
      end
      object btnIdentConsumi: TSpeedButton
        Left = 90
        Top = -1
        Width = 88
        Height = 37
        Hint = 'Pressione F9 ou clique aqui para capturar os dados do consumidor'
        Caption = 'Identificar'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Glyph.Data = {
          42060000424D4206000000000000420000002800000018000000100000000100
          20000300000000060000232E0000232E000000000000000000000000FF0000FF
          0000FF000000E1E1E1FF979696FF91908FFF929190FF8A8988FF81807FFF918F
          8FFF929190FF929190FF929190FF929190FF929190FF929190FF929190FF9190
          8FFF848382FF828180FF8A8988FF939191FF737170FF686665FF91908FFF9796
          96FFE1E1E1FFA5A4A4FFE8E6E6FFE0DFDEFFDFDEDEFFDFDEDEFFE1E0E0FFDFDE
          DEFFDFDEDEFFDFDEDEFFDFDEDEFFDFDEDEFFDFDEDEFFDFDEDEFFDFDEDEFFDFDE
          DEFFDFDEDEFFDFDEDEFFDFDEDEFFDFDEDEFFDFDEDEFFDFDEDEFFE0DFDEFFE7E6
          E6FFA4A4A3FFA3A2A2FFF6F6F7FFD7D6D6FFC8C8C6FFC9C9C8FFCCCCCBFFC9C9
          C8FFC9C9C8FFC9C9C8FFC9C9C8FFC9C9C8FFC9C9C8FFC9C9C8FFC9C9C8FFC9C9
          C8FFC9C9C8FFC9C9C8FFC9C9C8FFC9C9C8FFC9C9C8FFC8C8C6FFD7D6D6FFF6F6
          F6FFA2A1A0FFA1A0A0FFEEEEEEFFF2F3F3FFFBFBFBFFFAFAFAFFFAFAFAFFFAFA
          FAFFFAFAFAFFFAFAFAFFFAFAFAFFFAFAFAFFFAFAFAFFFAFAFAFFFAFAFAFFFAFA
          FAFFFAFAFAFFFAFAFAFFFAFAFAFFFAFAFAFFFAFAFAFFFBFBFBFFF2F2F2FFEFEE
          EEFFA1A0A0FFA2A1A0FFEAEAEAFFF3F3F3FFFFFFFFFFFDFDFDFFFDFDFDFFC1C2
          C2FF868787FFFDFDFDFFFDFDFDFFFDFDFDFFFDFDFDFFEBEBEAFFECECEBFFD2D2
          D1FFEAE9E8FFEBEAE9FFEEEEEDFFFDFDFDFFFDFDFDFFFFFFFFFFF1F0F0FFEBEA
          E9FFA2A1A0FFA2A2A1FFE7E7E6FFF1F1F1FFFBFBFBFFF8F8F8FFF8F8F8FF8384
          84FF0F1111FFF9F9F9FFF9F9F9FFF9F9F9FFF9F9F9FFEFEFEEFF474848FF0F11
          11FF2A2C2CFF7F807FFFF2F2F2FFF9F9F9FFF9F9F9FFFCFCFCFFEFEEEEFFE7E6
          E6FFA2A2A1FFA3A2A2FFE5E4E3FFEFEEEDFFF8F8F7FFF5F4F3FFF7F6F7FF8283
          83FF0F1111FFF5F5F4FFF7F6F6FFF6F6F6FFF6F6F6FF9D9D9DFF0F1111FFBBBB
          BBFF808181FF0F1111FFBBBBBBFFF6F5F5FFF7F6F6FFFAF9F9FFECECECFFE3E3
          E2FFA3A2A2FFA3A3A2FFE1E1E0FFEBEBEBFFF5F5F4FFF2F1F1FFF6F5F5FF8282
          82FF0F1111FFBBBABAFFBBBBBBFFBBBABAFFD8D8D8FFF2F2F1FFACADACFF6466
          66FF818281FF0F1111FF828382FFF4F4F3FFF4F2F2FFF7F7F6FFEAEAEAFFE0E0
          DFFFA3A3A2FFA4A3A3FFDDDDDDFFE9E8E8FFF5F4F4FFF4F4F4FFF6F6F5FF8282
          82FF0F1111FF0F1111FF0F1111FF0F1111FF818282FFD6D7D6FF0F1111FF2B2D
          2DFF1D1E1EFF0F1111FF828382FFF5F4F4FFF2F1F1FFF5F4F4FFE8E8E8FFDCDD
          DCFFA4A3A3FFA4A3A3FFDCDBDAFFEAE9E9FFF6F5F5FFF4F4F3FFF5F5F4FF8182
          82FF0F1111FFF4F3F3FFF4F4F3FFF2F2F1FFF3F3F2FF737373FF1C1E1EFFF5F4
          F4FFD7D8D8FF0F1111FF828282FFF4F3F3FFF0EFEFFFF2F1F1FFE6E6E5FFDAD9
          D8FFA4A4A3FFA5A4A3FFD9D8D7FFE8E8E7FFF5F5F4FFF4F3F2FFF5F5F4FF8182
          82FF0F1111FFB9B9B9FFBABAB9FFB8B8B8FFC6C8C7FF828383FF1D1E1EFFF6F6
          F6FFE7E7E6FF0F1111FF919191FFF2F1F1FFEEEEEDFFEFEFEEFFE4E3E3FFD7D5
          D5FFA5A4A4FFA5A5A4FFD5D3D3FFE4E4E3FFF2F2F1FFEFEFEEFFF3F3F2FF8081
          81FF0F1111FF0F1111FF0F1111FF0F1111FF0F1111FFCECECEFF1D1F1FFF2B2D
          2DFF2B2D2DFF1D1F1FFFDBDBDBFFEEEDEDFFEAE9E8FFECECEBFFE2E2E1FFD3D2
          D2FFA5A5A4FFA6A5A5FFCFCECDFFE1E0E0FFEEEEEDFFEDECEBFFEEEDEDFFE0DF
          DFFFB4B4B3FFB2B3B2FFB3B3B2FFB1B1B0FFC2C1C0FFFDFDFDFFC1C2C2FF7778
          78FFA3A4A4FFFDFDFDFFFDFDFDFFE9E8E7FFE7E6E5FFECEBEAFFDFDFDEFFCFCE
          CDFFA6A5A5FFA6A6A5FFD5D4D4FFECEBEBFFF5F5F5FFF2F2F2FFF2F1F1FFEFEF
          EFFFEFEFEFFFF1F0F0FFF3F3F3FFF4F4F4FFF1F1F2FFF9F9F9FFF9F9F9FFF9F9
          F9FFF9F9F9FFF9F9F9FFF9F9F9FFF2F1F1FFF4F4F4FFF6F6F6FFECEBEBFFD5D4
          D4FFA6A6A5FFAFAFAEFFE9E9E8FFF0F0F0FFEBEBEBFFECEBEBFFEDEDEDFFECEC
          ECFFECECECFFECEBEBFFEBEBEBFFEBEBEBFFECECEBFFECECECFFECECECFFECEB
          EBFFECECECFFECECECFFECEBEBFFECEBEBFFEBEBEBFFEBEBEBFFF0F0F0FFE9E9
          E8FFAFAFAEFFE1E1E1FFCECDCCFFD2D1D0FFD1D0CEFFCDCDCBFFCDCCCAFFD1D0
          CEFFD1D0CEFFD1D0CEFFD1D0CEFFD1D0CEFFD1D0CEFFD1D0CEFFD1D0CEFFD1D0
          CEFFD1D0CEFFD1D0CEFFD1D0CEFFD1D0CEFFC8C7C5FFC5C4C2FFD2D1D0FFCECD
          CCFFE1E1E1FF}
        Layout = blGlyphTop
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        Spacing = 2
        OnClick = btnIdentConsumiClick
      end
      object SpeedButton1: TSpeedButton
        Left = 276
        Top = -1
        Width = 88
        Height = 37
        Hint = 'Pressione Esc ou clique aqui para fechar o Frente de Caixa'
        Caption = 'Cancelar'
        Glyph.Data = {
          B6040000424DB604000000000000360000002800000018000000100000000100
          18000000000080040000120B0000120B00000000000000000000E1E1E1979696
          91908F9291908A898881807F918F8F9291909291909291909291909291909291
          9092919091908F8483828281808A898893919173717068666591908F979696E1
          E1E1A5A4A4E8E6E6E0DFDEDFDEDEDFDEDEE1E0E0DFDEDEDFDEDEDFDEDEDFDEDE
          DFDEDEDFDEDEDFDEDEDFDEDEDFDEDEDFDEDEDFDEDEDFDEDEDFDEDEDFDEDEDFDE
          DEE0DFDEE7E6E6A4A4A3A3A2A2F6F6F7D7D6D6C8C8C6C9C9C8CCCCCBC9C9C8C9
          C9C8C9C9C8C9C9C8C9C9C8C9C9C8C9C9C8C9C9C8C9C9C8C9C9C8C9C9C8C9C9C8
          C9C9C8C9C9C8C8C8C6D7D6D6F6F6F6A2A1A0A1A0A0EEEEEEF2F3F3FBFBFBFAFA
          FAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFA
          FAFAFAFAFAFAFAFAFAFAFAFAFAFAFBFBFBF2F2F2EFEEEEA1A0A0A2A1A0EAEAEA
          F3F3F3C3C3C3868787868787868787868787868787C1C2C2DFDFDF9495957778
          78949595FDFDFDFDFDFDDFDFDF686969A3A4A4EEEEEEFFFFFFF1F0F0EBEAE9A2
          A1A0A2A2A1E7E7E6F1F1F18586860F1111494A4A494A4A494A4A494A4AA1A2A2
          494A4A1D1F1F494A4A0F1111848585AFB0B00F11113A3C3C1D1F1F494A4AFCFC
          FCEFEEEEE7E6E6A2A2A1A3A2A2E5E4E3EFEEED8384840F1111F7F6F7F6F6F5F5
          F5F4F5F5F4F7F6F66566669FA0A09FA09F1D1E1E5658576566663A3B3BF4F4F4
          9F9F9F2B2D2DDDDCDCECECECE3E3E2A3A2A2A3A3A2E1E1E0EBEBEB8283820F11
          11BCBBBBBBBBBBBBBBBBBBBABAF4F4F47373730F11110F11112B2D2DC9C9C948
          4949565757F4F3F3F4F4F3F4F2F2F7F7F6EAEAEAE0E0DFA3A3A2A4A3A3DDDDDD
          E9E8E88282820F11110F11110F11110F11110F1111F4F4F40F1111565757CACA
          C94849499E9F9F5757572B2D2DD7D8D78282821C1E1EF5F4F4E8E8E8DCDDDCA4
          A3A3A4A3A3DCDBDAEAE9E98283830F1111BBBBBBBBBBBBBABABAC9C8C8F4F4F3
          7273730F11110F11111C1E1EC9C8C8C9CAC91C1E1E0F11110F1111717272F2F1
          F1E6E6E5DAD9D8A4A4A3A5A4A3D9D8D7E8E8E78283820F1111BBBBBBBBBABAB9
          BAB9B9B9B9E5E4E3F1F0F0B8BAB9BBBBBBD6D6D5F2F1F1F4F4F4F3F3F2B9BAB9
          D6D5D5EEEEEDEFEFEEE4E3E3D7D5D5A5A4A4A5A5A4D5D3D3E4E4E38081810F11
          110F11110F11110F11110F1111B6B7B6EBEAEAEEEEEDF2F2F2F0EFEEEFEEEDF3
          F2F2F1F0F0EFEFEEEEEDEDEAE9E8ECECEBE2E2E1D3D2D2A5A5A4A6A5A5CFCECD
          E1E0E0D2D2D1B5B5B4B7B6B6B6B6B6B4B4B3B2B3B2DCDBDBE7E6E5EBEAE9EEEE
          EDEBEAE9EAE9E8EEEEEDECECEBEAEAE9E9E8E7E7E6E5ECEBEADFDFDECFCECDA6
          A5A5A6A6A5D5D4D4ECEBEBF5F5F5F2F2F2F2F1F1EFEFEFEFEFEFF1F0F0F3F3F3
          F4F4F4F1F1F2EFEFEFEFEFEFF0F0F0F0F0EFEFEFEFF0F0EFF2F1F1F4F4F4F6F6
          F6ECEBEBD5D4D4A6A6A5AFAFAEE9E9E8F0F0F0EBEBEBECEBEBEDEDEDECECECEC
          ECECECEBEBEBEBEBEBEBEBECECEBECECECECECECECEBEBECECECECECECECEBEB
          ECEBEBEBEBEBEBEBEBF0F0F0E9E9E8AFAFAEE1E1E1CECDCCD2D1D0D1D0CECDCD
          CBCDCCCAD1D0CED1D0CED1D0CED1D0CED1D0CED1D0CED1D0CED1D0CED1D0CED1
          D0CED1D0CED1D0CED1D0CEC8C7C5C5C4C2D2D1D0CECDCCE1E1E1}
        Layout = blGlyphTop
        ParentShowHint = False
        ShowHint = True
        Spacing = 2
        OnClick = btnCancelarClick
      end
      object btnCadastroRapido: TSpeedButton
        Left = 183
        Top = -1
        Width = 88
        Height = 37
        Hint = 
          'Pressione F11 ou clique aqui para pr'#233'-cadastrar o cliente de for' +
          'ma r'#225'pida'
        Caption = 'Cadastro R'#225'pido'
        Glyph.Data = {
          B6040000424DB604000000000000360000002800000018000000100000000100
          18000000000080040000120B0000120B00000000000000000000E1E1E1979696
          91908F9291908A898881807F918F8F9291909291909291909291909291909291
          9092919091908F8483828281808A898893919173717068666591908F979696E1
          E1E1A5A4A4E8E6E6E0DFDEDFDEDEDFDEDEE1E0E0DFDEDEDFDEDEDFDEDEDFDEDE
          DFDEDEDFDEDEDFDEDEDFDEDEDFDEDEDFDEDEDFDEDEDFDEDEDFDEDEDFDEDEDFDE
          DEE0DFDEE7E6E6A4A4A3A3A2A2F6F6F7D7D6D6C8C8C6C9C9C8CCCCCBC9C9C8C9
          C9C8C9C9C8C9C9C8C9C9C8C9C9C8C9C9C8C9C9C8C9C9C8C9C9C8C9C9C8C9C9C8
          C9C9C8C9C9C8C8C8C6D7D6D6F6F6F6A2A1A0A1A0A0EEEEEEF2F3F3FBFBFBFAFA
          FAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFA
          FAFAFAFAFAFAFAFAFAFAFAFAFAFAFBFBFBF2F2F2EFEEEEA1A0A0A2A1A0EAEAEA
          F3F3F3C3C3C3868787FDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDDFDFDF8687
          87DFDFDFFDFDFDFDFDFDFDFDFDC1C2C2868787FDFDFDFFFFFFF1F0F0EBEAE9A2
          A1A0A2A2A1E7E7E6F1F1F18586860F1111F8F8F8F8F8F8F8F8F8F9F9F9F9F9F9
          F9F9F9BEBFBF0F1111BEBEBEF9F9F9F8F8F8F8F8F88484840F1111F9F9F9FCFC
          FCEFEEEEE7E6E6A2A2A1A3A2A2E5E4E3EFEEED8384840F1111F7F6F7F6F6F5F5
          F5F4F5F5F4F7F6F6F6F6F6BCBDBD0F1111BBBCBBF5F5F4F6F6F6F6F5F5818282
          0F1111F7F6F6FAF9F9ECECECE3E3E2A3A2A2A3A3A2E1E1E0EBEBEB8283820F11
          11BCBBBBBBBBBBBBBBBBD7D7D7F4F4F4F4F3F3BBBBBB0F1111BBBBBBF4F4F4F5
          F4F4F4F4F48182820F1111F4F2F2F7F7F6EAEAEAE0E0DFA3A3A2A4A3A3DDDDDD
          E9E8E88282820F11110F11110F11110F1111828282F4F4F4F3F2F2BBBBBA0F11
          11BBBBBAF5F4F4F6F4F4F4F4F48182820F1111F2F1F1F5F4F4E8E8E8DCDDDCA4
          A3A3A4A3A3DCDBDAEAE9E98283830F1111F5F5F4F4F4F4F3F3F2F4F3F3F4F4F3
          2B2C2C5657570F1111BABABAF4F3F2C9CAC92B2D2C4749490F1111F0EFEFF2F1
          F1E6E6E5DAD9D8A4A4A3A5A4A3D9D8D7E8E8E78283820F1111BBBBBBBBBABAB9
          BAB9C7C7C7F3F2F17172720F11110F1111B9BAB9F2F1F1E6E6E64749490F1111
          0F1111EEEEEDEFEFEEE4E3E3D7D5D5A5A4A4A5A5A4D5D3D3E4E4E38081810F11
          110F11110F11110F11110F1111EEEEEDEBEAEA9A9B9A0F1111B8B7B7EFEEEDF3
          F2F2F1F0F06264630F1111EAE9E8ECECEBE2E2E1D3D2D2A5A5A4A6A5A5CFCECD
          E1E0E0E0E0DFB5B5B4B7B6B6B6B6B6B4B4B3C0C0C0EAE9E8E7E6E5EBEAE9C4C4
          C4DDDCDBEAE9E8EEEEEDECECEBEAEAE9B2B2B1E7E6E5ECEBEADFDFDECFCECDA6
          A5A5A6A6A5D5D4D4ECEBEBF5F5F5F2F2F2F2F1F1EFEFEFEFEFEFF1F0F0F3F3F3
          F4F4F4F1F1F2EFEFEFEFEFEFF0F0F0F0F0EFEFEFEFF0F0EFF2F1F1F4F4F4F6F6
          F6ECEBEBD5D4D4A6A6A5AFAFAEE9E9E8F0F0F0EBEBEBECEBEBEDEDEDECECECEC
          ECECECEBEBEBEBEBEBEBEBECECEBECECECECECECECEBEBECECECECECECECEBEB
          ECEBEBEBEBEBEBEBEBF0F0F0E9E9E8AFAFAEE1E1E1CECDCCD2D1D0D1D0CECDCD
          CBCDCCCAD1D0CED1D0CED1D0CED1D0CED1D0CED1D0CED1D0CED1D0CED1D0CED1
          D0CED1D0CED1D0CED1D0CEC8C7C5C5C4C2D2D1D0CECDCCE1E1E1}
        Layout = blGlyphTop
        ParentShowHint = False
        ShowHint = True
        Spacing = 2
        OnClick = btnCadastroRapidoClick
      end
    end
  end
  object dsTempECF: TDataSource
    DataSet = dmCupomFiscal.dbTemp_NFCE
    OnDataChange = dsTempECFDataChange
    Left = 715
    Top = 110
  end
end
