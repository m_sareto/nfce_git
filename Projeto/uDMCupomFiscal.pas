unit uDMCupomFiscal;
interface


uses
  SysUtils, Classes, DB, IBCustomDataSet, IBDatabase, IBQuery, IBSQL,
  DBClient, IBStoredProc, Forms, ACBrValidador, ACBrBase, ACBrECF, ACBrRFD,
  ACBrBAL, IBEvents, Provider, System.Math, VCL.Dialogs;

type
  TdmCupomFiscal = class(TDataModule)
    dbTemp_NFCE_Item: TIBDataSet;
    DataBase: TIBDatabase;
    Transaction: TIBTransaction;
    dbQuery1: TIBQuery;
    IBSQL1: TIBSQL;
    dbECF: TIBDataSet;
    dbECF_Item: TIBDataSet;
    dbECFCAIXA: TIBStringField;
    dbECFCUPOM: TIBStringField;
    dbECFDATA: TDateField;
    dbECFVLR_DINHEIRO: TIBBCDField;
    dbECFVLR_CHEQUE: TIBBCDField;
    dbECFVLR_CARTAO: TIBBCDField;
    dbECFVLR_TIKET: TIBBCDField;
    dbECFVLR_CONVENIO: TIBBCDField;
    dbECFVLR_SUBTOTAL: TIBBCDField;
    dbECFVLR_DESCONTO: TIBBCDField;
    dbECFVLR_TOTAL: TIBBCDField;
    dbECFCCUSTO: TIntegerField;
    dbECFESTOQUE: TIBStringField;
    dbECFCLIFOR: TIntegerField;
    dbECFCF_NOME: TIBStringField;
    dbECFCF_ENDE: TIBStringField;
    dbECFCF_MUNI: TIBStringField;
    dbECFCF_CNPJ_CPF: TIBStringField;
    dbECFCF_IE: TIBStringField;
    dbCR: TIBDataSet;
    dbECFFUNCIONARIO: TIntegerField;
    dbECFOPERACAO: TIntegerField;
    dbECFFORMA_PAG: TIntegerField;
    cdsPar__: TClientDataSet;
    cdsPar__Valor: TCurrencyField;
    cdsPar__Data: TDateField;
    dbCRID: TIntegerField;
    dbCRNDOC: TIntegerField;
    dbCRPARCELA: TIntegerField;
    dbCRCLIFOR: TIntegerField;
    dbCRCCUSTO: TIntegerField;
    dbCRFUNCIONARIO: TIntegerField;
    dbCRFORMA_PAG: TIntegerField;
    dbCRDATA_OPE: TDateField;
    dbCRDATA_VEN: TDateField;
    dbCRVALOR: TIBBCDField;
    dbCRRESTA: TIBBCDField;
    dbCROBSERVACAO: TIBStringField;
    dbCx_Cheque: TIBDataSet;
    dbCx_ChequeBANCO: TIntegerField;
    dbCx_ChequeCONTA: TIBStringField;
    dbCx_ChequeCHEQUE: TIBStringField;
    dbCx_ChequeAGENCIA: TIBStringField;
    dbCx_ChequeCORRENTISTA: TIBStringField;
    dbCx_ChequeCNPJCPF: TIBStringField;
    dbCx_ChequeSITUACAO: TIntegerField;
    dbCx_ChequeCLIFOR: TIntegerField;
    dbCx_ChequeDTA_RECEBIMENTO: TDateField;
    dbCx_ChequeDTA_VENCIMENTO: TDateField;
    dbCx_ChequeVALOR: TIBBCDField;
    dbCx_ChequeREP_NOME: TIBStringField;
    dbCx_ChequeREP_DATA: TDateField;
    dbCx_ChequeCONCILIADO: TIBStringField;
    dbECFCF_KM: TIntegerField;
    dbECFCF_PLACA: TIBStringField;
    dbECFTURNO: TIBStringField;
    qConsulta_CR: TIBQuery;
    qConsulta_CRNDOC: TIntegerField;
    qConsulta_CRPARCELA: TIntegerField;
    qConsulta_CRDATA_OPE: TDateField;
    qConsulta_CRDATA_VEN: TDateField;
    qConsulta_CRVALOR: TIBBCDField;
    qConsulta_CRRESTA: TIBBCDField;
    dbAbastecida: TIBDataSet;
    dbAbastecidaID: TIntegerField;
    dbAbastecidaBOMBA: TIntegerField;
    dbAbastecidaMERCADORIA: TIBStringField;
    dbAbastecidaPRECO: TIBBCDField;
    dbAbastecidaLITRO: TIBBCDField;
    dbAbastecidaDINHEIRO: TIBBCDField;
    dbAbastecidaNIVEPRECO: TIntegerField;
    dbAbastecidaDATA_HORA: TDateTimeField;
    dbECFEL_CHAVE: TIntegerField;
    dbECFID: TIntegerField;
    dbCREL_CHAVE: TIntegerField;
    dbAbastecidaID_CONCENTRADOR: TIntegerField;
    dbAbastecidaLT_ENCERRANTE: TIBBCDField;
    dbAbastecidaSTATUS: TIBStringField;
    qConsulta_Ext_Cli: TIBQuery;
    qConsulta_Ext_CliMERCADORIA: TIBStringField;
    qConsulta_Ext_CliVLR: TFloatField;
    qConsulta_Ext_CliQTD: TIBBCDField;
    qConsulta_Ext_CliTOTAL: TIBBCDField;
    qConsulta_Ext_CliDATA: TDateField;
    dbECFSTATUS: TIBStringField;
    dbQuery2: TIBQuery;
    dsQuery1: TDataSource;
    dsQuery2: TDataSource;
    dbReducaoZ: TIBDataSet;
    dbReducaoZLOJA: TIntegerField;
    dbReducaoZCAIXA: TIntegerField;
    dbReducaoZCRO: TIntegerField;
    dbReducaoZCRZ: TIntegerField;
    dbReducaoZCOO: TIntegerField;
    dbReducaoZDATA: TDateField;
    dbReducaoZVLR_GT_FIN: TIBBCDField;
    dbReducaoZVLR_BRUTO: TIBBCDField;
    dbReducaoZVLR_DESCONTO: TIBBCDField;
    dbReducaoZVLR_CANCELADO: TIBBCDField;
    dbReducaoZVLR_LIQUIDO: TIBBCDField;
    dbReducaoZVLR_BC_07: TIBBCDField;
    dbReducaoZVLR_BC_12: TIBBCDField;
    dbReducaoZVLR_BC_17: TIBBCDField;
    dbReducaoZVLR_BC_25: TIBBCDField;
    dbReducaoZVLR_BC_IS: TIBBCDField;
    dbReducaoZVLR_BC_NT: TIBBCDField;
    dbReducaoZVLR_BC_ST: TIBBCDField;
    dbReducaoZVLR_BC_SS: TIBBCDField;
    dbReducaoZVLR_07: TIBBCDField;
    dbReducaoZVLR_12: TIBBCDField;
    dbReducaoZVLR_17: TIBBCDField;
    dbReducaoZVLR_25: TIBBCDField;
    dbReducaoZVLR_SS: TIBBCDField;
    dbQuery3: TIBQuery;
    dbECFVLR_RECEBIDO: TIBBCDField;
    dbECFVLR_TROCO: TIBBCDField;
    qConsulta_Ext_CliVLR_DESCONTO: TIBBCDField;
    qConsulta_Ext_CliFPAG_DESCRICAO: TIBStringField;
    dbECFVLR_TOTTRIB: TIBBCDField;
    dbReducaoZCCUSTO: TIntegerField;
    dbAbastecidaPAGO: TIBStringField;
    dbReducaoZVLR_DESCONTO_ISSQN: TIBBCDField;
    dbECFFUN_USUARIO: TIntegerField;
    dbReducaoZVLR_CANCELADO_ISSQN: TIBBCDField;
    qECF: TIBQuery;
    qECF_Item: TIBQuery;
    qECF_FormaPag: TIBQuery;
    qEmitente: TIBQuery;
    qEmitenteID: TIntegerField;
    qEmitenteDESCRICAO: TIBStringField;
    qEmitenteCNPJ: TIBStringField;
    qEmitenteIE: TIBStringField;
    qEmitentePAIS: TIntegerField;
    qEmitenteMUNICIPIO: TIntegerField;
    qEmitenteENDERECO: TIBStringField;
    qEmitenteBAIRRO: TIBStringField;
    qEmitenteCEP: TIBStringField;
    qEmitenteUF: TIBStringField;
    qEmitenteFONE: TIBStringField;
    qEmitenteFAX: TIBStringField;
    qEmitenteNUMERO_END: TIBStringField;
    qEmitenteCOMPLEMENTO_END: TIBStringField;
    qEmitenteNOME_FANTASIA: TIBStringField;
    qEmitenteIM: TIBStringField;
    qEmitenteCNAE: TIBStringField;
    qEmitenteE_MAIL: TIBStringField;
    qEmitenteALI_SIMPLESN_ICM: TIBBCDField;
    qEmitenteCRT: TIntegerField;
    qEmitenteCALCULA_PESO: TIBStringField;
    qEmitenteDATA_EXP: TDateField;
    qEmitenteANOMES: TIntegerField;
    qEmitenteNFS_INICIAL: TIntegerField;
    qEmitenteNFS_FINAL: TIntegerField;
    qEmitenteNFS_AUTORIZACAO: TIBStringField;
    qEmitenteCTE_RNTRC: TIBStringField;
    qEmitenteXMUN: TIBStringField;
    qEmitenteXPAIS: TIBStringField;
    qECF_FormaPagID_ECF: TIntegerField;
    qECF_FormaPagID: TIntegerField;
    qECF_FormaPagTPAG: TIBStringField;
    qECF_FormaPagVPAG: TIBBCDField;
    qECF_FormaPagCARD_CNPJ: TIBStringField;
    qECF_FormaPagCARD_TBAND: TIBStringField;
    qECF_FormaPagCARD_CAUT: TIBStringField;
    dbECF_FormaPag: TIBDataSet;
    dbECF_FormaPagID_ECF: TIntegerField;
    dbECF_FormaPagID: TIntegerField;
    dbECF_FormaPagTPAG: TIBStringField;
    dbECF_FormaPagVPAG: TIBBCDField;
    dbECF_FormaPagCARD_CNPJ: TIBStringField;
    dbECF_FormaPagCARD_TBAND: TIBStringField;
    dbECF_FormaPagCARD_CAUT: TIBStringField;
    dbECFCOD_SIT: TIBStringField;
    dbECFINDPAG: TIBStringField;
    dbECFMODELO_DOC: TIBStringField;
    dbECFSERIE: TIBStringField;
    dbECFNUMERO: TIntegerField;
    dbECFHORA: TTimeField;
    dbECFTPIMP: TIBStringField;
    dbECFTPEMIS: TIBStringField;
    dbECFTPAMB: TIBStringField;
    dbECFINDPRES: TIBStringField;
    dbECFVERPROC: TIBStringField;
    dbECFCF_NUMERO_END: TIBStringField;
    dbECFCF_MUNICIPIO: TIntegerField;
    dbECFCF_CEP: TIBStringField;
    dbECFINDIEDEST: TIBStringField;
    dbECFMODFRETE: TIBStringField;
    dbECFVLR_BC_ICM: TIBBCDField;
    dbECFVLR_ICM: TIBBCDField;
    dbECFVLR_FRETE: TIBBCDField;
    dbECFVLR_SEG: TIBBCDField;
    dbECFVLR_PIS: TIBBCDField;
    dbECFVLR_COFINS: TIBBCDField;
    dbECFVLR_OUTRO: TIBBCDField;
    dbECFCF_BAIRRO: TIBStringField;
    dbTemp_NFCE: TIBDataSet;
    dbTemp_NFCE_FormaPag: TIBDataSet;
    dbECFCHAVE_NFCE: TIBStringField;
    qMunicipio: TIBQuery;
    qMunicipioID: TIntegerField;
    qMunicipioDESCRICAO: TIBStringField;
    qMunicipioUF: TIBStringField;
    qMunicipioID_UF: TIBStringField;
    dsMunicipio: TDataSource;
    dbECFCONT_DT_HR: TDateTimeField;
    dbECFCONT_JUST: TIBStringField;
    dbTemp_NFCECAIXA: TIBStringField;
    dbTemp_NFCESERIE: TIBStringField;
    dbTemp_NFCEMODELO_DOC: TIBStringField;
    dbTemp_NFCEDATA: TDateField;
    dbTemp_NFCEHORA: TTimeField;
    dbTemp_NFCECCUSTO: TIntegerField;
    dbTemp_NFCEFUNCIONARIO: TIntegerField;
    dbTemp_NFCEFUN_USUARIO: TIntegerField;
    dbTemp_NFCEOPERACAO: TIntegerField;
    dbTemp_NFCEESTOQUE: TIBStringField;
    dbTemp_NFCEFORMA_PAG: TIntegerField;
    dbTemp_NFCEINDPAG: TIBStringField;
    dbTemp_NFCETPIMP: TIBStringField;
    dbTemp_NFCETPEMIS: TIBStringField;
    dbTemp_NFCETPAMB: TIBStringField;
    dbTemp_NFCEINDPRES: TIBStringField;
    dbTemp_NFCECLIFOR: TIntegerField;
    dbTemp_NFCECF_NOME: TIBStringField;
    dbTemp_NFCECF_ENDE: TIBStringField;
    dbTemp_NFCECF_CNPJ_CPF: TIBStringField;
    dbTemp_NFCECF_IE: TIBStringField;
    dbTemp_NFCECF_NUMERO_END: TIBStringField;
    dbTemp_NFCECF_MUNICIPIO: TIntegerField;
    dbTemp_NFCECF_CEP: TIBStringField;
    dbTemp_NFCECF_BAIRRO: TIBStringField;
    dbTemp_NFCECF_PLACA: TIBStringField;
    dbTemp_NFCECF_KM: TIntegerField;
    dbTemp_NFCEINDIEDEST: TIBStringField;
    dbTemp_NFCEMODFRETE: TIBStringField;
    dbTemp_NFCEVLR_SUBTOTAL: TIBBCDField;
    dbTemp_NFCEVLR_DESCONTO: TIBBCDField;
    dbTemp_NFCEVLR_TOTAL: TIBBCDField;
    dbTemp_NFCEVLR_RECEBIDO: TIBBCDField;
    dbTemp_NFCEVLR_TROCO: TIBBCDField;
    dbTemp_NFCEVLR_BC_ICM: TIBBCDField;
    dbTemp_NFCEVLR_ICM: TIBBCDField;
    dbTemp_NFCEVLR_FRETE: TIBBCDField;
    dbTemp_NFCEVLR_SEG: TIBBCDField;
    dbTemp_NFCEVLR_PIS: TIBBCDField;
    dbTemp_NFCEVLR_COFINS: TIBBCDField;
    dbTemp_NFCEVLR_OUTRO: TIBBCDField;
    dbTemp_NFCETURNO: TIBStringField;
    dbTemp_NFCECOD_SIT: TIBStringField;
    dbTemp_NFCEOPERACAO_DESC: TIBStringField;
    dbTemp_NFCECCUSTO_DESC: TIBStringField;
    dbTemp_NFCEFORMA_PAG_DESC: TIBStringField;
    dbTemp_NFCEFUNCIONARIO_NOME: TIBStringField;
    dbTemp_NFCE_FormaPagCAIXA: TIBStringField;
    dbTemp_NFCE_FormaPagID: TIntegerField;
    dbTemp_NFCE_FormaPagTPAG: TIBStringField;
    dbTemp_NFCE_FormaPagVPAG: TIBBCDField;
    dbTemp_NFCE_FormaPagCARD_CNPJ: TIBStringField;
    dbTemp_NFCE_FormaPagCARD_TBAND: TIBStringField;
    dbTemp_NFCE_FormaPagCARD_CAUT: TIBStringField;
    dbTemp_NFCEVLR_BC_PIS: TIBBCDField;
    dbTemp_NFCEVLR_BC_COFINS: TIBBCDField;
    dbECFVLR_BC_PIS: TIBBCDField;
    dbECFVLR_BC_COFINS: TIBBCDField;
    dbTemp_NFCE_ItemCAIXA: TIBStringField;
    dbTemp_NFCE_ItemITEM: TIBStringField;
    dbTemp_NFCE_ItemMERCADORIA: TIBStringField;
    dbTemp_NFCE_ItemQUANTIDADE: TIBBCDField;
    dbTemp_NFCE_ItemUNIDADE: TIBStringField;
    dbTemp_NFCE_ItemNCM: TIBStringField;
    dbTemp_NFCE_ItemVLR_UNITARIO: TFloatField;
    dbTemp_NFCE_ItemVLR_CUSTO_ATUAL: TIBBCDField;
    dbTemp_NFCE_ItemVLR_DESCONTO_ITEM: TIBBCDField;
    dbTemp_NFCE_ItemVLR_DESCONTO: TIBBCDField;
    dbTemp_NFCE_ItemVLR_TOTAL: TIBBCDField;
    dbTemp_NFCE_ItemICM: TIBStringField;
    dbTemp_NFCE_ItemALI_ICM: TIBBCDField;
    dbTemp_NFCE_ItemVLR_FRETE: TIBBCDField;
    dbTemp_NFCE_ItemVLR_SEG: TIBBCDField;
    dbTemp_NFCE_ItemVLR_OUTRO: TIBBCDField;
    dbTemp_NFCE_ItemVLR_BC_PIS: TIBBCDField;
    dbTemp_NFCE_ItemVLR_PIS: TIBBCDField;
    dbTemp_NFCE_ItemVLR_BC_COFINS: TIBBCDField;
    dbTemp_NFCE_ItemVLR_COFINS: TIBBCDField;
    dbTemp_NFCE_ItemVLR_TOTTRIB: TIBBCDField;
    dbTemp_NFCE_ItemINDTOT: TIBStringField;
    dbTemp_NFCE_ItemQTD_CAN: TIBBCDField;
    dbTemp_NFCE_ItemABASTECIDA: TIntegerField;
    dbTemp_NFCE_ItemCST_PIS: TIBStringField;
    dbTemp_NFCE_ItemCST_COFINS: TIBStringField;
    dbTemp_NFCE_ItemCST_ICM: TIBStringField;
    dbTemp_NFCE_ItemBICO: TIntegerField;
    dbTemp_NFCE_ItemCFOP: TIntegerField;
    dbTemp_NFCE_ItemVLR_BC_ICM: TIBBCDField;
    dbTemp_NFCE_ItemVLR_ICM: TIBBCDField;
    dbECF_ItemID_ECF: TIntegerField;
    dbECF_ItemITEM: TIBStringField;
    dbECF_ItemMERCADORIA: TIBStringField;
    dbECF_ItemQTD: TIBBCDField;
    dbECF_ItemVLR: TFloatField;
    dbECF_ItemTOTAL: TIBBCDField;
    dbECF_ItemICM: TIBStringField;
    dbECF_ItemVALOR_CUSTO: TFloatField;
    dbECF_ItemBICO: TIntegerField;
    dbECF_ItemCST_PIS: TIBStringField;
    dbECF_ItemCST_COFINS: TIBStringField;
    dbECF_ItemABASTECIDA: TIntegerField;
    dbECF_ItemVLR_DESCONTO: TIBBCDField;
    dbECF_ItemQTD_CAN: TIBBCDField;
    dbECF_ItemVLR_TOTTRIB: TIBBCDField;
    dbECF_ItemVLR_DESCONTO_ITEM: TIBBCDField;
    dbECF_ItemCFOP: TIntegerField;
    dbECF_ItemCST_ICM: TIBStringField;
    dbECF_ItemNCM: TIBStringField;
    dbECF_ItemUNIDADE: TIBStringField;
    dbECF_ItemINDTOT: TIBStringField;
    dbECF_ItemALI_ICM: TIBBCDField;
    dbECF_ItemVLR_BC_ICM: TIBBCDField;
    dbECF_ItemVLR_ICM: TIBBCDField;
    dbECF_ItemVLR_FRETE: TIBBCDField;
    dbECF_ItemVLR_SEG: TIBBCDField;
    dbECF_ItemVLR_OUTRO: TIBBCDField;
    dbECF_ItemVLR_BC_PIS: TIBBCDField;
    dbECF_ItemVLR_PIS: TIBBCDField;
    dbECF_ItemVLR_BC_COFINS: TIBBCDField;
    dbECF_ItemVLR_COFINS: TIBBCDField;
    qECF_ItemID_ECF: TIntegerField;
    qECF_ItemITEM: TIBStringField;
    qECF_ItemMERCADORIA: TIBStringField;
    qECF_ItemQTD: TIBBCDField;
    qECF_ItemVLR: TFloatField;
    qECF_ItemTOTAL: TIBBCDField;
    qECF_ItemICM: TIBStringField;
    qECF_ItemVALOR_CUSTO: TFloatField;
    qECF_ItemBICO: TIntegerField;
    qECF_ItemCST_PIS: TIBStringField;
    qECF_ItemCST_COFINS: TIBStringField;
    qECF_ItemABASTECIDA: TIntegerField;
    qECF_ItemVLR_DESCONTO: TIBBCDField;
    qECF_ItemQTD_CAN: TIBBCDField;
    qECF_ItemVLR_TOTTRIB: TIBBCDField;
    qECF_ItemVLR_DESCONTO_ITEM: TIBBCDField;
    qECF_ItemCFOP: TIntegerField;
    qECF_ItemCST_ICM: TIBStringField;
    qECF_ItemNCM: TIBStringField;
    qECF_ItemUNIDADE: TIBStringField;
    qECF_ItemINDTOT: TIBStringField;
    qECF_ItemALI_ICM: TIBBCDField;
    qECF_ItemVLR_BC_ICM: TIBBCDField;
    qECF_ItemVLR_ICM: TIBBCDField;
    qECF_ItemVLR_FRETE: TIBBCDField;
    qECF_ItemVLR_SEG: TIBBCDField;
    qECF_ItemVLR_OUTRO: TIBBCDField;
    qECF_ItemVLR_BC_PIS: TIBBCDField;
    qECF_ItemVLR_PIS: TIBBCDField;
    qECF_ItemVLR_BC_COFINS: TIBBCDField;
    qECF_ItemVLR_COFINS: TIBBCDField;
    qECF_ItemCEAN: TIBStringField;
    qECF_ItemORIGEM_MER: TIBStringField;
    qECFID: TIntegerField;
    qECFCAIXA: TIBStringField;
    qECFCUPOM: TIBStringField;
    qECFDATA: TDateField;
    qECFVLR_DINHEIRO: TIBBCDField;
    qECFVLR_CHEQUE: TIBBCDField;
    qECFVLR_CARTAO: TIBBCDField;
    qECFVLR_TIKET: TIBBCDField;
    qECFVLR_CONVENIO: TIBBCDField;
    qECFVLR_SUBTOTAL: TIBBCDField;
    qECFVLR_DESCONTO: TIBBCDField;
    qECFVLR_TOTAL: TIBBCDField;
    qECFVLR_TOTTRIB: TIBBCDField;
    qECFVLR_RECEBIDO: TIBBCDField;
    qECFVLR_TROCO: TIBBCDField;
    qECFCCUSTO: TIntegerField;
    qECFTURNO: TIBStringField;
    qECFFUNCIONARIO: TIntegerField;
    qECFESTOQUE: TIBStringField;
    qECFOPERACAO: TIntegerField;
    qECFFORMA_PAG: TIntegerField;
    qECFCLIFOR: TIntegerField;
    qECFCF_NOME: TIBStringField;
    qECFCF_ENDE: TIBStringField;
    qECFCF_MUNI: TIBStringField;
    qECFCF_CNPJ_CPF: TIBStringField;
    qECFCF_IE: TIBStringField;
    qECFCF_PLACA: TIBStringField;
    qECFCF_KM: TIntegerField;
    qECFEL_CHAVE: TIntegerField;
    qECFSTATUS: TIBStringField;
    qECFFUN_USUARIO: TIntegerField;
    qECFCONT_DT_HR: TDateTimeField;
    qECFCONT_JUST: TIBStringField;
    qECFCOD_SIT: TIBStringField;
    qECFINDPAG: TIBStringField;
    qECFMODELO_DOC: TIBStringField;
    qECFSERIE: TIBStringField;
    qECFNUMERO: TIntegerField;
    qECFHORA: TTimeField;
    qECFCHAVE_NFCE: TIBStringField;
    qECFTPIMP: TIBStringField;
    qECFTPEMIS: TIBStringField;
    qECFTPAMB: TIBStringField;
    qECFINDPRES: TIBStringField;
    qECFVERPROC: TIBStringField;
    qECFCF_NUMERO_END: TIBStringField;
    qECFCF_MUNICIPIO: TIntegerField;
    qECFCF_BAIRRO: TIBStringField;
    qECFCF_CEP: TIBStringField;
    qECFINDIEDEST: TIBStringField;
    qECFMODFRETE: TIBStringField;
    qECFVLR_BC_ICM: TIBBCDField;
    qECFVLR_ICM: TIBBCDField;
    qECFVLR_FRETE: TIBBCDField;
    qECFVLR_SEG: TIBBCDField;
    qECFVLR_BC_PIS: TIBBCDField;
    qECFVLR_PIS: TIBBCDField;
    qECFVLR_BC_COFINS: TIBBCDField;
    qECFVLR_COFINS: TIBBCDField;
    qECFVLR_OUTRO: TIBBCDField;
    qECFNATOPE: TIBStringField;
    qECFCF_XMUN: TIBStringField;
    qECFCF_UF: TIBStringField;
    dbECFINFADFISCO: TIBStringField;
    dbECFINFCPL: TIBStringField;
    dbTemp_NFCEINFADFISCO: TIBStringField;
    dbTemp_NFCEINFCPL: TIBStringField;
    qECFINFADFISCO: TIBStringField;
    qECFINFCPL: TIBStringField;
    dbECF_Fatura: TIBDataSet;
    dbECF_FaturaID_ECF: TIntegerField;
    dbECF_FaturaDATA: TDateField;
    dbECF_FaturaVALOR: TIBBCDField;
    dbECF_FaturaNDUP: TIBStringField;
    dbECF_FaturaPARCELA: TIntegerField;
    dbTemp_NFCE_Fatura: TIBDataSet;
    dbTemp_NFCE_FaturaCAIXA: TIBStringField;
    dbTemp_NFCE_FaturaDATA: TDateField;
    dbTemp_NFCE_FaturaVALOR: TIBBCDField;
    dbTemp_NFCE_FaturaNDUP: TIBStringField;
    dbTemp_NFCE_FaturaPARCELA: TIntegerField;
    qECF_Fatura: TIBQuery;
    qECF_FaturaID_ECF: TIntegerField;
    qECF_FaturaDATA: TDateField;
    qECF_FaturaVALOR: TIBBCDField;
    qECF_FaturaNDUP: TIBStringField;
    qECF_FaturaPARCELA: TIntegerField;
    dbTemp_NFCE_FormaPagFORMA_PAG: TIntegerField;
    dbECF_FormaPagFORMA_PAG: TIntegerField;
    dbECF_FormaPagINDICE_ECF: TIBStringField;
    DatabaseRemota: TIBDatabase;
    TransactionRemota: TIBTransaction;
    qurEstMercadorias_Remota: TIBQuery;
    qurEstMercadorias_RemotaID: TIBStringField;
    qurEstMercadorias_RemotaSUCINTO: TIBStringField;
    qurEstMercadorias_RemotaUNIDADE: TIBStringField;
    qurEstMercadorias_RemotaSUBGRUPO: TIBStringField;
    qurEstMercadorias_RemotaMARCA: TIBStringField;
    qurEstMercadorias_RemotaMODELO: TIBStringField;
    qurEstMercadorias_RemotaATIVO: TIBStringField;
    qurEstMercadorias_RemotaCOMPOSICAO: TIBStringField;
    qurEstMercadorias_RemotaLOCAL: TIBStringField;
    qurEstMercadorias_RemotaRECEITA: TIBStringField;
    qurEstMercadorias_RemotaSERVICO: TIBStringField;
    qurEstMercadorias_RemotaAPLICACAO: TIBStringField;
    qurEstMercadorias_RemotaCODIGO_ORIGINAL: TIBStringField;
    qurEstMercadorias_RemotaDESCONTO: TIBBCDField;
    qurEstMercadorias_RemotaPESO_LIQ: TIBBCDField;
    qurEstMercadorias_RemotaPESO_BRU: TIBBCDField;
    qurEstMercadorias_RemotaF_CONVERSAO: TIBBCDField;
    qurEstMercadorias_RemotaCUSTO_INICIAL: TIBBCDField;
    qurEstMercadorias_RemotaCUSTO_ULTIMO: TIBBCDField;
    qurEstMercadorias_RemotaCUSTO_MEDIO: TIBBCDField;
    qurEstMercadorias_RemotaVENDA: TIBBCDField;
    qurEstMercadorias_RemotaMARGEM: TIBBCDField;
    qurEstMercadorias_RemotaCOFINS: TIBBCDField;
    qurEstMercadorias_RemotaIPI: TIBBCDField;
    qurEstMercadorias_RemotaISSQN: TIBBCDField;
    qurEstMercadorias_RemotaPIS: TIBBCDField;
    qurEstMercadorias_RemotaCSLL: TIBBCDField;
    qurEstMercadorias_RemotaPREVIDENCIA: TIBBCDField;
    qurEstMercadorias_RemotaII: TIBBCDField;
    qurEstMercadorias_RemotaIRRF: TIBBCDField;
    qurEstMercadorias_RemotaICM_ECF: TIBStringField;
    qurEstMercadorias_RemotaECF_CFOP: TIntegerField;
    qurEstMercadorias_RemotaECF_CST_ICM: TIBStringField;
    qurEstMercadorias_RemotaNCM: TIBStringField;
    qurEstMercadorias_RemotaEXTIPI: TIBStringField;
    qurEstMercadorias_RemotaDT_VENDA: TDateField;
    qurEstMercadorias_RemotaDT_COMPRA: TDateField;
    qurEstMercadorias_RemotaCCAPITAL: TIBBCDField;
    qurEstMercadorias_RemotaFABRICANTE: TIntegerField;
    qurEstMercadorias_RemotaGTIN: TIBStringField;
    qurEstMercadorias_RemotaCONTA_CONTABIL: TIBStringField;
    qurEstMercadorias_RemotaRA_TOXICIDADE: TIBStringField;
    qurEstMercadorias_RemotaRA_CONCENTRACAO: TIBStringField;
    qurEstMercadorias_RemotaRA_FORMULACAO: TIBStringField;
    qurEstMercadorias_RemotaRA_INGREDIENTE_ATIVO: TIBStringField;
    qurEstMercadorias_RemotaRA_CLASSE: TIBStringField;
    qurEstMercadorias_RemotaRA_UNIDADE: TIBStringField;
    qurEstMercadorias_RemotaORIGEM_MER: TIBStringField;
    qurEstMercadorias_RemotaCST_IPI: TIBStringField;
    qurEstMercadorias_RemotaCST_PIS: TIBStringField;
    qurEstMercadorias_RemotaCST_COFINS: TIBStringField;
    qurEstMercadorias_RemotaCLS: TIntegerField;
    qurEstMercadorias_RemotaCST_PIS_ENTRADA: TIBStringField;
    qurEstMercadorias_RemotaCST_COFINS_ENTRADA: TIBStringField;
    qurEstMercadorias_RemotaPIS_ENTRADA: TIBBCDField;
    qurEstMercadorias_RemotaCOFINS_ENTRADA: TIBBCDField;
    qurEstMercadorias_RemotaAMPARO_CONTRIBUICOES: TIntegerField;
    qurEstMercadorias_RemotaSM_DTE_PROMOCAO: TDateField;
    qurEstMercadorias_RemotaSM_DTS_PROMOCAO: TDateField;
    qurEstMercadorias_RemotaSM_VENDA_PROMOCAO: TIBBCDField;
    qurEstMercadorias_RemotaSM_BLC_UP: TIBStringField;
    qurEstMercadorias_RemotaSM_BLC_DIAS: TIntegerField;
    qurEstMercadorias_RemotaSM_BLC_ATIVO: TIBStringField;
    qurEstMercadorias_RemotaNAT_REC_PIS_COFINS: TSmallintField;
    qurEstMercadorias_RemotaCOD_ANP: TIBStringField;
    qurEstMercadorias_RemotaTIPO_ITEM: TIBStringField;
    qurEstMercadorias_RemotaSUBTIPO_ITEM: TIBStringField;
    qurEstMercadorias_RemotaID_CODIGO: TIntegerField;
    qurEstMercadorias_RemotaAMPARO_PREVIDENCIA: TIntegerField;
    qurEstMercadorias_RemotaNFCI: TIBStringField;
    qurEstMercadorias_RemotaIPI_ENTRADA: TIBBCDField;
    qurEstMercadorias_RemotaCST_IPI_ENTRADA: TIBStringField;
    qurEstMercadorias_RemotaPEDE_LOTE: TIBStringField;
    dbEstMercadorias: TIBDataSet;
    dbEstMercadoriasID: TIBStringField;
    dbEstMercadoriasSUCINTO: TIBStringField;
    dbEstMercadoriasUNIDADE: TIBStringField;
    dbEstMercadoriasSUBGRUPO: TIBStringField;
    dbEstMercadoriasMARCA: TIBStringField;
    dbEstMercadoriasMODELO: TIBStringField;
    dbEstMercadoriasATIVO: TIBStringField;
    dbEstMercadoriasCOMPOSICAO: TIBStringField;
    dbEstMercadoriasLOCAL: TIBStringField;
    dbEstMercadoriasRECEITA: TIBStringField;
    dbEstMercadoriasSERVICO: TIBStringField;
    dbEstMercadoriasAPLICACAO: TIBStringField;
    dbEstMercadoriasCODIGO_ORIGINAL: TIBStringField;
    dbEstMercadoriasDESCONTO: TIBBCDField;
    dbEstMercadoriasPESO_LIQ: TIBBCDField;
    dbEstMercadoriasPESO_BRU: TIBBCDField;
    dbEstMercadoriasF_CONVERSAO: TIBBCDField;
    dbEstMercadoriasCUSTO_INICIAL: TIBBCDField;
    dbEstMercadoriasCUSTO_ULTIMO: TIBBCDField;
    dbEstMercadoriasCUSTO_MEDIO: TIBBCDField;
    dbEstMercadoriasVENDA: TIBBCDField;
    dbEstMercadoriasMARGEM: TIBBCDField;
    dbEstMercadoriasIPI: TIBBCDField;
    dbEstMercadoriasISSQN: TIBBCDField;
    dbEstMercadoriasCSLL: TIBBCDField;
    dbEstMercadoriasPREVIDENCIA: TIBBCDField;
    dbEstMercadoriasII: TIBBCDField;
    dbEstMercadoriasIRRF: TIBBCDField;
    dbEstMercadoriasICM_ECF: TIBStringField;
    dbEstMercadoriasECF_CFOP: TIntegerField;
    dbEstMercadoriasECF_CST_ICM: TIBStringField;
    dbEstMercadoriasNCM: TIBStringField;
    dbEstMercadoriasEXTIPI: TIBStringField;
    dbEstMercadoriasDT_VENDA: TDateField;
    dbEstMercadoriasDT_COMPRA: TDateField;
    dbEstMercadoriasCCAPITAL: TIBBCDField;
    dbEstMercadoriasFABRICANTE: TIntegerField;
    dbEstMercadoriasGTIN: TIBStringField;
    dbEstMercadoriasCONTA_CONTABIL: TIBStringField;
    dbEstMercadoriasRA_TOXICIDADE: TIBStringField;
    dbEstMercadoriasRA_CONCENTRACAO: TIBStringField;
    dbEstMercadoriasRA_FORMULACAO: TIBStringField;
    dbEstMercadoriasRA_INGREDIENTE_ATIVO: TIBStringField;
    dbEstMercadoriasRA_CLASSE: TIBStringField;
    dbEstMercadoriasRA_UNIDADE: TIBStringField;
    dbEstMercadoriasORIGEM_MER: TIBStringField;
    dbEstMercadoriasCST_IPI: TIBStringField;
    dbEstMercadoriasCST_PIS: TIBStringField;
    dbEstMercadoriasCST_COFINS: TIBStringField;
    dbEstMercadoriasCLS: TIntegerField;
    dbEstMercadoriasCST_PIS_ENTRADA: TIBStringField;
    dbEstMercadoriasCST_COFINS_ENTRADA: TIBStringField;
    dbEstMercadoriasPIS_ENTRADA: TIBBCDField;
    dbEstMercadoriasCOFINS_ENTRADA: TIBBCDField;
    dbEstMercadoriasAMPARO_CONTRIBUICOES: TIntegerField;
    dbEstMercadoriasSM_DTE_PROMOCAO: TDateField;
    dbEstMercadoriasSM_DTS_PROMOCAO: TDateField;
    dbEstMercadoriasSM_VENDA_PROMOCAO: TIBBCDField;
    dbEstMercadoriasSM_BLC_UP: TIBStringField;
    dbEstMercadoriasSM_BLC_DIAS: TIntegerField;
    dbEstMercadoriasSM_BLC_ATIVO: TIBStringField;
    dbEstMercadoriasNAT_REC_PIS_COFINS: TSmallintField;
    dbEstMercadoriasCOD_ANP: TIBStringField;
    dbEstMercadoriasTIPO_ITEM: TIBStringField;
    dbEstMercadoriasSUBTIPO_ITEM: TIBStringField;
    dbEstMercadoriasID_CODIGO: TIntegerField;
    dbEstMercadoriasAMPARO_PREVIDENCIA: TIntegerField;
    dbEstMercadoriasNFCI: TIBStringField;
    dbEstMercadoriasIPI_ENTRADA: TIBBCDField;
    dbEstMercadoriasCST_IPI_ENTRADA: TIBStringField;
    dbEstMercadoriasPEDE_LOTE: TIBStringField;
    dbECF_Rem: TIBDataSet;
    dbECF_Item_Rem: TIBDataSet;
    dbECF_FormaPag_Rem: TIBDataSet;
    dbECF_Fatura_Rem: TIBDataSet;
    dbECF_RemID: TIntegerField;
    dbECF_RemCAIXA: TIBStringField;
    dbECF_RemCUPOM: TIBStringField;
    dbECF_RemDATA: TDateField;
    dbECF_RemVLR_DINHEIRO: TIBBCDField;
    dbECF_RemVLR_CHEQUE: TIBBCDField;
    dbECF_RemVLR_CARTAO: TIBBCDField;
    dbECF_RemVLR_TIKET: TIBBCDField;
    dbECF_RemVLR_CONVENIO: TIBBCDField;
    dbECF_RemVLR_SUBTOTAL: TIBBCDField;
    dbECF_RemVLR_DESCONTO: TIBBCDField;
    dbECF_RemVLR_TOTAL: TIBBCDField;
    dbECF_RemVLR_TOTTRIB: TIBBCDField;
    dbECF_RemVLR_RECEBIDO: TIBBCDField;
    dbECF_RemVLR_TROCO: TIBBCDField;
    dbECF_RemCCUSTO: TIntegerField;
    dbECF_RemTURNO: TIBStringField;
    dbECF_RemFUNCIONARIO: TIntegerField;
    dbECF_RemESTOQUE: TIBStringField;
    dbECF_RemOPERACAO: TIntegerField;
    dbECF_RemFORMA_PAG: TIntegerField;
    dbECF_RemCLIFOR: TIntegerField;
    dbECF_RemCF_NOME: TIBStringField;
    dbECF_RemCF_ENDE: TIBStringField;
    dbECF_RemCF_MUNI: TIBStringField;
    dbECF_RemCF_CNPJ_CPF: TIBStringField;
    dbECF_RemCF_IE: TIBStringField;
    dbECF_RemCF_PLACA: TIBStringField;
    dbECF_RemCF_KM: TIntegerField;
    dbECF_RemEL_CHAVE: TIntegerField;
    dbECF_RemSTATUS: TIBStringField;
    dbECF_RemFUN_USUARIO: TIntegerField;
    dbECF_RemCONT_DT_HR: TDateTimeField;
    dbECF_RemCONT_JUST: TIBStringField;
    dbECF_RemCOD_SIT: TIBStringField;
    dbECF_RemINDPAG: TIBStringField;
    dbECF_RemMODELO_DOC: TIBStringField;
    dbECF_RemSERIE: TIBStringField;
    dbECF_RemNUMERO: TIntegerField;
    dbECF_RemHORA: TTimeField;
    dbECF_RemCHAVE_NFCE: TIBStringField;
    dbECF_RemTPIMP: TIBStringField;
    dbECF_RemTPEMIS: TIBStringField;
    dbECF_RemTPAMB: TIBStringField;
    dbECF_RemINDPRES: TIBStringField;
    dbECF_RemVERPROC: TIBStringField;
    dbECF_RemCF_NUMERO_END: TIBStringField;
    dbECF_RemCF_MUNICIPIO: TIntegerField;
    dbECF_RemCF_BAIRRO: TIBStringField;
    dbECF_RemCF_CEP: TIBStringField;
    dbECF_RemINDIEDEST: TIBStringField;
    dbECF_RemMODFRETE: TIBStringField;
    dbECF_RemVLR_BC_ICM: TIBBCDField;
    dbECF_RemVLR_ICM: TIBBCDField;
    dbECF_RemVLR_FRETE: TIBBCDField;
    dbECF_RemVLR_SEG: TIBBCDField;
    dbECF_RemVLR_BC_PIS: TIBBCDField;
    dbECF_RemVLR_PIS: TIBBCDField;
    dbECF_RemVLR_BC_COFINS: TIBBCDField;
    dbECF_RemVLR_COFINS: TIBBCDField;
    dbECF_RemVLR_OUTRO: TIBBCDField;
    dbECF_RemINFADFISCO: TIBStringField;
    dbECF_RemINFCPL: TIBStringField;
    dbECF_RemENVIADO: TIBStringField;
    dbECF_FormaPag_RemID_ECF: TIntegerField;
    dbECF_FormaPag_RemID: TIntegerField;
    dbECF_FormaPag_RemTPAG: TIBStringField;
    dbECF_FormaPag_RemVPAG: TIBBCDField;
    dbECF_FormaPag_RemCARD_CNPJ: TIBStringField;
    dbECF_FormaPag_RemCARD_TBAND: TIBStringField;
    dbECF_FormaPag_RemCARD_CAUT: TIBStringField;
    dbECF_FormaPag_RemFORMA_PAG: TIntegerField;
    dbECF_FormaPag_RemINDICE_ECF: TIBStringField;
    dbECF_Item_RemID_ECF: TIntegerField;
    dbECF_Item_RemITEM: TIBStringField;
    dbECF_Item_RemMERCADORIA: TIBStringField;
    dbECF_Item_RemQTD: TIBBCDField;
    dbECF_Item_RemVLR: TFloatField;
    dbECF_Item_RemTOTAL: TIBBCDField;
    dbECF_Item_RemICM: TIBStringField;
    dbECF_Item_RemVALOR_CUSTO: TFloatField;
    dbECF_Item_RemBICO: TIntegerField;
    dbECF_Item_RemCST_PIS: TIBStringField;
    dbECF_Item_RemCST_COFINS: TIBStringField;
    dbECF_Item_RemABASTECIDA: TIntegerField;
    dbECF_Item_RemVLR_DESCONTO: TIBBCDField;
    dbECF_Item_RemQTD_CAN: TIBBCDField;
    dbECF_Item_RemVLR_TOTTRIB: TIBBCDField;
    dbECF_Item_RemVLR_DESCONTO_ITEM: TIBBCDField;
    dbECF_Item_RemCFOP: TIntegerField;
    dbECF_Item_RemCST_ICM: TIBStringField;
    dbECF_Item_RemNCM: TIBStringField;
    dbECF_Item_RemUNIDADE: TIBStringField;
    dbECF_Item_RemINDTOT: TIBStringField;
    dbECF_Item_RemALI_ICM: TIBBCDField;
    dbECF_Item_RemVLR_BC_ICM: TIBBCDField;
    dbECF_Item_RemVLR_ICM: TIBBCDField;
    dbECF_Item_RemVLR_FRETE: TIBBCDField;
    dbECF_Item_RemVLR_SEG: TIBBCDField;
    dbECF_Item_RemVLR_OUTRO: TIBBCDField;
    dbECF_Item_RemVLR_BC_PIS: TIBBCDField;
    dbECF_Item_RemVLR_PIS: TIBBCDField;
    dbECF_Item_RemVLR_BC_COFINS: TIBBCDField;
    dbECF_Item_RemVLR_COFINS: TIBBCDField;
    dbECF_Fatura_RemID_ECF: TIntegerField;
    dbECF_Fatura_RemDATA: TDateField;
    dbECF_Fatura_RemVALOR: TIBBCDField;
    dbECF_Fatura_RemNDUP: TIBStringField;
    dbECF_Fatura_RemPARCELA: TIntegerField;
    qurECF: TIBQuery;
    qurECFID: TIntegerField;
    qurECFCAIXA: TIBStringField;
    qurECFCUPOM: TIBStringField;
    qurECFDATA: TDateField;
    qurECFVLR_DINHEIRO: TIBBCDField;
    qurECFVLR_CHEQUE: TIBBCDField;
    qurECFVLR_CARTAO: TIBBCDField;
    qurECFVLR_TIKET: TIBBCDField;
    qurECFVLR_CONVENIO: TIBBCDField;
    qurECFVLR_SUBTOTAL: TIBBCDField;
    qurECFVLR_DESCONTO: TIBBCDField;
    qurECFVLR_TOTAL: TIBBCDField;
    qurECFVLR_TOTTRIB: TIBBCDField;
    qurECFVLR_RECEBIDO: TIBBCDField;
    qurECFVLR_TROCO: TIBBCDField;
    qurECFCCUSTO: TIntegerField;
    qurECFTURNO: TIBStringField;
    qurECFFUNCIONARIO: TIntegerField;
    qurECFESTOQUE: TIBStringField;
    qurECFOPERACAO: TIntegerField;
    qurECFFORMA_PAG: TIntegerField;
    qurECFCLIFOR: TIntegerField;
    qurECFCF_NOME: TIBStringField;
    qurECFCF_ENDE: TIBStringField;
    qurECFCF_MUNI: TIBStringField;
    qurECFCF_CNPJ_CPF: TIBStringField;
    qurECFCF_IE: TIBStringField;
    qurECFCF_PLACA: TIBStringField;
    qurECFCF_KM: TIntegerField;
    qurECFEL_CHAVE: TIntegerField;
    qurECFSTATUS: TIBStringField;
    qurECFFUN_USUARIO: TIntegerField;
    qurECFCONT_DT_HR: TDateTimeField;
    qurECFCONT_JUST: TIBStringField;
    qurECFCOD_SIT: TIBStringField;
    qurECFINDPAG: TIBStringField;
    qurECFMODELO_DOC: TIBStringField;
    qurECFSERIE: TIBStringField;
    qurECFNUMERO: TIntegerField;
    qurECFHORA: TTimeField;
    qurECFCHAVE_NFCE: TIBStringField;
    qurECFTPIMP: TIBStringField;
    qurECFTPEMIS: TIBStringField;
    qurECFTPAMB: TIBStringField;
    qurECFINDPRES: TIBStringField;
    qurECFVERPROC: TIBStringField;
    qurECFCF_NUMERO_END: TIBStringField;
    qurECFCF_MUNICIPIO: TIntegerField;
    qurECFCF_BAIRRO: TIBStringField;
    qurECFCF_CEP: TIBStringField;
    qurECFINDIEDEST: TIBStringField;
    qurECFMODFRETE: TIBStringField;
    qurECFVLR_BC_ICM: TIBBCDField;
    qurECFVLR_ICM: TIBBCDField;
    qurECFVLR_FRETE: TIBBCDField;
    qurECFVLR_SEG: TIBBCDField;
    qurECFVLR_BC_PIS: TIBBCDField;
    qurECFVLR_PIS: TIBBCDField;
    qurECFVLR_BC_COFINS: TIBBCDField;
    qurECFVLR_COFINS: TIBBCDField;
    qurECFVLR_OUTRO: TIBBCDField;
    qurECFINFADFISCO: TIBStringField;
    qurECFINFCPL: TIBStringField;
    qurECFENVIADO: TIBStringField;
    qurECF_Item: TIBQuery;
    qurECF_FormaPag: TIBQuery;
    qurECF_Fatura: TIBQuery;
    qurECF_ItemID_ECF: TIntegerField;
    qurECF_ItemITEM: TIBStringField;
    qurECF_ItemMERCADORIA: TIBStringField;
    qurECF_ItemQTD: TIBBCDField;
    qurECF_ItemVLR: TFloatField;
    qurECF_ItemTOTAL: TIBBCDField;
    qurECF_ItemICM: TIBStringField;
    qurECF_ItemVALOR_CUSTO: TFloatField;
    qurECF_ItemBICO: TIntegerField;
    qurECF_ItemCST_PIS: TIBStringField;
    qurECF_ItemCST_COFINS: TIBStringField;
    qurECF_ItemABASTECIDA: TIntegerField;
    qurECF_ItemVLR_DESCONTO: TIBBCDField;
    qurECF_ItemQTD_CAN: TIBBCDField;
    qurECF_ItemVLR_TOTTRIB: TIBBCDField;
    qurECF_ItemVLR_DESCONTO_ITEM: TIBBCDField;
    qurECF_ItemCFOP: TIntegerField;
    qurECF_ItemCST_ICM: TIBStringField;
    qurECF_ItemNCM: TIBStringField;
    qurECF_ItemUNIDADE: TIBStringField;
    qurECF_ItemINDTOT: TIBStringField;
    qurECF_ItemALI_ICM: TIBBCDField;
    qurECF_ItemVLR_BC_ICM: TIBBCDField;
    qurECF_ItemVLR_ICM: TIBBCDField;
    qurECF_ItemVLR_FRETE: TIBBCDField;
    qurECF_ItemVLR_SEG: TIBBCDField;
    qurECF_ItemVLR_OUTRO: TIBBCDField;
    qurECF_ItemVLR_BC_PIS: TIBBCDField;
    qurECF_ItemVLR_PIS: TIBBCDField;
    qurECF_ItemVLR_BC_COFINS: TIBBCDField;
    qurECF_ItemVLR_COFINS: TIBBCDField;
    qurECF_FormaPagID_ECF: TIntegerField;
    qurECF_FormaPagID: TIntegerField;
    qurECF_FormaPagTPAG: TIBStringField;
    qurECF_FormaPagVPAG: TIBBCDField;
    qurECF_FormaPagCARD_CNPJ: TIBStringField;
    qurECF_FormaPagCARD_TBAND: TIBStringField;
    qurECF_FormaPagCARD_CAUT: TIBStringField;
    qurECF_FormaPagFORMA_PAG: TIntegerField;
    qurECF_FormaPagINDICE_ECF: TIBStringField;
    qurECF_FaturaID_ECF: TIntegerField;
    qurECF_FaturaDATA: TDateField;
    qurECF_FaturaVALOR: TIBBCDField;
    qurECF_FaturaNDUP: TIBStringField;
    qurECF_FaturaPARCELA: TIntegerField;
    qurECF_XML: TIBQuery;
    dbECF_XML_Rem: TIBDataSet;
    dbECF_XML_RemID_ECF: TIntegerField;
    dbECF_XML_RemCSTAT: TSmallintField;
    dbECF_XML_RemXMOTIVO: TIBStringField;
    dbECF_XML_RemNPROT: TIBStringField;
    dbECF_XML_RemXML_RETORNO: TMemoField;
    dbECF_XML_RemNPROT_CAN: TIBStringField;
    dbECF_XML_RemXML_RETORNO_CAN: TMemoField;
    dbECF_XML_RemNPROT_INU: TIBStringField;
    dbECF_XML_RemXML_RETORNO_INU: TMemoField;
    dbECF_XML_RemNPROT_DEN: TIBStringField;
    dbECF_XML_RemXML_RETORNO_DEN: TMemoField;
    qurECF_XMLID_ECF: TIntegerField;
    qurECF_XMLCSTAT: TSmallintField;
    qurECF_XMLXMOTIVO: TIBStringField;
    qurECF_XMLNPROT: TIBStringField;
    qurECF_XMLXML_RETORNO: TMemoField;
    qurECF_XMLNPROT_CAN: TIBStringField;
    qurECF_XMLXML_RETORNO_CAN: TMemoField;
    qurECF_XMLNPROT_INU: TIBStringField;
    qurECF_XMLXML_RETORNO_INU: TMemoField;
    qurECF_XMLNPROT_DEN: TIBStringField;
    qurECF_XMLXML_RETORNO_DEN: TMemoField;
    dbECFENVIADO: TIBStringField;
    ibSqlComando_Rem: TIBSQL;
    dbTemp_NFCEEL_CHAVE: TIntegerField;
    dbECF_ItemVLR_TOTTRIB_EST: TIBBCDField;
    dbECF_ItemVLR_TOTTRIB_MUN: TIBBCDField;
    dbTemp_NFCE_ItemVLR_TOTTRIB_EST: TIBBCDField;
    dbTemp_NFCE_ItemVLR_TOTTRIB_MUN: TIBBCDField;
    dbECFVLR_TOTTRIB_EST: TIBBCDField;
    dbECFVLR_TOTTRIB_MUN: TIBBCDField;
    dbECF_RemVLR_TOTTRIB_EST: TIBBCDField;
    dbECF_RemVLR_TOTTRIB_MUN: TIBBCDField;
    dbECF_Item_RemVLR_TOTTRIB_EST: TIBBCDField;
    dbECF_Item_RemVLR_TOTTRIB_MUN: TIBBCDField;
    qurECF_ItemVLR_TOTTRIB_EST: TIBBCDField;
    qurECF_ItemVLR_TOTTRIB_MUN: TIBBCDField;
    qurECFVLR_TOTTRIB_EST: TIBBCDField;
    qurECFVLR_TOTTRIB_MUN: TIBBCDField;
    eventoAbastecida: TIBEvents;
    dbTemp_NFCE_FormaPagTPAG_DESCRICAO: TIBStringField;
    dbTemp_NFCESTATUS: TIBStringField;
    dbTemp_NFCE_FormaPagVTROCO: TIBBCDField;
    dbECF_FormaPagVTROCO: TIBBCDField;
    qurECF_FormaPagVTROCO: TIBBCDField;
    qECF_FormaPagFORMA_PAG: TIntegerField;
    qECF_FormaPagINDICE_ECF: TIBStringField;
    qECF_FormaPagVTROCO: TIBBCDField;
    qECF_ItemCOD_ANP: TIBStringField;
    dbTemp_NFCE_ItemVLR_BC_ISSQN: TIBBCDField;
    dbTemp_NFCE_ItemVLR_ISSQN: TIBBCDField;
    dbTemp_NFCE_ItemALI_ISSQN: TIBBCDField;
    dbTemp_NFCEVLR_ISSQN: TIBBCDField;
    qECF_ItemVLR_BC_ISSQN: TIBBCDField;
    qECF_ItemALI_ISSQN: TIBBCDField;
    qECF_ItemVLR_ISSQN: TIBBCDField;
    qECFVLR_ISSQN: TIBBCDField;
    qECF_ItemCLS: TIntegerField;
    dbECF_ItemVLR_BC_ISSQN: TIBBCDField;
    dbECF_ItemALI_ISSQN: TIBBCDField;
    dbECF_ItemVLR_ISSQN: TIBBCDField;
    dbECFVLR_ISSQN: TIBBCDField;
    dbECFVLR_BC_ISSQN: TIBBCDField;
    dbTemp_NFCEVLR_BC_ISSQN: TIBBCDField;
    qECFVLR_BC_ISSQN: TIBBCDField;
    dbTemp_NFCEVLR_PRODUTOS: TIBBCDField;
    dbTemp_NFCEVLR_SERVICO: TIBBCDField;
    dbTemp_NFCE_ItemVLR_SERVICO: TIBBCDField;
    dbECFVLR_PRODUTOS: TIBBCDField;
    dbECFVLR_SERVICO: TIBBCDField;
    dbECF_ItemVLR_SERVICO: TIBBCDField;
    qECFVLR_PRODUTOS: TIBBCDField;
    qECFVLR_SERVICO: TIBBCDField;
    qECF_ItemVLR_SERVICO: TIBBCDField;
    qECF_ItemM_NCM: TIBStringField;
    dbTemp_NFCe_FormaPag_Cheque: TIBDataSet;
    dbTemp_NFCe_FormaPag_ChequeCAIXA: TIBStringField;
    dbTemp_NFCe_FormaPag_ChequeID: TIntegerField;
    dbTemp_NFCe_FormaPag_ChequeBANCO: TIntegerField;
    dbTemp_NFCe_FormaPag_ChequeCONTA: TIBStringField;
    dbTemp_NFCe_FormaPag_ChequeCHEQUE: TIBStringField;
    dbTemp_NFCe_FormaPag_ChequeAGENCIA: TIBStringField;
    dbTemp_NFCe_FormaPag_ChequeCORRENTISTA: TIBStringField;
    dbTemp_NFCe_FormaPag_ChequeCNPJCPF: TIBStringField;
    dbTemp_NFCe_FormaPag_ChequeDTA_VENCIMENTO: TDateField;
    dbTemp_NFCe_FormaPag_ChequeVALOR: TIBBCDField;
    qECF_ItemCEST: TIBStringField;
    dbTemp_NFCE_ItemCEST: TIBStringField;
    dbECF_ItemCEST: TIBStringField;
    dbCh_Movimento: TIBDataSet;
    dbCh_MovimentoBANCO: TIntegerField;
    dbCh_MovimentoCONTA: TIBStringField;
    dbCh_MovimentoCHEQUE: TIBStringField;
    dbCh_MovimentoSITUACAO: TIntegerField;
    dbCh_MovimentoCLIFOR: TIntegerField;
    dbCh_MovimentoVALOR: TIBBCDField;
    dbCh_MovimentoREP_NOME: TIBStringField;
    dbCh_MovimentoREP_DATA: TDateField;
    dbCh_MovimentoCONCILIADO: TIBStringField;
    dbCh_MovimentoEMITIDO_RECEBIDO: TIBStringField;
    dbCh_MovimentoCORRENTISTA: TIBStringField;
    dbCh_MovimentoCNPJCPF: TIBStringField;
    dbCh_MovimentoDATA: TDateField;
    dbCh_MovimentoDTA_CONCILIACAO: TDateField;
    dbCh_MovimentoDTA_RECEBIMENTO: TDateField;
    dbCh_MovimentoDTA_VENCIMENTO: TDateField;
    dbCh_MovimentoSTATUS: TIBStringField;
    dbCh_MovimentoEL_CHAVE: TIntegerField;
    dbCh_MovimentoCF_NOME: TIBStringField;
    dbCh_MovimentoAGENCIA: TIBStringField;
    dbECF_San_Sup: TIBDataSet;
    dbECF_San_SupCAIXA: TIBStringField;
    dbECF_San_SupCCUSTO: TIntegerField;
    dbECF_San_SupFUNCIONARIO: TIntegerField;
    dbECF_San_SupFORMA_PAG: TIntegerField;
    dbECF_San_SupTURNO: TIBStringField;
    dbECF_San_SupOBSERVACAO: TIBStringField;
    dbECF_San_SupDATA: TDateField;
    dbECF_San_SupEL_CHAVE: TIntegerField;
    dbECF_San_SupVALOR: TIBBCDField;
    dbTemp_NFCE_ItemALI_PIS: TIBBCDField;
    dbTemp_NFCE_ItemALI_COFINS: TIBBCDField;
    dbEstMercadoriasCOFINS: TIBBCDField;
    dbEstMercadoriasPIS: TIBBCDField;
    dbECF_ItemALI_PIS: TIBBCDField;
    dbECF_ItemALI_COFINS: TIBBCDField;
    qECF_ItemALI_PIS: TIBBCDField;
    qECF_ItemALI_COFINS: TIBBCDField;
    qECF_ItemVLR_TOTTRIB_EST: TIBBCDField;
    qECF_ItemVLR_TOTTRIB_MUN: TIBBCDField;
    qurECF_ItemALI_PIS: TIBBCDField;
    qurECF_ItemALI_COFINS: TIBBCDField;
    qurECF_ItemVLR_BC_ISSQN: TIBBCDField;
    qurECF_ItemALI_ISSQN: TIBBCDField;
    qurECF_ItemVLR_ISSQN: TIBBCDField;
    qurECF_ItemVLR_SERVICO: TIBBCDField;
    qurECF_ItemCEST: TIBStringField;
    dbECF_Item_RemALI_PIS: TIBBCDField;
    dbECF_Item_RemALI_COFINS: TIBBCDField;
    dbECF_Item_RemVLR_BC_ISSQN: TIBBCDField;
    dbECF_Item_RemALI_ISSQN: TIBBCDField;
    dbECF_Item_RemVLR_ISSQN: TIBBCDField;
    dbECF_Item_RemVLR_SERVICO: TIBBCDField;
    dbECF_Item_RemCEST: TIBStringField;
    db_Temp_Est_NF: TIBDataSet;
    db_Temp_Est_NFCLIFOR: TIntegerField;
    db_Temp_Est_NFCLIFOR_IE: TIBStringField;
    db_Temp_Est_NFNUMERO: TIntegerField;
    db_Temp_Est_NFSERIE: TIBStringField;
    db_Temp_Est_NFFORMA_PAG: TIntegerField;
    db_Temp_Est_NFOPERACAO: TIntegerField;
    db_Temp_Est_NFES: TIBStringField;
    db_Temp_Est_NFESTOQUE: TIBStringField;
    db_Temp_Est_NFFINNFE: TIBStringField;
    db_Temp_Est_NFCCUSTO: TIntegerField;
    db_Temp_Est_NFFUNCIONARIO: TIntegerField;
    db_Temp_Est_NFDATA_MOV: TDateField;
    db_Temp_Est_NFDATA: TDateField;
    db_Temp_Est_NFDATAE: TDateField;
    db_Temp_Est_NFHORAE: TTimeField;
    db_Temp_Est_NFVLR_PRODUTOS: TIBBCDField;
    db_Temp_Est_NFVLR_NOTAFISCAL: TIBBCDField;
    db_Temp_Est_NFVLR_DESCONTO: TIBBCDField;
    db_Temp_Est_NFVLR_FRETE: TIBBCDField;
    db_Temp_Est_NFVLR_SEGURO: TIBBCDField;
    db_Temp_Est_NFVLR_OUTRAS: TIBBCDField;
    db_Temp_Est_NFVLR_BC_ICM: TIBBCDField;
    db_Temp_Est_NFVLR_ICM: TIBBCDField;
    db_Temp_Est_NFVLR_BC_ICMS: TIBBCDField;
    db_Temp_Est_NFVLR_ICMS: TIBBCDField;
    db_Temp_Est_NFVLR_BC_IPI: TIBBCDField;
    db_Temp_Est_NFVLR_IPI: TIBBCDField;
    db_Temp_Est_NFVLR_ISSQN: TIBBCDField;
    db_Temp_Est_NFVLR_BC_IRRF: TIBBCDField;
    db_Temp_Est_NFVLR_IRRF: TIBBCDField;
    db_Temp_Est_NFVLR_BC_PREV_RETIDO: TIBBCDField;
    db_Temp_Est_NFVLR_PREV_RETIDO: TIBBCDField;
    db_Temp_Est_NFVLR_BC_CSLL_RETIDO: TIBBCDField;
    db_Temp_Est_NFVLR_CSLL_RETIDO: TIBBCDField;
    db_Temp_Est_NFVLR_SERVICO: TIBBCDField;
    db_Temp_Est_NFVLR_BC_ISSQN: TIBBCDField;
    db_Temp_Est_NFVLR_TOTTRIB: TIBBCDField;
    db_Temp_Est_NFTR_NOME: TIBStringField;
    db_Temp_Est_NFTR_FRETE: TIBStringField;
    db_Temp_Est_NFTR_PLACA: TIBStringField;
    db_Temp_Est_NFTR_CNPJ: TIBStringField;
    db_Temp_Est_NFTR_MUNICIPIO: TIBStringField;
    db_Temp_Est_NFTR_ENDERECO: TIBStringField;
    db_Temp_Est_NFTR_UF1: TIBStringField;
    db_Temp_Est_NFTR_UF2: TIBStringField;
    db_Temp_Est_NFTR_IE: TIBStringField;
    db_Temp_Est_NFTR_ESPECIE: TIBStringField;
    db_Temp_Est_NFTR_MARCA: TIBStringField;
    db_Temp_Est_NFTR_NUMERO: TIBStringField;
    db_Temp_Est_NFTR_PESOB: TIBBCDField;
    db_Temp_Est_NFTR_PESOL: TIBBCDField;
    db_Temp_Est_NFTR_QUANTIDADE: TIBBCDField;
    db_Temp_Est_NFTR_RNTC: TIBStringField;
    db_Temp_Est_NFDA_OBSERVACAO1: TIBStringField;
    db_Temp_Est_NFDA_OBSERVACAO2: TIBStringField;
    db_Temp_Est_NFDA_OBSERVACAO3: TIBStringField;
    db_Temp_Est_NFDA_OBSERVACAO: TIBStringField;
    db_Temp_Est_NFCONTRATO: TIntegerField;
    db_Temp_Est_NFROMANEIO: TIntegerField;
    db_Temp_Est_NFORCAMENTO: TIntegerField;
    db_Temp_Est_NFRECIBO: TIntegerField;
    db_Temp_Est_NFDATA_FATURA: TDateField;
    db_Temp_Est_NFIND_EMITENTE: TIBStringField;
    db_Temp_Est_NFMODELO_DOC: TIBStringField;
    db_Temp_Est_NFCHAVE_NFE: TIBStringField;
    db_Temp_Est_NFVLR_PIS: TIBBCDField;
    db_Temp_Est_NFVLR_BC_PIS: TIBBCDField;
    db_Temp_Est_NFVLR_COFINS: TIBBCDField;
    db_Temp_Est_NFVLR_BC_COFINS: TIBBCDField;
    db_Temp_Est_NFVLR_PIS_RETIDO: TIBBCDField;
    db_Temp_Est_NFVLR_COFINS_RETIDO: TIBBCDField;
    db_Temp_Est_NFVLR_BC_ICM_SN: TIBBCDField;
    db_Temp_Est_NFVLR_ICM_SN: TIBBCDField;
    db_Temp_Est_NFVLR_BC_II: TIBBCDField;
    db_Temp_Est_NFVLR_II: TIBBCDField;
    db_Temp_Est_NFVLR_II_DESP_ADUA: TIBBCDField;
    db_Temp_Est_NFVLR_II_IOF: TIBBCDField;
    db_Temp_Est_NFVLR_IMPOSTO_TAXA: TIBBCDField;
    db_Temp_Est_NFEX_UF: TIBStringField;
    db_Temp_Est_NFEX_LOCAL_EMBARQUE: TIBStringField;
    db_Temp_Est_NFNUMERO_NFREF: TIntegerField;
    db_Temp_Est_NFSERIE_NFREF: TIBStringField;
    db_Temp_Est_NFMODELO_NFREF: TIBStringField;
    db_Temp_Est_NFCHAVE_NFEREF: TIBStringField;
    db_Temp_Est_NFFAT_NFAT: TIBStringField;
    db_Temp_Est_NFFAT_VORIG: TIBBCDField;
    db_Temp_Est_NFFAT_VDESC: TIBBCDField;
    db_Temp_Est_NFFAT_VLIQ: TIBBCDField;
    db_Temp_Est_NFCOD_SIT: TIBStringField;
    db_Temp_Est_NFDUP_VLIQ: TIBBCDField;
    db_Temp_Est_NFTP_LIGACAO: TSmallintField;
    db_Temp_Est_NFCOD_GRUPO_TENSAO: TIBStringField;
    db_Temp_Est_NFSTATUS: TIBStringField;
    db_Temp_Est_NFFUN_USUARIO: TIntegerField;
    db_Temp_Est_NFDA_INFADFISCO: TIBStringField;
    db_Temp_Est_NFCONDICAO_PAG: TIntegerField;
    db_Temp_Est_NFORDEM_CARGA: TIntegerField;
    db_Temp_Est_NFLAN_MANUAL: TIBStringField;
    db_Temp_Est_NFROYALTIES: TIBStringField;
    db_Temp_Est_NF_Item: TIBDataSet;
    db_Temp_Est_NF_ItemCLIFOR: TIntegerField;
    db_Temp_Est_NF_ItemITEM: TIntegerField;
    db_Temp_Est_NF_ItemMERCADORIA: TIBStringField;
    db_Temp_Est_NF_ItemUNIDADE: TIBStringField;
    db_Temp_Est_NF_ItemQUANTIDADE: TIBBCDField;
    db_Temp_Est_NF_ItemVLR_UNITARIO: TFMTBCDField;
    db_Temp_Est_NF_ItemVLR_DESCONTO: TIBBCDField;
    db_Temp_Est_NF_ItemVLR_TOTAL: TIBBCDField;
    db_Temp_Est_NF_ItemOBSERVACAO: TIBStringField;
    db_Temp_Est_NF_ItemRECEITA: TIntegerField;
    db_Temp_Est_NF_ItemSERVICO: TIBStringField;
    db_Temp_Est_NF_ItemALI_PREV_RETIDO: TIBBCDField;
    db_Temp_Est_NF_ItemVLR_PREV_RETIDO: TIBBCDField;
    db_Temp_Est_NF_ItemVLR_BC_PREV_RETIDO: TIBBCDField;
    db_Temp_Est_NF_ItemALI_CSLL_RETIDO: TIBBCDField;
    db_Temp_Est_NF_ItemVLR_BC_CSLL_RETIDO: TIBBCDField;
    db_Temp_Est_NF_ItemVLR_CSLL_RETIDO: TIBBCDField;
    db_Temp_Est_NF_ItemCST_IPI: TIBStringField;
    db_Temp_Est_NF_ItemALI_IPI: TIBBCDField;
    db_Temp_Est_NF_ItemVLR_IPI: TIBBCDField;
    db_Temp_Est_NF_ItemVLR_BC_IPI: TIBBCDField;
    db_Temp_Est_NF_ItemAMPARO_ICM: TIntegerField;
    db_Temp_Est_NF_ItemCST_ICM: TIBStringField;
    db_Temp_Est_NF_ItemALI_ICM: TIBBCDField;
    db_Temp_Est_NF_ItemVLR_BC_ICM: TIBBCDField;
    db_Temp_Est_NF_ItemVLR_ICM: TIBBCDField;
    db_Temp_Est_NF_ItemVLR_BC_ICMS: TIBBCDField;
    db_Temp_Est_NF_ItemVLR_ICMS: TIBBCDField;
    db_Temp_Est_NF_ItemALI_ICMS: TIBBCDField;
    db_Temp_Est_NF_ItemPRED_BC: TIBBCDField;
    db_Temp_Est_NF_ItemPRED_BC_ST: TIBBCDField;
    db_Temp_Est_NF_ItemPMVA_ST: TIBBCDField;
    db_Temp_Est_NF_ItemVLR_SERVICO: TIBBCDField;
    db_Temp_Est_NF_ItemVLR_BC_ISSQN: TIBBCDField;
    db_Temp_Est_NF_ItemALI_ISSQN: TIBBCDField;
    db_Temp_Est_NF_ItemVLR_ISSQN: TIBBCDField;
    db_Temp_Est_NF_ItemALI_IRRF: TIBBCDField;
    db_Temp_Est_NF_ItemVLR_BC_IRRF: TIBBCDField;
    db_Temp_Est_NF_ItemVLR_IRRF: TIBBCDField;
    db_Temp_Est_NF_ItemVLR_CUSTO_ATUAL: TIBBCDField;
    db_Temp_Est_NF_ItemVLR_VENDA_ATUAL: TIBBCDField;
    db_Temp_Est_NF_ItemVLR_VENDA_ATUALIZAR: TIBBCDField;
    db_Temp_Est_NF_ItemPESO_LIQ: TIBBCDField;
    db_Temp_Est_NF_ItemPESO_BRU: TIBBCDField;
    db_Temp_Est_NF_ItemID_NF_MAE: TIntegerField;
    db_Temp_Est_NF_ItemITEM_MAE: TIntegerField;
    db_Temp_Est_NF_ItemCST_PIS: TIBStringField;
    db_Temp_Est_NF_ItemVLR_BC_PIS: TIBBCDField;
    db_Temp_Est_NF_ItemVLR_PIS: TIBBCDField;
    db_Temp_Est_NF_ItemCST_COFINS: TIBStringField;
    db_Temp_Est_NF_ItemVLR_BC_COFINS: TIBBCDField;
    db_Temp_Est_NF_ItemVLR_COFINS: TIBBCDField;
    db_Temp_Est_NF_ItemVLR_PIS_RETIDO: TIBBCDField;
    db_Temp_Est_NF_ItemVLR_COFINS_RETIDO: TIBBCDField;
    db_Temp_Est_NF_ItemAMPARO_CONTRIBUICOES: TIntegerField;
    db_Temp_Est_NF_ItemVLR_BC_ICM_SN: TIBBCDField;
    db_Temp_Est_NF_ItemALI_ICM_SN: TIBBCDField;
    db_Temp_Est_NF_ItemVLR_ICM_SN: TIBBCDField;
    db_Temp_Est_NF_ItemVLR_BC_II: TIBBCDField;
    db_Temp_Est_NF_ItemALI_II: TIBBCDField;
    db_Temp_Est_NF_ItemVLR_II: TIBBCDField;
    db_Temp_Est_NF_ItemVLR_II_DESP_ADUA: TIBBCDField;
    db_Temp_Est_NF_ItemVLR_II_IOF: TIBBCDField;
    db_Temp_Est_NF_ItemVLR_TOTTRIB: TIBBCDField;
    db_Temp_Est_NF_ItemORIGEM_MER: TIBStringField;
    db_Temp_Est_NF_ItemNCM: TIBStringField;
    db_Temp_Est_NF_ItemEXTIPI: TIBStringField;
    db_Temp_Est_NF_ItemVLR_SEGURO: TIBBCDField;
    db_Temp_Est_NF_ItemVLR_OUTRAS: TIBBCDField;
    db_Temp_Est_NF_ItemVLR_FRETE: TIBBCDField;
    db_Temp_Est_NF_ItemCFOP: TIntegerField;
    db_Temp_Est_NF_ItemDI_NDI: TIBStringField;
    db_Temp_Est_NF_ItemDI_DDI: TDateField;
    db_Temp_Est_NF_ItemDI_XLOCDESEMB: TIBStringField;
    db_Temp_Est_NF_ItemDI_UFDESEMB: TIBStringField;
    db_Temp_Est_NF_ItemDI_DDESEMB: TDateField;
    db_Temp_Est_NF_ItemDI_CEXPORTADOR: TIBStringField;
    db_Temp_Est_NF_ItemADI_NADICAO: TIBStringField;
    db_Temp_Est_NF_ItemADI_NSEQADIC: TIBStringField;
    db_Temp_Est_NF_ItemADI_CFABRICANTE: TIBStringField;
    db_Temp_Est_NF_ItemADI_VDESCDI: TIBBCDField;
    db_Temp_Est_NF_ItemVLR_SACA: TIBBCDField;
    db_Temp_Est_NF_ItemID_PEDIDO: TIntegerField;
    db_Temp_Est_NF_ItemID_ROMANEIO: TIntegerField;
    db_Temp_Est_NF_ItemID_OS: TIntegerField;
    db_Temp_Est_NF_ItemAMPARO_PREVIDENCIA: TIntegerField;
    db_Temp_Est_NF_ItemNAT_REC_CONTRIBUICOES: TSmallintField;
    db_Temp_Est_NF_ItemNFCI: TIBStringField;
    db_Temp_Est_NF_ItemALI_PIS: TIBBCDField;
    db_Temp_Est_NF_ItemALI_COFINS: TIBBCDField;
    db_Temp_Est_NF_Fatura: TIBDataSet;
    db_Temp_Est_NF_FaturaCLIFOR: TIntegerField;
    db_Temp_Est_NF_FaturaDATA: TDateField;
    db_Temp_Est_NF_FaturaVALOR: TIBBCDField;
    db_Temp_Est_NF_FaturaNDUP: TIBStringField;
    db_Temp_Est_NF_FaturaPARCELA: TIntegerField;
    db_Temp_Est_NF_NFe: TIBDataSet;
    db_Temp_Est_NF_NFeCLIFOR: TIntegerField;
    db_Temp_Est_NF_NFeITEM: TIntegerField;
    db_Temp_Est_NF_NFeCHAVE_NFEREF: TIBStringField;
    db_Temp_Est_NF_NFeID_ECF: TIntegerField;
    dsCcusto: TDataSource;
    qrCcusto: TIBQuery;
    qrCcustoID: TIntegerField;
    qrCcustoDESCRICAO: TIBStringField;
    qrCcustoLOCAL: TIntegerField;
    qrCcustoESTOQUE_ZERO: TIBStringField;
    qrCcustoDISTRIBUIDOR_BAYER: TIBStringField;
    qrCcustoESTOQUE_CONTROLA: TIBStringField;
    qrCcustoATIVO: TIBStringField;
    dbTef_Movimento: TIBDataSet;
    dbTef_MovimentoID: TIntegerField;
    dbTef_MovimentoEL_CHAVE: TIntegerField;
    dbTef_MovimentoDOCFISCAL: TIBStringField;
    dbTef_MovimentoVALOR: TIBBCDField;
    dbTef_MovimentoMOEDA: TIBStringField;
    dbTef_MovimentoENTIDADECLIENTE: TIBStringField;
    dbTef_MovimentoIDENTIFICADORCLIENTE: TIBStringField;
    dbTef_MovimentoSTATUS: TIBStringField;
    dbTef_MovimentoNSU: TIBStringField;
    dbTef_MovimentoCAUTORIZACAO: TIBStringField;
    dbTef_MovimentoDTCOMPRA: TDateField;
    dbTef_MovimentoHRCOMPRA: TTimeField;
    dbTef_MovimentoCCONTROLE: TIBStringField;
    dbTef_MovimentoMTEF: TBlobField;
    dbTef_MovimentoMSGOPERADOR: TIBStringField;
    dbTef_MovimentoEMISSOR: TIBStringField;
    dbTef_MovimentoMCUPOMREDUZIDO: TBlobField;
    dbTef_MovimentoMCLIENTE: TBlobField;
    dbTef_MovimentoMESTABELECIMENTO: TBlobField;
    dbTef_MovimentoNTERMINAL: TIBStringField;
    dbTef_MovimentoCESTABELECIMENTO: TIBStringField;
    dbTef_MovimentoSTATUSCONFIRMACAO: TIntegerField;
    dbTef_MovimentoOPERACAO: TIntegerField;
    dbTef_MovimentoTIPOCARTAO: TIntegerField;
    dbTef_MovimentoTIPOFINACIAMENTO: TIntegerField;
    dbTef_MovimentoVERSAOINTERFACE: TIntegerField;
    dbTef_MovimentoNOMEAUTOMACAO: TIBStringField;
    dbTef_MovimentoVERSAOAUTOMACAO: TIBStringField;
    dbTef_MovimentoVIASCOMPROVANTE: TIntegerField;
    dbTef_MovimentoREGISTROCERTIFICACAO: TIBStringField;
    dbTef_MovimentoINDICEREDE: TIBStringField;
    dbTef_MovimentoNCARTAO: TIBStringField;
    dbTef_MovimentoNOMECLIENTE: TIBStringField;
    dbTef_MovimentoFORMA_PAG: TIntegerField;
    dbTef_MovimentoPARCELAS: TIntegerField;
    transactionLicenca: TIBTransaction;
    qurLicenca: TIBQuery;
    databaseLicenca: TIBDatabase;
    dbEstMercadoriasDESCRICAO: TIBStringField;
    qConsulta_Ext_CliDESCRICAO: TIBStringField;
    qurEstMercadorias_RemotaDESCRICAO: TIBStringField;
    dbTemp_NFCe_FormaPag_CFrete: TIBDataSet;
    dbTemp_NFCe_FormaPag_CFreteCAIXA: TIBStringField;
    dbTemp_NFCe_FormaPag_CFreteID: TIntegerField;
    dbTemp_NFCe_FormaPag_CFreteCLIFOR: TIntegerField;
    dbTemp_NFCe_FormaPag_CFreteNUMERO: TIntegerField;
    dbTemp_NFCe_FormaPag_CFreteDATA_VEN: TDateField;
    dbTemp_NFCe_FormaPag_CFreteVALOR: TIBBCDField;
    dbTemp_NFCe_FormaPag_CFreteNOME: TIBStringField;
    dbTemp_NFCe_FormaPag_CCorrente: TIBDataSet;
    dbTemp_NFCe_FormaPag_CCorrenteCAIXA: TIBStringField;
    dbTemp_NFCe_FormaPag_CCorrenteID: TIntegerField;
    dbTemp_NFCe_FormaPag_CCorrenteID_CREDITO: TIntegerField;
    dbTemp_NFCe_FormaPag_CCorrenteCLIFOR: TIntegerField;
    dbTemp_NFCe_FormaPag_CCorrenteVALOR: TIBBCDField;
    dbTemp_NFCe_FormaPag_CCorrenteNOME: TIBStringField;
    dbCR_CartaFrete: TIBDataSet;
    dbCR_CartaFreteID: TIntegerField;
    dbCR_CartaFreteCLIFOR: TIntegerField;
    dbCR_CartaFreteNUMERO: TIntegerField;
    dbCR_CartaFreteDATA_OPE: TDateField;
    dbCR_CartaFreteDATA_VEN: TDateField;
    dbCR_CartaFreteVALOR: TIBBCDField;
    dbCR_CartaFreteFUNCIONARIO: TIntegerField;
    dbCR_CartaFreteEL_CHAVE: TIntegerField;
    dbTemp_NFCECONVENIO: TIntegerField;
    dbECFCONVENIO: TIntegerField;
    dbCR_ContaCorrente: TIBDataSet;
    dbCR_ContaCorrenteID: TIntegerField;
    dbCR_ContaCorrenteCLIFOR: TIntegerField;
    dbCR_ContaCorrenteDC: TIBStringField;
    dbCR_ContaCorrenteOBSERVACAO: TIBStringField;
    dbCR_ContaCorrenteVALOR: TIBBCDField;
    dbCR_ContaCorrenteEL_CHAVE: TIntegerField;
    dbCR_ContaCorrenteID_CREDITO: TIntegerField;
    dbCR_ContaCorrenteFUNCIONARIO: TIntegerField;
    dbCR_ContaCorrenteDATA_OPE: TDateField;
    dbCR_CartaFreteFORMA_PAG: TIntegerField;
    dbCR_CartaFreteCCUSTO: TIntegerField;
    dbTemp_NFCe_FormaPag_CFreteFORMA_PAG: TIntegerField;
    dbTemp_NFCE_ItemM_DESCRICAO: TIBStringField;
    qECF_ItemXPROD: TIBStringField;
    dbTemp_NFCE_FormaPagCR_CP_V: TIBStringField;
    dbAbastecidaPLACA: TIBStringField;
    dbTemp_NFCE_ItemSUBTIPO_ITEM: TIBStringField;
    dbTef_MovimentoREDE: TIBStringField;
    qECF_ItemM_CEST: TIBStringField;
    dbTemp_NFCE_ItemID_COMANDA: TIBStringField;
    dbECF_ItemID_COMANDA: TIBStringField;
    dbBarComandas: TIBDataSet;
    dbBarComandasID: TIBStringField;
    dbBarComandasDATA_HORA_AB: TDateTimeField;
    dbBarComandasDATA_HORA_FE: TDateTimeField;
    dbBarComandasVLR_TOTAL: TIBBCDField;
    dbBarComandasEL_CHAVE: TIntegerField;
    dbBarComandasOBSERVACAO: TIBStringField;
    dbBarComandasItens: TIBDataSet;
    dbBarComandasItensID: TIBStringField;
    dbBarComandasItensITEM: TIBStringField;
    dbBarComandasItensMERCADORIA: TIBStringField;
    dbBarComandasItensQUANTIDADE: TIBBCDField;
    dbBarComandasItensVLR_UNITARIO: TIBBCDField;
    dbBarComandasItensVLR_TOTAL: TIBBCDField;
    dbBarComandasID_FUNCIONARIO: TIntegerField;
    dbBarComandasItensID_FUNCIONARIO: TIntegerField;
    qConsulta_CRSDOC: TIBStringField;
    dbCRSDOC: TIBStringField;
    dbTemp_NFCE_ItemCOMANDA_ITEM: TIBStringField;
    dbBarComandasItensPAGO: TIBStringField;
    qECFVLR_TOTTRIB_EST: TIBBCDField;
    qECFVLR_TOTTRIB_MUN: TIBBCDField;
    dbECF_ItemAMPARO_ICM: TIntegerField;
    dbECF_ItemPRED_BC: TIBBCDField;
    dbECF_ItemMOTDESICM: TIBStringField;
    dbECF_ItemVLR_ICMDESON: TIBBCDField;
    dbTemp_NFCE_ItemAMPARO_ICM: TIntegerField;
    dbTemp_NFCE_ItemPRED_BC: TIBBCDField;
    dbTemp_NFCE_ItemMOTDESICM: TIBStringField;
    dbTemp_NFCE_ItemVLR_ICMDESON: TIBBCDField;
    dbTemp_NFCEVLR_COMISSAO: TIBBCDField;
    dbTemp_NFCEPER_COMISSAO: TIBBCDField;
    dbTemp_NFCE_ItemVLR_COMISSAO: TIBBCDField;
    dbTemp_NFCE_ItemPER_COMISSAO: TIBBCDField;
    dbECFVLR_COMISSAO: TIBBCDField;
    dbECFPER_COMISSAO: TIBBCDField;
    dbECF_ItemVLR_COMISSAO: TIBBCDField;
    dbECF_ItemPER_COMISSAO: TIBBCDField;
    dbTemp_NFCE_ItemKIT: TIBStringField;
    dbTemp_NFCECF_MOTORISTA: TIBStringField;
    qECFCF_MOTORISTA: TIBStringField;
    dbECFCF_MOTORISTA: TIBStringField;
    dbTemp_NFCE_ItemANP_PGLP: TIBBCDField;
    dbTemp_NFCE_ItemANP_PGNN: TIBBCDField;
    dbTemp_NFCE_ItemANP_PGNI: TIBBCDField;
    dbTemp_NFCE_ItemANP_VPART: TIBBCDField;
    dbTemp_NFCE_ItemANP_CODIF: TIBStringField;
    dbECF_ItemANP_PGLP: TIBBCDField;
    dbECF_ItemANP_PGNN: TIBBCDField;
    dbECF_ItemANP_PGNI: TIBBCDField;
    dbECF_ItemANP_VPART: TIBBCDField;
    dbECF_ItemANP_CODIF: TIBStringField;
    procedure DataModuleCreate(Sender: TObject);
    procedure eventoAbastecidaEventAlert(Sender: TObject;
      EventName: String; EventCount: Integer; var CancelAlerts: Boolean);
    procedure dbTemp_NFCE_ItemAfterPost(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
   fStatusLicenca: String;
   fID: Integer;
   Valor:Currency;
   Total:Currency;
   Flag_Cliente: Boolean;
   procedure getTemp_NFCE();
   procedure getTemp_NFCE_FormaPag();
   procedure getTemp_NFCE_Cheque();
   procedure getTemp_NFCE_CCorrente();
   procedure getTemp_NFCE_CFrete();
   procedure getTemp_NFCE_Fatura();
   procedure getTef_Movimento();
   procedure getMunicipios(uf:String);
   procedure LimparDadosCliente();
   procedure AbrirCcustos;
   procedure AcionarPeriferico(Caminho: String; Comando: AnsiString);
   function getDescOperacao(pID: Integer): string;
   function getDescFormaPag(pID: Integer): string;
   function getDadosCliente(pID: Integer): String;
   Function getMunicipioLocal(pCCusto: Integer): String;
   function GetTemp_Valor_Crediario: Currency;
   function VerificarNFCePossuiItem(pCaixa: Integer): Boolean;
   function getDiasPrazo(pClifor: integer): Integer;
   function getSerieNFe: String;
   function getTefTransFormaPag(pForma_Pag: Integer): Boolean;
   function ExisteComprovanteTef(pEL_Chave: Integer): Boolean;
   function TefGetValorFormaPag(pFPag, pEL_Chave: Integer): Currency;
   function ClientePossuiPrecoEsp(pMer: String) : Boolean;
   function ExistePagamentoPrazo(pCaixa:Integer):Boolean;
   function PermiteEstoqueNegativoCCusto(ACCusto: Integer):Boolean;
   function GetSubTipo_Item(ACaixa: Integer):Boolean;
   function GetDescontoPedido(AID: Integer): Currency;
   Function VerificaComandaPaga(pID: String): Boolean;
   procedure MoverComandasInseridas(pID: String; pEL_Chave: Integer);
   procedure ConfirmarPagamentoItemComanda(pID, pItem, pPago: String);
   procedure DeletarComandasPagas(pID: String);
   Function TefEstaAtivo(): Boolean;
   Function TefGetDocVinc(pNumero, pCcusto: Integer; pSerie: String): Integer;
   Function TefGetImpressora(): String;
  end;

var
  dmCupomFiscal: TdmCupomFiscal;
  caminhoExe:String;
  caminhoLog:String;

implementation

uses uFrmGerarParcelas, uFrmFim, uFrmCheque, uFrmLoja, uFuncoes, uFrmPosto,
  uFrmPrincipal, uRotinasGlobais, uDMComponentes, ufrmNfce;

{$R *.dfm}
{$DEFINE Use_Stream}
procedure TdmCupomFiscal.AbrirCcustos;
begin
  qrCcusto.Active := false;
  qrCcusto.Active := True;
  qrCcusto.Last;
  qrCcusto.First;
end;
// parametro FRENTE_CAIXA.GAV_Comando � obsoleto devido a n�o funcionar os
// comandos se est�o no formato string
procedure TdmCupomFiscal.AcionarPeriferico(Caminho: String; Comando: AnsiString);
var
	F : TextFile;
  s : string;
  ultima_posicao : integer;
  proxima_posicao : integer;
  aux : string;
begin
	AssignFile(F,Caminho);//Comunica��o com a impressora, pode ser porta ou compartilhamento
  Rewrite(F);
  ultima_posicao := 1;
  aux := comando;
  s := '';
  repeat
    aux := Copy(aux,ultima_posicao);
    proxima_posicao := Pos('+',aux);

    if proxima_posicao > 0 then
      s := s + char(strtoint(Copy(aux,2,proxima_posicao-2)))
    else
      s := s +char(strtoint(Copy(aux,2)));
    ultima_posicao := proxima_posicao+1;
  until proxima_posicao=0;

  //s:=char(027)+char(112)+char(000)+char(100)+'';
	writeln(F,s+''{/*Comando*/});
	CloseFile(F);
end;

function TdmCupomFiscal.ClientePossuiPrecoEsp(pMer: String): Boolean;
var
  qrBusca: TIBQuery;
  TPreco: Integer;
begin
 Result := True;

 if (dbTemp_NFCECLIFOR.Value>0) then
 begin
  try
    qrBusca := TIBQuery.Create(Self);
    qrBusca.Active := False;
    qrBusca.Database := DataBase;

{Lauro 23.01.17
    //Verifica se cliente possui tabela de preco
    qrBusca.SQL.Clear;
    qrBusca.SQL.Add('Select c.TPreco FROM CliFor c Where c.ID=:pID');
    qrBusca.ParamByName('pID').AsInteger := dbTemp_NFCECLIFOR.Value;
    qrBusca.Active := True;
    if not(qrBusca.IsEmpty) Then
      Result := False;
    else begin
      if(pMer <> '')then //Mercadoria especifica
      begin
        TPreco := qrBusca.FieldByName('TPreco').AsInteger;
        qrBusca.SQL.Clear;
        qrBusca.SQL.Add('Select Vlr_Venda From Est_Mercadorias_Preco WHERE (Mercadoria=:pMer) and (TPreco=:pTP)');
        qrBusca.ParamByName('pTP').AsInteger := TPreco;
        qrBusca.ParamByName('pMer').AsString := pMer;
        qrBusca.Active := True;
        if(qrBusca.IsEmpty)then
          Result := false;
      end
      else begin //Todas as no temp
        TPreco := qrBusca.FieldByName('TPreco').AsInteger;
        qrBusca.SQL.Clear;
        qrBusca.SQL.Add('Select mp.* from est_mercadorias_preco mp '+
                        'left outer join temp_nfce_item t on t.mercadoria = mp.mercadoria '+
                        'where  (TPreco=:pTP)');
        qrBusca.ParamByName('pTP').AsInteger := TPreco;
        qrBusca.Active := True;
        if(qrBusca.IsEmpty)then
          Result := false;
      end;
    end;
}
    //Lauro 23.01.17 - Preco diferenciado Est_Mercadorias_Preco
    qrBusca.Active:=False;
    qrBusca.SQL.Clear;
    qrBusca.SQL.Add('Select Count(MP.tpreco) as Flag'+
                    ' from temp_nfce n'+
                    ' left outer join temp_nfce_item i on i.caixa = n.caixa'+
                    ' left outer join clifor CF on CF.ID = n.CliFor'+
                    ' left outer join est_mercadorias_preco MP on MP.mercadoria = i.mercadoria and MP.tpreco=CF.tpreco'+
                    ' where (n.Caixa=:pCaixa)');
    qrBusca.ParamByName('pCaixa').AsString := dbTemp_NFCECAIXA.Value;
    qrBusca.Active := True;
    qrBusca.First;
    if (qrBusca.FieldByName('Flag').AsInteger>0) then
      Result := false;

    //Lauro 23.01.17 - Preco diferenciado Est_mercadorias_CliFor
    qrBusca.Active:=False;
    qrBusca.SQL.Clear;
    qrBusca.SQL.Add('Select Count(MC.mercadoria) as Flag'+
                    ' from temp_nfce n'+
                    ' left outer join temp_nfce_item i on i.Caixa = n.Caixa'+
                    ' left outer join est_mercadorias_CliFor MC on MC.mercadoria = i.mercadoria and MC.clifor=n.clifor'+
                    ' where (n.Caixa=:pCaixa)');
    qrBusca.ParamByName('pCaixa').AsString := dbTemp_NFCECAIXA.Value;
    qrBusca.Active := True;
    qrBusca.First;
    if (qrBusca.FieldByName('Flag').AsInteger>0) then
      Result := false;

    //lauro 23.01.17 Pesquisa se tem forma de pagamento a prazo
    If dmCupomFiscal.ExistePagamentoPrazo(FRENTE_CAIXA.Caixa) then
      Result := false;

  finally
    qrBusca.Free;
  end;
 end;
end;

procedure TdmCupomFiscal.ConfirmarPagamentoItemComanda(pID, pItem, pPago: String);
var
  qrUpdate: TIBQuery;
begin
  qrUpdate := TIBQuery.Create(Self);
  try
    qrUpdate.Database := DataBase;
    qrUpdate.SQL.Clear;
    qrUpdate.SQL.Add('Update Temp_Bar_Comandas_item set PAGO = '+QuotedStr(pPago)+' where ID = :pID and ITEM = :pItem');
    qrUpdate.ParamByName('pID').AsString := pID;
    qrUpdate.ParamByName('pItem').AsString := pItem;
    qrUpdate.ExecSQL;
    Transaction.CommitRetaining;
    qrUpdate.Close;
  finally
    qrUpdate.Free;
  end;
end;

function TdmCupomFiscal.PermiteEstoqueNegativoCCusto(ACCusto: Integer): Boolean;
var
  qrBusca: TIBQuery;
begin
  qrBusca := TIBQuery.Create(Self);
  try
    qrBusca.Active := False;
    qrBusca.Database := DataBase;
    qrBusca.SQL.Clear;
    qrBusca.SQL.Add('Select ESTOQUE_ZERO from CCUSTOS where ID = :pID');
    qrBusca.ParamByName('pID').AsInteger := ACCusto;
    qrBusca.Active := True;
    if(qrBusca.fieldByName('ESTOQUE_ZERO').asString = 'S')then
      Result := False
    else
      Result := True;
  finally
    qrBusca.Free;
  end;
end;

procedure TdmCupomFiscal.DataModuleCreate(Sender: TObject);
begin
  caminhoExe:=ExtractFileDir(Application.ExeName);
  caminhoLog:=caminhoExe+'\Log.txt';
  //lauro 23.01.17 Flag_Cliente := False;
end;

procedure TdmCupomFiscal.dbTemp_NFCE_ItemAfterPost(DataSet: TDataSet);
begin
  //lauro 23.01.17  Flag_Cliente := (dbTemp_NFCECLIFOR.Value > 0) and  (ClientePossuiPrecoEsp(''));
  dmCupomFiscal.Flag_Cliente := dmCupomFiscal.ClientePossuiPrecoEsp('');
end;

procedure TdmCupomFiscal.DeletarComandasPagas(pID: String);
var
  qryDelete: TIBQuery;
begin
  qryDelete := TIBQuery.Create(Self);
  try
    qryDelete.Database := DataBase;
    qryDelete.SQL.Clear;
    qryDelete.SQL.Add('Delete from TEMP_BAR_COMANDAS where ID = '+QuotedStr(pID));
    qryDelete.ExecSQL;
    Transaction.CommitRetaining;
    qryDelete.Close;
  finally
    qryDelete.Free;
  end;
end;

procedure TdmCupomFiscal.eventoAbastecidaEventAlert(Sender: TObject;
  EventName: String; EventCount: Integer; var CancelAlerts: Boolean);
begin
  if frmPosto <> nil then
  begin
    if POSTO.Eventos_Venda then
    begin
      frmPosto.btnLerAbastecidas.Click;
    end;
  end;
end;

function TdmCupomFiscal.ExisteComprovanteTef(pEL_Chave: Integer): Boolean;
var
  qrBusca: TIBQuery;
begin
  qrBusca := TIBQuery.Create(Self);
  try
    qrBusca.Active := False;
    qrBusca.Database := DataBase;
    qrBusca.SQL.Clear;
    qrBusca.SQL.Add('Select * from TEF_MOVIMENTO '+
                    ' Where EL_CHAVE = :pEL_Chave and STATUS_TRANSACAO = ''X''');
    qrBusca.ParamByName('pEL_Chave').AsInteger := pEL_Chave;
    qrBusca.Active := True;
    if(trim(qrBusca.FieldByName('CAUTORIZACAO').asString)<>'') or
      (trim(qrBusca.FieldByName('NSU').asString)<>'')Then
      Result := True
    else
      Result := False;
  finally
    qrBusca.Free;
  end;
end;

//Usada para verificar se permite ou n�o alterar o cliente
function TdmCupomFiscal.ExistePagamentoPrazo(pCaixa: Integer): Boolean;
var
  qrBusca: TIBQuery;
begin
  Result := True;

  qrBusca := TIBQuery.Create(Self);
  try
    qrBusca.Active := False;
    qrBusca.Database := DataBase;
    qrBusca.SQL.Clear;
    qrBusca.SQL.Add('Select t.* From Temp_NFCe_FormaPag t '+
                    'left outer join Formas_Pagamento f on f.ID = t.Forma_Pag '+
                    'where t.VPag > 0 and t.Caixa = :pCaixa '+
                    'and f.CR_CP_V = ''R''');
    qrBusca.ParamByName('pCaixa').AsString :=FormatFloat('0000', FRENTE_CAIXA.caixa);
    qrBusca.Active := True;
    Result := not qrBusca.IsEmpty;
  finally
    qrBusca.Free;
  end;
end;

function TdmCupomFiscal.getDadosCliente(pID: Integer): String;
var
  qrBusca: TIBQuery;
begin
  qrBusca := TIBQuery.Create(Self);
  try
    qrBusca.Active := False;
    qrBusca.Database := DataBase;
    qrBusca.SQL.Clear;
    qrBusca.SQL.Add('Select Recado, UF from CLIFOR where ID = :pID');
    qrBusca.ParamByName('pID').AsInteger := pID;
    qrBusca.Active := True;
    Result := qrBusca.FieldByName('UF').AsString+qrBusca.FieldByName('RECADO').AsString;
  finally
    qrBusca.Free;
  end;
end;

function TdmCupomFiscal.getDescFormaPag(pID: Integer): string;
var
  qrBusca: TIBQuery;
begin
  qrBusca := TIBQuery.Create(Self);
  try
    qrBusca.Active := False;
    qrBusca.Database := DataBase;
    qrBusca.SQL.Clear;
    qrBusca.SQL.Add('select Descricao from Formas_Pagamento where id = :pID');
    qrBusca.ParamByName('pID').AsInteger := pID;
    qrBusca.Active := True;
    if not(qrBusca.FieldByName('Descricao').IsNull)then
      Result := qrBusca.FieldByName('Descricao').AsString
    else
      Result := 'Forma de pagamento n�o cadastrada...';
  finally
    qrBusca.Free;
  end;
end;

function TdmCupomFiscal.GetDescontoPedido(AID: Integer): Currency;
var
  qrBusca: TIBQuery;
begin
  qrBusca := TIBQuery.Create(Self);
  try
    qrBusca.Active := False;
    qrBusca.Database := DataBase;
    qrBusca.SQL.Clear;
    qrBusca.SQL.Add('select Vlr_Desconto from Est_Pedidos where id = :pID');
    qrBusca.ParamByName('pID').AsInteger := AID;
    qrBusca.Active := True;
    Result := qrBusca.FieldByName('Vlr_Desconto').AsCurrency
  finally
    qrBusca.Free;
  end;
end;

function TdmCupomFiscal.getDescOperacao(pID: Integer): string;
var
  qrBusca: TIBQuery;
begin
  qrBusca := TIBQuery.Create(Self);
  try
    qrBusca.Active := False;
    qrBusca.Database := DataBase;
    qrBusca.SQL.Clear;
    qrBusca.SQL.Add('select Descricao from Operacoes where id = :pID');
    qrBusca.ParamByName('pID').AsInteger := pID;
    qrBusca.Active := True;
    if not(qrBusca.FieldByName('Descricao').IsNull)then
      Result := qrBusca.FieldByName('Descricao').AsString
    else
      Result := 'Operac�o n�o cadastrada...';
  finally
    qrBusca.Free;
  end;
end;

function TdmCupomFiscal.getDiasPrazo(pClifor: integer): Integer;
var
  qrBusca: TIBQuery;
begin
  qrBusca := TIBQuery.Create(Self);
  try
    qrBusca.Active := False;
    qrBusca.Database := DataBase;
    qrBusca.SQL.Clear;
    qrBusca.SQL.Add('select c.DIASPRAZO from Clifor c where c.id=:pCCusto');
    qrBusca.ParamByName('pCCusto').AsInteger := pClifor;
    qrBusca.Active := True;
    Result := qrBusca.FieldByName('DIASPRAZO').AsInteger;
  finally
    qrBusca.Free;
  end;
end;

Function TdmCupomFiscal.getMunicipioLocal(pCCusto: Integer): String;
var
  qrBusca: TIBQuery;
begin
  qrBusca := TIBQuery.Create(Self);
  try
    qrBusca.Active := False;
    qrBusca.Database := DataBase;
    qrBusca.SQL.Clear;
    qrBusca.SQL.Add('select l.Municipio, l.UF from locais l'+
                    ' left outer join ccustos c on c.local=l.id'+
                    ' where c.id=:pCCusto');
    qrBusca.ParamByName('pCCusto').AsInteger := pCCusto;
    qrBusca.Active := True;
    Result := qrBusca.FieldByName('UF').AsString+qrBusca.FieldByName('Municipio').AsString;
  finally
    qrBusca.Free;
  end;
end;

procedure TdmCupomFiscal.getMunicipios(uf: String);
begin
  if Trim(uf)<>'' then
  begin
    qMunicipio.Active:=False;
    qMunicipio.SQL.Clear;
    qMunicipio.SQL.Add('select * from MUNICIPIOS where UF=:pUF order by DESCRICAO');
    qMunicipio.ParamByName('pUF').Value:=uf;
    qMunicipio.Active:=True;
    qMunicipio.Last;
    qMunicipio.First;
  end
  else
  begin
    qMunicipio.Active:=False;
    qMunicipio.SQL.Clear;
    qMunicipio.SQL.Add('select * from MUNICIPIOS order by DESCRICAO');
    qMunicipio.Active:=True;
    qMunicipio.Last;
    qMunicipio.First;
  end;
end;

function TdmCupomFiscal.getSerieNFe: String;
begin
  DMCupomFiscal.dbQuery2.Active:=False;
  DMCupomFiscal.dbQuery2.SQL.Clear;
  DMCupomFiscal.dbQuery2.SQL.Add('Select coalesce(l.nfe_serie_padrao, 0) nfe_serie_padrao from locais_config_nfe l'+
                                 ' left outer join ccustos c on c.local=l.local'+
                                 ' where c.id=:pCCusto');
  DMCupomFiscal.dbQuery2.ParamByName('pCcusto').Value := CCUSTO.codigo;
  DMCupomFiscal.dbQuery2.Active:=True;
  DMCupomFiscal.dbQuery2.First;
  Result:=DMCupomFiscal.dbQuery2.FieldByName('nfe_serie_padrao').AsString;
end;

function TdmCupomFiscal.GetSubTipo_Item(ACaixa: Integer): Boolean;
var
  qrBusca: TIBQuery;
begin
  qrBusca := TIBQuery.Create(Self);
  try
    qrBusca.Active := False;
    qrBusca.Database := DataBase;
    qrBusca.SQL.Clear;
    qrBusca.SQL.Add('select m.SUBTIPO_ITEM from Est_Mercadorias m '+
                    'left outer join TEMP_NFCE_ITEM i on i.Mercadoria = m.ID '+
                    'where i.Caixa=:pCaixa and m.SubTipo_Item <> ''CO''');
    qrBusca.ParamByName('pCaixa').AsString :=FormatFloat('0000', FRENTE_CAIXA.caixa);;
    qrBusca.Active := True;
    Result := not(qrBusca.IsEmpty);
  finally
    qrBusca.Free;
  end;
end;

function TdmCupomFiscal.getTefTransFormaPag(pForma_Pag: Integer): Boolean;
begin
  dmCupomFiscal.dbQuery3.Active := False;
  dmCupomFiscal.dbQuery3.SQL.Clear;
  dmCupomFiscal.dbQuery3.SQL.add('Select * from TEF_Movimento where FORMA_PAG  = :pID');
  dmCupomFiscal.dbQuery3.ParamByName('pID').AsInteger := pForma_Pag;
  dmCupomFiscal.dbQuery3.Active := True;
  Result := not(dmCupomFiscal.dbQuery3.IsEmpty);
end;

procedure TdmCupomFiscal.getTef_Movimento;
begin
  dbTef_Movimento.Close;
  dbTef_Movimento.ParamByName('pChave').AsInteger := dbTemp_NFCEEL_CHAVE.AsInteger;
  dbTef_Movimento.Open;
end;

procedure TdmCupomFiscal.getTemp_NFCE;
begin
  dbTemp_NFCE.Close;
  dbTemp_NFCE.ParamByName('pCaixa').AsString:=FormatFloat('0000', FRENTE_CAIXA.Caixa);
  dbTemp_NFCE.Open;
  dbTemp_NFCE.First;
end;

procedure TdmCupomFiscal.getTemp_NFCE_Cheque;
begin
  dmCupomFiscal.dbTemp_NFCe_FormaPag_Cheque.Close;
  dmCupomFiscal.dbTemp_NFCe_FormaPag_Cheque.ParamByName('pCaixa').Value := FormatFloat('0000', FRENTE_CAIXA.Caixa);
  dmCupomFiscal.dbTemp_NFCe_FormaPag_Cheque.Open;
  dmCupomFiscal.dbTemp_NFCe_FormaPag_Cheque.First;
end;

procedure TdmCupomFiscal.getTemp_NFCE_CCorrente;
begin
  dmCupomFiscal.dbTemp_NFCe_FormaPag_CCorrente.Close;
  dmCupomFiscal.dbTemp_NFCe_FormaPag_CCorrente.ParamByName('pCaixa').Value := FormatFloat('0000', FRENTE_CAIXA.Caixa);
  dmCupomFiscal.dbTemp_NFCe_FormaPag_CCorrente.Open;
  dmCupomFiscal.dbTemp_NFCe_FormaPag_CCorrente.First;
end;

procedure TdmCupomFiscal.getTemp_NFCE_CFrete;
begin
  dmCupomFiscal.dbTemp_NFCe_FormaPag_CFrete.Close;
  dmCupomFiscal.dbTemp_NFCe_FormaPag_CFrete.ParamByName('pCaixa').Value := FormatFloat('0000', FRENTE_CAIXA.Caixa);
  dmCupomFiscal.dbTemp_NFCe_FormaPag_CFrete.Open;
  dmCupomFiscal.dbTemp_NFCe_FormaPag_CFrete.First;
end;

procedure TdmCupomFiscal.getTemp_NFCE_Fatura;
begin
  dmCupomFiscal.dbTemp_NFCE_Fatura.Close;
  dmCupomFiscal.dbTemp_NFCE_Fatura.ParamByName('pCaixa').Value := FormatFloat('0000', FRENTE_CAIXA.Caixa);
  dmCupomFiscal.dbTemp_NFCE_Fatura.Open;
  dmCupomFiscal.dbTemp_NFCE_Fatura.First;
end;

procedure TdmCupomFiscal.getTemp_NFCE_FormaPag;
begin
  if(dbTemp_NFCECLIFOR.Value <=0)Then
  begin
    dmCupomFiscal.dbTemp_NFCE_FormaPag.Close;
    dmCupomFiscal.dbTemp_NFCE_FormaPag.SelectSQL.Clear;
    dmCupomFiscal.dbTemp_NFCE_FormaPag.SelectSQL.Add('select f.*, f.forma_pag||''-''||fp.descricao as tpag_descricao, fp.cr_Cp_v from temp_nfce_formapag f '+
                  'left outer join formas_pagamento fp on fp.id=f.forma_pag '+
                  'where f.CAIXA=:pCaixa '+
                  'order by f.Forma_Pag');
    dmCupomFiscal.dbTemp_NFCE_FormaPag.ParamByName('pCaixa').AsString:=FormatFloat('0000', FRENTE_CAIXA.Caixa);
    dmCupomFiscal.dbTemp_NFCE_FormaPag.Open;
    dmCupomFiscal.dbTemp_NFCE_FormaPag.First;
  end
  else
  begin
    dmCupomFiscal.dbTemp_NFCE_FormaPag.Close;
    dmCupomFiscal.dbTemp_NFCE_FormaPag.SelectSQL.Clear;
    dmCupomFiscal.dbTemp_NFCE_FormaPag.SelectSQL.Add('select f.*, f.forma_pag||''-''||fp.descricao as tpag_descricao, fp.cr_Cp_v from temp_nfce_formapag f '+
                  'left outer join formas_pagamento fp on fp.id=f.forma_pag '+
                  'left outer join clifor_formas_pagamento c on c.forma_pag = f.forma_pag '+
                  'where f.CAIXA=:pCaixa and c.Clifor = :pClifor '+
                  'order by f.Forma_Pag');
    dmCupomFiscal.dbTemp_NFCE_FormaPag.ParamByName('pCaixa').AsString:=FormatFloat('0000', FRENTE_CAIXA.Caixa);
    dmCupomFiscal.dbTemp_NFCE_FormaPag.ParamByName('pClifor').AsInteger:=dbTemp_NFCECLIFOR.AsInteger;
    dmCupomFiscal.dbTemp_NFCE_FormaPag.Open;
    if(dbTemp_NFCE_FormaPag.IsEmpty)then
    begin
      dmCupomFiscal.dbTemp_NFCE_FormaPag.Close;
      dmCupomFiscal.dbTemp_NFCE_FormaPag.SelectSQL.Clear;
      dmCupomFiscal.dbTemp_NFCE_FormaPag.SelectSQL.Add('select f.*, f.forma_pag||''-''||fp.descricao as tpag_descricao, fp.cr_Cp_v from temp_nfce_formapag f '+
                    'left outer join formas_pagamento fp on fp.id=f.forma_pag '+
                    'where f.CAIXA=:pCaixa '+
                    'order by f.Forma_Pag');
      dmCupomFiscal.dbTemp_NFCE_FormaPag.ParamByName('pCaixa').AsString:=FormatFloat('0000', FRENTE_CAIXA.Caixa);
      dmCupomFiscal.dbTemp_NFCE_FormaPag.Open;
    end
    else
    begin
      //Se existem formas de pagamento vinculadas ao cliente vai testar se foi registrado venda
      //para uma forma que n�o esta vinculada, se sim vai zerar
      dmCupomFiscal.dbQuery1.Active := false;
      dmCupomFiscal.dbQuery1.SQL.Clear;
      dmCupomFiscal.dbQuery1.SQL.Add('select f.FORMA_PAG from temp_nfce_formapag f '+
                  'where f.CAIXA=:pCaixa '+
                  'order by f.Forma_Pag');
      dmCupomFiscal.dbQuery1.ParamByName('pCaixa').AsString:=FormatFloat('0000', FRENTE_CAIXA.Caixa);
      dmCupomFiscal.dbQuery1.Active := True;
      dmCupomFiscal.dbQuery1.First;
      while not(dmCupomFiscal.dbQuery1.Eof) do
      begin
        if not(dmCupomFiscal.dbTemp_NFCE_FormaPag.Locate('FORMA_PAG',
          dmCupomFiscal.dbQuery1.FieldByName('FORMA_PAG').AsInteger,[loCaseInsensitive]))then
        begin
          dmCupomFiscal.IBSQL1.Close;
          dmCupomFiscal.IBSQL1.SQL.Clear;
          dmCupomFiscal.IBSQL1.SQL.Add('update temp_nfce_formapag Set vPag = 0 where Forma_Pag = :pFPag and Caixa = :pCaixa');
          dmCupomFiscal.IBSQL1.ParamByName('pCaixa').AsString := FormatFloat('0000', FRENTE_CAIXA.Caixa);
          dmCupomFiscal.IBSQL1.ParamByName('pFPag').AsInteger := dmCupomFiscal.dbQuery1.FieldByName('FORMA_PAG').AsInteger;
          dmCupomFiscal.IBSQL1.ExecQuery;
          dmCupomFiscal.IBSQL1.Close;
        end;
        dmCupomFiscal.dbQuery1.Next;
      end;
    end;
    dmCupomFiscal.dbTemp_NFCE_FormaPag.First;
  end;
end;

function TdmCupomFiscal.GetTemp_Valor_Crediario: Currency;
var
  qrBusca: TIBQuery;
begin
  qrBusca := TIBQuery.Create(Self);
  try
    qrBusca.Active := False;
    qrBusca.Database := DataBase;
    qrBusca.SQL.Clear;
    qrBusca.SQL.Add('Select Sum(vPag) as xFlag from TEMP_NFCE_FormaPag where '+
                    'CAIXA = :pCaixa and tPag = ''05''');
    qrBusca.ParamByName('pCaixa').AsString := FormatFloat('0000', FRENTE_CAIXA.Caixa);
    qrBusca.Active := True;
    Result := qrBusca.FieldByName('xFlag').AsCurrency;
  finally
    qrBusca.Free;
  end;
end;

procedure TdmCupomFiscal.LimparDadosCliente();
begin
  try
    getTemp_NFCE;
    if not(dbTemp_NFCE.State in [dsEdit, dsInsert])then
      dbTemp_NFCE.Edit;
    dbTemp_NFCECLIFOR.Value    := 0;
    dbTemp_NFCECF_NOME.Clear;
    dbTemp_NFCECF_ENDE.Clear;
    dbTemp_NFCECF_CNPJ_CPF.Clear;
    dbTemp_NFCECF_IE.Clear;
    dbTemp_NFCECF_NUMERO_END.Clear;
    dbTemp_NFCECF_MUNICIPIO.Clear;
    dbTemp_NFCECF_CEP.Clear;
    dbTemp_NFCECF_BAIRRO.Clear;
    dbTemp_NFCEINDIEDEST.Value := '9';
    dbTemp_NFCE.Post;
    Transaction.CommitRetaining;
    TAB_PRECO.TabPreco := 0;
  except
    on E:Exception do
    begin
      Transaction.RollbackRetaining;
      raise Exception.Create('Erro ao limpar dados do cliente.'+#13+#13+E.Message);
    end;
  end;
  try
    //Delete as formas de pagamento
    dmCupomFiscal.IBSQL1.close;
    dmCupomFiscal.IBSQL1.SQL.Clear;
    dmCupomFiscal.IBSQL1.SQL.Add('Delete from TEMP_NFCE_FORMAPAG where CAIXA ='+FormatFloat('0000',FRENTE_CAIXA.Caixa));
    dmCupomFiscal.IBSQL1.Prepare;
    dmCupomFiscal.IBSQL1.ExecQuery;
    dmCupomFiscal.IBSQL1.Close;
    dmCupomFiscal.Transaction.CommitRetaining;
  except
    on E:Exception do
    begin
      Transaction.RollbackRetaining;
      raise Exception.Create('Erro ao deletar formas de pagamento do cliente.'+#13+#13+E.Message);
    end;
  end;
end;

procedure TdmCupomFiscal.MoverComandasInseridas(pID: String; pEL_Chave: Integer);
var
  lQrBusca: TIBQuery;
begin
  try
    dbBarComandas.Close;
    dbBarComandas.ParamByName('pID').asString := pID;
    dbBarComandas.Open;
    if(dbBarComandas.IsEmpty)then
    begin
      lQrBusca := TIBQuery.Create(nil);
      try
        lQrBusca.Database := DataBase;
        lQrBusca.SQL.add('Select * from TEMP_BAR_COMANDAS where ID = '+
          QuotedStr(pID));
        lQrBusca.Open;
        if not(lQrBusca.IsEmpty)then
        begin
          dbBarComandas.Insert;
          dbBarComandasID.Value := lQrBusca.FieldByName('ID').asString;
          dbBarComandasDATA_HORA_AB.Value :=
            lQrBusca.FieldByName('DATA_HORA').AsDateTime;
          dbBarComandasDATA_HORA_FE.Value := now;
          dbBarComandasVLR_TOTAL.Value :=
            lQrBusca.FieldByName('VLR_TOTAL').AsCurrency;
          dbBarComandasID_FUNCIONARIO.Value :=
            lQrBusca.FieldByName('ID_FUNCIONARIO').AsInteger;
          dbBarComandasEL_CHAVE.Value := pEL_Chave;
          dbBarComandasOBSERVACAO.Value :=
            lQrBusca.FieldByName('Observacao').asString;
          dbBarComandas.Post;
        end;
        lQrBusca.SQL.Clear;
        lQrBusca.SQL.add('Select i.id, i.item, i.mercadoria, i.quantidade, i.vlr_unitario, i.vlr_total, '+
                         'i.id_funcionario, i.pago from TEMP_BAR_COMANDAS_ITEM i where ID = '+
          QuotedStr(pID));
        lQrBusca.Open;
        if not(lQrBusca.IsEmpty)then
        begin
          lQrBusca.First;
          while not(lQrBusca.Eof) do
          begin
            dbBarComandasItens.Close;
            dbBarComandasItens.ParamByName('pID').asString := lQrBusca.FieldByName('ID').asString;
            dbBarComandasItens.ParamByName('pItem').asString := lQrBusca.FieldByName('Item').asString;
            dbBarComandasItens.Open;
            if (dbBarComandasItens.IsEmpty)then
            begin
              dbBarComandasItens.Insert;
              dbBarComandasItensID.AsString :=
                lQrBusca.FieldByName('ID').asString;
              dbBarComandasItensITEM.AsString :=
                lQrBusca.FieldByName('ITEM').asString;
              dbBarComandasItensMERCADORIA.AsString :=
                lQrBusca.FieldByName('Mercadoria').asString;
              dbBarComandasItensQUANTIDADE.AsCurrency :=
                lQrBusca.FieldByName('Quantidade').AsCurrency;
              dbBarComandasItensVLR_UNITARIO.AsCurrency :=
                lQrBusca.FieldByName('Vlr_Unitario').AsCurrency;
              dbBarComandasItensVLR_TOTAL.AsCurrency :=
                lQrBusca.FieldByName('Vlr_Total').AsCurrency;
              dbBarComandasItensID_FUNCIONARIO.AsCurrency :=
                lQrBusca.FieldByName('ID_FUNCIONARIO').AsInteger;
              dbBarComandasItensPAGO.AsString :=
                lQrBusca.FieldByName('PAGO').AsString;
              dbBarComandasItens.Post;
            end;
            lQrBusca.Next;
          end;
        end;
        lQrBusca.Close;
      finally
        lQrBusca.Free;
      end;
      Transaction.CommitRetaining;
    end
    else
    begin
      dbBarComandas.Edit;
      dbBarComandasEL_CHAVE.Value := pEL_Chave;
      dbBarComandas.Post;
      Transaction.CommitRetaining;
    end;
  Except
    On E:Exception do
      raise Exception.Create('[BAR_COMANDAS_ITEM] - Erro ao gravar comandas na tabelas.'+#13+e.Message);
  end;
end;

function TdmCupomFiscal.TefEstaAtivo: Boolean;
var
  qrBusca: TIBQuery;
begin
  qrBusca := TIBQuery.Create(Self);
  try
    qrBusca.Database := DataBase;
    qrBusca.SQL.Clear;
    qrBusca.SQL.Add('Select TEF_ATIVO from TERMINAIS where MACADRESS = :pMAC');
    qrBusca.ParamByName('pMAC').AsString := GetMACAdress;
    qrBusca.Open;
    Result := (qrBusca.FieldByName('TEF_ATIVO').AsString = 'S');
    qrBusca.Close;
  finally
    qrBusca.Free;
  end;
end;

function TdmCupomFiscal.TefGetDocVinc(pNumero, pCcusto: Integer;
  pSerie: String): Integer;
var
  qrBusca: TIBQuery;
begin
  qrBusca := TIBQuery.Create(Self);
  try
    qrBusca.Database := DataBase;
    qrBusca.SQL.Clear;
    qrBusca.SQL.Add('Select Coalesce(e.EL_CHAVE,0) as EL_CHAVE from EST_ECF e '+
      'where e.Numero = :pNum and e.CCusto = :pCC and e.Serie = :pSerie');
    qrBusca.ParamByName('pNum').asInteger := pNumero;
    qrBusca.ParamByName('pCC').asInteger := pCcusto;
    qrBusca.ParamByName('pSerie').asString := pSerie;
    qrBusca.Open;
    Result := qrBusca.FieldByName('EL_CHAVE').AsInteger;
    qrBusca.Close;
  finally
    qrBusca.Free;
  end;
end;

function TdmCupomFiscal.TefGetImpressora: String;
var
  lQryBusca: TIBQuery;
begin
  lQryBusca := TIBQuery.Create(nil);
  try
    lQryBusca.Database := dmCupomFiscal.DataBase;
    lQryBusca.SQL.Add('Select IMPRESSORA_PADRAO_BOBINA from TERMINAIS where MACADRESS = :pMAC');
    lQryBusca.ParamByName('pMAC').asString := GetMACAdress;
    lQryBusca.Open;
    Result := lQryBusca.FieldByName('IMPRESSORA_PADRAO_BOBINA').AsString;
    lQryBusca.Close;
  finally
    lQryBusca.Free;
  end;
end;

function TdmCupomFiscal.TefGetValorFormaPag(pFPag, pEL_Chave: Integer): Currency;
var
  qrBusca: TIBQuery;
begin
  qrBusca := TIBQuery.Create(Self);
  try
    qrBusca.Active := False;
    qrBusca.Database := DataBase;
    qrBusca.SQL.Clear;
    qrBusca.SQL.Add('Select Coalesce(Sum(Valor),0) as xValor from Tef_Movimento '+
                    ' Where EL_Chave = :pEL_Chave and FORMA_PAG = :pFPag and Operacao <> 51');
    qrBusca.ParamByName('pEL_Chave').asInteger := pEL_Chave;
    qrBusca.ParamByName('pFPag').asInteger := pFPag;
    qrBusca.Active := True;
    Result := qrBusca.FieldByName('xValor').AsCurrency;
  finally
    qrBusca.Free;
  end;
end;

function TdmCupomFiscal.VerificaComandaPaga(pID: String): Boolean;
var
  lQryBusca: TIBQuery;
begin
  lQryBusca := TIBQuery.Create(nil);
  try
    lQryBusca.Database := dmCupomFiscal.DataBase;
    lQryBusca.SQL.Add('Select * from TEMP_BAR_COMANDAS_ITEM where ID = :pID and ((PAGO <> ''S'' and PAGO <> ''X'' ) or Pago is null)');
    lQryBusca.ParamByName('pID').asString := pID;
    lQryBusca.Open;
    Result := lQryBusca.IsEmpty;
    lQryBusca.Close;
  finally
    lQryBusca.Free;
  end;
end;

function TdmCupomFiscal.VerificarNFCePossuiItem(pCaixa: Integer): Boolean;
var
  qrBusca: TIBQuery;
begin
  //Lauro 23.01.17 ver se precisao ???
  qrBusca := TIBQuery.Create(Self);
  try
    qrBusca.Active := False;
    qrBusca.Database := DataBase;
    qrBusca.SQL.Clear;
    qrBusca.SQL.Add('Select * from TEMP_NFCE_ITEM where CAIXA = :pCaixa');
    qrBusca.ParamByName('pCaixa').AsString := FormatFloat('0000', pCaixa);
    qrBusca.Active := True;
    Result := not(qrBusca.IsEmpty);
  finally
    qrBusca.Free;
  end;
end;

end.


