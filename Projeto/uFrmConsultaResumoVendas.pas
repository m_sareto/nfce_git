unit uFrmConsultaResumoVendas;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Mask, Buttons, ComCtrls, DB, IBCustomDataSet, IBQuery,
  RLReport,rlprinters;

type
  TFrmConsultaResumoVendas = class(TForm)
    StatusBar1: TStatusBar;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    MaskEdit1: TMaskEdit;
    Label2: TLabel;
    Label5: TLabel;
    Edit3: TEdit;
    edFun: TEdit;
    Label1: TLabel;
    Label4: TLabel;
    IBQAdm1: TIBQuery;
    RLReport1: TRLReport;
    IBQRel: TIBQuery;
    IBQRelMERCADORIA: TIBStringField;
    IBQRelNOME: TIBStringField;
    IBQRelQTD: TIBBCDField;
    DataSource1: TDataSource;
    RLBand1: TRLBand;
    RLBand4: TRLBand;
    IBQRelSUBGRUPO: TIBStringField;
    IBQRelDESCRICAO1: TIBStringField;
    RLGroup1: TRLGroup;
    RLBand2: TRLBand;
    RLDBText1: TRLDBText;
    RLDBText2: TRLDBText;
    RLBand3: TRLBand;
    RLLabel1: TRLLabel;
    RLLabel2: TRLLabel;
    RLBand5: TRLBand;
    RLDBText3: TRLDBText;
    RLBand6: TRLBand;
    RLMemo2: TRLMemo;
    RLLabel3: TRLLabel;
    RLLabel4: TRLLabel;
    RLLabel5: TRLLabel;
    RLBand7: TRLBand;
    RLSystemInfo1: TRLSystemInfo;
    RLBand8: TRLBand;
    RLLabel7: TRLLabel;
    RLLabel8: TRLLabel;
    RLLabel9: TRLLabel;
    RLMemo1: TRLMemo;
    RLDraw8: TRLDraw;
    qLocal: TIBQuery;
    qLocalID: TIntegerField;
    qLocalDESCRICAO: TIBStringField;
    qLocalCNPJ: TIBStringField;
    qLocalIE: TIBStringField;
    qLocalPAIS: TIntegerField;
    qLocalMUNICIPIO: TIntegerField;
    qLocalENDERECO: TIBStringField;
    qLocalBAIRRO: TIBStringField;
    qLocalCEP: TIBStringField;
    qLocalUF: TIBStringField;
    qLocalFONE: TIBStringField;
    qLocalFAX: TIBStringField;
    qLocalNUMERO_END: TIBStringField;
    qLocalCOMPLEMENTO_END: TIBStringField;
    qLocalNOME_FANTASIA: TIBStringField;
    qLocalIM: TIBStringField;
    qLocalCNAE: TIBStringField;
    qLocalE_MAIL: TIBStringField;
    qLocalALI_SIMPLESN_ICM: TIBBCDField;
    qLocalCRT: TIntegerField;
    qLocalCALCULA_PESO: TIBStringField;
    qLocalDATA_EXP: TDateField;
    qLocalANOMES: TIntegerField;
    qLocalDATA_MOV: TDateField;
    qLocalNFS_INICIAL: TIntegerField;
    qLocalNFS_FINAL: TIntegerField;
    qLocalNFS_AUTORIZACAO: TIBStringField;
    qLocalCTE_RNTRC: TIBStringField;
    qLocalTURNO: TIBStringField;
    qLocalVLR_IRRF: TIBBCDField;
    qLocalANP_ARI: TIBStringField;
    qLocalDES_MUNI: TIBStringField;
    qLocalIMPRESSORA: TIBStringField;
    IBQRelDESCRICAO: TIBStringField;
    procedure BitBtn2Click(Sender: TObject);
    procedure Label1Click(Sender: TObject);
    procedure edFunExit(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure RLLabel3BeforePrint(Sender: TObject; var Text: String;
      var PrintIt: Boolean);
    procedure RLLabel4BeforePrint(Sender: TObject; var Text: String;
      var PrintIt: Boolean);
    procedure RLLabel5BeforePrint(Sender: TObject; var Text: String;
      var PrintIt: Boolean);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure RLMemo1BeforePrint(Sender: TObject; var Text: String;
      var PrintIt: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure RLMemo2BeforePrint(Sender: TObject; var Text: String;
      var PrintIt: Boolean);
    procedure RLBand8BeforePrint(Sender: TObject; var PrintIt: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmConsultaResumoVendas: TFrmConsultaResumoVendas;

implementation

uses uFrmPesquisa, uDMCupomFiscal, uFrmPrincipal, uRotinasGlobais;

{$R *.dfm}

procedure TFrmConsultaResumoVendas.BitBtn2Click(Sender: TObject);
begin
  close;
end;

procedure TFrmConsultaResumoVendas.Label1Click(Sender: TObject);
begin
  frmPesquisa := tfrmPesquisa.Create(Self,'Select Id,Nome From Funcionarios Where (Nome>=:pTexto) Order By Nome',
                                          'Select Id,Nome From Funcionarios Where (Nome like :pTexto) Order By Nome');
  frmPesquisa.DBGrid1.Columns.Add;
  frmPesquisa.DBGrid1.Columns[0].Title.Caption:='C�digo';
  frmPesquisa.DBGrid1.Columns[0].Width:=42;
  frmPesquisa.DBGrid1.Columns[0].FieldName:='id';

  frmPesquisa.DBGrid1.Columns.Add;
  frmPesquisa.DBGrid1.Columns[1].Title.Caption:='Nome';
  frmPesquisa.DBGrid1.Columns[1].Width:=337;
  frmPesquisa.DBGrid1.Columns[1].FieldName:='Nome';
  try
    if (frmPesquisa.ShowModal=mrOK) then edFun.Text:= FrmPesquisa.IBQuery1.FieldByName('id').AsString;
  finally
    frmPesquisa.Free;
  end;
end;

procedure TFrmConsultaResumoVendas.edFunExit(Sender: TObject);
begin
  IBQAdm1.Active:=False;
  IBQAdm1.SQL.Clear;
  IBQAdm1.SQL.Add('Select Nome From Funcionarios Where (Id=:pId)');
  IBQAdm1.ParamByName('pId').AsInteger:=StrToIntDef(edFun.text,0);
  IBQAdm1.Active:=True;
  IBQAdm1.First;
  If not(IBQAdm1.eof) then
    Label4.Caption:=Copy(IBQAdm1.fieldbyname('Nome').AsString,1,30)
  Else
  begin
    ShowMessage('Funcion�rio N�o Cadastrado...');
    edFun.SetFocus;
    Abort;
  end;
end;

procedure TFrmConsultaResumoVendas.BitBtn1Click(Sender: TObject);
begin
  IBQRel.Close;
  IBQRel.SQL.Clear;
  IBQRel.SQL.Add('Select i.mercadoria, m.descricao,  f.nome, sum(i.qtd) as QTD,m.subgrupo,sb.descricao '+
                 'from est_ecf_item i '+
                 'Left outer join est_ecf e on e.id = i.id_ecf '+
                 'left outer join est_mercadorias m on m.id = i.mercadoria '+
                 'Left outer join Funcionarios f on f.id = e.funcionario   '+
                 'left outer join est_subgrupos sb on sb.id=m.subgrupo '+
                 'where (e.data = :pData) '+
                 'and m.subtipo_item<>''CO'' ');
  IBQRel.SQL.Add('and (e.funcionario = :pFun) ');

  if Trim(Edit3.Text)<>'' then
    IBQRel.SQL.Add('and (e.turno=:pTurno ) ');

  IBQRel.SQL.Add('group by 1,2,3,5,6 '+
                 'order by 5,3,2');

  IBQRel.ParamByName('pData').AsString:=MaskEdit1.Text;
  IBQRel.ParamByName('pFun').AsString:=edFun.Text;
  if Trim(Edit3.Text)<>'' then
    IBQRel.ParamByName('pTurno').AsString:=Edit3.Text;
  IBQRel.Open;
  IBQRel.First;


  if IBQRel.Eof then
    ShowMessage('Dados Inexistentes.'+#13+'Por Favor Verifique!')
  else
    RLReport1.Preview();
end;

procedure TFrmConsultaResumoVendas.RLLabel3BeforePrint(Sender: TObject;
  var Text: String; var PrintIt: Boolean);
begin
  Text:='Funcion�rio: ';
  if edFun.Text<>'0' then
    Text:=Text+' '+edFun.Text+' - ';
  Text:=Text+Label4.Caption;
end;

procedure TFrmConsultaResumoVendas.RLLabel4BeforePrint(Sender: TObject;
  var Text: String; var PrintIt: Boolean);
begin
  if Trim(Edit3.text)<>'' then
    text:='Turno: '+Edit3.Text
  else
    text:='Turno: Todos';
end;

procedure TFrmConsultaResumoVendas.RLLabel5BeforePrint(Sender: TObject;
  var Text: String; var PrintIt: Boolean);
begin
  Text:='Data: '+maskedit1.Text;
end;

procedure TFrmConsultaResumoVendas.FormKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if vk_escape=Key then BitBtn2Click(action);
  if (vk_f12=Key) and (edFun.Focused) then Label1Click(Action);
end;

procedure TFrmConsultaResumoVendas.RLMemo1BeforePrint(Sender: TObject;
  var Text: String; var PrintIt: Boolean);
begin
  Text:='Funcion�rio: ';
  if edFun.Text<>'0' then
    Text:=Text+' '+edFun.Text+' - ';
  Text:=Text+Label4.Caption;
  if Trim(Edit3.text)<>'' then
    text:=text+sLineBreak+'Turno: '+Edit3.Text;
  if Trim(Edit3.text)<>'' then
    text:='Turno: '+Edit3.Text
  else
    text:='Turno: '+Edit3.Text
end;

procedure TFrmConsultaResumoVendas.FormCreate(Sender: TObject);
begin
  MaskEdit1.Text:=FormatDateTime('dd/mm/yy',Now);
end;

procedure TFrmConsultaResumoVendas.RLMemo2BeforePrint(Sender: TObject;
  var Text: String; var PrintIt: Boolean);
begin
  Text:='Rela��o vendas de mercadorias';
end;

procedure TFrmConsultaResumoVendas.RLBand8BeforePrint(Sender: TObject;
  var PrintIt: Boolean);
begin
  qLocal.close;
  qLocal.ParamByName('pCC').value:=CCUSTO.codigo;
  qLocal.Open;
  RLLabel7.Caption    := qLocalNOME_FANTASIA.AsString;
  RLLabel8.Caption    := qLocalDESCRICAO.AsString;
  RLLabel9.Caption    := 'CNPJ:'+qLocalCNPJ.AsString + ' IE:'+qLocalIE.AsString + ' IM:'+qLocalIM.AsString;
  RLMemo1.Lines.Text  := ' '+qLocalENDERECO.AsString+' - '+qLocalNUMERO_END.AsString+', '+
                         qLocalBAIRRO.AsString+', '+qLocalDES_MUNI.AsString+', '+qLocalUF.AsString+', '+
                         qLocalCEP.AsString+', '+qLocalFONE.AsString;

  RLPrinter.PrinterName:=qLocalIMPRESSORA.Value;

end;

end.
