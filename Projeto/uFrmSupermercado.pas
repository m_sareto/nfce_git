{$I ACBr.inc}

unit uFrmSupermercado;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ExtCtrls, StdCtrls, jpeg, DB,
  IBCustomDataSet, Mask, DBCtrls, Buttons,UFrmCancelaItem,
  Inifiles, IBQuery, TREdit, ActnMan, ActnColorMaps,
  DBClient, Grids, DBGrids, Menus, AppEvnts, StrUtils,
  {$IFDEF Delphi6_UP}Types, {$ELSE} {$ENDIF} ACBrBase, ACBrBAL, ImgList,
   ACBrLCB, ACBrDevice, ToolWin;

  //Abrir gaveta Epson
  Function IniciaPorta(pszPorta:PAnsiChar):Integer; StdCall; External 'InterfaceEpsonNF.dll';
  function AcionaGaveta():Integer; StdCall; External 'InterfaceEpsonNF.dll';
  function FechaPorta():Integer; StdCall; External 'InterfaceEpsonNF.dll';
  function Le_Status():Integer; StdCall; External 'InterfaceEpsonNF.dll';

type
  TfrmSupermercado = class(TForm)
    dsItem: TDataSource;
    DBGrid1: TDBGrid;
    Image1: TImage;
    Timer2: TTimer;
    pnlLendoPeso: TPanel;
    pnlRodape: TPanel;
    pnlDadosIF: TPanel;
    lblNumCaixa: TLabel;
    lblCCusto: TLabel;
    lblOperador: TLabel;
    lblData: TLabel;
    pnlEntradaDados: TPanel;
    bevSuaCompra: TBevel;
    Label1: TLabel;
    Label4: TLabel;
    lblSuaCompra: TLabel;
    Label3: TLabel;
    lblBalanca: TLabel;
    lblLeitorCom: TLabel;
    reQuantidade: TRealEdit;
    reVlrUnitario: TRealEdit;
    edtCodigoMercadoria: TEdit;
    reSubTotal: TRealEdit;
    barStatus: TStatusBar;
    pnlBotoes: TPanel;
    btnFinalizarCupom: TSpeedButton;
    btnCancelarCupom: TSpeedButton;
    btnCancelarItem: TSpeedButton;
    btnConsultarCR: TSpeedButton;
    btnAbrirGaveta: TSpeedButton;
    btnCapturarPeso: TSpeedButton;
    btnPesquisaMerc: TSpeedButton;
    btnConsultaPreco: TSpeedButton;
    btnSair: TSpeedButton;
    ApplicationEvents1: TApplicationEvents;
    lblMercadoria: TLabel;
    pnlMensagem: TPanel;
    txtMensagem: TLabel;
    tmrScroll: TTimer;
    cdsImagens: TClientDataSet;
    cdsImagensCAMINHO: TStringField;
    procedure Timer2Timer(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtCodigoMercadoriaExit(Sender: TObject);

    procedure reVlrUnitarioExit(Sender: TObject);
    procedure reQuantidadeExit(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure reSubTotalEnter(Sender: TObject);
    Procedure ativarBalancaCheckOut();
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure pad(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure btnSairClick(Sender: TObject);
    procedure btnFinalizarCupomClick(Sender: TObject);
    procedure btnCancelarCupomClick(Sender: TObject);
    procedure btnCancelarItemClick(Sender: TObject);
    procedure btnConsultarCRClick(Sender: TObject);
    procedure btnAbrirGavetaClick(Sender: TObject);
    procedure btnCapturarPesoClick(Sender: TObject);
    procedure btnPesquisaMercClick(Sender: TObject);
    procedure btnConsultaPrecoClick(Sender: TObject);
    procedure reVlrUnitarioEnter(Sender: TObject);
    procedure DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure FormCreate(Sender: TObject);
    procedure ApplicationEvents1Hint(Sender: TObject);
    procedure tmrScrollTimer(Sender: TObject);
  private
    ncm : String;
    { Private declarations }
    procedure limpaCampos();
    procedure buscarImagens();
    procedure lerBalancaCheckOut;
    procedure setCores();
    procedure limpaCamposMercadoria();
    procedure formataQtd;
    Procedure formataVlrUni;
  public
    { Public declarations }
  end;

var
  frmSupermercado     : TfrmSupermercado;
  fotoSM              : String; //Fotos
  iRetorno            : Integer;

implementation

uses Math, uFrmFim,
  UfrmPesquisaCodigo, uFrmConsultaLimiteCR,
  ACBrUtil, uFrmAutorizacao, uRotinasGlobais, uFrmPrincipal, uFuncoes,
  uFrmPesquisaMercadoria, uDMComponentes, ACBrECF, uFrmLogin, uFrmNFCe,
  uDMCupomFiscal, uFrmAjustarNcm, uFrmClifor;

{$R *.dfm}

procedure TfrmSupermercado.Timer2Timer(Sender: TObject);
begin
  If (fotoSM=' ') then
  begin
    if(FileExists(ExtractFilePath(Application.EXEName)+'Foto\'+FRENTE_CAIXA.Pasta_Fotos+'\Foto1.jpg'))then
      Image1.Picture.LoadFromFile(ExtractFilePath(Application.EXEName)+'Foto\'+FRENTE_CAIXA.Pasta_Fotos+'\Foto1.jpg');
    fotoSM:='A';
  end

  ELSE IF (fotoSM='A') then
  begin
//    Image1.Picture.LoadFromFile(ExtractFilePath(Application.EXEName)+'Foto\Foto2.jpg');
    if(FileExists(ExtractFilePath(Application.EXEName)+'Foto\'+FRENTE_CAIXA.Pasta_Fotos+'\Foto2.jpg'))then
      Image1.Picture.LoadFromFile(ExtractFilePath(Application.EXEName)+'Foto\'+FRENTE_CAIXA.Pasta_Fotos+'\Foto2.jpg');
    fotoSM:='B';
  end

  ELSE if (fotoSM='B') THEN
  begin
//    Image1.Picture.LoadFromFile(ExtractFilePath(Application.EXEName)+'Foto\Foto3.jpg');
    if(FileExists(ExtractFilePath(Application.EXEName)+'Foto\'+FRENTE_CAIXA.Pasta_Fotos+'\Foto3.jpg'))then
      Image1.Picture.LoadFromFile(ExtractFilePath(Application.EXEName)+'Foto\'+FRENTE_CAIXA.Pasta_Fotos+'\Foto3.jpg');
    fotoSM:='C';
  end

  ELSE if (fotoSM='C') THEN
  begin
//    Image1.Picture.LoadFromFile(ExtractFilePath(Application.EXEName)+'Foto\Foto4.jpg');
    if(FileExists(ExtractFilePath(Application.EXEName)+'Foto\'+FRENTE_CAIXA.Pasta_Fotos+'\Foto4.jpg'))then
      Image1.Picture.LoadFromFile(ExtractFilePath(Application.EXEName)+'Foto\'+FRENTE_CAIXA.Pasta_Fotos+'\Foto4.jpg');
    fotoSM:='D';
  end

  ELSE if (fotoSM='D') THEN
  begin
//    Image1.Picture.LoadFromFile(ExtractFilePath(Application.EXEName)+'Foto\Foto5.jpg');
    if(FileExists(ExtractFilePath(Application.EXEName)+'Foto\'+FRENTE_CAIXA.Pasta_Fotos+'\Foto5.jpg'))then
      Image1.Picture.LoadFromFile(ExtractFilePath(Application.EXEName)+'Foto\'+FRENTE_CAIXA.Pasta_Fotos+'\Foto5.jpg');
    fotoSM:='E';
  end

  ELSE if (fotoSM='E') THEN
  begin
//    Image1.Picture.LoadFromFile(ExtractFilePath(Application.EXEName)+'Foto\Foto6.jpg');
    if(FileExists(ExtractFilePath(Application.EXEName)+'Foto\'+FRENTE_CAIXA.Pasta_Fotos+'\Foto6.jpg'))then
      Image1.Picture.LoadFromFile(ExtractFilePath(Application.EXEName)+'Foto\'+FRENTE_CAIXA.Pasta_Fotos+'\Foto6.jpg');
    fotoSM:=' ';
  end;
end;


procedure TfrmSupermercado.tmrScrollTimer(Sender: TObject);
begin
  txtMensagem.Left := txtMensagem.Left - 1;
  if(txtMensagem.Left <= pnlMensagem.Left - txtMensagem.Width)Then
    txtMensagem.Left := pnlMensagem.Width;
end;

procedure TfrmSupermercado.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  //Sair do Sistema
  If Key=VK_escape then
  begin
    btnSair.Click;
  end;

  //Finalizar Cupom - F1
  if Key=VK_F1 then
  begin
    btnFinalizarCupom.Click;
  end;

  //Cancelar Cupom - F2
  {if (Key=VK_F2) then
  begin
    btnCancelarCupom.Click;
  end;
  }
  //Cancelamento de Item - F3
  if (Key=VK_F3) then
  begin
    btnCancelarItem.Click;
  end;

  //Consulta Saldo/Limite Conta Corrente - F6
  if Key=VK_F6 then
  begin
    btnConsultarCR.Click;
  end;

  //Abre Gaveta - F7
  if Key=VK_F7 then
  begin
    btnAbrirGaveta.Click;
  end;

  //Captura Peso Balanca CheckOut - F8
  if Key=VK_F8 then
  begin
    btnCapturarPeso.Click;
  end;

  //Informa Valor Unit�rio - F9
  if Key=VK_F9 then
  begin
    reVlrUnitario.SetFocus;
  end;

  //Informa Quantidade - F10
  if Key=VK_F10 then
  begin
    reQuantidade.ReadOnly:=FALSE;
    reQuantidade.SetFocus;
  end;

  //Consulta por Descri��o - F11
  if Key=VK_F11 then
  begin
    btnPesquisaMerc.Click;
  end;

  //Consulta Preco - F12
  if key=VK_F12 then
  begin
    btnConsultaPreco.Click;
  end;

  //Alterar Operador - Ctrol + O
  if (Shift = [ssCtrl]) then
  begin
    //Saber c�digo da Tecla
    //ShowMessage(Format('O c�digo da tecla pressionada �: %d', [Key]));
    if Key=79 then
    begin
      if reSubTotal.Value > 0 then
      begin
        msgInformacao('N�o � poss�vel alterar o Operador ap�s iniciar a venda','');
      end
      else
      begin
        frmLogin:=TfrmLogin.create(Application);
        frmLogin.ShowModal;
        frmLogin.Free;
        If LOGIN.usuarioCod  <=0 then
        begin
          msgErro('Operador n�o identificado','');
          Close;
        end
        else
        begin
          setDadosRodape;
        end;
      end;
    end;
  end;
end;


procedure TfrmSupermercado.edtCodigoMercadoriaExit(Sender: TObject);
Var xQtd:String;
begin
  if (Trim(edtCodigoMercadoria.Text) <> '') then
  begin
    try
      reQuantidade.Font.Color:=clBlack;
      reVlrUnitario.Font.Color:=clBlack;

      //--- C�digos da Balan�a
      xQtd:='';
      MERCADORIA.codigo := Trim(Copy(edtCodigoMercadoria.Text, 1, 13));
      if (Copy(edtCodigoMercadoria.Text,1,2)='20') or (Copy(edtCodigoMercadoria.Text,1,2)='23') then
      begin
        xQtd := edtCodigoMercadoria.Text;
        MERCADORIA.codigo := IntToStr(StrToInt(Copy(edtCodigoMercadoria.Text,3,4)));
      end;

      dmCupomFiscal.dbQuery1.Active:=False;
      dmCupomFiscal.dbQuery1.SQL.Clear;
      dmCupomFiscal.dbQuery1.SQL.ADD('SELECT M.Unidade, M.Cst_Pis, M.Cst_Cofins, M.Venda, M.Descricao, M.Sucinto, M.ICM_ECF, M.ISSQN, M.SM_BLC_UP,M.servico,'+
                                    ' M.ECF_CST_ICM, M.ECF_CFOP, M.ncm, M.SUBTIPO_ITEM, M.Desconto,'+
                                    ' M.SM_DtE_Promocao, M.SM_DtS_Promocao, M.SM_Venda_Promocao, M.Ativo, M.Custo_Ultimo, M.Custo_Medio, Me.Est_Atual,'+
                                    ' M.ID_Codigo, M.Estoque_Vinculado'+
                                    ' FROM Est_Mercadorias M'+
                                    ' Left Outer Join Est_Mercadorias_Estoque ME on ME.Mercadoria=M.Id and Me.CCusto=:pCC'+
                                    ' WHERE (ID=:pId)');
      dmCupomFiscal.dbQuery1.ParamByName('pId').AsString :=MERCADORIA.codigo;
      dmCupomFiscal.dbQuery1.ParamByName('pCC').AsInteger := CCUSTO.codigo;
      dmCupomFiscal.dbQuery1.Active:=True;
      dmCupomFiscal.dbQuery1.First;
      If not(DMCupomFiscal.dbQuery1.Eof) then
      begin
        //Verifica se mercadoria est� Ativa
        If (dmCupomFiscal.dbQuery1.FieldByName('Ativo').AsString='N') then
        begin
          msgInformacao('Mercadoria est� Inativa. Favor verificar cadastro','Aten��o');
          limpaCamposMercadoria;
        end
        else
        begin
          ncm := DMCupomFiscal.dbQuery1.FieldByName('NCM').AsString;
          If not (frmPrincipal.Valida_NCM(ncm)) then
          begin
            if not(msgPergunta('NCM informado na mercadoria n�o existe na base de dados.'+#13+
                               'A partir de 01/01/2016 o NCM obrigatoriamente deve'+#13+
                               'existir na base de dados disponibilizada pelo governo.'+#13+#13+
                               'NCM: '+ncm+#13+
                               'Deseja ajustar o cadastro da mercadoria e tentar novamente?'
                               ,Application.Title))then
            begin
              edtCodigoMercadoria.SetFocus;
              Abort;
            end
            else
            begin
              try
                frmAjustarNcm := TfrmAjustarNcm.Create(Self, dmCupomFiscal.dbQuery1.fieldByName('NCM').asString,
                                 dmCupomFiscal.dbQuery1.fieldByName('Descricao').asString,
                                 MERCADORIA.codigo);
                case frmAjustarNcm.ShowModal of
                mrOK:begin //se OK ent�o cliente atualizou o NCM
                    if(frmAjustarNcm.yNCM_NOVO <> dmCupomFiscal.dbQuery1.fieldByName('NCM').asString)then
                      ncm := frmAjustarNcm.yNCM_NOVO;
                  end;
                mrCancel:begin
                    edtCodigoMercadoria.SetFocus;
                    Abort;
                  end;
                mrAbort:begin
                    edtCodigoMercadoria.SetFocus;
                    Abort;
                  end
                end;
              finally
                frmAjustarNcm.Free;
              end;
            end;
          end;
          MERCADORIA.subtipo_item := DMCupomFiscal.dbQuery1.FieldByName('subtipo_item').AsString;
          MERCADORIA.servico := dmCupomFiscal.dbQuery1.FieldByName('servico').AsString='S';
          MERCADORIA.descricao := dmCupomFiscal.dbQuery1.FieldByName('Descricao').AsString;
          MERCADORIA.Est_Vinculado := dmCupomFiscal.dbQuery1.FieldByName('Estoque_Vinculado').AsString;

          //Se quantidade <= 0 quantiade=1
          If (reQuantidade.Value <= 0) then
            reQuantidade.Text:='1,000';

          //Verifica se Estoque n�o esta nulo
          if DMCupomFiscal.dbQuery1.FieldByName('Est_Atual').IsNull then
          begin
            edtCodigoMercadoria.Clear;
            edtCodigoMercadoria.SetFocus;
            msgInformacao('Mercadoria n�o possui registro de estoque para centro de custo '+IntToStr(CCUSTO.codigo),'');
          end
          else
          begin
            //Verifica se CCusto n�o permite venda da mercadoria com estoque negativo
            If not(MERCADORIA.servico) and not(verificarEstoque(MERCADORIA.codigo,
              MERCADORIA.Est_Vinculado, MERCADORIA.subtipo_item,
              DMCupomFiscal.dbQuery1.FieldByName('est_atual').AsCurrency, reQuantidade.Value)) Then
            begin
              limpaCamposMercadoria;
            end
            else
            begin
              MERCADORIA.descricao  := DMCupomFiscal.dbQuery1.FieldByName('sucinto').AsString;
              MERCADORIA.unidade    := DMCupomFiscal.dbQuery1.FieldByName('unidade').AsString;
              MERCADORIA.icms       := DMCupomFiscal.dbQuery1.FieldByName('icm_ecf').AsString;
              MERCADORIA.aliqISS    := DMCupomFiscal.dbQuery1.FieldByName('issqn').AsCurrency;
              MERCADORIA.cstPIS     := DMCupomFiscal.dbQuery1.FieldByName('cst_pis').AsString;
              MERCADORIA.cstCOFINS  := DMCupomFiscal.dbQuery1.FieldByName('cst_cofins').AsString;
              MERCADORIA.estoque    := DMCupomFiscal.dbQuery1.FieldByName('est_atual').AsCurrency;
              MERCADORIA.cstICMS    := DMCupomFiscal.dbQuery1.FieldByName('ECF_CST_ICM').AsString;
              MERCADORIA.cfop       := DMCupomFiscal.dbQuery1.FieldByName('ECF_CFOP').AsInteger;
              MERCADORIA.ncm        := ncm;
              lblMercadoria.Caption := MERCADORIA.descricao;

              //Valor Unit�rio
              If (reVlrUnitario.Value <= 0) then
              begin
                //Verifica se produto esta na promo��o
                If (DMCupomFiscal.dbQuery1.FieldByName('SM_DtE_Promocao').AsDateTime <= Date)
                  and (DMCupomFiscal.dbQuery1.FieldByName('SM_DtS_Promocao').AsDateTime >= Date) then
                  reVlrUnitario.Value := DMCupomFiscal.dbQuery1.FieldByName('SM_Venda_Promocao').AsCurrency
                else
                  reVlrUnitario.Value := DMCupomFiscal.dbQuery1.FieldByName('Venda').AsCurrency;

                //Verifica se mercadora possui n�o valor de venda BLOQUEAR
                If (reVlrUnitario.Value <= 0) then
                begin
                  msgInformacao('Mercadoria sem valor de Venda','');
                  edtCodigoMercadoria.Clear;
                  edtCodigoMercadoria.SetFocus;
                  Abort;
                end;
              end;

              //Balan�a - Quantidade
              If (Copy(xQtd,1,2)='20') or (Copy(xQtd,1,2)='23') then
              begin
                reQuantidade.text   := Copy(xQtd,8,5);
                reQuantidade.Value  := ((reQuantidade.value/reVlrUnitario.value)/100);
                If DMCupomFiscal.dbQuery1.FieldByName('SM_BLC_UP').AsString='U' then
                begin
                  reQuantidade.Text := Copy(Trim(reQuantidade.Text),1,2);
                  reQuantidade.Text := StringReplace(FormatFloat('##0.000', reQuantidade.Value),'.' ,'',[rfReplaceAll]);
                  reQuantidade.Text := FormatFloat('###,##0.000', reQuantidade.Value);
                end;
              end;

              //Arredondar/Truncar Quantidade
              If FRENTE_CAIXA.Truncar then
                reQuantidade.Value := TruncarDecimal(reQuantidade.Value, FRENTE_CAIXA.Casas_Dec_Quantidade)
              Else
                reQuantidade.Value := ArredondaDecimal(reQuantidade.Value, FRENTE_CAIXA.Casas_Dec_Quantidade);

              MERCADORIA.vlrUnitario:=reVlrUnitario.Value;
              MERCADORIA.quantidade:=reQuantidade.Value;

              if ValidarNCM(MERCADORIA.ncm) then
              begin
                //Se n�o possuir registro temp_nfce insere
                if not(possuiRegistroTempECF(FRENTE_CAIXA.Caixa)) then
                begin
                  try
                    inserirTempNFCe();
                  except
                    on e:exception do
                    begin
                      dmCupomFiscal.dbTemp_NFCE.Transaction.RollbackRetaining;
                      raise Exception.Create('[TEMP_NFCE] - Erro ao inserir registro na base de dados. '+#13+e.Message);
                      //logErros(Self, caminhoLog, '[TEMP_NFCE] - Erro ao inserir registro na base de dados. '+e.Message, '[TEMP_NFCE] - Erro ao inserir registro na base de dados' , 'S', E);
                    end;
                  end;
                end;

                //-------------- Salvar TEMP_NFCE_ITEM BD -----------------
                try
                  Try
                    DMCupomFiscal.dbTemp_NFCE_Item.Append;
                    DMCupomFiscal.dbTemp_NFCE_ItemCAIXA.Value        := FormatFloat('0000', FRENTE_CAIXA.caixa);
                    DMCupomFiscal.dbTemp_NFCE_ItemITEM.Value         := FormatFloat('0000',0); //Ver controle item
                    DMCupomFiscal.dbTemp_NFCE_ItemQUANTIDADE.Value   := MERCADORIA.quantidade;
                    DMCupomFiscal.dbTemp_NFCE_ItemMERCADORIA.Value   := Trim(MERCADORIA.codigo);
                    DMCupomFiscal.dbTemp_NFCE_ItemM_DESCRICAO.Value  := Trim(MERCADORIA.descricao);
                    DMCupomFiscal.dbTemp_NFCE_ItemVLR_UNITARIO.Value := MERCADORIA.vlrUnitario;
                    If FRENTE_CAIXA.Truncar then
                      dmCupomFiscal.dbTemp_NFCE_ItemVLR_TOTAL.Value  := TruncarDecimal(MERCADORIA.vlrUnitario * MERCADORIA.quantidade,2)
                    else
                      dmCupomFiscal.dbTemp_NFCE_ItemVLR_TOTAL.Value  := ArredondaDecimal(MERCADORIA.vlrUnitario * MERCADORIA.quantidade,2);
                    DMCupomFiscal.dbTemp_NFCE_ItemQTD_CAN.Value      := 0;
                    dmCupomFiscal.dbTemp_NFCE_ItemVLR_DESCONTO.Value := 0;
                    dmCupomFiscal.dbTemp_NFCE_ItemVLR_DESCONTO_ITEM.Value := 0;
                    DMCupomFiscal.dbTemp_NFCE_ItemVLR_CUSTO_ATUAL.Value := 0; //Trigger busca
                    DMCupomFiscal.dbTemp_NFCE_ItemICM.Value          := MERCADORIA.icms;
                    DMCupomFiscal.dbTemp_NFCE_ItemCST_PIS.Value      := MERCADORIA.cstPIS;
                    DMCupomFiscal.dbTemp_NFCE_ItemCST_COFINS.Value   := MERCADORIA.cstCOFINS;
                    DMCupomFiscal.dbTemp_NFCE_ItemCST_ICM.Value      := MERCADORIA.cstICMS;
                    DMCupomFiscal.dbTemp_NFCE_ItemCFOP.Value         := MERCADORIA.cfop;
                    DMCupomFiscal.dbTemp_NFCE_ItemNCM.Value          := MERCADORIA.ncm;
                    DMCupomFiscal.dbTemp_NFCE_ItemUNIDADE.Value      := MERCADORIA.unidade;
                    DMCupomFiscal.dbTemp_NFCE_ItemINDTOT.Value       := '1'; //--Inclui valor no Total da NF
                    DMCupomFiscal.dbTemp_NFCE_ItemVLR_BC_ICM.Value   := 0;
                    DMCupomFiscal.dbTemp_NFCE_ItemALI_ICM.Value      := 0;
                    DMCupomFiscal.dbTemp_NFCE_ItemVLR_ICM.Value      := 0;
                    DMCupomFiscal.dbTemp_NFCE_ItemVLR_FRETE.Value    := 0;
                    DMCupomFiscal.dbTemp_NFCE_ItemVLR_SEG.Value      := 0;
                    DMCupomFiscal.dbTemp_NFCE_ItemVLR_OUTRO.Value    := 0;
                    DMCupomFiscal.dbTemp_NFCE_ItemVLR_BC_PIS.Value   := 0;
                    DMCupomFiscal.dbTemp_NFCE_ItemALI_PIS.Value      := 0;
                    DMCupomFiscal.dbTemp_NFCE_ItemVLR_PIS.Value      := 0;
                    DMCupomFiscal.dbTemp_NFCE_ItemVLR_BC_COFINS.Value:= 0;
                    DMCupomFiscal.dbTemp_NFCE_ItemALI_COFINS.Value   := 0;
                    DMCupomFiscal.dbTemp_NFCE_ItemVLR_COFINS.Value   := 0;
                    DMCupomFiscal.dbTemp_NFCE_ItemVLR_TOTTRIB.Value  := 0;
                    DMCupomFiscal.dbTemp_NFCE_ItemVLR_TOTTRIB_EST.Value  := 0;
                    DMCupomFiscal.dbTemp_NFCE_ItemVLR_TOTTRIB_MUN.Value  := 0;
                    DMCupomFiscal.dbTemp_NFCE_ItemKIT.value := 'N';
                    dmCupomFiscal.dbTemp_NFCE_Item.Post;
                    DMCupomFiscal.Transaction.CommitRetaining;
                  except
                    on e:exception do
                    begin
                      DMCupomFiscal.dbTemp_NFCE_Item.Transaction.RollbackRetaining;
                      getTemp_NFCE_Item;
                      raise Exception.Create('[TEMP_ECF_ITEM] - Erro ao gravar item na base de dados.'+#13+e.Message);
                    end;
                  end;
                  DBGrid1.Visible:=True;
                  reSubTotal.Value:=getValorSubtotalTemp(FRENTE_CAIXA.Caixa);
                except
                  on e:Exception do
                  begin
                    logErros(Self, caminhoLog, e.Message+#13+'Mercadoria: '+MERCADORIA.codigo+'-'+MERCADORIA.descricao, 'Erro ao vender Item. Mercadoria: '+MERCADORIA.codigo+'-'+MERCADORIA.descricao, 'S', E);
                  end;
                end;
              end;
            end;
          end;
        end;
      end
      Else
      begin
        msgInformacao('Mercadoria n�o cadastrada. Favor verificar!','');
      end;
      limpaCamposMercadoria;
    finally
      formataQtd;
      formataVlrUni;
      edtCodigoMercadoria.Clear;
      getTemp_NFCE_Item
    end;
  end;
end;


procedure TfrmSupermercado.reVlrUnitarioExit(Sender: TObject);
begin
  if (Trim(reVlrUnitario.Text)='') then reVlrUnitario.Value:=0;
  if valorUnitarioMaximoExcedeu(reVlrUnitario.Value) then
    reVlrUnitario.SetFocus
  else
  begin
    FormataVlrUni; //Fomata casa decimais Valor Unitario
    reVlrUnitario.Font.Color:=clRed;
    edtCodigoMercadoria.Clear;
    edtCodigoMercadoria.SetFocus;
  end;
end;

procedure TfrmSupermercado.reQuantidadeExit(Sender: TObject);
begin
  if (Trim(reQuantidade.Text)='') then reQuantidade.Value:=0;
  if quantidadeMaximaExcedeu(reQuantidade.Value) then
    reQuantidade.SetFocus
  else
  begin
    FormataQtd; //Fomata casa decimais na quantidade;
    reQuantidade.Font.Color:=clRed;
    edtCodigoMercadoria.Clear;
    edtCodigoMercadoria.SetFocus;
  end;
end;

procedure TfrmSupermercado.FormShow(Sender: TObject);
begin
  try
    //Leitor Fixo Serial
    if LEITOR.Leitor then
    begin
      lblLeitorCom.Caption:='Leitor: Sim';
      dmComponentes.ACBrLCB1.Ativar;
      frmPrincipal.mResp.Lines.Add( 'Leitor Ativar' );
    end
    else
      lblLeitorCom.Visible:=False;

    //Balan�a CheckOut
    If BALANCA.Modelo > 0  then //0=Nenhuma
      ativarBalancaCheckOut
    else
      lblBalanca.Visible:=False;

    setDadosRodape();
    limpaCampos();
    Timer2Timer(Sender);  //Para ja abrir com imagem sen�o espera 10 segundos para mostrar imagem
    getTemp_NFCE_Item();
  except
    on e:Exception do
    begin
      if e is EDatabaseError then
        logErros(Self, caminhoLog,'Erro ao abrir Frente de Caixa'+#13+E.Message, 'Erro ao abrir Frente de Caixa','S',e)
      else
        logErros(Self, caminhoLog,'N�o foi poss�vel comunicar-se com a Impressora Fiscal.', 'N�o foi poss�vel comunicar-se com a Impressora Fiscal','S',e);
      PostMessage(Handle, WM_CLOSE, 0, 0);  // Fechar o Form
    end;
  end;
end;

procedure TfrmSupermercado.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  CanClose := false;
  //Fecha Tabelas
  DMCupomFiscal.dbECF.Close;
  DMCupomFiscal.dbECF_Item.Close;
  DMCupomFiscal.dbTemp_NFCE_Item.Close;

  CanClose := true;
end;

procedure TfrmSupermercado.reSubTotalEnter(Sender: TObject);
begin
  edtCodigoMercadoria.SetFocus;
end;

Procedure TfrmSupermercado.ativarBalancaCheckOut();
begin
  Try
    //Se houver conec��o aberta(Ativa), Fecha a conec��o
    if not(dmComponentes.ACBrBAL1.Ativo) then
      dmComponentes.ACBrBAL1.Desativar;

      //Configura porta de comunica��o
    dmComponentes.ACBrBAL1.Modelo           := TACBrBALModelo(BALANCA.Modelo);     //Toledo, Filizola, ...
    dmComponentes.ACBrBAL1.Device.HandShake := TACBrHandShake(BALANCA.HandShake);  //
    dmComponentes.ACBrBAL1.Device.Parity    := TACBrSerialParity(BALANCA.Parity);
    dmComponentes.ACBrBAL1.Device.Stop      := TACBrSerialStop(BALANCA.Stop);
    dmComponentes.ACBrBAL1.Device.Data      := BALANCA.Data;
    dmComponentes.ACBrBAL1.Device.Baud      := BALANCA.Baud;
    dmComponentes.ACBrBAL1.Device.Porta     := BALANCA.Porta;

    //Ativa conex�o com a balan�a
    dmComponentes.ACBrBAL1.Ativar;

    lblBalanca.Caption:='Balan�a: '+dmComponentes.ACBrBAL1.ModeloStr;
    lblBalanca.Visible:=True;
    // Conecta com a balan�a
  except
    msgInformacao('N�o foi poss�vel comunicar com a Balan�a','');
  end;
end;

procedure TfrmSupermercado.lerBalancaCheckOut;
begin
  try
    pnlLendoPeso.Visible:=True;
    Application.ProcessMessages;
    reQuantidade.Value := dmComponentes.ACBrBAL1.LePeso( BALANCA.TimeOut );
    If reQuantidade.Value > 0 then
      reQuantidade.Font.Color:=clRed
    Else
    begin
      reQuantidade.Text:='0,000';
      msgErro('Erro ao capturar Peso','');
    end;
  finally
    pnlLendoPeso.Visible:=False;
    Application.ProcessMessages;
  end;
end;

procedure TfrmSupermercado.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  //Finaliza Componentes
  if BALANCA.Modelo > 0 then dmComponentes.ACBrBAL1.Desativar;
  if LEITOR.Leitor then dmComponentes.ACBrLCB1.Desativar;
  Action := caFree; // Libera o formul�rio da mem�ria.
  frmSupermercado := Nil; // Deixa o formul�rio vazio
end;

procedure TfrmSupermercado.pad(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  If DMCupomFiscal.dbTemp_NFCE_ItemQUANTIDADE.AsCurrency=0 then
  begin
    Dbgrid1.Canvas.Font.Color:= clRed; // coloque aqui a cor desejada
    Dbgrid1.Canvas.Font.Style:=[fsItalic];
  end;
  Dbgrid1.DefaultDrawDataCell(Rect, dbgrid1.columns[datacol].field, State);
end;

procedure TfrmSupermercado.limpaCampos;
begin
  lblMercadoria.Caption:='';
  reQuantidade.Text:='0,000';
  reVlrUnitario.Text:='0,00';
  formataQtd;
  formataVlrUni;
  fotoSM:=' ';
end;

procedure TfrmSupermercado.btnSairClick(Sender: TObject);
begin
  if edtCodigoMercadoria.Text='' then
    if msgPergunta('Sair do Frente de Caixa ?','') then
      close;
end;

procedure TfrmSupermercado.buscarImagens;
var
  I: Integer;
  SearchRec: TSearchRec;
begin
  try
    I := FindFirst(caminhoExe+'\Foto\*.jpg', 0, SearchRec);
    cdsImagens.Open;
    while I = 0 do
    begin
      cdsImagens.Append;
      cdsImagensCAMINHO.AsString:=caminhoExe+'\Foto\'+(copy(SearchRec.Name,1,Pos('.',SearchRec.Name)-1)+'.jpg');
      cdsImagens.Post;
      I := FindNext(SearchRec);
    end;
  except
    raise;
  end;
end;

procedure TfrmSupermercado.btnFinalizarCupomClick(Sender: TObject);
begin
  if (reSubTotal.Value > 0) then
  begin
    dmCupomFiscal.getTemp_NFCE;
    //Se n�o vinculou o cliente ou o valor da venda � igual ou superior a 10000 abre a tela
    if(dmCupomFiscal.dbTemp_NFCECLIFOR.Value = 0) or
      (dmCupomFiscal.dbTemp_NFCEVLR_TOTAL.Value >= 10000) then
    begin
      frmClifor:=TfrmClifor.create(Application);
      try
        case frmClifor.ShowModal of
        mrOk:begin
            dmCupomFiscal.Transaction.CommitRetaining;
            dmCupomFiscal.getTemp_NFCE;
          end;
        mrCancel:begin
            dmCupomFiscal.Transaction.RollbackRetaining;
            dmCupomFiscal.getTemp_NFCE;
            Abort;
          end;
        end;
      finally
        frmClifor.Free;
      end;
    end;
    FrmFim:=TFrmFim.create(Application);
    try
      case FrmFim.ShowModal of
      mrOk:begin
          dmCupomFiscal.Transaction.CommitRetaining;
        end;
      mrCancel:begin
          dmCupomFiscal.Transaction.RollbackRetaining;
          Abort;
        end;
      end;
    finally
      FrmFim.Free;
    end;
    getTemp_NFCE_Item;
  end;
end;

procedure TfrmSupermercado.btnCancelarCupomClick(Sender: TObject);
begin
  cancelarCupomFiscal;
end;

procedure TfrmSupermercado.btnCancelarItemClick(Sender: TObject);
begin
  if (reSubTotal.Value > 0) then
  begin
    FrmCancelaItem:=TFrmCancelaItem.Create(Application);
    FrmCancelaItem.ShowModal;
    FrmCancelaItem.Free;
    getTemp_NFCE_Item;
  end;
end;

procedure TfrmSupermercado.btnConsultarCRClick(Sender: TObject);
begin
  frmConsultaLimiteCR:=TfrmConsultaLimiteCR.create(Application);
  frmConsultaLimiteCR.ShowModal;
  frmConsultaLimiteCR.Free;
end;

procedure TfrmSupermercado.btnAbrirGavetaClick(Sender: TObject);
var
  F: textfile;
  iRetorno: Integer;
begin
  if(trim(FRENTE_CAIXA.GAV_Caminho) = '')then
  begin
    application.ProcessMessages;
    iRetorno := Le_Status();
    if iRetorno = 0 then //Erro ao comunicar com a impressora, tenta abrir a porta
    begin
      iRetorno := IniciaPorta('USB');
      if(iRetorno <> 1) then
      begin
        ShowMessage('Erro ao abrir a porta de comunica��o.');
        Abort;
      end;
    end;
    application.ProcessMessages;
    iRetorno := AcionaGaveta();
    if(iRetorno <> 1) then
      ShowMessage( 'Erro ao acionar a gaveta.' );
  end
  else
  begin                             {
    AssignFile(F,Trim(FRENTE_CAIXA.GAV_Caminho));
    Rewrite(F);
    Writeln(F,#027+#112+#000+#100'');
    CloseFile(F);                  }
    dmCupomFiscal.AcionarPeriferico(FRENTE_CAIXA.GAV_Caminho,FRENTE_CAIXA.GAV_Comando);
  end;
end;


procedure TfrmSupermercado.btnCapturarPesoClick(Sender: TObject);
begin
  if BALANCA.Modelo > 0 then
  begin
    //Se houver conec��o aberta(Ativa), Fecha a conec��o
{    if (dmComponentes.ACBrBAL1.Ativo) then
      dmComponentes.ACBrBAL1.Desativar;

    //Configura porta de comunica��o
    dmComponentes.ACBrBAL1.Modelo           := TACBrBALModelo(BALANCA.Modelo);     //Toledo, Filizola, ...
    dmComponentes.ACBrBAL1.Device.HandShake := TACBrHandShake(BALANCA.HandShake);  //
    dmComponentes.ACBrBAL1.Device.Parity    := TACBrSerialParity(BALANCA.Parity);
    dmComponentes.ACBrBAL1.Device.Stop      := TACBrSerialStop(BALANCA.Stop);
    dmComponentes.ACBrBAL1.Device.Data      := BALANCA.Data;
    dmComponentes.ACBrBAL1.Device.Baud      := BALANCA.Baud;
    dmComponentes.ACBrBAL1.Device.Porta     := BALANCA.Porta;

    //Ativa conex�o com a balan�a
    dmComponentes.ACBrBAL1.Ativar;
}
    reQuantidade.Font.Color:=clBlack;
    reQuantidade.Text:= '0,000';
    lerBalancaCheckOut;
  end;
end;

procedure TfrmSupermercado.btnPesquisaMercClick(Sender: TObject);
begin
  frmPesquisaMercadoria := TfrmPesquisaMercadoria.Create(Self);
  try
    if (frmPesquisaMercadoria.ShowModal=mrOK) then
      edtCodigoMercadoria.Text:= Trim(frmPesquisaMercadoria.qMercadoria.FieldByName('ID').AsString);
  finally
    frmPesquisaMercadoria.Free;
  end;
end;

procedure TfrmSupermercado.btnConsultaPrecoClick(Sender: TObject);
begin
  frmPesquisaCodigo := TfrmPesquisaCodigo.Create(Application);
  frmPesquisaCodigo.ShowModal;
  frmPesquisaCodigo.Free;
end;

procedure TfrmSupermercado.reVlrUnitarioEnter(Sender: TObject);
begin
  if not getAutorizacao('ALTERAR_VALOR','ALTERAR VALOR UNITARIO AO VENDER NO MERCADO',0) then
    edtCodigoMercadoria.SetFocus;
end;


procedure TfrmSupermercado.DBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  If DMCupomFiscal.dbTemp_NFCE_ItemQUANTIDADE.AsCurrency = 0 then
  begin
    Dbgrid1.Canvas.Font.Color:= clRed; //Coloque aqui a cor desejada
    //Dbgrid1.Canvas.Font.Style:=[fsItalic];
  end;
  Dbgrid1.DefaultDrawDataCell(Rect, dbgrid1.columns[datacol].field, State);
end;

procedure TfrmSupermercado.setCores;
begin
  txtMensagem.Color:=HexToTColor(PERSONALIZAR.corFundoCampo);
  txtMensagem.Font.Color:=HexToTColor(PERSONALIZAR.corFonte);
  pnlLendoPeso.Font.Color:=HexToTColor(PERSONALIZAR.corFonte);
  pnlEntradaDados.Color:=HexToTColor(PERSONALIZAR.corFundoCampo);
  pnlEntradaDados.Font.Color:=HexToTColor(PERSONALIZAR.corFonte);
  pnlDadosIF.Color:=HexToTColor(PERSONALIZAR.corFundoCampo);
  pnlDadosIF.Font.Color:=HexToTColor(PERSONALIZAR.corFonte);
  pnlBotoes.Color:=HexToTColor(PERSONALIZAR.corFundoCampo);
  reSubTotal.Color:=HexToTColor(PERSONALIZAR.corFundoCampo);
  reSubTotal.Font.Color:=HexToTColor(PERSONALIZAR.corFonte);
  alterarCorFonteLabel(pnlEntradaDados, HexToTColor(PERSONALIZAR.corFonte));
  alterarCorFonteLabel(pnlDadosIF, HexToTColor(PERSONALIZAR.corFonte));
end;

procedure TfrmSupermercado.limpaCamposMercadoria;
begin
  edtCodigoMercadoria.SetFocus;
  edtCodigoMercadoria.Clear;
  reQuantidade.Text:='0,000';
  reVlrUnitario.Text:='0,00';
  lblMercadoria.Caption:='';
end;

procedure TfrmSupermercado.FormCreate(Sender: TObject);
begin
  setCores;
  setMensagemDisplay;
end;

procedure TfrmSupermercado.ApplicationEvents1Hint(Sender: TObject);
begin
  barStatus.Panels[0].Text := ' '+Application.Hint;
end;

Procedure TfrmSupermercado.formataQtd;
begin
  //Ver com funcao formataValro uFuncoes
  Case FRENTE_CAIXA.Casas_Dec_Quantidade of
    1:begin
      reQuantidade.FormatSize:='10.1';
      reQuantidade.Text:=FormatFloat('##0.0',reQuantidade.value);
    end;
    2:begin
      reQuantidade.FormatSize:='10.2';
      reQuantidade.Text:=FormatFloat('##0.00',reQuantidade.value);
    end;
    3:begin
      reQuantidade.FormatSize:='10.3';
      reQuantidade.Text:=FormatFloat('##0.000',reQuantidade.value);
    end;
    4:begin
      reQuantidade.FormatSize:='10.4';
      reQuantidade.Text:=FormatFloat('##0.0000',reQuantidade.value);
    end;
  end;
end;

Procedure TfrmSupermercado.formataVlrUni;
begin
  Case FRENTE_CAIXA.Casas_Dec_Valor of
    1:begin
      reVlrUnitario.FormatSize:='10.1';
      reVlrUnitario.Text:=FormatFloat('##0.0',reVlrUnitario.value);
    end;
    2:begin
      reVlrUnitario.FormatSize:='10.2';
      reVlrUnitario.Text:=FormatFloat('##0.00',reVlrUnitario.value);
    end;
    3:begin
      reVlrUnitario.FormatSize:='10.3';
      reVlrUnitario.Text:=FormatFloat('##0.000',reVlrUnitario.value);
    end;
    4:begin
      reVlrUnitario.FormatSize:='10.4';
      reVlrUnitario.Text:=FormatFloat('##0.0000',reVlrUnitario.value);
    end;
  end;
end;

END.

