object frmCliforPreCadastro: TfrmCliforPreCadastro
  Left = 0
  Top = 0
  Caption = 'Informar dados para pr'#233' - cadastro'
  ClientHeight = 285
  ClientWidth = 501
  Color = clWindow
  Ctl3D = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDesktopCenter
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object pnlCabecalho: TPanel
    Left = 0
    Top = 0
    Width = 501
    Height = 42
    Align = alTop
    BevelOuter = bvNone
    Caption = 'Dados para pr'#233'-cadastro'
    Color = clHotLight
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -35
    Font.Name = 'Arial'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 42
    Width = 501
    Height = 243
    Align = alClient
    TabOrder = 1
    object Label9: TLabel
      Left = 13
      Top = 15
      Width = 102
      Height = 20
      Caption = 'Limite de Cr'#233'dito'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -16
      Font.Name = 'Arial Narrow'
      Font.Style = []
      ParentFont = False
    end
    object Label1: TLabel
      Left = 172
      Top = 15
      Width = 85
      Height = 20
      Caption = 'Dias de Prazo'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -16
      Font.Name = 'Arial Narrow'
      Font.Style = []
      ParentFont = False
    end
    object Label2: TLabel
      Left = 331
      Top = 15
      Width = 31
      Height = 20
      Caption = 'Fone'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -16
      Font.Name = 'Arial Narrow'
      Font.Style = []
      ParentFont = False
    end
    object Label3: TLabel
      Left = 13
      Top = 73
      Width = 19
      Height = 20
      Caption = 'RG'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -16
      Font.Name = 'Arial Narrow'
      Font.Style = []
      ParentFont = False
    end
    object Label7: TLabel
      Left = 331
      Top = 73
      Width = 41
      Height = 20
      Caption = 'Celular'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -16
      Font.Name = 'Arial Narrow'
      Font.Style = []
      ParentFont = False
    end
    object Label8: TLabel
      Left = 13
      Top = 133
      Width = 37
      Height = 20
      Caption = 'E-mail'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -16
      Font.Name = 'Arial Narrow'
      Font.Style = []
      ParentFont = False
    end
    object edtDiasPrazo: TEdit
      Left = 172
      Top = 35
      Width = 121
      Height = 30
      Alignment = taRightJustify
      Color = clHotLight
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -19
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      Text = '30'
      OnEnter = edtDiasPrazoEnter
      OnExit = edtLimiteExit
    end
    object edtLimite: TRealEdit
      Left = 13
      Top = 35
      Width = 121
      Height = 30
      Alignment = taRightJustify
      Color = clHotLight
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -19
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      FormatReal = fNumber
      FormatSize = '10.2'
      OnEnter = edtDiasPrazoEnter
      OnExit = edtLimiteExit
    end
    object edtFone: TEdit
      Left = 331
      Top = 35
      Width = 157
      Height = 30
      Color = clHotLight
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -19
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      OnEnter = edtDiasPrazoEnter
      OnExit = edtLimiteExit
    end
    object edtRg: TEdit
      Left = 13
      Top = 93
      Width = 157
      Height = 30
      Color = clHotLight
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -19
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
      OnEnter = edtDiasPrazoEnter
      OnExit = edtLimiteExit
    end
    object edtCelular: TEdit
      Left = 331
      Top = 93
      Width = 157
      Height = 30
      Color = clHotLight
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -19
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 4
      OnEnter = edtDiasPrazoEnter
      OnExit = edtLimiteExit
    end
    object edtEmail: TEdit
      Left = 13
      Top = 153
      Width = 475
      Height = 30
      Color = clHotLight
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -19
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 5
      OnEnter = edtDiasPrazoEnter
      OnExit = edtLimiteExit
    end
    object pnlFinalizar: TPanel
      Left = 1
      Top = 201
      Width = 499
      Height = 41
      Align = alBottom
      BevelOuter = bvNone
      Color = clHotLight
      ParentBackground = False
      TabOrder = 6
      object btnSalvar: TBitBtn
        Left = 774
        Top = 6
        Width = 53
        Height = 26
        Caption = 'Salvar'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        NumGlyphs = 2
        ParentFont = False
        TabOrder = 0
        Visible = False
      end
      object pnlFinalizarOrganiza: TPanel
        Left = 148
        Top = 3
        Width = 186
        Height = 36
        BevelOuter = bvNone
        ParentBackground = False
        ParentColor = True
        TabOrder = 1
        object btnFinalizar: TSpeedButton
          Left = 12
          Top = -1
          Width = 88
          Height = 37
          Hint = 'Pressione F1 ou clique aqui para Finalizar o Cupom Fiscal'
          Caption = 'Prosseguir'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Glyph.Data = {
            B6040000424DB604000000000000360000002800000018000000100000000100
            18000000000080040000120B0000120B00000000000000000000E1E1E1979696
            91908F9291908A898881807F918F8F9291909291909291909291909291909291
            9092919091908F8483828281808A898893919173717068666591908F979696E1
            E1E1A5A4A4E8E6E6E0DFDEDFDEDEDFDEDEE1E0E0DFDEDEDFDEDEDFDEDEDFDEDE
            DFDEDEDFDEDEDFDEDEDFDEDEDFDEDEDFDEDEDFDEDEDFDEDEDFDEDEDFDEDEDFDE
            DEE0DFDEE7E6E6A4A4A3A3A2A2F6F6F7D7D6D6C8C8C6C9C9C8CCCCCBC9C9C8C9
            C9C8C9C9C8C9C9C8C9C9C8C9C9C8C9C9C8C9C9C8C9C9C8C9C9C8C9C9C8C9C9C8
            C9C9C8C9C9C8C8C8C6D7D6D6F6F6F6A2A1A0A1A0A0EEEEEEF2F3F3FBFBFBFAFA
            FAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFA
            FAFAFAFAFAFAFAFAFAFAFAFAFAFAFBFBFBF2F2F2EFEEEEA1A0A0A2A1A0EAEAEA
            F3F3F3FFFFFFFDFDFDFDFDFDC1C2C2868787FDFDFDFDFDFDFDFDFDFDFDFDFDFD
            FDFDFDFDDFDFDF868787DFDFDFFDFDFDFDFDFDFDFDFDFFFFFFF1F0F0EBEAE9A2
            A1A0A2A2A1E7E7E6F1F1F1FBFBFBF8F8F8F8F8F88384840F1111F9F9F9F9F9F9
            F9F9F9F9F9F9F8F8F8F8F8F8BEBFBF0F1111BEBEBEF9F8F8F9F9F9F9F9F9FCFC
            FCEFEEEEE7E6E6A2A2A1A3A2A2E5E4E3EFEEEDF8F8F7F5F4F3F7F6F78283830F
            1111F5F5F4F7F6F6F6F6F6F6F6F6F6F6F5F5F5F4BBBCBB0F1111BCBCBCF4F4F4
            F6F5F5F7F6F6FAF9F9ECECECE3E3E2A3A2A2A3A3A2E1E1E0EBEBEBF5F5F4F2F1
            F1F6F5F58282820F1111BBBABABBBBBBBBBABAD8D8D8F5F4F4F4F4F4BBBBBB0F
            1111BBBBBBF4F3F3F4F4F3F4F2F2F7F7F6EAEAEAE0E0DFA3A3A2A4A3A3DDDDDD
            E9E8E8F5F4F4F4F4F4F6F6F58282820F11110F11110F11110F1111818282F5F5
            F4F5F4F3BBBBBB0F1111BBBBBBF4F4F3F5F4F4F2F1F1F5F4F4E8E8E8DCDDDCA4
            A3A3A4A3A3DCDBDAEAE9E9F6F5F5F4F4F3F5F5F48182820F1111F4F3F3F4F4F3
            F2F2F1F3F3F2F5F5F42B2C2C5657570F1111BBBBBAF3F3F2F4F3F3F0EFEFF2F1
            F1E6E6E5DAD9D8A4A4A3A5A4A3D9D8D7E8E8E7F5F5F4F4F3F2F5F5F48182820F
            1111B9B9B9BABAB9B8B8B8C6C8C7F4F4F47273730F11110F1111BABABAF2F2F1
            F2F1F1EEEEEDEFEFEEE4E3E3D7D5D5A5A4A4A5A5A4D5D3D3E4E4E3F2F2F1EFEF
            EEF3F3F28081810F11110F11110F11110F11110F1111F2F2F2F0EFEE9B9B9A0F
            1111B8B8B8EFEFEEEEEDEDEAE9E8ECECEBE2E2E1D3D2D2A5A5A4A6A5A5CFCECD
            E1E0E0EEEEEDEDECEBEEEDEDE0DFDFB4B4B3B2B3B2B3B3B2B1B1B0C2C1C0EEEE
            EDEBEAE9EAE9E8C4C4C4DEDEDDEAEAE9E9E8E7E7E6E5ECEBEADFDFDECFCECDA6
            A5A5A6A6A5D5D4D4ECEBEBF5F5F5F2F2F2F2F1F1EFEFEFEFEFEFF1F0F0F3F3F3
            F4F4F4F1F1F2EFEFEFEFEFEFF0F0F0F0F0EFEFEFEFF0F0EFF2F1F1F4F4F4F6F6
            F6ECEBEBD5D4D4A6A6A5AFAFAEE9E9E8F0F0F0EBEBEBECEBEBEDEDEDECECECEC
            ECECECEBEBEBEBEBEBEBEBECECEBECECECECECECECEBEBECECECECECECECEBEB
            ECEBEBEBEBEBEBEBEBF0F0F0E9E9E8AFAFAEE1E1E1CECDCCD2D1D0D1D0CECDCD
            CBCDCCCAD1D0CED1D0CED1D0CED1D0CED1D0CED1D0CED1D0CED1D0CED1D0CED1
            D0CED1D0CED1D0CED1D0CEC8C7C5C5C4C2D2D1D0CECDCCE1E1E1}
          Layout = blGlyphTop
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          Spacing = 2
          OnClick = btnFinalizarClick
        end
        object btnCancelar: TSpeedButton
          Left = 99
          Top = -1
          Width = 88
          Height = 37
          Hint = 'Pressione Esc ou clique aqui para fechar o Frente de Caixa'
          Caption = 'Cancelar'
          Glyph.Data = {
            B6040000424DB604000000000000360000002800000018000000100000000100
            18000000000080040000120B0000120B00000000000000000000E1E1E1979696
            91908F9291908A898881807F918F8F9291909291909291909291909291909291
            9092919091908F8483828281808A898893919173717068666591908F979696E1
            E1E1A5A4A4E8E6E6E0DFDEDFDEDEDFDEDEE1E0E0DFDEDEDFDEDEDFDEDEDFDEDE
            DFDEDEDFDEDEDFDEDEDFDEDEDFDEDEDFDEDEDFDEDEDFDEDEDFDEDEDFDEDEDFDE
            DEE0DFDEE7E6E6A4A4A3A3A2A2F6F6F7D7D6D6C8C8C6C9C9C8CCCCCBC9C9C8C9
            C9C8C9C9C8C9C9C8C9C9C8C9C9C8C9C9C8C9C9C8C9C9C8C9C9C8C9C9C8C9C9C8
            C9C9C8C9C9C8C8C8C6D7D6D6F6F6F6A2A1A0A1A0A0EEEEEEF2F3F3FBFBFBFAFA
            FAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFA
            FAFAFAFAFAFAFAFAFAFAFAFAFAFAFBFBFBF2F2F2EFEEEEA1A0A0A2A1A0EAEAEA
            F3F3F3C3C3C3868787868787868787868787868787C1C2C2DFDFDF9495957778
            78949595FDFDFDFDFDFDDFDFDF686969A3A4A4EEEEEEFFFFFFF1F0F0EBEAE9A2
            A1A0A2A2A1E7E7E6F1F1F18586860F1111494A4A494A4A494A4A494A4AA1A2A2
            494A4A1D1F1F494A4A0F1111848585AFB0B00F11113A3C3C1D1F1F494A4AFCFC
            FCEFEEEEE7E6E6A2A2A1A3A2A2E5E4E3EFEEED8384840F1111F7F6F7F6F6F5F5
            F5F4F5F5F4F7F6F66566669FA0A09FA09F1D1E1E5658576566663A3B3BF4F4F4
            9F9F9F2B2D2DDDDCDCECECECE3E3E2A3A2A2A3A3A2E1E1E0EBEBEB8283820F11
            11BCBBBBBBBBBBBBBBBBBBBABAF4F4F47373730F11110F11112B2D2DC9C9C948
            4949565757F4F3F3F4F4F3F4F2F2F7F7F6EAEAEAE0E0DFA3A3A2A4A3A3DDDDDD
            E9E8E88282820F11110F11110F11110F11110F1111F4F4F40F1111565757CACA
            C94849499E9F9F5757572B2D2DD7D8D78282821C1E1EF5F4F4E8E8E8DCDDDCA4
            A3A3A4A3A3DCDBDAEAE9E98283830F1111BBBBBBBBBBBBBABABAC9C8C8F4F4F3
            7273730F11110F11111C1E1EC9C8C8C9CAC91C1E1E0F11110F1111717272F2F1
            F1E6E6E5DAD9D8A4A4A3A5A4A3D9D8D7E8E8E78283820F1111BBBBBBBBBABAB9
            BAB9B9B9B9E5E4E3F1F0F0B8BAB9BBBBBBD6D6D5F2F1F1F4F4F4F3F3F2B9BAB9
            D6D5D5EEEEEDEFEFEEE4E3E3D7D5D5A5A4A4A5A5A4D5D3D3E4E4E38081810F11
            110F11110F11110F11110F1111B6B7B6EBEAEAEEEEEDF2F2F2F0EFEEEFEEEDF3
            F2F2F1F0F0EFEFEEEEEDEDEAE9E8ECECEBE2E2E1D3D2D2A5A5A4A6A5A5CFCECD
            E1E0E0D2D2D1B5B5B4B7B6B6B6B6B6B4B4B3B2B3B2DCDBDBE7E6E5EBEAE9EEEE
            EDEBEAE9EAE9E8EEEEEDECECEBEAEAE9E9E8E7E7E6E5ECEBEADFDFDECFCECDA6
            A5A5A6A6A5D5D4D4ECEBEBF5F5F5F2F2F2F2F1F1EFEFEFEFEFEFF1F0F0F3F3F3
            F4F4F4F1F1F2EFEFEFEFEFEFF0F0F0F0F0EFEFEFEFF0F0EFF2F1F1F4F4F4F6F6
            F6ECEBEBD5D4D4A6A6A5AFAFAEE9E9E8F0F0F0EBEBEBECEBEBEDEDEDECECECEC
            ECECECEBEBEBEBEBEBEBEBECECEBECECECECECECECEBEBECECECECECECECEBEB
            ECEBEBEBEBEBEBEBEBF0F0F0E9E9E8AFAFAEE1E1E1CECDCCD2D1D0D1D0CECDCD
            CBCDCCCAD1D0CED1D0CED1D0CED1D0CED1D0CED1D0CED1D0CED1D0CED1D0CED1
            D0CED1D0CED1D0CED1D0CEC8C7C5C5C4C2D2D1D0CECDCCE1E1E1}
          Layout = blGlyphTop
          ParentShowHint = False
          ShowHint = True
          Spacing = 2
          OnClick = btnCancelarClick
        end
      end
    end
  end
end
