object frmAutorizacao: TfrmAutorizacao
  Left = 491
  Top = 255
  BorderIcons = [biHelp]
  Caption = 'Autoriza'#231#227'o'
  ClientHeight = 188
  ClientWidth = 493
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 493
    Height = 188
    Align = alClient
    Color = clBtnHighlight
    TabOrder = 0
    object Label1: TLabel
      Left = 16
      Top = 70
      Width = 53
      Height = 20
      Caption = 'Senha'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label2: TLabel
      Left = 16
      Top = 10
      Width = 63
      Height = 20
      Caption = 'Usu'#225'rio'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel
      Left = 200
      Top = 10
      Width = 103
      Height = 20
      Caption = 'Justificativa*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object edtSenha: TMaskEdit
      Left = 15
      Top = 91
      Width = 166
      Height = 26
      CharCase = ecUpperCase
      Ctl3D = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      MaxLength = 6
      ParentCtl3D = False
      ParentFont = False
      PasswordChar = '*'
      TabOrder = 1
      Text = ''
    end
    object BitBtn2: TBitBtn
      Left = 250
      Top = 131
      Width = 75
      Height = 25
      Caption = 'Fechar'
      TabOrder = 4
      OnClick = BitBtn2Click
    end
    object edtUsuario: TEdit
      Left = 15
      Top = 32
      Width = 166
      Height = 26
      CharCase = ecUpperCase
      Ctl3D = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 0
    end
    object BitBtn1: TBitBtn
      Left = 167
      Top = 131
      Width = 75
      Height = 25
      Caption = 'OK'
      TabOrder = 3
      OnClick = BitBtn1Click
    end
    object mJustificativa: TMemo
      Left = 200
      Top = 32
      Width = 273
      Height = 85
      Ctl3D = False
      ParentCtl3D = False
      TabOrder = 2
      OnKeyDown = mJustificativaKeyDown
    end
    object StatusBar1: TStatusBar
      Left = 1
      Top = 168
      Width = 491
      Height = 19
      Panels = <>
    end
  end
end
