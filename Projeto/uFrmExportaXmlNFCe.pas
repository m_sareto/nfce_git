unit uFrmExportaXmlNFCe;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, StdCtrls, Buttons, Mask, ExtCtrls, FileCtrl, ZipMstr,
  IdAntiFreezeBase, IdAntiFreeze, IdIOHandler, IdIOHandlerSocket,
  IdSSLOpenSSL, IdComponent, IdTCPConnection, IdTCPClient, IdMessageClient,
  IdSMTP, IdBaseComponent, IdMessage, pcnAuxiliar, ACBrUtil,
  IdExplicitTLSClientServerBase, IdSMTPBase, System.StrUtils, System.DateUtils;

type
  TfrmExportaXmlNFCe = class(TForm)
    rgRotina: TRadioGroup;
    MaskEdit1: TMaskEdit;
    MaskEdit2: TMaskEdit;
    edtDiretorio: TEdit;
    btnOK: TBitBtn;
    btnDiretorio: TBitBtn;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    StatusBar1: TStatusBar;
    SpeedButton1: TSpeedButton;
    ZipMaster1: TZipMaster;
    mensagem: TIdMessage;
    cnxSMTP: TIdSMTP;
//    sslsocket: TIdSSLIOHandlerSocket;
    IdAntiFreeze1: TIdAntiFreeze;
    OpenDialog1: TOpenDialog;
    procedure rgRotinaClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnDiretorioClick(Sender: TObject);
    procedure btnOKClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    Function conectaServidorSMTP():Boolean;
    Function enviaEmail(destinatario, cc, cco, assunto : String; corpoMensagem : TStrings) : Boolean;
    procedure edtDiretorioChange(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmExportaXmlNFCe: TfrmExportaXmlNFCe;

implementation

uses uDMCupomFiscal, uFrmPrincipal, uDMComponentes, uFrmNFCe, uFuncoes, mimemess;

const
  SELDIRHELP = 1000;

{$R *.dfm}

procedure TfrmExportaXmlNFCe.rgRotinaClick(Sender: TObject);
begin
  case(rgRotina.ItemIndex)of
  0:begin
    Label3.Caption := 'Data de';
    MaskEdit1.EditMask := '!99/99/00;1;_';
    //MaskEdit1.Text     := DateToStr(Date-30);
    MaskEdit2.EditMask := '!99/99/00;1;_';
    //MaskEdit2.Text     := DateToStr(Date);
  end;
  1:begin
    Label3.Caption := 'N�mero de';
    MaskEdit1.EditMask := '';
    MaskEdit1.Text     := '1';
    MaskEdit2.EditMask := '';
    MaskEdit2.Text     := '999999';
  end;
  end;
end;

procedure TfrmExportaXmlNFCe.FormShow(Sender: TObject);
var data:TDate;
    ano,mes,dia:Word;
begin
  rgRotinaClick(Action);

  data:=IncMonth(Now,-1);
  DecodeDate(data,ano,mes,dia);
  dia:=01;
  data:=EncodeDate(ano,mes,dia);
  MaskEdit1.Text:=DateToStr(data);

  DecodeDate(data,ano,mes,dia);
  dia:=DaysInAMonth(ano,mes);
  data:=EncodeDate(ano,mes,dia);
  MaskEdit2.Text:=DateToStr(data);
  edtDiretorio.Text := 'C:\Elinfo\Temp\NFCe\';
end;

procedure TfrmExportaXmlNFCe.btnDiretorioClick(Sender: TObject);
var
  Diretorio : String;
begin
  Diretorio := ExtractFileDir(application.ExeName);
  if SelectDirectory(Diretorio, [sdAllowCreate, sdPerformCreate, sdPrompt],SELDIRHELP) then
    edtDiretorio.Text := Diretorio;
end;

procedure TfrmExportaXmlNFCe.btnOKClick(Sender: TObject);
var
  Linha, xml, chave, Diretorio: String;
  Texto : TStringList;
  count : Integer;
begin
  btnok.Enabled        := False;
  btnDiretorio.Enabled := False;
  try
    with dmCupomFiscal.dbQuery2 do
    begin
      Active := False;
      SQL.Clear;
      SQL.Add('select x.ID_ECF from est_ecf_xml x '+
              'left outer join est_ecf e on e.id = x.id_ecf ');
      if(rgRotina.ItemIndex=1) then
      begin
        SQL.Add('where (e.Numero >= :pnumI) and (e.Numero <= :pnumF) ');
        ParamByName('pnumI').AsInteger := StrToInt(MaskEdit1.text);
        ParamByName('pnumF').AsInteger := StrToInt(MaskEdit2.Text);
      end
      else
      begin
        SQL.Add('where cast(e.Data as Date) >= :pdataini and cast(e.Data as Date) <= :pdataFim ');
        ParamByName('pdataIni').AsDate := StrToDate(MaskEdit1.Text);
        ParamByName('pdataFim').AsDate := StrToDate(MaskEdit2.Text);
      end;

      SQL.add('and ((x.cstat=0) or (x.cstat is null) or '+
              '(x.cstat between 201 and 299) or '+
              '(x.cstat between 304 and 999) or (x.cstat = 1))');
      Active := True;
      First;
      if( not dmCupomFiscal.dbQuery2.IsEmpty)then
      begin
        ShowMessage('Existem NFC-es pendentes.'+sLineBreak+'Para a finaliza��o deste processo, � necess�rio que as mesmas sejam validadas.'+sLineBreak+'Em caso de d�vidas, entre em contato com o suporte!');
        abort;
      end;
    end;



    if(edtDiretorio.Text = '')then
    begin
      msgInformacao('N�o foi selecionado o diret�rio de Destino', 'Informa��o');
      edtDiretorio.SetFocus;
      Abort;
    end;
    Diretorio:=edtDiretorio.Text+FormatDateTime('yyyymmdd', Now)+'\XMLS\';
    if not DirectoryExists(ExtractFileDir(Diretorio)) then
        ForceDirectories(ExtractFileDir(Diretorio));

    dmCupomFiscal.dbQuery1.Active := False;
    dmCupomFiscal.dbQuery1.Sql.Clear;
    dmCupomFiscal.dbQuery1.sql.Add('select xml.xml_retorno, xml.xml_Retorno_Can, xml.XML_RETORNO_INU, '+
                                   'e.status, e.Numero, e.caixa, e.Chave_NFCe, e.Data, '+
                                   'e.Modelo_Doc, e.Serie, e.TPEMIS  from est_ecf_Xml xml '+
                                   'left outer join est_ecf e on e.id = xml.id_ecf ');
    if(rgRotina.ItemIndex = 1)then
    begin
      dmCupomFiscal.dbQuery1.SQL.Add('where (e.Numero >= :pnumI) and (e.Numero <= :pnumF) ');
      dmCupomFiscal.dbQuery1.ParamByName('pnumI').AsInteger := StrToInt(MaskEdit1.text);
      dmCupomFiscal.dbQuery1.ParamByName('pnumF').AsInteger := StrToInt(MaskEdit2.Text);
    end
    else
    begin
      dmCupomFiscal.dbQuery1.SQL.Add('where cast(e.Data as Date) >= :pdataini and cast(e.Data as Date) <= :pdataFim ');
      dmCupomFiscal.dbQuery1.ParamByName('pdataIni').AsDate := StrToDate(MaskEdit1.Text);
      dmCupomFiscal.dbQuery1.ParamByName('pdataFim').AsDate := StrToDate(MaskEdit2.Text);
    end;
    dmCupomFiscal.dbQuery1.SQL.Add('and (trim(xml.XML_RETORNO) is not null or trim(xml.XML_RETORNO_INU) is not null) '+
                                   'and (e.Modelo_Doc = ''65'') and (e.Status = ''X'' or e.Status = ''C'' or e.Status = ''I'')');
    dmCupomFiscal.dbQuery1.Active := true;
    if(dmCupomFiscal.dbQuery1.RecordCount = 0)then
    begin
      ShowMessage('N�o existem arquivos a serem exportados!');
      Abort;
    end;
    dmCupomFiscal.dbQuery1.First;
    Texto:=TStringList.Create;
    count := 0;
    while not dmCupomFiscal.dbQuery1.Eof do
    begin
      Texto.Clear;
      Linha := '';
      if(dmCupomFiscal.dbQuery1.FieldByName('Status').AsString = 'X')Then
        Linha := dmCupomFiscal.dbQuery1.fieldByName('XML_RETORNO').Value
      else
        if(dmCupomFiscal.dbQuery1.FieldByName('Status').AsString = 'C')Then
          Linha := dmCupomFiscal.dbQuery1.fieldByName('XML_RETORNO_CAN').Value
        else
          Linha := dmCupomFiscal.dbQuery1.fieldByName('XML_RETORNO_INU').Value;
      Texto.Add(Linha);
      if(Trim(dmCupomFiscal.dbQuery1.fieldByName('Chave_NFCe').AsString) = '')then
      begin
        Chave := MontaChaveAcessoNFe_v2(
                   UFparaCodigo(frmNFCe.qConfigNFCeUF.Value),      //Codigo UF
                   dmCupomFiscal.dbQuery1.fieldByName('Data').AsDateTime,            //Data
                   OnlyNumber(frmNFCe.qConfigNFCeCNPJ.Value),      //CNPJ
                   65,  //Modelo Doc
                   dmCupomFiscal.dbQuery1.fieldByName('Serie').AsInteger,       //Serie
                   dmCupomFiscal.dbQuery1.fieldByName('Numero').AsInteger, //Numero NF
                   dmCupomFiscal.dbQuery1.fieldByName('TPEMIS').AsInteger,//Tipo Emissao
                   dmCupomFiscal.dbQuery1.fieldByName('Numero').AsInteger);//Codigo Numerico
        if(dmCupomFiscal.dbQuery1.FieldByName('Status').AsString = 'X')Then
          Texto.SaveToFile(Diretorio+Chave+'.xml')
        else
          if(dmCupomFiscal.dbQuery1.FieldByName('Status').AsString = 'C')Then
            Texto.SaveToFile(Diretorio+Chave+'-Can.xml')
          else
            Texto.SaveToFile(Diretorio+Chave+'-Inu.xml');
      end
      else
      begin
        if(dmCupomFiscal.dbQuery1.FieldByName('Status').AsString = 'X')Then
          Texto.SaveToFile(Diretorio+dmCupomFiscal.dbQuery1.fieldByName('Chave_NFCe').Text+'.xml')
        else
          if(dmCupomFiscal.dbQuery1.FieldByName('Status').AsString = 'C')Then
            Texto.SaveToFile(Diretorio+dmCupomFiscal.dbQuery1.fieldByName('Chave_NFCe').Text+'-Can.xml')
          else
            Texto.SaveToFile(Diretorio+dmCupomFiscal.dbQuery1.fieldByName('Chave_NFCe').Text+'-Inu.xml');
      end;
      dmCupomFiscal.dbQuery1.Next;
      Inc(count);
    end;
    //zipa arquivo gerado
    ZipMaster1.ZipFileName := ExtractFileDir(Diretorio)+StringReplace(datetoStr(Date),'/','',[rfReplaceAll])+'-NFCe.zip';   // nome do arquivo compactado
    zipMaster1.TempDir     := extractfilepath(paramstr(0));
    zipMaster1.ExtrBaseDir := extractfilepath(paramstr(0));
    ZipMaster1.FSpecArgs.Add(Diretorio+'*.*');     // adiciona o primeiro arquivo
    ZipMaster1.Add;
    ShowMessage('Sucesso ao exportar arquivos! ' + IntToStr(count) + ' Arquivos exportados!');
    LimpaPastas(Diretorio);
  finally
    btnOK.Enabled        := True;
    btnDiretorio.Enabled := True;
  end;
end;

procedure TfrmExportaXmlNFCe.SpeedButton1Click(Sender: TObject);
var emailPara, Diretorio : string;
    emailCC   : TStrings;
    iRetorno  : Integer;
    SSL, TLS  : Boolean;
begin
  try
    SpeedButton1.Enabled := False;
    btnOK.Enabled := False;
    Try
      dmCupomFiscal.dbQuery1.Active := false;
      dmCupomFiscal.dbQuery1.SQL.clear;
      dmCupomFiscal.dbQuery1.sql.Add('select e.* from Email e '+
                                     'left outer join ccustos cc on cc.local = e.id '+
                                     'where cc.id = :pCC');
      dmCupomFiscal.dbQuery1.ParamByName('pCC').AsInteger := FRENTE_CAIXA.CCusto;
      dmCupomFiscal.dbQuery1.Active := True;
      if not(dmCupomFiscal.dbQuery1.IsEmpty)then
      begin
        //Pega endere�o e-mail que esta no destinatario
        StatusBar1.Panels[0].Text := 'Aguardando email para envio...';
        emailPara := dmCupomFiscal.dbQuery1.FieldByName('Destinatario').asString;
        if (InputQuery('Enviar E-mail', 'Endere�o do e-mail de destino', emailPara)) then
        begin
          If Length(emailPara)<=0 then
            msgInformacao('Endere�o de e-mail n�o informado!','')
          else
          begin
            emailCC:=TStringList.Create;
            StatusBar1.Panels[0].Text := 'Aguarde! Configurando e-mail para envio...';
            Application.ProcessMessages;
            if (Pos(';', emailPara) > 0) then //Varios endere�o para envio
            begin
              iRetorno := ExtractStrings([';'],[' '],PChar(emailPara),emailCC);
              if (iRetorno > 1)then
              begin
                emailPara := emailCC.Strings[0];
                emailCC.Delete(0);
              end;
            end
            else //Apenas um endere�o para envio
              emailCC.Add(emailPara);

            if(dmCupomFiscal.dbQuery1.FieldByName('SSL').Value = 'S')Then
              SSL := True
            else
              SSL := False;

            if(dmCupomFiscal.dbQuery1.FieldByName('TLS').Value = 'S')Then
              TLS := True
            else
              TLS := False;

            Sleep(500);
            dmComponentes.ACBrMail1.Clear;
            dmComponentes.ACBrMail1.From     := dmCupomFiscal.dbQuery1.FieldByName('EMAIL').AsString;
            dmComponentes.ACBrMail1.FromName := dmCupomFiscal.dbQuery1.FieldByName('NOME').AsString;
            dmComponentes.ACBrMail1.Host     := dmCupomFiscal.dbQuery1.FieldByName('SMTP_HOST').AsString;  //'smtp.gmail.com';
            dmComponentes.ACBrMail1.Username := dmCupomFiscal.dbQuery1.FieldByName('SMTP_USUARIO').AsString;
            dmComponentes.ACBrMail1.Password := dmCupomFiscal.dbQuery1.FieldByName('SMTP_SENHA').AsString;
            dmComponentes.ACBrMail1.Port     := dmCupomFiscal.dbQuery1.FieldByName('SMTP_PORTA').AsString;  //465 troque pela porta do seu servidor smtp
            dmComponentes.ACBrMail1.AddAddress(Trim(emailPara),''); // '' Pode colocar um nome para o Destinatario
            // ACBrMail1.AddBCC('um_email'); // opcional
            dmComponentes.ACBrMail1.Subject  := 'XML NFCe';  // assunto
            dmComponentes.ACBrMail1.SetSSL   := SSL;
            dmComponentes.ACBrMail1.SetTLS   := TLS;
            dmComponentes.ACBrMail1.Priority := MP_unknown;
            dmComponentes.ACBrMail1.AltBody.Text := 'Segue em anexo XMLs';
            Diretorio:=edtDiretorio.Text+FormatDateTime('yyyymmdd', Now)+'\XMLS\';
            if not FileExists(ExtractFileDir(Diretorio)+StringReplace(datetoStr(Date),'/','',[rfReplaceAll])+'-NFCe.zip') then
            begin
              StatusBar1.Panels[0].Text := 'Aguardando arquivo para anexar...';
              Application.ProcessMessages;
              msgInformacao('Arquivo n�o encontrado!','Aten��o');
              OpenDialog1.Title := 'Selecione o arquivo para envio';
              OpenDialog1.DefaultExt := '*.rar';
              OpenDialog1.Filter := 'Arquivos Zip(*.zip)|*.zip|Todos os Arquivos (*.*)|*.*';
              OpenDialog1.InitialDir := ExtractFileDir(edtDiretorio.text);
              if(OpenDialog1.Execute)then
              begin
                try
                  StatusBar1.Panels[0].Text := 'Aguarde! Enviando E-mail...';
                  Application.ProcessMessages;
                  dmComponentes.ACBrMail1.AddAttachment(OpenDialog1.FileName,'');
                  dmComponentes.ACBrMail1.Send(False);
                  msgInformacao('E-Mail enviado com sucesso.','Informa��o');
                except
                  on E:Exception do
                    logErros(Sender, caminholog, 'Erro ao enviar e-mail','Erro ao enviar e-mail','S',E);
                end;
              end;
            end
            else
            begin
              try
                dmComponentes.ACBrMail1.AddAttachment(ExtractFileDir(Diretorio)+StringReplace(datetoStr(Date),'/','',[rfReplaceAll])+'-NFCe.zip','');
                StatusBar1.Panels[0].Text := 'Aguarde! Enviando E-mail...';
                Application.ProcessMessages;
                dmComponentes.ACBrMail1.Send(false);
                application.ProcessMessages;
                ShowMessage('Envio de e-mail conclu�do!');
              except
                on E:Exception do
                  logErros(Sender, caminholog, 'Erro ao enviar e-mail','Erro ao enviar e-mail','S',E);
              end;
            end;
          end;
        end;
      end
      else
      begin
        msgInformacao('Configura��es de envio de email n�o definidas!', '');
      end;
    except
      on E:Exception do
        logErros(Sender,caminhoLog,'Erro ao enviar E-mail','Erro ao enviar E-mail','S',E);
    end;
  finally
    StatusBar1.Panels[0].Text := '';
    SpeedButton1.Enabled := True;
    btnOK.Enabled := True;
  end;
end;

Function TfrmExportaXmlNFCe.conectaServidorSMTP():Boolean;
begin
  //Verifica se h� conex�o com internet
  if not IsConnected then
  begin
    Result := False;
    Abort;
  end;
  Application.ProcessMessages;
  cnxSMTP.Host     := dmCupomFiscal.dbQuery1.fieldByName('Smtp_Host').AsString;
  cnxSMTP.Password := dmCupomFiscal.dbQuery1.fieldByName('Smtp_Senha').AsString;
  cnxSMTP.Username := dmCupomFiscal.dbQuery1.fieldByName('Smtp_Usuario').AsString;
  cnxSMTP.Port     := dmCupomFiscal.dbQuery1.fieldByName('Smtp_Porta').AsInteger;
  //-- requer autenticacao
  if(dmCupomFiscal.dbQuery1.fieldByName('Autenticacao').AsString = 'S')then
//    cnxSMTP.AuthenticationType := atLogin
  else
//    cnxSMTP.AuthenticationType := atNone;
  //-- conexao segura SSL
  if(dmCupomFiscal.dbQuery1.fieldByName('SSL').AsString = 'S')then
  begin
    if(cnxSMTP.Host = 'smtp.gmail.com')then
    begin
{      sslsocket.SSLOptions.Method := sslvSSLv3;
      sslsocket.SSLOptions.Mode := sslmClient;}
    end
    else
    begin
{      SSLSocket.SSLOptions.Method := sslvSSLv2;
      SSLSocket.SSLOptions.Mode   := sslmUnassigned;}
    end;
//    cnxSMTP.IOHandler := sslsocket;
  end
  else
    cnxSMTP.IOHandler := nil;
  Sleep(1000);
  Application.ProcessMessages;
  try
    cnxSMTP.Connect;
    Application.ProcessMessages;
    Result := True;
  except
    cnxSMTP.Disconnect;
    Result:=False;
  end;
end;

function TfrmExportaXmlNFCe.enviaEmail(destinatario, cc, cco, assunto : String; corpoMensagem: TStrings): Boolean;
//var txtPart, htmlPart : TIdText;
begin
  try
    //Dados para Envio do Email
    mensagem.From.Address              := dmCupomFiscal.dbQuery1.fieldByName('smtp_usuario').asString;   //Email de Origem
    mensagem.From.Name                 := dmCupomFiscal.dbQuery1.fieldByName('nome').asString; //Nome que aparecer� quando o destinat�rio verificar o e-mail
    mensagem.Recipients.EMailAddresses := destinatario; //Endere�o do destinatario Princiapl
    mensagem.CCList.EMailAddresses     := cc; //Endere�o Com Copia,tamb�m receber� o e-mail
//    mensagem.BccList.EMailAddresses    := 'suporte@elinfo.com.br'; //Endereco Com Copia Oculta, recebera mas destinatario principal nao ira ver
    mensagem.Subject                   := assunto; //Assunto do e-mail
    mensagem.Priority := mpHigh;
    mensagem.ReceiptRecipient.Text := mensagem.From.Text;
    mensagem.ContentType:='multipart/mixed';
    mensagem.Body.Assign(corpoMensagem);

    //Adiciona parte HTML ao email
{    htmlPart := TIdText.Create(mensagem.MessageParts, corpoMensagem);
    htmlPart.ContentType := 'text/html';

    txtPart := TIdText.Create(mensagem.MessageParts);
    txtPart.ContentType := 'text/plain';
  }
    cnxSMTP.Send(mensagem);
    Application.ProcessMessages;
    Sleep(1000);
    Application.ProcessMessages;
    cnxSMTP.Disconnect;
    Application.ProcessMessages;
    Result:=True;
  except
    on E:Exception do
    begin
      logErros(Self, '', 'Erro ao enviar Email', '', 'N', E);
      cnxSMTP.Disconnect;
      Result:=False;
    end;
  end;
end;

procedure TfrmExportaXmlNFCe.edtDiretorioChange(Sender: TObject);
begin
{  if DirectoryExists(ExtractFileDir(edtDiretorio.text)) then
    btnOK.Enabled := True
  else
    btnOK.Enabled := false;}
end;

procedure TfrmExportaXmlNFCe.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if(Key = Vk_Escape)then Close;
end;

end.
