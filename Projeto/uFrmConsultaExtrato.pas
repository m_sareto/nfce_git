unit uFrmConsultaExtrato;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, DB, IBCustomDataSet, IBQuery, Grids, DBGrids, StdCtrls,
  TREdit, Mask;

type
  TfrmConsultaExtrato = class(TForm)
    DBGrid1: TDBGrid;
    Panel1: TPanel;
    DataSource1: TDataSource;
    Label1: TLabel;
    mkDt_Final: TMaskEdit;
    procedure FormShow(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure mkDt_FinalExit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmConsultaExtrato: TfrmConsultaExtrato;

implementation

uses uDMCupomFiscal, uFrmLoja;

{$R *.dfm}

procedure TfrmConsultaExtrato.FormShow(Sender: TObject);
begin
  mkDt_Final.Text:=DateToStr(incMonth(Date,-1));
end;

procedure TfrmConsultaExtrato.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  If (Key=vk_escape) then Close;
end;

procedure TfrmConsultaExtrato.mkDt_FinalExit(Sender: TObject);
begin
  DMCupomFiscal.qConsulta_Ext_Cli.Active:=False;
  DMCupomFiscal.qConsulta_Ext_Cli.ParamByName('pData').AsDate:=StrToDate(mkDt_Final.Text);
  DMCupomFiscal.qConsulta_Ext_Cli.ParamByName('pCF').AsInteger:=StrToIntDef(frmLoja.edtCliente.Text,0);
  DMCupomFiscal.qConsulta_Ext_Cli.Active:=True;
  DMCupomFiscal.qConsulta_Ext_Cli.First;

  DBGrid1.SetFocus;
end;


end.

