unit uFrmConsulta_Caixa;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, IBCustomDataSet, IBQuery, StdCtrls, Mask, DBCtrls, IBSQL,
  Buttons, Tredit, ExtCtrls, ComCtrls, Grids, DBGrids, RLReport, RLPrinters,StrUtils;

type
  TFrmConsulta_Caixa = class(TForm)
    IBSQL1: TIBSQL;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    reVBruta: TRealEdit;
    reVDesconto: TRealEdit;
    reVLiquido: TRealEdit;
    reVRecebido: TRealEdit;
    reVTroco: TRealEdit;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    reVSuprimento: TRealEdit;
    reVSangria: TRealEdit;
    reVSaldo: TRealEdit;
    Panel1: TPanel;
    btConsulta: TBitBtn;
    StatusBar1: TStatusBar;
    mkDataI: TMaskEdit;
    Label9: TLabel;
    Label10: TLabel;
    edCaixa: TEdit;
    Label11: TLabel;
    edTurno: TEdit;
    Label12: TLabel;
    reVCR: TRealEdit;
    btFecha: TBitBtn;
    DataSource1: TDataSource;
    Label13: TLabel;
    mkDataF: TMaskEdit;
    btImprimir: TBitBtn;
    qCupom: TIBQuery;
    qCupomCUPOM_LT: TIBBCDField;
    qCupomCONCENTRADOR_LT: TIBBCDField;
    qCupomDiferenca: TCurrencyField;
    DataSource3: TDataSource;
    Panel2: TPanel;
    DBGrid3: TDBGrid;
    DBGrid1: TDBGrid;
    Panel3: TPanel;
    Panel4: TPanel;
    qAbastecidas: TIBQuery;
    qAbastecidasMERCADORIA: TIBStringField;
    qAbastecidasBOMBA: TIntegerField;
    qAbastecidasINI_ENCERRANTE: TIBBCDField;
    qAbastecidasFIM_ENCERRANTE: TIBBCDField;
    qAbastecidasVENDAS: TIBBCDField;
    qAbastecidasDESCRICAO: TIBStringField;
    qCupomDESCRICAO: TIBStringField;
    RLReport1: TRLReport;
    RLBand6: TRLBand;
    RLMemo2: TRLMemo;
    RLBand8: TRLBand;
    RLLabel7: TRLLabel;
    RLLabel8: TRLLabel;
    RLLabel9: TRLLabel;
    RLMemo1: TRLMemo;
    RLDraw8: TRLDraw;
    qLocal: TIBQuery;
    qLocalID: TIntegerField;
    qLocalDESCRICAO: TIBStringField;
    qLocalCNPJ: TIBStringField;
    qLocalIE: TIBStringField;
    qLocalPAIS: TIntegerField;
    qLocalMUNICIPIO: TIntegerField;
    qLocalENDERECO: TIBStringField;
    qLocalBAIRRO: TIBStringField;
    qLocalCEP: TIBStringField;
    qLocalUF: TIBStringField;
    qLocalFONE: TIBStringField;
    qLocalFAX: TIBStringField;
    qLocalNUMERO_END: TIBStringField;
    qLocalCOMPLEMENTO_END: TIBStringField;
    qLocalNOME_FANTASIA: TIBStringField;
    qLocalIM: TIBStringField;
    qLocalCNAE: TIBStringField;
    qLocalE_MAIL: TIBStringField;
    qLocalALI_SIMPLESN_ICM: TIBBCDField;
    qLocalCRT: TIntegerField;
    qLocalCALCULA_PESO: TIBStringField;
    qLocalDATA_EXP: TDateField;
    qLocalANOMES: TIntegerField;
    qLocalDATA_MOV: TDateField;
    qLocalNFS_INICIAL: TIntegerField;
    qLocalNFS_FINAL: TIntegerField;
    qLocalNFS_AUTORIZACAO: TIBStringField;
    qLocalCTE_RNTRC: TIBStringField;
    qLocalTURNO: TIBStringField;
    qLocalVLR_IRRF: TIBBCDField;
    qLocalANP_ARI: TIBStringField;
    qLocalDES_MUNI: TIBStringField;
    qLocalIMPRESSORA: TIBStringField;
    RLMemo3: TRLMemo;
    procedure btConsultaClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btFechaClick(Sender: TObject);
    procedure btImprimirClick(Sender: TObject);
    procedure qCupomCalcFields(DataSet: TDataSet);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure RLBand8BeforePrint(Sender: TObject; var PrintIt: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmConsulta_Caixa: TFrmConsulta_Caixa;

implementation

uses uDMCupomFiscal, uDMComponentes, uFuncoes, uFrmPrincipal, uRotinasGlobais;

{$R *.dfm}

procedure TFrmConsulta_Caixa.btConsultaClick(Sender: TObject);
begin
  frmPrincipal.abreFormAguarde('Aguarde...');
  try
    IBSQL1.Close;
    IBSQL1.ParamByName('pCaixa').AsString := edCaixa.Text;
    IBSQL1.ParamByName('pDataI').AsDate := StrToDate(mkDataI.Text);
    IBSQL1.ParamByName('pDataF').AsDate := StrToDate(mkDataF.Text);
    IBSQL1.ParamByName('pTurno').AsString := Trim(edTurno.Text);
    IBSQL1.ExecQuery;
  
    reVBruta.Value:=IBSQL1.FieldByName('vBruto').AsCurrency;
    reVDesconto.Value:=IBSQL1.FieldByName('vDesconto').AsCurrency;
    reVLiquido.Value:=IBSQL1.FieldByName('vLiquido').AsCurrency;
    reVRecebido.Value:=IBSQL1.FieldByName('vRecebido').AsCurrency;
    reVTroco.Value:=IBSQL1.FieldByName('vTroco').AsCurrency;
    reVSuprimento.Value:=IBSQL1.FieldByName('vSuprimento').AsCurrency;
    reVSangria.Value:=IBSQL1.FieldByName('vSangria').AsCurrency;
    reVCR.Value:=IBSQL1.FieldByName('vCRb').AsCurrency;
    reVSaldo.Value:=IBSQL1.FieldByName('vSaldo').AsCurrency;
    IBSQL1.Close;


    qAbastecidas.Active:=False;
    If StrToIntDef(edTurno.Text,0)=0 then
    begin
      qAbastecidas.ParamByName('pTurnoI').AsString:='0';
      qAbastecidas.ParamByName('pTurnoF').AsString:='ZZ';
    end
    Else
    begin
      qAbastecidas.ParamByName('pTurnoI').AsString:=Trim(edTurno.Text);
      qAbastecidas.ParamByName('pTurnoF').AsString:=Trim(edTurno.Text);
    end;
    qAbastecidas.ParamByName('pDataI').AsDate:=StrToDate(mkDataI.Text);
    qAbastecidas.ParamByName('pDataF').AsDate:=StrToDate(mkDataF.Text);
    qAbastecidas.Active:=True;
    qAbastecidas.First;


    qCupom.Active:=False;
    qCupom.ParamByName('pDataIni').AsDate:=StrToDate(mkDataI.Text);
    qCupom.ParamByName('pDataFim').AsDate:=StrToDate(mkDataF.Text);
    qCupom.Active:=True;
    qCupom.First;

  except
    on E: Exception do
    begin
      logErros(Self, caminhoLog, 'Erro ao excluir Consulta.'+e.Message, 'Erro ao excluir Consulta.', 'S', e);
    end;
  end;
  frmPrincipal.fechaFormAguarde();
end;

procedure TFrmConsulta_Caixa.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin

  if key=vk_escape then btFechaClick(action);
end;

procedure TFrmConsulta_Caixa.FormShow(Sender: TObject);
begin
  mkDataI.Text:= DateToStr(Date);
  mkDataF.Text:= DateToStr(Date);
  edCaixa.Text:= FormatFloat('0000', FRENTE_CAIXA.caixa);
end;

procedure TFrmConsulta_Caixa.btFechaClick(Sender: TObject);
begin
  Close;
end;

procedure TFrmConsulta_Caixa.btImprimirClick(Sender: TObject);
begin

  btImprimir.Enabled:=False;

  //RLMemo2.Lines.Add()

  RLMemo2.Lines.Clear;
  RLMemo2.Lines.Add('Caixa por Turno');
  RLMemo2.Lines.Add('Data:'+mkDataI.Text+'  Caixa:'+edCaixa.Text+'  Turno:'+ifthen(StrToIntDef(edTurno.Text,0)>0, edTurno.Text, 'Todos') );
  RLMemo2.Lines.Add('Venda Bruta:');
  RLMemo2.Lines.Add('Desconto:');
  RLMemo2.Lines.Add('Venda Liquida (+):');
  RLMemo2.Lines.Add('Meios de Pagamento:');
  RLMemo2.Lines.Add('Troco:');
  RLMemo2.Lines.Add('Suprimento (+):');
  RLMemo2.Lines.Add('Sangria (-):');
  RLMemo2.Lines.Add('Contas a Receber (+):');
  RLMemo2.Lines.Add('Saldo de Caixa   (=):');

  RLMemo3.Lines.Clear;
  RLMemo3.Lines.Add('');
  RLMemo3.Lines.Add('');
  RLMemo3.Lines.Add(FormatFloat('###,##0.00',reVBruta.Value));
  RLMemo3.Lines.Add(FormatFloat('###,##0.00',reVDesconto.Value));
  RLMemo3.Lines.Add(FormatFloat('###,##0.00',reVLiquido.Value));
  RLMemo3.Lines.Add(FormatFloat('###,##0.00',reVRecebido.Value));
  RLMemo3.Lines.Add(FormatFloat('###,##0.00',reVTroco.Value));
  RLMemo3.Lines.Add(FormatFloat('###,##0.00',reVSuprimento.Value));
  RLMemo3.Lines.Add(FormatFloat('###,##0.00',reVSangria.Value));
  RLMemo3.Lines.Add(FormatFloat('###,##0.00',reVCR.Value));
  RLMemo3.Lines.Add(FormatFloat('###,##0.00',reVSaldo.Value));

  RLReport1.preview();
  {
  dmComponentes.ACBrECF1.AbreRelatorioGerencial;
  dmComponentes.ACBRECF1.LinhaRelatorioGerencial(#14'Caixa por Turno');
  dmComponentes.ACBRECF1.LinhaRelatorioGerencial('Data:'+mkDataI.Text+'  Caixa:'+edCaixa.Text+'  Turno:'+edTurno.Text);
  dmComponentes.ACBRECF1.LinhaRelatorioGerencial('Venda Bruta          :'+FormatFloat('###,##0.00',reVBruta.Value));
  dmComponentes.ACBRECF1.LinhaRelatorioGerencial('Desconto             :'+FormatFloat('###,##0.00',reVDesconto.Value));
  dmComponentes.ACBRECF1.LinhaRelatorioGerencial('Venda Liquida (+)    :'+FormatFloat('###,##0.00',reVLiquido.Value));
  dmComponentes.ACBRECF1.LinhaRelatorioGerencial('Meios de Pagamento   :'+FormatFloat('###,##0.00',reVRecebido.Value));
  dmComponentes.ACBRECF1.LinhaRelatorioGerencial('Troco                :'+FormatFloat('###,##0.00',reVTroco.Value));
  dmComponentes.ACBRECF1.LinhaRelatorioGerencial('Suprimento (+)       :'+FormatFloat('###,##0.00',reVSuprimento.Value));
  dmComponentes.ACBRECF1.LinhaRelatorioGerencial('Sangria (-)          :'+FormatFloat('###,##0.00',reVSangria.Value));
  dmComponentes.ACBRECF1.LinhaRelatorioGerencial('Contas a Receber (+) :'+FormatFloat('###,##0.00',reVCR.Value));
  dmComponentes.ACBRECF1.LinhaRelatorioGerencial('Saldo de Caixa   (=) :'+FormatFloat('###,##0.00',reVSaldo.Value));
  dmComponentes.ACBRECF1.FechaRelatorio;}

  btImprimir.Enabled:=True;

end;

procedure TFrmConsulta_Caixa.qCupomCalcFields(DataSet: TDataSet);
begin
  qCupomDiferenca.Value:=qCupomCONCENTRADOR_LT.Value-qCupomCUPOM_LT.Value;
end;

procedure TFrmConsulta_Caixa.RLBand8BeforePrint(Sender: TObject;
  var PrintIt: Boolean);
begin
  qLocal.close;
  qLocal.ParamByName('pCC').value:=CCUSTO.codigo;
  qLocal.Open;
  RLLabel7.Caption    := qLocalNOME_FANTASIA.AsString;
  RLLabel8.Caption    := qLocalDESCRICAO.AsString;
  RLLabel9.Caption    := 'CNPJ:'+qLocalCNPJ.AsString + ' IE:'+qLocalIE.AsString + ' IM:'+qLocalIM.AsString;
  RLMemo1.Lines.Text  := ' '+qLocalENDERECO.AsString+' - '+qLocalNUMERO_END.AsString+', '+
                         qLocalBAIRRO.AsString+', '+qLocalDES_MUNI.AsString+', '+qLocalUF.AsString+', '+
                         qLocalCEP.AsString+', '+qLocalFONE.AsString;

  RLPrinter.PrinterName:=qLocalIMPRESSORA.Value;
end;

end.
