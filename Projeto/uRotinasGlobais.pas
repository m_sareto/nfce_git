unit uRotinasGlobais;

interface
uses SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls, Forms,
    Dialogs, Printers, DB, dbctrls, Stdctrls, Math, Variants, Mapi,
    ShellAPI, Registry, Windows, Mask, Spin, CheckLst,TlHelp32,
    Wininet, Winsock, NB30, FileCtrl, IniFiles, DateUtils,
    ACBrPAFClass, ACBrRFD, ACBrDevice, ACBrECFClass, ACBrConsts,ACBrECF, StrUtils;

type
  rMercadoriaItem=record
    codigo           : String;
    descricao        : String;
    unidade          : String;
    vlrUnitario      : Currency;
    vlrDesconto      : Currency;
    quantidade       : Currency;
    estoque          : Currency;
    icms             : string;
    cstPIS           : string;
    cstCOFINS        : string;
    aliqISS          : Currency;
    cstICMS          : string;
    cfop             : Integer;
    ncm              : string;
    subtipo_item     : string;
    percDescontoMax  : Currency;
    servico          : Boolean;
    Est_Vinculado    : String;
  end;

type
  rTabelaPreco=record
    TabPreco         : Integer;
    NomeCliente      : string;
  end;

type
  rLogin=record
    usuarioNome      : string;
    usuarioNivel     : String;
    usuarioCod       : Integer;
    usuarioFuncNome  : string;
  end;

type
  rVendedor=record
    vendedorCod      : Integer;
    vendedorNome     : string;
  end;

type
  rCCusto=record
    codigo           : Integer;
    estoqueZero      : string;
  end;

type
  rAutorizacao=record
    autorizado    : Boolean;
  end;

type
  rOperacao=record
    operarEstoque : string;
  end;

function crContasVencidas(cliente:Integer):boolean;
procedure setDadosRodape();
procedure ArredondarComponente(Componente: TWinControl; const Radius: SmallInt);
procedure setMensagemDisplay();
procedure setCaixaUso(funcionario:Integer; emUso:String);
procedure inserirTempNFCe();
function Converte(cmd: String): String;
Function crExcedeuLimite(cliente:Integer; subtotal:Currency):Boolean;
function crProtecaoCredito(cliente:Integer):Boolean;
function cancelarCupomFiscal(cancelarDireto:Boolean=False):Boolean;
function getAutorizacao(Historico, Msg: String; Valor: Currency):Boolean;
function getAutorizacaoLimiteCred(Historico, Msg: String; Valor: Currency):Boolean;
function gerarELChave():Integer;
function gerarELChave_BaseRemota():Integer;
function gerarID(pGenerator:String):Integer;
function buscarCCusto(codigo:Integer):Boolean;
function quantidadeMaximaExcedeu(quantidade:Double):Boolean;
function valorUnitarioMaximoExcedeu(valor:Double):Boolean;
function verificarEstoque(cod_mercadoria, Estoque_Vinculado, subtipo_item:String; estoque, quantidade:Double):Boolean;
function getQtdMercadoriaTempECFITem(codigo:String):Double;
function calculaJuros(taxJuroDia, valor:Currency; dataVencimento:TDateTime ): Currency;
function getSaldoCliente(id:integer; calcJuro:Boolean=True):Currency;
function getValorSubtotalTemp(caixa:integer):Real;
function possuiRegistroTempECF(caixa:integer):Boolean;
function getAutorizacaoIncondicional(Historico, Msg: String; Valor: Currency; Nivel:integer=2):Boolean;
procedure getTemp_NFCE_Item;
function ValidarNCM(const ncm:string):Boolean;
Function ValidaFrota(Cliente:Integer; Placa:String):Boolean;
function abrirGavetaBematech(Porta:String):Boolean;
function GetCalculaDataVencimento(CliFor:integer): TDate;
function GetQtdEstoqueProdVInculado(pID_Merc: String; pCCusto: Integer): Double;

var MERCADORIA  : rMercadoriaItem;
    TAB_PRECO   : rTabelaPreco;
    CCUSTO      : rCCusto;
    AUTORIZACAO : rautorizacao;
    OPERACAO    : rOperacao;
    LOGIN       : rLogin;
    VENDEDOR    : rVendedor;
    FDescricao_Vinculado: String;

implementation

uses uDMComponentes, uFrmLoja, uFrmSupermercado,uDMCupomFiscal, uFuncoes, uFrmConsultaCR,
  uFrmPrincipal, uFrmAutorizacao, uFrmConfiguracao, uFrmNFCe, uFrmPosto, IBQuery;

function buscarCCusto(codigo:Integer):Boolean;
begin
  dmCupomFiscal.dbQuery1.Active:=False;
  dmCupomFiscal.dbQuery1.SQL.Clear;
  dmCupomFiscal.dbQuery1.SQL.Add('Select ID, Descricao, ESTOQUE_ZERO FROM CCustos Where ID=:pCod');
  dmCupomFiscal.dbQuery1.ParamByName('pCod').AsInteger := codigo;
  dmCupomFiscal.dbQuery1.Active:=True;
  dmCupomFiscal.dbQuery1.First;
  If not(dmCupomFiscal.dbQuery1.Eof) then
  begin
    CCUSTO.estoqueZero := DMCupomFiscal.dbQuery1.FieldByName('Estoque_Zero').AsString;
    CCUSTO.codigo      := DMCupomFiscal.dbQuery1.FieldByName('id').AsInteger;
    Result := True
  end
  else
  begin
    Result := False;
    msgAviso('Centro de Custo '+IntToStr(codigo)+' configurado n�o encontrado!','');
  end;
end;

procedure setDadosRodape();
begin
  if frmLoja <> nil then
  begin
    frmLoja.lblNumCaixa.Caption  := 'Caixa: '+FormatFloat('0000', FRENTE_CAIXA.Caixa);
    frmLoja.lblOperador.Caption  := '&Operador: '+LOGIN.usuarioFuncNome;
    frmLoja.lblCCusto.Caption    := 'C.Custo: '+IntToStr(CCUSTO.codigo);
    frmLoja.lblTurno.Caption     := 'Turno: '+POSTO.Turno;
    frmLoja.lblData.Caption      := 'Data: '+DateToStr(Now);
  end
  else
  begin
    if frmSupermercado <> nil then
    begin
      frmSupermercado.lblNumCaixa.Caption  := 'Caixa: '+FormatFloat('0000', FRENTE_CAIXA.Caixa);
      frmSupermercado.lblOperador.Caption  := '&Operador: '+LOGIN.usuarioFuncNome;
      frmSupermercado.lblCCusto.Caption  := 'C.Custo: '+IntToStr(CCUSTO.codigo);
      frmSupermercado.lblData.Caption      := 'Data: '+DateToStr(Now);
    end
    else
    begin
      if frmPosto <> nil then
      begin
        frmPosto.lblNumCaixa.Caption  := 'Caixa: '+FormatFloat('0000', FRENTE_CAIXA.Caixa);
        frmPosto.lblOperador.Caption  := '&Operador: '+LOGIN.usuarioFuncNome;
        frmPosto.lblCCusto.Caption    := 'C.Custo: '+IntToStr(CCUSTO.codigo);
        frmPosto.lblTurno.Caption     := 'Turno: '+POSTO.Turno;
        frmPosto.lblData.Caption      := 'Data: '+DateToStr(Now);
      end;
    end;
  end;
end;

function crContasVencidas(cliente:Integer): boolean;
begin
  DMCupomFiscal.qConsulta_CR.Active:=False;
  DMCupomFiscal.qConsulta_CR.ParamByName('pCF').AsInteger:=cliente;
  DMCupomFiscal.qConsulta_CR.ParamByName('pData').AsDate:=Date;
  DMCupomFiscal.qConsulta_CR.Active:=True;
  DMCupomFiscal.qConsulta_CR.First;
  Result:=true;
  If not(DMCupomFiscal.qConsulta_CR.Eof) then
  begin
    FrmConsultaCR:=TfrmConsultaCR.create(Application);
    frmConsultaCR.ShowModal;
    frmConsultaCR.Free;{
    if msgPergunta('Cliente possui contas vencidas.'+#13#10+
                   'Deseja mesmo assim efetuar a venda?','')
    then
    begin
      if NOT(getAutorizacao('CONTA_VENCIDA','LIBERAR VENDA A PRAZO PARA CLIENTE '+IntToStr(cliente)+' COM CONTA VENCIDA',DMCupomFiscal.dbECFVLR_SUBTOTAL.value)) then
        result:=false;
    end
    else
      Result:=false;   }
  end;{
  Else
    msgInformacao('Cliente n�o possue d�bitos em Atraso!','');    }
end;


function converte(cmd: String): String;
var A : Integer ;
begin
  Result := '' ;
  For A := 1 to length( cmd ) do
  begin
     if (Ord(cmd[A]) < 32) or (Ord(cmd[A]) > 127) then
        Result := Result + '#' + IntToStr(ord( cmd[A] ))
     else
        Result := Result + cmd[A] ;
  end ;
end;

Function crExcedeuLimite(cliente:Integer; subtotal:Currency):Boolean;
var vlrDisponivel, Limite : Currency;
    dias:Integer;
begin
  if FRENTE_CAIXA.LimiteCredito then
  begin
    Result:=False;

    DMCupomFiscal.dbQuery3.Active:=False;
    DMCupomFiscal.dbQuery3.SQL.Clear;
    DMCupomFiscal.dbQuery3.SQL.Add('Select Limite From CliFor Where ID=:pID');
    DMCupomFiscal.dbQuery3.ParambyName('pID').asInteger:=Cliente;
    DMCupomFiscal.dbQuery3.Active:=True;
    DMCupomFiscal.dbQuery3.First;
    Limite:=DMCupomFiscal.dbQuery3.FieldByName('Limite').asCurrency;

{
    //Cheques
    DMCupomFiscal.dbQuery2.Active:=False;
    DMCupomFiscal.dbQuery2.SQL.Clear;
    DMCupomFiscal.dbQuery2.SQL.Add('Select Sum(Valor) as xValor From CH_Movimento'+
                                   ' Where (Status=''X'') and (Emitido_Recebido=''R'') and (Conciliado=''N'') and (CliFor=:pCF)');
    DMCupomFiscal.dbQuery2.ParamByName('pCF').AsInteger := Cliente;
    DMCupomFiscal.dbQuery2.Active:=True;
    DMCupomFiscal.dbQuery2.First;
    If not(DMCupomFiscal.dbQuery2.Eof) then
    begin

    end;
}
    // CR
    if FRENTE_CAIXA.CR_Baixa_Seletiva then
    begin
      DMCupomFiscal.dbQuery2.Active:=False;
      DMCupomFiscal.dbQuery2.SQL.Clear;
      DMCupomFiscal.dbQuery2.SQL.Add('Select Coalesce(Sum(Resta),0) as xResta From CR_Movimento Where CliFor=:pCF');
      DMCupomFiscal.dbQuery2.ParamByName('pCF').AsInteger := cliente;
      DMCupomFiscal.dbQuery2.Active:=True;
      DMCupomFiscal.dbQuery2.First;
      If not(DMCupomFiscal.dbQuery2.Eof) then
      begin
        vlrDisponivel := DMCupomFiscal.dbQuery2.FieldByName('xResta').AsCurrency;
        vlrDisponivel := (limite - vlrDisponivel);
        if vlrDisponivel < subtotal then
        begin
          if msgPergunta('N�o foi poss�vel liberar venda a prazo.'+#13#10+
                        'Limite de Cr�dito    R$: '+FormatFloat('###,##0.00',Limite)+#13+
                        'Cr�dito j� Utilizado R$: '+FormatFloat('###,##0.00',DMCupomFiscal.dbQuery2.FieldByName('xResta').AsCurrency)+#13+
                        'Saldo Dispon�vel     R$: '+FormatFloat('###,##0.00',vlrDisponivel)+#13+
                        'Venda Atual             R$: '+FormatFloat('###,##0.00',subtotal)+#13+
                        'Deseja mesmo assim efetuar a venda?','')
          then
          begin
            if getAutorizacaoLimiteCred('LIMITE_CRED','LIBERAR VENDA A PRAZO PARA CLIENTE SEM CREDITO',subtotal) then
              Result:=False
            else
              Result := True;
          end
          else
            Result:=True; //Excedeu Limite
        end;
      end;
    end
    else
    begin
      //-- Verifica se faz mais de 30 dias que n�o existe cr�dito
      dmCupomFiscal.dbQuery2.SQL.Clear;
      dmCupomFiscal.dbQuery2.SQL.Add('Select Max(Data_Ope) as Data_Credito'+
                      ' From Cr_Movimento CR '+
                      ' LEFT OUTER JOIN Formas_Pagamento Fp ON (Fp.ID=Cr.Forma_Pag)'+
                      ' Where (Fp.CR_DC=''C'') and (CR.CliFor=:pCliFor)');
      dmCupomFiscal.dbQuery2.ParamByName('pCliFor').AsInteger := cliente;
      dmCupomFiscal.dbQuery2.Active:=True;
      dmCupomFiscal.dbQuery2.First;
      If Trim(dmCupomFiscal.dbQuery2.FieldByName('Data_Credito').Text)>'' then
          dias := Date - DMCupomFiscal.dbQuery2.FieldByName('Data_Credito').AsVariant;

      //---Conta Corrente
      DMCupomFiscal.dbQuery2.Active:=False;
      DMCupomFiscal.dbQuery2.SQL.Clear;
      DMCupomFiscal.dbQuery2.SQL.Add('Select Sum(case when (F.CR_DC=''C'') then Valor else -Valor end) as TSaldo'+
                                     ' From CR_Movimento CR'+
                                     ' Left Outer join Formas_Pagamento F on F.id=CR.Forma_Pag'+
                                     ' Where CliFor=:pCliFor');
      DMCupomFiscal.dbQuery2.ParamByName('pCliFor').AsInteger := cliente;
      DMCupomFiscal.dbQuery2.Active:=True;
      DMCupomFiscal.dbQuery2.First;
      If not(DMCupomFiscal.dbQuery2.Eof) then
      begin
        vlrDisponivel:= DMCupomFiscal.dbQuery2.FieldByName('TSaldo').AsCurrency;
        vlrDisponivel:=(limite + vlrDisponivel);
        if vlrDisponivel < subtotal then
        begin
          if msgPergunta('N�o foi poss�vel liberar venda a prazo.'+#13#10+
                        'Limite de Cr�dito    R$: '+FormatFloat('###,##0.00',Limite)+#13+
                        'Saldo Dispon�vel     R$: '+FormatFloat('###,##0.00',vlrDisponivel)+#13+
                        'Venda Atual             R$: '+FormatFloat('###,##0.00',subtotal)+#13+
                        'Deseja mesmo assim efetuar a venda?','') then
          begin
            if getAutorizacaoLimiteCred('LIMITE_CRED','LIBERAR VENDA A PRAZO PARA CLIENTE SEM CREDITO',subtotal) then
              Result := False
            else
              Result := True;
          end
          else
            Result := True;
        end;
      end
    end;
  end
  else
    Result := False;
end;

Function ValidaFrota(cliente:Integer; Placa:String):Boolean;
begin
  Result:=True;
  if(Placa <> '')then
  begin
    DMCupomFiscal.dbQuery2.Active:=False;
    DMCupomFiscal.dbQuery2.SQL.Clear;
    DMCupomFiscal.dbQuery2.SQL.Add('Select Count(*) as Tem_Frota From Clifor_Frota f Where f.clifor=:pCF');
    DMCupomFiscal.dbQuery2.ParamByName('pCF').AsInteger := cliente;
    DMCupomFiscal.dbQuery2.Active:=True;
    DMCupomFiscal.dbQuery2.First;
    If (DMCupomFiscal.dbQuery2.FieldByName('Tem_Frota').asinteger>0) then // Tem Frota
    begin
      DMCupomFiscal.dbQuery2.Active:=False;
      DMCupomFiscal.dbQuery2.SQL.Clear;
      DMCupomFiscal.dbQuery2.SQL.Add('Select Count(*) as Tem_Placa From Clifor_Frota f Where f.clifor=:pCF  and F.Placa=:pPlaca');
      DMCupomFiscal.dbQuery2.ParamByName('pCF').AsInteger := cliente;
      DMCupomFiscal.dbQuery2.ParamByName('pPlaca').AsString := Placa;
      DMCupomFiscal.dbQuery2.Active:=True;
      DMCupomFiscal.dbQuery2.First;
      If (DMCupomFiscal.dbQuery2.FieldByName('Tem_Placa').asinteger>0) then // Tem Placa
        Result:=True
      else
        Result:=False;
    end;
  end;
end;



procedure arredondarComponente(Componente: TWinControl; const Radius: SmallInt);
var
  R : TRect;
  Rgn : HRGN;
begin
  with Componente do
  begin
    R := ClientRect;
    Rgn := CreateRoundRectRgn(R.Left, R.Top, R.Right, R.Bottom, Radius, Radius);
    Perform(EM_GETRECT, 0, lParam(@R));
    InflateRect(R, -5, -5);
    Perform(EM_SETRECTNP, 0, lParam(@R));
    SetWindowRgn(Handle, Rgn, True);
    Invalidate;
  end;
end;


function cancelarCupomFiscal(cancelarDireto:Boolean=False):Boolean;
var cupom : String;
    id, el_chave, cooAntesCancelar, cooAposCancelar, cuponsCancelados, cooCancelado : Integer;
begin
  try
    if(getAutorizacao('CANCELAMENTO_NFCE','EFETUAR CANCELAMENTO NFCE',0))then
    begin
      if not (msgPergunta('Confirmar o cancelamento do Cupom Fiscal ?', 'Confirma��o')) then
        Abort;
      try
        try
          try //--Cancelar na base de dados
            //Deleta dados da Tabela Temporaria
            DMCupomFiscal.dbTemp_NFCE.Close;
            DMCupomFiscal.dbTemp_NFCE_Item.Close;
            DMCupomFiscal.IBSQL1.Close;
            DMCupomFiscal.IBSQL1.SQL.Clear;
            DMCupomFiscal.IBSQL1.SQL.Add('Delete From temp_nfce_item Where Caixa=:pCaixa');
            DMCupomFiscal.IBSQL1.ParamByName('pCaixa').AsString:=Trim(FormatFloat('0000', FRENTE_CAIXA.Caixa));
            DMCupomFiscal.IBSQL1.ExecQuery;

            DMCupomFiscal.IBSQL1.Close;
            DMCupomFiscal.IBSQL1.SQL.Clear;
            DMCupomFiscal.IBSQL1.SQL.Add('Delete From temp_nfce_Fatura Where Caixa=:pCaixa');
            DMCupomFiscal.IBSQL1.ParamByName('pCaixa').AsString:=Trim(FormatFloat('0000', FRENTE_CAIXA.Caixa));
            DMCupomFiscal.IBSQL1.ExecQuery;

            DMCupomFiscal.IBSQL1.Close;
            DMCupomFiscal.IBSQL1.SQL.Clear;
            DMCupomFiscal.IBSQL1.SQL.Add('Delete From temp_nfce_formapag Where Caixa=:pCaixa');
            DMCupomFiscal.IBSQL1.ParamByName('pCaixa').AsString:=Trim(FormatFloat('0000', FRENTE_CAIXA.Caixa));
            DMCupomFiscal.IBSQL1.ExecQuery;

            DMCupomFiscal.IBSQL1.Close;
            DMCupomFiscal.IBSQL1.SQL.Clear;
            DMCupomFiscal.IBSQL1.SQL.Add('Delete From temp_nfce_formapag_CFRETE Where Caixa=:pCaixa');
            DMCupomFiscal.IBSQL1.ParamByName('pCaixa').AsString:=Trim(FormatFloat('0000', FRENTE_CAIXA.Caixa));
            DMCupomFiscal.IBSQL1.ExecQuery;

            DMCupomFiscal.IBSQL1.Close;
            DMCupomFiscal.IBSQL1.SQL.Clear;
            DMCupomFiscal.IBSQL1.SQL.Add('Delete From temp_nfce Where Caixa=:pCaixa');
            DMCupomFiscal.IBSQL1.ParamByName('pCaixa').AsString:=Trim(FormatFloat('0000', FRENTE_CAIXA.Caixa));
            DMCupomFiscal.IBSQL1.ExecQuery;

            DMCupomFiscal.Transaction.CommitRetaining;
            msgInformacao('NFC-e pendente cancelado com sucesso!','');
            Result := True;
          except
            on e : Exception do
            begin
              dmCupomFiscal.Transaction.RollbackRetaining;
              raise Exception.Create(e.Message);
            end;
          end;
        finally
          //Tratamento das Telas
          if frmLoja <> nil then
          begin
            frmLoja.edtCliente.Text:='';
            frmLoja.edtClienteNome.Text:='';
            getTemp_NFCE_Item;
          end
          else
          begin
            if frmSupermercado <> nil then
            begin
              frmSupermercado.DBGrid1.Visible:=False;
              getTemp_NFCE_Item;
            end
            else
            begin
              if frmPosto <> nil then
              begin
                frmPosto.edtCliente.Text:='';
                frmPosto.edtClienteNome.Text:='';
                getTemp_NFCE_Item;
              end
            end;
          end;
        end;
      except
        on E:Exception do
        begin
          logErros(nil, caminhoLog,'Erro ao cancelar NFC-e!'+#13+e.Message, 'Erro ao cancelar NFC-e','S',E);
          Abort;
        end;
      end;
    end;
  except
    Result := False;
  end;
end;

function getAutorizacao(Historico, Msg: String; Valor: Currency):Boolean;
begin
  if FRENTE_CAIXA.Autorizacao then
  begin
    Result := False;
    FrmAutorizacao:=TFrmAutorizacao.create(Application,Historico,Msg,Valor);
    frmAutorizacao.ShowModal;
    frmAutorizacao.Free;
    If AUTORIZACAO.autorizado then
      Result := True
    else
      Result := False;
  end
  else
    Result := True;
end;

function getAutorizacaoLimiteCred(Historico, Msg: String; Valor: Currency):Boolean;
begin
  frmAutorizacao:=TFrmAutorizacao.create(Application,Historico, Msg, Valor);
  frmAutorizacao.ShowModal;
  frmAutorizacao.Free;
  If AUTORIZACAO.autorizado then
    Result := True
  else
    Result := False;
end;


function gerarELChave():Integer;
begin
  try
    dmCupomFiscal.IBSQL1.Close;
    dmCupomFiscal.IBSQL1.SQL.Clear;
    dmCupomFiscal.IBSQL1.SQL.Add('select gen_id(EL_Chave, 1) as xChave from rdb$database');
    dmCupomFiscal.IBSQL1.ExecQuery;
    dmCupomFiscal.IBSQL1.Close;
    Result := dmCupomFiscal.IBSQL1.fieldbyname('xChave').asinteger;
  except
    on E:Exception do
    begin
      logErros(nil, caminhoLog,'Erro ao gerar chave do registro', 'Erro ao gerar chave do registro','S',E);
      Abort;
    end;
  end;
end;

function gerarID(pGenerator: String):Integer;
begin
  try
    dmCupomFiscal.IBSQL1.Close;
    dmCupomFiscal.IBSQL1.SQL.Clear;
    dmCupomFiscal.IBSQL1.SQL.Add('select gen_id('+pGenerator+', 1) as xID from rdb$database');
    dmCupomFiscal.IBSQL1.ExecQuery;
    dmCupomFiscal.IBSQL1.Close;
    Result := dmCupomFiscal.IBSQL1.fieldbyname('xID').asinteger;
  except
    on E:Exception do
    begin
      logErros(nil, caminhoLog,'Erro ao gerar ID do registro', 'Erro ao gerar ID do registro','S',E);
      Abort;
    end;
  end;
end;

function gerarELChave_BaseRemota():Integer;
begin
  try
    dmCupomFiscal.ibSqlComando_Rem.Close;
    dmCupomFiscal.ibSqlComando_Rem.SQL.Clear;
    dmCupomFiscal.ibSqlComando_Rem.SQL.Add('select gen_id(EL_Chave, 1) as xChave from rdb$database');
    dmCupomFiscal.ibSqlComando_Rem.ExecQuery;
    dmCupomFiscal.ibSqlComando_Rem.Close;
    Result := dmCupomFiscal.ibSqlComando_Rem.fieldbyname('xChave').asinteger;
  except
    on E:Exception do
    begin
      logErros(nil, caminhoLog,'Erro ao gerar chave do registro da base de dados remota', 'Erro ao gerar chave do registro da base de dados remota','S',E);
      Abort;
    end;
  end;
end;

function crProtecaoCredito(cliente:Integer):Boolean;
begin
  //---Prote��o ao Credito
  DMCupomFiscal.dbQuery2.Active:=False;
  DMCupomFiscal.dbQuery2.SQL.Clear;
  DMCupomFiscal.dbQuery2.SQL.Add('Select Count(*) As XX From CliFor_PC Where (CliFor=:pCliFor) and (Ativo=''S'')');
  DMCupomFiscal.dbQuery2.ParamByName('pCliFor').AsInteger := cliente;
  DMCupomFiscal.dbQuery2.Active:=True;
  DMCupomFiscal.dbQuery2.First;
  if DMCupomFiscal.dbQuery2.FieldByName('XX').AsInteger > 0 then
  begin
    msgInformacao('N�o foi poss�vel liberar venda a prazo. Cliente est� na Prote��o ao Cr�dito.','');
    Result := True;
  end
  else
    Result := False;
end;

function quantidadeMaximaExcedeu(quantidade:Double):Boolean;
begin
  if quantidade > FRENTE_CAIXA.Qtd_Maxima then
  begin
    msgAviso('Quantidade m�xima do item foi excedida!'+#13+'Quantidade M�xima: '+FormatFloat('###,##0.00',FRENTE_CAIXA.Qtd_Maxima),'');
    Result := True
  end
  else
    Result := False;
end;

function valorUnitarioMaximoExcedeu(valor:Double):Boolean;
begin
  if valor > FRENTE_CAIXA.Vlr_Maximo then
  begin
    msgAviso('Valor Unit�rio m�ximo do item foi excedido!'+#13+'Valor Unit�rio M�ximo: '+FormatFloat('###,##0.00',FRENTE_CAIXA.Vlr_Maximo),'');
    Result := True
  end
  else
    Result := False;
end;

function verificarEstoque(cod_mercadoria,Estoque_Vinculado, subtipo_item:String; estoque, quantidade:Double):Boolean;
var
  qEst_Vinc: Double;
begin
  if (Estoque_Vinculado='N') then
  begin
    if (estoque < (quantidade + getQtdMercadoriaTempECFItem(cod_mercadoria))) and (subtipo_item <> 'OU')then
    begin
      if (CCUSTO.EstoqueZero='N') then //CCusto n�o permite venda com estoque Negativo
      begin
        msgInformacao(Format('Estoque n�o suportado!'+#13+
                      'Centro de Custo %d n�o permite Venda com estoque Negativo.'+#13+
                      'Mercadoria: %s'+#13+
                      'Estoque: %.3f',[CCUSTO.codigo, Cod_Mercadoria+' - '+MERCADORIA.descricao, estoque]),'Aten��o');
        Result := False; //Quando n�o permite a venda
      end
      else //CCusto permite venda
        Result := True;
    end
    else
      Result := True;
  end
  else
  begin
    if (CCUSTO.EstoqueZero='N') then //CCusto n�o permite venda com estoque Negativo
    begin
      if (GetQtdEstoqueProdVInculado(cod_mercadoria, CCUSTO.codigo) <
        (quantidade + getQtdMercadoriaTempECFItem(cod_mercadoria))) and (subtipo_item <> 'OU')then
      begin
        msgInformacao(Format('Estoque n�o suportado!'+#13+
                      'Mercadoria: %s'+#13+
                      'Centro de Custo %d n�o permite Venda com estoque vinculado negativo.',
                      [FDescricao_Vinculado,CCusto.Codigo]),
                      Application.Title);
        Result := False; //Quando n�o permite a venda
      end
      else
        Result := True;
    end
    else
      Result := True;
  end;
end;


procedure setMensagemDisplay();
begin
  if PERSONALIZAR.Display then
  begin
    if frmLoja <> nil then
    begin
      frmLoja.tmrScroll.Enabled := True;
      frmLoja.txtMensagem.Caption := PERSONALIZAR.MensagemDisplay;
    end
    else
    begin
      if frmSupermercado <> nil then
      begin
        frmSupermercado.tmrScroll.Enabled := True;
        frmSupermercado.txtMensagem.Caption := PERSONALIZAR.MensagemDisplay;
      end
      else
      begin
        if frmPosto <> nil then
        begin
          frmPosto.tmrScroll.Enabled := True;
          frmPosto.txtMensagem.Caption := PERSONALIZAR.MensagemDisplay;
        end;
      end;
    end;
  end
  else
  begin
    if frmLoja <> nil then
    begin
      frmLoja.tmrScroll.Enabled := False;
      frmLoja.txtMensagem.Caption := '';
    end
    else
    begin
      if frmSupermercado <> nil then
      begin
        frmSupermercado.tmrScroll.Enabled := False;
        frmSupermercado.txtMensagem.Caption := '';
      end
      else
      begin
        if frmPosto <> nil then
        begin
          frmPosto.tmrScroll.Enabled := False;
          frmPosto.txtMensagem.Caption := '';
        end;
      end;
    end;
  end;
end;

function calculaJuros(taxJuroDia, valor:Currency; dataVencimento:TDateTime ): Currency;
var diasVencido, i:integer;
    valorTotalJuro:Currency;
begin
  valorTotalJuro  := 0;
  if dataVencimento < Now then  //Quer dizer que venceu a conta
  begin
    diasVencido := DaysBetween(Now, dataVencimento);
    If (diasVencido > 0) Then
    begin
      for i := 1 to diasVencido do
        valorTotalJuro := valorTotalJuro + (valor * taxJuroDia)/100;
    end;
  end;
  Result := valorTotalJuro;
end;

function getQtdMercadoriaTempECFItem(codigo:String):Double;
begin
  dmCupomFiscal.dbQuery3.Active:=False;
  dmCupomFiscal.dbQuery3.SQL.Clear;
  dmCupomFiscal.dbQuery3.SQL.Add('select coalesce(sum(i.quantidade),0) quantidade'+
                                 ' from temp_nfce_item i where i.mercadoria=:pID');
  dmCupomFiscal.dbQuery3.ParamByName('pID').AsString := codigo;
  dmCupomFiscal.dbQuery3.Active:=True;
  dmCupomFiscal.dbQuery3.First;
  If not(dmCupomFiscal.dbQuery3.Eof) then
    Result := dmCupomFiscal.dbQuery3.FieldByName('quantidade').Value
  else
    Result := 0;
end;

function getSaldoCliente(id:integer; calcJuro:Boolean=True):Currency;
Var vlrLimite:Currency;
    saldo:Currency;
    dias:Integer;
    taxaJuro:Currency;
begin
  vlrLimite:=0;
  saldo:=0;
  dias:=0;

  DMCupomFiscal.dbQuery1.Active:=False;
  DMCupomFiscal.dbQuery1.SQL.Clear;
  DMCupomFiscal.dbQuery1.SQL.Add('Select nome, limite, juro From CliFor Where ID=:pCliFor');
  DMCupomFiscal.dbQuery1.ParamByName('pCliFor').AsInteger:=id;
  DMCupomFiscal.dbQuery1.Active:=True;
  DMCupomFiscal.dbQuery1.First;
  If not(DMCupomFiscal.dbQuery1.Eof) then
  begin
    vlrLimite:=DMCupomFiscal.dbQuery1.FieldByName('Limite').AsCurrency;
    taxaJuro:=DMCupomFiscal.dbQuery1.FieldByName('juro').AsCurrency;

    //---Baixa Seletiva
    if FRENTE_CAIXA.CR_Baixa_Seletiva then
    begin
      DMCupomFiscal.dbQuery1.Active:=False;
      DMCupomFiscal.dbQuery1.SQL.Clear;
      DMCupomFiscal.dbQuery1.SQL.Add('Select data_ven, resta From CR_Movimento Where CliFor=:pCliFor and resta > 0');
      DMCupomFiscal.dbQuery1.ParamByName('pCliFor').AsInteger:=id;
      DMCupomFiscal.dbQuery1.Active:=True;
      DMCupomFiscal.dbQuery1.First;
      while not DMCupomFiscal.dbQuery1.Eof do
      begin
        //Fun��o calculaJuros calcula somente o juros deve ser somado o valor que resta
        if calcJuro then
          saldo := saldo + DMCupomFiscal.dbQuery1.FieldByName('resta').AsCurrency + calculaJuros(taxaJuro, DMCupomFiscal.dbQuery1.FieldByName('resta').AsCurrency, DMCupomFiscal.dbQuery1.FieldByName('data_ven').AsDateTime)
        else
          saldo := saldo + DMCupomFiscal.dbQuery1.FieldByName('resta').AsCurrency;
        DMCupomFiscal.dbQuery1.Next;
      end;
    end
    Else
    begin
      //-- Verifica Se Faz Mais de 30 Dias que N�o Existe Credito
      DMCupomFiscal.dbQuery1.SQL.Clear;
      DMCupomFiscal.dbQuery1.SQL.Add('Select Max(Data_Ope) as Data_Credito'+
                      ' From Cr_Movimento CR '+
                      ' LEFT OUTER JOIN Formas_Pagamento Fp ON (Fp.ID=Cr.Forma_Pag)'+
                      ' Where (Fp.CR_DC=''C'') and (CR.CliFor=:pCliFor)');
      DMCupomFiscal.dbQuery1.ParamByName('pCliFor').AsInteger:=id;
      DMCupomFiscal.dbQuery1.Active:=True;
      DMCupomFiscal.dbQuery1.First;
        If Trim(DMCupomFiscal.dbQuery1.FieldByName('Data_Credito').Text)>'' then
        begin
          Dias:=Date - DMCupomFiscal.dbQuery1.FieldByName('Data_Credito').AsVariant;
        end;

      //---Conta Corrente
      DMCupomFiscal.dbQuery1.Active:=False;
      DMCupomFiscal.dbQuery1.SQL.Clear;
      DMCupomFiscal.dbQuery1.SQL.Add('Select Sum(case when (F.CR_DC=''C'') then Valor else -Valor end) as TSaldo'+
      ' From CR_Movimento CR'+
      ' Left Outer join Formas_Pagamento F on F.id=CR.Forma_Pag'+
      ' Where CliFor=:pCliFor');
      DMCupomFiscal.dbQuery1.ParamByName('pCliFor').AsInteger:=id;
      DMCupomFiscal.dbQuery1.Active:=True;
      DMCupomFiscal.dbQuery1.First;
      saldo:=DMCupomFiscal.dbQuery1.FieldByName('TSaldo').AsCurrency;
    end;
    Result := saldo;
  end
  else
    Result := 0;
end;

function getValorSubtotalTemp(caixa:integer):Real;
begin
  DMCupomFiscal.dbQuery1.Active:=False;
  DMCupomFiscal.dbQuery1.SQL.Clear;
  DMCupomFiscal.dbQuery1.SQL.Add('Select coalesce(Sum(vlr_total),0) vlr_total From temp_nfce_item'+
                                 ' Where Caixa=:pCaixa');
  DMCupomFiscal.dbQuery1.ParamByName('pCaixa').AsString:=FormatFloat('0000', caixa);
  DMCupomFiscal.dbQuery1.Active:=True;
  DMCupomFiscal.dbQuery1.First;
  Result := DMCupomFiscal.dbQuery1.FieldByName('vlr_total').Value;
end;

function possuiRegistroTempECF(caixa:integer):Boolean;
begin
  DMCupomFiscal.dbQuery2.Active:=False;
  DMCupomFiscal.dbQuery2.SQL.Clear;
  DMCupomFiscal.dbQuery2.SQL.Add('Select count(*) as xFlag from temp_nfce e'+
                                 ' where e.caixa=:pcaixa');
  DMCupomFiscal.dbQuery2.ParamByName('pCaixa').AsString:=FormatFloat('0000', caixa);
  DMCupomFiscal.dbQuery2.Active:=True;
  DMCupomFiscal.dbQuery2.First;
  Result := (DMCupomFiscal.dbQuery2.FieldByName('xFlag').AsInteger > 0);
end;

procedure setCaixaUso(funcionario:Integer; emUso:String);
begin
  if not dmCupomFiscal.IBSQL1.Transaction.InTransaction then
    dmCupomFiscal.IBSQL1.Transaction.StartTransaction;
  dmCupomFiscal.IBSQL1.Close;
  dmCupomFiscal.IBSQL1.SQL.Clear;
  dmCupomFiscal.IBSQL1.SQL.Add('update funcionarios set nfce_uso=:pUso Where (ID=:pID)');
  dmCupomFiscal.IBSQL1.ParamByName('pID').AsInteger:=funcionario;
  dmCupomFiscal.IBSQL1.ParamByName('pUso').AsString:=emUso;
  dmCupomFiscal.IBSQL1.ExecQuery;
  dmCupomFiscal.IBSQL1.Close;
  dmCupomFiscal.Transaction.CommitRetaining;
end;

procedure getTemp_NFCE_Item;
begin
  if frmLoja <> nil then
    frmLoja.reTotal.Value:=getValorSubtotalTemp(FRENTE_CAIXA.Caixa)
  else
    if frmSupermercado <> nil then
      frmSupermercado.reSubTotal.Value:=getValorSubtotalTemp(FRENTE_CAIXA.Caixa)
    else
      if frmPosto <> nil then
        frmPosto.reTotal.Value:=getValorSubtotalTemp(FRENTE_CAIXA.Caixa);
  DMCupomFiscal.dbTemp_NFCE_Item.Close;
  DMCupomFiscal.dbTemp_NFCE_Item.ParamByName('pCaixa').AsString := FormatFloat('0000', FRENTE_CAIXA.Caixa);
  DMCupomFiscal.dbTemp_NFCE_Item.Open;
  DMCupomFiscal.dbTemp_NFCE_Item.FetchAll;
  DMCupomFiscal.dbTemp_NFCE_Item.First;
  if frmSupermercado <> nil then
  begin
    If DMCupomFiscal.dbTemp_NFCE_Item.Eof then
      frmSupermercado.DBGrid1.Visible:=False
    else
      frmSupermercado.DBGrid1.Visible:=True;
  end;
  DMCupomFiscal.dbTemp_NFCE_Item.Last;
end;

procedure inserirTempNFCe();
begin
  dmCupomFiscal.dbTemp_NFCE.Open;
  dmCupomFiscal.dbTemp_NFCE.Insert;
  dmCupomFiscal.dbTemp_NFCECAIXA.Value       := FormatFloat('0000', FRENTE_CAIXA.caixa);
  dmCupomFiscal.dbTemp_NFCESERIE.Value       := frmNFCe.LOCAL_SERIE_PADRAO;
  dmCupomFiscal.dbTemp_NFCEMODELO_DOC.Value  := '65';
  dmCupomFiscal.dbTemp_NFCEDATA.Value        := Now;
  dmCupomFiscal.dbTemp_NFCEHORA.Value        := Now;
  dmCupomFiscal.dbTemp_NFCECCUSTO.Value      := CCUSTO.codigo;
  dmCupomFiscal.dbTemp_NFCETURNO.Value       := POSTO.Turno;
  dmCupomFiscal.dbTemp_NFCEFUNCIONARIO.Value := LOGIN.usuarioCod;
  dmCupomFiscal.dbTemp_NFCEESTOQUE.Value     := 'S';
  dmCupomFiscal.dbTemp_NFCEOPERACAO.Value    := FRENTE_CAIXA.Operacao;
  dmCupomFiscal.dbTemp_NFCEFORMA_PAG.Value   := 0;
  dmCupomFiscal.dbTemp_NFCESTATUS.Value      := 'T';
  dmCupomFiscal.dbTemp_NFCEFUN_USUARIO.Value := LOGIN.usuarioCod;
  dmCupomFiscal.dbTemp_NFCECOD_SIT.Value     := '00';  //Documento regular
  dmCupomFiscal.dbTemp_NFCEINDPAG.Value      := '0';    //0-Vista
  dmCupomFiscal.dbTemp_NFCETPIMP.Value       := '4';     //4-DANFE NFC-e
  dmCupomFiscal.dbTemp_NFCETPEMIS.Value      := frmNFCe.LOCAL_FORMA_EMISSAO;
  dmCupomFiscal.dbTemp_NFCETPAMB.Value       := frmNFCe.LOCAL_WEBS_AMBIENTE;
  dmCupomFiscal.dbTemp_NFCEINDPRES.Value     := '1';   //1-Operacao Presencial

  dmCupomFiscal.dbTemp_NFCEINDIEDEST.Value   := '9'; //9-N�o contribuinte
  dmCupomFiscal.dbTemp_NFCEMODFRETE.Value    := '9';  //9-Sem frete
  dmCupomFiscal.dbTemp_NFCEVLR_SUBTOTAL.Value:= 0;
  dmCupomFiscal.dbTemp_NFCEVLR_DESCONTO.Value:= 0;
  dmCupomFiscal.dbTemp_NFCEVLR_TOTAL.Value   := 0;
  dmCupomFiscal.dbTemp_NFCEVLR_RECEBIDO.Value:= 0;
  dmCupomFiscal.dbTemp_NFCEVLR_TROCO.Value   := 0;
  dmCupomFiscal.dbTemp_NFCEVLR_BC_ICM.Value  := 0;
  dmCupomFiscal.dbTemp_NFCEVLR_ICM.Value     := 0;
  dmCupomFiscal.dbTemp_NFCEVLR_FRETE.Value   := 0;
  dmCupomFiscal.dbTemp_NFCEVLR_SEG.Value     := 0;
  dmCupomFiscal.dbTemp_NFCEVLR_BC_PIS.Value  := 0;
  dmCupomFiscal.dbTemp_NFCEVLR_PIS.Value     := 0;
  dmCupomFiscal.dbTemp_NFCEVLR_BC_COFINS.Value := 0;
  dmCupomFiscal.dbTemp_NFCEVLR_COFINS.Value  := 0;
  dmCupomFiscal.dbTemp_NFCEVLR_OUTRO.Value   := 0;
  DMCupomFiscal.dbTemp_NFCEEL_CHAVE.Value    := gerarELChave;
  if ((frmLoja <> nil) and (frmLoja.ELChavePedido > 0)) then
  begin
    with dmCupomFiscal.dbQuery1 do
    begin
      Active:=False;
      SQL.Clear;
      SQL.Add('select p.clifor, '+
              'c.Nome, c.TPreco, c.Limite, c.endereco as CF_END, '+
              'm.descricao as CF_MUNI, c.cnpjcpf as CF_CNPJ_CPF, '+
              'c.ie as CF_IE, c.numero_end as CF_NUMERO_END, '+
              'c.municipio as CF_MUNICIPIO, c.Bairro as CF_BAIRRO, '+
              'c.cep as CF_CEP, c.indiedest as INDIEDEST '+
              'from est_pedidos p '+
              'left outer join Clifor c on c.ID = p.Clifor '+
              'left outer join municipios m on m.id = c.municipio '+
              'where p.el_chave=:pEl_Chave');
      ParamByName('pel_chave').Value := frmLoja.ELChavePedido;
      Active:=True;
      First;
      if not (Eof) then
      begin
        dmCupomFiscal.dbTemp_NFCECLIFOR.Value        := FieldByName('clifor').AsInteger;
        dmCupomFiscal.dbTemp_NFCECF_NOME.Value       := FieldByName('Nome').AsString;
        dmCupomFiscal.dbTemp_NFCECF_ENDE.Value       := FieldByName('CF_END').AsString;
        dmCupomFiscal.dbTemp_NFCECF_CNPJ_CPF.Value   := FieldByName('CF_CNPJ_CPF').AsString;
        dmCupomFiscal.dbTemp_NFCECF_IE.Value         := FieldByName('CF_IE').AsString;
        dmCupomFiscal.dbTemp_NFCECF_NUMERO_END.Value := FieldByName('CF_NUMERO_END').AsString;
        dmCupomFiscal.dbTemp_NFCECF_MUNICIPIO.Value  := FieldByName('CF_MUNICIPIO').AsInteger;
        if(FieldByName('CF_MUNICIPIO').AsInteger = 0)then
          dmCupomFiscal.dbTemp_NFCECF_MUNICIPIO.Clear;
        dmCupomFiscal.dbTemp_NFCECF_CEP.Value        := FieldByName('CF_CEP').AsString;
        dmCupomFiscal.dbTemp_NFCECF_BAIRRO.Value     := FieldByName('CF_BAIRRO').AsString;
        dmCupomFiscal.dbTemp_NFCEINDIEDEST.Value     :=
          ifthen(FieldByName('INDIEDEST').AsString = '','9',FieldByName('INDIEDEST').AsString);
      end;
    end;
    dmCupomFiscal.dbTemp_NFCEEL_CHAVE.Value   := frmLoja.ELChavePedido;
  end
  else
  begin
    dmCupomFiscal.dbTemp_NFCECLIFOR.Value      := 0;
  end;
  dmCupomFiscal.dbTemp_NFCE.Post;
  dmCupomFiscal.dbTemp_NFCE.Transaction.CommitRetaining;
end;

//Essa autorizacao sera solicitada incondicionalmente
function getAutorizacaoIncondicional(Historico, Msg: String; Valor: Currency; Nivel:integer=2):Boolean;
begin
  frmAutorizacao:=TFrmAutorizacao.create(Application, Historico, Msg, Valor);
  frmAutorizacao.ShowModal;
  frmAutorizacao.Free;
  If AUTORIZACAO.autorizado then
    Result := True
  else
    Result := False;
end;

function ValidarNCM(const ncm:string):Boolean;
begin
  {C�digo NCM (8 posi��es), ser� permitida a informa��o do g�nero (posi��o
  do cap�tulo do NCM) quando a opera��o n�o for de com�rcio exterior
  (importa��o/exporta��o) ou o produto n�o seja tributado pelo IPI.
  Em caso de item de servi�o ou item que n�o tenham produto
  (Ex. transfer�ncia de cr�dito, cr�dito do ativo imobilizado, etc.),
  informar o c�digo 00 (zeros) (v2.0)}
  case Length(ncm) of
    2:Result := True; //Aceita g�nero ou '00'
    8:Result := True; //Se possuir 8 d�gitos nem verifica informa��o
  else
    msgInformacao(Format('NCM "%s" inv�lido, n�o � poss�vel registrar item',[ncm]),'');
    Result := False;
  end;
end;

function abrirGavetaBematech(Porta:String):Boolean;
var
  Impressora: TextFile;
begin
  try
    try
      Result := True;
      AssignFile(Impressora, Porta);
      ReWrite(Impressora);
      WriteLn(Impressora, #27 + #118 + #140); // Bematech
  //    WriteLn(Impressora,#027+#112+#000+#010+#100'');// Epson
    finally
      CloseFile(Impressora);
    end;
  except
    Result := False;
  end;
end;


//Lauro
function GetCalculaDataVencimento(CliFor:integer): TDate;
Var Data_Ven: TDateTime;
    dia, mes, ano: Word;
    i,Dia_Semana:Integer;
begin
  DecodeDate( Date, ano, mes, dia); //Data Atual

  dmCupomFiscal.dbQuery3.Active:=False;
  dmCupomFiscal.dbQuery3.sql.Clear;
  dmCupomFiscal.dbQuery3.sql.add('Select Calc_Data_Ven, DiasPrazo From CliFor Where ID=:pID');
  dmCupomFiscal.dbQuery3.paramByName('pID').asinteger:=CliFor;
  dmCupomFiscal.dbQuery3.Active:=True;
  dmCupomFiscal.dbQuery3.First;
  If dmCupomFiscal.dbQuery3.FieldByName('Calc_Data_Ven').asString='NDIAS' then
  begin
    Data_Ven:=IncDay(Date,dmCupomFiscal.dbQuery3.FieldByName('DiasPrazo').asInteger);
  end;

  If dmCupomFiscal.dbQuery3.FieldByName('Calc_Data_Ven').asString='DIA' then
  begin
    Data_ven:=StrToDate(dmCupomFiscal.dbQuery3.FieldByName('DiasPrazo').asString+'/'+ IntToStr(Mes) +'/'+IntToStr(Ano));

    If (Dia>dmCupomFiscal.dbQuery3.FieldByName('DiasPrazo').AsInteger) then
      Data_ven:=IncMonth(Data_Ven,1);
  end;

  If dmCupomFiscal.dbQuery3.FieldByName('Calc_Data_Ven').asString='SEMANA' then
  begin
    Dia_Semana:=dmCupomFiscal.dbQuery3.FieldByName('DiasPrazo').asInteger;
    Data_Ven:=IncDay(Date,1);
    For i := 1 to 7 do
    begin
      If Dia_Semana<>DayofWeek(Data_Ven) then
        Data_Ven:=IncDay(Data_Ven,1);
    end;
  end;

  If dmCupomFiscal.dbQuery3.FieldByName('Calc_Data_Ven').asString='QUINZENA' then
  begin
    If Dia<15 then
      Data_ven:=StrToDate('15/'+ IntToStr(Mes) +'/'+IntToStr(Ano));

    If (Dia>=15) and (Dia<31) then
    begin
      Data_ven:=StrToDate('01/'+ IntToStr(Mes) +'/'+IntToStr(Ano));
      Data_ven:=IncMonth(Data_Ven,1);
    end
  end;

  If dmCupomFiscal.dbQuery3.FieldByName('Calc_Data_Ven').asString='DEZENA' then
  begin
    If Dia<10 then
      Data_ven:=StrToDate('10/'+ IntToStr(Mes) +'/'+IntToStr(Ano));

    If (Dia>=10) and (Dia<20) then
      Data_ven:=StrToDate('20/'+ IntToStr(Mes) +'/'+IntToStr(Ano));

    If (Dia>=20) and (Dia<31) then
    begin
      Data_ven:=StrToDate('01/'+ IntToStr(Mes) +'/'+IntToStr(Ano));
      Data_ven:=IncMonth(Data_Ven,1);
    end
  end;

  Result:=Data_Ven;
end;

function GetQtdEstoqueProdVInculado(pID_Merc: String; pCCusto: Integer): Double;
var
  lQrBusca: TIBQuery;
begin
  lQrBusca := TIBQuery.create(nil);
  try
    lQrBusca.Database := dmCupomFiscal.DataBase;
    lQrBusca.SQL.Add('Select e.EST_ATUAL, m.Descricao from est_mercadorias_Estoque e '+
                     'left outer join est_mercadorias m on cast(m.ID_CODIGO as Varchar(13)) = e.MERCADORIA '+
                     'where e.CCUSTO = :pCC and m.id = :pID');
    lQrBusca.ParamByName('pID').asString := pID_Merc;
    lQrBusca.ParamByName('pCC').AsInteger := pCcusto;
    lQrBusca.Open;
    FDescricao_Vinculado := lQrBusca.FieldByName('Descricao').AsString;
    Result := lQrBusca.FieldByName('EST_ATUAL').AsFloat;
    lQrBusca.Close;
  finally
    lQrBusca.Free;
  end;
end;

end.
