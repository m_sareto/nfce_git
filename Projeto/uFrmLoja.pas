{http://acbr.sourceforge.net/drupal/?q=node/24 }
unit uFrmLoja;

interface

uses
  Windows, Messages, Variants, DBCtrls, DBClient, Grids, Math,
  AppEvnts, SysUtils, Forms, Types, Classes, ACBrLCB, ACBrBase, StdCtrls, StrUtils,
  Controls, ExtCtrls, ACBrUtil, ACBrECFBematech, TypInfo, ActiveX, MSHTML, IniFiles,
  DB, OleCtrls, SHDocVw, DBGrids, Buttons, Tredit, uFrmClifor,

  ACBrPAFClass, ACBrRFD, ACBrDevice, ACBrECFClass, ACBrConsts,ACBrECF,
  {$IFDEF Delphi6_UP} StrUtils, DateUtils, Types, {$ELSE} FileCtrl,{$ENDIF}
  Graphics,
  Dialogs, ComCtrls, Menus, Spin, jpeg
  {$IFDEF Delphi7},XPMan{$ENDIF}, ACBrAAC, Mask, IBDatabase,
  IBCustomDataSet, IBQuery, ACBrValidador, ToolWin, uFrmListaComanda;

type
  TfrmLoja = class(TForm)
    barStatus: TStatusBar;
    pnlRodape: TPanel;
    pnlBackground: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label6: TLabel;
    Label10: TLabel;
    la_Estoque: TLabel;
    Label7: TLabel;
    Label11: TLabel;
    Label13: TLabel;
    edtMercadoria: TEdit;
    reTotal: TRealEdit;
    btnVende: TBitBtn;
    btnCancela: TBitBtn;
    reVlrUnitario: TRealEdit;
    reSubtotal: TRealEdit;
    reVlrDesconto: TRealEdit;
    edtCliente: TEdit;
    edtClienteNome: TEdit;
    BitBtn3: TBitBtn;
    DsItem: TDataSource;
    Panel2: TPanel;
    DBGrid1: TDBGrid;
    mBobina: TMemo;
    wbBobina: TWebBrowser;
    pnlMensagem: TPanel;
    ApplicationEvents1: TApplicationEvents;
    pnlDadosIF: TPanel;
    pnlBotoes: TPanel;
    reQuantidade: TRealEdit;
    lblDescPerc: TLabel;
    pnlBotoesOrganiza: TPanel;
    btnFinalizarCupom: TSpeedButton;
    btnCancelarCupom: TSpeedButton;
    btnCancelarItem: TSpeedButton;
    btnPedido: TSpeedButton;
    btnGaveta: TSpeedButton;
    btnConsultarCR: TSpeedButton;
    btnConsultaPreco: TSpeedButton;
    btnPesquisa: TSpeedButton;
    btnSair: TSpeedButton;
    pnlDadosIFOrganiza: TPanel;
    lblOperador: TLabel;
    lblNumCaixa: TLabel;
    lblCCusto: TLabel;
    lblTurno: TLabel;
    lblData: TLabel;
    tmrScroll: TTimer;
    txtMensagem: TLabel;
    btnConsumidor: TSpeedButton;
    dsEcf: TDataSource;
    lblCusto: TLabel;
    btnLerComandas: TSpeedButton;
    procedure edtMercadoriaExit(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure edtMercadoriaEnter(Sender: TObject);
    procedure reVlrUnitarioExit(Sender: TObject);
    procedure btnCancelaClick(Sender: TObject);
    procedure reVlrDescontoExit(Sender: TObject);
    procedure btnVendeClick(Sender: TObject);
    procedure reQuantidadeEnter(Sender: TObject);
    procedure Label1Click(Sender: TObject);
    procedure subtotal;
    procedure Mer_Bico;
    procedure edtClienteExit(Sender: TObject);
    procedure edtClienteEnter(Sender: TObject);
    procedure Label11Click(Sender: TObject);
    procedure reSubtotalExit(Sender: TObject);
    procedure tratarBotoes;
    Procedure FormataQtd;
    Procedure FormataVlrUni;
    Procedure LimpaCampos;

    procedure Label13Click(Sender: TObject);
    procedure DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure edtClienteClick(Sender: TObject);

    procedure BloqueiaVenda;
    procedure LiberaVenda;
    procedure ApplicationEvents1Hint(Sender: TObject);
    procedure btnSairClick(Sender: TObject);
    procedure btnCancelarItemClick(Sender: TObject);
    procedure btnCancelarCupomClick(Sender: TObject);
    procedure btnFinalizarCupomClick(Sender: TObject);
    procedure edtCCustoClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnConsultarCRClick(Sender: TObject);
    procedure btnConsultaPrecoClick(Sender: TObject);
    procedure btnPesquisaClick(Sender: TObject);
    procedure reQuantidadeExit(Sender: TObject);
    procedure reVlrUnitarioEnter(Sender: TObject);
    procedure reVlrDescontoEnter(Sender: TObject);
    procedure reSubtotalEnter(Sender: TObject);
    procedure btnPedidoClick(Sender: TObject);
    procedure btnGavetaClick(Sender: TObject);
    procedure tmrScrollTimer(Sender: TObject);
    procedure FormMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure txtMensagemMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormResize(Sender: TObject);
    procedure DsItemDataChange(Sender: TObject; Field: TField);
    procedure btnConsumidorClick(Sender: TObject);
    procedure dsEcfDataChange(Sender: TObject; Field: TField);
    procedure btnLerComandasClick(Sender: TObject);
    procedure edtMercadoriaChange(Sender: TObject);


  private
    vlrDesconto: Currency;
    ncm : String;
    { Private declarations }
    procedure arrendarCampos();
    procedure setCores();
  public
    { Public declarations }
      Sender_Libera:String;
      Abastecimento, ELChavePedido:Integer;
      ID_Comanda: String;
      ITEM_Comanda: String;
      sVlr:Real;
  end;


var
  frmLoja: TfrmLoja;

implementation

uses uFrmPesquisaMercadoria, uDMCupomFiscal, uFrmCancelaItem,
  uFrmPesquisaCliente, UFrmFim, uFrmConsultaCR, uFrmAbastecidas,
  uFrmConsultaExtrato, uFrmFechamentoAbastecidas, uFuncoes,
  uFrmConfigurarSerial, uDMComponentes, uFrmPrincipal,
  uFrmConfiguracao, uRotinasGlobais, uFrmConsultaLimiteCR,
  UfrmPesquisaCodigo, uFrmImportaPedido, uFrmLogin, uFrmNFCe,
  uFrmAjustarNcm;

{$R *.dfm}

Procedure TfrmLoja.formataQtd;
begin
  Case FRENTE_CAIXA.Casas_Dec_Quantidade of
    1:begin
      reQuantidade.FormatSize:='10.1';
      reQuantidade.Text:=FormatFloat('##0.0',reQuantidade.value);
    end;
    2:begin
      reQuantidade.FormatSize:='10.2';
      reQuantidade.Text:=FormatFloat('##0.00',reQuantidade.value);
    end;
    3:begin
      reQuantidade.FormatSize:='10.3';
      reQuantidade.Text:=FormatFloat('##0.000',reQuantidade.value);
    end;
    4:begin
      reQuantidade.FormatSize:='10.4';
      reQuantidade.Text:=FormatFloat('##0.0000',reQuantidade.value);
    end;
  end;
end;

Procedure TfrmLoja.formataVlrUni;
begin
  Case FRENTE_CAIXA.Casas_Dec_Valor of
    1:begin
      reVlrUnitario.FormatSize:='10.1';
      reVlrUnitario.Text:=FormatFloat('##0.0',reVlrUnitario.value);
    end;
    2:begin
      reVlrUnitario.FormatSize:='10.2';
      reVlrUnitario.Text:=FormatFloat('##0.00',reVlrUnitario.value);
    end;
    3:begin
      reVlrUnitario.FormatSize:='10.3';
      reVlrUnitario.Text:=FormatFloat('##0.000',reVlrUnitario.value);
    end;
    4:begin
      reVlrUnitario.FormatSize:='10.4';
      reVlrUnitario.Text:=FormatFloat('##0.0000',reVlrUnitario.value);
    end;
  end;
end;

procedure TfrmLoja.edtMercadoriaExit(Sender: TObject);
begin
  Cor_Campo(Sender, HexToTColor(PERSONALIZAR.corFundoCampo));
  If (Length(Trim(edtMercadoria.Text)) > 13) or (Trim(edtMercadoria.Text)='') then
  begin
    Mer_Bico;
  end
  else
  begin
    DMCupomFiscal.dbQuery1.Active:=False;
    DMCupomFiscal.dbQuery1.SQL.Clear;
    DMCupomFiscal.dbQuery1.SQL.Add('SELECT M.ID, M.Unidade, M.CST_Pis, M.CST_Cofins, M.Descricao, M.Sucinto, M.Venda, M.Ativo, M.servico,'+
                            ' M.ECF_CST_ICM, M.ECF_CFOP, M.NCM, M.SUBTIPO_ITEM, M.Desconto,'+
                            ' M.SM_Venda_Promocao, M.Custo_Ultimo, M.Custo_Medio, M.ICM_ECF, M.ISSQN, ME.Est_Atual, MCF.Venda MCF_Venda,'+
                            ' m.ID_Codigo, m.Estoque_Vinculado'+
                            ' FROM Est_Mercadorias M'+
                            ' Left Outer Join Est_Mercadorias_Estoque ME on ME.Mercadoria=M.Id and Me.CCusto=:pCC'+
                            ' Left Outer Join Est_Mercadorias_CliFor MCF on MCF.Mercadoria=M.Id and MCF.CliFor=:pCF'+
                            ' WHERE (M.ID=:pCodigo)');
    DMCupomFiscal.dbQuery1.ParamByName('pCodigo').AsString:=Trim(edtMercadoria.Text);
    DMCupomFiscal.dbQuery1.ParamByName('pCC').AsInteger:=CCUSTO.codigo;
    DMCupomFiscal.dbQuery1.ParamByName('pCF').AsInteger:=StrToIntDef(edtCliente.Text,0);
    DMCupomFiscal.dbQuery1.Active:=True;
    DMCupomFiscal.dbQuery1.First;
    If not(DMCupomFiscal.dbQuery1.Eof) then
    begin
      //Verifica se mercadoria est� Ativa
      If DMCupomFiscal.dbQuery1.FieldByName('Ativo').AsString='N' then
      begin
        msgInformacao('Mercadoria est� Inativa. Favor verificar cadastro','Aten��o');
        edtMercadoria.Clear;
        Mer_Bico;
      end
      else
      begin
        ncm := DMCupomFiscal.dbQuery1.FieldByName('NCM').AsString;
        If not (frmPrincipal.Valida_NCM(ncm)) then
        begin
          if not(msgPergunta('NCM informado na mercadoria n�o existe na base de dados.'+#13+
                             'A partir de 01/01/2016 o NCM obrigatoriamente deve'+#13+
                             'existir na base de dados disponibilizada pelo governo.'+#13+#13+
                             'NCM: '+ncm+#13+
                             'Deseja ajustar o cadastro da mercadoria e tentar novamente?'
                             ,Application.Title))then
          begin
            edtMercadoria.SetFocus;
            Abort;
          end
          else
          begin
            try
              frmAjustarNcm := TfrmAjustarNcm.Create(Self, dmCupomFiscal.dbQuery1.fieldByName('NCM').asString,
                               dmCupomFiscal.dbQuery1.fieldByName('Descricao').asString,
                               dmCupomFiscal.dbQuery1.fieldByName('ID').asString);
              case frmAjustarNcm.ShowModal of
              mrOK:begin //se OK ent�o cliente atualizou o NCM
                  if(frmAjustarNcm.yNCM_NOVO <> dmCupomFiscal.dbQuery1.fieldByName('NCM').asString)then
                    ncm := frmAjustarNcm.yNCM_NOVO;
                end;
              mrCancel:begin
                  edtMercadoria.SetFocus;
                  Abort;
                end;
              mrAbort:begin
                  edtMercadoria.SetFocus;
                  Abort;
                end
              end;
            finally
              frmAjustarNcm.Free;
            end;
          end;
        end;

        la_Estoque.Caption:=FormatFloat('###,##0.000',DMCupomFiscal.dbQuery1.FieldByName('Est_Atual').AsCurrency);
        MERCADORIA.subtipo_item := DMCupomFiscal.dbQuery1.FieldByName('subtipo_item').AsString;
        MERCADORIA.servico := dmCupomFiscal.dbQuery1.FieldByName('servico').AsString='S';
        MERCADORIA.descricao := dmCupomFiscal.dbQuery1.FieldByName('Descricao').AsString;
        MERCADORIA.Est_Vinculado := dmCupomFiscal.dbQuery1.FieldByName('Estoque_Vinculado').AsString;
        //Verifica se Estoque n�o esta nulo
        if DMCupomFiscal.dbQuery1.FieldByName('Est_Atual').IsNull then
        begin
          Mer_Bico;
          msgInformacao('Mercadoria n�o possui registro de estoque para centro de custo '+IntToStr(CCUSTO.codigo),'');
        end
        else
        begin
          //Verifica o Estoque - Centro de Custo Permite venda com Estoque Negativo?
          If (not(MERCADORIA.servico) and
            not(verificarEstoque(Trim(edtMercadoria.Text),
            MERCADORIA.Est_Vinculado, MERCADORIA.subtipo_item,
            DMCupomFiscal.dbQuery1.FieldByName('Est_Atual').AsFloat,
            reQuantidade.Value))) Then
          begin
            Mer_Bico;
          end
          else
          begin
            //Mercadoria  com Pre�o Exclusivo para Cliente
            If DMCupomFiscal.dbQuery1.FieldByName('MCF_Venda').AsCurrency > 0 then
              reVlrUnitario.Text:=FormatFloat('#,##0.0000',DMCupomFiscal.dbQuery1.FieldByName('MCF_Venda').AsCurrency)
            else
            begin
              //Pre�o de Tabela
              If (reVlrUnitario.Value=0) and (TAB_PRECO.TabPreco>0) then
              begin
                DMCupomFiscal.dbQuery2.Active:=False;
                DMCupomFiscal.dbQuery2.SQL.Clear;
                DMCupomFiscal.dbQuery2.SQL.Add('Select Vlr_Venda From Est_Mercadorias_Preco WHERE (Mercadoria=:pMer) and (TPreco=:pTP)');
                DMCupomFiscal.dbQuery2.ParamByName('pMer').AsString:=Trim(edtMercadoria.Text);
                DMCupomFiscal.dbQuery2.ParamByName('pTp').AsInteger:=TAB_PRECO.TabPreco;
                DMCupomFiscal.dbQuery2.Active:=True;
                DMCupomFiscal.dbQuery2.First;
                if DMCupomFiscal.dbQuery2.FieldByName('Vlr_Venda').AsCurrency>0 then
                  reVlrUnitario.Value := DMCupomFiscal.dbQuery2.FieldByName('Vlr_Venda').AsCurrency
                Else
                begin
                  reVlrUnitario.Value:=DMCupomFiscal.dbQuery1.FieldByName('Venda').AsCurrency;
                  msgInformacao('Cliente possui tabela de Pre�o Especial.'+#13+
                             'Mercadoria n�o possui Pre�o de Venda para esta Tabela.'+#13+
                             'Sera sugerido o valor NORMAL de venda da Mercadoria','Aviso');
                end;
              end
              else
              begin
                // Preco Normal
                reVlrUnitario.Value:=DMCupomFiscal.dbQuery1.FieldByName('Venda').AsCurrency;
              end;
            end;

            MERCADORIA.codigo     := Trim(edtMercadoria.Text);
            MERCADORIA.descricao  := DMCupomFiscal.dbQuery1.FieldByName('descricao').AsString;
            edtMercadoria.Text    := MERCADORIA.descricao;
            MERCADORIA.unidade    := DMCupomFiscal.dbQuery1.FieldByName('Unidade').AsString;
            MERCADORIA.icms       := DMCupomFiscal.dbQuery1.FieldByName('ICM_ECF').AsString;
            MERCADORIA.aliqISS    := DMCupomFiscal.dbQuery1.FieldByName('ISSQN').AsCurrency;
            MERCADORIA.cstPIS     := DMCupomFiscal.dbQuery1.FieldByName('Cst_Pis').AsString;
            MERCADORIA.cstCOFINS  := DMCupomFiscal.dbQuery1.FieldByName('Cst_Cofins').AsString;
            MERCADORIA.estoque    := DMCupomFiscal.dbQuery1.FieldByName('Est_Atual').AsCurrency;
            MERCADORIA.cstICMS    := DMCupomFiscal.dbQuery1.FieldByName('ECF_CST_ICM').AsString;
            MERCADORIA.cfop       := DMCupomFiscal.dbQuery1.FieldByName('ECF_CFOP').AsInteger;
            MERCADORIA.ncm        := ncm;
            MERCADORIA.percDescontoMax := DMCupomFiscal.dbQuery1.FieldByName('desconto').AsCurrency;
            if(FRENTE_CAIXA.Exibir_Vlr_Custo)then
              lblCusto.Caption := FormatFloat('R$ ###,##0.000', DMCupomFiscal.dbQuery1.FieldByName('Custo_Ultimo').AsCurrency);
          end;
        end;
      end;
    end
    Else
    begin
      msgInformacao('Mercadoria n�o cadastrada. Favor verificar!','Aten��o');
      Mer_Bico;
    end;
  end;
end;

procedure TfrmLoja.SubTotal;
begin
  //Verifica se tem estoque disponivel caso n�o permite venda com estoque Zero
  If (not(MERCADORIA.servico)) and not (verificarEstoque(MERCADORIA.codigo,
    MERCADORIA.Est_Vinculado, //Lauro 11.07.17
    MERCADORIA.subtipo_item, MERCADORIA.estoque, reQuantidade.Value)) Then
  begin
    reQuantidade.SetFocus;
  end
  else
  begin
    //Arredondar/Truncar Quantidade
    If FRENTE_CAIXA.Truncar then
    begin
      reQuantidade.Value := TruncarDecimal(reQuantidade.Value, FRENTE_CAIXA.Casas_Dec_Quantidade);
      sVlr := (reQuantidade.Value * reVlrUnitario.Value);
      reSubtotal.Value := TruncarDecimal(sVlr,2);
    end
    Else
    begin
      reQuantidade.Value := ArredondaDecimal(reQuantidade.Value, FRENTE_CAIXA.Casas_Dec_Quantidade);
      sVlr := (reQuantidade.Value * reVlrUnitario.Value);
      reSubtotal.Value := ArredondaDecimal(sVlr,2);
    end;

    FormataQtd; //Fomata casa decimais na quantidade;
    FormataVlrUni; //Fomata casa decimais Valor Unitario

    //Diminui valor de Desconto
    If reVlrDesconto.Value > 0 then  // verr
      reSubtotal.Value := reSubtotal.Value - reVlrDesconto.Value;

    reVlrDesconto.Text:=FormatFloat('##0.00',reVlrDesconto.value);
    reSubtotal.Text:=FormatFloat('#,##0.00',reSubtotal.value);

    tratarBotoes;
    //reVlrDesconto.Value:=0;
  end;
end;

procedure TfrmLoja.tmrScrollTimer(Sender: TObject);
begin
  txtMensagem.Left := txtMensagem.Left - 1;
  if(txtMensagem.Left <= pnlMensagem.Left - txtMensagem.Width)Then
    txtMensagem.Left := pnlMensagem.Width;
end;

procedure TfrmLoja.tratarBotoes;
begin
  if reSubtotal.Value > 0 then
  begin
    btnVende.Enabled:=True;
    btnCancela.Enabled:=True;
  end
  Else
  begin
    btnVende.Enabled:=False;
    btnCancela.Enabled:=True;
  end;
end;

procedure TfrmLoja.txtMensagemMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
const
  SC_DRAGMOVE = $F012;
begin
  begin
    ReleaseCapture;
    Self.Perform(WM_SYSCOMMAND, SC_DRAGMOVE, 0);
  end;
end;

procedure TfrmLoja.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  //Fechar Tela
  if (Key=vk_escape) then
  begin
    btnSair.Click;
  end;

  //Finalizar Cupom - F1
  if (Key=Vk_F1) then
  begin
    btnFinalizarCupom.Click;
  end;

  //Cancelar Cupom - F2
  if (Key=Vk_F2) then
  begin
    btnCancelarCupom.Click;
  end;

  //Cancelar Item - F3
  if (Key=Vk_F3) then
  begin
    btnCancelarItem.Click;
  end;

  //Pedido - F4
  if (Key=Vk_F4) then
  begin
    btnPedido.Click;
  end;

  //Abrir Gaveta - F5/ Cliente
  if (Key=Vk_F5) then
  begin
    btnGaveta.Click;
  end;

  //Consultar CR - F6
  if (Key=Vk_F6) then
  begin
    btnConsultarCR.Click;
  end;

  //Importar Comandas - F7
  if (Key=Vk_F7) then
  begin
    btnLerComandas.Click;
  end;

    //Consultar CR - F6
  if (Key=Vk_F9) then
  begin
    btnConsumidor.Click;
  end;

  //Consulta de Preco - F11
  If (key=vk_F11) then
    btnConsultaPreco.Click;

  //Pesquisa Mercadorias - F12
  If (key=vk_F12) then
  begin
    btnPesquisa.Click;
  end;

  //Alterar Operador - Ctrol + O
  if (Shift = [ssCtrl]) then
  begin
    //Saber c�digo da Tecla
    //ShowMessage(Format('O c�digo da tecla pressionada �: %d', [Key]));
    if Key=79 then
    begin
      if reTotal.Value > 0 then
      begin
        msgInformacao('N�o � poss�vel alterar o Operador ap�s iniciar a venda','');
      end
      else
      begin
        setCaixaUso(LOGIN.usuarioCod, 'N');
        frmLogin:=TfrmLogin.create(Application);
        frmLogin.ShowModal;
        frmLogin.Free;
        If LOGIN.usuarioCod  <=0 then
        begin
          msgErro('Operador n�o identificado','');
          Close;
        end
        else
        begin
          setDadosRodape;
        end;
      end;
    end;
  end;
end;

procedure TfrmLoja.FormMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
const
  SC_DRAGMOVE = $F012;
begin
  begin
    ReleaseCapture;
    Self.Perform(WM_SYSCOMMAND, SC_DRAGMOVE, 0);
  end;
end;

procedure TfrmLoja.FormResize(Sender: TObject);
begin
  pnlBackground.Left:=Trunc((frmLoja.Width/2)-(pnlBackground.Width/2));
  pnlBotoesOrganiza.Left:=Trunc((frmLoja.Width/2)-(pnlBotoesOrganiza.Width/2));
  pnlDadosIFOrganiza.Left:=Trunc((frmLoja.Width/2)-(pnlDadosIFOrganiza.Width/2));
  if(dmCupomFiscal.dbTemp_NFCECLIFOR.Value > 0)Then
  begin
    edtCliente.Text := dmCupomFiscal.dbTemp_NFCECLIFOR.AsString;
    edtClienteNome.Text := dmCupomFiscal.dbTemp_NFCECF_NOME.AsString;
  end;
end;

procedure TfrmLoja.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  dmCupomFiscal.dbTemp_NFCE_Item.Close;
  dmCupomFiscal.dbAbastecida.Close;
  Action := caFree; // Libera o formul�rio da mem�ria
  frmLoja := Nil;   // Deixa o formul�rio vazio
end;

procedure TfrmLoja.edtMercadoriaChange(Sender: TObject);
begin
  if(Trim(edtMercadoria.Text) = '')Then
    reVlrUnitario.Value := 0;
end;

procedure TfrmLoja.edtMercadoriaEnter(Sender: TObject);
begin
  Cor_Campo(Sender, HexToTColor(PERSONALIZAR.corFundoFoco));
  if Sender_Libera <> 'venda' then
  begin
    Sender_Libera:='';
  end;
end;

procedure TfrmLoja.limpaCampos;
begin
  //reVlrDesconto.Text:='0,00';
  reQuantidade.Value:=FRENTE_CAIXA.Qtd_Inicial;
  formataQtd;    //Formata Quantidade
  reVlrUnitario.Value:=0;
  reVlrDesconto.Value:=0;
  lblDescPerc.Caption:='0,00%';
  FormataVlrUni; //Formata Valor Unitario
  reSubtotal.Text:='0,00';
  la_Estoque.Caption:='Estoque';
  lblCusto.Caption := 'Custo';
  Abastecimento:=0;
  btnVende.Enabled:=False;
  btnCancela.Enabled:=False;
end;

procedure TfrmLoja.reVlrUnitarioExit(Sender: TObject);
begin
  if (Trim(reVlrUnitario.Text)='') then
  begin
    reVlrUnitario.SetFocus;
    msgAviso('Favor informar o Valor Unit�rio', '');
  end
  else
  begin
    Cor_Campo(Sender, HexToTColor(PERSONALIZAR.corFundoCampo));
    if valorUnitarioMaximoExcedeu(reVlrUnitario.Value) then
      reVlrUnitario.SetFocus
    else
      SubTotal;
  end;
end;

procedure TfrmLoja.btnCancelaClick(Sender: TObject);
begin
  Mer_Bico;
end;

procedure TfrmLoja.reVlrDescontoExit(Sender: TObject);
var pDes : Currency;
begin
  Cor_Campo(Sender, HexToTColor(PERSONALIZAR.corFundoCampo));
  if (Trim(reVlrDesconto.Text)='') then reVlrDesconto.Value:=0;

  if (reSubtotal.Value > 0) then
  begin
    if (reVlrDesconto.Value < 0) then //Se informado em percentual Sinal -
      reVlrDesconto.Value := (reQuantidade.Value * reVlrUnitario.Value) * -(reVlrDesconto.Value)/100;

    pDes := (reVlrDesconto.Value / reSubtotal.Value) * 100;
    lblDescPerc.Caption := FormatCurr('##0.00%',pDes);

    If (reVlrDesconto.Value >= (reQuantidade.Value * reVlrUnitario.Value)) then
    begin
      msgInformacao('Valor do desconto deve-ser inferior ao subtotal do Item!','');
      reVlrDesconto.Value:=0;
      reVlrDesconto.SetFocus;
    end
    else
    begin
      if MERCADORIA.percDescontoMax >= pDes then
      begin
        if(reVlrDesconto.Value <> vlrDesconto) and (reVlrDesconto.Value > 0)then
        begin
          if (getAutorizacao('DESCONTO_NFCE','DAR DESCONTO NO ITEM DA NFCe',reVlrDesconto.Value)) then
          begin
            If ((reQuantidade.Value * reVlrUnitario.Value) > 0) and (reVlrDesconto.Value > 0) then
              reSubtotal.Value := (reQuantidade.Value * reVlrUnitario.Value) - reVlrDesconto.Value;
            SubTotal;
          end
          else
          begin
            application.ProcessMessages;
            reVlrDesconto.Value:=vlrDesconto;
            pDes := (reVlrDesconto.Value / reSubtotal.Value) * 100;
            lblDescPerc.Caption := FormatCurr('##0.00%',pDes);
            application.ProcessMessages;
          end;
        end
        else
          SubTotal;
      end
      else
      begin
        msgInformacao(Format('Percentual m�ximo de desconto da mercadoria excedido.'+sLineBreak+
                            'Desconto m�ximo: %f %%'+sLineBreak+
                            'Desconto concedido: %f %%',[MERCADORIA.percDescontoMax, pDes]),'');
        reVlrDesconto.Value:=0;
        reVlrDesconto.SetFocus;
      end;
    end;
  end
  else
  begin
    if reVlrDesconto.Value <> 0 then
      reVlrDesconto.Value := 0;
  end;
  reVlrDesconto.Text  := FormatFloat('#,##0.00', reVlrDesconto.value);
end;

procedure TfrmLoja.btnVendeClick(Sender: TObject);
begin
  try
    //Valida��o caso mercadoria n�o possua NCM ou esteja invalido deixar Vender
    if ValidarNCM(MERCADORIA.ncm) then
    begin
      //Se n�o possuir registro temp_nfce insere
      if not(possuiRegistroTempECF(FRENTE_CAIXA.Caixa)) then
      begin
        try
          inserirTempNFCe();
        except
          on e:exception do
          begin
            dmCupomFiscal.dbTemp_NFCE.Transaction.RollbackRetaining;
            raise Exception.Create('[TEMP_NFCE] - Erro ao inserir registro na base de dados. '+#13+e.Message);
            //logErros(Self, caminhoLog, '[TEMP_NFCE] - Erro ao inserir registro na base de dados. '+e.Message, '[TEMP_NFCE] - Erro ao inserir registro na base de dados' , 'S', E);
          end;
        end;
      end;
      Sender_Libera          := 'venda';
      btnVende.Enabled       := False;
      btnCancela.Enabled     := False;
      MERCADORIA.vlrUnitario := reVlrUnitario.Value;
      MERCADORIA.quantidade  := reQuantidade.Value;
      MERCADORIA.vlrDesconto := reVlrDesconto.Value;
      try
        try
          DMCupomFiscal.dbTemp_NFCE_Item.Open;
          DMCupomFiscal.dbTemp_NFCE_Item.Append;
          DMCupomFiscal.dbTemp_NFCE_ItemCAIXA.Value      := FormatFloat('0000', FRENTE_CAIXA.caixa);
          DMCupomFiscal.dbTemp_NFCE_ItemITEM.Value       := FormatFloat('0000',0); //Ver controle item
          DMCupomFiscal.dbTemp_NFCE_ItemQUANTIDADE.Value := MERCADORIA.quantidade;
          DMCupomFiscal.dbTemp_NFCE_ItemVLR_UNITARIO.Value := MERCADORIA.vlrUnitario;
          DMCupomFiscal.dbTemp_NFCE_ItemVLR_TOTAL.Value  := reSubtotal.Value;
          DMCupomFiscal.dbTemp_NFCE_ItemQTD_CAN.Value    := 0;
          dmCupomFiscal.dbTemp_NFCE_ItemVLR_DESCONTO.Value := 0;
          dmCupomFiscal.dbTemp_NFCE_ItemVLR_DESCONTO_ITEM.Value := MERCADORIA.vlrDesconto;
          //DMCupomFiscal.dbTemp_NFCE_ItemBICO.Value       := 0;
          //DMCupomFiscal.dbTemp_NFCE_ItemABASTECIDA.Value := 0;
          DMCupomFiscal.dbTemp_NFCE_ItemMERCADORIA.Value := Trim(MERCADORIA.codigo);
          DMCupomFiscal.dbTemp_NFCE_ItemM_DESCRICAO.Value:= Trim(MERCADORIA.descricao);
          DMCupomFiscal.dbTemp_NFCE_ItemVLR_CUSTO_ATUAL.Value := 0; //Trigger busca
          DMCupomFiscal.dbTemp_NFCE_ItemICM.Value        := MERCADORIA.icms;
          DMCupomFiscal.dbTemp_NFCE_ItemCST_PIS.Value    := MERCADORIA.cstPIS;
          DMCupomFiscal.dbTemp_NFCE_ItemCST_COFINS.Value := MERCADORIA.cstCOFINS;
          DMCupomFiscal.dbTemp_NFCE_ItemCST_ICM.Value    := MERCADORIA.cstICMS;
          DMCupomFiscal.dbTemp_NFCE_ItemCFOP.Value       := MERCADORIA.cfop;
          DMCupomFiscal.dbTemp_NFCE_ItemNCM.Value        := MERCADORIA.ncm;
          DMCupomFiscal.dbTemp_NFCE_ItemUNIDADE.Value    := MERCADORIA.unidade;
          DMCupomFiscal.dbTemp_NFCE_ItemINDTOT.Value     := '1'; //--Inclui valor no Total da NF
          DMCupomFiscal.dbTemp_NFCE_ItemVLR_BC_ICM.Value := 0;
          DMCupomFiscal.dbTemp_NFCE_ItemALI_ICM.Value    := 0;
          DMCupomFiscal.dbTemp_NFCE_ItemVLR_ICM.Value    := 0;
          DMCupomFiscal.dbTemp_NFCE_ItemVLR_FRETE.Value  := 0;
          DMCupomFiscal.dbTemp_NFCE_ItemVLR_SEG.Value    := 0;
          DMCupomFiscal.dbTemp_NFCE_ItemVLR_OUTRO.Value  := 0;
          DMCupomFiscal.dbTemp_NFCE_ItemVLR_BC_PIS.Value := 0;
          DMCupomFiscal.dbTemp_NFCE_ItemALI_PIS.Value    := 0;
          DMCupomFiscal.dbTemp_NFCE_ItemVLR_PIS.Value    := 0;
          DMCupomFiscal.dbTemp_NFCE_ItemVLR_BC_COFINS.Value := 0;
          DMCupomFiscal.dbTemp_NFCE_ItemALI_COFINS.Value := 0;
          DMCupomFiscal.dbTemp_NFCE_ItemVLR_COFINS.Value := 0;
          DMCupomFiscal.dbTemp_NFCE_ItemVLR_TOTTRIB.Value:= 0;
          DMCupomFiscal.dbTemp_NFCE_ItemVLR_TOTTRIB_MUN.Value:= 0;
          DMCupomFiscal.dbTemp_NFCE_ItemVLR_TOTTRIB_EST.Value:= 0;
          DMCupomFiscal.dbTemp_NFCE_ItemVLR_SERVICO.value := 0;
          DMCupomFiscal.dbTemp_NFCE_ItemKIT.value := 'N';
          if(trim(ID_Comanda) <> '')Then
          begin
            DMCupomFiscal.dbTemp_NFCE_ItemID_COMANDA.Value   := ID_Comanda;
            DMCupomFiscal.dbTemp_NFCE_ItemCOMANDA_ITEM.Value := ITEM_Comanda;
            ID_Comanda := '';
          end;
          DMCupomFiscal.dbTemp_NFCE_Item.Post;
          DMCupomFiscal.dbTemp_NFCE_Item.Transaction.CommitRetaining;
        except
          on E:Exception do
          begin
            DMCupomFiscal.dbTemp_NFCE_Item.Transaction.RollbackRetaining;
            getTemp_NFCE_Item;
            raise Exception.Create('[TEMP_NFCE_ITEM] - Erro ao inserir item na base de dados.'+#13+e.Message);
          end;
        end;
      except
        on e: Exception do
        begin
          logErros(Self, caminhoLog, e.Message+#13+'Mercadoria: '+MERCADORIA.codigo+'-'+MERCADORIA.descricao, 'Erro ao vender Item. Mercadoria: '+MERCADORIA.codigo+'-'+MERCADORIA.descricao, 'S', E);
          if (ELChavePedido > 0) then
            raise Exception.Create('Erro');//Erro para retornar para tela de importa��o de pedidos para tratar o cancelamento do cupom
        end;
      end;
    end;
  finally
    btnVende.Enabled:=True;
    getTemp_NFCE_Item;
    Sender_Libera:='';
    Mer_Bico;
  end;
end;

procedure TfrmLoja.reQuantidadeEnter(Sender: TObject);
begin
  Cor_Campo(Sender, HexToTColor(PERSONALIZAR.corFundoFoco));
  If Trim(edtMercadoria.Text)='' then
  begin
    Mer_Bico;
    Abort;
  end;
end;

procedure TfrmLoja.Label1Click(Sender: TObject);
begin
  FrmPesquisaMercadoria := TFrmPesquisaMercadoria.Create(Self);
  try
    if (FrmPesquisaMercadoria.ShowModal=mrOK) then
      edtMercadoria.Text:= Trim(FrmPesquisaMercadoria.qMercadoria.FieldByName('Id').AsString);
  finally
    FrmPesquisaMercadoria.Free;
  end;
end;

procedure TfrmLoja.Mer_Bico;
begin
  if Sender_Libera='edtCliente' then   //anderson aqui
  begin
    if(edtCliente.Enabled)then
      edtCliente.SetFocus
  end
  Else
  begin
    edtMercadoria.SetFocus;
  end;

  edtMercadoria.Clear;
  LimpaCampos;
end;


procedure TfrmLoja.edtClienteExit(Sender: TObject);
begin
  edtCliente.Color := clWhite;
  edtCliente.font.Color := clBlack;
  TAB_PRECO.TabPreco := 0;
  If (Trim(edtCliente.Text)<>'') then
  begin
    DMCupomFiscal.dbQuery1.Active:=False;
    DMCupomFiscal.dbQuery1.SQL.Clear;
    DMCupomFiscal.dbQuery1.SQL.Add('Select c.ID as CLIFOR,c.Nome, c.TPreco, c.Limite, c.endereco as CF_END, '+
                                   'm.descricao as CF_MUNI, c.cnpjcpf as CF_CNPJ_CPF, '+
                                   'c.ie as CF_IE, c.numero_end as CF_NUMERO_END, '+
                                   'c.municipio as CF_MUNICIPIO, c.Bairro as CF_BAIRRO, '+
                                   'c.cep as CF_CEP, c.indiedest as INDIEDEST '+
                                   'FROM CliFor c '+
                                   'left outer join municipios m on m.id = c.municipio '+
                                   'Where c.ID=:pID and Ativo = ''S''');
    DMCupomFiscal.dbQuery1.ParamByName('pID').AsInteger:=StrToIntDef(edtCliente.Text,0);
    DMCupomFiscal.dbQuery1.Active:=True;
    DMCupomFiscal.dbQuery1.First;
    If not(DMCupomFiscal.dbQuery1.Eof) then
    begin
      edtClienteNome.Text := DMCupomFiscal.dbQuery1.FieldByName('Nome').AsString;
      TAB_PRECO.TabPreco  := DMCupomFiscal.dbQuery1.FieldByName('TPreco').AsInteger;
      Sender_Libera:='';
      Mer_Bico;
      //Apartir desse momento n�o altera mais o cliente, somente se o NFCE n�o
      //tiver mais itens adicionados
      //Se n�o possuir registro temp_nfce insere
      if not(possuiRegistroTempECF(FRENTE_CAIXA.Caixa)) then
      begin
        try
          inserirTempNFCe();
        except
          on e:exception do
          begin
            dmCupomFiscal.dbTemp_NFCE.Transaction.RollbackRetaining;
            raise Exception.Create('[TEMP_NFCE] - Erro ao inserir registro na base de dados. '+#13+e.Message);
            //logErros(Self, caminhoLog, '[TEMP_NFCE] - Erro ao inserir registro na base de dados. '+e.Message, '[TEMP_NFCE] - Erro ao inserir registro na base de dados' , 'S', E);
          end;
        end;
      end;
      dmCupomFiscal.getTemp_NFCE;
      if not(dmCupomFiscal.dbTemp_NFCE.State in [dsEdit, dsInsert])then
        dmCupomFiscal.dbTemp_NFCE.Edit;
      dmCupomFiscal.dbTemp_NFCECLIFOR.Value        := DMCupomFiscal.dbQuery1.FieldByName('CLIFOR').AsInteger;
      dmCupomFiscal.dbTemp_NFCECF_NOME.Value       := DMCupomFiscal.dbQuery1.FieldByName('Nome').AsString;
      dmCupomFiscal.dbTemp_NFCECF_ENDE.Value       := DMCupomFiscal.dbQuery1.FieldByName('CF_END').AsString;
      dmCupomFiscal.dbTemp_NFCECF_CNPJ_CPF.Value   := DMCupomFiscal.dbQuery1.FieldByName('CF_CNPJ_CPF').AsString;
      dmCupomFiscal.dbTemp_NFCECF_IE.Value         := DMCupomFiscal.dbQuery1.FieldByName('CF_IE').AsString;
      dmCupomFiscal.dbTemp_NFCECF_NUMERO_END.Value := DMCupomFiscal.dbQuery1.FieldByName('CF_NUMERO_END').AsString;
      dmCupomFiscal.dbTemp_NFCECF_MUNICIPIO.Value  := DMCupomFiscal.dbQuery1.FieldByName('CF_MUNICIPIO').AsInteger;
      dmCupomFiscal.dbTemp_NFCECF_CEP.Value        := DMCupomFiscal.dbQuery1.FieldByName('CF_CEP').AsString;
      dmCupomFiscal.dbTemp_NFCECF_BAIRRO.Value     := DMCupomFiscal.dbQuery1.FieldByName('CF_BAIRRO').AsString;
      dmCupomFiscal.dbTemp_NFCEINDIEDEST.Value     := DMCupomFiscal.dbQuery1.FieldByName('INDIEDEST').AsString;
      dmCupomFiscal.dbTemp_NFCE.Post;
      //Caso for diferente deleta formas de pagamento
      if(dmCupomFiscal.dbTemp_NFCECLIFOR.NewValue <> dmCupomFiscal.dbTemp_NFCECLIFOR.OldValue)then
      begin
        dmCupomFiscal.IBSQL1.close;
        dmCupomFiscal.IBSQL1.SQL.Clear;
        dmCupomFiscal.IBSQL1.SQL.Add('Delete from TEMP_NFCE_FORMAPAG where CAIXA ='+FormatFloat('0000',FRENTE_CAIXA.Caixa));
        dmCupomFiscal.IBSQL1.Prepare;
        dmCupomFiscal.IBSQL1.ExecQuery;
        dmCupomFiscal.IBSQL1.Close;
        dmCupomFiscal.Transaction.CommitRetaining;
      end;
      if(FRENTE_CAIXA.Exibir_Contas_Vencidas)then
        if not(crContasVencidas(dmCupomFiscal.dbTemp_NFCECLIFOR.Value))then
        begin
          Sender_Libera:='edtCliente';
          edtCliente.Enabled:=true;
          edtCliente.SetFocus;
          edtCliente.SelectAll;
          abort;
        end;
    end
    Else
    begin
      TAB_PRECO.TabPreco:=0;
      if not(dmCupomFiscal.dbTemp_NFCE.IsEmpty)Then
        dmCupomFiscal.LimparDadosCliente;
      msgAviso('Cliente n�o cadastrado/Inativo','');
      if(edtCliente.Enabled)then
      begin
        edtCliente.Clear;
        edtCliente.SetFocus;
      end;
    end;
  end
  else
  begin
    if not(dmCupomFiscal.dbTemp_NFCE.IsEmpty)Then
      dmCupomFiscal.LimparDadosCliente;
    edtCliente.Text:='';
    edtClienteNome.Text:='';
    Sender_Libera:='';
    Mer_Bico;
  end;
end;

procedure TfrmLoja.edtClienteEnter(Sender: TObject);
begin
  Cor_Campo(Sender, HexToTColor(PERSONALIZAR.corFundoFoco));
  edtClienteNome.Clear;
end;

procedure TfrmLoja.Label11Click(Sender: TObject);
begin
  if(edtCliente.Enabled)then
  begin
     FrmPesquisaCliente := TFrmPesquisaCliente.Create(Self);
     try
     if (FrmPesquisaCliente.ShowModal=mrOK) then
        edtCliente.Text:= FrmPesquisaCliente.qCliFor.FieldByName('ID').AsString;
     finally
        FrmPesquisaCliente.Free;
     end;
  end;
end;

procedure TfrmLoja.reSubtotalExit(Sender: TObject);
begin
  Cor_Campo(Sender, HexToTColor(PERSONALIZAR.corFundoCampo));
  //----------------- Venda Total ---------------------------------------------
  if (reQuantidade.Value <= 0)  and (reSubtotal.Value > 0) then
  begin
    reQuantidade.Value:=(reSubtotal.Value/reVlrUnitario.Value);
    If FRENTE_CAIXA.Truncar then
      reQuantidade.Value:=reQuantidade.Value + 0.001;
  end;
  //---------------------------------------------------------------------------
  SubTotal;
  tratarBotoes;
  if reSubtotal.Value > 0 then
    btnVende.SetFocus;
end;

procedure TfrmLoja.Label13Click(Sender: TObject);
begin
  If StrToIntDef(edtCliente.Text,0)>0 then
  begin
    FrmConsultaExtrato:=TFrmConsultaExtrato.create(Application);
    FrmConsultaExtrato.ShowModal;
    FrmConsultaExtrato.Free;
  end
  Else
  begin
    Sender_Libera:='edtCliente';
    if(edtCliente.Enabled)then
      edtCliente.SetFocus;
    msgInformacao('N�o foi informado o cliente!','');
  end;
end;

procedure TfrmLoja.DBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  If DMCupomFiscal.dbTemp_NFCE_ItemQUANTIDADE.AsCurrency = 0 then
  begin
    Dbgrid1.Canvas.Font.Color:= clRed; // coloque aqui a cor desejada
    //Dbgrid1.Canvas.Font.Style:=[fsItalic];
  end;
  Dbgrid1.DefaultDrawDataCell(Rect, dbgrid1.columns[datacol].field, State);
end;

procedure TfrmLoja.dsEcfDataChange(Sender: TObject; Field: TField);
begin
  if(dmCupomFiscal.dbTemp_NFCECLIFOR.AsInteger > 0)then
  begin
    edtCliente.Text := dmCupomFiscal.dbTemp_NFCECLIFOR.AsString;
    edtClienteNome.Text := dmCupomFiscal.dbTemp_NFCECF_NOME.AsString;
  end;
end;

procedure TfrmLoja.DsItemDataChange(Sender: TObject; Field: TField);
begin
  dmCupomFiscal.Flag_Cliente := dmCupomFiscal.ClientePossuiPrecoEsp('');
    edtCliente.Enabled := (dmCupomFiscal.Flag_Cliente);
end;

procedure TfrmLoja.edtClienteClick(Sender: TObject);
begin
  // Armazena no do edtCliente em variavel para poder sair do campo edtMercadoria
  if sender is TEdit then Sender_Libera:=TEdit(Sender).Name;
  Mer_Bico; //--- Anderson 22/08/13
end;


procedure TfrmLoja.BloqueiaVenda;
begin
  btnVende.Caption := 'Aguarde...';
  btnVende.Enabled := False;
  Application.ProcessMessages;
end;

procedure TfrmLoja.LiberaVenda;
begin
  btnVende.Caption := 'OK';
  btnVende.Enabled := True;
end;

procedure TfrmLoja.arrendarCampos;
begin
  ArredondarComponente(edtMercadoria,10);
  ArredondarComponente(reQuantidade,10);
  ArredondarComponente(reTotal,10);
  ArredondarComponente(reVlrUnitario,10);
  ArredondarComponente(reSubtotal,10);
  ArredondarComponente(reVlrDesconto,10);
end;

procedure TfrmLoja.ApplicationEvents1Hint(Sender: TObject);
begin
  barStatus.Panels[0].Text := ' '+Application.Hint;
end;

procedure TfrmLoja.btnSairClick(Sender: TObject);
begin
  if edtMercadoria.Text='' then
    if msgPergunta('Sair do Frente de Caixa ?','') then
      close;
end;

procedure TfrmLoja.btnCancelarItemClick(Sender: TObject);
begin
  if (reTotal.Value > 0) then
  begin
    FrmCancelaItem:=TFrmCancelaItem.Create(Application);
    FrmCancelaItem.ShowModal;
    FrmCancelaItem.Free;
    dmCupomFiscal.Flag_Cliente := dmCupomFiscal.ClientePossuiPrecoEsp('');
      edtCliente.Enabled :=(dmCupomFiscal.Flag_Cliente);
    getTemp_NFCE_Item;
    Mer_Bico;
  end;
end;

procedure TfrmLoja.btnCancelarCupomClick(Sender: TObject);
begin
  if (reTotal.Value > 0) or not(dmCupomFiscal.dbTemp_NFCE.IsEmpty) then
  begin
    if(dmCupomFiscal.TefEstaAtivo)then
    begin
      if(dmCupomFiscal.ExisteComprovanteTef(dmCupomFiscal.dbTemp_NFCEEL_CHAVE.AsInteger))then  //Lauro 25.10 passa el_Chave
      begin
        msgInformacao('Existe forma de pagamento TEF para essa NFC-e.'+sLineBreak+sLineBreak+
          'Voc� deve finalizar a NFC-e e depois cancelar as transa��es tef',Application.Title);
        Abort;
      end;
    end;
    cancelarCupomFiscal;
    Mer_Bico;
  end;
end;

procedure TfrmLoja.btnFinalizarCupomClick(Sender: TObject);
begin
  if (reTotal.Value > 0) then
  begin
    dmCupomFiscal.getTemp_NFCE;
    try
      //Se n�o vinculou o cliente ou o valor da venda � igual ou superior a 10000 abre a tela
      if(dmCupomFiscal.dbTemp_NFCECLIFOR.Value = 0) or
        (dmCupomFiscal.dbTemp_NFCEVLR_TOTAL.Value >= 10000) then
      begin
        frmClifor:=TfrmClifor.create(Application);
        try
          case frmClifor.ShowModal of
          mrOk:begin
              dmCupomFiscal.Transaction.CommitRetaining;
              dmCupomFiscal.getTemp_NFCE;
            end;
          mrCancel:begin
              dmCupomFiscal.Transaction.RollbackRetaining;
//              dmCupomFiscal.getTemp_NFCE;
              Abort;
            end;
          end;
        finally
          frmClifor.Free;
        end;
      end;
      FrmFim:=TFrmFim.create(Application);
      try
        case FrmFim.ShowModal of
        mrOk:begin
            dmCupomFiscal.Transaction.CommitRetaining;
          end;
        mrCancel:begin
            dmCupomFiscal.Transaction.RollbackRetaining;
            //Abort;
          end;
        end;
      finally
        FrmFim.Free;
      end;
    finally
      dmCupomFiscal.getTemp_NFCE;
      //lauro 23.01.17 ver se precisa !!! edtCliente.Enabled := not ((dmCupomFiscal.Flag_Cliente) or  (dmCupomFiscal.ExistePagamentoPrazo(FRENTE_CAIXA.Caixa)));
      if(dmCupomFiscal.dbTemp_NFCECLIFOR.Value<=0)then
      begin
        edtCliente.Clear;
        edtClienteNome.Clear;
      end;
      getTemp_NFCE_Item;
    end;
    Mer_Bico;
  end;
end;

procedure TfrmLoja.edtCCustoClick(Sender: TObject);
begin
  if sender is TEdit then Sender_Libera:=TEdit(Sender).Name;
  Mer_Bico;
end;

procedure TfrmLoja.FormShow(Sender: TObject);
begin
  try
    setDadosRodape;
    dmCupomFiscal.getTemp_NFCE;
    dmCupomFiscal.Flag_Cliente := True; //Lauro 23.01.17
    if(dmCupomFiscal.dbTemp_NFCECLIFOR.Value > 0)Then
    begin
      edtCliente.Text := dmCupomFiscal.dbTemp_NFCECLIFOR.AsString;
      edtClienteNome.Text := dmCupomFiscal.dbTemp_NFCECF_NOME.AsString;
      //Lauro 23.01.17
      dmCupomFiscal.Flag_Cliente := dmCupomFiscal.ClientePossuiPrecoEsp('');
      edtCliente.Enabled :=(dmCupomFiscal.Flag_Cliente);
    end;
    getTemp_NFCE_Item;
  except
    on e:Exception do
    begin
      if e is EDatabaseError then
        logErros(Self, caminhoLog,'Erro ao abrir Frente de Caixa'+#13+E.Message, 'Erro ao abrir Frente de Caixa','S',e);
      PostMessage(Handle, WM_CLOSE, 0, 0);  // Fechar o Form
    end;
  end;
end;

procedure TfrmLoja.FormCreate(Sender: TObject);
begin
  setCores;
  setMensagemDisplay;
  Application.ProcessMessages;
  arrendarCampos;
  limpaCampos;
  reVlrUnitario.ReadOnly := not FRENTE_CAIXA.Altera_Valor;
end;

procedure TfrmLoja.btnConsultarCRClick(Sender: TObject);
begin
  frmConsultaLimiteCR:=TfrmConsultaLimiteCR.create(Application);
  try
    if(dmCupomFiscal.dbTemp_NFCECLIFOR.Value > 0)then
    begin
      frmConsultaLimiteCR.edtCliente.Text := dmCupomFiscal.dbTemp_NFCECLIFOR.AsString;
      frmConsultaLimiteCR.edtClienteNome.Text := dmCupomFiscal.dbTemp_NFCECF_NOME.AsString;
      frmConsultaLimiteCR.edtClienteExit(Action);
    end;
    frmConsultaLimiteCR.ShowModal;
  finally
    frmConsultaLimiteCR.Free;
  end;
end;

procedure TfrmLoja.btnConsumidorClick(Sender: TObject);
begin
  edtMercadoria.Clear; // Para n�o executar o onexit
  if(dmCupomFiscal.dbTemp_NFCECLIFOR.Value > 0)then //Se ja vinculou um cliente abre a tela
  begin
    frmClifor:=TfrmClifor.create(Application);
    try
      case frmClifor.ShowModal of
      mrOk:begin
          dmCupomFiscal.Transaction.CommitRetaining;
          if(dmCupomFiscal.dbTemp_NFCECLIFOR.Value = 0)then
          begin
            edtCliente.Clear;
            edtClienteNome.Clear;
          end;
        end;
      mrCancel:begin
          dmCupomFiscal.Transaction.CommitRetaining;
          if(dmCupomFiscal.dbTemp_NFCECLIFOR.Value = 0)then
          begin
            edtCliente.Clear;
            edtClienteNome.Clear;
          end;
        end;
      end;
    finally
      frmClifor.Free;
    end;
  end
  else //Se n�o s� foca no campo
    if(edtCliente.Enabled)then
    begin
      Sender_Libera := 'edtCliente';
      edtCliente.SetFocus;
    end;
end;

procedure TfrmLoja.btnConsultaPrecoClick(Sender: TObject);
begin
  frmPesquisaCodigo := TfrmPesquisaCodigo.Create(Application);
  frmPesquisaCodigo.ShowModal;
  frmPesquisaCodigo.Free;
end;

procedure TfrmLoja.btnPesquisaClick(Sender: TObject);
begin
  //Pesquisa Mercadoria
  if (edtMercadoria.Focused) then
    Label1Click(Action);

  //Pesquisa Cliente
  If  (edtCliente.Focused) then
    Label11Click(Action);
end;

procedure TfrmLoja.reQuantidadeExit(Sender: TObject);
begin
  Cor_Campo(Sender, HexToTColor(PERSONALIZAR.corFundoCampo));
  if (Trim(reQuantidade.Text)='') then reQuantidade.Value:=FRENTE_CAIXA.Qtd_Inicial;
  if quantidadeMaximaExcedeu(reQuantidade.Value) then
    reQuantidade.SetFocus
  else
    SubTotal;
end;

procedure TfrmLoja.setCores;
begin
  pnlMensagem.Color:=HexToTColor(PERSONALIZAR.corFundoCampo);
  pnlMensagem.Font.Color:=HexToTColor(PERSONALIZAR.corFonte);
  txtMensagem.Color:=HexToTColor(PERSONALIZAR.corFundoCampo);
  txtMensagem.Font.Color:=HexToTColor(PERSONALIZAR.corFonte);
  pnlDadosIF.Color:=HexToTColor(PERSONALIZAR.corFundoCampo);
  pnlDadosIF.Font.Color:=HexToTColor(PERSONALIZAR.corFonte);
  pnlBotoes.Color:=HexToTColor(PERSONALIZAR.corFundoCampo);
  edtMercadoria.Color:=HexToTColor(PERSONALIZAR.corFundoCampo);
  edtMercadoria.Font.Color:=HexToTColor(PERSONALIZAR.corFonte);
  reQuantidade.Color:=HexToTColor(PERSONALIZAR.corFundoCampo);
  reQuantidade.Font.Color:=HexToTColor(PERSONALIZAR.corFonte);
  reVlrUnitario.Color:=HexToTColor(PERSONALIZAR.corFundoCampo);
  reVlrUnitario.Font.Color:=HexToTColor(PERSONALIZAR.corFonte);
  reVlrDesconto.Color:=HexToTColor(PERSONALIZAR.corFundoCampo);
  reVlrDesconto.Font.Color:=HexToTColor(PERSONALIZAR.corFonte);
  reSubtotal.Color:=HexToTColor(PERSONALIZAR.corFundoCampo);
  reSubtotal.Font.Color:=HexToTColor(PERSONALIZAR.corFonte);
  reTotal.Color:=HexToTColor(PERSONALIZAR.corFundoCampo);
  reTotal.Font.Color:=HexToTColor(PERSONALIZAR.corFonte);
  alterarCorFonteLabel(pnlDadosIF, HexToTColor(PERSONALIZAR.corFonte));
end;

procedure TfrmLoja.reVlrUnitarioEnter(Sender: TObject);
begin
  Cor_Campo(Sender, HexToTColor(PERSONALIZAR.corFundoFoco));
end;

procedure TfrmLoja.reVlrDescontoEnter(Sender: TObject);
begin
  Cor_Campo(Sender, HexToTColor(PERSONALIZAR.corFundoFoco));
  lblDescPerc.Caption:='0,00%';
  vlrDesconto := reVlrDesconto.Value;
end;

procedure TfrmLoja.reSubtotalEnter(Sender: TObject);
begin
  Cor_Campo(Sender, HexToTColor(PERSONALIZAR.corFundoFoco));
end;

procedure TfrmLoja.btnPedidoClick(Sender: TObject);
var
  F: textfile;
begin
  if (reTotal.Value > 0) then
  begin
    msgInformacao('N�o � poss�vel importar Pedido com NFC-e Pendente','');
  end
  else
  begin
      Application.CreateForm(TfrmImportaPedido, frmImportaPedido);
      try
        if(trim(edtCliente.Text) <> '')then
        begin
          frmImportaPedido.edtCliente.Text:=edtCliente.Text;
          frmImportaPedido.edtClienteNome.Text:=edtClienteNome.Text;
        end
        else
        begin
          frmImportaPedido.edtCliente.Text:='0';
          frmImportaPedido.edtClienteNome.Text:='TODOS OS CLIENTES...';
        end;
        frmImportaPedido.ShowModal;
      finally
        frmImportaPedido.Free;
        frmLoja.Resize;
      end;
  end;
end;

procedure TfrmLoja.btnGavetaClick(Sender: TObject);
var
  F: textfile;
  iRetorno: Integer;
begin
  if not(trim(FRENTE_CAIXA.GAV_Caminho) = '')then
  begin
    dmCupomFiscal.AcionarPeriferico(FRENTE_CAIXA.GAV_Caminho,FRENTE_CAIXA.GAV_Comando);
  end;
end;

procedure TfrmLoja.btnLerComandasClick(Sender: TObject);
var
  lListaComanda: TFrmListaComandas;
begin
  lListaComanda := TFrmListaComandas.Create(Self);
  try
    lListaComanda.ShowModal;
  finally
    lListaComanda.Free;
  end;
end;

end.
