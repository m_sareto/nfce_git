unit uFrmFim;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, TREdit, Buttons, DB, IBCustomDataSet, Mask,
  Grids, DBGrids, DBClient,IBQuery, ACBrBase, ACBrValidador, ACBrDevice,
  DBCtrls, ACBrUtil, pcnAuxiliar, ImgList, ComCtrls, pngimage, System.ImageList,
  uCtrlFim, uFrmClifor, ACBrTEFDClass, uCtrlTef;

type
  TfrmFim = class(TForm)
    cdsTotalizadorRateio: TClientDataSet;
    cdsTotalizadorRateioICM: TStringField;
    cdsTotalizadorRateioTOTALIZADOR_VLR_DESC: TFloatField;
    cdsTotalizadorRateioTOTALIZADOR_SOMA: TFloatField;
    dsTempECF: TDataSource;
    dsFormaPagamento: TDataSource;
    dsTemp_ECF_FormaPag: TDataSource;
    cdsFormaPagamento: TClientDataSet;
    cdsFormaPagamentoID: TIntegerField;
    cdsFormaPagamentoDESCRICAO: TStringField;
    cdsFormaPagamentoTPAG: TStringField;
    qurBandeiraCartao: TIBQuery;
    qurBandeiraCartaoFORMA_PAG: TIntegerField;
    qurBandeiraCartaoCARD_TBAND: TIBStringField;
    qurBandeiraCartaoDESCRICAO: TIBStringField;
    pnlFundo: TPanel;
    pnlGeral: TPanel;
    imgLista: TImageList;
    pagControle: TPageControl;
    tabPrincipal: TTabSheet;
    Panel2: TPanel;
    edtVlrDescontoParcial: TEdit;
    pnlRecebido: TPanel;
    edtVlrRecebido: TDBEdit;
    pnlTroco: TPanel;
    edtVlrTroco: TDBEdit;
    pnlSubtotal: TPanel;
    edtVlrSubtotal: TDBEdit;
    pnlTotal: TPanel;
    edtVlrTotal: TDBEdit;
    pnlDesconto: TPanel;
    lblDescPerc: TLabel;
    edtVlrDesconto: TDBEdit;
    pnlPagamento: TPanel;
    Label19: TLabel;
    Label20: TLabel;
    Label2: TLabel;
    grdTemp_ECF_FormaPag: TDBGrid;
    lcmbFormaPagamento: TDBLookupComboBox;
    btnPagar: TBitBtn;
    edtFormaPag: TEdit;
    tabOpcoes: TTabSheet;
    Label21: TLabel;
    Label22: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label1: TLabel;
    Label27: TLabel;
    Label3: TLabel;
    rgAmbiente: TDBRadioGroup;
    rgIndicadorPresenca: TDBRadioGroup;
    rgModalidadeFrete: TDBRadioGroup;
    memInfCpl: TDBMemo;
    memInfAdFisco: TDBMemo;
    edtModelo: TDBEdit;
    edtSerie: TDBEdit;
    edtData: TDBEdit;
    edtHora: TDBEdit;
    rgIndicadorPag: TDBRadioGroup;
    edtOperacao: TDBEdit;
    edtOperacaoDesc: TDBEdit;
    edtCCusto: TDBEdit;
    edtCCustoDescricao: TDBEdit;
    edtFuncionario: TDBEdit;
    edtFuncionarioNome: TDBEdit;
    pnlCabecalho: TPanel;
    pnlRodape: TPanel;
    Label7: TLabel;
    Label8: TLabel;
    pnlFinalizar: TPanel;
    btnSalvar: TBitBtn;
    pnlFinalizarOrganiza: TPanel;
    btnFinalizar: TSpeedButton;
    btnDesconto: TSpeedButton;
    btnFormaPag: TSpeedButton;
    btnConsumidor: TSpeedButton;
    lblNCaixa: TDBText;
    lblData: TDBText;
    btnCancelar: TSpeedButton;
    edtPagValor: TRealEdit;
    chkNFe: TCheckBox;
    pnlAcrescimo: TPanel;
    lblAcrescPerc: TLabel;
    edtVlrAcrescimos: TDBEdit;
    Label4: TLabel;
    edtConvenio: TDBEdit;
    edtConvenioDesc: TEdit;
    pnlLegenda: TPanel;
    GridPanel1: TGridPanel;
    Image1: TImage;
    Image2: TImage;
    Image3: TImage;
    Image4: TImage;
    Image5: TImage;
    Label6: TLabel;
    Label9: TLabel;
    procedure btnFinalizarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Label9Click(Sender: TObject);
    procedure edtOperacaoExit(Sender: TObject);
    procedure edtFuncionarioExit(Sender: TObject);
    function TruncarDecimal( Valor: Real; Decimal: integer): real;
    procedure Label3Click(Sender: TObject);
    procedure lblPlacaClick(Sender: TObject);
    procedure edtIEExit(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    function calcularMediaConsumo(mercadoria, placa : String; kmAtual : integer):Currency;
    function verificaTotalizadorTributado(icm:String):Boolean;
    procedure edtPlacaExit(Sender: TObject);
    procedure Label15Click(Sender: TObject);
    procedure Label27Click(Sender: TObject);
    procedure edtCCustoExit(Sender: TObject);
    procedure edtVlrDescontoEnter(Sender: TObject);
    procedure edtVlrDescontoExit(Sender: TObject);
    procedure btnPagarClick(Sender: TObject);
    procedure btnSalvarClick(Sender: TObject);
    procedure dsTempECFStateChange(Sender: TObject);
    procedure edtSerieKeyPress(Sender: TObject; var Key: Char);
    procedure grdTemp_ECF_FormaPagDrawColumnCell(Sender: TObject;
      const Rect: TRect; DataCol: Integer; Column: TColumn;
      State: TGridDrawState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure lcmbMunicipioEnter(Sender: TObject);
    procedure grdTemp_ECF_FormaPagMouseUp(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure Label1Click(Sender: TObject);
    function validarCPFCNPJ(cpfcnpj:String):Boolean;
    procedure edtFormaPagExit(Sender: TObject);
    procedure btnConsumidorClick(Sender: TObject);
    procedure btnPagamentoClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure grdTemp_ECF_FormaPagDblClick(Sender: TObject);
    procedure btnDescontoClick(Sender: TObject);
    procedure recalculaTotais();
    procedure edtPagValorExit(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure edtFormaPagKeyPress(Sender: TObject; var Key: Char);
    procedure dsTempECFDataChange(Sender: TObject; Field: TField);
    procedure edtVlrAcrescimosEnter(Sender: TObject);
    procedure edtVlrAcrescimosExit(Sender: TObject);
    procedure Label4Click(Sender: TObject);
    procedure edtConvenioExit(Sender: TObject);
    procedure edtFormaPagEnter(Sender: TObject);
    procedure edtFormaPagChange(Sender: TObject);
    procedure lblMotoristaClick(Sender: TObject);
  private
    Tecla : Word;
    Desconto : Currency;
    Acrescimo : Currency;
    fFinalizando: Boolean;
    { Private declarations }
//    function validarInformacoes():Boolean;
    function getDescontoItens():Currency;
    function getTotalPagTemp_NFCE_FormaPag():Currency;
    procedure setCores();
    procedure getDadosPedido(el_chavePedido:integer);
    procedure getDescontoPedido(el_chavePedido:integer);
    procedure getFormasPagamento();
    procedure getBandeirasCartao(forma_pag: Integer);
    procedure zerarPagamento();
    procedure EliminaCheque();
    procedure EliminarCartaFrete();
    procedure EliminarContaCorrente();
  public
      //Variavies globais desse objeto so pode ser acessado de outros frmFim.Obs ...
    wCF_DiasPrazo, NF_Operacao, ID :Integer;
    Obs, txtParcelas:String;
    { Public declarations }
    procedure RestauraFoco;
    Function getVlrTef(pChave: Integer): Currency;
//    procedure Gera_NFe;
//    function getSerieNFe():String;
  end;

var
  frmFim:TfrmFim;
  flagCliente : Boolean;
  OldCliente : Integer;

implementation

uses Math, DateUtils, MaskUtils, uFrmPesquisaCliente, uFrmGerarParcelas,
  uDMCupomFiscal, uFrmLoja, uFrmCheque, uFrmPesquisaGenerica,
  uFrmPesquisa, uFuncoes, ACBrECF, uDMComponentes, uFrmPrincipal,
  uRotinasGlobais, uFrmSupermercado, StrUtils, uFrmNFCe,
  uFrmBandeiraCartao, uFrmPosto, uFrmStatusNFCe, uFrmCartaFrete,
  uFrmContaCorrente;
{$R *.dfm}

function TfrmFim.TruncarDecimal( Valor: Real; Decimal: integer): real;
var aux: string;
begin
  valor := valor * 100000;
  aux := FormatFloat('00000000000000000000',valor);
  aux := copy( aux, 1, 15) + copy( aux, 16, Decimal);
  case Decimal of
    2: valor := strToFloat(aux) / 100;
    3: valor := strToFloat(aux) / 1000;
    4: valor := strToFloat(aux) / 10000;
    5: valor := strToFloat(aux) / 100000;
  end;
  result := Valor;
end;

procedure TfrmFim.btnFinalizarClick(Sender: TObject);
var media : Currency;
    numero, el_chave : Integer;
    Controler : TCtrlFim;
    lCtrTef: TCtrlTef;
begin
  Controler := TCtrlFim.Create;
  try
    btnFinalizar.Enabled:=False;
    fFinalizando := True; //Para bloquear o teclado da aplica��o
    try
      if(dmCupomFiscal.dbTemp_NFCE.state in [dsInsert, dsEdit])Then
        dmCupomFiscal.dbTemp_NFCE.Post;
      dmCupomFiscal.Transaction.CommitRetaining;
      if(Controler.finalizar(chkNFe.Checked,FRENTE_CAIXA.Caixa))then
      begin
        if(dmCupomFiscal.TefEstaAtivo)then
        begin
          lCtrTef := TCtrlTef.Create(dmCupomFiscal.DataBase, dmCupomFiscal.Transaction);
          try
            lCtrTef.ACBrTEFD.ConfirmarTransacoesPendentes(True);
          finally
            lCtrTef.Free;
          end;
        end;
        frmNFCe.ID_NF_ENVIAR := Controler.ID;
        frmNFCe.ID_CCUSTO_ENVIAR := dmCupomFiscal.dbTemp_NFCECCUSTO.AsInteger;
        Try
          frmNFCe.FormActivate(Self);
        //Fecha tela finaliza��o
        finally
          ModalResult := mrOk;
        End;
      end
      else
      begin
        dmCupomFiscal.IBSQL1.Close;
        dmCupomFiscal.IBSQL1.SQL.Clear;
        dmCupomFiscal.IBSQL1.SQL.Add('Update temp_nfce Set status=''T'' Where Caixa=:pCaixa');
        dmCupomFiscal.IBSQL1.ParamByName('pCaixa').asString:=FormatFloat('0000', FRENTE_CAIXA.Caixa);
        dmCupomFiscal.IBSQL1.ExecQuery;
        dmCupomFiscal.IBSQL1.Close;
      end;
    except
      on E:Exception do
      begin
        dmCupomFiscal.Transaction.RollbackRetaining;
        if(e.Message <> 'Operation aborted')Then
          msgErro('Erro ao finalizar NFC-e.'+#13+#13+E.Message, 'Erro');
      end;
    end;
  finally
    fFinalizando := false;
    Controler.Free;
    frmFim.Enabled := True;
    btnFinalizar.Enabled:=True;
  end;
end;

procedure TfrmFim.FormShow(Sender: TObject);
begin
  fFinalizando := False;
  flagCliente := True; // Lauro 27.06.2016 Ativou pois n�o verificava LImite quando era Supermercado
  getFormasPagamento();
  pnlAcrescimo.Visible := FRENTE_CAIXA.Exibir_Acrescimos;
  recalculaTotais;
  case (FRENTE_CAIXA.Foco_Fim) of
  0:edtVlrDesconto.SetFocus;
  1:edtVlrAcrescimos.SetFocus;
  2:edtFormaPag.SetFocus;
  end;
end;

procedure TfrmFim.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Msg: TMsg;
begin
  if(fFinalizando)Then
  begin
    while PeekMessage(Msg, 0, WM_KEYFIRST, WM_KEYLAST, PM_REMOVE or PM_NOYIELD) do;
    Abort;
  end;

  Tecla := Key;

  //Fun
  If (key=vk_F12) and (edtSerie.Focused) then Label15Click(Action);

  //Operacao
  If (key=vk_F12) and (edtOperacao.Focused) then Label1Click(Action);

  //Esc
  If (key=vk_Escape) then ModalResult := mrCancel;

  //Funcionario
  If (key=vk_F12) and (edtFuncionario.Focused) then Label3Click(Action);

  //Convenio
  If (key=vk_F12) and (edtConvenio.Focused) then Label4Click(Action);

  //Desconto
  if (Key=VK_F2) and (edtVlrDesconto.Enabled) then
  begin
    pagControle.ActivePageIndex := 0;
    edtVlrDesconto.SetFocus;
    edtVlrDesconto.SelectAll;
  end;

  //F. pag.
  if (Key=VK_F3) and (edtFormaPag.Enabled) then
  begin
    pagControle.ActivePageIndex := 0;
    edtFormaPag.SetFocus;
    edtFormaPag.SelectAll;
  end;
  //Consumidor
  if (Key=VK_F9) then
  begin
    btnConsumidorClick(Action);
  end;

  if (Key=VK_F8) then
  begin
    pagControle.ActivePage := tabOpcoes;
    memInfCpl.SetFocus;
  end;

  if (Key=VK_F7) then
    pagControle.ActivePage := tabPrincipal;

  //Finalizar com F1
  If (key=vk_F1) and (btnFinalizar.Enabled) then
  begin
    if(dmCupomFiscal.dbTemp_NFCE.state in [dsInsert, dsEdit])Then
      dmCupomFiscal.dbTemp_NFCE.Post;
    pagControle.ActivePageIndex := 0;
    if(edtVlrDesconto.Focused)Then
      edtVlrDescontoExit(action);
    If(dmCupomFiscal.dbTemp_NFCEVLR_TROCO.Value < 0)then //Ainda Falta Pagamento
    begin
      edtFormaPag.Text := IntToStr(FRENTE_CAIXA.Forma_Pag);
      if(cdsFormaPagamento.Locate('ID',FRENTE_CAIXA.Forma_Pag,[loCaseInsensitive]))then
      begin
        if btnPagar.Enabled then
        begin
          btnPagar.Click;
          if btnFinalizar.Enabled then
            btnFinalizar.Click;
        end;
      end
      else begin
        msgInformacao('Forma de pagamento padr�o '+intToStr(FRENTE_CAIXA.Forma_Pag)+' n�o encontrada','Informa��o');
        Abort;
      end;
    end
    else
    begin
      pagControle.ActivePageIndex := 0;
      if btnFinalizar.Enabled then
        btnFinalizar.Click;
    end;
  end;
  //Se estiver fazendo o envio n�o permite clicar no enter
  if(key = VK_RETURN) and (frmStatusNFCe <> nil)then
    key := 0;
end;

procedure TfrmFim.Label9Click(Sender: TObject);
begin
  FrmPesquisaCliente := TFrmPesquisaCliente.Create(Self);
  try
    if (FrmPesquisaCliente.ShowModal=mrOK) then
    begin
      if not(dmCupomFiscal.dbTemp_NFCE.State in [dsEdit, dsInsert]) then
        dmCupomFiscal.dbTemp_NFCE.Edit;
      //edtCliente.Text:= FrmPesquisaCliente.qCliFor.FieldByName('ID').AsString;
    end;
  finally
    FrmPesquisaCliente.Free;
  end;
end;

procedure TfrmFim.edtOperacaoExit(Sender: TObject);
begin
  DMCupomFiscal.dbQuery1.Active:=False;
  DMCupomFiscal.dbQuery1.SQL.Clear;
  DMCupomFiscal.dbQuery1.SQL.Add('Select Entrada_Saida, ID, Descricao, Estoque, Forma_Pag FROM Operacoes Where ID=:pCod');
  DMCupomFiscal.dbQuery1.ParamByName('pCod').AsInteger:=StrToIntDef(edtOperacao.Text,0);
  DMCupomFiscal.dbQuery1.Active:=True;
  DMCupomFiscal.dbQuery1.First;
  If not(DMCupomFiscal.dbQuery1.IsEmpty) then
  begin
    If (DMCupomFiscal.dbQuery1.FieldByName('Entrada_Saida').Text='E') Then
    begin
      msgAviso('Favor informar uma Opera��o de Sa�da/Venda','');
      edtOperacao.SetFocus;
    end
    else
    begin
      edtOperacaoDesc.Text:=DMCupomFiscal.dbQuery1.FieldByName('Descricao').AsString;
      OPERACAO.operarEstoque := DMCupomFiscal.dbQuery1.FieldByName('Estoque').Text;
    end;
  end
  else
  begin
    msgAviso('Opera��o n�o cadastrada!','');
      if not(dmCupomFiscal.dbTemp_NFCE.State in [dsEdit, dsInsert])Then
        dmCupomFiscal.dbTemp_NFCE.Edit;
    dmCupomFiscal.dbTemp_NFCEOPERACAO.AsInteger := FRENTE_CAIXA.Operacao;
    edtOperacao.Text := IntToStr(FRENTE_CAIXA.Operacao);
    edtOperacao.SetFocus;
  end;
end;

procedure TfrmFim.edtFuncionarioExit(Sender: TObject);
begin
  DMCupomFiscal.dbQuery1.Active:=False;
  DMCupomFiscal.dbQuery1.SQL.Clear;
  DMCupomFiscal.dbQuery1.SQL.Add('Select id, nome, ativo FROM Funcionarios Where ID=:pCod');
  DMCupomFiscal.dbQuery1.ParamByName('pCod').AsInteger:=StrToIntDef(edtFuncionario.Text,0);
  DMCupomFiscal.dbQuery1.Active:=True;
  DMCupomFiscal.dbQuery1.First;
  If not(DMCupomFiscal.dbQuery1.Eof) then
  begin
    edtFuncionarioNome.Text := DMCupomFiscal.dbQuery1.FieldByName('Nome').AsString;
    if (DMCupomFiscal.dbQuery1.FieldByName('Ativo').AsString='N') then
    begin
      msgInformacao('Funcion�rio est� Inativo. Favor informar funcion�rio Ativo!','');
      edtFuncionario.SetFocus;
    end;
  end
  Else
  begin
    msgAviso('Funcion�rio n�o cadastrado!','');
    edtFuncionario.SetFocus;
  end;
end;

procedure TfrmFim.Label3Click(Sender: TObject);
begin
  frmPesquisa := tfrmPesquisa.Create(Self,'Select id, nome From Funcionarios Where (Nome >=:pTexto) and (Ativo=''S'') Order By Nome','Select Id, Nome From Funcionarios Where (Nome like :pTexto) and (Ativo=''S'') Order By Nome');

  frmPesquisa.DBGrid1.Columns.Add;
  frmPesquisa.DBGrid1.Columns[0].Title.Caption:='C�digo';
  frmPesquisa.DBGrid1.Columns[0].Width:=42;
  frmPesquisa.DBGrid1.Columns[0].FieldName:='id';

  frmPesquisa.DBGrid1.Columns.Add;
  frmPesquisa.DBGrid1.Columns[1].Title.Caption:='Nome';
  frmPesquisa.DBGrid1.Columns[1].Width:=337;
  frmPesquisa.DBGrid1.Columns[1].FieldName:='Nome';
  try
    if (frmPesquisa.ShowModal=mrOK) then
    begin
      if not(dmCupomFiscal.dbTemp_NFCE.State in [dsEdit, dsInsert]) then
        dmCupomFiscal.dbTemp_NFCE.Edit;
      dmCupomFiscal.dbTemp_NFCEFUNCIONARIO.AsInteger := FrmPesquisa.IBQuery1.FieldByName('id').Value;
    end;
  finally
    frmPesquisa.Free;
  end;
end;

procedure TfrmFim.Label4Click(Sender: TObject);
begin
  frmPesquisa := tfrmPesquisa.Create(Self,'Select id, Descricao From Convenios Where (Descricao >=:pTexto) Order By Descricao','Select Id, Descricao From Convenios Where (Descricao like :pTexto) Order By Descricao');

  frmPesquisa.DBGrid1.Columns.Add;
  frmPesquisa.DBGrid1.Columns[0].Title.Caption:='C�digo';
  frmPesquisa.DBGrid1.Columns[0].Width:=42;
  frmPesquisa.DBGrid1.Columns[0].FieldName:='id';

  frmPesquisa.DBGrid1.Columns.Add;
  frmPesquisa.DBGrid1.Columns[1].Title.Caption:='Descri��o';
  frmPesquisa.DBGrid1.Columns[1].Width:=337;
  frmPesquisa.DBGrid1.Columns[1].FieldName:='Descricao';
  try
    if (frmPesquisa.ShowModal=mrOK) then
    begin
      if not(dmCupomFiscal.dbTemp_NFCE.State in [dsEdit, dsInsert]) then
        dmCupomFiscal.dbTemp_NFCE.Edit;
      dmCupomFiscal.dbTemp_NFCEFUNCIONARIO.AsInteger := FrmPesquisa.IBQuery1.FieldByName('id').Value;
    end;
  finally
    frmPesquisa.Free;
  end;
end;

procedure TfrmFim.lblMotoristaClick(Sender: TObject);
begin
  frmPesquisa := tfrmPesquisa.Create(Self,'Select id, Descricao From MOTORISTAS Where (Descricao >=:pTexto) Order By Descricao',
    'Select Id, Descricao From MOTORISTAS Where (Descricao like :pTexto) Order By Descricao');

  frmPesquisa.DBGrid1.Columns.Add;
  frmPesquisa.DBGrid1.Columns[0].Title.Caption:='C�digo';
  frmPesquisa.DBGrid1.Columns[0].Width:=42;
  frmPesquisa.DBGrid1.Columns[0].FieldName:='id';

  frmPesquisa.DBGrid1.Columns.Add;
  frmPesquisa.DBGrid1.Columns[1].Title.Caption:='Descri��o';
  frmPesquisa.DBGrid1.Columns[1].Width:=337;
  frmPesquisa.DBGrid1.Columns[1].FieldName:='Descricao';
  try
    if (frmPesquisa.ShowModal=mrOK) then
    begin
      if not(dmCupomFiscal.dbTemp_NFCE.State in [dsEdit, dsInsert]) then
        dmCupomFiscal.dbTemp_NFCE.Edit;
      dmCupomFiscal.dbTemp_NFCECF_MOTORISTA.AsString := trim(Copy(FrmPesquisa.IBQuery1.FieldByName('Descricao').Value,1,30));
    end;
  finally
    frmPesquisa.Free;
  end;
end;

procedure TfrmFim.lblPlacaClick(Sender: TObject);
begin
  frmPesquisaGenerica := tfrmPesquisaGenerica.Create(Self,'Transportadores');
  try
    if (frmPesquisaGenerica.ShowModal=mrOK) then
      //edtPlaca.Text:= FrmPesquisaGenerica.IBQuery1.FieldByName('id').AsString;
  finally
    frmPesquisaGenerica.Free;
  end;

  // Lauro 21.04.2016 ---------------------
{

  if StrToIntDef(edtCliente.Text,0) > 0 then
  begin
    frmPesquisaPlaca := tfrmPesquisaPlaca.Create(Self,'Select f.placa, f.descricao, c.id clifor, c.nome clifor_nome From Clifor_Frota f'+
         ' left outer join clifor c on c.id=f.clifor'+
         ' Where c.ativo=''S'' and f.clifor='+Trim(edtCliente.Text)+
         ' Order By f.Placa', '');
  end
  else
  begin
    frmPesquisaPlaca := tfrmPesquisaPlaca.Create(Self,'Select f.placa, f.descricao, c.id clifor, c.nome clifor_nome From Clifor_Frota f'+
      ' left outer join clifor c on c.id=f.clifor'+
      ' Where (f.placa >= :pTexto) and c.ativo=''S'' Order By c.id, f.Placa',

    'Select f.placa, f.descricao, c.id clifor, c.nome clifor_nome From Clifor_Frota f'+
      ' left outer join clifor c on c.id=f.clifor'+
      ' Where (f.placa like :pTexto) and c.ativo=''S'' Order By c.id, f.Placa');
  end;
  frmPesquisaPlaca.DBGrid1.Columns.Add;
  frmPesquisaPlaca.DBGrid1.Columns[0].Title.Caption:='Placa';
  frmPesquisaPlaca.DBGrid1.Columns[0].Width:=60;
  frmPesquisaPlaca.DBGrid1.Columns[0].FieldName:='placa';

  frmPesquisaPlaca.DBGrid1.Columns.Add;
  frmPesquisaPlaca.DBGrid1.Columns[1].Title.Caption:='Descri��o';
  frmPesquisaPlaca.DBGrid1.Columns[1].Width:=150;
  frmPesquisaPlaca.DBGrid1.Columns[1].FieldName:='descricao';

  frmPesquisaPlaca.DBGrid1.Columns.Add;
  frmPesquisaPlaca.DBGrid1.Columns[2].Title.Caption:='Cliente';
  frmPesquisaPlaca.DBGrid1.Columns[2].Width:=350;
  frmPesquisaPlaca.DBGrid1.Columns[2].FieldName:='clifor_nome';
  frmPesquisaPlaca.Width:=600;
  try
    if (frmPesquisaPlaca.ShowModal=mrOK) then
    begin
      edtPlaca.Text:= frmPesquisaPlaca.IBQuery1.FieldByName('placa').AsString;
      if not(frmPesquisaPlaca.IBQuery1.FieldByName('clifor').IsNull) then
      begin
        if Trim(edtCliente.Text)='' then
        begin
          edtCliente.Text  :=frmPesquisaPlaca.IBQuery1.FieldByName('clifor').AsString;
          edtClienteExit(edtCliente);
        end;
      end
    end;
  finally
    frmPesquisaPlaca.Free;
  end;
}
end;

procedure TfrmFim.edtIEExit(Sender: TObject);
begin
{ Novo falta definir UF - Lauro

  if CHKCampo_Obrigatorio(edtIE.Text) then edtIE.SetFocus;
  ACBrValidador1.TipoDocto := docInscEst;
  ACBrValidador1.Documento   := Trim(edtIE.Text);
  ACBrValidador1.Complemento := DBComboBox1.Text;
  if not (ACBrValidador1.Validar) then
  begin
    DBEdit7.SetFocus;
    msgInformacao(ACBrValidador1.MsgErro, 'Aten��o');
  end;
}
end;

procedure TfrmFim.FormCreate(Sender: TObject);
begin
  dmComponentes.fCancelado := False ;
  //Para alinhar Edit da Esquerda para Direita
  SysLocale.MiddleEast := True;
  edtVlrDescontoParcial.BiDiMode := bdRightToLeft;
  edtVlrDescontoParcial.Text := FormatFloat('##,###0.00',getDescontoItens());

  setCores;
  pagControle.ActivePage:=tabPrincipal;
  dmCupomFiscal.getTemp_NFCE();
  //getTemp_NFCE_FormaPag();  //Onshow
  dmCupomFiscal.getTemp_NFCE_Fatura();
  //getMunicipios('');
end;

procedure TfrmFim.setCores;
begin
  pnlCabecalho.Color      :=  HexToTColor(PERSONALIZAR.corFundoCampo);
  pnlCabecalho.Font.Color :=  HexToTColor(PERSONALIZAR.corFonte);
  pnlFinalizar.Color      :=  HexToTColor(PERSONALIZAR.corFundoCampo);
  pnlRodape.Color         :=  HexToTColor(PERSONALIZAR.corFundoCampo);
  Label7.Font.Color       :=  HexToTColor(PERSONALIZAR.corFonte);
  Label8.Font.Color       :=  HexToTColor(PERSONALIZAR.corFonte);
  lblData.Font.Color      :=  HexToTColor(PERSONALIZAR.corFonte);
  lblNCaixa.Font.Color    :=  HexToTColor(PERSONALIZAR.corFonte);
end;

function TfrmFim.calcularMediaConsumo(mercadoria, placa: String;
  kmAtual: integer): Currency;
var
  kmAnterior : integer;
  litros, media : Currency;
begin
  media:=0;
  dmCupomFiscal.dbQuery2.Active:=False;
  dmCupomFiscal.dbQuery2.SQL.Clear;
  dmCupomFiscal.dbQuery2.SQL.Add('select e.id, e.cf_km, i.qtd from est_ecf e'+
                                 ' left outer join est_ecf_item i on i.id_ecf=e.id'+
                                 ' where e.id=(select max(e1.id) from est_ecf e1'+
                                 ' where e1.cf_placa=:pPlaca) and (i.mercadoria=:pMercadoria)');
  dmCupomFiscal.dbQuery2.ParamByName('pPlaca').AsString := placa;
  dmCupomFiscal.dbQuery2.ParamByName('pMercadoria').AsString := mercadoria;
  dmCupomFiscal.dbQuery2.Active:=True;
  dmCupomFiscal.dbQuery2.First;
  if not (dmCupomFiscal.dbQuery2.Eof) then
  begin
    kmAnterior := dmCupomFiscal.dbQuery2.FieldByName('cf_km').AsInteger;
    litros := dmCupomFiscal.dbQuery2.FieldByName('qtd').AsCurrency;
    if kmAnterior > 0 then
    begin
{      ShowMessage(IntToStr(kmAtual));
      ShowMessage(IntToStr(kmAnterior));
      ShowMessage(CurrToStr(litros));}
      media := (kmAtual - kmAnterior) / litros;
    end;
  end;
  Result:=media;
end;

{function TfrmFim.validarInformacoes: Boolean;
var retorno:Boolean;
    vlrTotalParcelas, vtotparc:Currency;
begin
  try
    vlrTotalParcelas := 0;
    //Cheque  18.04.16
    dmCupomFiscal.dbTemp_NFCE_FormaPag.First;
    If (dmCupomFiscal.dbTemp_NFCE_FormaPag.Locate('TPAG', '02', [loCaseInsensitive])) then //02 - Cheque
    begin
      if dmCupomFiscal.dbTemp_NFCE_FormaPagVPAG.Value > 0 then
      begin
        dmCupomFiscal.dbQuery1.Active:=False;
        dmCupomFiscal.dbQuery1.SQL.Clear;
        dmCupomFiscal.dbQuery1.SQL.Add('Select Sum(Valor) as Total From TEMP_NFCE_FORMAPAG_CHEQUE Where Caixa=:pCaixa');
        dmCupomFiscal.dbQuery1.ParamByName('pCaixa').AsString:=FormatFloat('0000', FRENTE_CAIXA.Caixa);
        dmCupomFiscal.dbQuery1.Active:=True;
        dmCupomFiscal.dbQuery1.First;
        if (dmCupomFiscal.dbQuery1.FieldByName('Total').AsCurrency<>dmCupomFiscal.dbTemp_NFCE_FormaPagVPAG.Value) then
        begin
          edtFormaPag.SetFocus;
          raise Exception.Create('Valor Total do(s) Cheque(s) Difere do Informado.'+#13+
                                 'Para Corrigir: Zerar o Valor Informado em Cheque e Lan�ar Novamente!');
        end;
      end;
    end;


    if Trim(dmCupomFiscal.dbTemp_NFCECF_MUNICIPIO.AsString) <> '' then
    begin
      if Trim(dmCupomFiscal.dbTemp_NFCECF_MUNICIPIO.AsString) = '' then
        raise Exception.Create('Obrigat�rio informar CPF/CNPJ ao identificar consumidor');
    end;

    //Valida informacoes endereco
    if not(dmCupomFiscal.dbTemp_NFCECF_MUNICIPIO.IsNull) or not(dmCupomFiscal.dbTemp_NFCECF_ENDE.IsNull) or
       not(dmCupomFiscal.dbTemp_NFCECF_NUMERO_END.IsNull) or not(dmCupomFiscal.dbTemp_NFCECF_CEP.IsNull) or
       not(dmCupomFiscal.dbTemp_NFCECF_BAIRRO.IsNull) or not(dmCupomFiscal.dbTemp_NFCECF_CEP.IsNull) then
    begin
      if (dmCupomFiscal.dbTemp_NFCECF_MUNICIPIO.IsNull) or (dmCupomFiscal.dbTemp_NFCECF_ENDE.IsNull) or
         (dmCupomFiscal.dbTemp_NFCECF_NUMERO_END.IsNull) or (dmCupomFiscal.dbTemp_NFCECF_CEP.IsNull) or
         (dmCupomFiscal.dbTemp_NFCECF_BAIRRO.IsNull) or (dmCupomFiscal.dbTemp_NFCECF_CEP.IsNull) then
      begin
        raise Exception.Create('Obrigat�rio informar todos dados do Endere�o ou deixar todos em branco');
      end;
    end;
    }
    // Lauro 21.04.2016
{    if Not(ValidaFrota( StrToIntDef(edtCliente.Text,0), Trim(edtPlaca.Text) )) then
    begin
      raise Exception.Create('Este Cliente Possui Frota.'+#13+
                             'Esta Placa N�o Consta em Sua Frota.'+#13+
                             'Verifique a Placa!');
    end;}
    //--Lauro Fim 21.04.2016
       
    {if (dmCupomFiscal.dbTemp_NFCEVLR_TOTAL.Value >= 10000) and
      (dmCupomFiscal.dbTemp_NFCECF_NOME.IsNull or dmCupomFiscal.dbTemp_NFCECF_ENDE.IsNull)then //Se valor maior ou igual a 10k informar cliente
    begin
      raise Exception.Create('Obrigat�rio informar dados do consumidor quando'+#13+
                             'valor da venda for igual ou superior a R$10.000,00');
    end;

    //Valida Falta Pagamento
    if dmCupomFiscal.dbTemp_NFCEVLR_TROCO.Value < 0 then
    begin
      edtPagValor.SetFocus;
      raise Exception.Create('Total do pagamento n�o pode ser INFERIOR ao valor total da NFC-e');
    end;

    //Valida venda no credi�rio, precisa verificar o caixa...
    if dmCupomFiscal.dbTemp_NFCE_FormaPag.Locate('TPAG;CAIXA',VarArrayOf(['05',FormatFloat('0000', FRENTE_CAIXA.Caixa)]), [loCaseInsensitive]) then
    begin
      if dmCupomFiscal.dbTemp_NFCE_FormaPagVPAG.Value > 0 then
      begin
{        if not(dmCupomFiscal.dbTemp_NFCECLIFOR.Value > 0) then
        begin
          edtCliente.SetFocus;
          raise Exception.Create('Obrigat�rio informar Consumidor quando venda for "Cr�dito da Loja"');
        end;
        //-- Lauro Verifica Limite 21.04.2016
        if (crProtecaoCredito(StrToIntDef(edtCliente.Text,0))) then
        begin
          edtCliente.SetFocus;
          Abort;
        end;

        If (crExcedeuLimite(StrToIntDef(edtCliente.Text,0), dmCupomFiscal.dbTemp_NFCE_FormaPagVPAG.AsCurrency)) then
        begin
          edtCliente.SetFocus;
          Abort;
        end;

        if (FRENTE_CAIXA.CR_Baixa_Seletiva) then
          crContasVencidas(StrToIntDef(edtCliente.Text,0));}
        //-- Lauro Fim 21.04.2016
     { end;

      //Verifica se nfce � vinculado ao um pedido
      dmCupomFiscal.dbQuery1.Active := false;
      dmCupomFiscal.dbQuery1.SQL.Clear;
      dmCupomFiscal.dbQuery1.SQL.Add('Select coalesce(n.EL_CHAVE,0) as EL_CHAVE from temp_nfce n '+
                                     'LEFT OUTER JOIN est_pedidos p ON p.EL_CHAVE = n.EL_CHAVE '+
                                     'where n.CAIXA = :pCaixa');
      dmCupomFiscal.dbQuery1.ParamByName('pCaixa').AsString := FormatFloat('0000', FRENTE_CAIXA.Caixa);
      dmCupomFiscal.dbQuery1.Active := True;
      //Se sim verifica a forma de pagamento se gera fatura pelo pedido
      if(dmCupomFiscal.dbQuery1.FieldByName('EL_CHAVE').asInteger > 0)then
      begin
        dmCupomFiscal.dbQuery2.Active := false;
        dmCupomFiscal.dbQuery2.SQL.Clear;
        dmCupomFiscal.dbQuery2.SQL.Add('Select f.PEDIDO_CR_CP, p.ID from Formas_Pagamento f '+
                                       'LEFT OUTER JOIN est_Pedidos p on P.Forma_Pag = f.ID '+
                                       'where p.EL_Chave = :pChave');
        dmCupomFiscal.dbQuery2.ParamByName('pChave').AsInteger :=
          dmCupomFiscal.dbQuery1.FieldByName('EL_CHAVE').asInteger;
        dmCupomFiscal.dbQuery2.Active := True;
        if(dmCupomFiscal.dbQuery2.FieldByName('PEDIDO_CR_CP').asString = 'R')then
        begin
          //Verifica se esta na conta do cliente
          dmCupomFiscal.dbQuery3.active := false;
          dmCupomFiscal.dbQuery3.SQL.Clear;
          dmCupomFiscal.dbQuery3.sql.Add('Select sum(Valor) as Valor from CR_Movimento '+
                                         'where SDoc = :pSDoc and NDoc = :pNDoc and Clifor = :pClifor');
          dmCupomFiscal.dbQuery3.ParamByName('pSDoc').asString := 'PE';
          dmCupomFiscal.dbQuery3.ParamByName('pNDoc').asInteger := dmCupomFiscal.dbQuery2.fieldByName('ID').asInteger;
          dmCupomFiscal.dbQuery3.ParamByName('pClifor').asInteger := dmCupomFiscal.dbTemp_NFCECLIFOR.asInteger;
          dmCupomFiscal.dbQuery3.Active := True;
          if(dmCupomFiscal.dbQuery3.FieldByName('Valor').AsCurrency > 0)then
            Result := True;
        end;
      end;
      //Verifica se gerou as parcelas
      while not(dmCupomFiscal.dbTemp_NFCE_fatura.Eof) do
      begin
        vlrTotalParcelas := vlrTotalParcelas + dmCupomFiscal.dbTemp_NFCE_FaturaVALOR.AsCurrency;
        dmCupomFiscal.dbTemp_NFCE_Fatura.Next;
      end;
      if(dmCupomFiscal.dbTemp_NFCE_FormaPagVPAG.AsCurrency <> vlrTotalParcelas)then
      begin
        try
          //Para gerar parcelas venda a prazo
          frmGerarParcelas:=TfrmGerarParcelas.create(Application);
          frmGerarParcelas.ShowModal;
          vlrTotalParcelas:=frmGerarParcelas.re_vl_T_Parcelas.Value;
          //verifica se cliente gerou alguma parcela
          if not(DMCupomFiscal.dbTemp_NFCE_Fatura.IsEmpty)then
          begin
            if dmCupomFiscal.dbTemp_NFCE_FormaPagVPAG.Value <> vlrTotalParcelas then
              raise Exception.Create('Valor pago com "Cr�dito da Loja" n�o pode ser diferente do total das Parcelas');
          end
          else
            Abort;
          frmGerarParcelas.Free;
          if not(dmCupomFiscal.dbTemp_NFCE.State in [dsEdit]) then
            dmCupomFiscal.dbTemp_NFCE.Edit;
          dmCupomFiscal.dbTemp_NFCEINDPAG.Value:='1'; //Prazo
          dmCupomFiscal.dbTemp_NFCE.Post;
        except //Quando n�o for informado as parcelas Retorna erro
          on e:Exception do
          begin
            dmCupomFiscal.dbTemp_NFCE_FormaPag.Cancel;
            if(E.message <> 'Operation aborted')then
              msgInformacao(e.Message,'');
            Abort;
          end;
        end;
      end;
    end;
    Result := True;
  except
    on e:Exception do
    begin
      if not (e is EAbort) then
        msgInformacao(e.Message,'');
      Result:=False;
    end;
  end;
end;}

function TfrmFim.getDescontoItens: Currency;
begin
  DMCupomFiscal.dbQuery2.Active:=False;
  DMCupomFiscal.dbQuery2.SQL.Clear;
  DMCupomFiscal.dbQuery2.SQL.Add('select coalesce(sum(vlr_desconto_item),0) vlr_desconto_item from temp_nfce_item'+
                ' Where Caixa=:pCaixa');
  DMCupomFiscal.dbQuery2.ParamByName('pCaixa').Value := FormatFloat('0000', FRENTE_CAIXA.Caixa);
  DMCupomFiscal.dbQuery2.Active:=True;
  DMCupomFiscal.dbQuery2.First;
  Result:=DMCupomFiscal.dbQuery2.FieldByName('vlr_desconto_item').AsCurrency;
end;

procedure TfrmFim.getDadosPedido(el_chavePedido:integer);
begin
  DMCupomFiscal.dbQuery2.Active:=False;
  DMCupomFiscal.dbQuery2.SQL.Clear;
  DMCupomFiscal.dbQuery2.SQL.Add('select * from est_pedidos where el_chave= :pElChave');
  DMCupomFiscal.dbQuery2.ParamByName('pElChave').Value := el_chavePedido;
  DMCupomFiscal.dbQuery2.Active:=True;
  DMCupomFiscal.dbQuery2.First;
  if not dmCupomFiscal.dbQuery2.Eof then
  begin
    edtOperacao.Text    := DMCupomFiscal.dbQuery2.FieldByName('operacao').AsString;
    cdsFormaPagamento.Locate('ID', DMCupomFiscal.dbQuery2.FieldByName('forma_pag').AsString, [loCaseInsensitive]);
    lcmbFormaPagamento.KeyValue:=cdsFormaPagamentoID.Value;
    edtFuncionario.Text := DMCupomFiscal.dbQuery2.FieldByName('funcionario').AsString;
{    edtCliente.Text     := DMCupomFiscal.dbQuery2.FieldByName('clifor').AsString;
    edtClienteNome.Text := DMCupomFiscal.dbQuery2.FieldByName('cf_nome').AsString;
    edtEndereco.Text    := DMCupomFiscal.dbQuery2.FieldByName('cf_ende').AsString;
    edtCPFCNPJ.Text     := DMCupomFiscal.dbQuery2.FieldByName('cf_cnpj_cpf').AsString;}
  end;
  getDescontoPedido(el_chavePedido);
end;

procedure TfrmFim.getDescontoPedido(el_chavePedido:integer);
begin
  DMCupomFiscal.dbQuery2.Active:=False;
  DMCupomFiscal.dbQuery2.SQL.Clear;
  DMCupomFiscal.dbQuery2.SQL.Add('select vlr_desconto from est_pedidos where el_chave= :pElChave');
  DMCupomFiscal.dbQuery2.ParamByName('pElChave').Value := el_chavePedido;
  DMCupomFiscal.dbQuery2.Active:=True;
  DMCupomFiscal.dbQuery2.First;
  if not dmCupomFiscal.dbQuery2.Eof then
  begin
    //reVlrDesconto.Value := DMCupomFiscal.dbQuery2.FieldByName('vlr_desconto').AsCurrency;
    //reVlrDesconto.Enabled := False;
  end;
end;

function TfrmFim.validarCPFCNPJ(cpfcnpj:String): Boolean;
begin
  //Quando tiver CPF/CNPJ informado
  If Length(limpaString(cpfcnpj)) > 0 then
  begin
    If Length(limpaString(cpfcnpj)) > 11 then
      dmComponentes.ACBrValidador1.TipoDocto := docCNPJ
    else
      dmComponentes.ACBrValidador1.TipoDocto := docCPF;

    dmComponentes.ACBrValidador1.Documento   := Trim(cpfcnpj);
    dmComponentes.ACBrValidador1.Complemento := '';
    if not(dmComponentes.ACBrValidador1.Validar) then
    begin
      msgInformacao(dmComponentes.ACBrValidador1.MsgErro, 'Aten��o');
      Result:=False;
    end
    else
      Result:=True;
  end;
end;

function TfrmFim.verificaTotalizadorTributado(icm: String): Boolean;
begin
  case AnsiIndexStr(icm, ['07', '12', '17', '18', '25', 'SS']) of
    0..5:Result := True;
  else
    Result := False;
  end
end;

procedure TfrmFim.edtPlacaExit(Sender: TObject);
begin
//--Lauro 21.04.2016  if (StrToIntDef(edtCliente.Text,0)<=0) and (Trim(edtClienteNome.Text)='') then
//begin
{    dmCupomFiscal.dbQuery1.Close;
    dmCupomFiscal.dbQuery1.SQL.Clear;
    dmCupomFiscal.dbQuery1.SQL.Add('Select clifor from clifor_Frota where placa = :pPlaca');
    dmCupomFiscal.dbQuery1.ParamByName('pPlaca').Value := trim(edtPlaca.Text);
    dmCupomFiscal.dbQuery1.Active:=True;
    dmCupomFiscal.dbQuery1.First;
    if not(dmCupomFiscal.dbQuery1.eof) then
    begin
      if (StrToIntDef(edtCliente.Text,0)<>dmCupomFiscal.dbQuery1.FieldByName('Clifor').AsInteger) then
      begin
        if msgPergunta('Cliente Informado ['+edtCliente.Text+']'+#13+
                       'Esta Placa esta Cadastada para a Frota do Cliente ['+dmCupomFiscal.dbQuery1.FieldByName('Clifor').AsString+']'+#13+
                       'Deseja Usar o Cliente da Frota ?','')
        then
        begin
          edtCliente.Text := dmCupomFiscal.dbQuery1.FieldByName('Clifor').Text;
          edtCliente.OnExit(Action);
        end
        Else
        begin
          edtPlaca.SetFocus;
          Abort;
        end;
      end;
    end;

{    if Not(ValidaFrota( StrToIntDef(edtCliente.Text,0), Trim(edtPlaca.Text) )) then
    begin
      msgInformacao('Este Cliente Possue Frota.'+#13+
                    'Esta Placa N�o Consta em Sua Frota.'+#13+
                    'Verifique a Placa!','Aten��o');
      edtPlaca.SetFocus;
      Abort;
    end;}

//end;
//-- Fim Lauro 21.04.2016
end;

procedure TfrmFim.Label15Click(Sender: TObject);
begin
  frmPesquisa := tfrmPesquisa.Create(Self,'Select * From Serie_Documentos Where (Descricao>=:pTexto) Order By Descricao',
                                         'Select * From Serie_Documentos Where (Descricao Like :pTexto) Order By Descricao');

  frmPesquisa.DBGrid1.Columns.Add;
  frmPesquisa.DBGrid1.Columns[0].Title.Caption:='C�digo';
  frmPesquisa.DBGrid1.Columns[0].Width:=42;
  frmPesquisa.DBGrid1.Columns[0].FieldName:='id';

  frmPesquisa.DBGrid1.Columns.Add;
  frmPesquisa.DBGrid1.Columns[1].Title.Caption:='Descri��o';
  frmPesquisa.DBGrid1.Columns[1].Width:=300;
  frmPesquisa.DBGrid1.Columns[1].FieldName:='Descricao';

  frmPesquisa.DBGrid1.Columns.Add;
  frmPesquisa.DBGrid1.Columns[2].Title.Caption:='Modelo';
  frmPesquisa.DBGrid1.Columns[2].Width:=50;
  frmPesquisa.DBGrid1.Columns[2].FieldName:='modelo_doc';
  try
    if (frmPesquisa.ShowModal=mrOK) then
    begin
      if not(dmCupomFiscal.dbTemp_NFCE.State in [dsEdit, dsInsert]) then
        dmCupomFiscal.dbTemp_NFCE.Edit;
      dmCupomFiscal.dbTemp_NFCESERIE.Value:=FrmPesquisa.IBQuery1.FieldByName('id').AsString;
      dmCupomFiscal.dbTemp_NFCEMODELO_DOC.Value:=FrmPesquisa.IBQuery1.FieldByName('modelo_doc').AsString;
    end;
  finally
    frmPesquisa.Free;
  end;
end;

procedure TfrmFim.Label27Click(Sender: TObject);
begin
  frmPesquisaGenerica := tfrmPesquisaGenerica.Create(Self,'CCustos');
  try
    if (frmPesquisaGenerica.ShowModal=mrOK) then
    begin
      if not(dmCupomFiscal.dbTemp_NFCE.State in [dsEdit, dsInsert]) then
        dmCupomFiscal.dbTemp_NFCE.Edit;
      dmCupomFiscal.dbTemp_NFCECCUSTO.Value := FrmPesquisaGenerica.IBQuery1.FieldByName('id').AsInteger;
    end;
  finally
    frmPesquisaGenerica.Free;
  end;
end;

procedure TfrmFim.edtCCustoExit(Sender: TObject);
begin
  DMCupomFiscal.dbQuery1.Active:=False;
  DMCupomFiscal.dbQuery1.SQL.Clear;
  DMCupomFiscal.dbQuery1.SQL.Add('Select Descricao FROM CCustos Where ID=:pCod');
  DMCupomFiscal.dbQuery1.ParamByName('pCod').AsInteger:=StrToIntDef(edtCCusto.Text,0);
  DMCupomFiscal.dbQuery1.Active:=True;
  DMCupomFiscal.dbQuery1.First;
  If not(DMCupomFiscal.dbQuery1.Eof) then
    edtCCustoDescricao.Text:=DMCupomFiscal.dbQuery1.FieldByName('Descricao').AsString
  else
  begin
    msgAviso('Centro de Custo n�o cadastrado!','');
    edtCCusto.SetFocus;
  end;
end;

procedure TfrmFim.edtConvenioExit(Sender: TObject);
begin
  DMCupomFiscal.dbQuery1.Active:=False;
  DMCupomFiscal.dbQuery1.SQL.Clear;
  DMCupomFiscal.dbQuery1.SQL.Add('Select Descricao FROM Convenios Where ID=:pCod');
  DMCupomFiscal.dbQuery1.ParamByName('pCod').AsInteger:=StrToIntDef(edtConvenio.Text,0);
  DMCupomFiscal.dbQuery1.Active:=True;
  DMCupomFiscal.dbQuery1.First;
  If not(DMCupomFiscal.dbQuery1.Eof) then
  begin
    edtConvenioDesc.Text := DMCupomFiscal.dbQuery1.FieldByName('Descricao').AsString;
    if not(dmCupomFiscal.dbTemp_NFCE.State in [dsEdit, dsInsert])then
      dmCupomFiscal.dbTemp_NFCE.edit;
    dmCupomFiscal.dbTemp_NFCE.Post;
    dmCupomFiscal.Transaction.CommitRetaining;
  end
  Else
  begin
    msgAviso('Conv�nio n�o cadastrado!','');
    edtConvenioDesc.Text := 'Conv�nio n�o cadastrado!';
  end;
end;

procedure TfrmFim.getFormasPagamento;
begin
  //Insere dentro do clientDataSet que mostra no LookupComboBox
  //Zerar o pagamento que leva para o CR, caso tenha registrado e voltado para os itens
  //Pois pode ter registrado um valor e depois cancelado um item, oque muda o valor que
  //deve lan�ar no CR, fazendo assim o cliente gerar novamente o pagamento
  dmCupomFiscal.getTemp_NFCE_FormaPag; //Carrega as formas de pagamento que est�o configuradas para o caixa
  if not(dmCupomFiscal.dbTemp_NFCE_FormaPag.IsEmpty)then
  begin
    dmCupomFiscal.dbTemp_NFCE_FormaPag.First;
    while not(dmCupomFiscal.dbTemp_NFCE_FormaPag.eof)do
    begin
      if(dmCupomFiscal.dbTemp_NFCE_FormaPagCR_CP_V.Value = 'R') and
        (dmCupomFiscal.dbTemp_NFCE_FormaPagVPAG.Value > 0)then
      begin
        dmCupomFiscal.IBSQL1.Close;
        dmCupomFiscal.IBSQL1.SQL.Clear;
        dmCupomFiscal.IBSQL1.SQL.Add('Delete From temp_NFCE_Fatura Where Caixa=:pCaixa');
        dmCupomFiscal.IBSQL1.ParamByName('pCaixa').AsString := FormatFloat('0000', dmCupomFiscal.dbTemp_NFCECAIXA.AsInteger);
        dmCupomFiscal.IBSQL1.ExecQuery;

        dmCupomFiscal.dbTemp_NFCE_FormaPag.Edit;
        dmCupomFiscal.dbTemp_NFCE_FormaPagVPAG.Value:=0;
        dmCupomFiscal.dbTemp_NFCE_FormaPagCARD_CNPJ.Clear;
        dmCupomFiscal.dbTemp_NFCE_FormaPagCARD_TBAND.Clear;
        dmCupomFiscal.dbTemp_NFCE_FormaPagCARD_CAUT.Clear;
        dmCupomFiscal.dbTemp_NFCE_FormaPag.Post;

        dmCupomFiscal.Transaction.CommitRetaining;
      end;
      dmCupomFiscal.dbTemp_NFCE_FormaPag.Next;
    end;
    recalculaTotais;
  end;

  if(dmCupomFiscal.dbTemp_NFCECLIFOR.AsInteger > 0)then
  begin
    dmCupomFiscal.dbQuery1.Close;
    dmCupomFiscal.dbQuery1.SQL.Clear;
    dmCupomFiscal.dbQuery1.SQL.Add('select fp.id, fp.descricao, fp.tpag from formas_pagamento fp '+
                                   'left outer join CLIFOR_FORMAS_PAGAMENTO cp on cp.forma_pag = fp.id '+
                                   'where fp.ativo=''S'' and cast(fp.tpag as integer) > 0 and (cp.CLIFOR = :pClifor) '+
                                   'and fp.Exibir_NFC = ''S''');
    dmCupomFiscal.dbQuery1.ParamByName('pClifor').AsInteger := dmCupomFiscal.dbTemp_NFCECLIFOR.AsInteger;
    dmCupomFiscal.dbQuery1.Active:=True;
    dmCupomFiscal.dbQuery1.First;
    if(dmCupomFiscal.dbQuery1.IsEmpty)then //Caso n�o tiver forma vinculada seleciona todas
    begin
      dmCupomFiscal.dbQuery1.Close;
      dmCupomFiscal.dbQuery1.SQL.Clear;
      dmCupomFiscal.dbQuery1.SQL.Add('select fp.id, fp.descricao, fp.tpag from formas_pagamento fp'+
                                     ' where fp.ativo=''S'' and cast(fp.tpag as integer) > 0 and'+
                                     ' fp.Exibir_NFC = ''S''');
      dmCupomFiscal.dbQuery1.Active:=True;
      dmCupomFiscal.dbQuery1.First;
    end;
  end
  else
  begin
    dmCupomFiscal.dbQuery1.Close;
    dmCupomFiscal.dbQuery1.SQL.Clear;
    dmCupomFiscal.dbQuery1.SQL.Add('select fp.id, fp.descricao, fp.tpag from formas_pagamento fp'+
                                   ' where fp.ativo=''S'' and cast(fp.tpag as integer) > 0'+
                                   ' and fp.Exibir_NFC = ''S''');
    dmCupomFiscal.dbQuery1.Active:=True;
    dmCupomFiscal.dbQuery1.First;
  end;
  //Se n�o tiver nada cadastrado da a mensagem mas deixa abrir a tela
  if(dmCupomFiscal.dbQuery1.IsEmpty)then
    msgAviso('N�o existem formas de pagamento cadastradas.','Aviso');

  cdsFormaPagamento.EmptyDataSet;
  cdsFormaPagamento.Open;
  dmCupomFiscal.dbQuery1.First;
  while not(dmCupomFiscal.dbQuery1.Eof) do
  begin
    cdsFormaPagamento.Append;
    cdsFormaPagamentoID.AsString          := dmCupomFiscal.dbQuery1.FieldByName('id').Value;
    cdsFormaPagamentoDESCRICAO.AsString   := dmCupomFiscal.dbQuery1.FieldByName('descricao').Value;
    cdsFormaPagamentoTPAG.AsString        := dmCupomFiscal.dbQuery1.FieldByName('tpag').AsString;
    cdsFormaPagamento.Post;
    if not dmCupomFiscal.dbTemp_NFCE_FormaPag.Locate('FORMA_PAG',dmCupomFiscal.dbQuery1.FieldByName('id').Value,[loCaseInsensitive]) then
    begin
      dmCupomFiscal.dbTemp_NFCE_FormaPag.Append;
      dmCupomFiscal.dbTemp_NFCE_FormaPagCAIXA.Value     := FormatFloat('0000', FRENTE_CAIXA.Caixa);
      dmCupomFiscal.dbTemp_NFCE_FormaPagID.Value        := 0;
      dmCupomFiscal.dbTemp_NFCE_FormaPagTPAG.Value      := dmCupomFiscal.dbQuery1.FieldByName('tpag').AsString;
      dmCupomFiscal.dbTemp_NFCE_FormaPagFORMA_PAG.Value := dmCupomFiscal.dbQuery1.FieldByName('id').Value;
      dmCupomFiscal.dbTemp_NFCE_FormaPagVPAG.Value      := 0;
      dmCupomFiscal.dbTemp_NFCE_FormaPagVTROCO.Value    := 0;
      dmCupomFiscal.dbTemp_NFCE_FormaPag.Post;
    end;
    dmCupomFiscal.dbQuery1.Next;
  end;
  dmCupomFiscal.Transaction.CommitRetaining;
  dmCupomFiscal.getTemp_NFCE_FormaPag;
  cdsFormaPagamento.First;
  if(cdsFormaPagamento.Locate('ID', FRENTE_CAIXA.Forma_Pag, [loCaseInsensitive])) and
    (FRENTE_CAIXA.Sugerir_FormaPag)then
  begin
    edtFormaPag.Text := IntToStr(FRENTE_CAIXA.Forma_Pag); //Setar na forma pagamento Padrao
    lcmbFormaPagamento.KeyValue := cdsFormaPagamentoID.Value;
  end;
  if cdsFormaPagamento.IsEmpty then
    btnPagar.Enabled := False
  else
    btnPagar.Enabled := True;
end;

procedure TfrmFim.edtVlrAcrescimosEnter(Sender: TObject);
begin
{  lblAcrescPerc.Caption:='0,00%';
  Acrescimo := dmCupomFiscal.dbTemp_NFCEVLR_OUTRO.Value;}
end;

procedure TfrmFim.edtVlrAcrescimosExit(Sender: TObject);
var pDes : Currency;
begin
  //Se n�o digitar nada vai inserir o valor de quando entrou no campo
  if Trim(edtVlrAcrescimos.Text)='' then
  begin
    showMessage('Entrei');
    dmCupomFiscal.dbTemp_NFCEVLR_OUTRO.Value := dmCupomFiscal.dbTemp_NFCEVLR_OUTRO.OldValue
  end
  else
  begin
    if(dmCupomFiscal.dbTemp_NFCEVLR_OUTRO.Value <> dmCupomFiscal.dbTemp_NFCEVLR_OUTRO.OldValue)then
    begin
      //Valor negativo - Calcula Desconto em Percentual
      if (dmCupomFiscal.dbTemp_NFCEVLR_OUTRO.Value < 0) then
         dmCupomFiscal.dbTemp_NFCEVLR_OUTRO.Value := dmCupomFiscal.dbTemp_NFCEVLR_SUBTOTAL.Value *
         -(dmCupomFiscal.dbTemp_NFCEVLR_OUTRO.Value)/100;
      if not(dmCupomFiscal.dbTemp_NFCE.State in [dsEdit, dsInsert])Then
        dmCupomFiscal.dbTemp_NFCE.edit;
      dmCupomFiscal.dbTemp_NFCE.Post;
      recalculaTotais;
      edtPagValor.Value := (dmCupomFiscal.dbTemp_NFCEVLR_TOTAL.AsCurrency+
        dmCupomFiscal.dbTemp_NFCEVLR_DESCONTO.AsCurrency) -
        dmCupomFiscal.dbTemp_NFCEVLR_RECEBIDO.AsCurrency;
    end;
  end;
  //Calcula a porcentagem de desconto
  pDes := (dmCupomFiscal.dbTemp_NFCEVLR_OUTRO.Value / dmCupomFiscal.dbTemp_NFCEVLR_SUBTOTAL.Value) * 100;
  lblAcrescPerc.Caption := FormatCurr('##0.00%',pDes);
end;

procedure TfrmFim.edtVlrDescontoEnter(Sender: TObject);
begin
//  lblDescPerc.Caption:='0,00%';
//  Desconto := dmCupomFiscal.dbTemp_NFCEVLR_DESCONTO.Value;
end;

procedure TfrmFim.edtVlrDescontoExit(Sender: TObject);
var pDes : Currency;
begin
  //MUITO IMPORTANTE> Cuidar os codigos, pois no F1 se tiver focado no campo vai executar...

  //Se n�o digitar nada vai inserir o valor de quando entrou no campo
  if Trim(edtVlrDesconto.Text)='' then
  begin
    if not(dmCupomFiscal.dbTemp_NFCE.State in [dsEdit])Then
      dmCupomFiscal.dbTemp_NFCE.Edit;
    dmCupomFiscal.dbTemp_NFCEVLR_DESCONTO.Value := dmCupomFiscal.dbTemp_NFCEVLR_DESCONTO.OldValue
  end
  else
  begin
    if(dmCupomFiscal.dbTemp_NFCEVLR_DESCONTO.Value <> dmCupomFiscal.dbTemp_NFCEVLR_DESCONTO.OldValue)then
    begin
      //Valor negativo - Calcula Desconto em Percentual
        if not(dmCupomFiscal.dbTemp_NFCE.State in [dsEdit, dsInsert])Then
          dmCupomFiscal.dbTemp_NFCE.edit;
      if (dmCupomFiscal.dbTemp_NFCEVLR_DESCONTO.Value < 0) then
        dmCupomFiscal.dbTemp_NFCEVLR_DESCONTO.Value := dmCupomFiscal.dbTemp_NFCEVLR_SUBTOTAL.Value *
        -(dmCupomFiscal.dbTemp_NFCEVLR_DESCONTO.Value)/100;
      if(dmCupomFiscal.dbTemp_NFCEVLR_DESCONTO.Value >
         dmCupomFiscal.dbTemp_NFCEVLR_SUBTOTAL.Value)then//-dmCupomFiscal.dbTemp_NFCEVLR_RECEBIDO.Value))then
      begin
        msgInformacao('O valor do desconto n�o pode ser maior que o valor a receber.', 'Informa��o');
        dmCupomFiscal.dbTemp_NFCEVLR_DESCONTO.Value := dmCupomFiscal.dbTemp_NFCEVLR_DESCONTO.OldValue;
        Abort;
      end;
      if (getAutorizacao('DESCONTO_NFCE','DAR DESCONTO AO FINALIZAR NFCE',dmCupomFiscal.dbTemp_NFCEVLR_DESCONTO.Value)) then
      begin
        if not(dmCupomFiscal.dbTemp_NFCE.State in [dsEdit, dsInsert])Then
          dmCupomFiscal.dbTemp_NFCE.edit;
        dmCupomFiscal.dbTemp_NFCE.Post;
        recalculaTotais;
      end
      else
      begin
        if not(dmCupomFiscal.dbTemp_NFCE.State in [dsEdit, dsInsert])Then
          dmCupomFiscal.dbTemp_NFCE.edit;
        dmCupomFiscal.dbTemp_NFCEVLR_DESCONTO.Value := dmCupomFiscal.dbTemp_NFCEVLR_DESCONTO.OldValue;
        dmCupomFiscal.dbTemp_NFCE.Post;
        edtVlrDesconto.SetFocus;
        edtVlrDesconto.SelectAll;
        Abort;
      end;
      recalculaTotais;
    end;
  end;
end;

procedure TfrmFim.btnPagarClick(Sender: TObject);
var vlrSomaFatura, vlrTotalParcelas, vlr_pag: Currency;
    retorno_mrOK : Boolean;
    fPag : Integer;
    lCtrlTef: TCtrlTef;
begin
  if(dmCupomFiscal.dbTemp_NFCE.State in [dsEdit, dsInsert])Then
    dmCupomFiscal.dbTemp_NFCE.Post;

  if edtPagValor.Value > 0 then //--- Se foi informado Valor Pago
  begin
    //Se ja foi Pago valor total n�o deixar registrar mais valor
    if (dmCupomFiscal.dbTemp_NFCEVLR_TROCO.Value < 0) then
    begin
      try
        try
          //Verifica se ja tiver registro com essa forma de pagamento ira editar com novo valor
          if dmCupomFiscal.dbTemp_NFCE_FormaPag.Locate('TPAG;FORMA_PAG',
            VarArrayOf([cdsFormaPagamentoTPAG.AsString,cdsFormaPagamentoID.AsString]), [loCaseInsensitive])then
          begin
            if not dmCupomFiscal.dbTemp_NFCE_FormaPag.Transaction.InTransaction then
              dmCupomFiscal.dbTemp_NFCE_FormaPag.Transaction.StartTransaction;

            dmCupomFiscal.dbTemp_NFCE_FormaPag.Edit;
            dmCupomFiscal.dbTemp_NFCE_FormaPagVPAG.Value:= edtPagValor.Value +
              dmCupomFiscal.dbTemp_NFCE_FormaPagVPAG.Value;
            case cdsFormaPagamentoTPAG.AsInteger of
              90:begin //Cart�o Debito/Credito - TEF90
{                if(edtPagValor.Value < 1)then
                begin
                  msgAviso('Transa��es TEF exigem valores maiores ou iguais � R$ 1,00.',
                    Application.Title);
                  Abort;
                end;}
                pnlGeral.SetFocus;
                lCtrlTef := TCtrlTef.Create(dmCupomFiscal.DataBase, dmCupomFiscal.Transaction, frmFim);
                try
                  lCtrlTef.EfetuarPagamento(edtPagValor.Value,'90',dmCupomFiscal.dbTemp_NFCEEL_CHAVE.AsString);
                  Application.BringToFront;
                finally
                  lCtrlTef.Free;
                end;
              end;
              3,4:begin //Cart�o Debito/Credito - POS
                  //Abre tela selecionar Bandeira
                  getBandeirasCartao(cdsFormaPagamentoID.Value);
                  if not(qurBandeiraCartao.IsEmpty) then
                  begin
                    repeat
                      frmBandeiraCartao := TfrmBandeiraCartao.Create(Self);
                      try
                        case frmBandeiraCartao.ShowModal of
                          mrOk:begin
                            dmCupomFiscal.dbTemp_NFCE_FormaPagCARD_TBAND.AsString := qurBandeiraCartaoCARD_TBAND.AsString;
                            dmCupomFiscal.dbTemp_NFCE_FormaPagCARD_CNPJ.Value := frmBandeiraCartao.qurCredenciadoraCNPJ.AsString;
                            dmCupomFiscal.dbTemp_NFCE_FormaPagCARD_CAUT.Value := frmBandeiraCartao.edtCartaocAut.Text;
                            retorno_mrOK := True;
                          end;
                          mrCancel:begin
                            Abort;
                          end;
                          mrAbort:begin
                            msgInformacao('Favor selecionar a Bandeira do Cart�o','');
                          end;
                        end;
                      finally
                        frmBandeiraCartao.Free;
                      end;
                    until(retorno_mrOK);
                  end
                  else
                  begin
                    msgAviso('N�o foi encontrada nenhuma bandeira configurada para esta forma de pagamento!', '');
                    Abort;
                  end;
                end;
              5:begin //05 - Credito Loja
                if(edtPagValor.Value > abs(dmCupomFiscal.dbTemp_NFCEVLR_TROCO.AsFloat))then
                begin
                  msgAviso('Valor pago a prazo n�o pode ser maior que o valor que resta para receber.',
                  Application.Title);
                  exit;
                end;
                if(dmCupomFiscal.dbTemp_NFCECLIFOR.Value <=0)then
                begin
                  msgInformacao('Obrigat�rio identificar consumidor para vendas a prazo','Informa��o');
                  fPag := cdsFormaPagamentoID.Value;
                  frmClifor:=TfrmClifor.create(Application);
                  try
                    case frmClifor.ShowModal of
                    mrOk:begin
                        if(dmCupomFiscal.dbTemp_NFCECLIFOR.Value > 0)then
                        begin
                          getFormasPagamento;
                          if(dmCupomFiscal.dbTemp_NFCE_FormaPag.Locate('FORMA_PAG',fPag,[loCaseInsensitive]))then
                          begin
                            If (crExcedeuLimite(dmCupomFiscal.dbTemp_NFCECLIFOR.Value, edtPagValor.Value)) then
                            begin
                              Abort;
                            end;
                            dmCupomFiscal.dbTemp_NFCE_FormaPag.Edit;
                            dmCupomFiscal.dbTemp_NFCE_FormaPagVPAG.Value:=edtPagValor.Value +
                              dmCupomFiscal.dbTemp_NFCE_FormaPagVPAG.Value;
                          end
                          else
                          begin
                            msgInformacao('Cliente possui forma de pagamento especial.'+#13+
                                          'A forma de pagamento selecionada n�o esta dispon�vel para o cliente.'+#13+#13+
                                          'A opera��o sera cancelada.','Informa��o');
                            edtFormaPag.SetFocus;
                            edtFormaPag.SelectAll;
                            Abort;
                          end;
                        end
                        else
                        begin
                          dmCupomFiscal.dbTemp_NFCE_FormaPag.Cancel;
                          dmCupomFiscal.Transaction.RollbackRetaining;
                          Abort;
                        end;
                      end;
                    mrCancel:begin
                        dmCupomFiscal.dbTemp_NFCE_FormaPag.Cancel;
                        dmCupomFiscal.Transaction.RollbackRetaining;
                        Abort;
                      end;
                    end;
                  finally
                    frmClifor.Free;
                  end;
                end
                else
                begin
                  If (crExcedeuLimite(dmCupomFiscal.dbTemp_NFCECLIFOR.Value, edtPagValor.Value)) then
                  begin
                    Abort;
                  end;
                end;
                // Posto N�o pede Parcelas
                dmCupomFiscal.dbTemp_NFCE_Item.Filter := 'SubTipo_Item <> ''CO''';
                dmCupomFiscal.dbTemp_NFCE_Item.Filtered := True;
                try
                  if (frmPosto <> nil) and not(dmCupomFiscal.GetSubTipo_Item(FRENTE_CAIXA.Caixa)) then
                  begin
                    try
                      if not(dmCupomFiscal.dbTemp_NFCE.State in [dsEdit, dsInsert]) then
                        dmCupomFiscal.dbTemp_NFCE.Edit;
                      dmCupomFiscal.dbTemp_NFCEINDPAG.Value:='1'; //Prazo
                      dmCupomFiscal.dbTemp_NFCE.Post;
                      dmCupomFiscal.dbTemp_NFCE_Fatura.Close;

                      dmCupomFiscal.IBSQL1.Close;
                      dmCupomFiscal.IBSQL1.SQL.Clear;
                      dmCupomFiscal.IBSQL1.SQL.Add('Delete From temp_nfce_Fatura Where Caixa=:pCaixa');
                      dmCupomFiscal.IBSQL1.ParamByName('pCaixa').AsString := FormatFloat('0000', FRENTE_CAIXA.Caixa);
                      dmCupomFiscal.IBSQL1.ExecQuery;

                      dmCupomFiscal.dbTemp_NFCE_Fatura.Open;
                      dmCupomFiscal.dbTemp_NFCE_Fatura.Insert;
                      DMCupomFiscal.dbTemp_NFCE_FaturaCAIXA.Value   := FormatFloat('0000', FRENTE_CAIXA.Caixa);
                      DMCupomFiscal.dbTemp_NFCE_FaturaDATA.Value    := GetCalculaDataVencimento(dmCupomFiscal.dbTemp_NFCECLIFOR.Value); //Lauro 29.09.16 OK Testado incDay(Date, wCF_DiasPrazo);
                      DMCupomFiscal.dbTemp_NFCE_FaturaVALOR.Value   := edtPagValor.Value;
                      DMCupomFiscal.dbTemp_NFCE_FaturaPARCELA.Value := 1;
                      DMCupomFiscal.dbTemp_NFCE_Fatura.Post;
                      dmCupomFiscal.Transaction.CommitRetaining;

                      dmCupomFiscal.getTemp_NFCE_Fatura();
                    except
                      on E:Exception do
                      begin
                        DMCupomFiscal.Transaction.RollbackRetaining;
                        raise Exception.Create('[EST_NFC_FATURA] - Erro ao gravar Faturas.'+#13+e.Message);
                      end;
                    end;
                  end;
                finally
                  dmCupomFiscal.dbTemp_NFCE_Item.Filtered := False;
                end;
              end;
            end;
          end;
          dmCupomFiscal.dbTemp_NFCE_FormaPag.Post;
          dmCupomFiscal.Transaction.CommitRetaining;
        except
          on e:Exception do
          begin
            dmCupomFiscal.Transaction.RollbackRetaining;
            if not (e is EAbort) then
              logErros(Sender, caminhoLog, '[TEMP_NFCE_FORMAPAG] - Erro ao gravar pagamento na base de dados. '+e.Message, '[TEMP_NFCE_FORMAPAG] - Erro ao gravar pagamento na base de dados', 'S', E);
          end;
        end;
      finally
        getTemp_NFCE_Item();
        dmCupomFiscal.getTemp_NFCE_FormaPag();
        dmCupomFiscal.getTemp_NFCE_Fatura();
        dmCupomFiscal.getTemp_NFCE();
        //Recebido = Total OU Recebido > Total
        if (dmCupomFiscal.dbTemp_NFCEVLR_RECEBIDO.Value >= dmCupomFiscal.dbTemp_NFCEVLR_TOTAL.Value)then
        begin
          edtPagValor.Value := 0; //Atrib
          edtFormaPag.Text := IntToStr(FRENTE_CAIXA.Forma_Pag);
          edtFormaPag.SetFocus;
          edtFormaPag.SelectAll;
        end
        else
        begin
          edtPagValor.Value := dmCupomFiscal.dbTemp_NFCEVLR_TROCO.Value * -1; //Atrib
          edtFormaPag.Text := IntToStr(FRENTE_CAIXA.Forma_Pag);
          edtFormaPag.SetFocus;
          edtFormaPag.SelectAll;
        end ;
      end;
    end
    else
    begin
      msgInformacao('N�o � poss�vel registrar novo pagamento quando Valor Total da NFC-e j� foi alcan�ado!','');
      edtPagValor.Value := 0; // Atrib
      edtFormaPag.Text := IntToStr(FRENTE_CAIXA.Forma_Pag);
      application.ProcessMessages;
      edtFormaPag.SetFocus;
      edtFormaPag.SelectAll;
    end;
  end
  else
  begin
    if(edtPagValor.Value = 0)then
    begin
      if dmCupomFiscal.dbTemp_NFCE_FormaPag.Locate('FORMA_PAG',
        cdsFormaPagamentoID.Value,[loCaseInsensitive]) then
      begin
        zerarPagamento;
        edtFormaPag.Text := IntToStr(FRENTE_CAIXA.Forma_Pag);
        application.ProcessMessages;
        edtFormaPag.SetFocus;
        edtFormaPag.SelectAll;
      end;
    end
    else
    begin
      msgAviso('Favor informar um valor pago v�lido','');
      edtPagValor.SetFocus;
    end;
  end;
end;

procedure TfrmFim.btnSalvarClick(Sender: TObject);
begin
  if not (dmCupomFiscal.dbTemp_NFCE.State in [dsEdit, dsInsert]) then
    dmCupomFiscal.dbTemp_NFCE.Edit;
  dmCupomFiscal.dbTemp_NFCEDATA.Value:=EL_Data;
  dmCupomFiscal.dbTemp_NFCEHORA.Value:=Now;
  dmCupomFiscal.dbTemp_NFCE.Post;
  dmCupomFiscal.Transaction.CommitRetaining;
end;

procedure TfrmFim.dsTempECFDataChange(Sender: TObject; Field: TField);
begin
  if(dmCupomFiscal.dbTemp_NFCEVLR_TOTAL.Value <=
    dmCupomFiscal.dbTemp_NFCEVLR_RECEBIDO.Value)then
  begin
    edtPagValor.Value := 0;
    pnlTroco.Caption := 'Troco R$';
  end
  else
  begin
    edtPagValor.Value := dmCupomFiscal.dbTemp_NFCEVLR_TROCO.Value*-1;
    pnlTroco.Caption := 'Falta R$';
  end;
end;

procedure TfrmFim.dsTempECFStateChange(Sender: TObject);
begin
  btnSalvar.Enabled:=dmCupomFiscal.dbTemp_NFCE.State in [dsEdit, dsInsert];
end;

procedure TfrmFim.edtSerieKeyPress(Sender: TObject; var Key: Char);
begin
  if not (isDigit(Key)) then Key:= #0;
end;

procedure TfrmFim.grdTemp_ECF_FormaPagDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  if not (dmCupomFiscal.dbTemp_NFCE_FormaPag.IsEmpty) then
  begin
    with grdTemp_ECF_FormaPag do
    begin
      if DataSource.DataSet.FieldByName('VPag').AsCurrency > 0 then
      begin
        Canvas.Font.Style := [fsBold];
        //Canvas.Font.Size  := 11;
        Canvas.Font.Color := HexToTColor(PERSONALIZAR.corFundoCampo);
      end;
      if (Column.Index = 0)then// or (Column.Index = 3) then
        Canvas.Brush.Color := clWhite;
      Canvas.FillRect(Rect);
      DefaultDrawColumnCell(Rect,DataCol,Column,State);
    end;

    if Column.Index=0 then
    begin
      grdTemp_ECF_FormaPag.Canvas.FillRect(Rect);
      imgLista.Draw(grdTemp_ECF_FormaPag.Canvas,Rect.Left+4,Rect.Top+2,0);
    end;
{    if Column.Index=3 then
    begin
      grdTemp_ECF_FormaPag.Canvas.FillRect(Rect);
      imgLista.Draw(grdTemp_ECF_FormaPag.Canvas,Rect.Left+4,Rect.Top+2,1);
    end;}
  end;
end;

procedure TfrmFim.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  //Fecha querys para nao ficar aberta em memoria
  dmCupomFiscal.qMunicipio.Close;
  dmCupomFiscal.dbTemp_NFCE_FormaPag.Close;
  dmCupomFiscal.dbTemp_NFCE.Close;
end;

procedure TfrmFim.lcmbMunicipioEnter(Sender: TObject);
begin
  //getMunicipios(cmbUF.Text);
end;

function TfrmFim.getTotalPagTemp_NFCE_FormaPag(): Currency;
begin
  DMCupomFiscal.dbQuery1.Active:=False;
  DMCupomFiscal.dbQuery1.SQL.Clear;
  DMCupomFiscal.dbQuery1.SQL.Add('Select coalesce(Sum(vpag),0) vpag From temp_nfce_formapag'+
                                 ' Where Caixa=:pCaixa');
  DMCupomFiscal.dbQuery1.ParamByName('pCaixa').AsString:=FormatFloat('0000', FRENTE_CAIXA.Caixa);
  DMCupomFiscal.dbQuery1.Active:=True;
  DMCupomFiscal.dbQuery1.First;
  Result := DMCupomFiscal.dbQuery1.FieldByName('vpag').AsCurrency;
end;

function TfrmFim.getVlrTef(pChave: Integer): Currency;
var
  lQryBusca: TIBQuery;
begin
  lQryBusca := TIBQuery.Create(nil);
  try
    lQryBusca.Database := dmCupomFiscal.DataBase;
    lQryBusca.SQL.Clear;
    lQryBusca.SQL.Add('Select Coalesce(sum(Valor),0) as Valor from TEF_MOVIMENTO '+
                      'where EL_CHAVE = :pChave and STATUS_TRANSACAO = ''X''');
    lQryBusca.ParamByName('pChave').asInteger := pChave;
    lQryBusca.Open;
    Result := lQryBusca.FieldByName('Valor').AsCurrency;
    lQryBusca.Close;
  finally
    lQryBusca.Free;
  end;
end;

procedure TfrmFim.grdTemp_ECF_FormaPagMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if not(dmCupomFiscal.dbTemp_NFCE_FormaPag.IsEmpty) then
  begin
    if (grdTemp_ECF_FormaPag.MouseCoord(X, Y).X in [0]) then
    begin
      if (dmCupomFiscal.dbTemp_NFCE_FormaPagID.Value > 0) then
      begin
        zerarPagamento();
      end;
    end;
  end;
end;

procedure TfrmFim.Label1Click(Sender: TObject);
begin
  frmPesquisaGenerica := tfrmPesquisaGenerica.Create(Self,'Operacoes');
  try
    if (frmPesquisaGenerica.ShowModal=mrOK) then
    begin
      if not(dmCupomFiscal.dbTemp_NFCE.State in [dsEdit, dsInsert]) then
        dmCupomFiscal.dbTemp_NFCE.Edit;
      dmCupomFiscal.dbTemp_NFCEOPERACAO.AsInteger := FrmPesquisaGenerica.IBQuery1.FieldByName('id').AsInteger;
    end;
  finally
    frmPesquisaGenerica.Free;
  end;
end;

//Captura bandeiras do cart�o vinculada com a forma de pagamento selecionada  FORMAS_PAGAMENTO_CARD_TBAND
procedure TfrmFim.getBandeirasCartao(forma_pag: Integer);
begin
  qurBandeiraCartao.Active := False;
  qurBandeiraCartao.ParamByName('pFormaPag').AsInteger := forma_pag;
  qurBandeiraCartao.Active := True;
  qurBandeiraCartao.First;
end;

procedure TfrmFim.edtFormaPagChange(Sender: TObject);
begin
  if(edtFormaPag.Text <> '')Then
  begin
    if cdsFormaPagamento.Locate('ID', Trim(edtFormaPag.Text), [loCaseInsensitive]) then
      lcmbFormaPagamento.KeyValue:=cdsFormaPagamentoID.Value;
  end;
end;

procedure TfrmFim.edtFormaPagEnter(Sender: TObject);
begin
  if(edtFormaPag.Text <> '')Then
  begin
    if(cdsFormaPagamento.Locate('ID', trim(edtFormaPag.Text), [loCaseInsensitive]))then
      lcmbFormaPagamento.KeyValue:=cdsFormaPagamentoID.Value;
  end;
end;

procedure TfrmFim.edtFormaPagExit(Sender: TObject);
begin
  if Trim(edtFormaPag.Text)<>'' then
  begin
    if not cdsFormaPagamento.Locate('ID', Trim(edtFormaPag.Text), [loCaseInsensitive]) then
    begin
      msgAviso(Format('Forma de Pagamento "%s" n�o encontrada',[edtFormaPag.Text]),'');
      edtFormaPag.Text := IntToStr(FRENTE_CAIXA.Forma_Pag);
      edtFormaPag.SetFocus;
      edtFormaPag.SelectAll;
    end
    Else
    begin
      lcmbFormaPagamento.KeyValue:=cdsFormaPagamentoID.Value;
      if(dmCupomFiscal.dbTemp_NFCE_FormaPag.Locate('FORMA_PAG',Trim(edtFormaPag.Text),[loCaseInsensitive]))then
      begin
        if(dmCupomFiscal.dbTemp_NFCe_FormaPagVPAG.value > 0) and
          (pnlTroco.Caption = 'Falta R$')then
          edtPagValor.Value := (dmCupomFiscal.dbTemp_NFCEVLR_TROCO.Value*-1);
      end;
    end;
  end
  else
  begin
    if (dmCupomFiscal.dbTemp_NFCEVLR_RECEBIDO.Value = dmCupomFiscal.dbTemp_NFCEVLR_TOTAL.Value) or
    (dmCupomFiscal.dbTemp_NFCEVLR_RECEBIDO.Value > dmCupomFiscal.dbTemp_NFCEVLR_TOTAL.Value) then
    begin
      edtPagValor.Value := 0; //Atrib
      dmCupomFiscal.dbTemp_NFCE_FormaPag.first;
      while not(dmCupomFiscal.dbTemp_NFCE_FormaPag.eof)do
      begin
        if dmCupomFiscal.dbTemp_NFCE_FormaPagTPAG.Value = '05' then //--Credito Loja
        begin
{          if dmCupomFiscal.dbTemp_NFCE_FormaPagVPAG.Value > 0 then
          begin
            if(Tecla = VK_F4)then
              edtVlrDesconto.SetFocus
            else
              if(dmCupomFiscal.dbTemp_NFCECLIFOR.Value > 0)then
//                btnFinalizar.SetFocus
              else
//                edtCliente.SetFocus;
            Break;
          end;}
        end;
        dmCupomFiscal.dbTemp_NFCE_FormaPag.Next;
{        if(dmCupomFiscal.dbTemp_NFCE_FormaPag.eof)then //se chegou ao fim e n�o identificou forma de pagamento a prazo
          if(dmCupomFiscal.dbTemp_NFCEVLR_TOTAL.AsCurrency >= 10000)then//testa se venda � maior que 10000 (lauro Quem fez Pq Ver ??)
          begin
            if(Tecla = VK_F4)then
              edtVlrDesconto.SetFocus
            else
              if(dmCupomFiscal.dbTemp_NFCECLIFOR.Value > 0)then
//                btnFinalizar.SetFocus
              else
                //edtCliente.SetFocus;
            Break;
          end
          else
          if(Tecla = VK_F4)then
            edtVlrDesconto.SetFocus
          else
            if(Tecla = VK_F3)then
              //edtCliente.SetFocus
            else
//              btnFinalizar.SetFocus;}
      end;
    end
  end;
end;

procedure TfrmFim.edtFormaPagKeyPress(Sender: TObject; var Key: Char);
begin
  if not(somenteNumeros(Key))then
    Key := #0;
end;

procedure TfrmFim.zerarPagamento;
Var
  VlrTef: Currency;
  FCtrlTef: TCtrlTef;
begin
  if msgPergunta(Format('Zerar pagamento %s do valor de R$ %f ?',[Trim(dmCupomFiscal.dbTemp_NFCE_FormaPagTPAG_DESCRICAO.AsString), dmCupomFiscal.dbTemp_NFCE_FormaPagVPAG.AsCurrency]),'') then
  begin
    if ((dmCupomFiscal.dbTemp_NFCE_FormaPagVPAG.Value > 0) and
        (dmCupomFiscal.dbTemp_NFCE_FormaPagTPAG.Value= '90')) and (dmCupomFiscal.TefEstaAtivo) then //--TEF90
    begin
      FCtrlTef := TCtrlTef.Create(dmCupomFiscal.DataBase, dmCupomFiscal.Transaction,
        Self);
      Try
        FCtrlTef.Imprimir(dmCupomFiscal.dbTemp_NFCEEL_CHAVE.AsInteger);
        FCtrlTef.EfetuarCancelamento(dmCupomFiscal.dbTemp_NFCEEL_CHAVE.AsInteger);
      Finally
        FCtrlTef.Free;
      End;
      dmCupomFiscal.dbTemp_NFCE_FormaPag.Edit;
      dmCupomFiscal.dbTemp_NFCE_FormaPagVPAG.Value := getVlrTef(dmCupomFiscal.dbTemp_NFCEEL_CHAVE.AsInteger);
      dmCupomFiscal.dbTemp_NFCE_FormaPag.Post;
      recalculaTotais;
    end;


    if (dmCupomFiscal.dbTemp_NFCE_FormaPagVPAG.Value > 0) and (dmCupomFiscal.dbTemp_NFCE_FormaPagTPAG.Value='05') then //--Credito Loja / Venda Prazo
    begin
      //Remover dados tabela temp_nfce_fatura
      dmCupomFiscal.IBSQL1.Close;
      dmCupomFiscal.IBSQL1.SQL.Clear;
      dmCupomFiscal.IBSQL1.SQL.Add('Delete From temp_nfce_Fatura Where Caixa=:pCaixa');
      dmCupomFiscal.IBSQL1.ParamByName('pCaixa').AsString := FormatFloat('0000', FRENTE_CAIXA.Caixa);
      dmCupomFiscal.IBSQL1.ExecQuery;

      if not(dmCupomFiscal.dbTemp_NFCE.State in [dsEdit, dsInsert]) then
        dmCupomFiscal.dbTemp_NFCE.Edit;
      dmCupomFiscal.dbTemp_NFCEINDPAG.Value:='0'; //Vista
      dmCupomFiscal.dbTemp_NFCE.Post;
    end;
    //Zera dados da forma de pagamento selecionada
    dmCupomFiscal.dbTemp_NFCE_FormaPag.Edit;
    dmCupomFiscal.dbTemp_NFCE_FormaPagVPAG.Value:=0;
    dmCupomFiscal.dbTemp_NFCE_FormaPagCARD_CNPJ.Clear;
    dmCupomFiscal.dbTemp_NFCE_FormaPagCARD_TBAND.Clear;
    dmCupomFiscal.dbTemp_NFCE_FormaPagCARD_CAUT.Clear;
    dmCupomFiscal.dbTemp_NFCE_FormaPag.Post;
    //Elimina cheques
    if(dmCupomFiscal.dbTemp_NFCE_FormaPagTPAG.Value='02')then
      EliminaCheque;
    if(dmCupomFiscal.dbTemp_NFCE_FormaPagTPAG.Value='13')then //Carta Frete
      EliminarCartaFrete;
    if(dmCupomFiscal.dbTemp_NFCE_FormaPagTPAG.Value='70')then //Conta Corrente
      EliminarContaCorrente;
    dmCupomFiscal.Transaction.CommitRetaining;
    dmCupomFiscal.getTemp_NFCE();
    if dmCupomFiscal.dbTemp_NFCEVLR_TROCO.AsCurrency < 0 then
    begin
      edtFormaPag.SetFocus;
      edtFormaPag.Clear;
      edtPagValor.Value := -(dmCupomFiscal.dbTemp_NFCEVLR_TROCO.AsCurrency); //Atrib
    end
    else
//      btnFinalizar.SetFocus;
  end;
end;

procedure TfrmFim.btnCancelarClick(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

procedure TfrmFim.btnConsumidorClick(Sender: TObject);
begin
  frmClifor:=TfrmClifor.create(Application);
  try
    case frmClifor.ShowModal of
    mrOk:begin
        dmCupomFiscal.Transaction.CommitRetaining;
        getFormasPagamento;
      end;
    mrCancel:begin
        dmCupomFiscal.Transaction.RollbackRetaining;
        getFormasPagamento;
        Abort;
      end;
    end;
  finally
    frmClifor.Free;
  end;
end;

procedure TfrmFim.btnPagamentoClick(Sender: TObject);
begin
  pagControle.ActivePage:=tabPrincipal;
  if edtFormaPag.Enabled then
  begin
    edtFormaPag.SetFocus;
    edtFormaPag.Clear;
  end;
end;

procedure TfrmFim.FormResize(Sender: TObject);
begin
  pnlFundo.Left:=Trunc((frmFim.Width/2)-(pnlFundo.Width/2));
  pnlFundo.Top :=Trunc((frmFim.Height/2)-(pnlFundo.Height/2)-(pnlCabecalho.Height/2)-
  (pnlFinalizar.Height/2)-(pnlRodape.Height/2));

  //Centraliza bot�es de Finaliza��o
  pnlFinalizarOrganiza.Top  := (pnlFinalizar.ClientHeight - pnlFinalizarOrganiza.Height) div 2;
  pnlFinalizarOrganiza.Left := (pnlFinalizar.ClientWidth - pnlFinalizarOrganiza.Width) div 2;
end;

procedure TfrmFim.grdTemp_ECF_FormaPagDblClick(Sender: TObject);
begin
  if not(dmCupomFiscal.dbTemp_NFCE_FormaPag.IsEmpty)then
  begin
    edtFormaPag.Text := dmCupomFiscal.dbTemp_NFCE_FormaPagFORMA_PAG.Text;
    edtFormaPag.SetFocus;
    edtFormaPag.SelectAll;
    edtFormaPagExit(Action);
  end;
end;

procedure TfrmFim.btnDescontoClick(Sender: TObject);
begin
  if(edtVlrDesconto.Enabled)then
    edtVlrDesconto.SetFocus;
end;

procedure TfrmFim.recalculaTotais;
var
  pDesc, Arredondamento, vDescCalc, vDifDesc, vMaiorDesconto : Currency;
  nItemMaiorVlrDesc: String;

  pAcresc, vAcrescCalc, vDifAcresc: Currency;
begin
  try
    Arredondamento := 0;
    dmCupomFiscal.getTemp_NFCE;
    //OK Percentual Desconto
    pDesc := (dmCupomFiscal.dbTemp_NFCEVLR_DESCONTO.Value /
      (dmCupomFiscal.dbTemp_NFCEVLR_SUBTOTAL.Value -
      strToCurr(edtVlrDescontoParcial.Text))) * 100;
    pDesc := StrToCurr(Formatfloat('##0.00',pDesc));

    pAcresc := (dmCupomFiscal.dbTemp_NFCEVLR_OUTRO.Value /
      (dmCupomFiscal.dbTemp_NFCEVLR_SUBTOTAL.Value) * 100);
    pAcresc := StrToCurr(Formatfloat('##0.00',pAcresc));

    vDescCalc := 0;
    vMaiorDesconto := 0;
    vDifDesc := 0;

    vAcrescCalc := 0;
    vDifAcresc := 0;
    dmCupomFiscal.dbTemp_NFCE_Item.Open;
    dmCupomFiscal.dbTemp_NFCE_Item.First;
    while not(dmCupomFiscal.dbTemp_NFCE_Item.eof) do
    begin
      Arredondamento := Arredondamento + StrToCurr(FormatFloat('##0.00',
        ((dmCupomFiscal.dbTemp_NFCE_ItemVLR_TOTAL.Value * pDesc)/100)));
      dmCupomFiscal.dbTemp_NFCE_Item.Edit; //StrToCurr(FormatFloat('##0.00',

      dmCupomFiscal.dbTemp_NFCE_ItemVLR_OUTRO.Value := StrToCurr(FormatFloat('##0.00',
        ((dmCupomFiscal.dbTemp_NFCE_ItemVLR_TOTAL.Value * pAcresc) /100)));
      vAcrescCalc := vAcrescCalc + dmCupomFiscal.dbTemp_NFCE_ItemVLR_OUTRO.Value;

      if(dmCupomFiscal.dbTemp_NFCE_Item.RecNo < dmCupomFiscal.dbTemp_NFCE_Item.RecordCount)then
        dmCupomFiscal.dbTemp_NFCE_ItemVLR_DESCONTO.Value := StrToCurr(FormatFloat('##0.00',
          ((dmCupomFiscal.dbTemp_NFCE_ItemVLR_TOTAL.Value * pDesc) /100)))
      else
      begin
        dmCupomFiscal.dbTemp_NFCE_ItemVLR_DESCONTO.Value := StrToCurr(FormatFloat('##0.00',
          ((dmCupomFiscal.dbTemp_NFCE_ItemVLR_TOTAL.Value * pDesc) /100)-
          (Arredondamento - dmCupomFiscal.dbTemp_NFCEVLR_DESCONTO.Value)));
        if(dmCupomFiscal.dbTemp_NFCE_ItemVLR_DESCONTO.Value < 0)then
          dmCupomFiscal.dbTemp_NFCE_ItemVLR_DESCONTO.Value := 0;
      end;
      dmCupomFiscal.dbTemp_NFCE_Item.Post;
      vDescCalc := vDescCalc + dmCupomFiscal.dbTemp_NFCE_ItemVLR_DESCONTO.Value;
      dmCupomFiscal.dbTemp_NFCE_Item.Next;
      if(vMaiorDesconto < dmCupomFiscal.dbTemp_NFCE_ItemVLR_DESCONTO.Value)then
      begin
        vMaiorDesconto := dmCupomFiscal.dbTemp_NFCE_ItemVLR_DESCONTO.Value;
        nItemMaiorVlrDesc := dmCupomFiscal.dbTemp_NFCE_ItemITEM.Value;
      end;
    end;
    //Quando chegar ao fim da Opera��o vai testar se o desconto rateado �
    //diferente do desconto que o usuario informou, se for vai ajustar no
    //maior desconto dado
    if(vDescCalc <> dmCupomFiscal.dbTemp_NFCEVLR_DESCONTO.Value)then
    begin
      vDifDesc := (vDescCalc - dmCupomFiscal.dbTemp_NFCEVLR_DESCONTO.Value);
      if (dmCupomFiscal.dbTemp_NFCE_Item.Locate('Item',nItemMaiorVlrDesc,[loCaseInsensitive])) then
      begin
        dmCupomFiscal.dbTemp_NFCE_Item.Edit;
        if(vDifDesc < 0)Then
          dmCupomFiscal.dbTemp_NFCE_ItemVLR_DESCONTO.value :=
            dmCupomFiscal.dbTemp_NFCE_ItemVLR_DESCONTO.value + Abs(vDifDesc)
        else
          dmCupomFiscal.dbTemp_NFCE_ItemVLR_DESCONTO.value :=
            dmCupomFiscal.dbTemp_NFCE_ItemVLR_DESCONTO.value - vDifDesc;
        dmCupomFiscal.dbTemp_NFCE_Item.Post;
      end;
    end;
    if(vAcrescCalc <> dmCupomFiscal.dbTemp_NFCEVLR_OUTRO.Value)then
    begin
      vDifAcresc := (vAcrescCalc - dmCupomFiscal.dbTemp_NFCEVLR_OUTRO.Value);
      dmCupomFiscal.dbTemp_NFCE_Item.First;
      dmCupomFiscal.dbTemp_NFCE_Item.Edit;
      if(vAcrescCalc < 0)Then
        dmCupomFiscal.dbTemp_NFCE_ItemVLR_OUTRO.value :=
          dmCupomFiscal.dbTemp_NFCE_ItemVLR_OUTRO.value + Abs(vDifAcresc)
      else
        dmCupomFiscal.dbTemp_NFCE_ItemVLR_OUTRO.value :=
          dmCupomFiscal.dbTemp_NFCE_ItemVLR_OUTRO.value - vDifAcresc;
      dmCupomFiscal.dbTemp_NFCE_Item.Post;
    end;
    lblDescPerc.Caption := FormatCurr('##0.00%',pDesc);
    lblAcrescPerc.Caption := FormatCurr('##0.00%',pAcresc);
    dmCupomFiscal.dbTemp_NFCE_Item.Close;
    dmCupomFiscal.Transaction.CommitRetaining;
  finally
    getTemp_NFCE_Item();
    dmCupomFiscal.getTemp_NFCE_FormaPag();
    dmCupomFiscal.getTemp_NFCE_Fatura();
    dmCupomFiscal.getTemp_NFCE();
  end;
end;

procedure TfrmFim.edtPagValorExit(Sender: TObject);
begin
  //Ver para usar case
  if(edtPagValor.Value > 0)then
  begin
    case dmCupomFiscal.dbTemp_NFCE_FormaPagTPAG.AsInteger of
    //-- Lauro Cheque ----------------------------------------------------------
    2:begin //02 - Cheque
        frmCheque:=TfrmCheque.create(Application);
        frmCheque.ShowModal;
        if dmCupomFiscal.dbTemp_NFCE_FormaPag.Locate('FORMA_PAG',
          VarArrayOf([cdsFormaPagamentoID.AsString]), [loCaseInsensitive])then
        begin
          dmCupomFiscal.dbTemp_NFCE_FormaPag.edit;
          dmCupomFiscal.dbTemp_NFCE_FormaPagVPAG.Value := 0;
          dmCupomFiscal.dbTemp_NFCE_FormaPag.Post;
          dmCupomFiscal.Transaction.CommitRetaining;
          recalculaTotais;
          application.ProcessMessages;
          frmFim.edtPagValor.Value:=frmCheque.qrCheque_TotalTOTAL.Value;
          btnPagar.Click;
        end;
        frmCheque.Free;
      end;
    //-- Lauro Carta Frete------------------------------------------------------
    13:begin //13 - Carta Frete
        frmCartaFrete:=TfrmCartaFrete.create(Application);
        frmCartaFrete.ShowModal;
        if dmCupomFiscal.dbTemp_NFCE_FormaPag.Locate('FORMA_PAG',
          VarArrayOf([cdsFormaPagamentoID.AsString]), [loCaseInsensitive])then
        begin
          dmCupomFiscal.dbTemp_NFCE_FormaPag.edit;
          dmCupomFiscal.dbTemp_NFCE_FormaPagVPAG.Value := 0;
          dmCupomFiscal.dbTemp_NFCE_FormaPag.Post;
          dmCupomFiscal.Transaction.CommitRetaining;
          recalculaTotais;
          application.ProcessMessages;
          frmFim.edtPagValor.Value:=frmCartaFrete.qrCFrete_TotalTOTAL.Value;
          btnPagar.Click;
        end;
        frmCartaFrete.Free;
      end;
    70:begin // 70 > Credito  Quando Atualiza na Base EST_ECF_FormaPag Muda 99
        frmContaCorrente:=TfrmContaCorrente.create(Application);
        frmContaCorrente.ShowModal;
        If dmCupomFiscal.dbTemp_NFCE_FormaPag.Locate('FORMA_PAG',
            VarArrayOf([cdsFormaPagamentoID.AsString]), [loCaseInsensitive])then
        begin
          dmCupomFiscal.dbTemp_NFCE_FormaPag.edit;
          dmCupomFiscal.dbTemp_NFCE_FormaPagVPAG.Value := 0;
          dmCupomFiscal.dbTemp_NFCE_FormaPag.Post;
          dmCupomFiscal.Transaction.CommitRetaining;
          recalculaTotais;
          application.ProcessMessages;
          frmFim.edtPagValor.Value:=frmContaCorrente.qrCCorrente_TotalTOTAL.Value;
          btnPagar.Click;
        end;
        frmContaCorrente.Free;
      end;
    end;
  end;
end;

// Lauro Cheque
procedure TfrmFim.EliminaCheque;
begin
  dmCupomFiscal.IBSQL1.close;
  dmCupomFiscal.IBSQL1.SQL.clear;
  dmCupomFiscal.IBSQL1.SQL.Add('Delete from TEMP_NFCE_FORMAPAG_CHEQUE where Caixa = :pCaixa');
  dmCupomFiscal.IBSQL1.ParamByName('pCaixa').AsString := FormatFloat('0000',FRENTE_CAIXA.Caixa);
  dmCupomFiscal.IBSQL1.ExecQuery;
  dmCupomFiscal.IBSQL1.Close;
end;

procedure TfrmFim.EliminarCartaFrete;
begin
  dmCupomFiscal.IBSQL1.close;
  dmCupomFiscal.IBSQL1.SQL.clear;
  dmCupomFiscal.IBSQL1.SQL.Add('Delete from TEMP_NFCE_FORMAPAG_CFRETE where Caixa = :pCaixa');
  dmCupomFiscal.IBSQL1.ParamByName('pCaixa').AsString := FormatFloat('0000',FRENTE_CAIXA.Caixa);
  dmCupomFiscal.IBSQL1.ExecQuery;
  dmCupomFiscal.IBSQL1.Close;
end;

procedure TfrmFim.EliminarContaCorrente;
begin
  dmCupomFiscal.IBSQL1.close;
  dmCupomFiscal.IBSQL1.SQL.clear;
  dmCupomFiscal.IBSQL1.SQL.Add('Delete from TEMP_NFCE_FORMAPAG_CCORRENTE where Caixa = :pCaixa');
  dmCupomFiscal.IBSQL1.ParamByName('pCaixa').AsString := FormatFloat('0000',FRENTE_CAIXA.Caixa);
  dmCupomFiscal.IBSQL1.ExecQuery;
  dmCupomFiscal.IBSQL1.Close;
end;

procedure TfrmFim.RestauraFoco;
var
  Form: TForm;
  Habilitado: Boolean;
begin
  Form := nil;
  //Verifica se j� existe um form ativo na tela.
  if Assigned(Screen.ActiveForm) then
    Form := Screen.ActiveForm
    //Do contr�rio, pega o �ltimo form criado
  else if Assigned(Screen.Forms[Screen.FormCOunt - 1]) then
    Form := Screen.Forms[Screen.FormCOunt - 1];
  if Assigned(Form) then
  begin
    //Verifica o estado da propriedade 'Enabled' do form
    Habilitado := Form.Enabled;
    //Habilita o form para permitir que o foco seja setado para o mesmo
    Form.Enabled := TRUE;
    //Envia o Form em quest�o para a frente da tela
    SetForeGroundWindow(Form.Handle);
    //Seta o foco da aplica��o para o form
    if Form.CanFocus then
      Form.SetFocus;
    Form.Refresh;
    //Retorna a propriedade 'Enabled' para o estado anterior
    Form.Enabled := Habilitado;
  end;
end;

//-- Lauro 06.07.16 ------------------------------------------------------------
//Retorna s�rie padr�o vinculada ao local atrav�s do centro de custo utilizado nos cupons fiscais
{function TfrmFim.getSerieNFe: String;
begin
  DMCupomFiscal.dbQuery2.Active:=False;
  DMCupomFiscal.dbQuery2.SQL.Clear;
  DMCupomFiscal.dbQuery2.SQL.Add('Select coalesce(l.nfe_serie_padrao, 0) nfe_serie_padrao from locais_config_nfe l'+
                                 ' left outer join ccustos c on c.local=l.local'+
                                 ' where c.id=:pCCusto');
  DMCupomFiscal.dbQuery2.ParamByName('pCcusto').Value := CCUSTO.codigo;
  DMCupomFiscal.dbQuery2.Active:=True;
  DMCupomFiscal.dbQuery2.First;
  Result:=DMCupomFiscal.dbQuery2.FieldByName('nfe_serie_padrao').AsString;
end; }


//procedure TfrmFim.Gera_NFe;
//Var Fat_Parcela:Integer;
//    xDup_VLiq:Currency;
//    Item:Integer;
//begin
//  If not(msgPergunta('Deseja gerar Nota Fiscal ?','')) then
//  begin
//    Abort;
//  end;
//
//  Fat_Parcela:=0;
//  xDup_VLiq:=0;
//  Item:=0;
//
//  //Verifica se cliente possue TEMP_EST_NF
//  {dmCupomFiscal.dbQuery1.Active:=False;
//  dmCupomFiscal.dbQuery1.SQL.Clear;
//  dmCupomFiscal.dbQuery1.SQL.Add('Select Count(*) as xFlag From temp_est_Nf Where Clifor=:pCliFor');
//  dmCupomFiscal.dbQuery1.ParamByName('pCliFor').AsInteger:=StrToIntDef(edtCliente.Text,0);
//  dmCupomFiscal.dbQuery1.Active:=True;
//  dmCupomFiscal.dbQuery1.First;
//  If (dmCupomFiscal.dbQuery1.FieldByName('xFlag').AsInteger > 0) then
//  begin
//    //----------------- Exclue Temp_Est_NF -----------------
//    If msgPergunta('Existe uma Nota Fiscal em digita��o!'+#13+
//                   'Deseja descartar a Nota Fiscal em digita��o para incluir este Cupom Fiscal ?', '') then
//    begin
//      dmCupomFiscal.IBSQL1.Close;
//      dmCupomFiscal.IBSQL1.SQL.Clear;
//      dmCupomFiscal.IBSQL1.SQL.Add('Delete From temp_Est_NF Where CliFor=:pCliFor');
//      dmCupomFiscal.IBSQL1.ParamByName('pClifor').asInteger:=StrToIntDef(edtCliente.Text,0);
//      dmCupomFiscal.IBSQL1.ExecQuery;
//      dmCupomFiscal.IBSQL1.Close;
//    end
//    Else
//    begin
//      dmCupomFiscal.dbQuery1.Active:=False;
//      Abort;
//    end
//  end; }
//
//  //----------------- Gera Nota Fiscal -----------------
//  try
//    dmCupomFiscal.dbQuery1.Active:=False;
//    dmCupomFiscal.dbQuery1.SQL.Clear;
//    dmCupomFiscal.dbQuery1.SQL.Add('Select * From Est_ECF Where ID=:pID');
//    dmCupomFiscal.dbQuery1.ParamByName('pID').AsInteger:=ID;
//    dmCupomFiscal.dbQuery1.Active:=True;
//    dmCupomFiscal.dbQuery1.First;
//
//    //Insert Est_NF
//    dmCupomFiscal.db_Temp_Est_NF.Open;
//    dmCupomFiscal.db_Temp_Est_NF.Insert;
//    dmCupomFiscal.db_Temp_Est_NFCLIFOR.Value    := dmCupomFiscal.dbQuery1.FieldByName('CliFor').AsInteger;
//    dmCupomFiscal.db_Temp_Est_NFCLIFOR_IE.Value := dmCupomFiscal.dbQuery1.FieldByName('CF_IE').AsString;
//    dmCupomFiscal.db_Temp_Est_NFCCUSTO.Value    := dmCupomFiscal.dbQuery1.FieldByName('CCusto').AsInteger;
//    dmCupomFiscal.db_Temp_Est_NFOPERACAO.Value  := NF_Operacao;
//    dmCupomFiscal.db_Temp_Est_NFES.Value        := 'S'; //??
//    dmCupomFiscal.db_Temp_Est_NFESTOQUE.Value   := 'N'; //??
//    dmCupomFiscal.db_Temp_Est_NFROYALTIES.Value := 'X';
//    dmCupomFiscal.db_Temp_Est_NFFUNCIONARIO.Value := dmCupomFiscal.dbQuery1.FieldByName('Funcionario').AsInteger;
//    dmCupomFiscal.db_Temp_Est_NFFUN_USUARIO.Value := dmCupomFiscal.dbQuery1.FieldByName('FUN_USUARIO').AsInteger;
//    dmCupomFiscal.db_Temp_Est_NFFORMA_PAG.Value   := 199; //??
//    dmCupomFiscal.db_Temp_Est_NFCONDICAO_PAG.Value:= 0; //??
//    dmCupomFiscal.db_Temp_Est_NFDATA_Mov.Value  := Date;
//    dmCupomFiscal.db_Temp_Est_NFDATA.Value      := Date;
//    dmCupomFiscal.db_Temp_Est_NFDATAE.Value     := Date;
//    dmCupomFiscal.db_Temp_Est_NFHORAE.Value     := Time;
//    dmCupomFiscal.db_Temp_Est_NFSerie.Value     := getSerieNFe;
//    if dmCupomFiscal.db_Temp_Est_NFSerie.Value = '0' then
//      msgInformacao('S�rie informada � inv�lida','Informa��o');
//    dmCupomFiscal.db_Temp_Est_NFNUMERO.Value    := 0;
//    dmCupomFiscal.db_Temp_Est_NFIND_Emitente.Value  := '0';  //--0-Proprio, 1-Terceiro
//    dmCupomFiscal.db_Temp_Est_NFModelo_Doc.Value    := '55';
//    dmCupomFiscal.db_Temp_Est_NFFINNFE.value    := '1';
//    dmCupomFiscal.db_Temp_Est_NFSTATUS.Value    := '0';
//
//    dmCupomFiscal.db_Temp_Est_NFCOD_SIT.Value   := '08';
//    dmCupomFiscal.db_Temp_Est_NFNUMERO_NFREF.Value := 0;
//    dmCupomFiscal.db_Temp_Est_NFROMANEIO.Value  := 0;
//    dmCupomFiscal.db_Temp_Est_NFORCAMENTO.Value := 0;
//    dmCupomFiscal.db_Temp_Est_NFCONTRATO.Value  := 0;
//    dmCupomFiscal.db_Temp_Est_NFRECIBO.Value    := 0;
//    dmCupomFiscal.db_Temp_Est_NFORDEM_CARGA.Value := 0;
//    dmCupomFiscal.db_Temp_Est_NFLAN_MANUAL.value := 'N';
//
//    //dmCupomFiscal.db_Temp_Est_NFFAT_NFAT.Value:= //Tg
//    dmCupomFiscal.db_Temp_Est_NFDUP_VLIQ.Value  := xDup_VLiq; //?? Cupom
//    dmCupomFiscal.db_Temp_Est_NFTR_FRETE.Value  := '9'; //9-Sem Frete
//{    dmCupomFiscal.db_Temp_Est_NFDA_OBSERVACAO.Value := 'NFC-e:'+dmCupomFiscal.dbQuery1.FieldByName('Numero').Text+'   Data:'+dmCupomFiscal.dbQuery1.FieldByName('Data').Text+';'+
//                                                       IfThen(edtPlaca.Text<>'', 'Placa:'+edtPlaca.Text+'           ','')+
//                                                       IfThen(edtKM.Text<>'', 'KM Atual:'+edtKM.Text,'');}
//
//    dmCupomFiscal.db_Temp_Est_NF.Post;
//    dmCupomFiscal.db_Temp_Est_NF.Close;
//
//    // Item
//    dmCupomFiscal.dbQuery2.Active:=False;
//    dmCupomFiscal.dbQuery2.SQL.Clear;
//    dmCupomFiscal.dbQuery2.SQL.Add('Select * From Est_ECF_Item Where ID_ECF=:pID');
//    dmCupomFiscal.dbQuery2.ParamByName('pID').asInteger:=ID;
//    dmCupomFiscal.dbQuery2.Active:=True;
//    dmCupomFiscal.dbQuery2.First;
//    while not(dmCupomFiscal.dbQuery2.EOf) do
//    begin
//      Inc(Item);
//      dmCupomFiscal.db_temp_Est_NF_item.Open;
//      dmCupomFiscal.db_temp_Est_NF_item.Insert;
//      dmCupomFiscal.db_temp_Est_NF_itemITEM.Value       := Item;
//      dmCupomFiscal.db_temp_Est_NF_itemCLIFOR.Value     := dmCupomFiscal.dbQuery1.FieldByName('CliFor').AsInteger;
//      dmCupomFiscal.db_temp_Est_NF_itemMERCADORIA.Value := dmCupomFiscal.dbQuery2.FieldByName('Mercadoria').AsString;
//      dmCupomFiscal.db_temp_Est_NF_itemVLR_UNITARIO.AsVariant := dmCupomFiscal.dbQuery2.FieldByName('Vlr').AsCurrency;
//      dmCupomFiscal.db_temp_Est_NF_itemQUANTIDADE.Value   := dmCupomFiscal.dbQuery2.FieldByName('Qtd').AsCurrency;
//      dmCupomFiscal.db_temp_Est_NF_itemVLR_DESCONTO.Value := dmCupomFiscal.dbQuery2.FieldByName('Vlr_Desconto_Item').AsCurrency+dmCupomFiscal.dbQuery2.FieldByName('Vlr_Desconto').AsCurrency;
//      dmCupomFiscal.db_temp_Est_NF_itemUNIDADE.Value      := 'UN'; //Tg Ajsuta
//      dmCupomFiscal.db_Temp_Est_NF_ItemSERVICO.Value      := 'N'; //Tg Ajsuta
//      dmCupomFiscal.db_Temp_Est_NF_ItemVLR_VENDA_ATUALIZAR.Value := 0;
//      dmCupomFiscal.db_Temp_Est_NF_ItemOBSERVACAO.Value   :='NFC-e:'+dmCupomFiscal.dbQuery1.FieldByName('Numero').Text+'   Data:'+dmCupomFiscal.dbQuery1.FieldByName('Data').Text;
//      dmCupomFiscal.db_Temp_Est_NF_ItemVLR_FRETE.Value    := 0;  //??
//      dmCupomFiscal.db_Temp_Est_NF_ItemVLR_SEGURO.Value   := 0; //??
//      dmCupomFiscal.db_Temp_Est_NF_ItemVLR_OUTRAS.Value   := 0; //??
//      dmCupomFiscal.db_Temp_Est_NF_ItemVLR_SACA.Value     := 0;   //??
//
//      dmCupomFiscal.db_Temp_Est_NF_ItemALI_II.Value       := 0;   //??
//      dmCupomFiscal.db_Temp_Est_NF_ItemVLR_BC_II.Value    := 0;   //??
//      dmCupomFiscal.db_Temp_Est_NF_ItemVLR_II.Value       := 0;   //??
//
//      dmCupomFiscal.db_Temp_Est_NF_ItemALI_IPI.Value      := 0;   //??
//      dmCupomFiscal.db_Temp_Est_NF_ItemALI_ICM.Value      := 0;   //??
//      dmCupomFiscal.db_Temp_Est_NF_ItemALI_ICMS.Value     := 0;  //??
//
//      dmCupomFiscal.db_Temp_Est_NF_ItemVLR_ICM.Value      := 0;   //??
//      dmCupomFiscal.db_Temp_Est_NF_ItemVLR_ICMS.Value     := 0;   //??
//      dmCupomFiscal.db_Temp_Est_NF_ItemVLR_ICM_SN.Value   := 0;   //??
//
//      dmCupomFiscal.db_Temp_Est_NF_ItemVLR_BC_ICM.Value   := 0;   //??
//      dmCupomFiscal.db_Temp_Est_NF_ItemVLR_BC_ICMS.Value  := 0;   //??
//      dmCupomFiscal.db_Temp_Est_NF_ItemVLR_BC_ICM_SN.Value:= 0;   //??
//
//      dmCupomFiscal.db_Temp_Est_NF_ItemVLR_IPI.Value      := 0;   //??
//      dmCupomFiscal.db_Temp_Est_NF_ItemVLR_BC_IPI.Value   := 0;   //??
//
//      dmCupomFiscal.db_Temp_Est_NF_ItemPRED_BC.Value      := 0;   //??
//      dmCupomFiscal.db_Temp_Est_NF_ItemPRED_BC_ST.Value   := 0;   //??
//      dmCupomFiscal.db_Temp_Est_NF_ItemPMVA_ST.Value      := 0;   //??
//
//      dmCupomFiscal.db_Temp_Est_NF_ItemID_PEDIDO.Value    := 0;
//      dmCupomFiscal.db_Temp_Est_NF_ItemRECEITA.Value      := 0;
//
//      dmCupomFiscal.db_temp_Est_NF_item.Post;
//      dmCupomFiscal.db_temp_Est_NF_item.Close;
//
//      //Insert NFCe Vinculado
//      dmCupomFiscal.db_Temp_Est_NF_NFe.Open;
//      dmCupomFiscal.db_Temp_Est_NF_NFe.Insert;
//      dmCupomFiscal.db_Temp_Est_NF_NFeITEM.Value := Item;
//      dmCupomFiscal.db_Temp_Est_NF_NFeCLIFOR.Value := dmCupomFiscal.dbQuery1.FieldByName('CliFor').AsInteger;
//      dmCupomFiscal.db_Temp_Est_NF_NFeID_ECF.Value := dmCupomFiscal.dbQuery1.FieldByName('ID').Value;
//      dmCupomFiscal.db_Temp_Est_NF_NFeCHAVE_NFEREF.Value := dmCupomFiscal.dbQuery1.FieldByName('CHAVE_NFCE').AsString;
//      dmCupomFiscal.db_Temp_Est_NF_NFe.Post;
//      dmCupomFiscal.db_Temp_Est_NF_NFe.Close;
//
//      dmCupomFiscal.dbQuery2.Next;
//    end;
//    dmCupomFiscal.Transaction.CommitRetaining;
//
//    //Verifica Tributacao dos Itens da NF
//    dmCupomFiscal.dbQuery3.Active:=False;
//    dmCupomFiscal.dbQuery3.SQL.Clear;
//    dmCupomFiscal.dbQuery3.SQL.Add('Select coalesce(count(*),0) as Flag From Temp_Est_NF_Item Where Clifor=:pCliFor and ((CST_ICM is null) or (CFOP is null) )');
//    dmCupomFiscal.dbQuery3.ParamByName('pCliFor').asInteger:=dmCupomFiscal.dbQuery1.FieldByName('CliFor').AsInteger;
//    dmCupomFiscal.dbQuery3.Active:=True;
//    dmCupomFiscal.dbQuery3.First;
//    If dmCupomFiscal.dbQuery3.FieldByName('Flag').AsInteger>0 then
//      msgErro('As mercadorias da NF-e possuem ERRO(s) no CFOP ou CST de ICMS.'+#13+
//              'NF-e pendente e aguardando corre��es.','')
//    else
//    begin
//      If msgPergunta('Deseja atualizar esta NF-e ?', '') then
//      begin
//        //-- Gera Nota do Temp_Est_NF para Est_NF
//        {dmCupomFiscal.IBSQL1.Close;
//        dmCupomFiscal.IBSQL1.SQL.Clear;
//        dmCupomFiscal.IBSQL1.SQL.Add('Update temp_Est_NF Set Status=''F'' Where CliFor=:pCliFor');
//        dmCupomFiscal.IBSQL1.ParamByName('pClifor').asInteger:=StrToIntDef(edtCliente.Text,0);
//        dmCupomFiscal.IBSQL1.ExecQuery;
//        dmCupomFiscal.IBSQL1.Close;
//
//        dmCupomFiscal.Transaction.CommitRetaining;
//
//        //-- Exclue Temp_Est_NF
//        dmCupomFiscal.IBSQL1.Close;
//        dmCupomFiscal.IBSQL1.SQL.Clear;
//        dmCupomFiscal.IBSQL1.SQL.Add('Delete From temp_Est_NF Where CliFor=:pCliFor');
//        dmCupomFiscal.IBSQL1.ParamByName('pClifor').asInteger:=StrToIntDef(edtCliente.Text,0);
//        dmCupomFiscal.IBSQL1.ExecQuery;
//        dmCupomFiscal.IBSQL1.Close;
//        }
//        dmCupomFiscal.Transaction.CommitRetaining;
//        msgAviso('NF-e gerada com sucesso, apenas aguardando Envio !', '');
//      end
//      else
//        msgAviso('NF-e gerada pendente, aguardando Atualiza��o !', '');
//    end;
//  except
//    on e:exception do
//    begin
//      dmCupomFiscal.Transaction.RollbackRetaining;
//      logErros(Self, caminhoLog, 'Erro ao gerar Nota Fiscal. '+e.Message, 'Erro ao gerar Nota Fiscal', 'S', e);
//    end;
//  end;
//end;
end.
