unit uFrmCartaFrete;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Grids, DBGrids, Mask, StdCtrls, TREdit, DBCtrls,
  Buttons, Provider, DB, DBClient, ACBrBase, ACBrValidador,
  IBCustomDataSet, IBQuery;


type
  TfrmCartaFrete = class(TForm)
    Panel1: TPanel;
    btFechar: TBitBtn;
    Panel2: TPanel;
    DBEdit1: TDBEdit;
    DBEdit4: TDBEdit;
    Label1: TLabel;
    Label4: TLabel;
    Label7: TLabel;
    DBEdit7: TDBEdit;
    Label8: TLabel;
    DBEdit8: TDBEdit;
    btExcluir: TBitBtn;
    btSalva: TBitBtn;
    btNovo: TBitBtn;
    btCancelar: TBitBtn;
    Label9: TLabel;
    dsCFrete_Total: TDataSource;
    dsCFrete: TDataSource;
    DBEdit9: TDBEdit;
    qrCFrete_Total: TIBQuery;
    qrCFrete_TotalTOTAL: TIBBCDField;
    edNome: TEdit;
    dbgrd1: TDBGrid;
    procedure btFecharClick(Sender: TObject);
    procedure btNovoClick(Sender: TObject);
    procedure btSalvaClick(Sender: TObject);
    procedure btExcluirClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure DBEdit8Exit(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure dsCFreteStateChange(Sender: TObject);
    procedure GetCFrete;
    procedure GetTotal;
    procedure DBEdit4Exit(Sender: TObject);
    procedure DBEdit1Exit(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Label1Click(Sender: TObject);
    function GetCartaFreteExisteTemp(pCaixa: String; pID, pClifor,
      pNum: Integer): Boolean;
    procedure dsCFreteDataChange(Sender: TObject; Field: TField);
    procedure dbgrd1DblClick(Sender: TObject);
  private
    { Private declarations }

  public
    { Public declarations }
  end;

var
  frmCartaFrete: TfrmCartaFrete;

implementation

uses uDMCupomFiscal, uFrmFim, uFrmPrincipal, uDMComponentes, uFuncoes,
  uFrmPesquisaCliente;

{$R *.dfm}

procedure TfrmCartaFrete.btFecharClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmCartaFrete.btNovoClick(Sender: TObject);
begin
  DMCupomFiscal.dbTemp_NFCe_FormaPag_CFrete.Open;
  DMCupomFiscal.dbTemp_NFCe_FormaPag_CFrete.Insert;
  dmCupomFiscal.dbTemp_NFCe_FormaPag_CFreteID.Value := dmCupomFiscal.dbTemp_NFCE_FormaPagID.AsInteger;
  dmCupomFiscal.dbTemp_NFCe_FormaPag_CFreteCaixa.Value := FormatFloat('0000', FRENTE_CAIXA.Caixa);
  DMCupomFiscal.dbTemp_NFCe_FormaPag_CFreteData_Ven.Value:=Date;
  DMCupomFiscal.dbTemp_NFCe_FormaPag_CFreteValor.Value:=0;
  DBEdit1.SetFocus;
end;

procedure TfrmCartaFrete.btSalvaClick(Sender: TObject);
begin
  if StrToIntDef(DBEdit1.Text,0)<=0 then
  begin
    ShowMessage('N�o Foi Informado Transportador!');
    DBEdit1.SetFocus;
    Abort;
  end;

  iF DMCupomFiscal.dbTemp_NFCe_FormaPag_CFrete.State in [dsInsert, dsEdit] then
  begin
    if((DMCupomFiscal.dbTemp_NFCe_FormaPag_CFrete.State in [dsInsert]) and
      (GetCartaFreteExisteTemp(
        DMCupomFiscal.dbTemp_NFCe_FormaPag_CFreteCAIXA.asString,
        DMCupomFiscal.dbTemp_NFCe_FormaPag_CFreteID.AsInteger,
        DMCupomFiscal.dbTemp_NFCe_FormaPag_CFreteCLIFOR.AsInteger,
        DMCupomFiscal.dbTemp_NFCe_FormaPag_CFreteNUMERO.AsInteger)))Then
    begin
      msgAviso('Esse registro ja consta como lan�ado'+#13+
        'Por favor edite ou exclua para lan�ar novamente.',Application.title);
      btCancelarClick(Action);
      abort;
    end;
    DMCupomFiscal.dbTemp_NFCe_FormaPag_CFrete.Post;
    DMCupomFiscal.Transaction.CommitRetaining;
    DMCupomFiscal.dbTemp_NFCe_FormaPag_CFrete.Close;
    edNome.Clear;

    GetCFrete;
    GetTotal;
  end;

  btNovo.SetFocus;
end;

procedure TfrmCartaFrete.btExcluirClick(Sender: TObject);
begin
  If(msgPergunta('Excluir registro selecionado?',Application.Title))then
  begin
    DMCupomFiscal.dbTemp_NFCe_FormaPag_CFrete.Delete;
    DMCupomFiscal.Transaction.CommitRetaining;
    DMCupomFiscal.dbTemp_NFCe_FormaPag_CFrete.Close;
    edNome.Clear;

    GetCFrete;
    GetTotal;
  end;
  btNovo.SetFocus;
end;

procedure TfrmCartaFrete.btCancelarClick(Sender: TObject);
begin
   DMCupomFiscal.dbTemp_NFCe_FormaPag_CFrete.Cancel;
   DMCupomFiscal.dbTemp_NFCe_FormaPag_CFrete.Close;
   DMCupomFiscal.Transaction.RollbackRetaining;
   btNovo.SetFocus;
   edNome.Clear;
end;

procedure TfrmCartaFrete.DBEdit1Exit(Sender: TObject);
begin
  if(trim(DBEdit1.text) <> '')then
  begin
    DMCupomFiscal.dbQuery1.Active:=False;
    DMCupomFiscal.dbQuery1.SQL.Clear;
    DMCupomFiscal.dbQuery1.SQL.Add('Select Nome From CliFor Where (ID=:pID)');
    DMCupomFiscal.dbQuery1.ParamByName('pID').AsInteger:=StrToIntDef(DBEdit1.Text,0);
    DMCupomFiscal.dbQuery1.Active:=True;
    DMCupomFiscal.dbQuery1.First;
    if DMCupomFiscal.dbQuery1.EOF then
    begin
      ShowMessage('Transportador n�o cadastrado como cliente/fornecedor.');
      DBEdit1.SetFocus;
      Abort
    End
    Else
    begin
      edNome.Text:=DMCupomFiscal.dbQuery1.FieldByName('Nome').AsString;
    end;
  end
  Else
    btCancelar.SetFocus;
end;

procedure TfrmCartaFrete.DBEdit4Exit(Sender: TObject);
begin
  if StrToIntDef(DBEdit4.Text,0)<=0 then
  begin
    ShowMessage('Numero da Carta Frete � Obrigat�rio!');
    DBEdit4.SetFocus;
    Abort;
  end;
end;

procedure TfrmCartaFrete.DBEdit8Exit(Sender: TObject);
begin
  If DMCupomFiscal.dbTemp_NFCe_FormaPag_CFreteValor.Value<=0 then
  begin
    ShowMessage('Valor deve ser Maior que Zero.');
    DBEdit8.SetFocus;
    Abort;
  end;
end;

procedure TfrmCartaFrete.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  DMCupomFiscal.dbTemp_NFCe_FormaPag_CFrete.Close;
end;

procedure TfrmCartaFrete.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  If (Key=vk_escape) then Close;

  //Cliente
  If (key=vk_F12) and (DBEdit1.Focused) then Label1Click(Action);
end;

procedure TfrmCartaFrete.FormShow(Sender: TObject);
begin
  GetCFrete;
  GetTotal;
end;

procedure TfrmCartaFrete.dbgrd1DblClick(Sender: TObject);
begin
  DBEdit1.SetFocus;
end;

procedure TfrmCartaFrete.dsCFreteDataChange(Sender: TObject; Field: TField);
begin
  edNome.Text:=DMCupomFiscal.dbTemp_NFCe_FormaPag_CFreteNOME.Text;
end;

procedure TfrmCartaFrete.dsCFreteStateChange(Sender: TObject);
begin
  btNovo.Enabled    :=not(DMCupomFiscal.dbTemp_NFCe_FormaPag_CFrete.State in [dsInsert,dsEdit]);
  btSalva.Enabled   :=   (DMCupomFiscal.dbTemp_NFCe_FormaPag_CFrete.State in [dsInsert,dsEdit]);
  btExcluir.Enabled :=not(DMCupomFiscal.dbTemp_NFCe_FormaPag_CFrete.State in [dsInsert,dsEdit])
                      and(DMCupomFiscal.dbTemp_NFCe_FormaPag_CFrete.State in [dsBrowse]);
  btCancelar.Enabled:=   (DMCupomFiscal.dbTemp_NFCe_FormaPag_CFrete.State in [dsInsert,dsEdit,dsBrowse]);
  btFechar.Enabled  :=not(DMCupomFiscal.dbTemp_NFCe_FormaPag_CFrete.State in [dsInsert,dsEdit]);
  dbgrd1.Enabled    :=not(DMCupomFiscal.dbTemp_NFCe_FormaPag_CFrete.State in [dsInsert,dsEdit]);
end;

procedure TfrmCartaFrete.GetCFrete;
begin
  dmCupomFiscal.dbTemp_NFCe_FormaPag_CFrete.Close;
  dmCupomFiscal.dbTemp_NFCe_FormaPag_CFrete.ParamByName('pCaixa').asString  := FormatFloat('0000', FRENTE_CAIXA.Caixa);
  dmCupomFiscal.dbTemp_NFCe_FormaPag_CFrete.ParamByName('pID').AsInteger  := dmCupomFiscal.dbTemp_NFCE_FormaPagID.AsInteger;
  dmCupomFiscal.dbTemp_NFCe_FormaPag_CFrete.Open;
end;

procedure TfrmCartaFrete.GetTotal;
begin
  qrCFrete_Total.Active := false;
  qrCFrete_Total.ParamByName('pCaixa').asString  := FormatFloat('0000', FRENTE_CAIXA.Caixa);
  qrCFrete_Total.Active := True;
end;

function TFrmCartaFrete.GetCartaFreteExisteTemp(pCaixa: String; pID, pClifor,
  pNum: Integer): Boolean;
var
  qrBusca: TIBQuery;
begin
  qrBusca := TIBQuery.Create(Self);
  try
    qrBusca.Active := False;
    qrBusca.Database := dmCupomFiscal.DataBase;
    qrBusca.SQL.Clear;
    qrBusca.SQL.Add('Select * from TEMP_NFCE_FORMAPAG_CFRETE where CAIXA = :pCaixa '+
      'and ID= :pID and Clifor = :pClifor and Numero = :pNum');
    qrBusca.ParamByName('pCaixa').AsString   := pCaixa;
    qrBusca.ParamByName('pID').AsInteger     := pID;
    qrBusca.ParamByName('pClifor').AsInteger := pClifor;
    qrBusca.ParamByName('pNum').AsInteger    := pNum;
    qrBusca.Active := True;
    Result := not(qrBusca.IsEmpty);
  finally
    qrBusca.Free;
  end;
end;

procedure TfrmCartaFrete.Label1Click(Sender: TObject);
begin
  FrmPesquisaCliente := TFrmPesquisaCliente.Create(Self);
  try
    if (frmPesquisaCliente.ShowModal=mrOK) then DBEdit1.Text:= frmPesquisaCliente.qCliFor.FieldByName('id').AsString;
  finally
    frmPesquisaCliente.Free;
  end;
end;

end.
