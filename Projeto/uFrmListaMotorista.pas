unit uFrmListaMotorista;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Data.DB, Vcl.Grids,
  Vcl.DBGrids, Vcl.StdCtrls, Vcl.Buttons, Datasnap.DBClient;

type
  TfrmListaMotoristas = class(TForm)
    pnlBotoes: TPanel;
    grdDados: TDBGrid;
    cdsDados: TClientDataSet;
    dsDados: TDataSource;
    cdsDadosNOME: TStringField;
    btnOK: TBitBtn;
    btnCancelar: TBitBtn;
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmListaMotoristas: TfrmListaMotoristas;

implementation

{$R *.dfm}

procedure TfrmListaMotoristas.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if(key = vk_Escape)Then ModalResult := mrCancel;

  if(key = vk_Return)Then ModalResult := mrOk;
end;

end.
