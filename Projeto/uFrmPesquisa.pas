unit uFrmPesquisa;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, Grids, DBGrids, DB, IBCustomDataSet, IBQuery,
  ComCtrls, jpeg, ExtCtrls;

type
  TfrmPesquisa = class(TForm)
    DataSource1: TDataSource;
    IBQuery1: TIBQuery;
    DBGrid1: TDBGrid;
    StatusBar1: TStatusBar;
    Edit1: TEdit;
    CheckBox1: TCheckBox;
    Bevel3: TBevel;
    Label32: TLabel;
    procedure FormShow(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGrid1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Edit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGrid1Enter(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    constructor Create(AOwner: TComponent; SqlTextoA:String;SqlTextoB:String);
    reintroduce;
  end;

var
  frmPesquisa: TfrmPesquisa;
  aSQLTEXTO:String;
  bSQLTEXTO:String;

implementation

uses Math, uDMCupomFiscal, uFrmPrincipal;

{$R *.dfm}
constructor TfrmPesquisa.Create(AOwner: TComponent; SqlTextoA:String; SqlTextoB:String);
begin
  inherited Create(AOwner);
  aSQLTexto:=sqltextoA;
  bSQLTexto:=sqltextoB;

  if Trim(bSQLTEXTO)<>'' then
    CheckBox1.Enabled:=Enabled
  else
    CheckBox1.Enabled:=False;
end;

procedure TfrmPesquisa.FormShow(Sender: TObject);
begin
  Edit1.SetFocus;
  CheckBox1.Checked := FRENTE_CAIXA.IniciePor;
end;

procedure TfrmPesquisa.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 case Key of
    vk_Escape: ModalResult := mrCancel;
    vk_F12: FrmPesquisa.Edit1.SetFocus;
  end;
end;

procedure TfrmPesquisa.DBGrid1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     if (key = vk_Return) and not (DataSource1.DataSet.IsEmpty) then
     ModalResult := mrOK;

     //if key=VK_F1 then ShowMessage('Valor de Custo R$  '+FormatFloat('###,##0.00',IBQuery1.FieldByName('Custo_medio').AsCurrency));

     if key=VK_F1 then ShowMessage('CNPJ/CPJ  :'+IBQuery1.FieldByName('CNPJCPF').AsString+#13+#13+
                                   'Munic�pio :'+IBQuery1.FieldByName('Municipio').AsString+#13+#13+
                                   'Endere�o  :'+IBQuery1.FieldByName('Endereco').AsString);
end;

procedure TfrmPesquisa.Edit1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (key in [vk_Down, vk_Up]) and not (DataSource1.DataSet.IsEmpty) then
  DBGrid1.SetFocus;
end;

procedure TfrmPesquisa.DBGrid1Enter(Sender: TObject);
var  i,c:integer;
begin
     if Trim(Edit1.Text)<>'' then
     begin
          If CheckBox1.Checked then
          begin
          IBQuery1.Close;
          IBQuery1.SQL.Clear;
          IBQuery1.SQL.Add( aSqlTexto );
          IBQuery1.ParamByName('pTexto').AsString:=Edit1.Text;
          IBQuery1.Open;
          IBQuery1.First;
          end
          ELSE
          begin
          IBQuery1.Close;
          IBQuery1.SQL.Clear;
          IBQuery1.SQL.Add( bSqlTexto );
          IBQuery1.ParamByName('pTexto').AsString:='%'+Trim(Edit1.Text)+'%';
          IBQuery1.Open;
          IBQuery1.First;
          end;

          C:=DBGrid1.Columns.Count;
          For i:=1 to C do
          begin
          If DBGrid1.Columns[i-1].FieldName='VENDA' then TNumericField(IBQuery1.FieldByName('VENDA') ).DisplayFormat := '###,##0.00';
          If DBGrid1.Columns[i-1].FieldName='EST_ATUAL' then TNumericField(IBQuery1.FieldByName('EST_ATUAL') ).DisplayFormat := '###,##0.000';
          end;
     end
     ELSE
     Edit1.SetFocus;
end;

procedure TfrmPesquisa.DBGrid1DblClick(Sender: TObject);
begin
     if not (DataSource1.DataSet.IsEmpty) then
     ModalResult := mrOK;
end;

end.



