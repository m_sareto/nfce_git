unit uCtrlEnvio;

interface

Uses
  uDMCupomFiscal, uDMComponentes;

type
  TCtrlEnvio = Class
  private
    Procedure SetDadosNFCeACBr(AID: Integer);
  public
    ID: Integer;
    Procedure Enviar(AID: Integer);
  end;

implementation

{ TCtrlEnvio }

procedure TCtrlEnvio.Enviar(AID: Integer);
begin
  //Seta os dados da NFCe passada por parametro
  SetDadosNFCeACBr(AID);
end;

procedure TCtrlEnvio.SetDadosNFCeACBr(AID: Integer);
begin
  //carregar os dados da NFCe passada por parametro...
end;

end.
