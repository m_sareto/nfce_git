unit uFrmPesquisaCliente;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, Grids, DBGrids, DB, IBCustomDataSet, IBQuery,
  ComCtrls, jpeg, ExtCtrls;

type
  TfrmPesquisaCliente = class(TForm)
    DataSource1: TDataSource;
    qCliFor: TIBQuery;
    DBGrid1: TDBGrid;
    GroupBox1: TGroupBox;
    Edit1: TEdit;
    StatusBar1: TStatusBar;
    CheckBox1: TCheckBox;
    qCliForNOME: TIBStringField;
    qCliForFONE: TIBStringField;
    qCliForID: TIntegerField;
    qCliForCNPJCPF: TIBStringField;
    procedure FormShow(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGrid1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Edit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGrid1Enter(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    constructor Create(AOwner: TComponent);
    reintroduce;
  end;

var
  frmPesquisaCliente: TfrmPesquisaCliente;

implementation

uses Math, uDMCupomFiscal, uFrmPrincipal;

{$R *.dfm}
constructor TfrmPesquisaCliente.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
end;

procedure TfrmPesquisaCliente.FormShow(Sender: TObject);
begin
  Edit1.SetFocus;
  CheckBox1.Checked := FRENTE_CAIXA.IniciePor;
end;

procedure TfrmPesquisaCliente.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case Key of
    vk_Escape: ModalResult := mrCancel;
    vk_F12: FrmPesquisaCliente.Edit1.SetFocus;
  end;
end;

procedure TfrmPesquisaCliente.DBGrid1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (key = vk_Return) and not (DataSource1.DataSet.IsEmpty) then
    ModalResult := mrOK;
end;

procedure TfrmPesquisaCliente.Edit1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (key in [vk_Down, vk_Up]) and not (DataSource1.DataSet.IsEmpty) then
  DBGrid1.SetFocus;
end;

procedure TfrmPesquisaCliente.DBGrid1Enter(Sender: TObject);
begin
  if Trim(Edit1.Text)<>'' then
  begin
    qCliFor.Active:=False;
    qCliFor.SQL.Clear;
    qCliFor.SQL.Add('select ID, Nome, CNPJCPF, Fone'+
                  ' From CliFor');
    if CheckBox1.Checked then
    begin
      qCliFor.SQL.Add('Where Nome>=:pDes and (CF=''C'') and (Ativo=''S'')'+
                  ' Order By Nome');
      qCliFor.ParamByName('pDes').AsString:=Trim(Edit1.Text);
    end
    else
    begin
      qCliFor.SQL.Add('Where Nome like ''%'+Trim(Edit1.Text)+'%'' and (CF=''C'') and (Ativo=''S'')'+
                  ' Order By Nome');
    end;
    qCliFor.Active:=True;
    qCliFor.First;
  end;
end;

end.



