unit ufrmImpTefComprovantes;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, RLReport, Printers, RLPrinters;

type
  TfrmImpTefComprovantes = class(TForm)
    rlVenda: TRLReport;
    Memo1: TRLMemo;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmImpTefComprovantes: TfrmImpTefComprovantes;

implementation

Uses
  uDMComponentes;

{$R *.dfm}

procedure TfrmImpTefComprovantes.FormCreate(Sender: TObject);
begin
  RLPrinter.PrinterName := dmComponentes.ACBrNFeDANFe.Impressora;
end;

end.
