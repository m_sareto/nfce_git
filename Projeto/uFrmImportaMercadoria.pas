unit uFrmImportaMercadoria;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Mask, DBCtrls, Buttons, ExtCtrls, DB, ComCtrls,
  IBCustomDataSet, IBQuery, Grids, DBGrids;

type
  TfrmImportaMercadoria = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    btnEnviar: TBitBtn;
    DBGrid1: TDBGrid;
    dsMercadoriasImportar: TDataSource;
    btnSair: TBitBtn;
    statusBar: TStatusBar;
    Panel4: TPanel;
    procedure btnEnviarClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btnSairClick(Sender: TObject);
  private
    { Private declarations }
    procedure conectarBaseDadosRemota();
    procedure desconectarBaseDadosRemota();
    procedure atualizarPedidoLocalEnviado(id_pedido: Integer);
    procedure getMercadoriasImportar();
    function mercadoriaCadastrada(mercadoria:String):Boolean;
  public
    { Public declarations }
  end;

var
  frmImportaMercadoria: TfrmImportaMercadoria;

implementation

uses uFrmPrincipal, uDMCupomFiscal, uFuncoes;


{$R *.dfm}

{ TfrmTransferenciaPedidos }


procedure TfrmImportaMercadoria.conectarBaseDadosRemota;
begin
  try
    If Trim(FRENTE_CAIXA.DatabaseRemoto) <> ''  then
    begin
      dmCupomFiscal.DatabaseRemota.Connected := False;
      dmCupomFiscal.TransactionRemota.Active := False;
      dmCupomFiscal.DatabaseRemota.DatabaseName := Trim(FRENTE_CAIXA.DatabaseRemoto);
      dmCupomFiscal.DatabaseRemota.Connected := True;
      dmCupomFiscal.TransactionRemota.Active := True;
    end
    else
      raise Exception.Create('Caminho da base de dados do servidor remoto n�o informado');
  except
    on e:Exception do
      raise Exception.Create('Erro ao conectar-se com a base de dados do servidor. '+e.Message);
  end;
end;


procedure TfrmImportaMercadoria.btnEnviarClick(Sender: TObject);
var ID_Pedido_Remoto, ID_Pedido_Local, contInserido, contEditado, contTotal:Integer;
begin
  if msgPergunta('Confirmar a importa��o das mercadorias para base Local ?','') then
  begin
    try
      btnEnviar.Enabled:=False;
      btnSair.Enabled:=False;
      frmImportaMercadoria.Enabled:=False;
      frmPrincipal.abreFormAguarde('Aguarde...');
      getMercadoriasImportar();
      try
        contEditado:=0;
        contInserido:=0;
        contTotal:=dmCupomFiscal.qurEstMercadorias_Remota.RecordCount;
        if not(dmCupomFiscal.qurEstMercadorias_Remota.Eof) then
        begin
          dmCupomFiscal.qurEstMercadorias_Remota.First;
          while not(dmCupomFiscal.qurEstMercadorias_Remota.Eof) do
          begin
            if not  (mercadoriaCadastrada(dmCupomFiscal.qurEstMercadorias_RemotaID.AsString)) then
            begin
              Inc(contInserido);
              //Insert
              dmCupomFiscal.dbEstMercadorias.Open;
              dmCupomFiscal.dbEstMercadorias.Insert;
              dmCupomFiscal.dbEstMercadoriasID.Value := dmCupomFiscal.qurEstMercadorias_RemotaID.Value;
              dmCupomFiscal.dbEstMercadoriasDESCRICAO.Value := dmCupomFiscal.qurEstMercadorias_RemotaDESCRICAO.Value;
              dmCupomFiscal.dbEstMercadoriasSUCINTO.Value := dmCupomFiscal.qurEstMercadorias_RemotaSUCINTO.Value;
              dmCupomFiscal.dbEstMercadoriasUNIDADE.Value := dmCupomFiscal.qurEstMercadorias_RemotaUNIDADE.Value;
              dmCupomFiscal.dbEstMercadoriasSUBGRUPO.Value := dmCupomFiscal.qurEstMercadorias_RemotaSUBGRUPO.Value;
              dmCupomFiscal.dbEstMercadoriasMARCA.Value := dmCupomFiscal.qurEstMercadorias_RemotaMARCA.Value;
              dmCupomFiscal.dbEstMercadoriasMODELO.Value := dmCupomFiscal.qurEstMercadorias_RemotaMODELO.Value;
              dmCupomFiscal.dbEstMercadoriasATIVO.Value := dmCupomFiscal.qurEstMercadorias_RemotaATIVO.Value;
              dmCupomFiscal.dbEstMercadoriasCOMPOSICAO.Value := dmCupomFiscal.qurEstMercadorias_RemotaCOMPOSICAO.Value;
              dmCupomFiscal.dbEstMercadoriasLOCAL.Value := dmCupomFiscal.qurEstMercadorias_RemotaLOCAL.Value;
              dmCupomFiscal.dbEstMercadoriasRECEITA.Value := dmCupomFiscal.qurEstMercadorias_RemotaRECEITA.Value;
              dmCupomFiscal.dbEstMercadoriasSERVICO.Value := dmCupomFiscal.qurEstMercadorias_RemotaSERVICO.Value;
              dmCupomFiscal.dbEstMercadoriasAPLICACAO.Value := dmCupomFiscal.qurEstMercadorias_RemotaAPLICACAO.Value;
              dmCupomFiscal.dbEstMercadoriasCODIGO_ORIGINAL.Value := dmCupomFiscal.qurEstMercadorias_RemotaCODIGO_ORIGINAL.Value;
              dmCupomFiscal.dbEstMercadoriasDESCONTO.Value := dmCupomFiscal.qurEstMercadorias_RemotaDESCONTO.Value;
              dmCupomFiscal.dbEstMercadoriasPESO_LIQ.Value := dmCupomFiscal.qurEstMercadorias_RemotaPESO_LIQ.Value;
              dmCupomFiscal.dbEstMercadoriasPESO_BRU.Value := dmCupomFiscal.qurEstMercadorias_RemotaPESO_BRU.Value;
              dmCupomFiscal.dbEstMercadoriasF_CONVERSAO.Value := dmCupomFiscal.qurEstMercadorias_RemotaF_CONVERSAO.Value;
              dmCupomFiscal.dbEstMercadoriasCUSTO_INICIAL.Value := dmCupomFiscal.qurEstMercadorias_RemotaCUSTO_INICIAL.Value;
              dmCupomFiscal.dbEstMercadoriasCUSTO_ULTIMO.Value := dmCupomFiscal.qurEstMercadorias_RemotaCUSTO_ULTIMO.Value;
              dmCupomFiscal.dbEstMercadoriasCUSTO_MEDIO.Value := dmCupomFiscal.qurEstMercadorias_RemotaCUSTO_MEDIO.Value;
              dmCupomFiscal.dbEstMercadoriasVENDA.Value := dmCupomFiscal.qurEstMercadorias_RemotaVENDA.Value;
              dmCupomFiscal.dbEstMercadoriasMARGEM.Value := dmCupomFiscal.qurEstMercadorias_RemotaMARGEM.Value;
              dmCupomFiscal.dbEstMercadoriasCOFINS.Value := dmCupomFiscal.qurEstMercadorias_RemotaCOFINS.Value;
              dmCupomFiscal.dbEstMercadoriasIPI.Value := dmCupomFiscal.qurEstMercadorias_RemotaIPI.Value;
              dmCupomFiscal.dbEstMercadoriasISSQN.Value := dmCupomFiscal.qurEstMercadorias_RemotaISSQN.Value;
              dmCupomFiscal.dbEstMercadoriasPIS.Value := dmCupomFiscal.qurEstMercadorias_RemotaPIS.Value;
              dmCupomFiscal.dbEstMercadoriasCSLL.Value := dmCupomFiscal.qurEstMercadorias_RemotaCSLL.Value;
              dmCupomFiscal.dbEstMercadoriasPREVIDENCIA.Value := dmCupomFiscal.qurEstMercadorias_RemotaPREVIDENCIA.Value;
              dmCupomFiscal.dbEstMercadoriasII.Value := dmCupomFiscal.qurEstMercadorias_RemotaII.Value;
              dmCupomFiscal.dbEstMercadoriasIRRF.Value := dmCupomFiscal.qurEstMercadorias_RemotaIRRF.Value;
              dmCupomFiscal.dbEstMercadoriasICM_ECF.Value := dmCupomFiscal.qurEstMercadorias_RemotaICM_ECF.Value;
              dmCupomFiscal.dbEstMercadoriasECF_CFOP.Value := dmCupomFiscal.qurEstMercadorias_RemotaECF_CFOP.Value;
              dmCupomFiscal.dbEstMercadoriasECF_CST_ICM.Value := dmCupomFiscal.qurEstMercadorias_RemotaECF_CST_ICM.Value;
              dmCupomFiscal.dbEstMercadoriasNCM.Value := dmCupomFiscal.qurEstMercadorias_RemotaNCM.Value;
              dmCupomFiscal.dbEstMercadoriasEXTIPI.Value := dmCupomFiscal.qurEstMercadorias_RemotaEXTIPI.Value;
              dmCupomFiscal.dbEstMercadoriasDT_VENDA.Value := dmCupomFiscal.qurEstMercadorias_RemotaDT_VENDA.Value;
              dmCupomFiscal.dbEstMercadoriasDT_COMPRA.Value := dmCupomFiscal.qurEstMercadorias_RemotaDT_COMPRA.Value;
              dmCupomFiscal.dbEstMercadoriasCCAPITAL.Value := dmCupomFiscal.qurEstMercadorias_RemotaCCAPITAL.Value;
              dmCupomFiscal.dbEstMercadoriasFABRICANTE.Value := dmCupomFiscal.qurEstMercadorias_RemotaFABRICANTE.Value;
              dmCupomFiscal.dbEstMercadoriasGTIN.Value := dmCupomFiscal.qurEstMercadorias_RemotaGTIN.Value;
              dmCupomFiscal.dbEstMercadoriasCONTA_CONTABIL.Value := dmCupomFiscal.qurEstMercadorias_RemotaCONTA_CONTABIL.Value;
              dmCupomFiscal.dbEstMercadoriasRA_TOXICIDADE.Value := dmCupomFiscal.qurEstMercadorias_RemotaRA_TOXICIDADE.Value;
              dmCupomFiscal.dbEstMercadoriasRA_CONCENTRACAO.Value := dmCupomFiscal.qurEstMercadorias_RemotaRA_CONCENTRACAO.Value;
              dmCupomFiscal.dbEstMercadoriasRA_FORMULACAO.Value := dmCupomFiscal.qurEstMercadorias_RemotaRA_FORMULACAO.Value;
              dmCupomFiscal.dbEstMercadoriasRA_INGREDIENTE_ATIVO.Value := dmCupomFiscal.qurEstMercadorias_RemotaRA_INGREDIENTE_ATIVO.Value;
              dmCupomFiscal.dbEstMercadoriasRA_CLASSE.Value := dmCupomFiscal.qurEstMercadorias_RemotaRA_CLASSE.Value;
              dmCupomFiscal.dbEstMercadoriasRA_UNIDADE.Value := dmCupomFiscal.qurEstMercadorias_RemotaRA_UNIDADE.Value;
              dmCupomFiscal.dbEstMercadoriasORIGEM_MER.Value := dmCupomFiscal.qurEstMercadorias_RemotaORIGEM_MER.Value;
              dmCupomFiscal.dbEstMercadoriasCST_IPI.Value := dmCupomFiscal.qurEstMercadorias_RemotaCST_IPI.Value;
              dmCupomFiscal.dbEstMercadoriasCST_PIS.Value := dmCupomFiscal.qurEstMercadorias_RemotaCST_PIS.Value;
              dmCupomFiscal.dbEstMercadoriasCST_COFINS.Value := dmCupomFiscal.qurEstMercadorias_RemotaCST_COFINS.Value;
              dmCupomFiscal.dbEstMercadoriasCLS.Value := dmCupomFiscal.qurEstMercadorias_RemotaCLS.Value;
              dmCupomFiscal.dbEstMercadoriasCST_PIS_ENTRADA.Value := dmCupomFiscal.qurEstMercadorias_RemotaCST_PIS_ENTRADA.Value;
              dmCupomFiscal.dbEstMercadoriasCST_COFINS_ENTRADA.Value := dmCupomFiscal.qurEstMercadorias_RemotaCST_COFINS_ENTRADA.Value;
              dmCupomFiscal.dbEstMercadoriasPIS_ENTRADA.Value := dmCupomFiscal.qurEstMercadorias_RemotaPIS_ENTRADA.Value;
              dmCupomFiscal.dbEstMercadoriasCOFINS_ENTRADA.Value := dmCupomFiscal.qurEstMercadorias_RemotaCOFINS_ENTRADA.Value;
              dmCupomFiscal.dbEstMercadoriasAMPARO_CONTRIBUICOES.Value := dmCupomFiscal.qurEstMercadorias_RemotaAMPARO_CONTRIBUICOES.Value;
              dmCupomFiscal.dbEstMercadoriasSM_DTE_PROMOCAO.Value := dmCupomFiscal.qurEstMercadorias_RemotaSM_DTE_PROMOCAO.Value;
              dmCupomFiscal.dbEstMercadoriasSM_DTS_PROMOCAO.Value := dmCupomFiscal.qurEstMercadorias_RemotaSM_DTS_PROMOCAO.Value;
              dmCupomFiscal.dbEstMercadoriasSM_VENDA_PROMOCAO.Value := dmCupomFiscal.qurEstMercadorias_RemotaSM_VENDA_PROMOCAO.Value;
              dmCupomFiscal.dbEstMercadoriasSM_BLC_UP.Value := dmCupomFiscal.qurEstMercadorias_RemotaSM_BLC_UP.Value;
              dmCupomFiscal.dbEstMercadoriasSM_BLC_DIAS.Value := dmCupomFiscal.qurEstMercadorias_RemotaSM_BLC_DIAS.Value;
              dmCupomFiscal.dbEstMercadoriasSM_BLC_ATIVO.Value := dmCupomFiscal.qurEstMercadorias_RemotaSM_BLC_ATIVO.Value;
              dmCupomFiscal.dbEstMercadoriasCOD_ANP.Value := dmCupomFiscal.qurEstMercadorias_RemotaCOD_ANP.Value;
              dmCupomFiscal.dbEstMercadoriasTIPO_ITEM.Value := dmCupomFiscal.qurEstMercadorias_RemotaTIPO_ITEM.Value;
              dmCupomFiscal.dbEstMercadoriasSUBTIPO_ITEM.Value := dmCupomFiscal.qurEstMercadorias_RemotaSUBTIPO_ITEM.Value;
              dmCupomFiscal.dbEstMercadoriasID_CODIGO.Value := dmCupomFiscal.qurEstMercadorias_RemotaID_CODIGO.Value;
              dmCupomFiscal.dbEstMercadoriasAMPARO_PREVIDENCIA.Value := dmCupomFiscal.qurEstMercadorias_RemotaAMPARO_PREVIDENCIA.Value;
              dmCupomFiscal.dbEstMercadoriasNFCI.Value := dmCupomFiscal.qurEstMercadorias_RemotaNFCI.Value;
              dmCupomFiscal.dbEstMercadoriasIPI_ENTRADA.Value := dmCupomFiscal.qurEstMercadorias_RemotaIPI_ENTRADA.Value;
              dmCupomFiscal.dbEstMercadoriasCST_IPI_ENTRADA.Value := dmCupomFiscal.qurEstMercadorias_RemotaCST_IPI_ENTRADA.Value;
              dmCupomFiscal.dbEstMercadoriasPEDE_LOTE.Value := dmCupomFiscal.qurEstMercadorias_RemotaPEDE_LOTE.Value;
              dmCupomFiscal.dbEstMercadorias.Post;
              dmCupomFiscal.dbEstMercadorias.Close;
            end
            else
            begin
              //Update
              Inc(contEditado);
              dmCupomFiscal.dbEstMercadorias.ParamByName('pID').Value := dmCupomFiscal.qurEstMercadorias_RemotaID.Value;
              dmCupomFiscal.dbEstMercadorias.Open;
              dmCupomFiscal.dbEstMercadorias.Edit;
              dmCupomFiscal.dbEstMercadoriasDESCRICAO.Value := dmCupomFiscal.qurEstMercadorias_RemotaDESCRICAO.Value;
              dmCupomFiscal.dbEstMercadoriasSUCINTO.Value := dmCupomFiscal.qurEstMercadorias_RemotaSUCINTO.Value;
              dmCupomFiscal.dbEstMercadoriasUNIDADE.Value := dmCupomFiscal.qurEstMercadorias_RemotaUNIDADE.Value;
              dmCupomFiscal.dbEstMercadoriasSUBGRUPO.Value := dmCupomFiscal.qurEstMercadorias_RemotaSUBGRUPO.Value;
              dmCupomFiscal.dbEstMercadoriasMARCA.Value := dmCupomFiscal.qurEstMercadorias_RemotaMARCA.Value;
              dmCupomFiscal.dbEstMercadoriasMODELO.Value := dmCupomFiscal.qurEstMercadorias_RemotaMODELO.Value;
              dmCupomFiscal.dbEstMercadoriasATIVO.Value := dmCupomFiscal.qurEstMercadorias_RemotaATIVO.Value;
              dmCupomFiscal.dbEstMercadoriasCOMPOSICAO.Value := dmCupomFiscal.qurEstMercadorias_RemotaCOMPOSICAO.Value;
              dmCupomFiscal.dbEstMercadoriasLOCAL.Value := dmCupomFiscal.qurEstMercadorias_RemotaLOCAL.Value;
              dmCupomFiscal.dbEstMercadoriasRECEITA.Value := dmCupomFiscal.qurEstMercadorias_RemotaRECEITA.Value;
              dmCupomFiscal.dbEstMercadoriasSERVICO.Value := dmCupomFiscal.qurEstMercadorias_RemotaSERVICO.Value;
              dmCupomFiscal.dbEstMercadoriasAPLICACAO.Value := dmCupomFiscal.qurEstMercadorias_RemotaAPLICACAO.Value;
              dmCupomFiscal.dbEstMercadoriasCODIGO_ORIGINAL.Value := dmCupomFiscal.qurEstMercadorias_RemotaCODIGO_ORIGINAL.Value;
              dmCupomFiscal.dbEstMercadoriasDESCONTO.Value := dmCupomFiscal.qurEstMercadorias_RemotaDESCONTO.Value;
              dmCupomFiscal.dbEstMercadoriasPESO_LIQ.Value := dmCupomFiscal.qurEstMercadorias_RemotaPESO_LIQ.Value;
              dmCupomFiscal.dbEstMercadoriasPESO_BRU.Value := dmCupomFiscal.qurEstMercadorias_RemotaPESO_BRU.Value;
              dmCupomFiscal.dbEstMercadoriasF_CONVERSAO.Value := dmCupomFiscal.qurEstMercadorias_RemotaF_CONVERSAO.Value;
              dmCupomFiscal.dbEstMercadoriasCUSTO_INICIAL.Value := dmCupomFiscal.qurEstMercadorias_RemotaCUSTO_INICIAL.Value;
              dmCupomFiscal.dbEstMercadoriasCUSTO_ULTIMO.Value := dmCupomFiscal.qurEstMercadorias_RemotaCUSTO_ULTIMO.Value;
              dmCupomFiscal.dbEstMercadoriasCUSTO_MEDIO.Value := dmCupomFiscal.qurEstMercadorias_RemotaCUSTO_MEDIO.Value;
              dmCupomFiscal.dbEstMercadoriasVENDA.Value := dmCupomFiscal.qurEstMercadorias_RemotaVENDA.Value;
              dmCupomFiscal.dbEstMercadoriasMARGEM.Value := dmCupomFiscal.qurEstMercadorias_RemotaMARGEM.Value;
              dmCupomFiscal.dbEstMercadoriasCOFINS.Value := dmCupomFiscal.qurEstMercadorias_RemotaCOFINS.Value;
              dmCupomFiscal.dbEstMercadoriasIPI.Value := dmCupomFiscal.qurEstMercadorias_RemotaIPI.Value;
              dmCupomFiscal.dbEstMercadoriasISSQN.Value := dmCupomFiscal.qurEstMercadorias_RemotaISSQN.Value;
              dmCupomFiscal.dbEstMercadoriasPIS.Value := dmCupomFiscal.qurEstMercadorias_RemotaPIS.Value;
              dmCupomFiscal.dbEstMercadoriasCSLL.Value := dmCupomFiscal.qurEstMercadorias_RemotaCSLL.Value;
              dmCupomFiscal.dbEstMercadoriasPREVIDENCIA.Value := dmCupomFiscal.qurEstMercadorias_RemotaPREVIDENCIA.Value;
              dmCupomFiscal.dbEstMercadoriasII.Value := dmCupomFiscal.qurEstMercadorias_RemotaII.Value;
              dmCupomFiscal.dbEstMercadoriasIRRF.Value := dmCupomFiscal.qurEstMercadorias_RemotaIRRF.Value;
              dmCupomFiscal.dbEstMercadoriasICM_ECF.Value := dmCupomFiscal.qurEstMercadorias_RemotaICM_ECF.Value;
              dmCupomFiscal.dbEstMercadoriasECF_CFOP.Value := dmCupomFiscal.qurEstMercadorias_RemotaECF_CFOP.Value;
              dmCupomFiscal.dbEstMercadoriasECF_CST_ICM.Value := dmCupomFiscal.qurEstMercadorias_RemotaECF_CST_ICM.Value;
              dmCupomFiscal.dbEstMercadoriasNCM.Value := dmCupomFiscal.qurEstMercadorias_RemotaNCM.Value;
              dmCupomFiscal.dbEstMercadoriasEXTIPI.Value := dmCupomFiscal.qurEstMercadorias_RemotaEXTIPI.Value;
              dmCupomFiscal.dbEstMercadoriasDT_VENDA.Value := dmCupomFiscal.qurEstMercadorias_RemotaDT_VENDA.Value;
              dmCupomFiscal.dbEstMercadoriasDT_COMPRA.Value := dmCupomFiscal.qurEstMercadorias_RemotaDT_COMPRA.Value;
              dmCupomFiscal.dbEstMercadoriasCCAPITAL.Value := dmCupomFiscal.qurEstMercadorias_RemotaCCAPITAL.Value;
              dmCupomFiscal.dbEstMercadoriasFABRICANTE.Value := dmCupomFiscal.qurEstMercadorias_RemotaFABRICANTE.Value;
              dmCupomFiscal.dbEstMercadoriasGTIN.Value := dmCupomFiscal.qurEstMercadorias_RemotaGTIN.Value;
              dmCupomFiscal.dbEstMercadoriasCONTA_CONTABIL.Value := dmCupomFiscal.qurEstMercadorias_RemotaCONTA_CONTABIL.Value;
              dmCupomFiscal.dbEstMercadoriasRA_TOXICIDADE.Value := dmCupomFiscal.qurEstMercadorias_RemotaRA_TOXICIDADE.Value;
              dmCupomFiscal.dbEstMercadoriasRA_CONCENTRACAO.Value := dmCupomFiscal.qurEstMercadorias_RemotaRA_CONCENTRACAO.Value;
              dmCupomFiscal.dbEstMercadoriasRA_FORMULACAO.Value := dmCupomFiscal.qurEstMercadorias_RemotaRA_FORMULACAO.Value;
              dmCupomFiscal.dbEstMercadoriasRA_INGREDIENTE_ATIVO.Value := dmCupomFiscal.qurEstMercadorias_RemotaRA_INGREDIENTE_ATIVO.Value;
              dmCupomFiscal.dbEstMercadoriasRA_CLASSE.Value := dmCupomFiscal.qurEstMercadorias_RemotaRA_CLASSE.Value;
              dmCupomFiscal.dbEstMercadoriasRA_UNIDADE.Value := dmCupomFiscal.qurEstMercadorias_RemotaRA_UNIDADE.Value;
              dmCupomFiscal.dbEstMercadoriasORIGEM_MER.Value := dmCupomFiscal.qurEstMercadorias_RemotaORIGEM_MER.Value;
              dmCupomFiscal.dbEstMercadoriasCST_IPI.Value := dmCupomFiscal.qurEstMercadorias_RemotaCST_IPI.Value;
              dmCupomFiscal.dbEstMercadoriasCST_PIS.Value := dmCupomFiscal.qurEstMercadorias_RemotaCST_PIS.Value;
              dmCupomFiscal.dbEstMercadoriasCST_COFINS.Value := dmCupomFiscal.qurEstMercadorias_RemotaCST_COFINS.Value;
              dmCupomFiscal.dbEstMercadoriasCLS.Value := dmCupomFiscal.qurEstMercadorias_RemotaCLS.Value;
              dmCupomFiscal.dbEstMercadoriasCST_PIS_ENTRADA.Value := dmCupomFiscal.qurEstMercadorias_RemotaCST_PIS_ENTRADA.Value;
              dmCupomFiscal.dbEstMercadoriasCST_COFINS_ENTRADA.Value := dmCupomFiscal.qurEstMercadorias_RemotaCST_COFINS_ENTRADA.Value;
              dmCupomFiscal.dbEstMercadoriasPIS_ENTRADA.Value := dmCupomFiscal.qurEstMercadorias_RemotaPIS_ENTRADA.Value;
              dmCupomFiscal.dbEstMercadoriasCOFINS_ENTRADA.Value := dmCupomFiscal.qurEstMercadorias_RemotaCOFINS_ENTRADA.Value;
              dmCupomFiscal.dbEstMercadoriasAMPARO_CONTRIBUICOES.Value := dmCupomFiscal.qurEstMercadorias_RemotaAMPARO_CONTRIBUICOES.Value;
              dmCupomFiscal.dbEstMercadoriasSM_DTE_PROMOCAO.Value := dmCupomFiscal.qurEstMercadorias_RemotaSM_DTE_PROMOCAO.Value;
              dmCupomFiscal.dbEstMercadoriasSM_DTS_PROMOCAO.Value := dmCupomFiscal.qurEstMercadorias_RemotaSM_DTS_PROMOCAO.Value;
              dmCupomFiscal.dbEstMercadoriasSM_VENDA_PROMOCAO.Value := dmCupomFiscal.qurEstMercadorias_RemotaSM_VENDA_PROMOCAO.Value;
              dmCupomFiscal.dbEstMercadoriasSM_BLC_UP.Value := dmCupomFiscal.qurEstMercadorias_RemotaSM_BLC_UP.Value;
              dmCupomFiscal.dbEstMercadoriasSM_BLC_DIAS.Value := dmCupomFiscal.qurEstMercadorias_RemotaSM_BLC_DIAS.Value;
              dmCupomFiscal.dbEstMercadoriasSM_BLC_ATIVO.Value := dmCupomFiscal.qurEstMercadorias_RemotaSM_BLC_ATIVO.Value;
              dmCupomFiscal.dbEstMercadoriasCOD_ANP.Value := dmCupomFiscal.qurEstMercadorias_RemotaCOD_ANP.Value;
              dmCupomFiscal.dbEstMercadoriasTIPO_ITEM.Value := dmCupomFiscal.qurEstMercadorias_RemotaTIPO_ITEM.Value;
              dmCupomFiscal.dbEstMercadoriasSUBTIPO_ITEM.Value := dmCupomFiscal.qurEstMercadorias_RemotaSUBTIPO_ITEM.Value;
              dmCupomFiscal.dbEstMercadoriasID_CODIGO.Value := dmCupomFiscal.qurEstMercadorias_RemotaID_CODIGO.Value;
              dmCupomFiscal.dbEstMercadoriasAMPARO_PREVIDENCIA.Value := dmCupomFiscal.qurEstMercadorias_RemotaAMPARO_PREVIDENCIA.Value;
              dmCupomFiscal.dbEstMercadoriasNFCI.Value := dmCupomFiscal.qurEstMercadorias_RemotaNFCI.Value;
              dmCupomFiscal.dbEstMercadoriasIPI_ENTRADA.Value := dmCupomFiscal.qurEstMercadorias_RemotaIPI_ENTRADA.Value;
              dmCupomFiscal.dbEstMercadoriasCST_IPI_ENTRADA.Value := dmCupomFiscal.qurEstMercadorias_RemotaCST_IPI_ENTRADA.Value;
              dmCupomFiscal.dbEstMercadoriasPEDE_LOTE.Value := dmCupomFiscal.qurEstMercadorias_RemotaPEDE_LOTE.Value;
              dmCupomFiscal.dbEstMercadorias.Post;
              dmCupomFiscal.dbEstMercadorias.Close;
            end;
            Application.ProcessMessages;
            statusBar.Panels[0].Text:='Importa��o de mercadoria INSERIDA/EDITADA '+IntToStr(contInserido)+'/'+IntToStr(contEditado)+' de '+IntToStr(contTotal);
            statusBar.Refresh;
            dmCupomFiscal.Transaction.CommitRetaining; //Confirma transacao no final
            dmCupomFiscal.qurEstMercadorias_Remota.Next;
          end;
          statusBar.Panels[0].Text:='';
          dmCupomFiscal.dbEstMercadorias.Transaction.Commit;
          msgAviso('Lote com total de '+IntToStr(contTotal)+' mercadorias:'+#13#10+
                   'Mercadorias inseridas :'+IntToStr(contInserido)+#13#10+
                   'Mercadorias editadas :'+IntToStr(contEditado), '');
          desconectarBaseDadosRemota();
        end
        else
          msgInformacao('N�o ha mercadorias pendentes para importar para base Local','Aten��o');
      except
        on e:Exception do
        begin
          dmCupomFiscal.dbEstMercadorias.Transaction.Rollback;
          logErros(Sender, caminhoLog, 'Erro ao importar mercadorias para base Local. '+e.Message, 'Erro ao importar mercadorias para base Local', 'S', E);
        end;
      end;
    finally
      frmPrincipal.fechaFormAguarde;
      frmImportaMercadoria.Enabled:=True;
      btnEnviar.Enabled:=True;
      btnSair.Enabled:=True;
    end;
  end;
end;

procedure TfrmImportaMercadoria.atualizarPedidoLocalEnviado(id_pedido: Integer);
begin
  dmCupomFiscal.IBSQL1.Close;
  dmCupomFiscal.IBSQL1.SQL.Clear;
  dmCupomFiscal.IBSQL1.SQL.Add('update est_pedidos set enviado=''S'' where id=:pID_Pedido');
  dmCupomFiscal.IBSQL1.ParamByName('pID_Pedido').AsInteger:=id_pedido;
  dmCupomFiscal.IBSQL1.ExecQuery;
  dmCupomFiscal.Transaction.CommitRetaining;
end;

procedure TfrmImportaMercadoria.FormKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if Key=vk_escape then btnSair.Click;
end;

procedure TfrmImportaMercadoria.btnSairClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmImportaMercadoria.desconectarBaseDadosRemota;
begin
  dmCupomFiscal.DatabaseRemota.Connected := False;
  dmCupomFiscal.TransactionRemota.Active := False;
end;

procedure TfrmImportaMercadoria.getMercadoriasImportar;
begin
  conectarBaseDadosRemota();
  //----- Busca mercadorias base remota
  dmCupomFiscal.qurEstMercadorias_Remota.Active:=False;
  dmCupomFiscal.qurEstMercadorias_Remota.SQL.Clear;
  dmCupomFiscal.qurEstMercadorias_Remota.SQL.Add('select * from est_mercadorias');
  dmCupomFiscal.qurEstMercadorias_Remota.Active:=True;
  dmCupomFiscal.qurEstMercadorias_Remota.FetchAll;
  dmCupomFiscal.qurEstMercadorias_Remota.First;
  Panel4.Caption:=IntToStr(dmCupomFiscal.qurEstMercadorias_Remota.RecordCount)+' Mercadoria(s) a ser importada(s)';
end;

function TfrmImportaMercadoria.mercadoriaCadastrada(
  mercadoria: String): Boolean;
begin
  dmCupomFiscal.dbQuery2.Active:=False;
  dmCupomFiscal.dbQuery2.SQL.Clear;
  dmCupomFiscal.dbQuery2.SQL.Add('select id from est_mercadorias where id=:pID');
  dmCupomFiscal.dbQuery2.ParamByName('pID').AsString:=mercadoria;
  dmCupomFiscal.dbQuery2.Active:=True;
  dmCupomFiscal.dbQuery2.First;
  if (dmCupomFiscal.dbQuery2.Eof) then
    Result := False
  else
    Result := True;
end;

end.
