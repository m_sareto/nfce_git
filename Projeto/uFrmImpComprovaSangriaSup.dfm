object frmImpCompSangSup: TfrmImpCompSangSup
  Left = 183
  Top = 183
  Caption = 'frmImpCompSangSup'
  ClientHeight = 561
  ClientWidth = 775
  Color = clWindow
  Ctl3D = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  WindowState = wsMaximized
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object rlVenda: TRLReport
    Left = 64
    Top = 22
    Width = 280
    Height = 1512
    Margins.LeftMargin = 0.610000000000000000
    Margins.TopMargin = 2.000000000000000000
    Margins.RightMargin = 0.610000000000000000
    Margins.BottomMargin = 0.000000000000000000
    AllowedBands = [btHeader, btDetail, btSummary, btFooter]
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -9
    Font.Name = 'Arial'
    Font.Style = []
    PageSetup.PaperSize = fpCustom
    PageSetup.PaperWidth = 74.000000000000000000
    PageSetup.PaperHeight = 400.000000000000000000
    PrintDialog = False
    ShowProgress = False
    object rlbDadosCliche: TRLBand
      Left = 2
      Top = 8
      Width = 276
      Height = 81
      AutoSize = True
      BandType = btHeader
      object pLogoeCliche: TRLPanel
        Left = 0
        Top = 0
        Width = 276
        Height = 81
        Align = faTop
        AutoExpand = True
        AutoSize = True
        object lEmitCNPJ_IE_IM: TRLLabel
          Left = 0
          Top = 31
          Width = 276
          Height = 12
          Align = faTop
          Alignment = taCenter
          Caption = 
            'CNPJ: 22.222.222/22222-22  IE:223.233.344.233 IM:2323.222.333.23' +
            '3'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Layout = tlBottom
          ParentFont = False
          BeforePrint = lEmitCNPJ_IE_IMBeforePrint
        end
        object lEndereco: TRLMemo
          Left = 0
          Top = 43
          Width = 276
          Height = 30
          Align = faTop
          Alignment = taCenter
          Behavior = [beSiteExpander]
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Lines.Strings = (
            'Endere'#231'o')
          ParentFont = False
          BeforePrint = lEnderecoBeforePrint
        end
        object RLDraw1: TRLDraw
          Left = 0
          Top = 73
          Width = 276
          Height = 8
          Align = faTop
          DrawKind = dkLine
          Pen.Width = 2
        end
        object imgLogo: TRLImage
          Left = 0
          Top = 0
          Width = 276
          Height = 1
          Align = faTop
          AutoSize = True
          Center = True
          Scaled = True
          Transparent = False
        end
        object lNomeFantasia: TRLMemo
          Left = 0
          Top = 1
          Width = 276
          Height = 18
          Align = faTop
          Alignment = taCenter
          Behavior = [beSiteExpander]
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Layout = tlCenter
          Lines.Strings = (
            'Nome Fantasia')
          ParentFont = False
          BeforePrint = lNomeFantasiaBeforePrint
        end
        object lRazaoSocial: TRLMemo
          Left = 0
          Top = 19
          Width = 276
          Height = 12
          Align = faTop
          Alignment = taCenter
          Behavior = [beSiteExpander]
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Lines.Strings = (
            'Raz'#227'o Social')
          ParentFont = False
          BeforePrint = lRazaoSocialBeforePrint
        end
      end
    end
    object rlBSubTitulo: TRLBand
      Left = 2
      Top = 89
      Width = 276
      Height = 24
      AutoSize = True
      BandType = btHeader
      object RLLabel1: TRLLabel
        Left = 0
        Top = 0
        Width = 276
        Height = 16
        Align = faTop
        Alignment = taCenter
        Caption = 'Comprovante de Sangria/Suprimento'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object RLDraw5: TRLDraw
        Left = 0
        Top = 16
        Width = 276
        Height = 8
        Align = faTop
        DrawKind = dkLine
        Pen.Width = 2
      end
    end
    object RLBand1: TRLBand
      Left = 2
      Top = 113
      Width = 276
      Height = 106
      BandType = btHeader
      object RLLabel2: TRLLabel
        Left = 69
        Top = 20
        Width = 42
        Height = 12
      end
      object RLLabel3: TRLLabel
        Left = 69
        Top = 36
        Width = 42
        Height = 12
      end
      object RLLabel4: TRLLabel
        Left = 69
        Top = 52
        Width = 42
        Height = 12
      end
      object RLLabel6: TRLLabel
        Left = 69
        Top = 84
        Width = 42
        Height = 12
      end
      object RLLabel7: TRLLabel
        Left = 32
        Top = 19
        Width = 32
        Height = 14
        Caption = 'Valor:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object RLLabel8: TRLLabel
        Left = 9
        Top = 35
        Width = 55
        Height = 14
        Caption = 'Descricao:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object RLLabel9: TRLLabel
        Left = 2
        Top = 51
        Width = 62
        Height = 14
        Caption = 'Funcion'#225'rio:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object RLLabel11: TRLLabel
        Left = 30
        Top = 83
        Width = 34
        Height = 14
        Caption = 'Turno:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object RLDraw2: TRLDraw
        Left = 0
        Top = 98
        Width = 276
        Height = 8
        Align = faBottom
        DrawKind = dkLine
        Pen.Width = 2
      end
      object RLLabel10: TRLLabel
        Left = 4
        Top = 67
        Width = 60
        Height = 14
        Caption = 'Forma Pag.:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object RLLabel12: TRLLabel
        Left = 69
        Top = 68
        Width = 42
        Height = 12
      end
      object RLLabel13: TRLLabel
        Left = 69
        Top = 4
        Width = 42
        Height = 12
      end
      object RLLabel14: TRLLabel
        Left = 36
        Top = 3
        Width = 28
        Height = 14
        Caption = 'Data:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
    end
    object rlbRodape: TRLBand
      Left = 2
      Top = 219
      Width = 276
      Height = 7
      AutoSize = True
      BandType = btHeader
      object RLLabel5: TRLLabel
        Left = 0
        Top = 0
        Width = 276
        Height = 7
        Align = faTop
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        BeforePrint = RLLabel5BeforePrint
      end
    end
  end
  object dsSanSup: TDataSource
    AutoEdit = False
    Left = 456
    Top = 72
  end
end
