unit uFrmFechamentoAbastecidas;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Mask, ExtCtrls, ComCtrls, Buttons, Grids, DBGrids, DB,
  DBClient, IBCustomDataSet, IBQuery, System.ImageList, Vcl.ImgList, Tredit;

type
  TfrmFechamentoAbastecidas = class(TForm)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Panel1: TPanel;
    Label1: TLabel;
    mkData: TMaskEdit;
    grdLMC: TDBGrid;
    btFecha: TBitBtn;
    cdsLMC: TClientDataSet;
    dsLMC: TDataSource;
    dsConcentrador: TDataSource;
    cdsLMCMercadoria: TStringField;
    cdsLMCDescricao: TStringField;
    cdsLMCFlag: TStringField;
    cdsLMCQtd_Leitura: TCurrencyField;
    cdsLMCQtd_Venda: TCurrencyField;
    cdsLMCQtd_Diferenca: TCurrencyField;
    btPesquisa: TBitBtn;
    Panel2: TPanel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    cdsConcentrador: TClientDataSet;
    cdsConcentradorMercadoria: TStringField;
    cdsConcentradorQuantidade: TCurrencyField;
    cdsConcentradorValor: TCurrencyField;
    cdsConcentradorTotal: TCurrencyField;
    cdsConcentradorAbastecida: TIntegerField;
    cdsConcentradorFlag: TStringField;
    cdsConcentradorBico: TIntegerField;
    TabSheet2: TTabSheet;
    grdConcentrador: TDBGrid;
    Panel3: TPanel;
    btCupom_Concentrador: TBitBtn;
    chkSelecionarTodos: TCheckBox;
    btCupom_LMC: TBitBtn;
    Panel4: TPanel;
    TabSheet3: TTabSheet;
    qEncerrante: TIBQuery;
    dsEncerrante: TDataSource;
    qEncerranteBOMBA: TIntegerField;
    qEncerranteINI_ENCERRANTE: TIBBCDField;
    qEncerranteFIM_ENCERRANTE: TIBBCDField;
    qEncerranteVENDAS: TIBBCDField;
    grdEncerrantes: TDBGrid;
    cdsConcentradorTemp_ECF_Item: TBooleanField;
    Panel5: TPanel;
    DBGrid1: TDBGrid;
    cdsEncerrante: TClientDataSet;
    dsCdsEncerrante: TDataSource;
    cdsEncerranteSaida_ECF: TCurrencyField;
    cdsEncerranteDiferenca: TCurrencyField;
    cdsEncerranteMercadoria: TStringField;
    cdsEncerranteSaida_Abastecimento: TCurrencyField;
    qEncerranteMERCADORIA: TIBStringField;
    chbUltimaLeitEncerrante: TCheckBox;
    qEncerranteDESCRICAO: TIBStringField;
    cdsConcentradorDescricao: TStringField;
    cdsEncerranteMerc_Descricao: TStringField;
    cdsConcentradorCHECK: TBooleanField;
    imgListAbastecidas: TImageList;
    Label5: TLabel;
    Panel6: TPanel;
    reTotal: TRealEdit;
    reUnitario: TRealEdit;
    reQuantidade: TRealEdit;
    procedure btPesquisaClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btFechaClick(Sender: TObject);
    procedure grdLMCDblClick(Sender: TObject);
    procedure grdLMCKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure grdLMCDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);

    procedure Selecionar;
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btCupom_ConcentradorClick(Sender: TObject);
    procedure chkSelecionarTodosClick(Sender: TObject);
    procedure grdConcentradorDrawColumnCell(Sender: TObject;
      const Rect: TRect; DataCol: Integer; Column: TColumn;
      State: TGridDrawState);
    procedure btCupom_LMCClick(Sender: TObject);
    procedure grdConcentradorKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure DBGrid1CellClick(Column: TColumn);
    procedure grdConcentradorMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure grdConcentradorTitleClick(Column: TColumn);
    procedure cdsConcentradorAfterPost(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
    nVlr_Total, nVlr_Unitario, nQtd: Currency;
    Function GetAbastecidaInserida(AID: Integer): Boolean;
    Procedure SomaTotais;
    Procedure SubtraiTotais;
  end;

var
  frmFechamentoAbastecidas: TfrmFechamentoAbastecidas;

implementation

uses uDMCupomFiscal, ufrmPosto, Math, uDMComponentes, uRotinasGlobais,
  uFuncoes, uFrmPrincipal;

{$R *.dfm}

procedure TfrmFechamentoAbastecidas.btPesquisaClick(Sender: TObject);
begin
  Case PageControl1.TabIndex of
    0:begin //LMC
      CDSLMC.Close;
      CDSLMC.CreateDataSet;
      CDSLMC.Active:=True;
      CDSLMC.Open;

      //Leitura LMC
      DMCupomFiscal.dbQuery2.Active:=False;
      DMCupomFiscal.dbQuery2.SQL.Clear;
      DMCupomFiscal.dbQuery2.SQL.Add('Select l.mercadoria, m.Descricao, l.vol_saida as Qtd_Leitura'+
                                         ' From LMC l'+
                                         ' Left Outer Join est_mercadorias m on m.id=l.mercadoria'+
                                         ' Where (l.Data=:pData)'+
                                         ' Order By m.Descricao');
      DMCupomFiscal.dbQuery2.ParamByName('pData').AsDate:=StrToDateDef(mkData.Text, EL_Data);
      DMCupomFiscal.dbQuery2.Active:=True;
      DMCupomFiscal.dbQuery2.First;
      If DMCupomFiscal.dbQuery2.Eof then
      begin
        msgInformacao('N�o ha leitura de LMC para esta Data!','');
        mkData.SetFocus;
        Abort;
      end;

      While Not(DMCupomFiscal.dbQuery2.Eof) do
      begin
        //Vendas no Dia
        DMCupomFiscal.dbQuery1.Active:=False;
        DMCupomFiscal.dbQuery1.SQL.Clear;
        DMCupomFiscal.dbQuery1.SQL.Add('Select Sum(ECFi.Qtd) as Qtd_Venda'+
                                       ' From Est_ECF ECF'+
                                       ' Left Outer Join Est_ECF_Item ECFi on ECF.ID=ECFi.ID_ECF'+
                                       ' Where (ECF.Data=:pData) and (ECF.CCusto=:pCC) and (ECFi.Mercadoria=:pMer) and (ECF.Status=''X'')');
        DMCupomFiscal.dbQuery1.ParamByName('pData').AsDate:=StrToDateDef(mkData.Text, EL_Data);
        DMCupomFiscal.dbQuery1.ParamByName('pMer').AsString:=DMCupomFiscal.dbQuery2.FieldByName('mercadoria').AsString;
        DMCupomFiscal.dbQuery1.ParamByName('pCC').AsInteger:= CCUSTO.codigo;
        DMCupomFiscal.dbQuery1.Active:=True;
        DMCupomFiscal.dbQuery1.First;

        CDSLMC.Append;
        CDSLMCMercadoria.Value:=DMCupomFiscal.dbQuery2.FieldByName('Mercadoria').AsString;
        CDSLMCDescricao.Value:=DMCupomFiscal.dbQuery2.FieldByName('Descricao').AsString;
        CDSLMCQtd_Venda.Value:=DMCupomFiscal.dbQuery1.FieldByName('Qtd_Venda').AsCurrency;
        CDSLMCQtd_Leitura.Value:=DMCupomFiscal.dbQuery2.FieldByName('Qtd_Leitura').AsCurrency;
        CDSLMCQtd_Diferenca.Value:= DMCupomFiscal.dbQuery2.FieldByName('Qtd_Leitura').AsCurrency - DMCupomFiscal.dbQuery1.FieldByName('Qtd_Venda').AsCurrency;
        CDSLMCFlag.Value:='0';
        CDSLMC.Post;
        DMCupomFiscal.dbQuery2.Next;
      end;
      grdLMC.SetFocus;
    end;
    1:begin  //Concentrador
      CDSConcentrador.Close;
      CDSConcentrador.CreateDataSet; 
      CDSConcentrador.Active:=True;
      CDSConcentrador.Open;

      //Leitura Abastecidas
      DMCupomFiscal.dbQuery1.Active:=False;
      DMCupomFiscal.dbQuery1.SQL.Clear;
      DMCupomFiscal.dbQuery1.SQL.Add('Select A.Litro, A.Preco, A.Dinheiro, A.ID, A.Bomba, M.ID as Codigo, M.Descricao,'+
                                   ' t.abastecida'+
                                   ' From Est_Abastecimentos A'+
                                   ' Left Outer Join Est_Mercadorias_Bico B on B.ID=A.Bomba'+
                                   ' Left Outer Join Est_Mercadorias M on M.ID=B.Mercadoria'+
                                   ' Left Outer Join temp_nfce_item t on t.abastecida=A.ID'+
                                   ' WHERE (cast(A.Data_Hora as date)=:pDH) and (A.Status=''X'')'+
                                   ' and not exists (Select i.Abastecida from Temp_nfce_item i where i.abastecida = a.id and i.caixa <> :pCaixa)'+
                                   ' Order By M.Descricao');
      DMCupomFiscal.dbQuery1.ParamByName('pDH').AsDateTime:=StrToDateTime(mkData.Text);
      DMCupomFiscal.dbQuery1.ParamByName('pCaixa').AsString:=FormatFloat('0000',FRENTE_CAIXA.Caixa);
      DMCupomFiscal.dbQuery1.Active:=True;
      DMCupomFiscal.dbQuery1.First;
      If DMCupomFiscal.dbQuery1.Eof then
      begin
        msgInformacao('N�o existem abastecidas pendentes!','');
        mkData.SetFocus;
        Abort;
      end;
      While Not(DMCupomFiscal.dbQuery1.Eof) do
      begin
        cdsConcentrador.Append;
        cdsConcentradorMercadoria.Value := DMCupomFiscal.dbQuery1.FieldByName('Codigo').AsString;
        cdsConcentradorDescricao.Value  := DMCupomFiscal.dbQuery1.FieldByName('Descricao').AsString;
        cdsConcentradorQuantidade.Value := DMCupomFiscal.dbQuery1.FieldByName('Litro').AsCurrency;
        cdsConcentradorValor.Value      := DMCupomFiscal.dbQuery1.FieldByName('Preco').AsCurrency;
        cdsConcentradorTotal.Value      := DMCupomFiscal.dbQuery1.FieldByName('Dinheiro').AsCurrency;
        cdsConcentradorAbastecida.Value := DMCupomFiscal.dbQuery1.FieldByName('ID').AsInteger;
        cdsConcentradorBico.Value       := DMCupomFiscal.dbQuery1.FieldByName('Bomba').AsInteger; //Ecf_item esta Bico nas Abast esta Bomba
        cdsConcentradorCHECK.Value      := False;
        if DMCupomFiscal.dbQuery1.FieldByName('Abastecida').AsInteger > 0 then
        begin
          cdsConcentradorTemp_ECF_Item.Value := True;
          cdsConcentradorFlag.Value          := '1';
          nVlr_Total := nVlr_Total + cdsConcentradorTotal.Value;
          nVlr_Unitario := nVlr_Unitario + cdsConcentradorValor.Value;
          nQtd := nQtd + cdsConcentradorQuantidade.Value;
        end
        else
        begin
          cdsConcentradorTemp_ECF_Item.Value := False;
          cdsConcentradorFlag.Value          := '0';
        end;
        cdsConcentrador.Post;
        DMCupomFiscal.dbQuery1.Next;
      end;
      reTotal.Value := nVlr_Total;
      reUnitario.Value := nVlr_Unitario;
      reQuantidade.Value := nQtd;
      grdConcentrador.SetFocus;
    end;
    2:begin //Leitura dos Encerrantes
      qEncerrante.Active:=False;
      qEncerrante.SQL.Clear;
      if chbUltimaLeitEncerrante.Checked then
      begin
        qEncerrante.SQL.Add('SELECT M.Descricao, m.id mercadoria, A.Bomba,'+
                           ' 0.000 ini_Encerrante,'+
                           ' MAX(A.Lt_Encerrante) AS Fim_Encerrante,'+
                           ' 0.000 AS Vendas'+
                           ' FROM est_abastecimentos A'+
                           ' LEFT OUTER JOIN Est_Mercadorias_Bico MB ON MB.ID=A.Bomba'+
                           ' LEFT OUTER JOIN Est_Mercadorias M ON M.ID=MB.Mercadoria'+
                           ' GROUP BY M.Descricao, m.id, A.Bomba'+
                           ' ORDER BY M.Descricao');
      end
      else
      begin
        qEncerrante.SQL.Add('SELECT M.Descricao, m.id mercadoria, A.Bomba,'+
                           ' MIN(A.Lt_Encerrante - A.litro) AS ini_Encerrante,'+
                           ' MAX(A.Lt_Encerrante) AS Fim_Encerrante,'+
                           ' MAX(A.Lt_Encerrante) - MIN(A.Lt_Encerrante - A.litro) AS Vendas'+
                           ' FROM est_abastecimentos A'+
                           ' LEFT OUTER JOIN Est_Mercadorias_Bico MB ON MB.ID=A.Bomba'+
                           ' LEFT OUTER JOIN Est_Mercadorias M ON M.ID=MB.Mercadoria'+
                           ' WHERE CAST(A.Data_hora AS DATE)=:pData'+
                           ' GROUP BY M.Descricao, m.id, A.Bomba'+
                           ' ORDER BY M.Descricao');
        qEncerrante.ParamByName('pData').AsDate:=StrToDate(mkData.Text);
      end;
      qEncerrante.Active:=True;
      qEncerrante.First;
      If qEncerrante.eof then
        msgInformacao('N�o existem dados para serem visualizados!','');
    end;
  end;
end;

procedure TfrmFechamentoAbastecidas.FormShow(Sender: TObject);
begin
  DMCupomFiscal.dbQuery1.Active:=False;
  mkData.Text:=DateToStr(EL_Data);
  PageControl1.TabIndex:=0;
end;

function TfrmFechamentoAbastecidas.GetAbastecidaInserida(AID: Integer): Boolean;
var
  QryBusca: TIBQuery;
begin
  QryBusca := TIBQuery.Create(Self);
  try
    QryBusca.active := false;
    QryBusca.Database := dmCupomFiscal.DataBase;
    QryBusca.sql.clear;
    QryBusca.sql.Add('Select * from EST_NFCE_ITEM where Abastecida = :pID');
    QryBusca.ParamByName('pID').AsInteger := AID;
    QryBusca.Active := True;
    Result := not(QryBusca.IsEmpty);
  finally
    QryBusca.free;
  end;
end;

procedure TfrmFechamentoAbastecidas.btFechaClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmFechamentoAbastecidas.Selecionar;
begin
  If CDSLMCQtd_Diferenca.AsCurrency=0 then
  begin
    ShowMessage('N�o existe diferen�aa entre Leitura LMC e Cupom Fiscal.');
    btFecha.Click;
  end;

  If CDSLMCQtd_Diferenca.AsCurrency<0 then
  begin
    ShowMessage('Leitura LMC � MENOR que venda em Cupom Fiscal.'+#13+'Verifique a Leitura do LMC.');
    grdLMC.SetFocus;
    Abort;
  end;

  frmPosto.edtMercadoria.Text:=CDSLMCMercadoria.Text;
  frmPosto.reQuantidade.Value:=CDSLMCQtd_Diferenca.AsCurrency;
  frmPosto.edtMercadoria.SetFocus;
  btFecha.Click;
end;

procedure TfrmFechamentoAbastecidas.SomaTotais;
begin
  nVlr_Total := nVlr_Total + cdsConcentradorTotal.Value;
  nVlr_Unitario := nVlr_Unitario + cdsConcentradorValor.Value;
  nQtd := nQtd + cdsConcentradorQuantidade.Value;
  reQuantidade.Value := nQtd;
  reTotal.Value := nVlr_Total;
  reUnitario.Value := nVlr_Unitario;
end;

procedure TfrmFechamentoAbastecidas.SubtraiTotais;
begin
  nVlr_Total := nVlr_Total - cdsConcentradorTotal.Value;
  nVlr_Unitario := nVlr_Unitario - cdsConcentradorValor.Value;
  nQtd := nQtd - cdsConcentradorQuantidade.Value;
  reQuantidade.Value := nQtd;
  reTotal.Value := nVlr_Total;
  reUnitario.Value := nVlr_Unitario;
end;

procedure TfrmFechamentoAbastecidas.grdLMCDblClick(Sender: TObject);
begin
  Selecionar;
end;

procedure TfrmFechamentoAbastecidas.grdLMCKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if Key=13 then Selecionar;
end;

procedure TfrmFechamentoAbastecidas.grdLMCDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  If CDSLMCQtd_Diferenca.Value = 0 then
     grdLMC.Canvas.Font.Color:= clLime;

  If CDSLMCQtd_Diferenca.Value < 0 then
    grdLMC.Canvas.Font.Color:= clBlue;

  If CDSLMCQtd_Diferenca.Value > 0 then
    grdLMC.Canvas.Font.Color:= clRed;

  grdLMC.DefaultDrawDataCell(Rect, grdLMC.columns[datacol].field, State);
end;

procedure TfrmFechamentoAbastecidas.FormKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  If Key=VK_ESCAPE then btFecha.Click;
end;

procedure TfrmFechamentoAbastecidas.btCupom_ConcentradorClick(Sender: TObject);
//var contSelecionados:Integer;
begin
  //contSelecionados:=0;
  Panel4.Visible:=True;
  btCupom_Concentrador.Enabled := false;
  try
    Application.ProcessMessages;
    CDSConcentrador.First;
    While not(CDSConcentrador.Eof) do
    begin
      If (CDSConcentradorFlag.AsInteger=1) and not(cdsConcentradorTemp_ECF_Item.Value)then
      begin
        frmPosto.edtMercadoria.Text:=CDSConcentradorMercadoria.Text;
        frmPosto.reQuantidade.Value:=CDSConcentradorQuantidade.Value;
        frmPosto.reVlrUnitario.Value:=CDSConcentradorValor.Value;
        frmPosto.reTotal.Value:=CDSConcentradorTotal.Value;
        frmPosto.edtBico.Text:=CDSConcentradorBico.Text;
        frmPosto.Abastecimento:=CDSConcentradorAbastecida.Value;
        frmPosto.edtMercadoriaExit(Self);
        frmPosto.SubTotal;
        frmPosto.btnVende.Click;
        //contSelecionados:=contSelecionados+1;
      end;
      CDSConcentrador.Next;
    end;
  {  if contSelecionados=0 then
    begin
      msgInformacao('Nenhuma abastecida foi selecionada. Favor selecionar abastecida desejada e pressionar a tecla Enter','Aten��o');
      Panel4.Visible:=False;
      Application.ProcessMessages;
    end
    else}
  finally
    btCupom_Concentrador.Enabled := True;
  end;
  btFecha.Click;
end;

procedure TfrmFechamentoAbastecidas.cdsConcentradorAfterPost(DataSet: TDataSet);
var
  nAbastecida: Integer;
begin
  if not(cdsConcentrador.IsEmpty)Then
  begin
    nAbastecida := cdsConcentradorAbastecida.AsInteger;
    btCupom_Concentrador.Enabled := cdsConcentrador.Locate('Flag','1',[loCaseInsensitive]);
    cdsConcentrador.Locate('Abastecida',nAbastecida,[loCaseInsensitive]);
  end;
end;

procedure TfrmFechamentoAbastecidas.chkSelecionarTodosClick(Sender: TObject);
begin
  cdsConcentrador.First;
  While not(cdsConcentrador.eof) do
  begin
    if not(cdsConcentradorTemp_ECF_Item.Value) then
    begin
      cdsConcentrador.Edit;
      if chkSelecionarTodos.Checked then
        cdsConcentradorFlag.Value:='1'
      Else
        cdsConcentradorFlag.Value:='0';
      cdsConcentrador.Post;
    end;
    cdsConcentrador.Next;
  end;
  cdsConcentrador.First;
end;

procedure TfrmFechamentoAbastecidas.grdConcentradorDrawColumnCell(
  Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  if not(cdsConcentrador.Eof) then
  begin
    If cdsConcentradorTemp_ECF_Item.Value then
    begin
      grdConcentrador.Canvas.Brush.Color := HexToTColor('88B8FF');
      grdConcentrador.Canvas.FillRect(Rect);
    end;

    If CDSConcentradorFlag.Value = '0' then
       grdConcentrador.Canvas.Font.Color:= clBlack;

    If CDSConcentradorFlag.Value = '1' then
      grdConcentrador.Canvas.Font.Color:= clRed;

    //dbGConcentrador.DefaultDrawDataCell(Rect, dbGConcentrador.columns[datacol].field, State);
    grdConcentrador.DefaultDrawColumnCell(Rect, DataCol, Column, State);

    if cdsConcentradorFlag.Value = '1' then
    begin
      grdConcentrador.Canvas.Brush.Color := HexToTColor('88B8FF');
      grdConcentrador.Canvas.FillRect(Rect);
      grdConcentrador.DefaultDrawColumnCell(Rect, DataCol, Column, State);
    end;
    if LowerCase(Column.FieldName) = 'flag' then
    begin
      grdConcentrador.Canvas.Brush.Color := DBGrid1.Color;
      grdConcentrador.Canvas.FillRect(Rect);
      grdConcentrador.DefaultDrawColumnCell(Rect,DataCol,Column,State);
      grdConcentrador.Canvas.FillRect(Rect);
      if (cdsConcentradorFlag.Value = '1') then
        imgListAbastecidas.Draw(grdConcentrador.Canvas,Rect.Left+04,Rect.Top+1,0)
      else
        if(cdsConcentradorTemp_ECF_Item.Value)then
          imgListAbastecidas.Draw(grdConcentrador.Canvas,Rect.Left+04,Rect.Top+1,0)
        else
          imgListAbastecidas.Draw(grdConcentrador.Canvas,Rect.Left+04,Rect.Top+1,1);
    end;
  end;
end;

procedure TfrmFechamentoAbastecidas.btCupom_LMCClick(Sender: TObject);
begin
  Selecionar;
end;

procedure TfrmFechamentoAbastecidas.grdConcentradorKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if (Key=13) or (Key = VK_SPACE) then
  begin
    if not(cdsConcentradorTemp_ECF_Item.Value) then
    begin
      CDSConcentrador.Edit;
      If CDSConcentradorFlag.Value='0' then
        CDSConcentradorFlag.Value:='1'
      Else
        if not(cdsConcentradorTemp_ECF_Item.Value)then
          CDSConcentradorFlag.Value:='0';
      CDSConcentrador.Post;
      if not(cdsConcentradorTemp_ECF_Item.Value)Then
      begin
        if(cdsConcentradorFlag.Value = '1')then
        begin
          nVlr_Total := nVlr_Total + cdsConcentradorTotal.Value;
          nVlr_Unitario := nVlr_Unitario + cdsConcentradorValor.Value;
          nQtd := nQtd + cdsConcentradorQuantidade.Value;
        end
        else
        begin
          nVlr_Total := nVlr_Total - cdsConcentradorTotal.Value;
          nVlr_Unitario := nVlr_Unitario - cdsConcentradorValor.Value;
          nQtd := nQtd - cdsConcentradorQuantidade.Value;
        end;
      end;
      reTotal.Value := nVlr_Total;
      reUnitario.Value := nVlr_Unitario;
      reQuantidade.Value := nQtd;
    end;
  end;
end;

procedure TfrmFechamentoAbastecidas.grdConcentradorMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if not(cdsConcentrador.IsEmpty)then
  begin
    if (grdConcentrador.MouseCoord(X, Y).X in [1]) and
      not (grdConcentrador.MouseCoord(X, Y).Y in [0]) then
    begin
      cdsConcentrador.Edit;
      If CDSConcentradorFlag.Value='0' then
        CDSConcentradorFlag.Value:='1'
      Else
        if not(cdsConcentradorTemp_ECF_Item.Value)then
          CDSConcentradorFlag.Value:='0';
      cdsConcentrador.Post;
      if not(cdsConcentradorTemp_ECF_Item.Value)Then
      begin
        if(cdsConcentradorFlag.Value = '1')then
        begin
          nVlr_Total := nVlr_Total + cdsConcentradorTotal.Value;
          nVlr_Unitario := nVlr_Unitario + cdsConcentradorValor.Value;
          nQtd := nQtd + cdsConcentradorQuantidade.Value;
        end
        else
        begin
          nVlr_Total := nVlr_Total - cdsConcentradorTotal.Value;
          nVlr_Unitario := nVlr_Unitario - cdsConcentradorValor.Value;
          nQtd := nQtd - cdsConcentradorQuantidade.Value;
        end;
      end;
      reTotal.Value := nVlr_Total;
      reUnitario.Value := nVlr_Unitario;
      reQuantidade.Value := nQtd;
    end;
  end;
end;

procedure TfrmFechamentoAbastecidas.grdConcentradorTitleClick(Column: TColumn);
var
  nAbastecida: Integer;
begin
  if not(cdsConcentrador.IsEmpty)then
  begin
    if LowerCase(Column.FieldName) = 'flag' then
    begin
      nAbastecida := cdsConcentradorAbastecida.Value;
      cdsConcentrador.First;
      while not(cdsConcentrador.Eof)do
      begin
        cdsConcentrador.Edit;
        If CDSConcentradorFlag.Value='0' then
          CDSConcentradorFlag.Value:='1'
        Else
          if not(cdsConcentradorTemp_ECF_Item.Value)then
            CDSConcentradorFlag.Value:='0';
        cdsConcentrador.Post;
        if not(cdsConcentradorTemp_ECF_Item.Value)Then
        begin
          if(cdsConcentradorFlag.Value = '1')then
          begin
            nVlr_Total := nVlr_Total + cdsConcentradorTotal.Value;
            nVlr_Unitario := nVlr_Unitario + cdsConcentradorValor.Value;
            nQtd := nQtd + cdsConcentradorQuantidade.Value;
          end
          else
          begin
            nVlr_Total := nVlr_Total - cdsConcentradorTotal.Value;
            nVlr_Unitario := nVlr_Unitario - cdsConcentradorValor.Value;
            nQtd := nQtd - cdsConcentradorQuantidade.Value;
          end;
          reTotal.Value := nVlr_Total;
          reUnitario.Value := nVlr_Unitario;
          reQuantidade.Value := nQtd;
        end;
        cdsConcentrador.Next;
        Refresh;
      end;
      cdsConcentrador.Locate('Abastecida',nAbastecida,[loCaseInsensitive]);
      application.ProcessMessages;
    end;
  end;
end;

procedure TfrmFechamentoAbastecidas.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  DMCupomFiscal.dbQuery1.Active:=False;
  qEncerrante.Active:=False;
end;

procedure TfrmFechamentoAbastecidas.FormCreate(Sender: TObject);
begin
  nVlr_Total := 0;
  nVlr_Unitario := 0;
  nQtd := 0;
end;

procedure TfrmFechamentoAbastecidas.DBGrid1CellClick(Column: TColumn);
begin
  if not(chbUltimaLeitEncerrante.Checked) then
  begin
    cdsEncerrante.Close;
    cdsEncerrante.CreateDataSet;
    cdsEncerrante.Active:=True;
    cdsEncerrante.Open;

    //Vendas no Dia
    DMCupomFiscal.dbQuery1.Active:=False;
    DMCupomFiscal.dbQuery1.SQL.Clear;
    DMCupomFiscal.dbQuery1.SQL.Add('Select Sum(ECFi.Qtd) as Qtd_Venda'+
                                   ' From Est_ECF ECF'+
                                   ' Left Outer Join Est_ECF_Item ECFi on ECF.ID=ECFi.ID_ECF'+
                                   ' Where (ECF.Data=:pData) and (ECF.CCusto=:pCC) and (ECFi.Mercadoria=:pMer) and (ECF.Status=''X'')');
    DMCupomFiscal.dbQuery1.ParamByName('pData').AsDate  := StrToDateDef(mkData.Text, EL_Data);
    DMCupomFiscal.dbQuery1.ParamByName('pMer').AsString := qEncerranteMERCADORIA.AsString;;
    DMCupomFiscal.dbQuery1.ParamByName('pCC').AsInteger := CCUSTO.codigo;
    DMCupomFiscal.dbQuery1.Active:=True;
    DMCupomFiscal.dbQuery1.First;

    //Abastecida do dia
    DMCupomFiscal.dbQuery2.Active:=False;
    DMCupomFiscal.dbQuery2.SQL.Clear;
    DMCupomFiscal.dbQuery2.SQL.Add('SELECT MIN(A.Lt_Encerrante - A.litro) AS ini_Encerrante,'+
                                  ' MAX(A.Lt_Encerrante) AS Fim_Encerrante,'+
                                  ' MAX(A.Lt_Encerrante) - MIN(A.Lt_Encerrante - A.litro) AS Vendas'+
                                  ' FROM est_abastecimentos A'+
                                  ' LEFT OUTER JOIN Est_Mercadorias_Bico MB ON MB.ID=A.Bomba'+
                                  ' LEFT OUTER JOIN Est_Mercadorias M ON M.ID=MB.Mercadoria'+
                                  ' WHERE CAST(A.Data_hora AS DATE)=:pData and m.id=:pMer');
    DMCupomFiscal.dbQuery2.ParamByName('pData').AsDate  := StrToDateDef(mkData.Text, EL_Data);
    DMCupomFiscal.dbQuery2.ParamByName('pMer').AsString := qEncerranteMERCADORIA.AsString;;
    DMCupomFiscal.dbQuery2.Active:=True;
    DMCupomFiscal.dbQuery2.First;
    if not(dmCupomFiscal.dbQuery2.Eof) then
    begin
      cdsEncerrante.Insert;
      cdsEncerranteSaida_ECF.Value           := dmCupomFiscal.dbQuery1.FieldByName('qtd_venda').AsCurrency;
      cdsEncerranteMercadoria.Value          := qEncerranteMERCADORIA.AsString;
      cdsEncerranteMerc_Descricao.Value      := qEncerranteDESCRICAO.AsString;
      cdsEncerranteSaida_Abastecimento.Value := dmCupomFiscal.dbQuery2.FieldByName('vendas').AsCurrency;
      cdsEncerranteDiferenca.Value           := cdsEncerranteSaida_Abastecimento.Value - cdsEncerranteSaida_ECF.Value;
      cdsEncerrante.Post;
    end;
  end;
end;

end.

