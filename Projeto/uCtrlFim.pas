unit uCtrlFim;

interface

Uses
  System.SysUtils, System.StrUtils, pcnauxiliar, ACBrUtil, Data.DB,
  System.Variants, Vcl.Forms, Vcl.Controls, Vcl.Dialogs, IBX.IBQuery,
  System.DateUtils;

type
  TCtrlFim = Class
  private
    Function validarInformacoes: Boolean;
    Function getNomeVendedor(pId: Integer): String;
  public
    ID: Integer;
    Function finalizar(pNF: Boolean; pcaixa: Integer): Boolean;
    Function gerarParcelas: Boolean;
    Function GerarCR(el_chave, numero: Integer): String;
    Function Gera_NFe(ID, NF_Operacao: Integer): Boolean;
    function getSaldoDevedor(): Currency;
    function getInformaSaldoDevedor(): Boolean;
    function getJuroCliente(): Currency;
  end;

implementation

uses
  uDMCupomFiscal, uFrmLoja, uFrmPosto, uFuncoes, uFrmSupermercado, uDMComponentes,
  uFrmNFCe, uFrmPrincipal, uRotinasGlobais, uFrmGerarParcelas, uFrmVendedor;

{ CtrlFim }

function TCtrlFim.finalizar(pNF: Boolean; pcaixa: Integer):Boolean;
var media, lSaldoDevedor : Currency;
    numero, el_chave, NF_OPERACAO : Integer;
    Finaliza, Obs, txtParcelas, ID_COMANDA : String;
    lQrBusca: TIBQuery;
    lFrmVendedor: TfrmVendedor;
begin
  Result := True;
  if(validarInformacoes)then
  begin
    if(frmNFCe.qConfigNFCeNFCE_CONFIRMA_FINALIZACAO.AsString = 'S')then
    begin
      if msgPergunta('Confirmar finaliza��o da NFCe ?','') then
        Finaliza := 'S'
      else
        Finaliza := 'N';
    end
    else
      Finaliza := 'S';

    if(Finaliza = 'S')then
    begin
      //--- NFe Lauro -----
      If(pNF)then
      begin
        if dmCupomFiscal.dbTemp_NFCECLIFOR.Value <= 0 then
        begin
          msgInformacao('Para emiss�o de NF-e deve ser informado cliente','Informa��o');
          Result := False;
          Abort;
        end
        else
        begin
          //CliFor - UF, IE
          dmCupomFiscal.dbQuery1.Active:=False;
          dmCupomFiscal.dbQuery1.SQL.Clear;
          dmCupomFiscal.dbQuery1.SQL.Add('Select CF.UF From CliFor_IE CF Where CF.Clifor=:pCliFor and CF.IE=:pIE');
          dmCupomFiscal.dbQuery1.ParamByName('pCliFor').AsInteger:=dmCupomFiscal.dbTemp_NFCECLIFOR.AsInteger;
          dmCupomFiscal.dbQuery1.ParamByName('pIE').AsString:=dmCupomFiscal.dbTemp_NFCECF_IE.AsString;
          dmCupomFiscal.dbQuery1.Active:=True;
          dmCupomFiscal.dbQuery1.First;
          If dmCupomFiscal.dbQuery1.Eof then
          begin
            msgAviso('Verifique o cadastro do cliente, bem como sua'+
              'Inscri��o Estadual para emiss�o da NF-e','Informa��o');
            Result := False;
            Abort;
          end;

          If (dmCupomFiscal.dbQuery1.FieldByName('UF').AsString='RS') then
            NF_Operacao:=592901
          Else
            NF_Operacao:=692901;
        end;
      end;
      //--- Fim NFe Lauro -----

      //Solicita Funcionario
      if(FRENTE_CAIXA.Informar_Vendedor_Fim)Then
      begin
        lFrmVendedor := TfrmVendedor.Create(nil);
        try
          lFrmVendedor.edtFunc.Text := dmCupomFiscal.dbTemp_NFCEFUNCIONARIO.AsString;
          case lFrmVendedor.ShowModal of
          mrCancel: Abort;
          end;
        finally
          lFrmVendedor.Free;
        end;
      end;

      numero:=0;
      try
        //Chama trigger calcula troco por Forma Pag;
        dmCupomFiscal.IBSQL1.Close;
        dmCupomFiscal.IBSQL1.SQL.Clear;
        dmCupomFiscal.IBSQL1.SQL.Add('Update temp_nfce Set status=''F'' Where Caixa=:pCaixa');
        dmCupomFiscal.IBSQL1.ParamByName('pCaixa').asString:=FormatFloat('0000', pcaixa);
        dmCupomFiscal.IBSQL1.ExecQuery;
        dmCupomFiscal.IBSQL1.Close;

        Obs:='';
        //Obs. para Simples Nacional - Obrigatorio Lei para Simples Nacional
        if (Trim(FRENTE_CAIXA.Msg_Simples_Nacional) <> '') then
          Obs:=Obs+Trim(FRENTE_CAIXA.Msg_Simples_Nacional+';');
        //Imprimir Vendedor
        if FRENTE_CAIXA.ImprimirVendedor then
          Obs:=Obs+'Vendedor: '+Trim(dmCupomFiscal.dbTemp_NFCEFUNCIONARIO.AsString)+'/'+
            Trim(getNomeVendedor(dmCupomFiscal.dbTemp_NFCEFUNCIONARIO.AsInteger))+';';
        //Obs. Mensagem Promocional Cupom
        if Trim(FRENTE_CAIXA.MensagemCupom) <> '' then
          Obs:=Obs+FRENTE_CAIXA.MensagemCupom+';';
        Obs:=Obs+IfThen(Trim(dmCupomFiscal.dbTemp_NFCECF_IE.AsString)='', '',
          'IE: '+Trim(dmCupomFiscal.dbTemp_NFCECF_IE.AsString)+'  ');
        //Quando tiver Placa ou KM informar nas Obs
        If (Trim(dmCupomFiscal.dbTemp_NFCECF_PLACA.AsString) <> '') or
          (StrToIntDef(dmCupomFiscal.dbTemp_NFCECF_KM.AsString,0) > 0) then
          Obs:=Obs+'Placa: '+dmCupomFiscal.dbTemp_NFCECF_PLACA.AsString+'  KM: '+
          dmCupomFiscal.dbTemp_NFCECF_KM.AsString;
        if not(dmCupomFiscal.dbTemp_NFCECF_MOTORISTA.IsNull)Then
          Obs:=Obs+';Motorista: '+dmCupomFiscal.dbTemp_NFCECF_MOTORISTA.AsString+';';
        //Observa��o referente Faturas
        DMCupomFiscal.dbTemp_NFCE_Fatura.First;
        if not DMCupomFiscal.dbTemp_NFCE_Fatura.Eof then
        begin
          txtParcelas := ';;Reconheco o presente como documento de divida, em meu nome e '+
            'comprometo-me a pagar no(s) vencimento(s) abaixo estipulado(s):;'+
            'Vencimento      Valor R$';
          While not(DMCupomFiscal.dbTemp_NFCE_Fatura.Eof) do
          begin
            txtParcelas := txtParcelas +';'+ DMCupomFiscal.dbTemp_NFCE_FaturaData.Text+'          '+DMCupomFiscal.dbTemp_NFCE_FaturaValor.Text;
            DMCupomFiscal.dbTemp_NFCE_Fatura.Next;
          end;
          //if(allTrim(txtParcelas)<>';;VencimentoValorR$') then
          Obs:=Obs+txtParcelas;
        end;

        Obs:=Copy(Obs,1,Length(Obs));
        //Obs:=StringReplace(Obs,';',#10,[rfReplaceAll,rfIgnoreCase]) ;

        //--- Gravar no Banco de dados EST_ECF
        Try
          IF (Numero=0) then
          begin
            dmCupomFiscal.IBSQL1.Close;
            dmCupomFiscal.IBSQL1.SQL.Clear;
            dmCupomFiscal.IBSQL1.SQL.Add('Update Serie_Documentos Set Numero=Numero+1 Where (ID=:pID) and (modelo_doc=:pCodMod) and (Local=:pLocal)');
            dmCupomFiscal.IBSQL1.ParamByName('pId').asString:=dmCupomFiscal.dbTemp_NFCESERIE.AsString;
            dmCupomFiscal.IBSQL1.ParamByName('pLocal').AsInteger:=getLocalCCusto(dmCupomFiscal.dbTemp_NFCECCUSTO.AsInteger);
            dmCupomFiscal.IBSQL1.ParamByName('pCodMod').AsString:=Trim(dmCupomFiscal.dbTemp_NFCEMODELO_DOC.AsString);
            dmCupomFiscal.IBSQL1.ExecQuery;
            dmCupomFiscal.IBSQL1.Close;

            dmCupomFiscal.dbQuery1.Active:=False;
            dmCupomFiscal.dbQuery1.SQL.Clear;
            dmCupomFiscal.dbQuery1.SQL.Add('Select Numero From Serie_Documentos Where Id=:pId and modelo_doc=:pCodMod and Local=:pLocal');
            dmCupomFiscal.dbQuery1.ParamByName('pId').asString:=dmCupomFiscal.dbTemp_NFCESERIE.AsString;
            dmCupomFiscal.dbQuery1.ParamByName('pLocal').AsInteger:=getLocalCCusto(dmCupomFiscal.dbTemp_NFCECCUSTO.AsInteger);
            dmCupomFiscal.dbQuery1.ParamByName('pCodMod').AsString:=Trim(dmCupomFiscal.dbTemp_NFCEMODELO_DOC.AsString);
            dmCupomFiscal.dbQuery1.Active:=True;
            Numero:=dmCupomFiscal.dbQuery1.fieldbyname('Numero').AsInteger;
          end;

          DMCupomFiscal.dbTemp_NFCE.edit;
          DMCupomFiscal.dbTemp_NFCEDATA.Value := Now;
          DMCupomFiscal.dbTemp_NFCEHORA.Value := Time;
          DMCupomFiscal.dbTemp_NFCE.Post;

          DMCupomFiscal.dbECF.Open;
          DMCupomFiscal.dbECF.Insert;
          DMCupomFiscal.dbECFCAIXA.Value        := FormatFloat('0000',FRENTE_CAIXA.Caixa);
          dmCupomFiscal.dbECFMODELO_DOC.Value   := DMCupomFiscal.dbTemp_NFCEMODELO_DOC.Value;
          DMCupomFiscal.dbECFCUPOM.Value        := '0'; //Somente EST ECF
          dmCupomFiscal.dbECFNUMERO.Value       := numero;
          dmCupomFiscal.dbECFSERIE.Value        := DMCupomFiscal.dbTemp_NFCESERIE.Value;
          DMCupomFiscal.dbECFDATA.Value         := dmCupomFiscal.dbTemp_NFCEDATA.Value;
          dmCupomFiscal.dbECFHORA.Value         := DMCupomFiscal.dbTemp_NFCEHORA.Value;
          dmCupomFiscal.dbECFCOD_SIT.Value      := DMCupomFiscal.dbTemp_NFCECOD_SIT.Value;
          dmCupomFiscal.dbECFINDPAG.Value       := DMCupomFiscal.dbTemp_NFCEINDPAG.Value;
          dmCupomFiscal.dbECFTPIMP.Value        := DMCupomFiscal.dbTemp_NFCETPIMP.Value;
          dmCupomFiscal.dbECFTPEMIS.Value       := DMCupomFiscal.dbTemp_NFCETPEMIS.Value;
          dmCupomFiscal.dbECFTPAMB.Value        := DMCupomFiscal.dbTemp_NFCETPAMB.Value;
          dmCupomFiscal.dbECFINDPRES.Value      := DMCupomFiscal.dbTemp_NFCEINDPRES.Value;
          dmCupomFiscal.dbECFVERPROC.Value      := Copy(frmPrincipal.barStatus.Panels[0].Text,
            Pos(frmPrincipal.barStatus.Panels[0].Text, 'Vers�o:') + 9, 11); //Versao aplicativo emissor de NFe
          dmCupomFiscal.dbECFINDIEDEST.Value    := DMCupomFiscal.dbTemp_NFCEINDIEDEST.Value;
          dmCupomFiscal.dbECFMODFRETE.Value     := DMCupomFiscal.dbTemp_NFCEMODFRETE.Value;
          DMCupomFiscal.dbECFCCUSTO.Value       := dmCupomFiscal.dbTemp_NFCECCUSTO.Value;
          DMCupomFiscal.dbECFOPERACAO.Value     := dmCupomFiscal.dbTemp_NFCEOPERACAO.Value;
          DMCupomFiscal.dbECFESTOQUE.Value      := dmCupomFiscal.dbTemp_NFCEESTOQUE.Value;
          DMCupomFiscal.dbECFFORMA_PAG.Value    := dmCupomFiscal.dbTemp_NFCEFORMA_PAG.Value;
          DMCupomFiscal.dbECFFUNCIONARIO.Value  := dmCupomFiscal.dbTemp_NFCEFUNCIONARIO.Value;
          DMCupomFiscal.dbECFFUN_USUARIO.Value  := dmCupomFiscal.dbTemp_NFCEFUN_USUARIO.Value;
          DMCupomFiscal.dbECFTURNO.Value        := dmCupomFiscal.dbTemp_NFCETURNO.Value;
          DMCupomFiscal.dbECFCLIFOR.Value       := DMCupomFiscal.dbTemp_NFCECLIFOR.Value;
          DMCupomFiscal.dbECFCF_NOME.Value      := DMCupomFiscal.dbTemp_NFCECF_NOME.Value;
          DMCupomFiscal.dbECFCF_ENDE.Value      := DMCupomFiscal.dbTemp_NFCECF_ENDE.Value;
          DMCupomFiscal.dbECFCF_CNPJ_CPF.Value  := DMCupomFiscal.dbTemp_NFCECF_CNPJ_CPF.Value;
          DMCupomFiscal.dbECFCF_IE.Value        := DMCupomFiscal.dbTemp_NFCECF_IE.Value;
          dmCupomFiscal.dbECFCF_NUMERO_END.Value:= DMCupomFiscal.dbTemp_NFCECF_NUMERO_END.Value;
          dmCupomFiscal.dbECFCF_MUNICIPIO.Value := DMCupomFiscal.dbTemp_NFCECF_MUNICIPIO.Value;
          dmCupomFiscal.dbECFCF_CEP.Value       := DMCupomFiscal.dbTemp_NFCECF_CEP.Value;
          dmCupomFiscal.dbECFCF_BAIRRO.Value    := DMCupomFiscal.dbTemp_NFCECF_BAIRRO.Value;
          DMCupomFiscal.dbECFCF_KM.Value        := DMCupomFiscal.dbTemp_NFCECF_KM.Value;
          DMCupomFiscal.dbECFCF_Placa.Value     := DMCupomFiscal.dbTemp_NFCECF_PLACA.Value;
          DMCupomFiscal.dbECFSTATUS.Value       := 'X';
          DMCupomFiscal.dbECFVLR_DINHEIRO.Value := 0;
          DMCupomFiscal.dbECFVLR_TIKET.Value    := 0;
          DMCupomFiscal.dbECFVLR_CHEQUE.Value   := 0;
          DMCupomFiscal.dbECFVLR_CARTAO.Value   := 0;
          DMCupomFiscal.dbECFVLR_CONVENIO.Value := 0;
          DMCupomFiscal.dbECFVLR_SUBTOTAL.Value := DMCupomFiscal.dbTemp_NFCEVLR_SUBTOTAL.Value;
          DMCupomFiscal.dbECFVLR_TOTAL.Value    := DMCupomFiscal.dbTemp_NFCEVLR_TOTAL.Value;
          DMCupomFiscal.dbECFVLR_DESCONTO.Value := DMCupomFiscal.dbTemp_NFCEVLR_DESCONTO.Value;
          dmCupomFiscal.dbECFVLR_RECEBIDO.Value := DMCupomFiscal.dbTemp_NFCEVLR_RECEBIDO.Value;
          dmCupomFiscal.dbECFVLR_TROCO.Value    := DMCupomFiscal.dbTemp_NFCEVLR_TROCO.Value;
          dmCupomFiscal.dbECFVLR_BC_ICM.Value   := DMCupomFiscal.dbTemp_NFCEVLR_BC_ICM.Value;
          dmCupomFiscal.dbECFVLR_ICM.Value      := DMCupomFiscal.dbTemp_NFCEVLR_ICM.Value;
          dmCupomFiscal.dbECFVLR_FRETE.Value    := DMCupomFiscal.dbTemp_NFCEVLR_FRETE.Value;
          dmCupomFiscal.dbECFVLR_SEG.Value      := DMCupomFiscal.dbTemp_NFCEVLR_SEG.Value;
          dmCupomFiscal.dbECFVLR_BC_PIS.Value   := DMCupomFiscal.dbTemp_NFCEVLR_BC_PIS.Value;
          dmCupomFiscal.dbECFVLR_PIS.Value      := DMCupomFiscal.dbTemp_NFCEVLR_PIS.Value;
          dmCupomFiscal.dbECFVLR_BC_COFINS.Value:= DMCupomFiscal.dbTemp_NFCEVLR_BC_COFINS.Value;
          dmCupomFiscal.dbECFVLR_COFINS.Value   := DMCupomFiscal.dbTemp_NFCEVLR_COFINS.Value;
          dmCupomFiscal.dbECFVLR_OUTRO.Value    := DMCupomFiscal.dbTemp_NFCEVLR_OUTRO.Value;
          dmCupomFiscal.dbECFINFADFISCO.Value   := DMCupomFiscal.dbTemp_NFCEINFADFISCO.Value;
          dmCupomFiscal.dbECFINFCPL.Value       := IfThen(Trim(DMCupomFiscal.dbTemp_NFCEINFCPL.AsString)='', '', DMCupomFiscal.dbTemp_NFCEINFCPL.AsString+';')+Obs;
          dmCupomFiscal.dbECFENVIADO.Value      := 'N';
          dmCupomFiscal.dbECFVLR_ISSQN.value    := DMCupomFiscal.dbTemp_NFCEVLR_ISSQN.Value;
          dmCupomFiscal.dbECFVLR_BC_ISSQN.value := DMCupomFiscal.dbTemp_NFCEVLR_BC_ISSQN.Value;
          dmCupomFiscal.dbECFVLR_SERVICO.Value  := DMCupomFiscal.dbTemp_NFCEVLR_SERVICO.value;
          dmCupomFiscal.dbECFVLR_PRODUTOS.Value := DMCupomFiscal.dbTemp_NFCEVLR_PRODUTOS.value;
          dmCupomFiscal.dbECFCONVENIO.Value     := DMCupomFiscal.dbTemp_NFCECONVENIO.value;
          dmCupomFiscal.dbECFPER_COMISSAO.Value := DMCupomFiscal.dbTemp_NFCEPER_COMISSAO.value;
          dmCupomFiscal.dbECFVLR_COMISSAO.Value := DMCupomFiscal.dbTemp_NFCEVLR_COMISSAO.value;
          dmCupomFiscal.dbECFCF_MOTORISTA.Value := DMCupomFiscal.dbTemp_NFCECF_MOTORISTA.value;
          dmCupomFiscal.dbECFCHAVE_NFCE.Value   := MontaChaveAcessoNFe_v2(
                  UFparaCodigo(frmNFCe.LOCAL_UF),                 //Codigo UF
                  dmCupomFiscal.dbTemp_NFCEDATA.Value,            //Data
                  OnlyNumber(frmNFCe.LOCAL_CNPJ),                 //CNPJ
                  dmCupomFiscal.dbTemp_NFCEMODELO_DOC.AsInteger,  //Modelo Doc
                  dmCupomFiscal.dbTemp_NFCESERIE.AsInteger,       //Serie
                  numero,                                         //Numero NF
                  dmCupomFiscal.dbTemp_NFCETPEMIS.AsInteger,      //Tipo Emissao
                  numero                                          //Codigo Numerico
          );
          if DMCupomFiscal.dbTemp_NFCEEL_CHAVE.AsInteger > 0 then //Indica que possui EL_CHAVE usado pre-venda
            DMCupomFiscal.dbECFEL_CHAVE.Value := DMCupomFiscal.dbTemp_NFCEEL_CHAVE.Value
          else
            DMCupomFiscal.dbECFEL_CHAVE.Value := gerarELChave;


          IF Length(Trim(dmCupomFiscal.dbECFCHAVE_NFCE.Value)) < 44 then
          begin
            msgAviso('Chave de acesso da NFC-e inv�lida '+dmCupomFiscal.dbECFCHAVE_NFCE.Value,'Aviso');
            Result := False;
          end;
          DMCupomFiscal.dbECF.Post;
          ID := DMCupomFiscal.dbECFID.Value;
          el_chave := DMCupomFiscal.dbECFEL_CHAVE.Value;
          DMCupomFiscal.dbECF.Close;
        except
          on E:Exception do
          begin
            DMCupomFiscal.Transaction.RollbackRetaining;
            Result := False;
            raise Exception.Create('[EST_ECF] - Erro ao gravar NFC-e na base de dados.'+#13+e.Message);
          end;
        end;

        //--- Grava Itens NFCE
        try
          dmCupomFiscal.dbTemp_NFCE_Item.First;
          dmCupomFiscal.dbECF_Item.Open;
          While not(DMCupomFiscal.dbTemp_NFCE_Item.Eof) do
          begin
            DMCupomFiscal.dbECF_Item.Insert;
            DMCupomFiscal.dbECF_ItemID_ECF.Value      := ID;
            DMCupomFiscal.dbECF_ItemITEM.Value        := DMCupomFiscal.dbTemp_NFCE_ItemITEM.AsString;
            DMCupomFiscal.dbECF_ItemMERCADORIA.Value  := DMCupomFiscal.dbTemp_NFCE_ItemMERCADORIA.AsString;
            DMCupomFiscal.dbECF_ItemQTD.Value         := DMCupomFiscal.dbTemp_NFCE_ItemQUANTIDADE.AsCurrency;
            DMCupomFiscal.dbECF_ItemQTD_CAN.Value     := DMCupomFiscal.dbTemp_NFCE_ItemQTD_CAN.AsCurrency;
            DMCupomFiscal.dbECF_ItemVLR.Value         := DMCupomFiscal.dbTemp_NFCE_ItemVLR_UNITARIO.AsCurrency;
            DMCupomFiscal.dbECF_ItemTOTAL.Value       := DMCupomFiscal.dbTemp_NFCE_ItemVLR_TOTAL.AsCurrency -
                                                          DMCupomFiscal.dbTemp_NFCE_ItemVLR_DESCONTO.AsCurrency;
            DMCupomFiscal.dbECF_ItemCEST.value        := DMCupomFiscal.dbTemp_NFCE_ItemCEST.AsString;
            DMCupomFiscal.dbECF_ItemICM.Value         := DMCupomFiscal.dbTemp_NFCE_ItemICM.AsString;
            DMCupomFiscal.dbECF_ItemVALOR_CUSTO.Value := DMCupomFiscal.dbTemp_NFCE_ItemVLR_CUSTO_ATUAL.AsCurrency;
            DMCupomFiscal.dbECF_ItemBico.Value        := DMCupomFiscal.dbTemp_NFCE_ItemBico.AsInteger;
            DMCupomFiscal.dbECF_ItemAbastecida.Value  := DMCupomFiscal.dbTemp_NFCE_ItemAbastecida.AsInteger;
            DMCupomFiscal.dbECF_ItemCST_PIS.Value     := DMCupomFiscal.dbTemp_NFCE_ItemCST_PIS.AsString;
            DMCupomFiscal.dbECF_ItemCST_COFINS.Value  := DMCupomFiscal.dbTemp_NFCE_ItemCST_COFINS.AsString;
            DMCupomFiscal.dbECF_ItemCST_ICM.Value     := DMCupomFiscal.dbTemp_NFCE_ItemCST_ICM.AsString;
            DMCupomFiscal.dbECF_ItemCFOP.Value        := DMCupomFiscal.dbTemp_NFCE_ItemCFOP.AsInteger;
            DMCupomFiscal.dbECF_ItemCFOP.Value        := DMCupomFiscal.dbTemp_NFCE_ItemCFOP.AsInteger;
            DMCupomFiscal.dbECF_ItemVLR_DESCONTO.Value:= DMCupomFiscal.dbTemp_NFCE_ItemVLR_DESCONTO.AsCurrency;
            DMCupomFiscal.dbECF_ItemVLR_DESCONTO_ITEM.Value := DMCupomFiscal.dbTemp_NFCE_ItemVLR_DESCONTO_ITEM.AsCurrency;
            DMCupomFiscal.dbECF_ItemINDTOT.Value      := DMCupomFiscal.dbTemp_NFCE_ItemINDTOT.Value;
            DMCupomFiscal.dbECF_ItemVLR_TOTTRIB.Value := DMCupomFiscal.dbTemp_NFCE_ItemVLR_TOTTRIB.AsCurrency;
            DMCupomFiscal.dbECF_ItemVLR_TOTTRIB_Mun.Value := DMCupomFiscal.dbTemp_NFCE_ItemVLR_TOTTRIB_Mun.AsCurrency;
            DMCupomFiscal.dbECF_ItemVLR_TOTTRIB_Est.Value := DMCupomFiscal.dbTemp_NFCE_ItemVLR_TOTTRIB_Est.AsCurrency;
            DMCupomFiscal.dbECF_ItemNCM.Value         := DMCupomFiscal.dbTemp_NFCE_ItemNCM.Value;
            DMCupomFiscal.dbECF_ItemUNIDADE.Value     := DMCupomFiscal.dbTemp_NFCE_ItemUNIDADE.Value;
            DMCupomFiscal.dbECF_ItemVLR_BC_ICM.Value  := DMCupomFiscal.dbTemp_NFCE_ItemVLR_BC_ICM.Value;
            DMCupomFiscal.dbECF_ItemALI_ICM.Value     := DMCupomFiscal.dbTemp_NFCE_ItemALI_ICM.Value;
            DMCupomFiscal.dbECF_ItemVLR_ICM.Value     := DMCupomFiscal.dbTemp_NFCE_ItemVLR_ICM.Value;
            DMCupomFiscal.dbECF_ItemVLR_SEG.Value     := DMCupomFiscal.dbTemp_NFCE_ItemVLR_SEG.Value;
            DMCupomFiscal.dbECF_ItemVLR_FRETE.Value   := DMCupomFiscal.dbTemp_NFCE_ItemVLR_FRETE.Value;
            DMCupomFiscal.dbECF_ItemVLR_OUTRO.Value   := DMCupomFiscal.dbTemp_NFCE_ItemVLR_OUTRO.Value;
            DMCupomFiscal.dbECF_ItemVLR_BC_PIS.Value  := DMCupomFiscal.dbTemp_NFCE_ItemVLR_BC_PIS.Value;
            DMCupomFiscal.dbECF_ItemALI_PIS.Value     := DMCupomFiscal.dbTemp_NFCE_ItemALI_PIS.Value;
            DMCupomFiscal.dbECF_ItemVLR_PIS.Value     := DMCupomFiscal.dbTemp_NFCE_ItemVLR_PIS.Value;
            DMCupomFiscal.dbECF_ItemVLR_BC_COFINS.Value := DMCupomFiscal.dbTemp_NFCE_ItemVLR_BC_COFINS.Value;
            DMCupomFiscal.dbECF_ItemALI_COFINS.Value  := DMCupomFiscal.dbTemp_NFCE_ItemALI_COFINS.Value;
            DMCupomFiscal.dbECF_ItemVLR_COFINS.Value  := DMCupomFiscal.dbTemp_NFCE_ItemVLR_COFINS.Value;
            DMCupomFiscal.dbECF_ItemVLR_BC_ISSQN.Value := DMCupomFiscal.dbTemp_NFCE_ItemVLR_BC_ISSQN.Value;
            DMCupomFiscal.dbECF_ItemVLR_ISSQN.Value    := DMCupomFiscal.dbTemp_NFCE_ItemVLR_ISSQN.Value;
            DMCupomFiscal.dbECF_ItemALI_ISSQN.Value    := DMCupomFiscal.dbTemp_NFCE_ItemALI_ISSQN.value;
            dmCupomFiscal.dbECF_ItemVLR_SERVICO.value  := DMCupomFiscal.dbTemp_NFCE_ItemVLR_SERVICO.value;
            dmCupomFiscal.dbECF_ItemID_COMANDA.value   := DMCupomFiscal.dbTemp_NFCE_ItemID_COMANDA.value;

            dmCupomFiscal.dbECF_ItemAMPARO_ICM.value     := DMCupomFiscal.dbTemp_NFCE_ItemAMPARO_ICM.value;
            dmCupomFiscal.dbECF_ItemPRED_BC.value        := DMCupomFiscal.dbTemp_NFCE_ItemPRED_BC.value;
            dmCupomFiscal.dbECF_ItemMotDesICM.value      := DMCupomFiscal.dbTemp_NFCE_ItemMotDesICM.value;
            dmCupomFiscal.dbECF_ItemVlr_ICMDeson.value   := DMCupomFiscal.dbTemp_NFCE_ItemVlr_ICMDeson.value;
            dmCupomFiscal.dbECF_ItemVLR_COMISSAO.value   := DMCupomFiscal.dbTemp_NFCE_ItemVLR_COMISSAO.value;
            dmCupomFiscal.dbECF_ItemPER_COMISSAO.value   := DMCupomFiscal.dbTemp_NFCE_ItemPER_COMISSAO.value;
            dmCupomFiscal.dbECF_ItemANP_PGLP.AsCurrency  := DMCupomFiscal.dbTemp_NFCE_ItemANP_PGLP.AsCurrency;
            dmCupomFiscal.dbECF_ItemANP_PGNN.AsCurrency  := DMCupomFiscal.dbTemp_NFCE_ItemANP_PGNN.AsCurrency;
            dmCupomFiscal.dbECF_ItemANP_PGNI.AsCurrency  := DMCupomFiscal.dbTemp_NFCE_ItemANP_PGNI.AsCurrency;
            dmCupomFiscal.dbECF_ItemANP_VPART.AsCurrency := DMCupomFiscal.dbTemp_NFCE_ItemANP_VPART.AsCurrency;
            dmCupomFiscal.dbECF_ItemANP_CODIF.AsString   := DMCupomFiscal.dbTemp_NFCE_ItemANP_CODIF.AsString;

            if not(DMCupomFiscal.dbTemp_NFCE_ItemID_COMANDA.IsNull)Then
            begin
              dmCupomFiscal.ConfirmarPagamentoItemComanda(DMCupomFiscal.dbTemp_NFCE_ItemID_COMANDA.value,
                DMCupomFiscal.dbTemp_NFCE_ItemCOMANDA_ITEM.value,'S');
              if(dmCupomFiscal.VerificaComandaPaga(DMCupomFiscal.dbTemp_NFCE_ItemID_COMANDA.value))then
              begin
                dmCupomFiscal.MoverComandasInseridas(dmCupomFiscal.dbTemp_NFCE_ItemID_COMANDA.Value, el_chave);
                dmCupomFiscal.DeletarComandasPagas(dmCupomFiscal.dbTemp_NFCE_ItemID_COMANDA.Value);
              end;
            end;
            DMCupomFiscal.dbECF_Item.Post;

            //Para n�o alterar e/ou excluir abastecida quando o item esta cancelado
            if (DMCupomFiscal.dbTemp_NFCE_ItemQUANTIDADE.AsCurrency > 0) and (DMCupomFiscal.dbTemp_NFCE_ItemABASTECIDA.AsInteger > 0) then
            begin
              //---Abastecidas
              DMCupomFiscal.IBSQL1.Close;
              DMCupomFiscal.IBSQL1.SQL.Clear;
              If POSTO.Abastecidas_Del Then
                DMCupomFiscal.IBSQL1.SQL.Add('Update Est_Abastecimentos Set Status=''E'' Where (Id=:pId)')
              Else
                DMCupomFiscal.IBSQL1.SQL.Add('Update Est_Abastecimentos Set Status=''F'' Where (Id=:pId)');

              DMCupomFiscal.IBSQL1.ParamByName('pId').AsInteger := DMCupomFiscal.dbTemp_NFCE_ItemABASTECIDA.AsInteger;
              DMCupomFiscal.IBSQL1.ExecQuery;
              DMCupomFiscal.IBSQL1.Close;
            end;
            DMCupomFiscal.dbTemp_NFCE_Item.Next;
          end;
          DMCupomFiscal.dbECF_Item.Close;
          DMCupomFiscal.dbTemp_NFCE_Item.Close;
        except
          on E:Exception do
          begin
            Result := False;
            DMCupomFiscal.Transaction.RollbackRetaining;
            raise Exception.Create('[EST_ECF_ITEM] - Erro ao gravar itens na base de dados.'+#13+e.Message);
          end;
        end;

        //--- Gravar Formas de Pagamento da NFCe
        try
          dmCupomFiscal.dbECF_FormaPag.Open;
          dmCupomFiscal.getTemp_NFCE_FormaPag;
          DMCupomFiscal.dbTemp_NFCE_FormaPag.First;
          While not(DMCupomFiscal.dbTemp_NFCE_FormaPag.Eof) do
          begin
            if dmCupomFiscal.dbTemp_NFCE_FormaPagVPAG.Value > 0 then  //Somente Pagamentos que possui Valor >  0
            begin
              dmCupomFiscal.dbECF_FormaPag.Insert;
              dmCupomFiscal.dbECF_FormaPagID_ECF.Value     := ID;
              dmCupomFiscal.dbECF_FormaPagID.Value         := DMCupomFiscal.dbTemp_NFCE_FormaPagID.Value;
              dmCupomFiscal.dbECF_FormaPagVPAG.Value       := DMCupomFiscal.dbTemp_NFCE_FormaPagVPAG.Value;
              dmCupomFiscal.dbECF_FormaPagTPAG.Value       := DMCupomFiscal.dbTemp_NFCE_FormaPagTPAG.Value;
              dmCupomFiscal.dbECF_FormaPagCARD_CNPJ.Value  := DMCupomFiscal.dbTemp_NFCE_FormaPagCARD_CNPJ.Value;
              dmCupomFiscal.dbECF_FormaPagCARD_TBAND.Value := DMCupomFiscal.dbTemp_NFCE_FormaPagCARD_TBAND.Value;
              dmCupomFiscal.dbECF_FormaPagCARD_CAUT.Value  := DMCupomFiscal.dbTemp_NFCE_FormaPagCARD_CAUT.Value;
              dmCupomFiscal.dbECF_FormaPagFORMA_PAG.Value  := DMCupomFiscal.dbTemp_NFCE_FormaPagFORMA_PAG.Value;
              dmCupomFiscal.dbECF_FormaPagVTROCO.Value     := DMCupomFiscal.dbTemp_NFCE_FormaPagVTROCO.Value;
              dmCupomFiscal.dbECF_FormaPagINDICE_ECF.Value := DMCupomFiscal.dbTemp_NFCE_FormaPagTPAG.Value; //Lauro BIO
              DMCupomFiscal.dbECF_FormaPag.Post;
            end;
            DMCupomFiscal.dbTemp_NFCE_FormaPag.Next;
          end;
        except
          on E:Exception do
          begin
            Result := False;
            DMCupomFiscal.Transaction.RollbackRetaining;
            raise Exception.Create('[EST_ECF_FORMAPAG] - Erro ao gravar formas de pagamento na base de dados.'+#13+e.Message);
          end;
        end;

        //---Grava Cartas Frete
        try
          dmCupomFiscal.getTemp_NFCE_CFrete;
          if not(dmCupomFiscal.dbTemp_NFCe_FormaPag_CFrete.Eof) then
          begin
            dmCupomFiscal.dbCR_CartaFrete.Open;
            While not(dmCupomFiscal.dbTemp_NFCe_FormaPag_CFrete.Eof) do
            begin
              dmCupomFiscal.dbCR_CartaFrete.Insert;
              dmCupomFiscal.dbCR_CartaFreteCLIFOR.asInteger :=
                dmCupomFiscal.dbTemp_NFCe_FormaPag_CFreteCLIFOR.AsInteger;
              dmCupomFiscal.dbCR_CartaFreteNUMERO.asInteger :=
                dmCupomFiscal.dbTemp_NFCe_FormaPag_CFreteNUMERO.AsInteger;
              dmCupomFiscal.dbCR_CartaFreteDATA_OPE.AsDateTime := Now;
              dmCupomFiscal.dbCR_CartaFreteDATA_VEN.AsDateTime :=
                dmCupomFiscal.dbTemp_NFCe_FormaPag_CFreteDATA_VEN.AsDateTime;
              dmCupomFiscal.dbCR_CartaFreteVALOR.AsCurrency :=
                dmCupomFiscal.dbTemp_NFCe_FormaPag_CFreteVALOR.AsCurrency;
              dmCupomFiscal.dbCR_CartaFreteFORMA_PAG.asInteger :=
                dmCupomFiscal.dbTemp_NFCe_FormaPag_CFreteFORMA_PAG.AsInteger;
              dmCupomFiscal.dbCR_CartaFreteFUNCIONARIO.asInteger :=
                dmCupomFiscal.dbTemp_NFCEFUNCIONARIO.Value;
              dmCupomFiscal.dbCR_CartaFreteCCUSTO.asInteger :=
                dmCupomFiscal.dbTemp_NFCECCUSTO.Value;
              dmCupomFiscal.dbCR_CartaFreteCCUSTO.asInteger :=
                dmCupomFiscal.dbTemp_NFCECCUSTO.Value;
              dmCupomFiscal.dbCR_CartaFreteEL_CHAVE.asInteger := el_chave;
              DMCupomFiscal.dbCR_CartaFrete.Post;

              dmCupomFiscal.dbTemp_NFCe_FormaPag_CFrete.Next
            end;
            dmCupomFiscal.dbCR_CartaFrete.Open;
            //Lauro Gambiara pois nao apagar na trigger - Carta Frete
            dmCupomFiscal.dbTemp_NFCe_FormaPag_CFrete.First;
            while not (dmCupomFiscal.dbTemp_NFCe_FormaPag_CFrete.Eof) do
              dmCupomFiscal.dbTemp_NFCe_FormaPag_CFrete.Delete;
          end
        except
          on E:Exception do
          begin
            Result := False;
            DMCupomFiscal.Transaction.RollbackRetaining;
            raise Exception.Create('[CR_CARTA_FRETE] - Erro ao gravar formas de pagamento na base de dados.'+#13+e.Message);
          end;
        end;

        //---Grava Conta corrente
        try
          dmCupomFiscal.getTemp_NFCE_CCorrente;
          if not(dmCupomFiscal.dbTemp_NFCe_FormaPag_CCorrente.Eof) then
          begin
            dmCupomFiscal.dbCR_ContaCorrente.Open;
            While not(dmCupomFiscal.dbTemp_NFCe_FormaPag_CCorrente.Eof) do
            begin
              dmCupomFiscal.dbCR_ContaCorrente.Insert;
              dmCupomFiscal.dbCR_ContaCorrenteCLIFOR.asInteger :=
                dmCupomFiscal.dbTemp_NFCe_FormaPag_CCorrenteCLIFOR.asInteger;
              dmCupomFiscal.dbCR_ContaCorrenteDC.asString := 'D';
              dmCupomFiscal.dbCR_ContaCorrenteVALOR.AsCurrency :=
                dmCupomFiscal.dbTemp_NFCe_FormaPag_CCorrenteVALOR.AsCurrency;
              dmCupomFiscal.dbCR_ContaCorrenteID_CREDITO.asInteger :=
                dmCupomFiscal.dbTemp_NFCe_FormaPag_CCorrenteID_CREDITO.AsInteger;
              dmCupomFiscal.dbCR_ContaCorrenteEL_CHAVE.asInteger := el_chave;
              //Now porque sempre que � finalizado o NFCE a data � atualizada para a atual
              dmCupomFiscal.dbCR_ContaCorrenteDATA_OPE.AsDateTime := Now;
              dmCupomFiscal.dbCR_ContaCorrenteFUNCIONARIO.asInteger :=
                dmCupomFiscal.dbTemp_NFCEFUNCIONARIO.Value;
              DMCupomFiscal.dbCR_ContaCorrente.Post;
              dmCupomFiscal.dbTemp_NFCe_FormaPag_CCorrente.Next;
            end;
            dmCupomFiscal.dbCR_ContaCorrente.Close;

            //Lauro Gambiara pois nao apagar na trigger - Conta Corrente
            dmCupomFiscal.dbTemp_NFCe_FormaPag_CCorrente.First;
            while not (dmCupomFiscal.dbTemp_NFCe_FormaPag_CCorrente.Eof) do
              dmCupomFiscal.dbTemp_NFCe_FormaPag_CCorrente.Delete;
          end;
        except
          on E:Exception do
          begin
            Result := False;
            DMCupomFiscal.Transaction.RollbackRetaining;
            raise Exception.Create('[CR_CONTA_CORRENTE] - Erro ao gravar formas de pagamento na base de dados.'+#13+e.Message);
          end;
        end;

        //---Lauro Gravar Cheques da NFCe
        try
          dmCupomFiscal.getTemp_NFCE_Cheque;
          if not(dmCupomFiscal.dbTemp_NFCe_FormaPag_Cheque.Eof) then
          begin
            dmCupomFiscal.dbCh_Movimento.Open;
            While not(dmCupomFiscal.dbTemp_NFCe_FormaPag_Cheque.Eof) do
            begin
              dmCupomFiscal.dbCh_Movimento.Insert;
              dmCupomFiscal.dbCh_MovimentoBANCO.Value:= dmCupomFiscal.dbTemp_NFCe_FormaPag_ChequeBANCO.Value;
              dmCupomFiscal.dbCh_MovimentoCONTA.Value:= dmCupomFiscal.dbTemp_NFCe_FormaPag_ChequeCONTA.Value;
              dmCupomFiscal.dbCh_MovimentoAGENCIA.Value:= dmCupomFiscal.dbTemp_NFCe_FormaPag_ChequeAGENCIA.Value;
              dmCupomFiscal.dbCh_MovimentoCHEQUE.Value:= dmCupomFiscal.dbTemp_NFCe_FormaPag_ChequeCHEQUE.Value;
              dmCupomFiscal.dbCh_MovimentoSITUACAO.Value:=1;
              dmCupomFiscal.dbCh_MovimentoVALOR.Value:= dmCupomFiscal.dbTemp_NFCe_FormaPag_ChequeVALOR.Value;
              dmCupomFiscal.dbCh_MovimentoCONCILIADO.Value:= 'N';
              dmCupomFiscal.dbCh_MovimentoEMITIDO_RECEBIDO.Value:= 'R';
              dmCupomFiscal.dbCh_MovimentoCORRENTISTA.Value:= dmCupomFiscal.dbTemp_NFCe_FormaPag_ChequeCORRENTISTA.Value;
              dmCupomFiscal.dbCh_MovimentoCNPJCPF.Value:= dmCupomFiscal.dbTemp_NFCe_FormaPag_ChequeCNPJCPF.Value;
              dmCupomFiscal.dbCh_MovimentoDATA.Value:= Date;
              dmCupomFiscal.dbCh_MovimentoDTA_RECEBIMENTO.Value:= Date;
              dmCupomFiscal.dbCh_MovimentoDTA_VENCIMENTO.Value:= dmCupomFiscal.dbTemp_NFCe_FormaPag_ChequeDTA_VENCIMENTO.Value;
              dmCupomFiscal.dbCh_MovimentoEL_CHAVE.Value:= EL_chave;
              dmCupomFiscal.dbCh_MovimentoSTATUS.Value:='X';
              dmCupomFiscal.dbCh_MovimentoCLIFOR.Value:=dmCupomFiscal.dbTemp_NFCECLIFOR.AsInteger;
              dmCupomFiscal.dbCh_MovimentoCF_NOME.Value:=dmCupomFiscal.dbTemp_NFCECF_NOME.asString;
              //dmCupomFiscal.dbCh_MovimentoCX_HISTORICO.Value:=
              //dmCupomFiscal.dbCh_MovimentoCX_CONTA.Value:=
              dmCupomFiscal.dbCh_Movimento.Post;
              dmCupomFiscal.dbTemp_NFCe_FormaPag_Cheque.Next;
            end;
            dmCupomFiscal.dbCh_Movimento.Close;

            //Lauro Gambiara pois nao apagar na trigger - Cheque
            dmCupomFiscal.dbTemp_NFCe_FormaPag_Cheque.First;
            while not (dmCupomFiscal.dbTemp_NFCe_FormaPag_Cheque.Eof) do
              dmCupomFiscal.dbTemp_NFCe_FormaPag_Cheque.Delete;
          end;
        except
          on E:Exception do
          begin
            Result := False;
            DMCupomFiscal.Transaction.RollbackRetaining;
            raise Exception.Create('[EST_ECF_FORMAPAG_CHEQUE] - Erro ao gravar Cheques na base de dados.'+#13+e.Message);
          end;
        end;

        //-- Grava CR
        dmCupomFiscal.dbTemp_NFCE_FormaPag.First;
        while not(dmCupomFiscal.dbTemp_NFCE_FormaPag.eof) do //05 - Credito Loja
        begin
          if (dmCupomFiscal.dbTemp_NFCE_FormaPagTPAG.Value = '05')then  //Ja est�o inseridos somente os pagamentos com valor > 0
          begin
            if(dmCupomFiscal.dbTemp_NFCE_FormaPagVPAG.Value > 0)then
            begin
              //--- Gravar Faturas da NFCe
              try
                dmCupomFiscal.dbECF_Fatura.Open;
                dmCupomFiscal.dbTemp_NFCE_Fatura.First;
                While not(dmCupomFiscal.dbTemp_NFCE_Fatura.Eof) do
                begin
                  dmCupomFiscal.dbECF_Fatura.Insert;
                  dmCupomFiscal.dbECF_FaturaID_ECF.value  := ID;
                  dmCupomFiscal.dbECF_FaturaDATA.value    := DMCupomFiscal.dbTemp_NFCE_FaturaData.Value;
                  dmCupomFiscal.dbECF_FaturaVALOR.value   := DMCupomFiscal.dbTemp_NFCE_FaturaValor.Value;
                  dmCupomFiscal.dbECF_FaturaNDUP.value    := IntToStr(numero)+'/'+DMCupomFiscal.dbTemp_NFCE_FaturaPARCELA.AsString;
                  dmCupomFiscal.dbECF_FaturaPARCELA.value := DMCupomFiscal.dbTemp_NFCE_FaturaPARCELA.Value;
                  DMCupomFiscal.dbECF_Fatura.Post;
                  DMCupomFiscal.dbTemp_NFCE_Fatura.Next;
                end;
                DMCupomFiscal.dbECF_Fatura.Close;
                //Esse break � porque ja percorreu todas as faturas geradas, se percorrer de novo da viola�ao de chave...
                Break;
              except
                on E:Exception do
                begin
                  Result := False;
                  DMCupomFiscal.Transaction.RollbackRetaining;
                  raise Exception.Create('[EST_ECF_FATURA] - Erro ao gravar faturas na base de dados.'+#13+e.Message);
                end;
              end;
            end;
          end;
          dmCupomFiscal.dbTemp_NFCE_FormaPag.Next;
        end;  //--- Lan�ar  Contas a Receber --- Lauro ver Trigger

        try
          //verifica se ja foi lan�ado pelo pedido
          DMCupomFiscal.dbQuery2.Active:=False;
          DMCupomFiscal.dbQuery2.SQL.Clear;
          DMCupomFiscal.dbQuery2.SQL.Add('Select ID FROM EST_PEDIDOS Where EL_CHAVE=:pChave');
          DMCupomFiscal.dbQuery2.ParamByName('pChave').AsInteger:=dmCupomFiscal.dbTemp_NFCEEL_CHAVE.Value;
          DMCupomFiscal.dbQuery2.Active:=True;
          DMCupomFiscal.dbQuery2.First;
          if not(DMCupomFiscal.dbQuery2.IsEmpty)then
          begin
            DMCupomFiscal.dbQuery1.Active:=False;
            DMCupomFiscal.dbQuery1.SQL.Clear;
            DMCupomFiscal.dbQuery1.SQL.Add('Select * FROM CR_MOVIMENTO Where NDOC=:pCod and SDOC=''PE'' ');
            DMCupomFiscal.dbQuery1.ParamByName('pCod').AsInteger:=DMCupomFiscal.dbQuery2.FieldByName('ID').AsInteger;
            DMCupomFiscal.dbQuery1.Active:=True;
            DMCupomFiscal.dbQuery1.First;
            if(DMCupomFiscal.dbQuery1.isEmpty)then
              txtParcelas := gerarCR(el_chave, numero);
          end
          else
            txtParcelas := gerarCR(el_chave, numero);
        except
          on E:Exception do
          begin
            Result := False;
            DMCupomFiscal.Transaction.RollbackRetaining;
            raise Exception.Create('[CR_MOVIMENTO] - Erro ao gerar lan�amento no Contas a Receber.'+#13+e.Message);
          end;
        end;

        if(getInformaSaldoDevedor)Then
        begin
          if(dmCupomFiscal.dbTemp_NFCECLIFOR.AsInteger > 0)then
          begin
            lSaldoDevedor := getSaldoDevedor;
            if(lSaldoDevedor > 0)Then
            begin
              dmCupomFiscal.IBSQL1.Close;
              dmCupomFiscal.IBSQL1.SQL.Clear;
              dmCupomFiscal.IBSQL1.SQL.Add('UPDATE EST_ECF SET INFCPL = INFCPL || :pINFCPL where ID = :pID');
              dmCupomFiscal.IBSQL1.ParamByName('pINFCPL').asString := ';Saldo devedor em '+
                FormatDateTime('dd/mm/yyyy',now)+': '+ FormatFloat('R$ ###,##0.00',lSaldoDevedor);
              dmCupomFiscal.IBSQL1.ParamByName('pID').asInteger := ID;
              dmCupomFiscal.IBSQL1.ExecQuery;
              dmCupomFiscal.IBSQL1.Close;
              dmCupomFiscal.Transaction.CommitRetaining;
            end;
          end;
        end;

        //Gambiara pois nao apagar na trigger
        while not (dmCupomFiscal.dbTemp_NFCE_Fatura.Eof) do
          dmCupomFiscal.dbTemp_NFCE_Fatura.Delete;
        //Deletar dados das tabelas TEMP_NFCE
        try
          if not (DMCupomFiscal.Transaction.Active) then
            DMCupomFiscal.Transaction.Active:=True;

          DMCupomFiscal.IBSQL1.Close;
          DMCupomFiscal.IBSQL1.SQL.Clear;
          DMCupomFiscal.IBSQL1.SQL.Add('Delete From temp_nfce Where Caixa=:pCaixa');
          DMCupomFiscal.IBSQL1.ParamByName('pCaixa').AsString := FormatFloat('0000',FRENTE_CAIXA.Caixa);
          DMCupomFiscal.IBSQL1.ExecQuery;
        except
          on E:Exception do
          begin
            Result := False;
            logErros(Self, caminhoLog,'Erro ao limpar dados tempor�rios da NFC-e.'+#13+e.Message, 'Erro ao limpar tabelas tempor�rias da NFC-e','S',E);
          end;
        end;

        DMCupomFiscal.Transaction.CommitRetaining;
        if frmLoja <> nil then //Loja
        begin
          frmLoja.edtCliente.Text     := '';
          frmLoja.edtClienteNome.Text := '';
          TAB_PRECO.TabPreco:=0;
        end
        else
        begin
          if frmPosto <> nil then
          begin
            frmPosto.edtCliente.Text    := '';
            frmPosto.edtClienteNome.Text:= '';
            TAB_PRECO.TabPreco:=0;
          end;
        end;

        try
        //--------------- Gerar NFe vinculada ao Cupom ---------------
          If pNF then
          begin
            If msgPergunta('Deseja gerar Nota Fiscal ?','') then
            begin
              if not(Gera_NFe(ID, NF_OPERACAO)) then
                Abort;
            end;
          end;
        except
          on E:Exception do
          begin
            Result := False;
            logErros(Self, caminhoLog,'Erro ao Gerar NF-e vinculada ao NFC-e.'+#13+e.Message, 'Erro ao Gerar NF-e vinculada ao NFC-e','S',E);
          end;
        end;
      Except
        on e:Exception do
        begin
          Result := False;
          dmCupomFiscal.Transaction.RollbackRetaining;
          logErros(Self, caminhoLog,'Erro ao finalizar NFC-e.'+#13+e.Message, 'Erro ao finalizar NFC-e','S',E);
        end;
      end;
    end
    else
      Result := false;
  end
  else
    Result := false;
end;

function TCtrlFim.GerarCR(el_Chave, numero: Integer): String;
var
  txtParcelas : String;
begin
  DMCupomFiscal.dbCR.Open;
  DMCupomFiscal.dbTemp_NFCE_Fatura.First;
  txtParcelas := '';
  While not(DMCupomFiscal.dbTemp_NFCE_Fatura.Eof) do
  begin
    txtParcelas := txtParcelas + DMCupomFiscal.dbTemp_NFCE_FaturaData.Text+'          '+DMCupomFiscal.dbTemp_NFCE_FaturaValor.Text+#13+#10;
    DMCupomFiscal.dbCR.Insert;
    DMCupomFiscal.dbCREL_CHAVE.value    := el_chave;
    DMCupomFiscal.dbCRSDOC.value        := dmCupomFiscal.dbTemp_NFCESERIE.Value;
    DMCupomFiscal.dbCRNDOC.Value        := numero;
    DMCupomFiscal.dbCRPARCELA.Value     := DMCupomFiscal.dbTemp_NFCE_FaturaPARCELA.Value;;
    DMCupomFiscal.dbCRCLIFOR.Value      := dmCupomFiscal.dbTemp_NFCECLIFOR.Value;
    DMCupomFiscal.dbCRFUNCIONARIO.Value := dmCupomFiscal.dbTemp_NFCEFUNCIONARIO.Value; //LOGIN.usuarioCod;  >Lauro Machado e Filho 13.06.18 N�o Leva o Vendedor quando Mudar ao Finalizar
    DMCupomFiscal.dbCRFORMA_PAG.Value   := dmCupomFiscal.dbTemp_NFCE_FormaPagFORMA_PAG.Value;
    DMCupomFiscal.dbCRCCUSTO.Value      := dmCupomFiscal.dbTemp_NFCECCUSTO.Value;
    DMCupomFiscal.dbCRDATA_OPE.Value    := EL_Data;
    DMCupomFiscal.dbCRDATA_VEN.Value    := DMCupomFiscal.dbTemp_NFCE_FaturaData.Value;
    DMCupomFiscal.dbCRVALOR.Value       := DMCupomFiscal.dbTemp_NFCE_FaturaValor.Value;
    DMCupomFiscal.dbCRRESTA.Value       := DMCupomFiscal.dbTemp_NFCE_FaturaValor.Value;
    DMCupomFiscal.dbCROBSERVACAO.Value  := FormatFloat('0000',FRENTE_CAIXA.Caixa)+'/NFCE/'+IntToStr(numero);
    DMCupomFiscal.dbCR.Post;
    DMCupomFiscal.dbTemp_NFCE_Fatura.Next;
  end;
  DMCupomFiscal.dbCR.Close;
  Result := txtParcelas;
end;

function TCtrlFim.gerarParcelas: Boolean;
var
  vlrTotalParcelas: Currency;
begin
  Result := True;
  //Para gerar parcelas venda a prazo
  frmGerarParcelas:=TfrmGerarParcelas.create(Application);
  try
    try
      case frmGerarParcelas.ShowModal of
      mrOK:begin
          vlrTotalParcelas:=frmGerarParcelas.re_vl_T_Parcelas.Value;
          //verifica se cliente gerou alguma parcela
          if not(DMCupomFiscal.dbTemp_NFCE_Fatura.IsEmpty)then
          begin
            if dmCupomFiscal.GetTemp_Valor_Crediario <> vlrTotalParcelas then
            begin
              msgInformacao('Valor pago com "Cr�dito da Loja" n�o pode ser diferente do total das Parcelas','Informa��o');
              Result := false;
            end;
          end;
          if not(dmCupomFiscal.dbTemp_NFCE.State in [dsEdit]) then
            dmCupomFiscal.dbTemp_NFCE.Edit;
          dmCupomFiscal.dbTemp_NFCEINDPAG.Value:='1'; //Prazo
          dmCupomFiscal.dbTemp_NFCE.Post;
        end;
      mrCancel:begin
          dmCupomFiscal.dbTemp_NFCE_Fatura.Cancel;
          dmCupomFiscal.dbTemp_NFCE_Fatura.Close;
          dmCupomFiscal.Transaction.RollbackRetaining;
          Result := False;
        end;
      end;
    except
      Result := False;
    end;
  finally
    frmGerarParcelas.Free;
  end;
end;

function TCtrlFim.Gera_NFe(ID, NF_Operacao: Integer): Boolean;
Var Fat_Parcela:Integer;
    xDup_VLiq:Currency;
    Item:Integer;
begin
  Result := True;
  Fat_Parcela:=0;
  xDup_VLiq:=0;
  Item:=0;

  //Verifica se cliente possue TEMP_EST_NF
  dmCupomFiscal.dbQuery1.Active:=False;
  dmCupomFiscal.dbQuery1.SQL.Clear;
  dmCupomFiscal.dbQuery1.SQL.Add('Select Count(*) as xFlag From temp_est_Nf Where Clifor=:pCliFor');
  dmCupomFiscal.dbQuery1.ParamByName('pCliFor').AsInteger:=dmCupomFiscal.dbTemp_NFCECLIFOR.AsInteger;
  dmCupomFiscal.dbQuery1.Active:=True;
  dmCupomFiscal.dbQuery1.First;
  If (dmCupomFiscal.dbQuery1.FieldByName('xFlag').AsInteger > 0) then
  begin
    //----------------- Exclue Temp_Est_NF -----------------
    If msgPergunta('Existe uma Nota Fiscal em digita��o!'+#13+
                   'Deseja descartar a Nota Fiscal em digita��o para incluir este Cupom Fiscal ?', '') then
    begin
      dmCupomFiscal.IBSQL1.Close;
      dmCupomFiscal.IBSQL1.SQL.Clear;
      dmCupomFiscal.IBSQL1.SQL.Add('Delete From temp_Est_NF Where CliFor=:pCliFor');
      dmCupomFiscal.IBSQL1.ParamByName('pClifor').asInteger:=dmCupomFiscal.dbTemp_NFCECLIFOR.AsInteger;;
      dmCupomFiscal.IBSQL1.ExecQuery;
      dmCupomFiscal.IBSQL1.Close;
    end
    Else
    begin
      dmCupomFiscal.dbQuery1.Active:=False;
      Result := False;
    end
  end;
  //----------------- Gera Nota Fiscal -----------------
  try
    dmCupomFiscal.dbQuery1.Active:=False;
    dmCupomFiscal.dbQuery1.SQL.Clear;
    dmCupomFiscal.dbQuery1.SQL.Add('Select * From Est_ECF Where ID=:pID');
    dmCupomFiscal.dbQuery1.ParamByName('pID').AsInteger:=ID;
    dmCupomFiscal.dbQuery1.Active:=True;
    dmCupomFiscal.dbQuery1.First;

    //Insert Est_NF
    dmCupomFiscal.db_Temp_Est_NF.Open;
    dmCupomFiscal.db_Temp_Est_NF.Insert;
    dmCupomFiscal.db_Temp_Est_NFCLIFOR.Value    := dmCupomFiscal.dbQuery1.FieldByName('CliFor').AsInteger;
    dmCupomFiscal.db_Temp_Est_NFCLIFOR_IE.Value := dmCupomFiscal.dbQuery1.FieldByName('CF_IE').AsString;
    dmCupomFiscal.db_Temp_Est_NFCCUSTO.Value    := dmCupomFiscal.dbQuery1.FieldByName('CCusto').AsInteger;
    dmCupomFiscal.db_Temp_Est_NFOPERACAO.Value  := NF_Operacao;
    dmCupomFiscal.db_Temp_Est_NFES.Value        := 'S'; //??
    dmCupomFiscal.db_Temp_Est_NFESTOQUE.Value   := 'N'; //??
    dmCupomFiscal.db_Temp_Est_NFROYALTIES.Value := 'X';
    dmCupomFiscal.db_Temp_Est_NFFUNCIONARIO.Value := dmCupomFiscal.dbQuery1.FieldByName('Funcionario').AsInteger;
    dmCupomFiscal.db_Temp_Est_NFFUN_USUARIO.Value := dmCupomFiscal.dbQuery1.FieldByName('FUN_USUARIO').AsInteger;
    dmCupomFiscal.db_Temp_Est_NFFORMA_PAG.Value   := 199; //??
    dmCupomFiscal.db_Temp_Est_NFCONDICAO_PAG.Value:= 0; //??
    dmCupomFiscal.db_Temp_Est_NFDATA_Mov.Value  := Date;
    dmCupomFiscal.db_Temp_Est_NFDATA.Value      := Date;
    dmCupomFiscal.db_Temp_Est_NFDATAE.Value     := Date;
    dmCupomFiscal.db_Temp_Est_NFHORAE.Value     := Time;
    dmCupomFiscal.db_Temp_Est_NFSerie.Value     := dmcupomFiscal.getSerieNFe;
    if dmCupomFiscal.db_Temp_Est_NFSerie.Value = '0' then
      msgInformacao('S�rie informada � inv�lida','Informa��o');
    dmCupomFiscal.db_Temp_Est_NFNUMERO.Value    := 0;
    dmCupomFiscal.db_Temp_Est_NFIND_Emitente.Value  := '0';  //--0-Proprio, 1-Terceiro
    dmCupomFiscal.db_Temp_Est_NFModelo_Doc.Value    := '55';
    dmCupomFiscal.db_Temp_Est_NFFINNFE.value    := '1';
    dmCupomFiscal.db_Temp_Est_NFSTATUS.Value    := '0';

    dmCupomFiscal.db_Temp_Est_NFCOD_SIT.Value   := '08';
    dmCupomFiscal.db_Temp_Est_NFNUMERO_NFREF.Value := 0;
    dmCupomFiscal.db_Temp_Est_NFROMANEIO.Value  := 0;
    dmCupomFiscal.db_Temp_Est_NFORCAMENTO.Value := 0;
    dmCupomFiscal.db_Temp_Est_NFCONTRATO.Value  := 0;
    dmCupomFiscal.db_Temp_Est_NFRECIBO.Value    := 0;
    dmCupomFiscal.db_Temp_Est_NFORDEM_CARGA.Value := 0;
    dmCupomFiscal.db_Temp_Est_NFLAN_MANUAL.value := 'N';

    //dmCupomFiscal.db_Temp_Est_NFFAT_NFAT.Value:= //Tg
    dmCupomFiscal.db_Temp_Est_NFDUP_VLIQ.Value  := xDup_VLiq; //?? Cupom
    dmCupomFiscal.db_Temp_Est_NFTR_FRETE.Value  := '9'; //9-Sem Frete
    dmCupomFiscal.db_Temp_Est_NFDA_OBSERVACAO.Value := 'NFC-e:'+dmCupomFiscal.dbQuery1.FieldByName('Numero').Text+
                                                       '   Data:'+dmCupomFiscal.dbQuery1.FieldByName('Data').Text+';'+
                                                       IfThen(dmCupomFiscal.dbQuery1.FieldByName('CF_PLACA').asString<>'',
                                                       'Placa:'+dmCupomFiscal.dbQuery1.FieldByName('CF_PLACA').asString+'           ','')+
                                                       IfThen(dmCupomFiscal.dbQuery1.FieldByName('CF_KM').asString<>'',
                                                       'KM Atual:'+dmCupomFiscal.dbQuery1.FieldByName('CF_KM').asString,'');

    dmCupomFiscal.db_Temp_Est_NF.Post;
    dmCupomFiscal.db_Temp_Est_NF.Close;

    // Item
    dmCupomFiscal.dbQuery2.Active:=False;
    dmCupomFiscal.dbQuery2.SQL.Clear;
    dmCupomFiscal.dbQuery2.SQL.Add('Select * From Est_ECF_Item Where ID_ECF=:pID');
    dmCupomFiscal.dbQuery2.ParamByName('pID').asInteger:=ID;
    dmCupomFiscal.dbQuery2.Active:=True;
    dmCupomFiscal.dbQuery2.First;
    while not(dmCupomFiscal.dbQuery2.EOf) do
    begin
      Inc(Item);
      dmCupomFiscal.db_temp_Est_NF_item.Open;
      dmCupomFiscal.db_temp_Est_NF_item.Insert;
      dmCupomFiscal.db_temp_Est_NF_itemITEM.Value       := Item;
      dmCupomFiscal.db_temp_Est_NF_itemCLIFOR.Value     := dmCupomFiscal.dbQuery1.FieldByName('CliFor').AsInteger;
      dmCupomFiscal.db_temp_Est_NF_itemMERCADORIA.Value := dmCupomFiscal.dbQuery2.FieldByName('Mercadoria').AsString;
      dmCupomFiscal.db_temp_Est_NF_itemVLR_UNITARIO.AsVariant := dmCupomFiscal.dbQuery2.FieldByName('Vlr').AsCurrency;
      dmCupomFiscal.db_temp_Est_NF_itemQUANTIDADE.Value   := dmCupomFiscal.dbQuery2.FieldByName('Qtd').AsCurrency;
      dmCupomFiscal.db_temp_Est_NF_itemVLR_DESCONTO.Value := dmCupomFiscal.dbQuery2.FieldByName('Vlr_Desconto_Item').AsCurrency+dmCupomFiscal.dbQuery2.FieldByName('Vlr_Desconto').AsCurrency;
      dmCupomFiscal.db_temp_Est_NF_itemUNIDADE.Value      := 'UN'; //Tg Ajsuta
      dmCupomFiscal.db_Temp_Est_NF_ItemSERVICO.Value      := 'N'; //Tg Ajsuta
      dmCupomFiscal.db_Temp_Est_NF_ItemVLR_VENDA_ATUALIZAR.Value := 0;
      dmCupomFiscal.db_Temp_Est_NF_ItemOBSERVACAO.Value   :='NFC-e:'+dmCupomFiscal.dbQuery1.FieldByName('Numero').Text+'   Data:'+dmCupomFiscal.dbQuery1.FieldByName('Data').Text;
      dmCupomFiscal.db_Temp_Est_NF_ItemVLR_FRETE.Value    := 0;  //??
      dmCupomFiscal.db_Temp_Est_NF_ItemVLR_SEGURO.Value   := 0; //??
      dmCupomFiscal.db_Temp_Est_NF_ItemVLR_OUTRAS.Value   := 0; //??
      dmCupomFiscal.db_Temp_Est_NF_ItemVLR_SACA.Value     := 0;   //??

      dmCupomFiscal.db_Temp_Est_NF_ItemALI_II.Value       := 0;   //??
      dmCupomFiscal.db_Temp_Est_NF_ItemVLR_BC_II.Value    := 0;   //??
      dmCupomFiscal.db_Temp_Est_NF_ItemVLR_II.Value       := 0;   //??

      dmCupomFiscal.db_Temp_Est_NF_ItemALI_IPI.Value      := 0;   //??
      dmCupomFiscal.db_Temp_Est_NF_ItemALI_ICM.Value      := 0;   //??
      dmCupomFiscal.db_Temp_Est_NF_ItemALI_ICMS.Value     := 0;  //??

      dmCupomFiscal.db_Temp_Est_NF_ItemVLR_ICM.Value      := 0;   //??
      dmCupomFiscal.db_Temp_Est_NF_ItemVLR_ICMS.Value     := 0;   //??
      dmCupomFiscal.db_Temp_Est_NF_ItemVLR_ICM_SN.Value   := 0;   //??

      dmCupomFiscal.db_Temp_Est_NF_ItemVLR_BC_ICM.Value   := 0;   //??
      dmCupomFiscal.db_Temp_Est_NF_ItemVLR_BC_ICMS.Value  := 0;   //??
      dmCupomFiscal.db_Temp_Est_NF_ItemVLR_BC_ICM_SN.Value:= 0;   //??

      dmCupomFiscal.db_Temp_Est_NF_ItemVLR_IPI.Value      := 0;   //??
      dmCupomFiscal.db_Temp_Est_NF_ItemVLR_BC_IPI.Value   := 0;   //??

      dmCupomFiscal.db_Temp_Est_NF_ItemPRED_BC.Value      := 0;   //??
      dmCupomFiscal.db_Temp_Est_NF_ItemPRED_BC_ST.Value   := 0;   //??
      dmCupomFiscal.db_Temp_Est_NF_ItemPMVA_ST.Value      := 0;   //??

      dmCupomFiscal.db_Temp_Est_NF_ItemID_PEDIDO.Value    := 0;
      dmCupomFiscal.db_Temp_Est_NF_ItemRECEITA.Value      := 0;

      dmCupomFiscal.db_temp_Est_NF_item.Post;
      dmCupomFiscal.db_temp_Est_NF_item.Close;

      //Insert NFCe Vinculado
      dmCupomFiscal.db_Temp_Est_NF_NFe.Open;
      dmCupomFiscal.db_Temp_Est_NF_NFe.Insert;
      dmCupomFiscal.db_Temp_Est_NF_NFeITEM.Value := Item;
      dmCupomFiscal.db_Temp_Est_NF_NFeCLIFOR.Value := dmCupomFiscal.dbQuery1.FieldByName('CliFor').AsInteger;
      dmCupomFiscal.db_Temp_Est_NF_NFeID_ECF.Value := dmCupomFiscal.dbQuery1.FieldByName('ID').Value;
      dmCupomFiscal.db_Temp_Est_NF_NFeCHAVE_NFEREF.Value := dmCupomFiscal.dbQuery1.FieldByName('CHAVE_NFCE').AsString;
      dmCupomFiscal.db_Temp_Est_NF_NFe.Post;
      dmCupomFiscal.db_Temp_Est_NF_NFe.Close;

      dmCupomFiscal.dbQuery2.Next;
    end;
    dmCupomFiscal.Transaction.CommitRetaining;

    //Verifica Tributacao dos Itens da NF
    dmCupomFiscal.dbQuery3.Active:=False;
    dmCupomFiscal.dbQuery3.SQL.Clear;
    dmCupomFiscal.dbQuery3.SQL.Add('Select coalesce(count(*),0) as Flag From Temp_Est_NF_Item Where Clifor=:pCliFor and ((CST_ICM is null) or (CFOP is null) )');
    dmCupomFiscal.dbQuery3.ParamByName('pCliFor').asInteger:=dmCupomFiscal.dbQuery1.FieldByName('CliFor').AsInteger;
    dmCupomFiscal.dbQuery3.Active:=True;
    dmCupomFiscal.dbQuery3.First;
    If dmCupomFiscal.dbQuery3.FieldByName('Flag').AsInteger>0 then
      msgErro('As mercadorias da NF-e possuem ERRO(s) no CFOP ou CST de ICMS.'+#13+
              'NF-e pendente e aguardando corre��es.','')
    else
    begin
      If msgPergunta('Deseja atualizar esta NF-e ?', '') then
      begin
        //-- Gera Nota do Temp_Est_NF para Est_NF
        dmCupomFiscal.IBSQL1.Close;
        dmCupomFiscal.IBSQL1.SQL.Clear;
        dmCupomFiscal.IBSQL1.SQL.Add('Update temp_Est_NF Set Status=''F'' Where CliFor=:pCliFor');
        dmCupomFiscal.IBSQL1.ParamByName('pClifor').asInteger:=dmCupomFiscal.dbTemp_NFCECLIFOR.AsInteger;
        dmCupomFiscal.IBSQL1.ExecQuery;
        dmCupomFiscal.IBSQL1.Close;

        dmCupomFiscal.Transaction.CommitRetaining;

        //-- Exclue Temp_Est_NF
        dmCupomFiscal.IBSQL1.Close;
        dmCupomFiscal.IBSQL1.SQL.Clear;
        dmCupomFiscal.IBSQL1.SQL.Add('Delete From temp_Est_NF Where CliFor=:pCliFor');
        dmCupomFiscal.IBSQL1.ParamByName('pClifor').asInteger:=dmCupomFiscal.dbTemp_NFCECLIFOR.AsInteger;
        dmCupomFiscal.IBSQL1.ExecQuery;
        dmCupomFiscal.IBSQL1.Close;

        dmCupomFiscal.Transaction.CommitRetaining;
        msgAviso('NF-e gerada com sucesso, aguardando Envio!', '');
      end
      else
        msgAviso('NF-e gerada pendente, aguardando Atualiza��o!', '');
    end;
  except
    on e:exception do
    begin
      dmCupomFiscal.Transaction.RollbackRetaining;
      msgErro('Erro ao gerar Nota Fiscal. '+#13+e.Message,'Erro');
      Result := False;
    end;
  end;
end;

function TCtrlFim.getInformaSaldoDevedor: Boolean;
var
  lQryBusca: TIBQuery;
begin
  lQryBusca := TIBQuery.Create(nil);
  try
    lQryBusca.Database := dmCupomFiscal.DataBase;
    lQryBusca.SQL.add('Select DANFE_SALDODEVEDOR from LOCAIS_CONFIG_NFE where LOCAL = :pID and DANFE_SALDODEVEDOR = ''S''');
    lQryBusca.ParamByName('pID').asInteger := getLocalCCusto(dmCupomFiscal.dbTemp_NFCECCUSTO.AsInteger);
    lQryBusca.Open;
    Result := not lQryBusca.IsEmpty;
    lQryBusca.Close;
  finally
    lQryBusca.Free;
  end;
end;

function TCtrlFim.getJuroCliente: Currency;
var
  lQryBusca: TIBQuery;
begin
  lQryBusca := TIBQuery.Create(nil);
  try
    lQryBusca.Database := dmCupomFiscal.DataBase;
    lQryBusca.SQL.add('Select JURO from CLIFOR where ID = :pID');
    lQryBusca.ParamByName('pID').asInteger := dmCupomFiscal.dbTemp_NFCECLIFOR.AsInteger;
    lQryBusca.Open;
    Result := lQryBusca.FieldByName('JURO').AsCurrency;
    lQryBusca.Close;
  finally
    lQryBusca.Free;
  end;
end;

function TCtrlFim.getNomeVendedor(pId: Integer): String;
var
  lQryBusca: TIBQuery;
begin
  lQryBusca := TIBQuery.Create(nil);
  try
    lQryBusca.Database := dmCupomFiscal.DataBase;
    lQryBusca.SQL.add('Select Nome from Funcionarios where ID = :pID');
    lQryBusca.ParamByName('pID').asInteger := pID;
    lQryBusca.Open;
    Result := lQryBusca.FieldByName('Nome').asString;
    lQryBusca.Close;
  finally
    lQryBusca.Free;
  end;
end;

function TCtrlFim.getSaldoDevedor: Currency;
var
  lSqlBusca: TIBQuery;
  Dias, i: Integer;
  wCF_Juro, Valor, Resta, Juro: Currency;
begin
  {lSqlBusca := TIBQuery.Create(nil);
  try
    lSqlBusca.Database := dmCupomFiscal.DataBase;
    lSqlBusca.SQL.add('Select RESTA, DATA_VEN from cr_movimento where CLIFOR = :pclifor and RESTA > 0');
    lSqlBusca.ParamByName('pClifor').AsInteger := dmCupomFiscal.dbTemp_NFCECLIFOR.AsInteger;
    lSqlBusca.Open;
    if not(lSqlBusca.IsEmpty)Then
    begin
      Valor:=0; Dias:=0; Juro:=0; i:=0; Resta:=0;
      wCF_Juro := getJuroCliente;
      lSqlBusca.First;
      while not(lSqlBusca.Eof) do
      begin
        Dias:=DaysBetween(Now, lSqlBusca.fieldByName('DATA_VEN').AsDateTime);
        If Dias>0 Then
        begin
          For i := 1 To Dias do
            Juro:=Juro+(lSqlBusca.fieldByName('RESTA').AsCurrency * wCF_Juro)/100;
          Valor := Valor + Juro;
          Juro := 0;
        end;
        Resta := Resta + lSqlBusca.fieldByName('RESTA').AsCurrency;
        lSqlBusca.Next;
      end;
    end;
    Result := Valor + Resta;
    lSqlBusca.Close;
  finally
    lSqlBusca.Free;
  end;}
  Result := getSaldoCliente(dmCupomFiscal.dbTemp_NFCECLIFOR.AsInteger, True);
end;

function TCtrlFim.validarInformacoes: Boolean;
var retorno:Boolean;
    vlrTotalParcelas, vtotparc, vCheque:Currency;
begin
  try
    Result := True;
    vlrTotalParcelas := 0;
    vCheque := 0;
    //Cheque  18.04.16 / ajusta 11/08/2016 Mauricio. Desse jeito pode ter mais que um tpag 02
    dmCupomFiscal.dbTemp_NFCE_FormaPag.First;
    while not(dmCupomFiscal.dbTemp_NFCE_FormaPag.eof) do
    begin
      If (dmCupomFiscal.dbTemp_NFCE_FormaPagTPAG.Value = '02') then //02 - Cheque
      begin
        if dmCupomFiscal.dbTemp_NFCE_FormaPagVPAG.Value > 0 then
          vCheque := vCheque + dmCupomFiscal.dbTemp_NFCE_FormaPagVPAG.Value;
      end;
      dmCupomFiscal.dbTemp_NFCE_FormaPag.Next;
      if(vCheque > 0)then
      begin
        dmCupomFiscal.dbQuery1.Active:=False;
        dmCupomFiscal.dbQuery1.SQL.Clear;
        dmCupomFiscal.dbQuery1.SQL.Add('Select Sum(Valor) as Total From TEMP_NFCE_FORMAPAG_CHEQUE Where Caixa=:pCaixa');
        dmCupomFiscal.dbQuery1.ParamByName('pCaixa').AsString:=FormatFloat('0000', FRENTE_CAIXA.Caixa);
        dmCupomFiscal.dbQuery1.Active:=True;
        dmCupomFiscal.dbQuery1.First;
        if (dmCupomFiscal.dbQuery1.FieldByName('Total').AsCurrency<>vcheque) then
        begin
          msgInformacao('Valor Total do(s) Cheque(s) Difere do Informado.'+#13+
                        'Para Corrigir: Zerar o Valor Informado em Cheque e Lan�ar Novamente!','Informa��o');
          Abort;
        end;
      end;
    end;

    //Valida informacoes endereco
    if not(dmCupomFiscal.dbTemp_NFCECF_MUNICIPIO.IsNull) or not(dmCupomFiscal.dbTemp_NFCECF_ENDE.IsNull) or
       not(dmCupomFiscal.dbTemp_NFCECF_NUMERO_END.IsNull) or not(dmCupomFiscal.dbTemp_NFCECF_CEP.IsNull) or
       not(dmCupomFiscal.dbTemp_NFCECF_BAIRRO.IsNull) or not(dmCupomFiscal.dbTemp_NFCECF_CEP.IsNull) then
    begin
      if (dmCupomFiscal.dbTemp_NFCECF_MUNICIPIO.IsNull) or (dmCupomFiscal.dbTemp_NFCECF_ENDE.IsNull) or
         (dmCupomFiscal.dbTemp_NFCECF_NUMERO_END.IsNull) or (dmCupomFiscal.dbTemp_NFCECF_CEP.IsNull) or
         (dmCupomFiscal.dbTemp_NFCECF_BAIRRO.IsNull) or (dmCupomFiscal.dbTemp_NFCECF_CEP.IsNull) then
      begin
        msgInformacao('Obrigat�rio informar todos dados do Endere�o ou deixar todos em branco','Informa��o');
        Abort;
      end;
    end;

    if (not(ValidaFrota( dmCupomFiscal.dbTemp_NFCECLIFOR.Value, Trim(dmCupomFiscal.dbTemp_NFCECF_PLACA.Value)))
       and not(FRENTE_CAIXA.Permite_Frota)) then
    begin
      msgInformacao('Este Cliente Possui Frota.'+#13+
                    'Esta Placa N�o Consta em Sua Frota.'+#13+
                    'Verifique a Placa!','Informa��o');
      Abort;
    end;
    //--Lauro Fim 21.04.2016

    if (dmCupomFiscal.dbTemp_NFCEVLR_TOTAL.Value >= 10000) and
      (dmCupomFiscal.dbTemp_NFCECF_NOME.IsNull or dmCupomFiscal.dbTemp_NFCECF_ENDE.IsNull)then //Se valor maior ou igual a 10k informar cliente
    begin
      msgInformacao('Obrigat�rio informar dados do consumidor quando'+#13+
                    'valor da venda for igual ou superior a R$10.000,00','Informa��o');
      Abort;
    end;

    //Valida Falta Pagamento
    if dmCupomFiscal.dbTemp_NFCEVLR_TROCO.Value < 0 then
    begin
      msgInformacao('Total do pagamento n�o pode ser INFERIOR ao valor total da NFC-e','Informa��o');
      Abort;
      //Result := False;
    end;

    //Valida venda no credi�rio, precisa verificar o caixa...
    if (dmCupomFiscal.GetTemp_Valor_Crediario > 0) then
    begin
      if not(dmCupomFiscal.dbTemp_NFCECLIFOR.Value > 0) then
      begin
        msgInformacao('Obrigat�rio informar Consumidor quando venda for "Cr�dito da Loja"','Informa��o');
        Abort;
      end;

        //-- Lauro Verifica Limite 21.04.2016
{        if (crProtecaoCredito(StrToIntDef(edtCliente.Text,0))) then
        begin
          edtCliente.SetFocus;
          Abort;
        end;

        If (crExcedeuLimite(StrToIntDef(edtCliente.Text,0), dmCupomFiscal.dbTemp_NFCE_FormaPagVPAG.AsCurrency)) then
        begin
          edtCliente.SetFocus;
          Abort;
        end;

        if (FRENTE_CAIXA.CR_Baixa_Seletiva) then
          crContasVencidas(StrToIntDef(edtCliente.Text,0));}
        //-- Lauro Fim 21.04.2016

      //Verifica se nfce � vinculado ao um pedido
      dmCupomFiscal.dbQuery1.Active := false;
      dmCupomFiscal.dbQuery1.SQL.Clear;
      dmCupomFiscal.dbQuery1.SQL.Add('Select coalesce(n.EL_CHAVE,0) as EL_CHAVE from temp_nfce n '+
                                     'LEFT OUTER JOIN est_pedidos p ON p.EL_CHAVE = n.EL_CHAVE '+
                                     'where n.CAIXA = :pCaixa');
      dmCupomFiscal.dbQuery1.ParamByName('pCaixa').AsString := FormatFloat('0000', FRENTE_CAIXA.Caixa);
      dmCupomFiscal.dbQuery1.Active := True;
      //Se sim verifica a forma de pagamento se gera fatura pelo pedido
      if(dmCupomFiscal.dbQuery1.FieldByName('EL_CHAVE').asInteger > 0)then
      begin
        dmCupomFiscal.dbQuery2.Active := false;
        dmCupomFiscal.dbQuery2.SQL.Clear;
        dmCupomFiscal.dbQuery2.SQL.Add('Select f.PEDIDO_CR_CP, p.ID from Formas_Pagamento f '+
                                       'LEFT OUTER JOIN est_Pedidos p on P.Forma_Pag = f.ID '+
                                       'where p.EL_Chave = :pChave');
        dmCupomFiscal.dbQuery2.ParamByName('pChave').AsInteger :=
          dmCupomFiscal.dbQuery1.FieldByName('EL_CHAVE').asInteger;
        dmCupomFiscal.dbQuery2.Active := True;
        if(dmCupomFiscal.dbQuery2.FieldByName('PEDIDO_CR_CP').asString = 'R')then
        begin
          //Verifica se esta na conta do cliente
          dmCupomFiscal.dbQuery3.active := false;
          dmCupomFiscal.dbQuery3.SQL.Clear;
          dmCupomFiscal.dbQuery3.sql.Add('Select sum(Valor) as Valor from CR_Movimento '+
                                         'where SDoc = :pSDoc and NDoc = :pNDoc and Clifor = :pClifor');
          dmCupomFiscal.dbQuery3.ParamByName('pSDoc').asString := 'PE';
          dmCupomFiscal.dbQuery3.ParamByName('pNDoc').asInteger := dmCupomFiscal.dbQuery2.fieldByName('ID').asInteger;
          dmCupomFiscal.dbQuery3.ParamByName('pClifor').asInteger := dmCupomFiscal.dbTemp_NFCECLIFOR.asInteger;
          dmCupomFiscal.dbQuery3.Active := True;
          if(dmCupomFiscal.dbQuery3.FieldByName('Valor').AsCurrency <> dmCupomFiscal.GetTemp_Valor_Crediario)then
            if not(gerarParcelas)then
              Result := False;
        end;
      end;
      //Verifica se gerou as parcelas
      dmCupomFiscal.getTemp_NFCE_Fatura;
      while not(dmCupomFiscal.dbTemp_NFCE_fatura.Eof) do
      begin
        vlrTotalParcelas := vlrTotalParcelas + dmCupomFiscal.dbTemp_NFCE_FaturaVALOR.AsCurrency;
        dmCupomFiscal.dbTemp_NFCE_Fatura.Next;
      end;
      if(vlrTotalParcelas = 0)then
      begin
        if not(gerarParcelas)then
          Result := False;
      end
      else
        if(dmCupomFiscal.GetTemp_Valor_Crediario <> vlrTotalParcelas)then
        begin
          msgInformacao('Total das parcelas n�o pode ser diferente do total pago a prazo.','Informa��o');
          if not(gerarParcelas)then
            Result := False;
        end;
    end;
  except
    on e:Exception do
    begin
      if not (e is EAbort) then
        msgInformacao(e.Message,'');
      Result:=False;
    end;
  end;
end;

end.

