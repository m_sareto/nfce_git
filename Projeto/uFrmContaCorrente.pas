unit uFrmContaCorrente;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Grids, DBGrids, Mask, StdCtrls, TREdit, DBCtrls,
  Buttons, Provider, DB, DBClient, ACBrBase, ACBrValidador,
  IBCustomDataSet, IBQuery;

type
  TfrmContaCorrente = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    DBEdit4: TDBEdit;
    Label8: TLabel;
    DBEdit8: TDBEdit;
    Label9: TLabel;
    dsCCorrente_Total: TDataSource;
    dsCCorrente: TDataSource;
    DBEdit9: TDBEdit;
    qrCCorrente_Total: TIBQuery;
    qrCCorrente_TotalTOTAL: TIBBCDField;
    Label1: TLabel;
    Label2: TLabel;
    DBEdit1: TDBEdit;
    edNome: TEdit;
    reSaldo: TRealEdit;
    Label3: TLabel;
    dbgrd1: TDBGrid;
    btNovo: TBitBtn;
    btSalva: TBitBtn;
    btExcluir: TBitBtn;
    btCancelar: TBitBtn;
    btFechar: TBitBtn;
    procedure btFecharClick(Sender: TObject);
    procedure btNovoClick(Sender: TObject);
    procedure btSalvaClick(Sender: TObject);
    procedure btExcluirClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure DBEdit8Exit(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure dsCCorrenteStateChange(Sender: TObject);
    procedure GetCCorrente;
    procedure GetTotal;
    procedure DBEdit1Exit(Sender: TObject);
    procedure DBEdit4Exit(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Label2Click(Sender: TObject);
  private
    { Private declarations }
    function GetSaldoTemp: Currency;
  public
    { Public declarations }
  end;
var
  frmContaCorrente: TfrmContaCorrente;

implementation

uses uDMCupomFiscal, uFrmFim, uFrmPrincipal, uDMComponentes, uFuncoes,
  Math, uFrmPesquisaCliente;

{$R *.dfm}

procedure TfrmContaCorrente.btFecharClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmContaCorrente.btNovoClick(Sender: TObject);
begin
  DMCupomFiscal.dbTemp_NFCe_FormaPag_CCorrente.Open;
  DMCupomFiscal.dbTemp_NFCe_FormaPag_CCorrente.Insert;
  dmCupomFiscal.dbTemp_NFCe_FormaPag_CCorrenteID.Value :=  dmCupomFiscal.dbTemp_NFCE_FormaPagID.AsInteger;
  dmCupomFiscal.dbTemp_NFCe_FormaPag_CCorrenteID_CREDITO.Value := 0;
  dmCupomFiscal.dbTemp_NFCe_FormaPag_CCorrenteCaixa.Value := FormatFloat('0000', FRENTE_CAIXA.Caixa);
  DMCupomFiscal.dbTemp_NFCe_FormaPag_CCorrenteValor.Value:=0;
  DBEdit1.SetFocus;
end;

procedure TfrmContaCorrente.btSalvaClick(Sender: TObject);
begin
  if StrToIntDef(DBEdit4.Text,0)<=0 then
  begin
    ShowMessage('N�o Foi Informado Cliente!');
    DBEdit4.SetFocus;
    Abort;
  end;

  If DMCupomFiscal.dbTemp_NFCe_FormaPag_CCorrente.State in [dsInsert, dsEdit] then
  begin
    DMCupomFiscal.dbTemp_NFCe_FormaPag_CCorrente.Post;
    DMCupomFiscal.Transaction.CommitRetaining;

    edNome.Clear;
    reSaldo.Clear;
    DBEdit4.Enabled := True;

    GetCCorrente;
    GetTotal;
  end;
  btNovo.SetFocus;
end;

procedure TfrmContaCorrente.btExcluirClick(Sender: TObject);
begin
  If(msgPergunta('Excluir registro selecionado?',Application.Title))then
  begin
    DMCupomFiscal.dbTemp_NFCe_FormaPag_CCorrente.Delete;
    DMCupomFiscal.Transaction.CommitRetaining;

    edNome.Clear;
    reSaldo.Clear;
    DBEdit4.Enabled := True;

    GetCCorrente;
    GetTotal;
  end;
  btNovo.SetFocus;
end;

procedure TfrmContaCorrente.btCancelarClick(Sender: TObject);
begin
  DMCupomFiscal.dbTemp_NFCe_FormaPag_CCorrente.Cancel;
  DMCupomFiscal.Transaction.RollbackRetaining;

  edNome.Clear;
  reSaldo.Clear;
  DBEdit4.Enabled := True;
  btNovo.SetFocus;
end;

procedure TfrmContaCorrente.DBEdit1Exit(Sender: TObject);
begin
  if StrToIntDef(DBEdit1.Text,0)<=0 then
    DBEdit1.Text:='0'
  Else
  begin
    DMCupomFiscal.dbQuery1.Active:=False;
    DMCupomFiscal.dbQuery1.SQL.Clear;
    DMCupomFiscal.dbQuery1.SQL.Add('Select cc.CliFor, cc.Valor, c.nome '+
      'From Cr_Conta_Corrente cc, clifor c '+
      'Where (cc.ID=:pID) and (cc.clifor=c.id)');
    DMCupomFiscal.dbQuery1.ParamByName('pID').AsInteger:=StrToIntDef(DBEdit1.Text,0);
    DMCupomFiscal.dbQuery1.Active:=True;
    DMCupomFiscal.dbQuery1.First;
    if DMCupomFiscal.dbQuery1.eof then
    begin
      ShowMessage('Vale Inexistente !');
      DBEdit1.SetFocus;
    end
    Else
    begin
      DMCupomFiscal.dbQuery2.Active:=False;
      DMCupomFiscal.dbQuery2.SQL.Clear;
      DMCupomFiscal.dbQuery2.SQL.Add('Select Sum(case DC when ''C'' then Valor when ''D'' then -Valor end) as Saldo'+
                                     ' From Cr_Conta_Corrente Where (Clifor=:pCF)');
      DMCupomFiscal.dbQuery2.ParamByName('pCF').AsInteger:=DMCupomFiscal.dbQuery1.FieldByName('Clifor').AsInteger;
      DMCupomFiscal.dbQuery2.Active:=True;
      DMCupomFiscal.dbQuery2.First;
      if (DMCupomFiscal.dbQuery2.FieldByName('Saldo').AsCurrency - GetSaldoTemp) <= 0 then
      begin
        msgInformacao('O valor total do vale j� foi atingido.', Application.Title);
        DBEdit1.SetFocus;
        Abort
      end
      Else
      begin
        reSaldo.Value := DMCupomFiscal.dbQuery2.FieldByName('Saldo').AsCurrency - GetSaldoTemp;

        dmCupomFiscal.dbTemp_NFCe_FormaPag_CCorrenteCLIFOR.AsInteger :=
          DMCupomFiscal.dbQuery1.FieldByName('ClIFor').AsInteger;
        dmCupomFiscal.dbTemp_NFCe_FormaPag_CCorrenteVALOR.AsCurrency :=
          DMCupomFiscal.dbQuery1.FieldByName('Valor').AsCurrency - GetSaldoTemp;

        edNome.Text := DMCupomFiscal.dbQuery1.FieldByName('Nome').AsString;

        DBEdit4.Enabled := False;
        DBEdit8.SetFocus;
      end;
    end;
  end;
end;

procedure TfrmContaCorrente.DBEdit4Exit(Sender: TObject);
begin
  DMCupomFiscal.dbQuery1.Active:=False;
  DMCupomFiscal.dbQuery1.SQL.Clear;
  DMCupomFiscal.dbQuery1.SQL.Add('Select Nome, ID From CliFor Where (ID=:pID)');
  DMCupomFiscal.dbQuery1.ParamByName('pID').AsInteger:=StrToIntDef(DBEdit4.Text,0);
  DMCupomFiscal.dbQuery1.Active:=True;
  DMCupomFiscal.dbQuery1.First;
  if DMCupomFiscal.dbQuery1.EOF then
  begin
    ShowMessage('Cliente N�o Cadastrado!');
    DBEdit4.SetFocus;
    Abort
  End
  Else
  begin
    edNome.Text:=DMCupomFiscal.dbQuery1.FieldByName('Nome').AsString;

    // Verifica se j� foi lancado para este cliente sem ser Vale
    DMCupomFiscal.dbQuery2.Active:=False;
    DMCupomFiscal.dbQuery2.SQL.Clear;
    DMCupomFiscal.dbQuery2.SQL.Add('Select ID From TEMP_NFCE_FORMAPAG_CCorrente '+
                    'Where (CAIXA=:pCAIXA) and (ID=:pID) and (CliFor=:pCliFor) and (ID_Credito=0)');
    DMCupomFiscal.dbQuery2.ParamByName('pCaixa').AsString :=FormatFloat('0000',FRENTE_CAIXA.Caixa);
    DMCupomFiscal.dbQuery2.ParamByName('pID').AsInteger :=dmCupomFiscal.dbTemp_NFCE_FormaPagID.AsInteger;
    DMCupomFiscal.dbQuery2.ParamByName('pCliFor').AsInteger :=StrToIntDef(DBEdit4.Text,0);
    DMCupomFiscal.dbQuery2.Active := True;
    DMCupomFiscal.dbQuery2.First;
    If not(DMCupomFiscal.dbQuery2.Eof) then
    begin
      msgErro('Cliente j� possue Lan�amento sem Vale!',Application.title);
      DBEdit1.SetFocus;
      Abort;
    end;
    //----------------------------------------------------------

    DMCupomFiscal.dbQuery2.Active:=False;
    DMCupomFiscal.dbQuery2.SQL.Clear;
    DMCupomFiscal.dbQuery2.SQL.Add('Select Sum(case DC when ''C'' then Valor when ''D'' then -Valor end) as Saldo'+
                                   ' From Cr_Conta_Corrente Where (Clifor=:pCF)');
    DMCupomFiscal.dbQuery2.ParamByName('pCF').AsInteger:=DMCupomFiscal.dbQuery1.FieldByName('ID').AsInteger;
    DMCupomFiscal.dbQuery2.Active:=True;
    DMCupomFiscal.dbQuery2.First;
    reSaldo.Value := DMCupomFiscal.dbQuery2.FieldByName('Saldo').AsCurrency - GetSaldoTemp;
    if(reSaldo.Value<=0)then
    begin
      msgErro('Saldo N�o Dispoin�vel!',Application.title);
      DBEdit4.SetFocus;
      Abort;
    end;
  end;
end;

procedure TfrmContaCorrente.DBEdit8Exit(Sender: TObject);
begin
  If DMCupomFiscal.dbTemp_NFCe_FormaPag_CCorrenteValor.Value<=0 then
  begin
    ShowMessage('Valor deve ser Maior que Zero.');
    DBEdit8.SetFocus;
  end
  else
    if(DMCupomFiscal.dbTemp_NFCe_FormaPag_CCorrenteValor.Value > reSaldo.Value)then
    begin
      msgErro('O valor informado � maior que o cr�dito dispon�vel.',Application.title);
      DBEdit8.SetFocus;
    end;
end;

procedure TfrmContaCorrente.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  DMCupomFiscal.dbTemp_NFCe_FormaPag_CCorrente.Close;
end;

procedure TfrmContaCorrente.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  If (Key=vk_escape) then Close;

  //Cliente
  If (key=vk_F12) and (DBEdit4.Focused) then Label2Click(Action);
end;

procedure TfrmContaCorrente.FormShow(Sender: TObject);
begin
  GetCCorrente;
  GetTotal;
end;

procedure TfrmContaCorrente.dsCCorrenteStateChange(Sender: TObject);
begin
  btNovo.Enabled    :=not(DMCupomFiscal.dbTemp_NFCe_FormaPag_CCorrente.State in [dsInsert,dsEdit]);
  btSalva.Enabled   :=   (DMCupomFiscal.dbTemp_NFCe_FormaPag_CCorrente.State in [dsInsert,dsEdit]);
  btExcluir.Enabled :=not(DMCupomFiscal.dbTemp_NFCe_FormaPag_CCorrente.State in [dsInsert,dsEdit])
                      and(DMCupomFiscal.dbTemp_NFCe_FormaPag_CCorrente.State in [dsBrowse]);
  btCancelar.Enabled:=   (DMCupomFiscal.dbTemp_NFCe_FormaPag_CCorrente.State in [dsInsert,dsEdit,dsBrowse]);
  btFechar.Enabled  :=not(DMCupomFiscal.dbTemp_NFCe_FormaPag_CCorrente.State in [dsInsert,dsEdit]);
  dbgrd1.Enabled    :=not(DMCupomFiscal.dbTemp_NFCe_FormaPag_CCorrente.State in [dsInsert,dsEdit]);
end;

procedure TfrmContaCorrente.GetCCorrente;
begin
  dmCupomFiscal.dbTemp_NFCe_FormaPag_CCorrente.Close;
  dmCupomFiscal.dbTemp_NFCe_FormaPag_CCorrente.ParamByName('pCaixa').asString  := FormatFloat('0000', FRENTE_CAIXA.Caixa);
  dmCupomFiscal.dbTemp_NFCe_FormaPag_CCorrente.ParamByName('pID').asinteger  := dmCupomFiscal.dbTemp_NFCE_FormaPagID.AsInteger;
  dmCupomFiscal.dbTemp_NFCe_FormaPag_CCorrente.Open;
end;

procedure TfrmContaCorrente.GetTotal;
begin
  qrCCorrente_Total.Active := false;
  qrCCorrente_Total.ParamByName('pCaixa').asString  := FormatFloat('0000', FRENTE_CAIXA.Caixa);
  qrCCorrente_Total.Active := True;
end;

function TfrmContaCorrente.GetSaldoTemp: Currency;
var
  qrBusca: TIBQuery;
begin
  qrBusca := TIBQuery.Create(Self);
  try
    qrBusca.Active := False;
    qrBusca.Database := dmCupomFiscal.DataBase;
    qrBusca.SQL.Add('Select Sum(Valor) as Saldo From TEMP_NFCE_FORMAPAG_CCorrente '+
                    'Where (CAIXA=:pCAIXA) and (ID=:pID) and (CliFor=:pCliFor)');
    qrBusca.ParamByName('pCaixa').AsString :=FormatFloat('0000',FRENTE_CAIXA.Caixa);
    qrBusca.ParamByName('pID').AsInteger :=dmCupomFiscal.dbTemp_NFCE_FormaPagID.AsInteger;
    qrBusca.ParamByName('pCliFor').AsInteger :=StrToIntDef(DBEdit4.Text,0);
    qrBusca.Active := True;
    Result := qrBusca.FieldByName('Saldo').AsCurrency;
  finally
    qrBusca.Free;
  end;
end;


procedure TfrmContaCorrente.Label2Click(Sender: TObject);
begin
  FrmPesquisaCliente := TFrmPesquisaCliente.Create(Self);
  try
    if (frmPesquisaCliente.ShowModal=mrOK) then DBEdit4.Text:= frmPesquisaCliente.qCliFor.FieldByName('id').AsString;
  finally
    frmPesquisaCliente.Free;
  end;
end;

end.
