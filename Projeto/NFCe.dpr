program NFCe;

{$R *.dres}

uses
  Forms,
  Windows,
  uFrmLoja in 'uFrmLoja.pas' {frmLoja},
  UFrmFim in 'UFrmFim.pas' {frmFim},
  uFrmGerarParcelas in 'uFrmGerarParcelas.pas' {frmGerarParcelas},
  uFrmCancelaItem in 'uFrmCancelaItem.pas' {frmCancelaItem},
  uFrmPesquisaMercadoria in 'uFrmPesquisaMercadoria.pas' {frmPesquisaMercadoria},
  uDMCupomFiscal in 'uDMCupomFiscal.pas' {dmCupomFiscal: TDataModule},
  uFrmPesquisaCliente in 'uFrmPesquisaCliente.pas' {frmPesquisaCliente},
  uFrmConsultaCR in 'uFrmConsultaCR.pas' {frmConsultaCR},
  uFrmAbastecidas in 'uFrmAbastecidas.pas' {frmAbastecidas},
  uFrmConsultaExtrato in 'uFrmConsultaExtrato.pas' {frmConsultaExtrato},
  uFrmPesquisaGenerica in 'uFrmPesquisaGenerica.pas' {frmPesquisaGenerica},
  uFrmPesquisa in 'uFrmPesquisa.pas' {frmPesquisa},
  uFrmFechamentoAbastecidas in 'uFrmFechamentoAbastecidas.pas' {frmFechamentoAbastecidas},
  uFuncoes in 'C:\ELINFO\Biblioteca_Funcoes\uFuncoes.pas',
  uFrmConfigurarSerial in 'uFrmConfigurarSerial.pas' {frmConfigurarSerial},
  uFrmPrincipal in 'uFrmPrincipal.pas' {frmPrincipal},
  uFrmConfiguracao in 'uFrmConfiguracao.pas' {frmConfiguracao},
  uDMComponentes in 'uDMComponentes.pas' {TDataModule: TDataModule},
  uRotinasGlobais in 'uRotinasGlobais.pas',
  uFrmAutorizacao in 'uFrmAutorizacao.pas' {frmAutorizacao},
  uFrmSupermercado in 'uFrmSupermercado.pas' {frmSupermercado},
  UfrmPesquisaCodigo in 'UfrmPesquisaCodigo.pas' {frmPesquisaCodigo},
  uFrmConsultaLimiteCR in 'uFrmConsultaLimiteCR.pas' {frmConsultaLimiteCR},
  uFrmLogin in 'uFrmLogin.pas' {frmLogin},
  uFrmImportaPedido in 'uFrmImportaPedido.pas' {frmImportaPedido},
  uFrmStatusNFCe in '\\192.168.254.200\d\ELINFO\Comuns\uFrmStatusNFCe.pas' {frmStatusNFCe},
  uFrmNFCe in 'uFrmNFCe.pas' {frmNFCe},
  uFrmNFCeInutilizacao in 'uFrmNFCeInutilizacao.pas' {frmNFCeInutilizacao},
  uFrmImportaMercadoria in 'uFrmImportaMercadoria.pas' {frmImportaMercadoria},
  uFrmAguarde in 'uFrmAguarde.pas' {frmAguarde},
  uFrmExportaNFCe in 'uFrmExportaNFCe.pas' {frmExportaNFCe},
  uFrmBandeiraCartao in 'uFrmBandeiraCartao.pas' {frmBandeiraCartao},
  uFrmExportaXmlNFCe in 'uFrmExportaXmlNFCe.pas' {frmExportaXmlNFCe},
  uFrmNcm in 'uFrmNcm.pas' {frmNcm},
  uFrmPosto in 'uFrmPosto.pas' {frmPosto},
  uFrmAbastecidaCliente in 'uFrmAbastecidaCliente.pas' {frmAbastecidaCliente},
  uFrmContingencia in 'uFrmContingencia.pas' {frmContingencia},
  uFrmAjustarNcm in 'uFrmAjustarNcm.pas' {frmAjustarNcm},
  uFrmConsulta_Caixa in 'uFrmConsulta_Caixa.pas' {FrmConsulta_Caixa},
  uFrmSangriaSuprimento in 'uFrmSangriaSuprimento.pas' {frmSangriaSuprimento},
  uFrmImpComprovaSangriaSup in 'uFrmImpComprovaSangriaSup.pas' {frmImpCompSangSup},
  uFrm_Imp_Fatura in 'uFrm_Imp_Fatura.pas' {Frm_Imp_Fatura},
  uFrmConsultaResumoVendas in 'uFrmConsultaResumoVendas.pas' {FrmConsultaResumoVendas},
  uFrmConsultaHistoricoAutorizacao in 'uFrmConsultaHistoricoAutorizacao.pas' {frmConsultaHistoricoAutorizacao},
  uFrmClifor in 'uFrmClifor.pas' {frmClifor},
  uCtrlFim in 'uCtrlFim.pas',
  uCtrlTef in '\\192.168.254.200\d\ELINFO\Comuns\uCtrlTef.pas',
  uFrm_Conf_Redes in 'uFrm_Conf_Redes.pas' {frmTefConfigRedes},
  UFrmVerificandoLicenca in '..\..\Administrador\Administrador\Projeto\UFrmVerificandoLicenca.pas' {frmVerificandoLicenca},
  uFrmCartaFrete in 'uFrmCartaFrete.pas' {frmCartaFrete},
  uFrmContaCorrente in 'uFrmContaCorrente.pas' {frmContaCorrente},
  uFrmCheque in 'uFrmCheque.pas' {frmCheque},
  uFrmPesquisaPlaca in 'uFrmPesquisaPlaca.pas' {FrmPesquisaPlaca},
  uCtrlEnvio in 'uCtrlEnvio.pas',
  Unit4 in 'Unit4.pas' {Form4},
  Unit5 in 'Unit5.pas' {Form5},
  UFrmTurno_Geral in 'UFrmTurno_Geral.pas' {FrmTurno_Geral},
  uFrmListaComanda in 'uFrmListaComanda.pas' {FrmListaComandas},
  uFrmImpComanda in 'uFrmImpComanda.pas' {frmImpCompComanda},
  uCtrlNFCe in '\\192.168.254.200\d\ELINFO\Comuns\uCtrlNFCe.pas',
  uNFCe in '\\192.168.254.200\d\ELINFO\Comuns\uNFCe.pas',
  uFrmMsgTef in '\\192.168.254.200\d\ELINFO\Comuns\uFrmMsgTef.pas' {frmMsgTef},
  uFrmVendedor in 'uFrmVendedor.pas' {frmVendedor},
  uFrmImpCompDebito in '\\192.168.254.200\d\ELINFO\Comuns\uFrmImpCompDebito.pas' {FrmImpCompDebito},
  ufrmImpTefComprovantes in '\\192.168.254.200\d\ELINFO\Comuns\ufrmImpTefComprovantes.pas' {frmImpTefComprovantes},
  uFrmCliforPreCadastro in 'uFrmCliforPreCadastro.pas' {frmCliforPreCadastro},
  uFrmListaMotorista in 'uFrmListaMotorista.pas' {frmListaMotoristas};

var Hwnd : THandle;

begin
  //FindWindow vai procurar pela classe TApplication que tenha o nome do Title que vc configurou}
  Hwnd := FindWindow('TApplication', '::. Programa Emissor de NFC-e .::'); //Cupom_Fiscal � o titulo da sua aplica��o

  if Hwnd = 0 then //Se o Handle � 0 significa que nao encontrou
  begin
    Application.Initialize;
    Application.Title := '::. Programa Emissor de NFC-e .::';
    Application.CreateForm(TdmCupomFiscal, dmCupomFiscal);
  Application.CreateForm(TdmComponentes, dmComponentes);
  Application.CreateForm(TfrmPrincipal, frmPrincipal);
  Application.CreateForm(TfrmConfiguracao, frmConfiguracao);
  Application.CreateForm(TfrmLogin, frmLogin);
  Application.Run;
  end
  else
  begin
    //Esta funcao traz para frente (da o foco) para a janela da aplicacao que ja esta rodando
    SetForegroundWindow(Hwnd);
  end;

{
//Colina
  Application.Initialize;
  Application.Title := '::. Colina - Programa Emissor de NFC-e .::';
  Application.CreateForm(TdmCupomFiscal, dmCupomFiscal);
  Application.CreateForm(TdmComponentes, dmComponentes);
  Application.CreateForm(TfrmPrincipal, frmPrincipal);
  Application.CreateForm(TfrmConfiguracao, frmConfiguracao);
  Application.CreateForm(TfrmLogin, frmLogin);
  Application.CreateForm(TfrmVerificandoLicenca, frmVerificandoLicenca);
  Application.Run;
}
end.
