unit uFrmLogin;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, pngimage, ExtCtrls, PngFunctions, IdTCPClient,
  Vcl.Menus, System.ImageList, Vcl.ImgList, IBSQL;

type
  TfrmLogin = class(TForm)
    edtUsuario: TEdit;
    edtSenha: TEdit;
    btnOK: TBitBtn;
    btnCancelar: TBitBtn;
    Image1: TImage;
    Label1: TLabel;
    Label2: TLabel;
    PopupMenu1: TPopupMenu;
    ChavedeAutenticao1: TMenuItem;
    ImageList1: TImageList;
    procedure btnOKClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ChavedeAutenticao1Click(Sender: TObject);
  private
    { Private declarations }
    procedure logar();
    procedure AbreFormVerificaLicenca();
    procedure FechaFormVerificaLicenca();
    function ConexaoLicenca():Boolean;
    function pingServidor(const Host: string; const Port, TimeOut: Integer): Boolean;
    procedure DesconectaLicenca;
  public
    { Public declarations }
  end;

var
  frmLogin: TfrmLogin;
  ID : Integer;
  chaveDigitada : String;

implementation

uses uDMCupomFiscal, uRotinasGlobais, uFuncoes, uFrmPrincipal,
  uDMComponentes, DB, UFrmVerificandoLicenca;

{$R *.dfm}

procedure TfrmLogin.btnOKClick(Sender: TObject);
begin
  logar;
end;

procedure TfrmLogin.ChavedeAutenticao1Click(Sender: TObject);
begin
  chaveDigitada := InputBox('Chave de libera��o', 'Informe a chave da licen�a:', chaveDigitada);
end;

function TfrmLogin.ConexaoLicenca: Boolean;
begin
  if (pingServidor(Licenca.Licenca_Servidor1, 3050, 3000)) then
  begin
    try
      dmCupomFiscal.databaseLicenca.Connected := False;
      dmCupomFiscal.transactionLicenca.Active := False;
      dmCupomFiscal.databaseLicenca.DatabaseName := Licenca.Licenca_Servidor1+':'+Licenca.Licenca_CaminhoBase1;
      dmCupomFiscal.databaseLicenca.Connected := True;
      dmCupomFiscal.transactionLicenca.Active := True;
      Result := dmCupomFiscal.databaseLicenca.Connected;
    except
      Result := False;
    end
  end
  else
  begin
    if (pingServidor(Licenca.Licenca_Servidor2, 3050, 3000)) then
    begin
      try
        dmCupomFiscal.databaseLicenca.Connected := False;
        dmCupomFiscal.transactionLicenca.Active := False;
        dmCupomFiscal.databaseLicenca.DatabaseName := Licenca.Licenca_Servidor2+':'+Licenca.Licenca_CaminhoBase2;
        dmCupomFiscal.databaseLicenca.Connected := True;
        dmCupomFiscal.transactionLicenca.Active := True;
        Result := dmCupomFiscal.databaseLicenca.Connected;
      except
        Result := False;
      end
    end
    else
      Result := False;
  end;
end;

procedure TfrmLogin.DesconectaLicenca;
begin
  dmCupomFiscal.databaseLicenca.Connected := False;
  dmCupomFiscal.transactionLicenca.Active := False;
end;

procedure TfrmLogin.logar;
var
  msg: String;
begin
  if (Trim(edtUsuario.Text) <> '') and (Trim(edtSenha.Text) <> '') then
  begin
    dmCupomFiscal.dbQuery3.Active:=False;
    dmCupomFiscal.dbQuery3.SQL.Clear;
    dmCupomFiscal.dbQuery3.SQL.Add('Select U.*,F.Nome F_Nome, F.ativo, F.nfce_caixa, F.nfce_uso'+
        ' From Usuarios U, Funcionarios F'+
        ' Where (U.Nome=:pNome) and (U.Senha=:pSenha) and (U.Funcionario=F.Id)');
    dmCupomFiscal.dbQuery3.ParamByName('pNome').AsString:=Trim(edtUsuario.Text);
    dmCupomFiscal.dbQuery3.ParamByName('pSenha').AsString:=Trim(edtSenha.Text);
    dmCupomFiscal.dbQuery3.Active:=True;
    dmCupomFiscal.dbQuery3.First;
    if dmCupomFiscal.dbQuery3.eof then
    begin
      msgInformacao('Usu�rio e/ou Senha inv�lido','');
      edtUsuario.SetFocus;
      Abort;
    end
    else
    begin
      if (chaveDigitada <> '') then
      begin
        //Seleciona a ID do Local
        dmCupomFiscal.dbQuery1.Active:=False;
        dmCupomFiscal.dbQuery1.SQL.Clear;
        dmCupomFiscal.dbQuery1.SQL.Add('Select L.id From Locais L'+
                              ' Left Outer Join CCustos CC on CC.Local=L.Id'+
                              ' Where CC.ID=:pID');
        dmCupomFiscal.dbQuery1.ParamByName('pID').AsInteger:=FRENTE_CAIXA.CCusto;
        dmCupomFiscal.dbQuery1.Active:=True;
        dmCupomFiscal.dbQuery1.First;

        //Atualiza Data Expira Locais para Data gerada pela chave
        dmCupomFiscal.dbQuery2.Active:=False;
        dmCupomFiscal.dbQuery2.SQL.Clear;
        dmCupomFiscal.dbQuery2.SQL.Add('update locais set data_exp = :pData'+
                              ' Where ID=:pID');
        dmCupomFiscal.dbQuery2.ParamByName('pID').AsInteger:=dmCupomFiscal.dbQuery1.FieldByName('id').AsInteger;
        dmCupomFiscal.dbQuery2.ParamByName('pData').AsDate:=strToDate(decifra(chaveDigitada,3));
        dmCupomFiscal.dbQuery2.ExecSQL;
        dmCupomFiscal.dbQuery2.Transaction.CommitRetaining;
        logErros(Self, caminhoLog, '', 'Logado no sistema com Chave de libera��o', 'S', nil);
      end;
      //Seleciona os dados do local
      dmCupomFiscal.dbQuery1.Active:=False;
      dmCupomFiscal.dbQuery1.SQL.Clear;
      dmCupomFiscal.dbQuery1.SQL.Add('Select L.CRT, L.id, L.cnpj, L.Data_Exp, L.DATA_ULT_VER_LICENCA, '+
                            ' LC.Adm_Decimal_Vlr_Unitario, LC.ADM_EST_ENTREGA_FUTURA'+
                            ' From Locais L'+
                            ' Left Outer Join locais_config LC on LC.Local=L.Id'+
                            ' Left Outer Join CCustos CC on CC.Local=L.Id'+
                            ' Where CC.ID=:pID');
      dmCupomFiscal.dbQuery1.ParamByName('pID').AsInteger:=FRENTE_CAIXA.CCusto;
      dmCupomFiscal.dbQuery1.Active:=True;
      dmCupomFiscal.dbQuery1.First;

      //Alterado a condi��o para que o sistema s� verifique a licen�a uma x por dia
      //(Date >= (dmCupomFiscal.dbQuery1.FieldByName('data_exp').AsDateTime)
      //and (dmCupomFiscal.dbQuery1.FieldByName('DATA_ULT_VER_LICENCA').AsDateTime <> Date)
      if ((Date > (dmCupomFiscal.dbQuery1.FieldByName('data_exp').AsDateTime))
        and not (FileExists(caminhoExe+'\NFCe.dpr')))then
      begin
        abreFormVerificaLicenca();
        //Tenta efetuar Conex�o com BD Licen�a
        if (conexaoLicenca()) then
        begin
          dmCupomFiscal.qurLicenca.Active := False;
          dmCupomFiscal.qurLicenca.SQL.Clear;
          dmCupomFiscal.qurLicenca.SQL.Add('select * from licenca where CNPJ=:pCNPJ');
          dmCupomFiscal.qurLicenca.ParamByName('pCNPJ').AsString := dmCupomFiscal.dbQuery1.FieldByName('cnpj').AsString;
          dmCupomFiscal.qurLicenca.Active := True;
          dmCupomFiscal.qurLicenca.First;
          if (dmCupomFiscal.qurLicenca.Eof) then
          begin
            fechaFormVerificaLicenca();
            logErros(Self, caminhoLog, 'Erro durante verifica��o de licen�a.'+#13+
                                       'N�o registrado na base de licen�as;'+#13+
                                       'Entre em contato com suporte!',
            'Conectado. No entanto, licen�a n�o identificada no servidor de licen�as', 'S',nil );
            Abort;
          end
          else
          begin
            ID := dmCupomFiscal.qurLicenca.FieldByName('ID').asInteger;
            if (dmCupomFiscal.qurLicenca.FieldByName('ativo').AsString = 'L') then
            begin
              //Atualiza Data Expira Locais para Data de Hoje
              dmCupomFiscal.dbQuery2.Active:=False;
              dmCupomFiscal.dbQuery2.SQL.Clear;
              dmCupomFiscal.dbQuery2.SQL.Add('update locais set data_exp = :pDataHoje, '+
                'DATA_ULT_VER_LICENCA = :pDtVerLic Where ID=:pID');
              dmCupomFiscal.dbQuery2.ParamByName('pID').AsInteger:=dmCupomFiscal.dbQuery1.FieldByName('id').AsInteger;
              dmCupomFiscal.dbQuery2.ParamByName('pDataHoje').AsDate:=Date;
              dmCupomFiscal.dbQuery2.ParamByName('pDtVerLic').AsDate:=Date;
              dmCupomFiscal.dbQuery2.ExecSQL;
              dmCupomFiscal.Transaction.Commit;
              fechaFormVerificaLicenca();
              dmCupomFiscal.fStatusLicenca := 'L';
            end
            else
            begin
              if (dmCupomFiscal.qurLicenca.FieldByName('ativo').AsString = 'N') then //Notificado
              begin
                msg := dmCupomFiscal.qurLicenca.FieldByName('msg_notificacao').AsString;
                dmCupomFiscal.dbQuery2.Active:=False;
                dmCupomFiscal.dbQuery2.SQL.Clear;
                dmCupomFiscal.dbQuery2.SQL.Add('update locais set data_exp = :pDataHoje, '+
                  'DATA_ULT_VER_LICENCA = :pDtVerLic Where ID=:pID');
                dmCupomFiscal.dbQuery2.ParamByName('pID').AsInteger:=dmCupomFiscal.dbQuery1.FieldByName('id').AsInteger;
                dmCupomFiscal.dbQuery2.ParamByName('pDataHoje').AsDate:=Date;
                dmCupomFiscal.dbQuery2.ParamByName('pDtVerLic').AsDate:=Date;
                dmCupomFiscal.dbQuery2.ExecSQL;
                dmCupomFiscal.Transaction.Commit;
                fechaFormVerificaLicenca();
                logErros(Self, caminhoLog, msg , 'Conectado. Notifica��o:'+msg, 'S',nil );
                dmCupomFiscal.fStatusLicenca := 'N';
              end
              else
              begin
                if (dmCupomFiscal.qurLicenca.FieldByName('ativo').AsString = 'B') then
                begin
                  fechaFormVerificaLicenca();
                  logErros(Self, caminhoLog,'Per�odo de licen�a expirou!'+#13+
                                            'Sistema ir� reverter para o modo de consulta.'+#13+#13+
                                            'Favor contatar o Suporte.',
                                            'Conectado. Per�odo de licen�a expirou!'+#13+
                                            'Favor contatar Suporte.', 'S',nil );
                  dmCupomFiscal.fStatusLicenca := 'B';
                end;
              end;
            end;
          end;
          //Ap�s fazer tudo desconecta da base de licen�a
          desconectaLicenca();
        end
        else //N�o conseguiu conex�o com o BD Licen�a
        begin
          //Se Data Atual � maior que DataExpira+10 -- Pasou 10 dias
          if (Date > ( dmCupomFiscal.dbQuery1.FieldByName('data_exp').AsDateTime + 10))then
          begin
            fechaFormVerificaLicenca();
            logErros(Self, caminhoLog,'N�o foi poss�vel conectar com servidor de licen�a',
                                      'Desconectado. N�o foi poss�vel conectar com servidor de licen�a. '+
                                      'E expirou per�odo de 10 dias', 'S', nil);
            Abort;
          end
          else
          begin
            fechaFormVerificaLicenca();
            logErros(Self, caminhoLog,'', 'Desconectado. N�o foi poss�vel conectar com servidor de licen�a. '+
                                          'Mas esta dentro do per�odo de 10 dias', 'S', nil);
            dmCupomFiscal.fStatusLicenca := 'E'; //Expirado por�m na data permitida
          end;
        end;
      end;
      //Gambiara pq por algum motivo perdia as fields ***Segundo HUELLISSON Solu��o Alternativa(SA)
      dmCupomFiscal.dbQuery3.Active:=False;
      dmCupomFiscal.dbQuery3.SQL.Clear;
      dmCupomFiscal.dbQuery3.SQL.Add('Select U.*,F.Nome F_Nome, F.ativo, F.nfce_caixa, F.nfce_uso'+
          ' From Usuarios U, Funcionarios F'+
          ' Where (U.Nome=:pNome) and (U.Senha=:pSenha) and (U.Funcionario=F.Id)');
      dmCupomFiscal.dbQuery3.ParamByName('pNome').AsString:=Trim(edtUsuario.Text);
      dmCupomFiscal.dbQuery3.ParamByName('pSenha').AsString:=Trim(edtSenha.Text);
      dmCupomFiscal.dbQuery3.Active:=True;
      dmCupomFiscal.dbQuery3.First;
      Application.ProcessMessages;
      if dmCupomFiscal.dbQuery3.FieldByName('ativo').AsString='S' then
      begin
        if StrToIntDef(dmCupomFiscal.dbQuery3.FieldByName('nfce_caixa').AsString, 0)=0 then
          msgInformacao(Format('Funcion�rio %s n�o possui nenhum caixa configurado',[dmCupomFiscal.dbQuery3.FieldByName('F_Nome').AsString]),'')
        else
        begin
          if dmCupomFiscal.dbQuery3.FieldByName('nfce_uso').AsString='S' then
          begin
            if not (msgPergunta('Caixa '+dmCupomFiscal.dbQuery3.FieldByName('nfce_caixa').AsString+' ja esta em Uso. Deseja mesmo assim inici�-lo ?','Confirma��o')) then
              Abort;
          end;
          FRENTE_CAIXA.Caixa    := dmCupomFiscal.dbQuery3.FieldByName('nfce_caixa').AsInteger;
          LOGIN.usuarioNivel    := dmCupomFiscal.dbQuery3.FieldByName('Nivel').AsString;
          LOGIN.usuarioNome     := dmCupomFiscal.dbQuery3.FieldByName('Nome').AsString;
          LOGIN.usuarioCod      := dmCupomFiscal.dbQuery3.FieldByName('Funcionario').AsInteger;
          LOGIN.usuarioFuncNome := dmCupomFiscal.dbQuery3.FieldByName('F_Nome').AsString;
          setCaixaUso(LOGIN.usuarioCod, 'S');
          frmPrincipal.barStatus.Panels[1].Text:='Operador: '+IntToStr(LOGIN.usuarioCod)+'/'+LOGIN.usuarioFuncNome;
          Close;
        end;
      end
      else
      begin
        msgInformacao('N�o � poss�vel logar no sistema com esse Usu�rio e Senha'+sLineBreak+
                      'Funcion�rio esta Inativo','');
        edtUsuario.SetFocus;
        Abort;
      end;
    end;
  end;
end;

procedure TfrmLogin.AbreFormVerificaLicenca;
begin
  Application.CreateForm(TfrmVerificandoLicenca, frmVerificandoLicenca);
  frmVerificandoLicenca.Show; //Ir� fazer a Tela de Splash aparecer
  frmVerificandoLicenca.Refresh; // Faz com que a tela atualize
end;


procedure TfrmLogin.btnCancelarClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmLogin.FechaFormVerificaLicenca;
begin
  frmVerificandoLicenca.Release; // Elimina a tela depois na mem�ria
  //frmVerificandoLicenca.Free;
  frmVerificandoLicenca := nil; // Ir� anular a referencia ao ponteiro do objeto
end;

procedure TfrmLogin.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  frmPrincipal.Enabled:=True;
end;

procedure TfrmLogin.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=vk_Escape then
    Close;
end;

function TfrmLogin.pingServidor(const Host: string; const Port, TimeOut: Integer):Boolean;
var
  TCPClient: TIdTCPClient;
begin
  TCPClient := TIdTCPClient.Create(nil);
  try
    try
      TCPClient.Host := Host;
      TCPClient.Port := Port;
      //7 TCPClient.Connect(TimeOut);
      TCPClient.Connect;  //7
      Result := TCPClient.Connected;
    except
      on e: Exception do
      begin
        logErros(nil, caminhoLog, '','Sem comunica��o com servidor '+Host+':'+IntToStr(Port), 'S', E);
        Result := False;
      end;
    end;
  finally
    TCPClient.Free;
  end;
end;

end.
