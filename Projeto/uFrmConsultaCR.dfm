object frmConsultaCR: TfrmConsultaCR
  Left = 474
  Top = 269
  BorderIcons = [biSystemMenu]
  Caption = 'Consulta Contas Vencidas'
  ClientHeight = 406
  ClientWidth = 413
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDesktopCenter
  OnKeyDown = FormKeyDown
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object DBGrid1: TDBGrid
    Left = 0
    Top = 0
    Width = 413
    Height = 372
    Align = alClient
    Ctl3D = False
    DataSource = DataSource1
    DrawingStyle = gdsClassic
    FixedColor = clYellow
    Options = [dgTitles, dgColumnResize, dgColLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    ParentCtl3D = False
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'SDOC'
        Title.Alignment = taCenter
        Title.Caption = 'S'#233'rie'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NDOC'
        Title.Alignment = taCenter
        Title.Caption = 'N'#250'mero'
        Width = 45
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PARCELA'
        Title.Alignment = taCenter
        Title.Caption = 'Nr.Par'
        Width = 40
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DATA_OPE'
        Title.Alignment = taCenter
        Title.Caption = 'Opera'#231#227'o'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DATA_VEN'
        Title.Alignment = taCenter
        Title.Caption = 'Vencimento'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'VALOR'
        Title.Alignment = taCenter
        Title.Caption = 'Valor R$'
        Width = 70
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'RESTA'
        Title.Alignment = taCenter
        Title.Caption = 'Resta R$'
        Width = 70
        Visible = True
      end>
  end
  object Panel1: TPanel
    Left = 0
    Top = 372
    Width = 413
    Height = 34
    Align = alBottom
    Ctl3D = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentCtl3D = False
    ParentFont = False
    TabOrder = 1
    object Label1: TLabel
      Left = 183
      Top = 9
      Width = 60
      Height = 16
      Caption = 'Total R$'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object reValor: TRealEdit
      Left = 247
      Top = 7
      Width = 73
      Height = 20
      Alignment = taRightJustify
      Ctl3D = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 0
      FormatReal = fNumber
      FormatSize = '10.2'
    end
    object reResta: TRealEdit
      Left = 319
      Top = 7
      Width = 71
      Height = 20
      Alignment = taRightJustify
      Ctl3D = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 1
      FormatReal = fNumber
      FormatSize = '10.2'
    end
  end
  object DataSource1: TDataSource
    DataSet = dmCupomFiscal.qConsulta_CR
    Left = 128
    Top = 136
  end
end
