unit uFrmImpComprovaSangriaSup;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, RLReport, DB;

type
  TfrmImpCompSangSup = class(TForm)
    rlVenda: TRLReport;
    rlbDadosCliche: TRLBand;
    pLogoeCliche: TRLPanel;
    lEmitCNPJ_IE_IM: TRLLabel;
    lEndereco: TRLMemo;
    RLDraw1: TRLDraw;
    imgLogo: TRLImage;
    lNomeFantasia: TRLMemo;
    lRazaoSocial: TRLMemo;
    rlBSubTitulo: TRLBand;
    RLLabel1: TRLLabel;
    RLDraw5: TRLDraw;
    RLBand1: TRLBand;
    RLLabel2: TRLLabel;
    RLLabel3: TRLLabel;
    RLLabel4: TRLLabel;
    RLLabel6: TRLLabel;
    dsSanSup: TDataSource;
    RLLabel7: TRLLabel;
    RLLabel8: TRLLabel;
    RLLabel9: TRLLabel;
    RLLabel11: TRLLabel;
    RLDraw2: TRLDraw;
    rlbRodape: TRLBand;
    RLLabel5: TRLLabel;
    RLLabel10: TRLLabel;
    RLLabel12: TRLLabel;
    RLLabel13: TRLLabel;
    RLLabel14: TRLLabel;
    function GetDadosCabecalho: String;
    function GetDadosEndereco:String;
    Procedure GetLocal;
    procedure lNomeFantasiaBeforePrint(Sender: TObject; var Text: String;
      var PrintIt: Boolean);
    procedure lRazaoSocialBeforePrint(Sender: TObject; var Text: String;
      var PrintIt: Boolean);
    procedure lEmitCNPJ_IE_IMBeforePrint(Sender: TObject; var Text: String;
      var PrintIt: Boolean);
    procedure lEnderecoBeforePrint(Sender: TObject; var Text: String;
      var PrintIt: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure RLLabel5BeforePrint(Sender: TObject; var Text: String;
      var PrintIt: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmImpCompSangSup: TfrmImpCompSangSup;

implementation

uses
  uFrmPrincipal, uDMCupomFiscal, uRotinasGlobais;

{$R *.dfm}

function TfrmImpCompSangSup.GetDadosCabecalho: String;
var
  CNPJ_IE_IM: String;
begin
  CNPJ_IE_IM := 'CNPJ:'+DMCupomFiscal.dbQuery3.fieldByName('cnpj').AsSTring;
  if (DMCupomFiscal.dbQuery3.fieldByName('IE').AsSTring <> '') then
    CNPJ_IE_IM := CNPJ_IE_IM + ' IE:'+DMCupomFiscal.dbQuery3.fieldByName('IE').AsSTring;
  if (DMCupomFiscal.dbQuery3.fieldByName('IM').AsSTring <> '') then
    CNPJ_IE_IM := CNPJ_IE_IM + ' IM:'+DMCupomFiscal.dbQuery3.fieldByName('IM').AsSTring;
  Result := CNPJ_IE_IM;
end;

function TfrmImpCompSangSup.GetDadosEndereco: String;
var
  Endereco, CEP: String;
begin
    // Definindo dados do Cliche //
    Endereco := dmCupomFiscal.dbQuery3.FieldByName('Endereco').asString ;
    if (dmCupomFiscal.dbQuery3.FieldByName('Numero_End').asString <> '') then
      Endereco := Endereco + ', '+dmCupomFiscal.dbQuery3.FieldByName('Numero_End').asString;
    if (dmCupomFiscal.dbQuery3.FieldByName('complemento_end').asString <> '') then
      Endereco := Endereco + ' - '+dmCupomFiscal.dbQuery3.FieldByName('complemento_end').asString;
    if (dmCupomFiscal.dbQuery3.FieldByName('Bairro').asString <> '') then
      Endereco := Endereco + ' - '+dmCupomFiscal.dbQuery3.FieldByName('Bairro').asString;
    if (dmCupomFiscal.dbQuery3.FieldByName('Municipio').asString <> '') then
      Endereco := Endereco + ' - '+dmCupomFiscal.dbQuery3.FieldByName('Descricao').asString;
    if (dmCupomFiscal.dbQuery3.FieldByName('UF').asString <> '') then
      Endereco := Endereco + ' - '+dmCupomFiscal.dbQuery3.FieldByName('UF').asString;
    if (dmCupomFiscal.dbQuery3.FieldByName('Cep').asString <> '') then
      Endereco := Endereco + ' - '+dmCupomFiscal.dbQuery3.FieldByName('Cep').asString;
    if (dmCupomFiscal.dbQuery3.FieldByName('Fone').asString <> '') then
      Endereco := Endereco + ' - FONE: '+dmCupomFiscal.dbQuery3.FieldByName('Fone').asString;
  Result := Endereco;
end;

procedure TfrmImpCompSangSup.GetLocal;
begin
  dmCupomFiscal.dbQuery3.Active:=False;
  dmCupomFiscal.dbQuery3.SQL.Clear;
  dmCupomFiscal.dbQuery3.SQL.Add('Select l.descricao, l.cnpj, l.nome_fantasia, l.IE, l.IM, '+
        'l.endereco, l.numero_end, l.complemento_end, l.bairro, m.descricao as Municipio, l.uf, l.cep, l.fone  from locais l '+
        'left outer join ccustos c on c.local=l.id '+
        'left outer join Municipios m on m.id=l.municipio '+
        'where c.id=:pCCusto');
  dmCupomFiscal.dbQuery3.ParamByName('pCCusto').AsInteger := CCUSTO.codigo;
  dmCupomFiscal.dbQuery3.Active:=True;
  dmCupomFiscal.dbQuery3.First;
end;

procedure TfrmImpCompSangSup.lNomeFantasiaBeforePrint(Sender: TObject;
  var Text: String; var PrintIt: Boolean);
begin
  Text := dmCupomFiscal.dbQuery3.FieldByName('Descricao').asString;
end;

procedure TfrmImpCompSangSup.lRazaoSocialBeforePrint(Sender: TObject;
  var Text: String; var PrintIt: Boolean);
begin
  Text := dmCupomFiscal.dbQuery3.FieldByName('nome_fantasia').asString;
end;

procedure TfrmImpCompSangSup.lEmitCNPJ_IE_IMBeforePrint(Sender: TObject;
  var Text: String; var PrintIt: Boolean);
begin
  Text := GetDadosCabecalho;
end;

procedure TfrmImpCompSangSup.lEnderecoBeforePrint(Sender: TObject;
  var Text: String; var PrintIt: Boolean);
begin
  Text := GetDadosEndereco;
end;

procedure TfrmImpCompSangSup.FormCreate(Sender: TObject);
begin
  GetLocal;
end;

procedure TfrmImpCompSangSup.RLLabel5BeforePrint(Sender: TObject;
  var Text: String; var PrintIt: Boolean);
begin
  Text := DateTimeToStr(Now)+'  ';
end;

end.

