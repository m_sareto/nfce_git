object Frm_Imp_Fatura: TFrm_Imp_Fatura
  Left = 258
  Top = 33
  Caption = 'NFC-e - Comprovante de D'#233'bito'
  ClientHeight = 643
  ClientWidth = 963
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object RLReport1: TRLReport
    Left = 328
    Top = 16
    Width = 272
    Height = 1039
    Margins.LeftMargin = 2.000000000000000000
    Margins.TopMargin = 2.000000000000000000
    Margins.RightMargin = 2.000000000000000000
    Margins.BottomMargin = 2.000000000000000000
    AllowedBands = [btHeader, btDetail, btSummary]
    Borders.Sides = sdCustom
    Borders.DrawLeft = False
    Borders.DrawTop = False
    Borders.DrawRight = False
    Borders.DrawBottom = False
    DataSource = DataSource1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -9
    Font.Name = 'Arial'
    Font.Style = []
    PageSetup.PaperSize = fpCustom
    PageSetup.PaperWidth = 72.000000000000000000
    PageSetup.PaperHeight = 275.000000000000000000
    PageSetup.ForceEmulation = True
    PrintDialog = False
    BeforePrint = RLReport1BeforePrint
    object RLGroup2: TRLGroup
      Left = 8
      Top = 8
      Width = 256
      Height = 307
      object RLBand2: TRLBand
        Left = 0
        Top = 81
        Width = 256
        Height = 24
        AutoSize = True
        BandType = btHeader
        object RLMemo1: TRLMemo
          Left = 0
          Top = 0
          Width = 256
          Height = 24
          Align = faClientTop
          Alignment = taCenter
          Behavior = [beSiteExpander]
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Layout = tlCenter
          Lines.Strings = (
            'N'#195'O '#201' DOCUMENTO FISCAL'
            'COMPROVANTE DE CR'#201'DITO OU D'#201'BITO '#192' PRAZO')
          ParentFont = False
        end
      end
      object RLBand3: TRLBand
        Left = 0
        Top = 105
        Width = 256
        Height = 48
        AutoSize = True
        BandType = btHeader
        object RLLabel1: TRLLabel
          Left = 0
          Top = 8
          Width = 88
          Height = 12
          Align = faLeftTop
          Caption = 'Valor da Compra R$'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object RLLabel3: TRLLabel
          Left = 206
          Top = 8
          Width = 36
          Height = 12
          Alignment = taRightJustify
          Caption = 'Compra'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          BeforePrint = RLLabel3BeforePrint
        end
        object RLLabel4: TRLLabel
          Left = 0
          Top = 22
          Width = 81
          Height = 12
          Align = faLeftOnly
          Caption = 'Valor Recebido R$'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object RLLabel5: TRLLabel
          Left = 216
          Top = 22
          Width = 26
          Height = 12
          Alignment = taRightJustify
          Caption = 'Num:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          BeforePrint = RLLabel5BeforePrint
        end
        object RLLabel6: TRLLabel
          Left = 0
          Top = 36
          Width = 57
          Height = 12
          Align = faLeftOnly
          Caption = 'Vencimentos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object RLLabel7: TRLLabel
          Left = 203
          Top = 36
          Width = 39
          Height = 12
          Alignment = taRightJustify
          Caption = 'Valor R$'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object RLDraw3: TRLDraw
          Left = 0
          Top = 0
          Width = 256
          Height = 8
          Align = faTop
          DrawKind = dkLine
          Pen.Width = 2
        end
      end
      object RLBand6: TRLBand
        Left = 0
        Top = 0
        Width = 256
        Height = 81
        AutoSize = True
        BandType = btHeader
        object RLDraw1: TRLDraw
          Left = 0
          Top = 73
          Width = 256
          Height = 8
          Align = faBottom
          DrawKind = dkLine
          Pen.Width = 2
        end
        object RLDraw14: TRLDraw
          Left = 0
          Top = 53
          Width = 256
          Height = 8
          Align = faTop
          DrawKind = dkLine
          Pen.Width = 2
        end
        object lEnderecoCanc: TRLMemo
          Left = 0
          Top = 41
          Width = 256
          Height = 12
          Align = faTop
          Alignment = taCenter
          Behavior = [beSiteExpander]
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Lines.Strings = (
            'Endere'#231'o')
          ParentFont = False
          BeforePrint = lEnderecoCancBeforePrint
        end
        object lEmitCNPJ_IE_IM_Camc: TRLLabel
          Left = 0
          Top = 29
          Width = 256
          Height = 12
          Align = faTop
          Alignment = taCenter
          Caption = 
            'CNPJ: 22.222.222/22222-22  IE:223.233.344.233 IM:2323.222.333.23' +
            '3'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Layout = tlBottom
          ParentFont = False
          BeforePrint = lEmitCNPJ_IE_IM_CamcBeforePrint
        end
        object lRazaoSocialCanc: TRLMemo
          Left = 0
          Top = 17
          Width = 256
          Height = 12
          Align = faTop
          Alignment = taCenter
          Behavior = [beSiteExpander]
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Lines.Strings = (
            'Raz'#227'o Social')
          ParentFont = False
          BeforePrint = lRazaoSocialCancBeforePrint
        end
        object lNomeFantasiaCanc: TRLMemo
          Left = 0
          Top = 0
          Width = 256
          Height = 17
          Align = faTop
          Alignment = taCenter
          Behavior = [beSiteExpander]
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Layout = tlCenter
          Lines.Strings = (
            'Nome Fantasia')
          ParentFont = False
          BeforePrint = lNomeFantasiaCancBeforePrint
        end
        object Data: TRLLabel
          Left = 0
          Top = 61
          Width = 23
          Height = 12
          Align = faLeftTop
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          BeforePrint = DataBeforePrint
        end
        object RLLabel10: TRLLabel
          Left = 209
          Top = 61
          Width = 47
          Height = 12
          Align = faClientRight
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          BeforePrint = RLLabel10BeforePrint
        end
      end
      object RLBand4: TRLBand
        Left = 0
        Top = 153
        Width = 256
        Height = 14
        AutoSize = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        object RLData_Venc: TRLDBText
          Left = 0
          Top = 2
          Width = 27
          Height = 12
          DataField = 'DATA'
          DataSource = DataSource1
          Text = ''
        end
        object RLValor_Venc: TRLDBText
          Left = 209
          Top = 2
          Width = 34
          Height = 12
          Alignment = taRightJustify
          DataField = 'VALOR'
          DataSource = DataSource1
          Text = ''
        end
      end
      object RLBand5: TRLBand
        Left = 0
        Top = 193
        Width = 256
        Height = 138
        Margins.TopMargin = 1.000000000000000000
        BandType = btSummary
        object RLDraw4: TRLDraw
          Left = 0
          Top = 3
          Width = 256
          Height = 6
          Align = faWidth
          DrawKind = dkLine
        end
        object RLTotal: TRLLabel
          Left = 231
          Top = 8
          Width = 25
          Height = 11
          Align = faRightOnly
          Alignment = taRightJustify
          Caption = 'Total'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          BeforePrint = RLTotalBeforePrint
        end
        object RLLabel9: TRLLabel
          Left = 0
          Top = 8
          Width = 40
          Height = 11
          Align = faLeftOnly
          Caption = 'Total R$'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object RLMemo2: TRLMemo
          Left = 0
          Top = 19
          Width = 256
          Height = 36
          Align = faWidth
          Alignment = taJustify
          Behavior = [beSiteExpander]
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Lines.Strings = (
            
              'Reconhe'#231'o o presente como documento de d'#237'vida em meu nome e comp' +
              'rometo-me a pagar no(s) vencimento(s) acima estipulado(s). ')
          ParentFont = False
        end
        object RLDraw2: TRLDraw
          Left = 0
          Top = 76
          Width = 256
          Height = 6
          Align = faWidth
          DrawKind = dkLine
        end
        object RLLabel8: TRLLabel
          Left = 105
          Top = 79
          Width = 42
          Height = 12
          Alignment = taCenter
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          BeforePrint = RLLabel8BeforePrint
        end
        object RLMemo3: TRLMemo
          Left = 0
          Top = 96
          Width = 256
          Height = 12
          Align = faWidth
          Alignment = taCenter
          Behavior = [beSiteExpander]
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          BeforePrint = RLMemo3BeforePrint
        end
        object RLLabel2: TRLLabel
          Left = 0
          Top = 126
          Width = 256
          Height = 12
          Align = faBottom
          Alignment = taRightJustify
          Caption = 'Operador:'
          BeforePrint = RLLabel2BeforePrint
        end
      end
      object RLSubDetail1: TRLSubDetail
        Left = 0
        Top = 167
        Width = 256
        Height = 26
        DataSource = DataSource2
        BeforePrint = RLSubDetail1BeforePrint
        object RLBand1: TRLBand
          Left = 0
          Top = 0
          Width = 256
          Height = 15
          AutoSize = True
          BandType = btHeader
          object RLLabel11: TRLLabel
            Left = 0
            Top = 3
            Width = 48
            Height = 12
            Caption = 'Mercadoria'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -9
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
          end
          object RLLabel12: TRLLabel
            Left = 142
            Top = 3
            Width = 37
            Height = 12
            Caption = 'Vlr. Unit'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -9
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
          end
          object RLLabel13: TRLLabel
            Left = 196
            Top = 3
            Width = 18
            Height = 12
            Caption = 'Qtd'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -9
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
          end
          object RLLabel14: TRLLabel
            Left = 233
            Top = 3
            Width = 23
            Height = 12
            Caption = 'Total'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -9
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
          end
          object RLDraw5: TRLDraw
            Left = 0
            Top = 0
            Width = 256
            Height = 6
            Align = faTop
            DrawKind = dkLine
          end
        end
        object RLBand7: TRLBand
          Left = 0
          Top = 15
          Width = 256
          Height = 14
          AutoSize = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          object RLDBText1: TRLDBText
            Left = 0
            Top = 2
            Width = 59
            Height = 12
            DataField = 'DESCRICAO'
            DataSource = DataSource2
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -9
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
            Text = ''
          end
          object RLDBText2: TRLDBText
            Left = 225
            Top = 2
            Width = 31
            Height = 12
            Alignment = taRightJustify
            DataField = 'TOTAL'
            DataSource = DataSource2
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -9
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
            Text = ''
          end
          object RLDBText3: TRLDBText
            Left = 158
            Top = 2
            Width = 21
            Height = 12
            Alignment = taRightJustify
            DataField = 'VLR'
            DataSource = DataSource2
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -9
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
            Text = ''
          end
          object RLDBText4: TRLDBText
            Left = 193
            Top = 2
            Width = 22
            Height = 12
            Alignment = taRightJustify
            DataField = 'QTD'
            DataSource = DataSource2
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -9
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
            Text = ''
          end
        end
      end
    end
  end
  object qFatura: TIBQuery
    Database = dmCupomFiscal.DataBase
    Transaction = dmCupomFiscal.Transaction
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'select * from EST_ECF_FATURA where  ID_ECF=:pID_ECF')
    Left = 160
    Top = 144
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'pID_ECF'
        ParamType = ptUnknown
      end>
    object qFaturaID_ECF: TIntegerField
      FieldName = 'ID_ECF'
      Origin = '"EST_ECF_FATURA"."ID_ECF"'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object qFaturaDATA: TDateField
      FieldName = 'DATA'
      Origin = '"EST_ECF_FATURA"."DATA"'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object qFaturaVALOR: TIBBCDField
      FieldName = 'VALOR'
      Origin = '"EST_ECF_FATURA"."VALOR"'
      Required = True
      DisplayFormat = '##,##0.00'
      Precision = 18
      Size = 2
    end
    object qFaturaNDUP: TIBStringField
      FieldName = 'NDUP'
      Origin = '"EST_ECF_FATURA"."NDUP"'
      Size = 60
    end
    object qFaturaPARCELA: TIntegerField
      FieldName = 'PARCELA'
      Origin = '"EST_ECF_FATURA"."PARCELA"'
      Required = True
    end
  end
  object DataSource1: TDataSource
    DataSet = qFatura
    Left = 32
    Top = 304
  end
  object qMercadorias: TIBQuery
    Database = dmCupomFiscal.DataBase
    Transaction = dmCupomFiscal.Transaction
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      
        'select SubString(m.DESCRICAO from 1 for 24) as DESCRICAO, i.VLR,' +
        ' i.QTD, i.TOTAL'
      
        'from EST_ECF_ITEM i, EST_MERCADORIAS m where i.MERCADORIA = m.ID' +
        ' and  ID_ECF=:pID_ECF')
    Left = 190
    Top = 324
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'pID_ECF'
        ParamType = ptUnknown
      end>
    object qMercadoriasVLR: TFloatField
      FieldName = 'VLR'
      Origin = '"EST_ECF_ITEM"."VLR"'
      Required = True
      DisplayFormat = '###,##0.00'
    end
    object qMercadoriasQTD: TIBBCDField
      FieldName = 'QTD'
      Origin = '"EST_ECF_ITEM"."QTD"'
      Required = True
      DisplayFormat = '###,##0.000'
      Precision = 18
      Size = 3
    end
    object qMercadoriasTOTAL: TIBBCDField
      FieldName = 'TOTAL'
      Origin = '"EST_ECF_ITEM"."TOTAL"'
      DisplayFormat = '###,##0.00'
      Precision = 18
      Size = 2
    end
    object qMercadoriasDESCRICAO: TIBStringField
      FieldName = 'DESCRICAO'
      ProviderFlags = []
      Size = 100
    end
  end
  object DataSource2: TDataSource
    DataSet = qMercadorias
    Left = 118
    Top = 374
  end
end
