unit uFrmAjustarNcm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ImgList, ExtCtrls, StdCtrls, ComCtrls, Buttons, System.ImageList;

type
  TfrmAjustarNcm = class(TForm)
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    edtNcmAtual: TEdit;
    lblNcmAtual: TLabel;
    lblNcmNovo: TLabel;
    edtNcmNovo: TEdit;
    Image1: TImage;
    Image2: TImage;
    imgLista: TImageList;
    btnSair: TBitBtn;
    btnOK: TBitBtn;
    procedure edtNcmNovoChange(Sender: TObject);
    procedure lblNcmNovoClick(Sender: TObject);
    procedure btnOKClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure edtNcmAtualChange(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btnSairClick(Sender: TObject);
    procedure edtNcmNovoKeyPress(Sender: TObject; var Key: Char);
    procedure btnOKKeyPress(Sender: TObject; var Key: Char);
  private
    yID : String;
    { Private declarations }
  public
    yNCM_NOVO : String;
    constructor Create(AOwner: TComponent; NCM:String; Descricao:String; ID:String);
    reintroduce;
    { Public declarations }
  end;
var
  frmAjustarNcm: TfrmAjustarNcm;

implementation

uses
  uFrmPrincipal, uDMCupomFiscal, uFrmPesquisa, uFuncoes;

{$R *.dfm}

constructor TfrmAjustarNcm.Create(AOwner: TComponent; NCM: String; Descricao: String; ID: String);
begin
  inherited Create(AOwner);
  edtNcmAtual.Text := NCM;
  Panel1.Caption   := Descricao;
  yID := ID;
end;

procedure TfrmAjustarNcm.edtNcmNovoChange(Sender: TObject);
var
  bmp :TBitMap;
begin
  bmp := TBitMap.Create;
  try
    if(frmPrincipal.Valida_NCM(trim(edtNcmNovo.Text)))then
    begin
      imgLista.GetBitmap(0, bmp);
      btnOK.Enabled := True;
    end
    else
    begin
      imgLista.GetBitmap(1, bmp);
      btnOK.Enabled := False;
    end;
    if bmp.Handle > 0 then
    begin
      Image1.Picture.Bitmap.Assign(bmp);
      Image1.Refresh;
    end;
  finally
    bmp.Free;
  end;
end;

procedure TfrmAjustarNcm.lblNcmNovoClick(Sender: TObject);
begin
  frmPesquisa := tfrmPesquisa.Create(Self,'select codigo, descricao from ibpt where (upper(descricao) >= :pTexto) and char_Length(codigo) = 8 order by codigo',
                 'select codigo, descricao from ibpt where (upper(descricao) like :pTexto) and char_Length(codigo) = 8 order by codigo');

  FrmPesquisa.Width:=650;
  FrmPesquisa.CheckBox1.Checked:=False;

  frmPesquisa.DBGrid1.Columns.Add;
  frmPesquisa.DBGrid1.Columns[0].Title.Caption:='NCM';
  frmPesquisa.DBGrid1.Columns[0].Width:=55;
  frmPesquisa.DBGrid1.Columns[0].FieldName:='codigo';

  frmPesquisa.DBGrid1.Columns.Add;
  frmPesquisa.DBGrid1.Columns[1].Title.Caption:='Descri��o';
  frmPesquisa.DBGrid1.Columns[1].Width:=550;
  frmPesquisa.DBGrid1.Columns[1].FieldName:='Descricao';
  try
    if (frmPesquisa.ShowModal=mrOK) then
    begin
      edtNcmNovo.Text:= FrmPesquisa.IBQuery1.FieldByName('codigo').AsString;
      StatusBar1.Panels[0].Text := FrmPesquisa.IBQuery1.FieldByName('Descricao').AsString;
    end;
  finally
    frmPesquisa.Free;
  end;
end;

procedure TfrmAjustarNcm.btnOKClick(Sender: TObject);
begin
  try
    dmCupomFiscal.IBSQL1.Close;
    dmCupomFiscal.IBSQL1.SQL.Clear;
    dmCupomFiscal.IBSQL1.SQL.Add('Update est_mercadorias set NCM = :pNCM where ID = :pID');
    dmCupomFiscal.IBSQL1.ParamByName('pNCM').AsString := edtNcmNovo.Text;
    dmCupomFiscal.IBSQL1.ParamByName('pID').AsString  := yID;
    dmCupomFiscal.IBSQL1.ExecQuery;
    dmCupomFiscal.Transaction.CommitRetaining;
    yNCM_NOVO := edtNcmNovo.Text;
    ModalResult := mrOK;
  except
    on E:Exception do
    begin
      logErros(Sender, caminhoLog, 'Erro ao atualizar NCM da mercadoria','Erro ao atualizar NCM da mercadoria','S',E);
      ModalResult := mrCancel;      
    end;
  end;
end;

procedure TfrmAjustarNcm.FormShow(Sender: TObject);
begin
  edtNcmNovoChange(action);
  edtNcmNovoChange(action);
end;

procedure TfrmAjustarNcm.edtNcmAtualChange(Sender: TObject);
var
  bmp :TBitMap;
begin
  bmp := TBitMap.Create;
  try
    if(frmPrincipal.Valida_NCM(trim(edtNcmAtual.Text)))then
      imgLista.GetBitmap(0, bmp)
    else
      imgLista.GetBitmap(1, bmp);
    if bmp.Handle > 0 then
    begin
      Image2.Picture.Bitmap.Assign(bmp);
      Image2.Refresh;
    end;
  finally
    bmp.Free;
  end;
end;

procedure TfrmAjustarNcm.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if(key = vk_escape)then close;

  if(key = Vk_f12) and edtNcmNovo.Focused then lblNcmNovoClick(Action);
end;

procedure TfrmAjustarNcm.btnSairClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmAjustarNcm.edtNcmNovoKeyPress(Sender: TObject;
  var Key: Char);
begin
  if not (somenteNumeros(Key)) then
    Key := #0; //No onKeyPess bloqueia tecla}
end;

procedure TfrmAjustarNcm.btnOKKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #13 then
    btnOKClick(Action);
end;

end.
