unit UFrmTurno_Geral;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, Vcl.Mask;

type
  TFrmTurno_Geral = class(TForm)
    edtTurno: TEdit;
    Label1: TLabel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Label2: TLabel;
    edtFun: TEdit;
    Edit2: TEdit;
    mkDH_Ini: TMaskEdit;
    mkDH_Fim: TMaskEdit;
    Label3: TLabel;
    Label4: TLabel;
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure edtFunExit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmTurno_Geral: TFrmTurno_Geral;

implementation

uses uFuncoes, uDMCupomFiscal, uFrmLogin;

{$R *.dfm}

procedure TFrmTurno_Geral.BitBtn1Click(Sender: TObject);
begin
  try
    dmCupomFiscal.IBSQL1.Close;
    dmCupomFiscal.IBSQL1.SQL.Clear;
    dmCupomFiscal.IBSQL1.SQL.Add('Insert Into Turnos (id, Fun_Caixa, Data_Hora_Ini, Data_Hora_Fim)'+
                                 ' Values(:pTurno, :pFun_Caixa, :pDH_Ini,:pDH_Fim)');
    dmCupomFiscal.IBSQL1.ParamByName('pTurno').AsString:=Trim(edtTurno.Text);
    dmCupomFiscal.IBSQL1.ParamByName('pFun_Caixa').AsInteger:=StrToIntDef(edtFun.Text,0);
    dmCupomFiscal.IBSQL1.ParamByName('pDH_ini').AsDateTime:=StrToDateTimeDef(mkDH_Ini.Text,Date);
    dmCupomFiscal.IBSQL1.ParamByName('pDH_fim').AsDateTime:=StrToDateTimeDef(mkDH_Fim.Text,Date);
    dmCupomFiscal.IBSQL1.ExecQuery;
    dmCupomFiscal.IBSQL1.Close;

    dmCupomFiscal.IBSQL1.Close;
    dmCupomFiscal.IBSQL1.SQL.Clear;
    dmCupomFiscal.IBSQL1.SQL.Add('Update Funcionarios_Turnos Set Turno=:pTurno');
    dmCupomFiscal.IBSQL1.ParamByName('pTurno').asString:=Trim(edtTurno.Text);
    dmCupomFiscal.IBSQL1.ExecQuery;
    dmCupomFiscal.IBSQL1.Close;

    dmCupomFiscal.Transaction.CommitRetaining;

    frmLogin:=TfrmLogin.create(Application);
    frmLogin.ShowModal;
    frmLogin.Free;

    ShowMessage('Turnos Alterados com Sucesso!');
  except
    on E:Exception do
    begin
      logErros(Self, caminhoLog,'Erro ao Alterar os Turnos', 'Erro ao Alterar os Turnos','S',E);
      Abort;
    end;
  end;
  Close;
end;

procedure TFrmTurno_Geral.BitBtn2Click(Sender: TObject);
begin
  Close;
end;

procedure TFrmTurno_Geral.edtFunExit(Sender: TObject);
begin
  dmCupomFiscal.dbQuery1.Active:=False;
  dmCupomFiscal.dbQuery1.SQL.Clear;
  dmCupomFiscal.dbQuery1.SQL.Add('Select Nome from Funcionarios Where ID=:pID');
  dmCupomFiscal.dbQuery1.ParamByName('pID').AsInteger:=StrToIntDef(edtFun.Text,0);
  dmCupomFiscal.dbQuery1.Active:=True;
  dmCupomFiscal.dbQuery1.First;
  If dmCupomFiscal.dbQuery1.Eof then
  begin
    ShowMessage('Funcion�rio N�o Cadastrado...');
    Abort;
  end
  Else
    Edit2.Text:=dmCupomFiscal.dbQuery1.FieldByName('Nome').Text;
end;

procedure TFrmTurno_Geral.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  //Sair
  If (key=vk_Escape) then BitBtn2.Click;
end;

procedure TFrmTurno_Geral.FormShow(Sender: TObject);
begin
  mkDH_Ini.Text:=DateToStr(Date)+' 00:00:00';
  mkDH_Fim.Text:=DateToStr(Date)+' 23:59:59';
end;

end.
