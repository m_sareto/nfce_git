unit uFrmPesquisaGenerica;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, Grids, DBGrids, DB, IBCustomDataSet, IBQuery,
  ComCtrls, jpeg, ExtCtrls;

type
  TfrmPesquisaGenerica = class(TForm)
    DataSource1: TDataSource;
    IBQuery1: TIBQuery;
    DBGrid1: TDBGrid;
    StatusBar1: TStatusBar;
    Edit1: TEdit;
    CheckBox1: TCheckBox;
    Bevel3: TBevel;
    Label32: TLabel;
    procedure FormShow(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGrid1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Edit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGrid1Enter(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    constructor Create(AOwner: TComponent; xSql_Tabela:String);
    reintroduce;
  end;

var
  frmPesquisaGenerica: TfrmPesquisaGenerica;
  SQL_Tabela:String;

implementation

uses Math, uDMCupomFiscal, uFrmPrincipal;

{$R *.dfm}
constructor TfrmPesquisaGenerica.Create(AOwner: TComponent; xSql_Tabela:String);
begin
  inherited Create(AOwner);
  SQL_Tabela:=xSql_Tabela;

  if Trim(SQL_Tabela)<>'' then
  CheckBox1.Enabled:=Enabled
  else
  CheckBox1.Enabled:=False;
end;

procedure TfrmPesquisaGenerica.FormShow(Sender: TObject);
var TC: TComponent;
     T: TStringField;
     xx:sTRING;
begin
{ aDICIONAR RETIRAR CAMPOS dbGrid

TC := FindComponent('ibQuery1ID');
  if not (TC = nil) then
  begin
    IBQuery1.Close;
    TC.Free;
    ibQuery1.Open;
  end;

//---------------------
  IBQuery1.Close;
  T := TStringField.Create(Self);
  T.FieldName := 'ID';
  T.Name := IBQuery1.Name + T.FieldName;
  T.Index := IBQuery1.FieldCount;
  T.DataSet := IBQuery1;

  IBQuery1.FieldDefs.UpDate;
//  IBQuery1.Open;
//---------------------
}

  Edit1.SetFocus;
  CheckBox1.Checked := FRENTE_CAIXA.IniciePor;
end;

procedure TfrmPesquisaGenerica.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case Key of
    vk_Escape: ModalResult := mrCancel;
    vk_F12: FrmPesquisaGenerica.Edit1.SetFocus;
  end;
end;

procedure TfrmPesquisaGenerica.DBGrid1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (key = vk_Return) and not (DataSource1.DataSet.IsEmpty) then ModalResult := mrOK;
end;

procedure TfrmPesquisaGenerica.Edit1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (key in [vk_Down, vk_Up]) and not (DataSource1.DataSet.IsEmpty) then DBGrid1.SetFocus;
end;

procedure TfrmPesquisaGenerica.DBGrid1Enter(Sender: TObject);
var  i,c:integer;
begin
  if Trim(Edit1.Text)<>'' then
  begin
    IBQuery1.Close;
    IBQuery1.SQL.Clear;
    IBQuery1.SQL.Add('Select Id,Descricao From '+Trim(SQL_Tabela));

      If CheckBox1.Checked then
      begin
      IBQuery1.SQL.Add(' Where (Descricao>=:pTexto) Order By Descricao');
      IBQuery1.ParamByName('pTexto').AsString:=Trim(Edit1.Text);
      end
      ELSE
      begin
      IBQuery1.SQL.Add(' Where (Descricao like :pTexto) Order By Descricao');
      IBQuery1.ParamByName('pTexto').AsString:='%'+Trim(Edit1.Text)+'%';
      end;
    IBQuery1.Open;
    IBQuery1.First;
  end
  Else
  Edit1.SetFocus;
end;

procedure TfrmPesquisaGenerica.DBGrid1DblClick(Sender: TObject);
begin
  if not (DataSource1.DataSet.IsEmpty) then ModalResult := mrOK;
end;

end.
