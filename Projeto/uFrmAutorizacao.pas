unit uFrmAutorizacao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Mask, Buttons, ExtCtrls, ComCtrls;

type
  TfrmAutorizacao = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    edtSenha: TMaskEdit;
    BitBtn2: TBitBtn;
    edtUsuario: TEdit;
    BitBtn1: TBitBtn;
    mJustificativa: TMemo;
    Label3: TLabel;
    StatusBar1: TStatusBar;
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure mJustificativaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
//    constructor Create(AOwner: TComponent; Historico:String;Msg:String;Valor:Currency;nivel:integer=2);
    constructor Create(AOwner: TComponent; Historico:String;Msg:String;Valor:Currency;nivel:integer=2);
    reintroduce;
    { Public declarations }
  end;

var
  frmAutorizacao: TfrmAutorizacao;
  _Historico:string;
  _Msg:string;
  _Valor: Currency;
  _Nivel: Integer;
  
implementation

uses uDMCupomFiscal, uFrmPrincipal, uFuncoes, uRotinasGlobais;


{$R *.dfm}

procedure TfrmAutorizacao.BitBtn2Click(Sender: TObject);
begin
  AUTORIZACAO.autorizado:=False;
  Close;
end;

procedure TfrmAutorizacao.BitBtn1Click(Sender: TObject);
begin
  AUTORIZACAO.autorizado:=False;
  DMCupomFiscal.dbQuery3.Active:=False;
  DMCupomFiscal.dbQuery3.SQL.Clear;
  DMCupomFiscal.dbQuery3.SQL.Add('Select Nivel From Usuarios Where Nome=:pUsuario and Senha=:pSenha');
  DMCupomFiscal.dbQuery3.ParamByName('pUsuario').AsString:=trim(edtUsuario.Text);
  DMCupomFiscal.dbQuery3.ParamByName('pSenha').AsString:=trim(edtSenha.Text);
  DMCupomFiscal.dbQuery3.Active:=True;
  DMCupomFiscal.dbQuery3.First;
  If DMCupomFiscal.dbQuery3.Eof then
  begin
    edtUsuario.SetFocus;
    msgInformacao('Usu�rio e/ou Senha n�o cadastrado(s)','');
  end
  Else
  begin
    If(DMCupomFiscal.dbQuery3.FieldByName('Nivel').AsString >inttostr(_nivel)) and
      (DMCupomFiscal.dbQuery3.FieldByName('Nivel').AsString <>'X') then
      msgInformacao('Usu�rio sem autoriza��o', 'Aten��o')
    Else
    begin
      AUTORIZACAO.autorizado := True;
      try
        dmCupomFiscal.IBSQL1.Close;
        dmCupomFiscal.IBSQL1.SQL.Clear;
        dmCupomFiscal.IBSQL1.SQL.Add('Insert Into Log_EL(DATA,USUARIO,HISTORICO,VALOR,MSG,JUSTIFICATIVA) Values(:DATA,:USUARIO,:HISTORICO,:VALOR,:MSG,:JUSTIFICATIVA)');
        dmCupomFiscal.IBSQL1.ParamByName('Data').AsDateTime:=Now;
        dmCupomFiscal.IBSQL1.ParamByName('USUARIO').asString:=Trim(edtUsuario.Text);
        dmCupomFiscal.IBSQL1.ParamByName('HISTORICO').asString:=Copy(Trim(_Historico),1,40);
        dmCupomFiscal.IBSQL1.ParamByName('VALOR').asCurrency:=_Valor;
        dmCupomFiscal.IBSQL1.ParamByName('MSG').asString:=_Msg;
        dmCupomFiscal.IBSQL1.ParamByName('JUSTIFICATIVA').asString:=Copy(Trim(mJustificativa.Text),1,100);
        dmCupomFiscal.IBSQL1.ExecQuery;
        dmCupomFiscal.IBSQL1.Close;
        dmCupomFiscal.Transaction.CommitRetaining;
      except
        on E:Exception do
        begin
          logErros(Self, caminhoLog,'Erro ao gerar Log_EL', 'Erro ao gerar Log_EL','S',E);
          Abort;
        end;
      end
    end;
    Close;
  end;
end;

procedure TfrmAutorizacao.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  If Key=VK_escape then Close;
end;

procedure TfrmAutorizacao.FormCreate(Sender: TObject);
begin
  AUTORIZACAO.autorizado:=False;
end;

constructor TfrmAutorizacao.Create(AOwner: TComponent; Historico,
  Msg: String; Valor: Currency; nivel:integer=2);
begin
  inherited Create(AOwner);
  _Historico:=Historico;
  _Msg:=Msg;
  _Valor:=Valor;
  _Nivel := nivel;
end;

procedure TfrmAutorizacao.mJustificativaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if (Key = VK_RETURN) then
  begin
    Key := VK_CLEAR;
    Keybd_event(VK_TAB,0,0,0);
  end;
end;

end.
