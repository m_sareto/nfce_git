unit uDMComponentes;

interface

uses
  SysUtils, Classes, ACBrLCB, vcl.Forms, Math, ActiveX, MSHTML, SHDocVw,
  ACBrBase, ACBrECFClass, ACBrConsts, ACBrBAL, ACBrDevice, pcnConversao, Windows,
  {$IFDEF Delphi6_UP} StrUtils, DateUtils, Types, {$ELSE} FileCtrl,{$ENDIF}

  Menus, jpeg
  {$IFDEF Delphi7},XPMan{$ENDIF}, ACBrAAC, ACBrValidador, ACBrECFVirtual,
  ACBrECFVirtualPrinter, ACBrECFVirtualNaoFiscal, ACBrNFe,
  ACBrNFeDANFEClass, ACBrDANFCeFortesFr,
  ACBrNFeDANFeESCPOS, ACBrDFe, ACBrNFeDANFeRLClass, ACBrMail, pcnConversaoNFe,
  ACBrPosPrinter, ACBrTEFD, ACBrECFVirtualBuffer, ACBrTEFDClass, Vcl.Controls,
  System.DateUtils, ACBrECF, Vcl.ExtCtrls, Vcl.Dialogs, ufrmImpTefComprovantes,
  Data.DB, System.TypInfo, Unit4, Unit5, ACBrTEFDCliSiTef,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  IBX.IBCustomDataSet, IBX.IBQuery;

type
  TdmComponentes = class(TDataModule)
    ACBrBAL1: TACBrBAL;
    ACBrLCB1: TACBrLCB;
    ACBrValidador1: TACBrValidador;
    ACBrNFeDANFe: TACBrNFeDANFCeFortes;
    xACBrNFeDANFe: TACBrNFeDANFeRL;
    ACBrMail1: TACBrMail;
    tmrTef: TTimer;
    ACBrNFe1: TACBrNFe;
    ACBrNFe2_Lauro: TACBrNFe;
    procedure ACBrLCB1LeCodigo(Sender: TObject);
    procedure ACBrNFe1StatusChange(Sender: TObject);
    procedure ConfiguraAcbrMail(Host, UserName, Password, from,
      FromName, Port: string; SetSSl, SetTLS, ReadingConfirmation,
      UseThread: Boolean);
    procedure DataModuleCreate(Sender: TObject);
    procedure ACBrMail1MailProcess(const AMail: TACBrMail;
      const aStatus: TMailStatus);
    procedure ACBrBAL1LePeso(Peso: Double; Resposta: AnsiString);
  private
    { Private declarations }
    function converteResposta(cmd: String): String;
    procedure setConfigACBrLCB();
    procedure setConfigACBrBAL();
  public
    fCancelado: Boolean;
    fCancelamento: Boolean;
    fRecarga: Boolean;
    { Public declarations }
    procedure setConfigACBr();
    procedure WB_LoadHTML(WebBrowser: TWebBrowser; HTMLCode: string);
    procedure WB_ScrollToBottom(WebBrowser1: TWebBrowser);
    Function ValidarCNPJ(CNPJ: String): Boolean;
    Function ValidarCEP(CEP, UF: String): Boolean;
  end;

var
  dmComponentes: TdmComponentes;
  ImagemComprovante_ADM : string;

  implementation

uses uFrmConfiguracao, uFuncoes, uFrmPrincipal, uDMCupomFiscal, uFrmLoja,
  uFrmSupermercado, uRotinasGlobais, UfrmPesquisaCodigo, uFrmStatusNFCe, ufrmFim,
  uFrm_Conf_Redes, uFrmPosto, uFrmNFCe, mimemess;

{$R *.dfm}



function TdmComponentes.converteResposta(cmd: String): String;
var A : Integer ;
begin
  Result := '' ;
  For A := 1 to length( cmd ) do
  begin
    if not (cmd[A] in ['A'..'Z','a'..'z','0'..'9',
                        ' ','.',',','/','?','<','>',';',':',']','[','{','}',
                        '\','|','=','+','-','_',')','(','*','&','^','%','$',
                        '#','@','!','~',']' ]) then
      Result := Result + '#' + IntToStr(ord( cmd[A] )) + ' '
    else
      Result := Result + cmd[A] + ' ';
  end ;
end;



procedure TdmComponentes.DataModuleCreate(Sender: TObject);
begin
  fCancelado := False;
  fRecarga := False;

end;

procedure TdmComponentes.setConfigACBrLCB;
begin
  try
    if (LEITOR.Leitor) then
    begin
      if ACBrLCB1.Ativo then
        ACBrLCB1.Desativar;

      ACBrLCB1.Porta       := LEITOR.Porta;
      ACBrLCB1.Sufixo      := LEITOR.Sufixo;
      ACBrLCB1.UsarFila    := LEITOR.UsarFila;
      ACBrLCB1.Device.Baud := LEITOR.Baud;
      ACBrLCB1.Device.Data := LEITOR.Data;
      ACBrLCB1.ExcluirSufixo := LEITOR.ExcluirSufixo;
      ACBrLCB1.Intervalo   := 300;   //Ver
      ACBrLCB1.Device.HandShake := TACBrHandShake(LEITOR.HandShake);
      ACBrLCB1.Device.HardFlow  := False;;
      ACBrLCB1.Device.SoftFlow  := False;
      ACBrLCB1.Device.Parity := TACBrSerialParity(LEITOR.Parity);
      ACBrLCB1.Device.Stop   := TACBrSerialStop(LEITOR.Stop);

      {ACBrLCB1.Ativar;
      if ACBrLCB1.Ativo then
      begin
        ShowMessage('Leitor Ativado');
      end;}
    end;
  except
    on e:Exception do
    begin
      logErros(nil, caminhoLog, '', 'Erro ao setar Configurações do Leitor Serial','S',e);
    end;
  end;
end;

function TdmComponentes.ValidarCEP(CEP, UF: String): Boolean;
var
  Txt : String;
begin
  Result := false;
  Txt:=limpaString(CEP);
  dmComponentes.ACBrValidador1.TipoDocto   := docCEP;
  dmComponentes.ACBrValidador1.Documento   := Trim(CEP);
  dmComponentes.ACBrValidador1.Complemento := UF;
  if not (dmComponentes.ACBrValidador1.Validar) then
    msgInformacao('Existem inconsistências com a informação inserida.'+#13+#13+
      dmComponentes.ACBrValidador1.MsgErro,'Informação')
  else
    Result := True;
end;

function TdmComponentes.ValidarCNPJ(CNPJ: String): Boolean;
var
  Txt : String;
begin
  Result := false;
  Txt:=limpaString(CNPJ);
  If Length(Txt)>11 then   //CNPJ
    dmComponentes.ACBrValidador1.TipoDocto := docCNPJ
  Else
    dmComponentes.ACBrValidador1.TipoDocto := docCPF;
  dmComponentes.ACBrValidador1.Documento   := Trim(CNPJ);
  dmComponentes.ACBrValidador1.Complemento := '';
  if not (dmComponentes.ACBrValidador1.Validar) then
    msgInformacao('Existem inconsistências com a informação inserida.'+#13+#13+
      dmComponentes.ACBrValidador1.MsgErro,'Informação')
  else
    Result := True;
end;

procedure TdmComponentes.setConfigACBrBAL;
begin
  try
    if (BALANCA.Modelo <> 0) then
    begin
      if ACBrBAL1.Ativo then
        ACBrBAL1.Desativar;

      ACBrBAL1.Modelo           := TACBrBALModelo(BALANCA.Modelo);
      ACBrBAL1.Device.HandShake := TACBrHandShake(BALANCA.HandShake);
      ACBrBAL1.Device.Parity    := TACBrSerialParity(BALANCA.Parity);
      ACBrBAL1.Device.Stop      := TACBrSerialStop(BALANCA.Stop);
      ACBrBAL1.Device.Data      := BALANCA.Data;
      ACBrBAL1.Device.Baud      := BALANCA.Baud;
      ACBrBAL1.Device.Porta     := BALANCA.Porta;

      {ACBrBAL1.Ativar;
      if ACBrBAL1.Ativo then
      begin
        ShowMessage('Balança Ativa');
      end ;}
    end;
  except
    on e:Exception do
    begin
      logErros(nil, caminhoLog, '', 'Erro ao setar Configurações da Balança Checkout','S',e);
    end;
  end;
end;

//Setar configurações das variaveis de registro nos componentes ACBr
procedure TdmComponentes.setConfigACBr;
begin
  try
    setConfigACBrLCB;
    setConfigACBrBAL;
  except
    on e :Exception do
    begin
      logErros(nil, caminhoLog, '', 'Erro ao setar Configurações ACBr','S',e);
    end;
  end;
end;


procedure TdmComponentes.WB_LoadHTML(WebBrowser: TWebBrowser; HTMLCode: string);
var
  sl: TStringList;
  ms: TMemoryStream;
begin
  WebBrowser.Navigate('about:blank');
  while WebBrowser.ReadyState < READYSTATE_INTERACTIVE do
   Application.ProcessMessages;

  if Assigned(WebBrowser.Document) then
  begin
    sl := TStringList.Create;
    try
      ms := TMemoryStream.Create;
      try
        sl.Text := HTMLCode;
        sl.SaveToStream(ms);
        ms.Seek(0, 0);
        (WebBrowser.Document as IPersistStreamInit).Load(TStreamAdapter.Create(ms));
      finally
        ms.Free;
      end;
    finally
      sl.Free;
    end;
  end;
end;

procedure TdmComponentes.WB_ScrollToBottom(WebBrowser1: TWebBrowser);
var
  scrollpos: Integer;
  pw : IHTMLWindow2;
  Doc: IHTMLDocument2;
begin
  Doc := WebBrowser1.Document as IHTMLDocument2;
  pw := IHTMLWindow2(Doc.parentWindow);
  scrollPos := pw.screen.height;
  pw.scrollBy(0, scrollpos);
end;


procedure TdmComponentes.ACBrBAL1LePeso(Peso: Double; Resposta: AnsiString);
var valid : Integer;
begin
  if Peso <= 0 then
  begin
    valid := Trunc(ACBrBAL1.UltimoPesoLido);
    case valid of
      0 : msgInformacao('Coloque o produto sobre a Balança','Atenção');
      -1 : msgInformacao('Penso instável. Tente nova Leitura!','Atenção');
      -2 : msgInformacao('Peso Negativo!','Atenção');
      -10 : msgInformacao('Sobrepeso na Balança!','Atenção');
    end;
  end;
end;

procedure TdmComponentes.ACBrLCB1LeCodigo(Sender: TObject);
begin
  if frmSupermercado <> nil then
  begin
    if (frmSupermercado.edtCodigoMercadoria.Focused)then
    begin
      frmSupermercado.edtCodigoMercadoria.Clear;
      //frmSupermercado.edtCodigoMercadoria.Text := Trim(ACBrLCB1.LerFila);
      frmSupermercado.edtCodigoMercadoria.Text:=ACBrLCB1.UltimaLeitura;
      frmSupermercado.edtCodigoMercadoriaExit(Sender);
    end;

    if frmPesquisaCodigo <> nil then
    begin
      frmPesquisaCodigo.edtCodigo.Clear;
      frmPesquisaCodigo.edtCodigo.Text:=ACBrLCB1.UltimaLeitura;
      frmPesquisaCodigo.edtCodigo.OnExit(Self);
    end;
  end
  else
  begin
    if (frmConfiguracao <> nil) then
    begin
      frmConfiguracao.sttCodigoBarras.Caption := ACBrLCB1.UltimaLeitura;
    end
  end;
end;

procedure TdmComponentes.ACBrMail1MailProcess(const AMail: TACBrMail;
  const aStatus: TMailStatus);
begin
  Case aStatus of
    pmsDone,pmsError:
      if ( frmStatusNFCe <> nil ) then frmStatusNFCe.Hide;
  else
    Application.ProcessMessages;
    if ( frmStatusNFCe = nil ) then frmStatusNFCe := TfrmStatusNFCe.Create(Application);
    frmStatusNFCe.lblStatus.Caption := 'Enviando Email...';
    frmStatusNFCe.Show;
    frmStatusNFCe.BringToFront;
  End;

end;

procedure TdmComponentes.ACBrNFe1StatusChange(Sender: TObject);
begin
  case ACBrNFe1.Status of
    stIdle :
    begin
      if ( frmStatusNFCe <> nil ) then frmStatusNFCe.Hide;
    end;

    stNFeCCe:
    begin
      if ( frmStatusNFCe = nil ) then frmStatusNFCe := TfrmStatusNFCe.Create(Application);
      frmStatusNFCe.lblStatus.Caption := 'Enviando dados da CCe...';
      frmStatusNFCe.Show;
      frmStatusNFCe.BringToFront;
    end;

    stNFeEvento:
    begin
      if ( frmStatusNFCe = nil ) then frmStatusNFCe := TfrmStatusNFCe.Create(Application);
      frmStatusNFCe.lblStatus.Caption := 'Enviando Evento...';
      frmStatusNFCe.Show;
      frmStatusNFCe.BringToFront;
    end;

    stNFeStatusServico :
    begin
      if ( frmStatusNFCe = nil ) then frmStatusNFCe := TfrmStatusNFCe.Create(Application);
      frmStatusNFCe.lblStatus.Caption := 'Verificando Status do Serviço...';
      frmStatusNFCe.Show;
      frmStatusNFCe.BringToFront;
    end;

    stNFeRecepcao :
    begin
      if ( frmStatusNFCe = nil ) then frmStatusNFCe := TfrmStatusNFCe.Create(Application);
      frmStatusNFCe.lblStatus.Caption := 'Enviando dados da NFCe...';
      frmStatusNFCe.Show;
      frmStatusNFCe.BringToFront;
    end;

    stNfeRetRecepcao :
    begin
      if ( frmStatusNFCe = nil ) then frmStatusNFCe := TfrmStatusNFCe.Create(Application);
      frmStatusNFCe.lblStatus.Caption := 'Recebendo dados da NFCe...';
      frmStatusNFCe.Show;
      frmStatusNFCe.BringToFront;
    end;

    stNfeConsulta :
    begin
      if ( frmStatusNFCe = nil ) then frmStatusNFCe := TfrmStatusNFCe.Create(Application);
      frmStatusNFCe.lblStatus.Caption := 'Consultando NFCe...';
      frmStatusNFCe.Show;
      frmStatusNFCe.BringToFront;
    end;

    stNfeCancelamento :
    begin
      if ( frmStatusNFCe = nil ) then frmStatusNFCe := TfrmStatusNFCe.Create(Application);
      frmStatusNFCe.lblStatus.Caption := 'Enviando cancelamento da NFCe...';
      frmStatusNFCe.Show;
      frmStatusNFCe.BringToFront;
    end;

    stNfeInutilizacao :
    begin
      if ( frmStatusNFCe = nil ) then frmStatusNFCe := TfrmStatusNFCe.Create(Application);
      frmStatusNFCe.lblStatus.Caption := 'Enviando pedido de Inutilização...';
      frmStatusNFCe.Show;
      frmStatusNFCe.BringToFront;
    end;

    stNFeRecibo :
    begin
      if ( frmStatusNFCe = nil ) then frmStatusNFCe := TfrmStatusNFCe.Create(Application);
      frmStatusNFCe.lblStatus.Caption := 'Consultando Recibo de Lote...';
      frmStatusNFCe.Show;
      frmStatusNFCe.BringToFront;
    end;

    stNFeCadastro :
    begin
      if ( frmStatusNFCe = nil ) then frmStatusNFCe := TfrmStatusNFCe.Create(Application);
      frmStatusNFCe.lblStatus.Caption := 'Consultando Cadastro...';
      frmStatusNFCe.Show;
      frmStatusNFCe.BringToFront;
    end;

    stNFeEmail :
    begin
      if ( frmStatusNFCe = nil ) then frmStatusNFCe := TfrmStatusNFCe.Create(Application);
      frmStatusNFCe.lblStatus.Caption := 'Enviando Email...';
      frmStatusNFCe.Show;
      frmStatusNFCe.BringToFront;
    end;
  end;
  Application.ProcessMessages;
end;

procedure TdmComponentes.ConfiguraAcbrMail(Host, UserName, Password, from,
  FromName, Port: string; SetSSl, SetTLS, ReadingConfirmation,
  UseThread: Boolean);
begin
  ACBrMail1.Clear;
  ACBrMail1.Host     := Host;  //'smtp.gmail.com';
  ACBrMail1.Username := UserName;
  ACBrMail1.Password := Password;
  ACBrMail1.From     := from;
  ACBrMail1.FromName := FromName;
  ACBrMail1.Port     := Port;  //465 troque pela porta do seu servidor smtp
  ACBrMail1.SetSSL   := SetSSl;
  ACBrMail1.SetTLS   := SetTLS;
  ACBrMail1.Priority := MP_unknown;
end;

end.
