unit uFrmCancelaItem;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, Buttons;

type
  TfrmCancelaItem = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    btnOK: TBitBtn;
    btnCancelar: TBitBtn;
    edItem: TEdit;
    procedure btnOKClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure edItemExit(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCancelaItem: TfrmCancelaItem;

implementation

uses uDMCupomFiscal, uFrmLoja, uFuncoes, uDMComponentes, uRotinasGlobais,
  uFrmPrincipal;

{$R *.dfm}

procedure TfrmCancelaItem.btnOKClick(Sender: TObject);
begin
  if (StrToIntDef(edItem.Text,0) > 0) then
  begin
    try
      DmCupomFiscal.IBSQL1.Close;
      DmCupomFiscal.IBSQL1.SQL.Clear;
      DmCupomFiscal.IBSQL1.SQL.Add('Delete from temp_nfce_item Where (Caixa=:pCaixa) and (Item=:pItem)');
      DmCupomFiscal.IBSQL1.ParamByName('pCaixa').AsString:= FormatFloat('0000', FRENTE_CAIXA.Caixa);
      DmCupomFiscal.IBSQL1.ParamByName('pItem').AsString:=edItem.Text;
      DmCupomFiscal.IBSQL1.ExecQuery;

      frmPrincipal.mResp.Lines.Add( 'Cancelar Item Vendido: '+edItem.Text );
      DmCupomFiscal.Transaction.CommitRetaining;
      Close;
    except
      on E:Exception do
      begin
        DMCupomFiscal.Transaction.RollbackRetaining;
        logErros(Self, caminhoLog,'Erro ao Cancelar Item', '[TEMP_ECF_ITEM] - Erro ao Cancelar Item','S',E);
      end;
    end;
  end
  else
  begin
    msgInformacao('N�mero do item inv�lido','');
  end;
end;

procedure TfrmCancelaItem.btnCancelarClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmCancelaItem.edItemExit(Sender: TObject);
begin
  edItem.Text := FormatFloat('0000',StrToIntDef(edItem.Text,0))
end;

procedure TfrmCancelaItem.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=vk_escape then btnCancelar.Click;
end;

end.



