object frmConsultaLimiteCR: TfrmConsultaLimiteCR
  Left = 390
  Top = 364
  Width = 421
  Height = 204
  BorderIcons = [biSystemMenu]
  Caption = 'Consultar Limite/Saldo'
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDesktopCenter
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object pnlBackground: TPanel
    Left = 0
    Top = 0
    Width = 405
    Height = 166
    Align = alClient
    Color = clWhite
    TabOrder = 0
    object Label1: TLabel
      Left = 9
      Top = 11
      Width = 32
      Height = 13
      Caption = 'Cliente'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsUnderline]
      ParentFont = False
      OnClick = Label1Click
    end
    object Label2: TLabel
      Left = 8
      Top = 56
      Width = 107
      Height = 13
      Caption = 'Dias do '#218'ltimo Cr'#233'dito:'
      Visible = False
    end
    object lbData: TLabel
      Left = 120
      Top = 56
      Width = 8
      Height = 13
      Caption = '0'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label4: TLabel
      Left = 267
      Top = 56
      Width = 97
      Height = 13
      Caption = 'Prote'#231#227'o ao Cr'#233'dito:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object lbPC: TLabel
      Left = 367
      Top = 56
      Width = 24
      Height = 13
      Caption = 'N'#227'o'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object edtCliente: TEdit
      Left = 9
      Top = 26
      Width = 45
      Height = 19
      Ctl3D = False
      ParentCtl3D = False
      TabOrder = 0
      OnExit = edtClienteExit
      OnKeyPress = edtClienteKeyPress
    end
    object edtClienteNome: TEdit
      Left = 53
      Top = 26
      Width = 348
      Height = 19
      Color = clBtnFace
      Ctl3D = False
      ParentCtl3D = False
      ReadOnly = True
      TabOrder = 1
    end
    object pnlLimite: TPanel
      Left = 1
      Top = 83
      Width = 403
      Height = 41
      Align = alBottom
      Caption = 'Limite R$'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentBackground = False
      ParentFont = False
      TabOrder = 2
    end
    object pnlSaldo: TPanel
      Left = 1
      Top = 124
      Width = 403
      Height = 41
      Align = alBottom
      Caption = 'Saldo R$'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentBackground = False
      ParentFont = False
      TabOrder = 3
    end
  end
end
