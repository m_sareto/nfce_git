unit uFrmAguarde;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls;

type
  TfrmAguarde = class(TForm)
    pnlFundo: TPanel;
    lblMensagem: TLabel;
    Image1: TImage;
  private
    { Private declarations }
  public
    { Public declarations }
    constructor Create(AOwner: TComponent; mensagem:String);
  end;

var
  frmAguarde: TfrmAguarde;

implementation

{$R *.dfm}

constructor TFrmAguarde.Create(AOwner: TComponent; mensagem:String);
begin
  inherited Create(AOwner);
  lblMensagem.Caption:=mensagem;
end;

end.

