unit uFrmConsultaHistoricoAutorizacao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGrids, ExtCtrls, DB, IBCustomDataSet, IBQuery,
  StdCtrls, Buttons, Mask, RDprint, Provider, DBClient, ImgList,
  System.ImageList;

type
  TfrmConsultaHistoricoAutorizacao = class(TForm)
    pnlTop: TPanel;
    pnlRodape: TPanel;
    grdDados: TDBGrid;
    Label1: TLabel;
    Label2: TLabel;
    edtDtIni: TMaskEdit;
    edtDtFim: TMaskEdit;
    btnPesquisar: TBitBtn;
    dsDados: TDataSource;
    qrDados: TIBQuery;
    qrDadosDATA: TDateTimeField;
    qrDadosUSUARIO: TIBStringField;
    qrDadosHISTORICO: TIBStringField;
    qrDadosVALOR: TIBBCDField;
    qrDadosJUSTIFICATIVA: TIBStringField;
    btnImprimir: TBitBtn;
    RDprint1: TRDprint;
    imgListAbastecidas: TImageList;
    cdsDados: TClientDataSet;
    DataSetProvider1: TDataSetProvider;
    cdsDadosDATA: TDateTimeField;
    cdsDadosVALOR: TBCDField;
    qrDadosFLAG: TIBStringField;
    cdsDadosUSUARIO: TWideStringField;
    cdsDadosHISTORICO: TWideStringField;
    cdsDadosJUSTIFICATIVA: TWideStringField;
    cdsDadosFLAG: TWideStringField;
    qrDadosMSG: TIBStringField;
    cdsDadosMSG: TWideStringField;
    procedure btnPesquisarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure RDprint1NewPage(Sender: TObject; Pagina: Integer);
    procedure grdDadosDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure grdDadosKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure grdDadosMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure grdDadosTitleClick(Column: TColumn);
    procedure btnImprimirClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmConsultaHistoricoAutorizacao: TfrmConsultaHistoricoAutorizacao;

implementation

Uses
  uDMCupomFiscal, uFrmNFCe, uFuncoes;

{$R *.dfm}

procedure TfrmConsultaHistoricoAutorizacao.btnPesquisarClick(
  Sender: TObject);
begin
  qrDados.Active := false;
  qrDados.ParamByName('pDtIni').AsDate := StrToDate(edtDtIni.Text);
  qrDados.ParamByName('pDtFim').AsDate := StrToDate(edtDtFim.Text);
  qrDados.Active := True;

  cdsDados.Open;
  cdsDados.EmptyDataSet;
  cdsdados.Refresh;
  cdsDados.First;  
end;

procedure TfrmConsultaHistoricoAutorizacao.FormShow(Sender: TObject);
begin
  edtDtIni.Text := dateToStr(now - 30);
  edtDtFim.Text := dateToStr(now);
end;

procedure TfrmConsultaHistoricoAutorizacao.FormKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if(Key=VK_Escape)then Close;
end;

procedure TfrmConsultaHistoricoAutorizacao.RDprint1NewPage(Sender: TObject;
  Pagina: Integer);
begin
  rdprint1.impC (01,01,'Hist�rico de Autoriza��es',[normal]);
  rdprint1.impC (01,75,DateToStr(Date),[]);
  rdprint1.imp  (02,01,'-------------------------------------------------------------------------------');
  rdprint1.impF (03,01,'Data',[Normal]);
  rdprint1.impF (03,12,'Usu�rio',[Normal]);
  rdprint1.impF (03,21,'Valor',[Normal]);
  rdprint1.impF (03,29,'Justificativa',[Normal]);
  linha := 4;
end;

procedure TfrmConsultaHistoricoAutorizacao.grdDadosDrawColumnCell(
  Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  if not(cdsDados.IsEmpty) then
  begin
    if StrToBoolDef(cdsDadosFLAG.Value,True) then
    begin
      grdDados.Canvas.Brush.Color := HexToTColor('88B8FF');
      grdDados.Canvas.FillRect(Rect);
      grdDados.DefaultDrawColumnCell(Rect, DataCol, Column, State);
    end;

    if LowerCase(Column.FieldName) = 'flag' then
    begin
      grdDados.Canvas.Brush.Color := grdDados.Color;
      grdDados.Canvas.FillRect(Rect);
      grdDados.DefaultDrawColumnCell(Rect,DataCol,Column,State);
      grdDados.Canvas.FillRect(Rect);
      if StrToBoolDef(cdsDadosFlag.Value,True) then
        imgListAbastecidas.Draw(grdDados.Canvas,Rect.Left+05,Rect.Top+1,0)
      else
        imgListAbastecidas.Draw(grdDados.Canvas,Rect.Left+05,Rect.Top+1,1);
    end;
  end;
end;

procedure TfrmConsultaHistoricoAutorizacao.grdDadosKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  Flag : Boolean;
begin
  if not(cdsDados.IsEmpty) and (grdDados.Columns[0].Visible) then
  begin
    if(Key = VK_SPACE) or (Key = VK_RETURN) then
    begin
      Flag:=(not StrToBoolDef(cdsDadosFLAG.Value,True));
      cdsDados.Edit;
      cdsDadosFLAG.Value :=(BoolToStr(Flag));
      cdsDados.Post;
    end;
  end;
end;

procedure TfrmConsultaHistoricoAutorizacao.grdDadosMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  Flag : Boolean;
begin
  if not(cdsDados.IsEmpty) and (grdDados.Columns[0].Visible) then
  begin
    if (grdDados.MouseCoord(X, Y).X in [1]) and
      not (grdDados.MouseCoord(X, Y).Y in [0]) then
    begin
      Flag:=(not StrToBoolDef(cdsDadosFLAG.Value, True));
      cdsDados.Edit;
      cdsDadosFLAG.Value:=(BoolToStr(FLAG));
      cdsDados.Post;
    end;
  end;
end;

procedure TfrmConsultaHistoricoAutorizacao.grdDadosTitleClick(
  Column: TColumn);
var
  Flag : Boolean;
begin
  if not(cdsDados.IsEmpty)then
  begin
    if LowerCase(Column.FieldName) = 'flag' then
    begin
      cdsDados.First;
      while not(cdsDados.Eof)do
      begin
        Flag:=(not StrToBoolDef(cdsDadosFLAG.Value,True));
        cdsDados.Edit;
        cdsDadosFLAG.Value := (BoolToStr(Flag));
        cdsDados.Post;
        cdsDados.Next;
      end;
    end;
  end;

end;

procedure TfrmConsultaHistoricoAutorizacao.btnImprimirClick(
  Sender: TObject);
var
  count : Integer;
begin
  count := 0;
  rdprint1.abrir;
  cdsDados.First;
  while not(cdsDados.eof) do
  begin
    if(StrToBoolDef(cdsDadosFLAG.Value,True))Then
    begin
      rdprint1.impF (Linha,01,cdsDadosDATA.AsString,[comp20]);
      rdprint1.impF (Linha,12,cdsDadosUSUARIO.AsString,[Comp17]);
      rdprint1.impF (Linha,21,FormatFloat('R$ ###,##0.00',cdsDadosVALOR.AsCurrency),[Comp17]);
      rdprint1.impF (Linha,29,cdsDadosJUSTIFICATIVA.AsString,[Comp17]);
      Inc(Linha); if(Linha > 66)then RDprint1.Novapagina;
      Inc(count);
    end;
    cdsDados.Next;
    if(cdsDados.Eof) and (count>0) Then
    begin
      rdprint1.imp(Linha,01,'-------------------------------------------------------------------------------');
      Inc(Linha); if(Linha > 66)then RDprint1.Novapagina;
      rdprint1.impF (Linha,01,'Total de registros: '+IntToStr(count),[Normal]);
    end;
  end;
  rdprint1.Fechar;
end;

end.
