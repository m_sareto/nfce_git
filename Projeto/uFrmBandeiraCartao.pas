unit uFrmBandeiraCartao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, Grids, DBGrids, ExtCtrls, StdCtrls, Buttons,
  IBCustomDataSet, IBQuery, DBCtrls;

type
  TfrmBandeiraCartao = class(TForm)
    pnlCartao: TPanel;
    Panel1: TPanel;
    grdDados: TDBGrid;
    dsBandeiraCartao: TDataSource;
    pnlRodape: TPanel;
    btnOK: TBitBtn;
    btnCancelar: TBitBtn;
    pnlDados: TPanel;
    lcmbCredenciadora: TDBLookupComboBox;
    dsCredenciadora: TDataSource;
    Label1: TLabel;
    Label25: TLabel;
    edtCartaocAut: TEdit;
    qurCredenciadora: TIBQuery;
    qurCredenciadoraCNPJ: TIBStringField;
    qurCredenciadoraDESCRICAO: TIBStringField;
    procedure grdDadosKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btnOKClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    procedure getCredenciadora();
    procedure setCores();
  public
    { Public declarations }
  end;

var
  frmBandeiraCartao: TfrmBandeiraCartao;

implementation

uses UFrmFim, uDMCupomFiscal, uFuncoes, uFrmPrincipal;

{$R *.dfm}

procedure TfrmBandeiraCartao.grdDadosKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if (key = vk_Return) and (trim(edtCartaocAut.Text) = '') then
    lcmbCredenciadora.SetFocus
  else
    if((key = vk_Return) and (trim(edtCartaocAut.Text) <> '') )then
      btnOK.SetFocus;
end;

procedure TfrmBandeiraCartao.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (key = vk_Escape) then
    ModalResult := mrCancel;
end;

procedure TfrmBandeiraCartao.btnOKClick(Sender: TObject);
begin
  //Valida se CNPJ foi imformado e � valido
  if not(frmFim.validarCPFCNPJ(qurCredenciadoraCNPJ.Text)) or (qurCredenciadoraCNPJ.Text = '')then
  begin
    lcmbCredenciadora.SetFocus;
    msgAviso('Favor informar o CNPJ v�lido da credenciadora do cart�o','');
    Abort;
  end;
  ModalResult := mrOK;
end;

procedure TfrmBandeiraCartao.btnCancelarClick(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

procedure TfrmBandeiraCartao.getCredenciadora;
begin
  qurCredenciadora.Active:=False;
  qurCredenciadora.Active:=True;
  qurCredenciadora.Last;
  qurCredenciadora.First;
  lcmbCredenciadora.KeyValue := qurCredenciadoraCNPJ.AsString;
end;

procedure TfrmBandeiraCartao.FormCreate(Sender: TObject);
begin
  setCores;
  getCredenciadora;
end;

procedure TfrmBandeiraCartao.setCores;
begin
  Panel1.Color:=HexToTColor(PERSONALIZAR.corFundoCampo);
  Panel1.Font.Color:=HexToTColor(PERSONALIZAR.corFonte);
  pnlRodape.Color:=HexToTColor(PERSONALIZAR.corFundoCampo);
end;

end.
