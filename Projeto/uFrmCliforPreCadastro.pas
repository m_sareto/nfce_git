unit uFrmCliforPreCadastro;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Mask, Vcl.DBCtrls,
  Vcl.ExtCtrls, Tredit, Vcl.Buttons;

type
  TfrmCliforPreCadastro = class(TForm)
    pnlCabecalho: TPanel;
    GroupBox1: TGroupBox;
    Label9: TLabel;
    Label1: TLabel;
    edtDiasPrazo: TEdit;
    edtLimite: TRealEdit;
    edtFone: TEdit;
    Label2: TLabel;
    edtRg: TEdit;
    Label3: TLabel;
    edtCelular: TEdit;
    Label7: TLabel;
    edtEmail: TEdit;
    Label8: TLabel;
    pnlFinalizar: TPanel;
    btnSalvar: TBitBtn;
    pnlFinalizarOrganiza: TPanel;
    btnFinalizar: TSpeedButton;
    btnCancelar: TSpeedButton;
    procedure btnFinalizarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure edtDiasPrazoEnter(Sender: TObject);
    procedure edtLimiteExit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCliforPreCadastro: TfrmCliforPreCadastro;

implementation

Uses
  uFuncoes, uFrmPrincipal;

{$R *.dfm}

procedure TfrmCliforPreCadastro.btnCancelarClick(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

procedure TfrmCliforPreCadastro.btnFinalizarClick(Sender: TObject);
begin
  ModalResult := mrOk;
end;

procedure TfrmCliforPreCadastro.edtDiasPrazoEnter(Sender: TObject);
begin
  if sender is TEdit then
    Cor_Campo(Sender, HexToTColor(PERSONALIZAR.corFundoFoco));
end;

procedure TfrmCliforPreCadastro.edtLimiteExit(Sender: TObject);
begin
  Cor_Campo(Sender, HexToTColor(PERSONALIZAR.corFundoCampo));
end;

procedure TfrmCliforPreCadastro.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if(key = VK_F1)Then
    btnFinalizarClick(Action);

  if(key = VK_ESCAPE)Then
    btnCancelarClick(Action);
end;

end.
