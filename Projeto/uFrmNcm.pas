unit uFrmNcm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, DBClient, ExtCtrls, Grids, DBGrids, StdCtrls, Buttons;

type
  TfrmNcm = class(TForm)
    DBGrid1: TDBGrid;
    Panel1: TPanel;
    dsNcm: TDataSource;
    btnAtualizar: TBitBtn;
    btnSair: TBitBtn;
    procedure btnSairClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BitBtn1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DBGrid1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
  end;

var
  frmNcm: TfrmNcm;

implementation

Uses uFrmImportaPedido, uDMCupomFiscal, IBSQL, uFuncoes;

{$R *.dfm}

procedure TfrmNcm.btnSairClick(Sender: TObject);
begin
  if(frmImportaPedido.cdsNCM.State in[dsInsert, dsEdit])then
  begin
    if not(msgPergunta('Existem transa��es pendentes, sair sem confirmar?', Application.title))then
      Close;
  end
  else
    Close;
end;

procedure TfrmNcm.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if(key = vk_escape)then btnSair.Click;
end;

procedure TfrmNcm.BitBtn1Click(Sender: TObject);
begin
  if(msgPergunta('Este processo vai atualizar os dados da mercadoria.'+#13+ 'Confirma atualiza��o?',Application.Title))then
  begin
    try
      frmImportaPedido.cdsNCM.First;
      while not(frmImportaPedido.cdsNCM.eof)do
      begin
        dmCupomFiscal.IBSQL1.Close;
        dmCupomFiscal.IBSQL1.SQL.Clear;
        dmCupomFiscal.IBSQL1.SQL.Add('Update est_mercadorias set NCM = :pNCM where ID = :pID');
        dmCupomFiscal.IBSQL1.ParamByName('pNCM').AsString := frmImportaPedido.cdsNCMNCM.AsString;
        dmCupomFiscal.IBSQL1.ParamByName('pID').AsString  := frmImportaPedido.cdsNCMMERCADORIA.asString;
        dmCupomFiscal.IBSQL1.ExecQuery;
        frmImportaPedido.cdsNCM.next;
        if(frmImportaPedido.cdsNCM.Eof)then
          frmImportaPedido.cdsNCM.EmptyDataSet;
      end;
      dmCupomFiscal.Transaction.CommitRetaining;
      Close;
    except
      on E:Exception do
        logErros(sender, caminholog, 'Erro ao atualizar NCM','Erro ao atualizar NCM','S',E);
    end;
  end;
end;

procedure TfrmNcm.FormShow(Sender: TObject);
begin
 Dbgrid1.SelectedIndex := 2;
 Dbgrid1.SetFocus;
end;

procedure TfrmNcm.DBGrid1KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if(key = 13)then
  begin
    if not(frmImportaPedido.cdsNCM.Eof) and (frmImportaPedido.cdsNCM.State in [dsedit])then
      frmImportaPedido.cdsNCM.Next
    else
      frmImportaPedido.cdsNCM.edit;
  end;
  //N�o permite inserir um registro novo
  if (Key = VK_DOWN) and (frmImportaPedido.cdsNCM.RecNo = frmImportaPedido.cdsNCM.RecordCount) then
    Key := 0;
  if (Key = VK_TAB) and ((frmImportaPedido.cdsNCM.RecNo = frmImportaPedido.cdsNCM.RecordCount)
  and(DBGrid1.SelectedField = frmImportaPedido.cdsNCMNCM)) then
    Key := 0;
end;

end.
