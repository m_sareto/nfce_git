object FrmTurno_Geral: TFrmTurno_Geral
  Left = 599
  Top = 347
  Anchors = [akBottom]
  Caption = 'Turno Geral'
  ClientHeight = 209
  ClientWidth = 384
  Color = clWindow
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDesktopCenter
  OnKeyDown = FormKeyDown
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 23
    Top = 6
    Width = 28
    Height = 13
    Caption = 'Turno'
  end
  object Label2: TLabel
    Left = 23
    Top = 57
    Width = 129
    Height = 13
    Caption = 'Funcion'#225'rio Caixa Princ'#237'pal'
  end
  object Label3: TLabel
    Left = 24
    Top = 105
    Width = 81
    Height = 13
    Caption = 'Data/Hora Inicial'
  end
  object Label4: TLabel
    Left = 169
    Top = 105
    Width = 76
    Height = 13
    Caption = 'Data/Hora Final'
  end
  object edtTurno: TEdit
    Left = 24
    Top = 21
    Width = 45
    Height = 19
    Ctl3D = False
    ParentCtl3D = False
    TabOrder = 0
  end
  object BitBtn1: TBitBtn
    Left = 24
    Top = 165
    Width = 75
    Height = 25
    Caption = 'Ok'
    TabOrder = 5
    OnClick = BitBtn1Click
  end
  object BitBtn2: TBitBtn
    Left = 104
    Top = 165
    Width = 75
    Height = 25
    Caption = 'Fechar'
    TabOrder = 6
    OnClick = BitBtn2Click
  end
  object edtFun: TEdit
    Left = 24
    Top = 72
    Width = 45
    Height = 19
    CharCase = ecUpperCase
    Ctl3D = False
    ParentCtl3D = False
    TabOrder = 1
    OnExit = edtFunExit
  end
  object Edit2: TEdit
    Left = 69
    Top = 72
    Width = 292
    Height = 19
    TabStop = False
    CharCase = ecUpperCase
    Ctl3D = False
    ParentCtl3D = False
    ReadOnly = True
    TabOrder = 2
  end
  object mkDH_Ini: TMaskEdit
    Left = 24
    Top = 121
    Width = 105
    Height = 21
    EditMask = '!99/99/00 !90:00:00;1;_'
    MaxLength = 17
    TabOrder = 3
    Text = '  /  /     :  :  '
  end
  object mkDH_Fim: TMaskEdit
    Left = 169
    Top = 121
    Width = 105
    Height = 21
    EditMask = '!99/99/00 !90:00:00;1;_'
    MaxLength = 17
    TabOrder = 4
    Text = '  /  /     :  :  '
  end
end
