unit uFrmNFCe;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, jpeg, RDprint, xmldom, XMLIntf,
  msxmldom, XMLDoc, ComCtrls, Mask, DB, Grids, DBGrids, DBCtrls,
  IBCustomDataSet, IBQuery, TREdit, pngimage, ImgList, ACBrNFeDANFEClass,
  ACBrNFe, OleCtrls, SHDocVw,
  pcnConversao, ACBrUtil, pcnNFeW, pcnNFeRTXT, pcnAuxiliar, ACBrDFeUtil,
  pcnNFe, ACBrNFeNotasFiscais, Menus, Clipbrd, pcnConversaoNFe, DBClient,
  ACBrGIF, System.ImageList, ACBrTEFDClass, ufrmImpTefComprovantes, uFrmFim,
  ACBrDFeSSL, blcksock, uCtrlNFCe, uCtrlTef;

type
  TfrmNFCe = class(TForm)
    qECF: TIBQuery;
    dsECF: TDataSource;
    pnlFiltro: TPanel;
    pnlBotoes: TPanel;
    Image2: TImage;
    btnEnviar: TBitBtn;
    btnCancelar: TBitBtn;
    btnInutilizar: TBitBtn;
    btnConsultar: TBitBtn;
    btnEditar: TBitBtn;
    btnFechar: TBitBtn;
    Image3: TImage;
    Label30: TLabel;
    Label31: TLabel;
    dtpInicial: TDateTimePicker;
    dtpFinal: TDateTimePicker;
    Label7: TLabel;
    edtNumIni: TEdit;
    edtNumFim: TEdit;
    Image1: TImage;
    Label1: TLabel;
    lblClifor: TLabel;
    edtClifor: TEdit;
    dsECFItem: TDataSource;
    qECFItem: TIBQuery;
    btnExcluir: TBitBtn;
    imgLista: TImageList;
    StatusBar1: TStatusBar;
    pnlMostrarOcultar: TPanel;
    btnMostrarOcultarItens: TSpeedButton;
    pnlDados: TPanel;
    pnlNFItens: TPanel;
    grdDadosECFItem: TDBGrid;
    pnlTextoNFItem: TPanel;
    grdDadosECF: TDBGrid;
    pnlTextoNF: TPanel;
    MainMenu1: TMainMenu;
    Consulta1: TMenuItem;
    lcbEmpresa: TDBLookupComboBox;
    Label36: TLabel;
    btnImprimir: TBitBtn;
    dsConfigNFe: TDataSource;
    qConfigNFCe: TIBQuery;
    qConfigNFCeNFE_VALIDADOR: TIntegerField;
    qConfigNFCeNFE_SERIE_PADRAO: TIBStringField;
    qConfigNFCeNFE_DATA_HORA: TIBStringField;
    qConfigNFCeEMAIL_ID: TIntegerField;
    qConfigNFCeJUSTIFICA_CAN: TIBStringField;
    qConfigNFCeJUSTIFICA_INU: TIBStringField;
    qConfigNFCeJUSTIFICA_DPEC: TIBStringField;
    qConfigNFCeCER_CAMINHO: TIBStringField;
    qConfigNFCeCER_NSERIE: TIBStringField;
    qConfigNFCeGERAL_FORMA_EMISSAO: TSmallintField;
    qConfigNFCeGERAL_PATH_SALVAR: TIBStringField;
    qConfigNFCeGERAL_PATH_SCHEMAS: TIBStringField;
    qConfigNFCeGERAL_SALVAR: TIBStringField;
    qConfigNFCeGERAL_EXIBIR_ERRO_SCHEMA: TIBStringField;
    qConfigNFCeGERAL_FORMATO_ALERTA: TIBStringField;
    qConfigNFCeARQUIVO_PATH_NFE: TIBStringField;
    qConfigNFCeARQUIVO_PATH_INU: TIBStringField;
    qConfigNFCeARQUIVO_PATH_EVENTO: TIBStringField;
    qConfigNFCeARQUIVO_ADICIONAR_LITERAL: TIBStringField;
    qConfigNFCeARQUIVO_EMISSAO_PATHNFE: TIBStringField;
    qConfigNFCeARQUIVO_PASTA_MENSAL: TIBStringField;
    qConfigNFCeARQUIVO_SALVAR: TIBStringField;
    qConfigNFCeWEBS_UF: TIBStringField;
    qConfigNFCeWEBS_AMBIENTE: TSmallintField;
    qConfigNFCeWEBS_VISUALIZA_MSG: TIBStringField;
    qConfigNFCeWEBS_PROXY_HOST: TIBStringField;
    qConfigNFCeWEBS_PROXY_PORTA: TIBStringField;
    qConfigNFCeWEBS_PROXY_USUARIO: TIBStringField;
    qConfigNFCeWEBS_PROXY_SENHA: TIBStringField;
    qConfigNFCeDANFE_DECIMAISQTD: TSmallintField;
    qConfigNFCeDANFE_DECIMAISVALOR: TSmallintField;
    qConfigNFCeDANFE_EXIBERESUMO: TIBStringField;
    qConfigNFCeDANFE_EXPANDIRLOGO: TIBStringField;
    qConfigNFCeDANFE_IMPRESSORA: TIBStringField;
    qConfigNFCeDANFE_ESPESSURABORDA: TSmallintField;
    qConfigNFCeDANFE_IMPDESCPORC: TIBStringField;
    qConfigNFCeDANFE_IMPRIMIRVALLIQ: TIBStringField;
    qConfigNFCeDANFE_PATH_LOGO: TIBStringField;
    qConfigNFCeDANFE_MARGEMINF: TIBBCDField;
    qConfigNFCeDANFE_MARGEMSUP: TIBBCDField;
    qConfigNFCeDANFE_MARGEMDIR: TIBBCDField;
    qConfigNFCeDANFE_MARGEMESQ: TIBBCDField;
    qConfigNFCeDANFE_MOSTRARPREVIEW: TIBStringField;
    qConfigNFCeDANFE_MOSTRARSTATUS: TIBStringField;
    qConfigNFCeDANFE_MOSTRARSETUP: TIBStringField;
    qConfigNFCeDANFE_NFE_CANCELADA: TIBStringField;
    qConfigNFCeDANFE_NUMCOPIAS: TSmallintField;
    qConfigNFCeDANFE_PATH_PDF: TIBStringField;
    qConfigNFCeDANFE_SOFTWAREHOUSE: TIBStringField;
    qConfigNFCeDANFE_SITE: TIBStringField;
    qConfigNFCeDANFE_PRODUTOSPAG: TSmallintField;
    qConfigNFCeDANFE_FONTECAMPOS: TSmallintField;
    qConfigNFCeDANFE_TIPO: TSmallintField;
    qConfigNFCeDANFE_PREIMPRESSO: TIBStringField;
    qConfigNFCeLOCAL: TIntegerField;
    imgListaStatusNFe: TImageList;
    StatusdoServio1: TMenuItem;
    qConfigNFCeCNPJ: TIBStringField;
    qConfigNFCeVERSAO: TIntegerField;
    qConfigNFCeEMI_RAZAO_SOCIAL: TIBStringField;
    qConfigNFCeSMTP_HOST: TIBStringField;
    qConfigNFCeSMTP_PORTA: TIntegerField;
    qConfigNFCeSMTP_SENHA: TIBStringField;
    qConfigNFCeSMTP_USUARIO: TIBStringField;
    qConfigNFCeSSL: TIBStringField;
    qConfigNFCeEMAIL_ASSUNTO: TIBStringField;
    OpenDialog1: TOpenDialog;
    btnEmail: TBitBtn;
    Image4: TImage;
    qConfigNFCeGERAL_PATH_SAIDA: TIBStringField;
    qConfigNFCeNFE_CAMINHO_VALIDADOR: TIBStringField;
    rgBuscarPor: TRadioGroup;
    ValidadeCertificadoDigital1: TMenuItem;
    N2: TMenuItem;
    GerararquivoXML1: TMenuItem;
    qConfigNFCeE_MAIL: TIBStringField;
    qConfigNFCeFAX: TIBStringField;
    qECFID: TIntegerField;
    qECFSERIE: TIBStringField;
    qECFNUMERO: TIntegerField;
    qECFCLIFOR_NOME: TIBStringField;
    qECFCLIFOR_EMAIL: TIBStringField;
    qECFVLR_TOTAL: TIBBCDField;
    qECFSTATUS: TIBStringField;
    qECFDATA: TDateField;
    qECFMODELO_DOC: TIBStringField;
    qECFOPER_DESCRICAO: TIBStringField;
    qECFLOCAL_ID: TIntegerField;
    qECFCCUSTO: TIntegerField;
    qECFEL_CHAVE: TIntegerField;
    qECFCHAVE_NFCE: TIBStringField;
    qECFCSTAT: TSmallintField;
    qECFXMOTIVO: TIBStringField;
    qECFNPROT: TIBStringField;
    qECFNPROT_CAN: TIBStringField;
    qECFNPROT_INU: TIBStringField;
    qECFNPROT_DEN: TIBStringField;
    qECFItemITEM: TIBStringField;
    qECFItemMERCADORIA: TIBStringField;
    qECFItemQTD: TIBBCDField;
    qECFItemVLR: TFloatField;
    qECFItemVLR_DESCONTO: TIBBCDField;
    qECFItemTOTAL: TIBBCDField;
    qECFItemCFOP: TIntegerField;
    qECFItemCST_ICM: TIBStringField;
    qECFItemCST_PIS: TIBStringField;
    qECFItemCST_COFINS: TIBStringField;
    qConfigNFCeUF: TIBStringField;
    btnNovo: TBitBtn;
    qECFCF_CNPJ_CPF: TIBStringField;
    qECFHORA: TTimeField;
    qConfigNFCeNFCE_SERIE_PADRAO: TIBStringField;
    qConfigNFCeNFCE_TOKEN_ID: TIBStringField;
    qConfigNFCeNFCE_TOKEN: TIBStringField;
    Arquivo1: TMenuItem;
    Contingncia1: TMenuItem;
    qECFCONT_DT_HR: TDateTimeField;
    qECFCONT_JUST: TIBStringField;
    qECFTPEMIS: TIBStringField;
    qECFItemVLR_BC_ICM: TIBBCDField;
    qECFItemALI_ICM: TIBBCDField;
    qECFItemVLR_ICM: TIBBCDField;
    Label2: TLabel;
    cmbSituacao: TComboBox;
    qECFItemUNIDADE: TIBStringField;
    qConfigNFCeNFCE_DANFE_IMPRESSORA: TIBStringField;
    qConfigNFCeNFCE_DANFE_MOSTRARPREVIEW: TIBStringField;
    qConfigNFCeNFCE_DANFE_DETALHADO: TIBStringField;
    qConfigNFCeNFCE_DANFE_PATH_LOGO: TIBStringField;
    qConfigNFCeNFCE_ENVIAR_DIRETO: TIBStringField;
    qECFVLR_TROCO: TIBBCDField;
    qECFItemNCM: TIBStringField;
    qConfigNFCeARQUIVO_SEPARAR_CNPJ: TIBStringField;
    qConfigNFCeARQUIVO_SEPARAR_MODELO: TIBStringField;
    rdPrintVinculado: TRDprint;
    qECFCLIFOR: TIntegerField;
    qECFENDERECO: TIBStringField;
    qECFNUMERO_END: TIBStringField;
    qECFMUNI: TIBStringField;
    qECFUF: TIBStringField;
    qConfigNFCeNFCE_INCLUIR_QRCODE: TIBStringField;
    cdsImpFormaPag: TClientDataSet;
    cdsImpFormaPagTPAG: TStringField;
    cdsImpFormaPagVPAG: TCurrencyField;
    qrImpFormaPag: TIBQuery;
    qrImpFormaPagTPAG: TIBStringField;
    qrImpFormaPagVPAG: TIBBCDField;
    qConfigNFCeNFCE_CONFIRMA_FINALIZACAO: TIBStringField;
    qConfigNFCeMUNICIPIO: TIntegerField;
    EnviararquivoXML1: TMenuItem;
    qrImpFormaPagID_ECF: TIntegerField;
    qrImpFormaPagID: TIntegerField;
    qrImpFormaPagCARD_CNPJ: TIBStringField;
    qrImpFormaPagCARD_TBAND: TIBStringField;
    qrImpFormaPagCARD_CAUT: TIBStringField;
    qrImpFormaPagFORMA_PAG: TIntegerField;
    qrImpFormaPagINDICE_ECF: TIBStringField;
    qrImpFormaPagVTROCO: TIBBCDField;
    btnRecarga: TBitBtn;
    qConfigNFCeWEBS_TIMEOUT: TIntegerField;
    qECFItemMER_DESCRICAO: TIBStringField;
    ConsultaNFCeemContingencia1: TMenuItem;
    qConfigNFCeCER_FORMA_ASSINATURA: TIBStringField;
    qConfigNFCeDANFE_IMPRIMIRDESCACRESCITEM: TIBStringField;
    qConfigNFCeCER_SENHA: TIBStringField;
    qConfigNFCeTLS: TIBStringField;
    EF1: TMenuItem;
    ransaoAdministrativa1: TMenuItem;
    qConfigNFCeCER_SSLHTTPLIB: TIBStringField;
    qConfigNFCeCER_SSLXMLSIGNLIB: TIBStringField;
    qConfigNFCeCER_SSLTYPE: TIBStringField;
    imgCertificado: TImage;
    qConfigNFCeEMAIL_MSG: TIBStringField;
    qECFVLR_TOTTRIB_EST: TIBBCDField;
    qECFVLR_TOTTRIB_MUN: TIBBCDField;
    qECFVLR_TOTTRIB: TIBBCDField;
    qConfigNFCeCER_SSLCRYPTLIB: TIBStringField;

    procedure btnFecharClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edID_NFKeyPress(Sender: TObject; var Key: Char);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnEditarClick(Sender: TObject);
    procedure ativarDesativarTimerRetorno(tipo:String);
    procedure btnEnviarClick(Sender: TObject);
    procedure getECF();
    procedure edtCliforChange(Sender: TObject);
    procedure getECFItens(id:Integer);
    procedure edtNumIniKeyPress(Sender: TObject; var Key: Char);
    procedure edtNumFimKeyPress(Sender: TObject; var Key: Char);
    procedure dtpFinalChange(Sender: TObject);
    procedure dtpInicialChange(Sender: TObject);
    procedure edtNumIniExit(Sender: TObject);
    procedure edtNumFimExit(Sender: TObject);
    procedure dsECFDataChange(Sender: TObject; Field: TField);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnInutilizarClick(Sender: TObject);
    procedure grdDadosECFDblClick(Sender: TObject);
    procedure btnExcluirClick(Sender: TObject);
    procedure btnMostrarOcultarItensClick(Sender: TObject);
    procedure edtCCustoIniKeyPress(Sender: TObject; var Key: Char);
    procedure edtCCustoFimKeyPress(Sender: TObject; var Key: Char);
    procedure btnConsultarClick(Sender: TObject);
    procedure btnImprimirClick(Sender: TObject);
    procedure grdDadosECFDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure StatusdoServio1Click(Sender: TObject);
    procedure btnCopiarClick(Sender: TObject);
    procedure btnEmailClick(Sender: TObject);
    procedure lcbEmpresaClick(Sender: TObject);
    procedure ValidadeCertificadoDigital1Click(Sender: TObject);
    procedure GerararquivoXML1Click(Sender: TObject);
    procedure lcmbModeloDocClick(Sender: TObject);
    procedure grdDadosECFMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure grdDadosECFMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormShow(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure btnNovoClick(Sender: TObject);
    procedure rgBuscarPorClick(Sender: TObject);
    procedure Contingncia1Click(Sender: TObject);
    procedure cmbSituacaoClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure rdPrintVinculadoNewPage(Sender: TObject; Pagina: Integer);
    procedure getPagamento();
    procedure EnviararquivoXML1Click(Sender: TObject);
    procedure btnRecargaClick(Sender: TObject);
    procedure ransaoAdministrativa1Click(Sender: TObject);
  private
    { Private declarations }
    //Variaveis Globais Privadas
    txt : string;
    bmRegistroSelecionado : TBookmark;
    VendaCreditoLoja : Boolean ; //Var para controlar se foi a prazo ou nao - para imprimir 2 vias
    procedure controlaBotoes();
    procedure cancelarECFBaseDados(id, el_chave:integer);
    procedure excluirECFBaseDados(id:integer; aposCancelar:Boolean=False);
    procedure setDadosNFCeACBr(id_ecf: integer);
    procedure getEmpresas(ccusto_selecionar:Integer);
    procedure atualizarStatusNFCe(id_ecf, xcStat:integer; xMotivo, xnProt, xml_retorno:String; AConsulta: Boolean);
    procedure cancelarECFInutilizacao(numero, ccusto:Integer; serie:String);
    procedure enviarEmail(acao, emailPara : String);
    procedure setConfigNFCeACBr();
    procedure atualizarStatusCCe(xcStat:integer; chnfe, xMotivo, xnProt, xml_retorno:String; dhEvento:TDateTime);
    procedure verificarValidadeCertificado(Status : Integer);
    procedure moverXMLProc();
    procedure ImprimeVinculado(ID : Integer);
    function inutilizarECFBaseDados(numero, ccusto: Integer; serie, modelo, chaveInut: String):Integer;
    function montarCaminhoArquivoXML(): String;
    function setContingenciaNFCe():Boolean;
    function isContingenciaPendente(numero, ccusto: Integer; serie:String):Boolean;
    function getDescontoNFCeItem(id_ecf:Integer):Currency;
    function getDiretorioNFCe():String;
    function getDescontoNFCeItemSemServico(id_ecf: Integer): Currency;
    function getDescontoNFCeItemServico(id_ecf: Integer): Currency;
    function getFaturaAdicionais():String;
    function getCompTef():String;
    function ConsultaStatusNFCe:Boolean;
    procedure AtualizaInformacoesMercadoriaNFCeItem(AID_NFCe: Integer; AID_Mercadoria: String);
    Function TemTef(pID: Integer): Boolean;
  public
    //Vars passagem parametro envio direto NFCe
    ID_NF_ENVIAR : Integer; // Armazena ID NF passada por parametro no create da tela para ficar disponivel posteriormente
    ID_CCUSTO_ENVIAR : Integer; //Armazena ID CCusto empresa que fez NFCe
    LOCAL: Integer;
    LOCAL_UF           : String; //UF Emitente
    LOCAL_CNPJ         : String; //CNPJ Emitente
    LOCAL_SERIE_PADRAO : String; //Serie Padrao NFCe
    LOCAL_FORMA_EMISSAO: String; //Normal, Contingencia
    LOCAL_WEBS_AMBIENTE: String; //Homologacao, Producao
    LOCAL_IMPRIMIR_DETALHADO: String; //S-Sim  N-Nao  P-Perguntar
    LOCAL_ENVIAR_DIRETO: String; //S-Sim  N-Nao  P-Perguntar


    { Public declarations }
    procedure inutilizarNumeroNFCe(numero, ccusto: Integer; serie:String);
    procedure enviarEmailEvento(acao, emailPara : String);
    function getPendentes():Boolean;
    procedure AtualizarDadosCliente();
  end;

var
  frmNFCe: TfrmNFCe;
  Linha : Integer;

implementation

uses uFrmPesquisa, MaskUtils, Math, uFuncoes,IBSQL, StrUtils,
  DateUtils,ACBrNFeWebServices, uDMCupomFiscal, uDMComponentes,
  uFrmPrincipal, uFrmLogin, uRotinasGlobais, uFrmSupermercado, uFrmLoja,
  uFrmNFCeInutilizacao, uFrmPosto, uFrmContingencia, uFrm_Imp_Fatura,
  mimemess;

{$R *.dfm}

procedure TfrmNFCe.btnFecharClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmNFCe.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  //Sair
  If (key=vk_Escape) then btnFechar.Click;
end;

procedure TfrmNFCe.edID_NFKeyPress(Sender: TObject; var Key: Char);
begin
  if not(Key in ['0'..'9',#8]) then  Abort;
end;

procedure TfrmNFCe.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  qECF.Active:=False;
end;

procedure TfrmNFCe.btnEditarClick(Sender: TObject);
var msg:string;
begin
  if not(qECF.Eof) then
  begin
    msg:='N�o � poss�vel editar nota fiscal n�mero/s�rie: '+qECFNUMERO.AsString+'/'+qECFSERIE.AsString+sLineBreak+
         'ID: '+qECFID.AsString+sLineBreak+
         'Status: '+qECFCSTAT.AsString+'/'+qECFXMOTIVO.AsString;

    case qECFCSTAT.AsInteger of
      100,150: begin //Autorizado Uso
        msgAviso(msg + sLineBreak+
                 'Protocolo Autoriza��o Uso: '+qECFNPROT.AsString, '');
      end;
      101,135,151,155: begin //Cancelado
        msgAviso(msg + sLineBreak+
                 'Protocolo Cancelamento: '+qECFNPROT_CAN.AsString, '');
      end;
      102 : begin //Inutilizado
        msgAviso(msg + sLineBreak+
                 'Protocolo Inutiliza��o: '+qECFNPROT_INU.AsString, '');
      end;
      110,301,302,303: begin //Denegado
        msgAviso(msg + sLineBreak+
                 'Protocolo Denegado: '+qECFNPROT_DEN.AsString, '');
      end;
      //103..105:  //Lote Rec,Proc
      //201..299,304..999: //Rejeitado
    else
      try
        bmRegistroSelecionado:=qECF.GetBookmark;
        //tela de edicao
      finally
        getECF;
        grdDadosECF.SetFocus;
        qECF.GotoBookmark(bmRegistroSelecionado);
        qECF.FreeBookmark(bmRegistroSelecionado);
      end;
    end;
  end
  else
    msgInformacao('Nenhuma Nota Fiscal selecionada!','');
end;


procedure TfrmNFCe.ativarDesativarTimerRetorno(tipo: String);
begin
  if tipo='A' then //Ativar
  begin
    pnlDados.Enabled:=False;
    pnlBotoes.Enabled:=False;
    pnlFiltro.Enabled:=False;
    pnlMostrarOcultar.Enabled:=False;
  end
  else
  begin
    StatusBar1.Panels[2].Text:='';
    pnlDados.Enabled:=True;
    pnlBotoes.Enabled:=True;
    pnlFiltro.Enabled:=True;
    pnlMostrarOcultar.Enabled:=True;
    Application.ProcessMessages;
  end;
end;

procedure TfrmNFCe.btnEnviarClick(Sender: TObject);
var msgEnvio: string;
    enviarTodasNFCe, ImpTodasCont: Boolean;
    lCtrlNFCe: TCtrlNFCe;
begin
  try
    msgEnvio:='';
    enviarTodasNFCe := False;
    ImpTodasCont := True;
    case cmbSituacao.ItemIndex of
      1:msgEnvio:='Enviar todas NF-e pendentes ?'; //Pendente
      7:msgEnvio:='Enviar todas NFC-e emitidas em conting�ncia?'; //Contingencia
    end;
    if (Trim(msgEnvio)<>'') then
    begin
      if msgPergunta(msgEnvio, '') then
      begin
        if cmbSituacao.ItemIndex = 7 then
        begin
          if not(msgPergunta('Deseja imprimir todas as NFCes emitidas em contingencia?', '')) then
            ImpTodasCont := False
        end;
        enviarTodasNFCe:=True;
        qECF.First;
      end;
    end;
    repeat
      bmRegistroSelecionado:=qECF.GetBookmark;
      StatusBar1.panels[2].Text:='Enviando NFC-e. Por favor aguarde...';
      ativarDesativarTimerRetorno('A');  //Bloqueia para nao mudar NF selecionada
      lCtrlNFCe := TCtrlNFCe.Create(dmCupomFiscal.DataBase,
        dmCupomFiscal.Transaction, lcbEmpresa.KeyValue, 0, nil, ImpTodasCont);
      try
        lCtrlNFCe.Enviar(qECFID.AsInteger);
      finally
        lCtrlNFCe.Free;
      end;
      if enviarTodasNFCe then
        qECF.Next;
    until (not(enviarTodasNFCe) or (qECF.Eof));
  finally
    getECF;
    ativarDesativarTimerRetorno('D');
    grdDadosECF.SetFocus;
    StatusBar1.panels[2].Text:='';
    qECF.GotoBookmark(bmRegistroSelecionado);
    qECF.FreeBookmark(bmRegistroSelecionado);
    moverXMLProc();
  end;
end;

procedure TfrmNFCe.getECF;
begin
  qECF.Active:=False;
  qECF.SQL.Clear;
  qECF.SQL.Add('select ecf.id, ecf.serie, ecf.numero, ecf.clifor||''/''||ecf.cf_nome as clifor_nome, cf.e_mail as clifor_email,'+
               ' ecf.vlr_total, ecf.vlr_troco, ecf.status, ecf.data, ecf.hora, ecf.modelo_doc, ecf.operacao||''/''|| op.descricao as oper_descricao,'+
               ' cc.local local_id, ecf.ccusto, ecf.el_chave, ecf.chave_nfce, ecf.cf_cnpj_cpf, ecf.cont_dt_hr, ecf.cont_just, ecf.tpEmis,'+
               ' x.cstat, x.xmotivo, x.nprot, x.nprot_can, x.nprot_inu, x.nprot_den, cf.ID as Clifor, cf.Endereco, cf.numero_End, m.Descricao as muni, m.UF,'+
               ' ecf.VLR_TOTTRIB_EST, ecf.VLR_TOTTRIB_MUN, ecf.VLR_TOTTRIB'+
               ' from est_ecf ecf'+
               ' left outer join cliFor cf on cf.id=ecf.clifor'+
               ' left outer join operacoes op on op.id=ecf.operacao'+
               ' left outer join ccustos cc on cc.id=ecf.ccusto'+
               ' left outer join est_ecf_xml x on x.id_ecf=ecf.id'+
               ' Left outer join Municipios m on m.Id = cf.municipio'+
               ' where'+
               ' (ecf.modelo_doc=''65'') and'+
               ' (cc.local=:pLocal)');
  case cmbsituacao.ItemIndex of
    //0: Todas - n�o precisa filtro
    1:qECF.SQL.Add(' and ((x.cstat=0) or (x.cstat is null) or (x.cstat between 201 and 299) or (x.cstat between 304 and 999))'); //Pendente
    2:qECF.SQL.Add(' and ((x.cstat=100) or (x.cstat=150))'); //Efetivado
    3:qECF.SQL.Add(' and ((x.cstat=101) or (x.cstat=135) or (x.cstat=151) or (x.cstat=155))'); //Cancelado
    4:qECF.SQL.Add(' and (x.cstat=102)'); //Inutilizado
    5:qECF.SQL.Add(' and ((x.cstat=110) or (x.cstat=301) or (x.cstat=302) or (x.cstat=303))'); //Denegado
    6:qECF.SQL.Add(' and ((x.cstat between 201 and 299) or (x.cstat between 304 and 999))'); //Rejeitado
    7:qECF.SQL.Add(' and (x.cstat=1)'); //Contingencia Pendente
    8:qECF.SQL.Add(' and ((x.cstat=0) or (x.cstat is null) or '+
                   '(x.cstat between 201 and 299) or (x.cstat = 105) or   '+
                   '(x.cstat between 304 and 999) or (x.cstat = 1))'); //Pendente de Envio/Processamento
  end;
  if (cmbSituacao.ItemIndex<>7) and (cmbSituacao.ItemIndex<>8) then  //Se n�o � contingencia
  begin
    qECF.SQL.Add(' and (ecf.numero between :pNumeroIni and :pNumeroFim) and'+
                 ' (ecf.data between :pDataIni and :pDataFim)');
    if Trim(edtClifor.Text) <> '' then
    begin
      case rgBuscarPor.ItemIndex of
        0:qECF.SQL.Add(' and (upper(ecf.cf_nome) like  upper(''%'+Trim(edtClifor.Text)+'%''))');
        1:qECF.SQL.Add(' and (replace(replace(replace(ecf.cf_cnpj_cpf,''/'',''''),''-'',''''),''.'','''') like  upper(''%'+limpaString(Trim(edtClifor.Text))+'%''))');
      end;
    end;
  end;
  qECF.SQL.Add(' order by ecf.data desc, ecf.Hora desc, ecf.numero desc, ecf.serie');
  if (cmbSituacao.ItemIndex<>7) and (cmbSituacao.ItemIndex<>8) then //Se n�o � contingencia
  begin
    qECF.ParamByName('pNumeroIni').AsInteger := StrToIntDef(edtNumIni.Text,0);
    qECF.ParamByName('pNumeroFim').AsInteger := StrToIntDef(edtNumFim.Text,0);
    qECF.ParamByName('pDataIni').AsDate      := dtpInicial.Date;
    qECF.ParamByName('pDataFim').AsDate      := dtpFinal.Date;
  end;
  qECF.ParamByName('pLocal').AsInteger:=qConfigNFCeLOCAL.AsInteger;
  qECF.Active:=True;
  qECF.First;
end;

procedure TfrmNFCe.edtCliforChange(Sender: TObject);
begin
  getECF;
end;

procedure TfrmNFCe.getECFItens(id: Integer);
begin
  qECFItem.Active:=False;
  qECFItem.ParamByName('pID').AsInteger:=id;
  qECFItem.Active:=True;
  qECFItem.First;
end;

procedure TfrmNFCe.edtNumIniKeyPress(Sender: TObject; var Key: Char);
begin
  if not (isDigit(Key)) then Key:= #0;
end;

procedure TfrmNFCe.edtNumFimKeyPress(Sender: TObject; var Key: Char);
begin
  if not (isDigit(Key)) then Key:= #0;
end;

procedure TfrmNFCe.dtpFinalChange(Sender: TObject);
begin
  getECF;
end;

procedure TfrmNFCe.dtpInicialChange(Sender: TObject);
begin
  getECF;
end;

procedure TfrmNFCe.edtNumIniExit(Sender: TObject);
begin
  if Trim(edtNumIni.Text)='' then
    edtNumIni.Text:='0';
  getECF;
end;

procedure TfrmNFCe.edtNumFimExit(Sender: TObject);
begin
  if Trim(edtNumFim.Text)='' then
    edtNumFim.Text:='9999999';
  getECF;
end;

procedure TfrmNFCe.dsECFDataChange(Sender: TObject; Field: TField);
begin
  StatusBar1.Panels[1].Text := qECFCSTAT.Text+IfThen(Trim(qECFXMOTIVO.Text)<>'','/'+qECFXMOTIVO.Text ,'');
  getECFItens(qECFID.AsInteger);
  controlaBotoes;
end;

procedure TfrmNFCe.controlaBotoes;
begin
    if not(qECF.IsEmpty) then
    begin
      case qECFCSTAT.AsInteger of
        1:begin //Contingencia
          btnEditar.Enabled:=False;
          btnEnviar.Enabled:=True;
          btnCancelar.Enabled:=False;
          btnExcluir.Enabled:=False;
          btnInutilizar.Enabled:=True;
          btnConsultar.Enabled:=False;
          btnImprimir.Enabled:=False;
          btnEmail.Enabled:=False;
        end;
        102: begin //Inutilizado
          btnEditar.Enabled:=False;
          btnEnviar.Enabled:=False;
          btnCancelar.Enabled:=False;
          btnExcluir.Enabled:=False;
          btnInutilizar.Enabled:=True;
          btnConsultar.Enabled:=False;
          btnImprimir.Enabled:=False;
          btnEmail.Enabled:=False;
          Contingncia1.Enabled := false;
        end;
        101,135,151,155: begin //Cancelado
          btnEditar.Enabled:=False;
          btnEnviar.Enabled:=False;
          btnCancelar.Enabled:=False;
          btnExcluir.Enabled:=False;
          btnInutilizar.Enabled:=True;
          btnConsultar.Enabled:=True;
          btnImprimir.Enabled:=False;
          btnEmail.Enabled:=False;
          Contingncia1.Enabled := false;
        end;
        103..105: begin //Lote Rec,Proc
          btnEditar.Enabled:=False;
          btnEnviar.Enabled:=False;
          btnCancelar.Enabled:=False;
          btnExcluir.Enabled:=False;
          btnInutilizar.Enabled:=True;
          btnConsultar.Enabled:=True;
          btnImprimir.Enabled:=False;
          btnEmail.Enabled:=False;
          Contingncia1.Enabled := false;
        end;
        110,301,302,303: begin //Denegado
          btnEditar.Enabled:=False;
          btnEnviar.Enabled:=False;
          btnCancelar.Enabled:=False;
          btnExcluir.Enabled:=False;
          btnInutilizar.Enabled:=True;
          btnConsultar.Enabled:=True;
          btnImprimir.Enabled:=False;
          btnEmail.Enabled:=False;
          Contingncia1.Enabled := false;
        end;
        201..299,304..999: begin //Rejeitado
          btnEditar.Enabled:=True;
          btnEnviar.Enabled:=True;
          btnCancelar.Enabled:=False;
          btnExcluir.Enabled:=True;
          btnInutilizar.Enabled:=True;
          btnConsultar.Enabled:=True;
          btnImprimir.Enabled:=False;
          btnEmail.Enabled:=False;
          Contingncia1.Enabled := True;
        end;
        100,150: begin//Autorizado Uso
          btnEditar.Enabled:=False;
          btnEnviar.Enabled:=False;
          btnCancelar.Enabled:=True;
          btnExcluir.Enabled:=False;
          btnInutilizar.Enabled:=True;
          btnConsultar.Enabled:=True;
          btnImprimir.Enabled:=True;
          btnEmail.Enabled:=True;
          Contingncia1.Enabled := false;
        end;
      else
        //Nenhuma acima
        btnEditar.Enabled:=True;
        btnEnviar.Enabled:=True;
        btnCancelar.Enabled:=False;
        btnExcluir.Enabled:=True;
        btnInutilizar.Enabled:=True;
        btnConsultar.Enabled:=True;
        btnImprimir.Enabled:=False;
        btnEmail.Enabled:=False;
        Contingncia1.Enabled := True;
      end;
      if(dmCupomFiscal.fStatusLicenca = 'B')then //Se esta bloqueado n�o permite opera��o com a sefaz
      begin
        btnEnviar.Enabled := false;
        btnCancelar.Enabled := false;
        btnInutilizar.Enabled := false;
        btnConsultar.Enabled := false;
        btnRecarga.Enabled := false;
        GerararquivoXML1.Enabled := False;
        EnviararquivoXML1.Enabled := false;
        StatusdoServio1.Enabled := False;
        ValidadeCertificadoDigital1.Enabled := false;
        Contingncia1.Enabled := false;
      end;
    end
    else
    begin
      btnEditar.Enabled:=False;
      btnEnviar.Enabled:=False;
      btnCancelar.Enabled:=False;
      btnExcluir.Enabled:=False;
      btnEmail.Enabled:=False;
      btnInutilizar.Enabled:=True;
      btnConsultar.Enabled:=False;
      btnImprimir.Enabled:=False;
      Contingncia1.Enabled := false;
      if(dmCupomFiscal.fStatusLicenca = 'B')then //Se esta bloqueado n�o permite opera��o com a sefaz
      begin
        btnEnviar.Enabled := false;
        btnCancelar.Enabled := false;
        btnInutilizar.Enabled := false;
        btnConsultar.Enabled := false;
        btnRecarga.Enabled := false;
        GerararquivoXML1.Enabled := False;
        EnviararquivoXML1.Enabled := false;
        StatusdoServio1.Enabled := False;
        ValidadeCertificadoDigital1.Enabled := false;
        Contingncia1.Enabled := false;
      end;
    end;
end;

procedure TfrmNFCe.btnCancelarClick(Sender: TObject);
var
  lCtrlNFCe: TCtrlNFCe;
begin
  try
    bmRegistroSelecionado:=qECF.GetBookmark;
    StatusBar1.panels[2].Text:='Enviando cancelamento. Por favor aguarde...';
    ativarDesativarTimerRetorno('A');  //Bloqueia para nao mudar NF selecionada
    lCtrlNFCe := TCtrlNFCe.Create(dmCupomFiscal.DataBase,
      dmCupomFiscal.Transaction,lcbEmpresa.KeyValue, 0,frmNFCe);
    try
      lCtrlNFCe.Cancelar(qECFID.AsInteger);
    finally
      lCtrlNFCe.Free;
    end;
  finally
    getECF;
    ativarDesativarTimerRetorno('D');
    grdDadosECF.SetFocus;
    StatusBar1.panels[2].Text:='';
    qECF.GotoBookmark(bmRegistroSelecionado);
    qECF.FreeBookmark(bmRegistroSelecionado);
    moverXMLProc();
  end;
end;

procedure TfrmNFCe.btnInutilizarClick(Sender: TObject);
begin
  frmNFCeInutilizacao:= TfrmNFCeInutilizacao.Create(Self, qConfigNFCeLOCAL.AsInteger, qConfigNFCeEMI_RAZAO_SOCIAL.AsString);
  try
    frmNFCeInutilizacao.edtSerie.Text     := qECFSERIE.Text;
    frmNFCeInutilizacao.edtNrInicial.Text := qECFNUMERO.Text;
    frmNFCeInutilizacao.ShowModal;
  finally
    frmNFCeInutilizacao.Free;
  end;
end;

procedure TfrmNFCe.inutilizarNumeroNFCe(numero, ccusto: Integer; serie:String);
var
  lCtrlNFCe: TCtrlNFCe;
begin
  if not (isContingenciaPendente(numero, ccusto, serie)) then
  begin
    Try
      ativarDesativarTimerRetorno('A');
      StatusBar1.panels[2].Text:='Enviando pedido de inutiliza��o. Por favor aguarde...';
      lCtrlNFCe := TCtrlNFCe.Create(dmCupomFiscal.DataBase,
        dmCupomFiscal.Transaction,lcbEmpresa.KeyValue);
      try
        lCtrlNFCe.Inutilizar(numero, ccusto, serie);
      finally
        lCtrlNFCe.Free;
      end;
    finally
      getECF;
      ativarDesativarTimerRetorno('D');
      grdDadosECF.SetFocus;
      StatusBar1.panels[2].Text:='';
      moverXMLProc();
    end;
  end
  else
  begin
    msgInformacao(Format('NFC-e n�mero/s�rie %s/%s n�o pode ser inutilizado pois esta em Conting�ncia',[IntToStr(numero), serie]),'');
  end;
end;

{Trigger esta fazendo esse procedimento ao cancelar ver se esta realmente funcionando}
procedure TfrmNFCe.cancelarECFBaseDados(id, el_chave: integer);
begin
  Try
{
    //--- Exclusao de Caixa - Tem Trigger --------------------------------------
    dmCupomFiscal.IBSQL1.Close;
    dmCupomFiscal.IBSQL1.SQL.Clear;
    dmCupomFiscal.IBSQL1.SQL.Add('Delete From Cx_Movimento Where (el_Chave=:pel_Chave)');
    dmCupomFiscal.IBSQL1.ParamByName('pel_Chave').AsInteger:=el_chave;
    dmCupomFiscal.IBSQL1.ExecQuery;
    dmCupomFiscal.IBSQL1.Close;

    //--- Exclusao Contas A Receber_Baixa ---------------------------------------
    dmCupomFiscal.dbQuery1.Active:=False;
    dmCupomFiscal.dbQuery1.SQL.Clear;
    dmCupomFiscal.dbQuery1.SQL.Add('Select Id From Cr_Movimento Where (el_Chave=:pel_Chave)');
    dmCupomFiscal.dbQuery1.ParamByName('pel_Chave').AsInteger:=el_chave;
    dmCupomFiscal.dbQuery1.Active:=True;
    dmCupomFiscal.dbQuery1.First;
    While not dmCupomFiscal.dbQuery1.Eof do
    begin
      dmCupomFiscal.IBSQL1.Close;
      dmCupomFiscal.IBSQL1.SQL.Clear;
      dmCupomFiscal.IBSQL1.SQL.Add('Delete From CR_Movimento_Baixas Where (Id_CR=:pId)');
      dmCupomFiscal.IBSQL1.ParamByName('pId').AsInteger:=dmCupomFiscal.dbQuery1.FieldByName('Id').AsInteger;
      dmCupomFiscal.IBSQL1.ExecQuery;
      dmCupomFiscal.IBSQL1.Close;
      dmCupomFiscal.dbQuery1.Next;
    end;

    //Exclusao Contas A Receber - Tem Trigger ----------------------------------
    dmCupomFiscal.IBSQL1.Close;
    dmCupomFiscal.IBSQL1.SQL.Clear;
    dmCupomFiscal.IBSQL1.SQL.Add('Delete From CR_Movimento Where (el_Chave=:pel_Chave)');
    dmCupomFiscal.IBSQL1.ParamByName('pel_Chave').AsInteger:=el_chave;
    dmCupomFiscal.IBSQL1.ExecQuery;
    dmCupomFiscal.IBSQL1.Close;

    //--- Exclusao Contas A Pagar_Baixa ----------------------------------------
    dmCupomFiscal.dbQuery1.Active:=False;
    dmCupomFiscal.dbQuery1.SQL.Clear;
    dmCupomFiscal.dbQuery1.SQL.Add('Select Id From Cp_Movimento Where (el_Chave=:pel_Chave)');
    dmCupomFiscal.dbQuery1.ParamByName('pel_Chave').AsInteger:=el_chave;
    dmCupomFiscal.dbQuery1.Active:=True;
    dmCupomFiscal.dbQuery1.First;
    While not dmCupomFiscal.dbQuery1.Eof do
    begin
      dmCupomFiscal.IBSQL1.Close;
      dmCupomFiscal.IBSQL1.SQL.Clear;
      dmCupomFiscal.IBSQL1.SQL.Add('Delete From Cp_Movimento_Baixas Where (Id_Cp=:pId)');
      dmCupomFiscal.IBSQL1.ParamByName('pId').AsInteger:=dmCupomFiscal.dbQuery1.FieldByName('Id').AsInteger;
      dmCupomFiscal.IBSQL1.ExecQuery;
      dmCupomFiscal.IBSQL1.Close;
      dmCupomFiscal.dbQuery1.Next;
    end;

    //Exclusao Contas A Pagar - Tem Trigger ------------------------------------
    dmCupomFiscal.IBSQL1.Close;
    dmCupomFiscal.IBSQL1.SQL.Clear;
    dmCupomFiscal.IBSQL1.SQL.Add('Delete From CP_Movimento Where (el_Chave=:pel_Chave)');
    dmCupomFiscal.IBSQL1.ParamByName('pel_Chave').AsInteger:=el_chave;
    dmCupomFiscal.IBSQL1.ExecQuery;
    dmCupomFiscal.IBSQL1.Close;
}

    //--- CANCELAMENTO NF - STATUS ----------------------------------------------
    dmCupomFiscal.IBSQL1.Close;
    dmCupomFiscal.IBSQL1.SQL.Clear;
    dmCupomFiscal.IBSQL1.SQL.Add('UPDATE Est_ECF Set Status=''C'', cod_sit=''02'' Where (ID=:pID)');
    dmCupomFiscal.IBSQL1.ParamByName('pID').AsInteger:=id;
    dmCupomFiscal.IBSQL1.ExecQuery;
    dmCupomFiscal.IBSQL1.Close;

    DMCupomFiscal.dbQuery1.Active:=False;
    DMCupomFiscal.dbQuery1.SQL.Clear;
    DMCupomFiscal.dbQuery1.SQL.Add('Select abastecida from est_ECF_item Where (abastecida > 0) and (Id_ecf=:pID)');
    DMCupomFiscal.dbQuery1.ParamByName('pID').AsInteger:=id;
    DMCupomFiscal.dbQuery1.Active:=True;
    DMCupomFiscal.dbQuery1.First;
    while not (DMCupomFiscal.dbQuery1.Eof) do
    begin
      //Volta Abastecida status X
      DMCupomFiscal.IBSQL1.Close;
      DMCupomFiscal.IBSQL1.SQL.Clear;
      DMCupomFiscal.IBSQL1.SQL.Add('Update est_abastecimentos set status=''X'' where id=:pAbastecida');
      DMCupomFiscal.IBSQL1.ParamByName('pAbastecida').AsInteger:=DMCupomFiscal.dbQuery1.FieldByName('abastecida').AsInteger;
      DMCupomFiscal.IBSQL1.ExecQuery;
      DMCupomFiscal.IBSQL1.Close;
      DMCupomFiscal.dbQuery1.Next;
    end;
    dmCupomFiscal.Transaction.CommitRetaining;
  except
    on E:Exception do
    begin
      dmCupomFiscal.Transaction.RollbackRetaining;
      raise EDatabaseError.Create('Ocorreu seguinte erro ao cancelar NFC-e na base de dados. '+e.message);
    end;
  end;
end;

procedure TfrmNFCe.excluirECFBaseDados(id: integer; aposCancelar:Boolean=False);
begin
  //Excluir Registro da Base de Dados
  Try
    //--- Exclusao de Itens
    dmCupomFiscal.IBSQL1.Close;
    dmCupomFiscal.IBSQL1.SQL.Clear;
    dmCupomFiscal.IBSQL1.SQL.Add('Delete From Est_ECF_Item Where (ID_ECF=:pID)');
    dmCupomFiscal.IBSQL1.ParamByName('pID').AsInteger:=id;
    dmCupomFiscal.IBSQL1.ExecQuery;
    dmCupomFiscal.IBSQL1.Close;

    //--- Exclusao de Fatura
    dmCupomFiscal.IBSQL1.Close;
    dmCupomFiscal.IBSQL1.SQL.Clear;
    dmCupomFiscal.IBSQL1.SQL.Add('Delete From Est_ECF_Fatura Where (ID_ECF=:pID)');
    dmCupomFiscal.IBSQL1.ParamByName('pID').AsInteger:=id;
    dmCupomFiscal.IBSQL1.ExecQuery;
    dmCupomFiscal.IBSQL1.Close;

    //--- Exclusao de Forma Pag
    dmCupomFiscal.IBSQL1.Close;
    dmCupomFiscal.IBSQL1.SQL.Clear;
    dmCupomFiscal.IBSQL1.SQL.Add('Delete From Est_ECF_FormaPag Where (ID_ECF=:pID)');
    dmCupomFiscal.IBSQL1.ParamByName('pID').AsInteger:=id;
    dmCupomFiscal.IBSQL1.ExecQuery;
    dmCupomFiscal.IBSQL1.Close;

    //--- Exclusao de XML
    dmCupomFiscal.IBSQL1.Close;
    dmCupomFiscal.IBSQL1.SQL.Clear;
    dmCupomFiscal.IBSQL1.SQL.Add('Delete From Est_ECF_XML Where (ID_ECF=:pID)');
    dmCupomFiscal.IBSQL1.ParamByName('pID').AsInteger:=id;
    dmCupomFiscal.IBSQL1.ExecQuery;
    dmCupomFiscal.IBSQL1.Close;

    //--- Exclusao de Nota Fiscal
    dmCupomFiscal.IBSQL1.Close;
    dmCupomFiscal.IBSQL1.SQL.Clear;
    dmCupomFiscal.IBSQL1.SQL.Add('Delete From Est_ECF Where (ID=:pID)');
    dmCupomFiscal.IBSQL1.ParamByName('pID').AsInteger:=id;
    dmCupomFiscal.IBSQL1.ExecQuery;
    dmCupomFiscal.IBSQL1.Close;

    dmCupomFiscal.Transaction.CommitRetaining;
  except
    on E:Exception do
    begin
      dmCupomFiscal.Transaction.RollbackRetaining;
      raise EDatabaseError.Create('Ocorreu seguinte erro ao efetuar exclus�o do cupom fiscal na base de dados. '+e.Message);
    end;
  end;
end;

function TfrmNFCe.inutilizarECFBaseDados(numero, ccusto: Integer; serie, modelo, chaveInut: String):Integer;
Var ID_ECF:Integer;
begin
  Try
    dmCupomFiscal.IBSQL1.Close;
    dmCupomFiscal.IBSQL1.SQL.Clear;
    dmCupomFiscal.IBSQL1.SQL.Add('Delete from est_ecf Where (ccusto=:pCCusto) and (Serie=:pSerie) and (Numero=:pNumero) and (modelo_doc=:pModelo)');
    dmCupomFiscal.IBSQL1.ParamByName('pCCusto').AsInteger:=ccusto;
    dmCupomFiscal.IBSQL1.ParamByName('pSerie').AsString:=serie;
    dmCupomFiscal.IBSQL1.ParamByName('pNumero').AsInteger:=numero;
    dmCupomFiscal.IBSQL1.ParamByName('pModelo').AsString:=modelo;
    dmCupomFiscal.IBSQL1.ExecQuery;
    dmCupomFiscal.IBSQL1.Close;

    dmCupomFiscal.IBSQL1.Close;
    dmCupomFiscal.IBSQL1.SQL.Clear;
    dmCupomFiscal.IBSQL1.SQL.Add('select gen_id(EST_ECF, 1) as xEST_ECF from rdb$database;');
    dmCupomFiscal.IBSQL1.ExecQuery;
    dmCupomFiscal.IBSQL1.Close;
    ID_ECF:=dmCupomFiscal.IBSQL1.fieldbyname('xEST_ECF').AsInteger;

    dmCupomFiscal.dbECF.Open;
    dmCupomFiscal.dbECF.Insert;
    DMCupomFiscal.dbECFCAIXA.Value:= FormatFloat('0000',FRENTE_CAIXA.Caixa);
    DMCupomFiscal.dbECFCUPOM.Value:= '0';
    dmCupomFiscal.dbECFNUMERO.Value:= numero;
    DMCupomFiscal.dbECFDATA.Value:= Now;
    dmCupomFiscal.dbECFHORA.Value:=StrToTime('00:00:00');
    DMCupomFiscal.dbECFEL_CHAVE.Value:=gerarELChave;
    DMCupomFiscal.dbECFCCUSTO.Value:= ccusto;
    DMCupomFiscal.dbECFOPERACAO.Value:=0;
    DMCupomFiscal.dbECFESTOQUE.Value := 'S';
    DMCupomFiscal.dbECFFORMA_PAG.Value:=0;
    DMCupomFiscal.dbECFFUNCIONARIO.Value:=LOGIN.usuarioCod;
    DMCupomFiscal.dbECFFUN_USUARIO.Value:=LOGIN.usuarioCod;
    DMCupomFiscal.dbECFTURNO.Value:='0';
    DMCupomFiscal.dbECFCLIFOR.Value:=0;
    DMCupomFiscal.dbECFSTATUS.Value:='I';
    DMCupomFiscal.dbECFVLR_DINHEIRO.Value:=0;
    DMCupomFiscal.dbECFVLR_TIKET.Value:=0;
    DMCupomFiscal.dbECFVLR_CHEQUE.Value:=0;
    DMCupomFiscal.dbECFVLR_CARTAO.Value:=0;
    DMCupomFiscal.dbECFVLR_CONVENIO.Value:=0;
    DMCupomFiscal.dbECFVLR_DESCONTO.Value:=0;
    DMCupomFiscal.dbECFVLR_SUBTOTAL.Value:=0;
    DMCupomFiscal.dbECFVLR_TOTAL.Value:=0;
    dmCupomFiscal.dbECFVLR_RECEBIDO.Value:=0;
    dmCupomFiscal.dbECFVLR_TROCO.Value:=0;
    dmCupomFiscal.dbECFCOD_SIT.Value:='05'; //Conforme SPED Fiscal
    dmCupomFiscal.dbECFINDPAG.Value:='2';
    dmCupomFiscal.dbECFMODELO_DOC.Value:=modelo;
    dmCupomFiscal.dbECFSERIE.Value:=serie;
    dmCupomFiscal.dbECFTPIMP.Value:='0';
    dmCupomFiscal.dbECFTPEMIS.Value:='1';
    dmCupomFiscal.dbECFINDPRES.Value:='1';
    dmCupomFiscal.dbECFVERPROC.Value:='1.00';
    dmCupomFiscal.dbECFMODFRETE.Value:='9';
    dmCupomFiscal.dbECFVLR_BC_ICM.Value:=0;
    dmCupomFiscal.dbECFVLR_ICM.Value:=0;
    dmCupomFiscal.dbECFVLR_FRETE.Value:=0;
    dmCupomFiscal.dbECFVLR_SEG.Value:=0;
    dmCupomFiscal.dbECFVLR_PIS.Value:=0;
    dmCupomFiscal.dbECFVLR_COFINS.Value:=0;
    dmCupomFiscal.dbECFVLR_OUTRO.Value:=0;
    if chaveInut <> '' then
      dmCupomFiscal.dbECFCHAVE_NFCE.Value:=chaveInut;
    dmCupomFiscal.dbECFID.Value:=ID_ECF;
    dmCupomFiscal.dbECF.Post;

    dmCupomFiscal.Transaction.CommitRetaining;
    Result:=ID_ECF;
  except
    on e:Exception do
    begin
      dmCupomFiscal.Transaction.RollbackRetaining;
      raise EDatabaseError.Create('Ocorreu seguinte erro ao inutilizar n�mero da NFC-e na base de dados. '+e.Message);
    end;
  end;
end;

procedure TfrmNFCe.grdDadosECFDblClick(Sender: TObject);
begin
  btnEditar.Click;
end;

procedure TfrmNFCe.btnExcluirClick(Sender: TObject);
var
  lCtrlTef: TCtrlTef;
begin
  try
    if msgPergunta('Etapas que ser�o executadas pelo sistema:'+sLineBreak+
                   '1� - Cancelamento da NFC-e no Administrador'+sLineBreak+
                   '2� - Estorno dos Lan�amentos no Administrador'+sLineBreak+
                   '3� - Exclus�o da NFC-e do Administrador'+sLineBreak+sLineBreak+
                   'Confirmar cancelamento da NFC-e n�mero/s�rie '+qECFNUMERO.AsString+'/'+qECFSERIE.AsString+' ?','Confirma��o') then
    begin
      try
        bmRegistroSelecionado:=qECF.GetBookmark;
        //Primeiro cancela o tef
        if(TemTef(qECFID.AsInteger))Then
        begin
          lCtrlTef := TCtrlTef.Create(dmCupomFiscal.DataBase,
            dmCupomFiscal.Transaction,Self,qConfigNFCeDANFE_IMPRESSORA.AsString);
          try
            lCtrlTef.EfetuarCancelamento(qECFEL_CHAVE.AsInteger);
          finally
            lCtrlTef.Free;
          end;
        end;
        //Cancela no adm
        cancelarECFBaseDados(qECFID.AsInteger, qECFEL_CHAVE.AsInteger);
        excluirECFBaseDados(qECFID.AsInteger, True);
      finally
        getECF;
        grdDadosECF.SetFocus;
        qECF.GotoBookmark(bmRegistroSelecionado);
        qECF.FreeBookmark(bmRegistroSelecionado);
      end;
      msgInformacao('Exclus�o da Nota Fiscal efetuada com sucesso!','');
    end;
  except
    on e:Exception do
    begin
      logErros(Sender, caminhoLog,'Erro ao excluir NFC-e '+sLineBreak+e.Message, 'Erro ao excluir NFC-e','S',E);
    end;
  end;
end;

procedure TfrmNFCe.btnMostrarOcultarItensClick(Sender: TObject);
begin
  if btnMostrarOcultarItens.Caption='Mostrar Itens' then
  begin
    btnMostrarOcultarItens.Caption:='Ocultar Itens';
    //getECFItens(qECFID.AsInteger);
    pnlNFItens.Height:=250;
    pnlNFItens.Visible:=True;
    btnMostrarOcultarItens.Glyph:=nil;
    imgLista.GetBitmap(1, btnMostrarOcultarItens.Glyph);
  end
  else
  begin
    btnMostrarOcultarItens.Caption:='Mostrar Itens';
    //qECFItem.Active:=False;
    pnlNFItens.Visible:=False;
    btnMostrarOcultarItens.Glyph:=nil;
    imgLista.GetBitmap(0, btnMostrarOcultarItens.Glyph);
  end;
end;

procedure TfrmNFCe.edtCCustoIniKeyPress(Sender: TObject;
  var Key: Char);
begin
  if not(isDigit(key)) then Key:= #0;
end;

procedure TfrmNFCe.edtCCustoFimKeyPress(Sender: TObject;
  var Key: Char);
begin
  if not(isDigit(key)) then Key:= #0;
end;

procedure TfrmNFCe.btnConsultarClick(Sender: TObject);
var
  lCtrlNFCe: TCtrlNFCe;
begin
  try
    bmRegistroSelecionado:=qECF.GetBookmark;
    StatusBar1.panels[2].Text:='Consultando Status. Por favor aguarde...';
    ativarDesativarTimerRetorno('A');  //Bloqueia para nao mudar NF selecionada
    lCtrlNFCe := TCtrlNFCe.Create(dmCupomFiscal.DataBase,
      dmCupomFiscal.Transaction,lcbEmpresa.KeyValue);
    try
      lCtrlNFCe.Consultar(qECFID.AsInteger);
    finally
      lCtrlNFCe.Free;
    end;
  finally
    getECF;
    ativarDesativarTimerRetorno('D');
    grdDadosECF.SetFocus;
    StatusBar1.panels[2].Text:='';
    qECF.GotoBookmark(bmRegistroSelecionado);
    qECF.FreeBookmark(bmRegistroSelecionado);
    moverXMLProc();
  end;
//  ConsultaStatusNFCe;
end;

//Lauro 19.01.17 criado
function TfrmNFCe.ConsultaStatusNFCe:Boolean;
Var Chave_NFCe:String;
    TpEmis:Integer;
begin
  try
    bmRegistroSelecionado:=qECF.GetBookmark;
    try
      DMComponentes.ACBrNFe1.WebServices.Consulta.Clear;
      DMComponentes.ACBrNFe1.WebServices.Consulta.NFeChave:=qECFCHAVE_NFCE.AsString;
      DMComponentes.ACBrNFe1.WebServices.Consulta.Executar;

      //lauro 19.01.17 Retorno q j� existe esta nota mas com chave Diferencte
      if DMComponentes.ACBrNFe1.WebServices.Consulta.cStat = 613 then
      begin
        //Caso TpEmis seja 1 consulta no 9 VV
        if qECFTPEMIS.AsString='1' then
          TpEmis:=9
        else
          TpEmis:=1;

        //Monta Chave para Consutal TPEmis 1 Normal
        Chave_NFCe:=MontaChaveAcessoNFe_v2(
          UFparaCodigo(qConfigNFCeUF.Value), //Codigo UF
          qECFDATA.Value,        //Data
          OnlyNumber(qConfigNFCeCNPJ.Value), //CNPJ
          qECFMODELO_DOC.AsInteger,  //Modelo Doc
          qECFSERIE.AsInteger,   //Serie
          qECFNUMERO.Value, //Numero NF
          TpEmis, // Tipo de Emissao Normal
          qECFNUMERO.Value //Codigo Numerico
          );
        DMComponentes.ACBrNFe1.WebServices.Consulta.Clear;
        DMComponentes.ACBrNFe1.WebServices.Consulta.NFeChave := Chave_NFce;
        DMComponentes.ACBrNFe1.WebServices.Consulta.Executar;

        dmCupomFiscal.IBSQL1.close;
        dmCupomFiscal.IBSQL1.sql.clear;
        dmCupomFiscal.IBSQL1.sql.Add('Update est_Ecf set Chave_NFCe = :pChave, '+
          ' TPEMIS =:pTpEmis where ID = :pID');
        dmCupomFiscal.IBSQL1.ParamByName('pChave').AsString :=Chave_NFce;
        dmCupomFiscal.IBSQL1.ParamByName('pID').AsInteger := qECFID.Value;
        dmCupomFiscal.IBSQL1.ParamByName('pTpEmis').AsInteger := TpEmis;
        dmCupomFiscal.IBSQL1.Prepare;
        dmCupomFiscal.IBSQL1.ExecQuery;
        dmCupomFiscal.IBSQL1.Close;
        dmcupomfiscal.Transaction.CommitRetaining;
      end;
      //Fim Lauro 19.01.17

      case DMComponentes.ACBrNFe1.WebServices.Consulta.cStat of
        110,301,302,303:
        begin //Denegado
          //Cancela na base do administrador a nota fiscal e seus vinculos
          cancelarECFBaseDados(qECFID.AsInteger, qECFEL_CHAVE.AsInteger);
        end;
      end;
      atualizarStatusNFCe(qECFID.Value, DMComponentes.ACBrNFe1.WebServices.Consulta.cStat,
        DMComponentes.ACBrNFe1.WebServices.Consulta.xMotivo,
        DMComponentes.ACBrNFe1.WebServices.Consulta.Protocolo,
        '', True);

      msgInformacao('N�mero: '+qECFNUMERO.AsString+'     '+'S�rie: '+qECFSERIE.AsString+#13+
               'Protocolo: '+DMComponentes.ACBrNFe1.WebServices.Consulta.Protocolo+#13+
               'Sefaz: '+IntToStr(DMComponentes.ACBrNFe1.WebServices.Consulta.cStat)+' - '+DMComponentes.ACBrNFe1.WebServices.Consulta.xMotivo+#13+
               'Administrador : '+qECFCSTAT.AsString+' - '+qECFXMOTIVO.AsString, 'Status da NFe');
    except
      on E:Exception do
      begin
        logErros(Self,caminhoLog,'Erro ao consultar Status da NFC-e.'+#13+
                'N�o foi poss�vel estabelecer conex�o com Sefaz devido poss�veis erros:'+#13+
                '-Certificado digital n�o identificado;'+#13+
                '-Sem conex�o local com internet;'+#13+
                '-Ambiente de envio diverge do ambiente de recebimento;'+#13+
                '-Servidores da Sefaz inoperantes;'+#13+
                e.Message,
                'Erro ao consultar Status da NFC-e','S',E);
      end;
    end;
  finally
    getECF;
    grdDadosECF.SetFocus;
    qECF.GotoBookmark(bmRegistroSelecionado);
    qECF.FreeBookmark(bmRegistroSelecionado);
    moverXMLProc();
  end;
end;

procedure TfrmNFCe.setDadosNFCeACBr(id_ecf: integer);
var okConversao: Boolean;
    vlr_Desconto_Item_Soma, vServico, vBCIssqn, vPIS_servttlnfe,
    vCOFINS_servttlnfe, vOutro, vDescCon, vDescInc : Currency;
begin
  try
    //Var para controlar se foi a prazo ou nao - para imprimir 2 vias
    VendaCreditoLoja := False;
    vServico := 0;
    vOutro := 0;
    vDescCon := 0;
    vDescInc := 0;
    //--Query dados ECF
    dmCupomFiscal.qECF.Active:=False;
    dmCupomFiscal.qECF.ParamByName('pID').AsInteger:=id_ecf;
    dmCupomFiscal.qECF.Active:=True;
    dmCupomFiscal.qECF.First;
    if not (dmCupomFiscal.qECF.Eof) then
    begin
      //Pega informa��es do Emitente
      dmCupomFiscal.qEmitente.Active:=False;
      dmCupomFiscal.qEmitente.ParamByName('pID').Value:=getLocalCCusto(dmCupomFiscal.qECFCCUSTO.Value);
      dmCupomFiscal.qEmitente.Active:=True;
      dmCupomFiscal.qEmitente.First;
      if not(dmCupomFiscal.qEmitente.Eof) then
      begin
        //Aqui alimenta componente com dados do emitente
        dmComponentes.ACBrNFe1.NotasFiscais.Clear;
        with dmComponentes.ACBrNFe1.NotasFiscais.Add.NFe do
        begin
          //-------------------------- A - Dados da NFe --------------------
          with infNFe do
          begin
            ID := 'NFe'+dmCupomFiscal.qECFCHAVE_NFCE.AsString;
          end;

          //-------------------------- B - Identifica��o da NFCe --------------------------
          Ide.cUF       := UFtoCUF(dmCupomFiscal.qEmitenteUF.Value);
          Ide.cNF       := dmCupomFiscal.qECFNUMERO.AsInteger;
          Ide.natOp     := dmCupomFiscal.qECFNATOPE.AsString;
          Ide.indPag    := StrToIndpag(okConversao, dmCupomFiscal.qECFINDPAG.AsString); //0:Vista    1:Prazo   2:Outros
          Ide.modelo    := dmCupomFiscal.qECFMODELO_DOC.AsInteger;
          Ide.serie     := dmCupomFiscal.qECFSERIE.AsInteger;
          Ide.nNF       := dmCupomFiscal.qECFNUMERO.AsInteger;
          Ide.dEmi      := dmCupomFiscal.qECFDATA.Value + dmCupomFiscal.qECFHORA.Value;
          Ide.dSaiEnt   := Now;
          Ide.hSaiEnt   := Now;
          Ide.tpNF      := tnSaida;
          Ide.idDest    := doInterna;
          Ide.cMunFG    := dmCupomFiscal.qEmitenteMUNICIPIO.AsInteger;
          Ide.tpImp     := StrToTpImp(okConversao, dmCupomFiscal.qECFTPIMP.AsString);
          dmComponentes.ACBrNFe1.Configuracoes.Geral.FormaEmissao := StrToTpEmis(okConversao, dmCupomFiscal.qECFTPEMIS.AsString);
          Ide.tpEmis    := StrToTpEmis(okConversao, dmCupomFiscal.qECFTPEMIS.AsString);
          Ide.tpAmb     := StrToTpAmb(okConversao, dmCupomFiscal.qECFTPAMB.AsString);
          Ide.finNFe    := fnNormal;
          Ide.indFinal  := cfConsumidorFinal;
          Ide.indPres   := StrToPresencaComprador(okConversao, dmCupomFiscal.qECFINDPRES.AsString);
          Ide.procEmi   := peAplicativoContribuinte;
          Ide.verProc   := dmCupomFiscal.qECFVERPROC.AsString;
          if dmCupomFiscal.qECFCONT_DT_HR.Value > 0 then //Indica que esta em contigencia
          begin
            Ide.dhCont := dmCupomFiscal.qECFCONT_DT_HR.Value;
            Ide.xJust  := dmCupomFiscal.qECFCONT_JUST.Value;
          end;

          //-------------------------- C - Identifica��o do Emitente do NFCe --------------------------
          with Emit do
          begin
            CNPJCPF           := limpaString(dmCupomFiscal.qEmitenteCNPJ.Value);
            xNome             := dmCupomFiscal.qEmitenteDESCRICAO.AsString;
            xFant             := dmCupomFiscal.qEmitenteNOME_FANTASIA.AsString;
            with EnderEmit do
            begin
              xLgr    := dmCupomFiscal.qEmitenteENDERECO.AsString;
              nro     := dmCupomFiscal.qEmitenteNUMERO_END.AsString;
              xCpl    := dmCupomFiscal.qEmitenteCOMPLEMENTO_END.AsString;
              xBairro := dmCupomFiscal.qEmitenteBAIRRO.AsString;
              cMun    := dmCupomFiscal.qEmitenteMUNICIPIO.Value;
              xMun    := dmCupomFiscal.qEmitenteXMUN.AsString;
              UF      := dmCupomFiscal.qEmitenteUF.AsString;
              CEP     := StrToInt(OnlyNumber(dmCupomFiscal.qEmitenteCEP.Value));
              cPais   := dmCupomFiscal.qEmitentePAIS.Value;
              xPais   := dmCupomFiscal.qEmitenteXPAIS.Value;
              fone    := dmCupomFiscal.qEmitenteFONE.AsString;
            end;
            IE                := limpaString(dmCupomFiscal.qEmitenteIE.Value);
            IEST              := ''; //Exclusivo para IE de ST
            IM                := dmCupomFiscal.qEmitenteIM.AsString;
            CNAE              := dmCupomFiscal.qEmitenteCNAE.AsString;
            CRT               := StrToCRT(okConversao, dmCupomFiscal.qEmitenteCRT.AsString);
          end;

          //-------------------------- E - Identifica��o do Destinat�rio do NFCe --------------------------
          with Dest do
          begin
            CNPJCPF   := limpaString(dmCupomFiscal.qECFCF_CNPJ_CPF.Value);
            xNome     := dmCupomFiscal.qECFCF_NOME.Value;
            //idEstrangeiro     := ''; //Somente quando for vender estrangeiro
            with EnderDest do
            begin
              xLgr    := dmCupomFiscal.qECFCF_ENDE.AsString;
              nro     := dmCupomFiscal.qECFCF_NUMERO_END.AsString;
              xCpl    := '';
              xBairro := dmCupomFiscal.qECFCF_BAIRRO.AsString;
              cMun    := dmCupomFiscal.qECFCF_MUNICIPIO.AsInteger;
              xMun    := dmCupomFiscal.qECFCF_XMUN.AsString;
              UF      := dmCupomFiscal.qECFCF_UF.AsString;
              CEP     := StrToIntDef(OnlyNumber(dmCupomFiscal.qECFCF_CEP.AsString),0);
              //cPais   := 1058;
              //xPais   := 'BRASIL';
              //Fone    := '1533243333';
            end;
            //Indicador da IE do Destinatario, no caso de NFC-e informar 9 e n�o informar a tag IE do destinat�rio
{            case StrToIntDef(dmCupomFiscal.qECFINDIEDEST.AsString, 9) of
              1: indIEDest := inContribuinte;
              2: indIEDest := inIsento;
              9: indIEDest := inNaoContribuinte;
            end}
            indIEDest := inNaoContribuinte; //Default sempre 9 pois NFCe n�o pode ser diferente
            //IE    := ''; //NFC-e n�o aceita IE
            //ISUF  := '';
            //IM    := '';
            //Email := '';
          end;

          {//--- Use os campos abaixo para informar o endere�o de retirada quando for diferente do Remetente/Destinat�rio
          Retirada.CNPJCPF := '';
          Retirada.xLgr    := '';
          Retirada.nro     := '';
          Retirada.xCpl    := '';
          Retirada.xBairro := '';
          Retirada.cMun    := 0;
          Retirada.xMun    := '';
          Retirada.UF      := '';
          }

          {//--- Use os campos abaixo para informar o endere�o de entrega quando for diferente do Remetente/Destinat�rio
          Entrega.CNPJCPF := '';
          Entrega.xLgr    := '';
          Entrega.nro     := '';
          Entrega.xCpl    := '';
          Entrega.xBairro := '';
          Entrega.cMun    := 0;
          Entrega.xMun    := '';
          Entrega.UF      := '';}

          //Adicionando Produtos
          dmCupomFiscal.qECF_Item.Active := False;
          dmCupomFiscal.qECF_Item.ParamByName('pID').AsInteger := id_ecf;
          dmCupomFiscal.qECF_Item.Active := True;
          dmCupomFiscal.qECF_Item.First;
          while not (dmCupomFiscal.qECF_Item.Eof) do
          begin
            //-------------------------- H - Detalhamento de Produtos e Servi�os --------------------------
            with Det.Add do
            begin
              Prod.nItem    := dmCupomFiscal.qECF_ItemITEM.AsInteger;
              Prod.cProd    := dmCupomFiscal.qECF_ItemMERCADORIA.AsString;
              Prod.cEAN     := dmCupomFiscal.qECF_ItemCEAN.AsString;
              Prod.xProd    := dmCupomFiscal.qECF_ItemXPROD.AsString; //produ��o
              Prod.CEST     := dmCupomFiscal.qECF_ItemM_CEST.AsString; //produ��o
              AtualizaInformacoesMercadoriaNFCeItem(id_ecf,
                dmCupomFiscal.qECF_ItemMERCADORIA.AsString);
              if(dmCupomFiscal.qECFTPAMB.AsInteger = 2) and
                (dmCupomFiscal.qECF_ItemITEM.AsInteger = 1)then//Homologa��o primeiro item com essa descri��o
                Prod.xProd  := 'NFC-E EMITIDA EM AMBIENTE DE HOMOLOGACAO - SEM VALOR FISCAL';
              Prod.NCM      := dmCupomFiscal.qECF_ItemM_NCM.AsString; //2016
              Prod.EXTIPI   := '';
              Prod.CFOP     := dmCupomFiscal.qECF_ItemCFOP.AsString;

              //Se cfop de combustivel, informar grupo de combustivel
              if(dmCupomFiscal.qECF_ItemCFOP.AsString = '5656') or
                (dmCupomFiscal.qECF_ItemCFOP.AsString = '6656')then
              begin
                with Prod.comb do
                begin
                  cProdANP := StrToIntDef(dmCupomFiscal.qECF_ItemCOD_ANP.AsString, 0) ;
                  CODIF    := '';
                  qTemp    := dmCupomFiscal.qECF_ItemQTD.AsCurrency;
                  UFcons   := dmCupomFiscal.qEmitenteUF.AsString;
                  {DMCupomFiscal.dbQuery1.Active:=False;
                   DMCupomFiscal.dbQuery1.SQL.Clear;
                   DMCupomFiscal.dbQuery1.SQL.Add('Select l.bomba as nBomba, ab.lt_encerrante as vEncFim, '+
                                                  'ab.lt_encerrante - i.qtd as vEncIni, '+
                                                  'l.bico as nBico, '+
                                                  'l.tanque as nTanque '+
                                                  'from est_abastecimentos ab '+
                                                  'Left outer join est_ECF_item i on i.abastecida = ab.id '+
                                                  'Left outer join lmc_bomba_bico l on l.bico = ab.bomba '+
                                                  'Where (Id_ecf=:pID) and (ITEM=:pItem)');
                   DMCupomFiscal.dbQuery1.ParamByName('pID').AsInteger  := dmCupomFiscal.qECF_ItemID_ECF.asInteger;
                   DMCupomFiscal.dbQuery1.ParamByName('pItem').AsString := dmCupomFiscal.qECF_ItemITEM.AsString;
                   DMCupomFiscal.dbQuery1.Active:=True;
                   DMCupomFiscal.dbQuery1.First;
                   //if not(DMCupomFiscal.dbQuery1.FieldByName('vEncFim').IsNull)then //Se existe valor do encerrante
                   //begin
                   with encerrante do
                   begin
                      nBico     := 7;
      //                    if not(DMCupomFiscal.dbQuery1.FieldByName('nBomba').IsNull)then
                      nBomba  := 5;
                      nTanque   := 4;
                      vEncIni   := '203057928';
                      vEncFin   := '203169120';
                    end}
                end;
              end;
              Prod.uCom     := dmCupomFiscal.qECF_ItemUNIDADE.AsString;
              Prod.qCom     := dmCupomFiscal.qECF_ItemQTD.AsCurrency;
              Prod.vUnCom   := dmCupomFiscal.qECF_ItemVLR.AsCurrency;
              Prod.vProd    := dmCupomFiscal.qECF_ItemTOTAL.AsCurrency +
                               dmCupomFiscal.qECF_ItemVLR_DESCONTO_ITEM.AsCurrency+
                               dmCupomFiscal.qECF_ItemVLR_DESCONTO.AsCurrency; //Anderson 10.12.14
              Prod.cEANTrib := dmCupomFiscal.qECF_ItemCEAN.AsString;
              Prod.uTrib    := dmCupomFiscal.qECF_ItemUNIDADE.AsString;
              Prod.qTrib    := dmCupomFiscal.qECF_ItemQTD.AsCurrency;
              Prod.vUnTrib  := dmCupomFiscal.qECF_ItemVLR.AsCurrency;
              Prod.vFrete   := dmCupomFiscal.qECF_ItemVLR_FRETE.AsCurrency;
              Prod.vSeg     := dmCupomFiscal.qECF_ItemVLR_SEG.AsCurrency;
              Prod.vDesc    := dmCupomFiscal.qECF_ItemVLR_DESCONTO_ITEM.AsCurrency+
                               dmCupomFiscal.qECF_ItemVLR_DESCONTO.AsCurrency; //Anderson 10.12.14
              Prod.vOutro   := dmCupomFiscal.qECF_ItemVLR_OUTRO.AsCurrency;
              vOutro        := vOutro + dmCupomFiscal.qECF_ItemVLR_OUTRO.AsCurrency;

              case StrToIntDef(dmCupomFiscal.qECF_ItemINDTOT.AsString, 1) of
                0: Prod.IndTot := itNaoSomaTotalNFe;
                1: Prod.IndTot := itSomaTotalNFe;
              end;
              //-------------------------- M - Tributos incidentes no Produto/Servi�o --------------------------
              with Imposto do
              begin
                vTotTrib := dmCupomFiscal.qECF_ItemVLR_TOTTRIB.AsCurrency+
                            dmCupomFiscal.qECF_ItemVLR_TOTTRIB_EST.AsCurrency+
                            dmCupomFiscal.qECF_ItemVLR_TOTTRIB_MUN.AsCurrency; //Lei Transparencia
                //CFOP de servi�o ISSQN
                if(dmCupomFiscal.qECF_ItemCFOP.Value = 5933) or
                (dmCupomFiscal.qECF_ItemCFOP.Value = 6933)then
                begin
                  vPIS_servttlnfe    := vPIS_servttlnfe +  dmCupomFiscal.qECF_ItemVLR_PIS.AsCurrency;
                  vCOFINS_servttlnfe := vCOFINS_servttlnfe +  dmCupomFiscal.qECF_ItemVLR_COFINS.AsCurrency;
                  with ISSQN do
                  begin
                    vBC       := dmCupomFiscal.qECF_ItemVLR_BC_ISSQN.AsCurrency;
                    vBCIssqn  := vBCIssqn + vBC;
                    vAliq     := dmCupomFiscal.qECF_ItemALI_ISSQN.AsCurrency;
                    vISSQN    := dmCupomFiscal.qECF_ItemVLR_ISSQN.AsCurrency;
                    cMunFG    := qConfigNFCeMUNICIPIO.AsInteger; // VER- SE Mun. Desti ou Emitente
                    case dmComponentes.ACBrNFe1.Configuracoes.Geral.VersaoDF of
                      ve200 : cListServ := dmCupomFiscal.qECF_ItemCLS.AsString;
                    else
                      cListServ := StringReplace(FormatFloat('00.00',dmCupomFiscal.qECF_ItemCLS.AsInteger / 100),',','.',[rfReplaceAll]);
                    end;
                    //vDeducao  := 0;
                    //vOutro := dmCupomFiscal.qECF_ItemVLR_OUTRO.AsCurrency;
                    vDescIncond := dmCupomFiscal.qECF_ItemVLR_DESCONTO.AsCurrency +
                      dmCupomFiscal.qECF_ItemVLR_DESCONTO_ITEM.AsCurrency;
                    vDescInc := vDescInc + vDescIncond;
                    vDescCond := dmCupomFiscal.qECF_ItemVLR_DESCONTO.AsCurrency +
                      dmCupomFiscal.qECF_ItemVLR_DESCONTO_ITEM.AsCurrency;
                    vDescCon := vDescCon + vDescCond;
                    //vISSRet := 0;  Ver ADM
                    indISS := iiExigivel; //Estatico
                    //cServico := '';
                    //cMun := '';
                    cPais := 1058;
                    //nProcesso := '';
                    indIncentivo := iiNao;
                    cSitTrib  := ISSQNcSitTribNORMAL; //Anderson 04.12.14 erro Muskof precisa na versao 2.00 foi eliminado na 3.10
                    vServico  := vServico + dmCupomFiscal.qECF_ItemVLR_SERVICO.AsCurrency;
                  end;
                end; //Fim ISSQN

                //-------------------------- N - ICMS Normal e ST --------------------------
                with ICMS do
                begin
                  orig     := StrToOrig(okConversao , dmCupomFiscal.qECF_ItemORIGEM_MER.AsString);
                  if Emit.CRT = crtRegimeNormal then //3-Regime Normal
                  begin
                    CST      := StrToCSTICMS(okConversao, dmCupomFiscal.qECF_ItemCST_ICM.AsString);
                    modBC    := dbiMargemValorAgregado; //0 - Estatico
                    vBC      := dmCupomFiscal.qECF_ItemVLR_BC_ICM.AsCurrency;
                    pICMS    := dmCupomFiscal.qECF_ItemALI_ICM.AsCurrency;
                    vICMS    := dmCupomFiscal.qECF_ItemVLR_ICM.AsCurrency;
                    modBCST  := dbisPrecoTabelado; //0 - Estatico
                    pMVAST   := 0;
                    pRedBCST := 0;
                    vBCST    := 0;
                    pICMSST  := 0;
                    vICMSST  := 0;
                    pRedBC   := 0;
                  end
                  else //1-Regime Simples Nacional / 2-Simples Nacional excesso de sublimite de receita bruta
                  begin
                    CSOSN       := StrToCSOSNIcms(okConversao, dmCupomFiscal.qECF_ItemCST_ICM.AsString);
                    pCredSN     := 0;
                    vCredICMSSN := 0;
                    modBCST     := dbisPrecoTabelado; //0 - Estatico
                    pMVAST      := 0;
                    pRedBCST    := 0;
                    vBCST       := 0;
                    pICMSST     := 0;
                    vICMSST     := 0;
                    vBCSTRet    := 0;
                    vICMSSTRet  := 0;
                    modBC       := dbiMargemValorAgregado; //0 - Estatico  Ver se e correto deixar esse
                    vBC         := dmCupomFiscal.qECF_ItemVLR_BC_ICM.AsCurrency;
                    pRedBC      := 0;
                    pICMS       := dmCupomFiscal.qECF_ItemALI_ICM.AsCurrency;
                    vICMS       := dmCupomFiscal.qECF_ItemVLR_ICM.AsCurrency;
                  end;
                end;
                //-------------------------- Q - PIS --------------------------
                with PIS do
                begin
                  CST   := StrToCSTPIS(okConversao, dmCupomFiscal.qECF_ItemCST_PIS.AsString);
                  vBC   := dmCupomFiscal.qECF_ItemVLR_BC_PIS.AsCurrency;
                  pPIS  := dmCupomFiscal.qECF_ItemALI_PIS.AsCurrency;
                  vPIS  := dmCupomFiscal.qECF_ItemVLR_PIS.AsCurrency;
                  {qBCProd   := 0;
                  vAliqProd := 0;
                  vPIS      := 0;}
                end;

                {//-------------------------- R - PIS ST --------------------------
                with PISST do
                begin
                  vBc       := 0;
                  pPis      := 0;
                  qBCProd   := 0;
                  vAliqProd := 0;
                  vPIS      := 0;
                end;}

                //-------------------------- S - COFINS --------------------------
                with COFINS do
                begin
                  CST     := StrToCSTCOFINS(okConversao, dmCupomFiscal.qECF_ItemCST_COFINS.AsString);
                  vBC     := dmCupomFiscal.qECF_ItemVLR_BC_COFINS.AsCurrency;
                  pCOFINS := dmCupomFiscal.qECF_ItemALI_COFINS.AsCurrency;
                  vCOFINS := dmCupomFiscal.qECF_ItemVLR_COFINS.AsCurrency;
                  {qBCProd   := 0;
                  vAliqProd := 0;}
                end;

                {//-------------------------- T - COFINS ST --------------------------
                with COFINSST do
                 begin
                   vBC       := 0;
                   pCOFINS   := 0;
                   qBCProd   := 0;
                   vAliqProd := 0;
                   vCOFINS   := 0;
                 end;

                //-------------------------- U - ISSQN --------------------------
                with ISSQN do
                begin
                  cSitTrib  := ISSQNcSitTribNORMAL;
                  vBC       := 100;
                  vAliq     := 2;
                  vISSQN    := 2;
                  cMunFG    := 3554003;
                  cListServ := 1402; // Preencha este campo usando a tabela dispon�vel
                end;}
              end;

              //-------------------------- V - Informa��es adicionais do Item --------------------------
              infAdProd     := '';
            end;
            dmCupomFiscal.qECF_Item.Next;
          end;

          //-------------------------- W - Total da NFC-e --------------------------
          vlr_Desconto_Item_Soma := getDescontoNFCeItemSemServico(id_ecf);
          with Total do
          begin
            ICMSTot.vBC         := dmCupomFiscal.qECFVLR_BC_ICM.AsCurrency;
            ICMSTot.vICMS       := dmCupomFiscal.qECFVLR_ICM.AsCurrency;
            ICMSTot.vICMSDeson  := 0;
            ICMSTot.vBCST       := 0;
            ICMSTot.vST         := 0;
            ICMSTot.vProd       := dmCupomFiscal.qECFVLR_PRODUTOS.AsCurrency +
                                   vlr_Desconto_Item_Soma +
                                   dmCupomFiscal.qECFVLR_DESCONTO.AsCurrency - vOutro;
                                  {dmCupomFiscal.qECFVLR_TOTAL.AsCurrency +
                                   vlr_Desconto_Item_Soma+
                                   dmCupomFiscal.qECFVLR_DESCONTO.AsCurrency -
                                   dmCupomFiscal.qECFVLR_SERVICO.AsCurrency;  //Anderson 10.12.14}
            ICMSTot.vFrete      := dmCupomFiscal.qECFVLR_FRETE.AsCurrency;;
            ICMSTot.vSeg        := dmCupomFiscal.qECFVLR_SEG.AsCurrency;
            vlr_Desconto_Item_Soma := getDescontoNFCeItem(id_ecf);
            ICMSTot.vDesc       := dmCupomFiscal.qECFVLR_DESCONTO.AsCurrency + vlr_Desconto_Item_Soma ; //Anderson 10.12.14
            ICMSTot.vII         := 0;
            ICMSTot.vIPI        := 0;
            ICMSTot.vPIS        := dmCupomFiscal.qECFVLR_PIS.AsCurrency - vPIS_servttlnfe;
            ICMSTot.vCOFINS     := dmCupomFiscal.qECFVLR_COFINS.AsCurrency - vCOFINS_servttlnfe;
            ICMSTot.vOutro      := dmCupomFiscal.qECFVLR_OUTRO.AsCurrency;
            ICMSTot.vNF         := dmCupomFiscal.qECFVLR_TOTAL.AsCurrency;
            ICMSTot.vTotTrib    := dmCupomFiscal.qECFVLR_TOTTRIB.AsCurrency+
                                   dmCupomFiscal.qECFVLR_TOTTRIB_EST.AsCurrency+
                                   dmCupomFiscal.qECFVLR_TOTTRIB_MUN.AsCurrency;
            if(dmCupomFiscal.qECFVLR_ISSQN.AsCurrency > 0)then
            begin
              with ISSQNtot do
              begin
                vServ     := vServico + getDescontoNFCeItemServico(ID_ECF);
                vBC       := dmCupomFiscal.qECFVLR_BC_ISSQN.AsCurrency;
                vISS      := dmCupomFiscal.qECFVLR_ISSQN.AsCurrency;
                vPIS      := vPIS_servttlnfe;
                vCOFINS   := vCOFINS_servttlnfe;
                dCompet   := dmCupomFiscal.qECFDATA.AsDateTime; //Ver Dese. Adm
                vDescIncond := vDescInc;
                vDescCond   := vDescCon;
                //cRegTrib  :=
              end; //Fim ISSQNTot
            end;
          end;

          //-------------------------- X - Informa��es do Transporte da NFC-e --------------------------
          with Transp do
          begin
            modFrete := StrTomodFrete(okConversao, dmCupomFiscal.qECFMODFRETE.AsString);
            //Outros campos desse grupo de informa�oes n�o precisam
          end;

          //Carrega Faturas do cupom fiscal  - NT2012.004 n�o deve informar Cobranca para NFCe
          {dmCupomFiscal.qECF_Fatura.Active:=False;
          dmCupomFiscal.qECF_Fatura.ParamByName('pID').AsInteger:=id_ecf;
          dmCupomFiscal.qECF_Fatura.Active:=True;
          dmCupomFiscal.qECF_Fatura.First;
          while not (dmCupomFiscal.qECF_Fatura.Eof) do
          begin
            //-------------------------- Y - Dados da Cobran�a --------------------------
            with Cobr do
            begin
              with Fat do
              begin
                nFat  := 'Numero da Fatura';
                vOrig := 100 ;
                vDesc := 0 ;
                vLiq  := 100 ;
              end;
              with Dup.Add do //Ocor: 0-120
              begin
                nDup  := dmCupomFiscal.qECF_FaturaNDUP.Value;
                dVenc := dmCupomFiscal.qECF_FaturaDATA.Value;
                vDup  := dmCupomFiscal.qECF_FaturaVALOR.Value;
              end;
            end;
            dmCupomFiscal.qECF_Fatura.Next;
          end;}

          //** Lauro ***********************************************************
          //Carrega Formas Pag do cupom fiscal
          dmCupomFiscal.qECF_FormaPag.Active:=False;
          dmCupomFiscal.qECF_FormaPag.ParamByName('pID').AsInteger:=id_ecf;
          dmCupomFiscal.qECF_FormaPag.Active:=True;
          dmCupomFiscal.qECF_FormaPag.First;
          pag.Clear;
          while not (dmCupomFiscal.qECF_FormaPag.Eof) do
          begin
            //-------------------------- YA - Formas de Pagamento --------------------------
            // LAURO 25.10 colocar condicao no SQL
            case dmCupomFiscal.qECF_FormaPagTPAG.AsInteger of
            1,2,5,10,11,12,13,99:
              begin
                with pag.Add do
                begin
                  case dmCupomFiscal.qECF_FormaPagTPAG.AsInteger of
                    01: tPag := fpDinheiro;
                    02: tPag := fpCheque;
                    05: begin
                          VendaCreditoLoja := True;
                          tPag := fpCreditoLoja;
                        end;
                    10: tPag := fpValeAlimentacao;
                    11: tPag := fpValeRefeicao;
                    12: tPag := fpValePresente;
                    13: tPag := fpValeCombustivel;
                    99: tPag := fpOutro;
                  end;
                  vPag := dmCupomFiscal.qECF_FormaPagVPAG.AsCurrency - dmCupomFiscal.qECF_FormaPagVTROCO.AsCurrency;
                end;
              end;
            end;
            dmCupomFiscal.qECF_FormaPag.Next;
          end;

          // TEF90 --Alterar o valor e pegar o troco caso ocorrer
          dmCupomFiscal.dbQuery1.active := False;
          dmCupomFiscal.dbQuery1.SQL.Clear;
          dmCupomFiscal.dbQuery1.SQL.Add('Select t.tPag,t.NSU,t.CAUTORIZACAO, c.cnpj, t.Valor, '+
                  't.CARD_CNPJ,t.CARD_TBAND,t.CARD_CAut, fp.vTroco '+
                  'from tef_movimento t '+
                  'Left outer join TEF_REDE c on c.descricao = t.rede '+
                  'left outer join est_ecf e on e.el_chave = t.el_chave '+
                  'Left outer join Est_Ecf_FormaPag fp on fp.id_ecf = e.id and fp.forma_pag=t.forma_pag '+
                  'where t.EL_Chave = :pChave and t.operacao <> 51');
          dmCupomFiscal.dbQuery1.ParamByName('pChave').asInteger := dmCupomFiscal.qECFEL_CHAVE.Value;
          dmCupomFiscal.dbQuery1.Active := True;
          dmCupomFiscal.dbQuery1.First;
          while not(dmCupomFiscal.dbQuery1.EOF) do
          begin
            with pag.Add do
            begin
              case dmCupomFiscal.dbQuery1.FieldByName('TPAG').AsInteger of
                03: tPag := fpCartaoCredito;
                04: tPag := fpCartaoDebito;
              end;
              //Mauricio 14.11.2016
              vPag := dmCupomFiscal.dbQuery1.FieldByName('Valor').AsCurrency -
                dmCupomFiscal.dbQuery1.FieldByName('vTroco').AsCurrency;
              //-- YA04 - Grupo de cart�es
              case tPag of
                fpCartaoCredito, fpCartaoDebito :begin
                  //lAURO Tipo de TEF ou POS
                  if Trim(dmCupomFiscal.dbQuery1.FieldByName('NSU').AsString)<>'' then
                  begin
                    tpIntegra := StrTotpIntegra(okConversao,'1'); //TEF
                  //  CNPJ  := limpaString(dmCupomFiscal.dbQuery1.FieldByName('CNPJ').AsString);
                  //  tBand := StrToBandeiraCartao(okConversao,dmCupomFiscal.dbQuery1.FieldByName('CARD_TBAND').AsString); //Ver
                  //  cAut  := dmCupomFiscal.dbQuery1.FieldByName('CAutorizacao').AsString; //Ver
                  end
                  else
                  begin
                    tpIntegra := StrTotpIntegra(okConversao,'2'); //POS
                    CNPJ  := limpaString(dmCupomFiscal.dbQuery1.FieldByName('CARD_CNPJ').AsString);
                    tBand := StrToBandeiraCartao(okConversao, dmCupomFiscal.dbQuery1.FieldByName('CARD_TBAND').AsString); //Ver
                    cAut  := dmCupomFiscal.dbQuery1.FieldByName('CARD_CAut').AsString; //Ver
                  end;
                end;
              end;
            end;
            dmCupomFiscal.dbQuery1.Next;
          end;
          {+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
          dmCupomFiscal.qECF_FormaPag.Active:=False;
          dmCupomFiscal.qECF_FormaPag.ParamByName('pID').AsInteger:=id_ecf;
          dmCupomFiscal.qECF_FormaPag.Active:=True;
          dmCupomFiscal.qECF_FormaPag.First;
          while not (dmCupomFiscal.qECF_FormaPag.Eof) do
          begin
            //-------------------------- YA - Formas de Pagamento --------------------------
            if not(dmComponentes.getTransacoesTefFPAG(dmCupomFiscal.qECFEL_CHAVE.Value,dmCupomFiscal.qECF_FormaPagFORMA_PAG.value))then
            begin
              with pag.Add do
              begin
                case dmCupomFiscal.qECF_FormaPagTPAG.AsInteger of
                  01: tPag := fpDinheiro;
                  02: tPag := fpCheque;
                  03: begin
                        if(Trim(dmCupomFiscal.qECF_FormaPagCARD_CAUT.AsString) <> '')Then
                          tPag := fpCartaoCredito
                        else
                          tPag := fpDinheiro;
                      end;
                  04: begin
                        if(Trim(dmCupomFiscal.qECF_FormaPagCARD_CAUT.AsString) <> '')Then
                          tPag := fpCartaoDebito
                        else
                          tPag := fpDinheiro;
                      end;
                  05: begin
                        VendaCreditoLoja := True;
                        tPag := fpCreditoLoja;
                      end;
                  10: tPag := fpValeAlimentacao;
                  11: tPag := fpValeRefeicao;
                  12: tPag := fpValePresente;
                  13: tPag := fpValeCombustivel;
                  99: tPag := fpOutro;
                end;
                vPag := dmCupomFiscal.qECF_FormaPagVPAG.AsCurrency - dmCupomFiscal.qECF_FormaPagVTROCO.AsCurrency;

                //-- YA04 - Grupo de cart�es
                case tPag of
                  fpCartaoCredito, fpCartaoDebito :
                    begin
                      tpIntegra := StrTotpIntegra(okConversao,'2');
                      CNPJ      := limpaString(dmCupomFiscal.qECF_FormaPagCARD_CNPJ.AsString);
                      tBand     := StrToBandeiraCartao(okConversao, dmCupomFiscal.qECF_FormaPagCARD_TBAND.AsString);
                      cAut      := dmCupomFiscal.qECF_FormaPagCARD_CAUT.AsString;
                    end;
                end;
              end;
            end
            else   // TEF
            begin
              dmCupomFiscal.dbQuery1.First;
              while not(dmCupomFiscal.dbQuery1.eof) do
              begin
                with pag.Add do
                begin
                  case dmCupomFiscal.qECF_FormaPagTPAG.AsInteger of
                    01: tPag := fpDinheiro;
                    02: tPag := fpCheque;
                    03: tPag := fpCartaoCredito;
                    04: tPag := fpCartaoDebito;
                    05: begin
                          VendaCreditoLoja := True;
                          tPag := fpCreditoLoja;
                        end;
                    10: tPag := fpValeAlimentacao;
                    11: tPag := fpValeRefeicao;
                    12: tPag := fpValePresente;
                    13: tPag := fpValeCombustivel;
                    99: tPag := fpOutro;
                  end;
                  vPag := dmCupomFiscal.dbQuery1.FieldByName('Valor').AsCurrency;
                  //-- YA04 - Grupo de cart�es
                  case tPag of
                    fpCartaoCredito, fpCartaoDebito :
                      begin
                        if(TEF.Ativo)then
                          tpIntegra := StrTotpIntegra(okConversao,'1')
                        else
                          tpIntegra := StrTotpIntegra(okConversao,'2');
                        CNPJ      := limpaString(dmCupomFiscal.dbQuery1.FieldByName('CNPJ').AsString);
                        tBand     := StrToBandeiraCartao(okConversao, dmCupomFiscal.qECF_FormaPagCARD_TBAND.AsString);
                        cAut      := dmCupomFiscal.dbQuery1.FieldByName('CAutorizacao').AsString;
                      end;
                  end;
                end;
                dmCupomFiscal.dbQuery1.Next;
              end;
            end;
            dmCupomFiscal.qECF_FormaPag.Next;
          end;
          }

          //-------------------------- Z - Informa��es adicionais da NFCe --------------------------
          with InfAdic do
          begin
            infAdFisco :=  dmCupomFiscal.qECFINFADFISCO.Value; //Informacoes interesse ao fisco
            infCpl     :=  dmCupomFiscal.qECFINFCPL.Value; //Informacoes interesse ao contribuinte
          end;
        end;
        dmComponentes.ACBrNFeDANFE.vTroco   := qECFVLR_TROCO.Value;
        dmComponentes.ACBrNFeDANFE.vTribEst := qECFVLR_TOTTRIB_EST.Value;
        dmComponentes.ACBrNFeDANFE.vTribMun := qECFVLR_TOTTRIB_MUN.Value;
        dmComponentes.ACBrNFeDANFE.vTribFed := qECFVLR_TOTTRIB.Value;
      end
      else
      begin
        msgInformacao('Emitente n�o identificado. Favor verificar Centro de Custo.','');
      end;
    end
    else
    begin
      msgInformacao('NFC-e n�o encontrada','');
    end;
  except
    on e:Exception do
    begin
      logErros(Self, caminhoLog,'Erro ao gerar NF-e. '+sLineBreak+e.Message, 'Erro ao gerar NF-e','S',E);
      Abort;
    end;
  end;
end;

procedure TfrmNFCe.btnImprimirClick(Sender: TObject);
var
  lCtrlNFCe: TCtrlNFCe;
begin
  try
    bmRegistroSelecionado:=qECF.GetBookmark;
    ativarDesativarTimerRetorno('A');  //Bloqueia para nao mudar NF selecionada
    lCtrlNFCe := TCtrlNFCe.Create(dmCupomFiscal.DataBase,
      dmCupomFiscal.Transaction,lcbEmpresa.KeyValue);
    try
      lCtrlNFCe.Imprimir(qECFID.AsInteger);
    finally
      lCtrlNFCe.Free;
    end;
  finally
    getECF;
    ativarDesativarTimerRetorno('D');
    grdDadosECF.SetFocus;
    qECF.GotoBookmark(bmRegistroSelecionado);
    qECF.FreeBookmark(bmRegistroSelecionado);
    moverXMLProc();
  end;
end;

{Parametro: ccusto_selecionar
Passar codigo do ccusto que deseja ser setado como configuracao da NFCe}
procedure TfrmNFCe.getEmpresas(ccusto_selecionar:Integer);
begin
  qConfigNFCe.Active:=False;
  qConfigNFCe.Active:=True;
  qConfigNFCe.Last;
  qConfigNFCe.First;
  qConfigNFCe.Locate('LOCAL', getLocalCCusto(ccusto_selecionar), [loCaseInsensitive]);
  lcbEmpresa.KeyValue := qConfigNFCeLOCAL.AsInteger;
  if qConfigNFCe.Eof then
    msgInformacao('N�o foi poss�vel encontrar nenhuma Empresa configurada para este Centro de Custo','')
  else
    setConfigNFCeACBr();
end;

//A Consulta � para n�o trocar o XML pelo retornado da consulta quando consultar o status de uma nota
procedure TfrmNFCe.atualizarStatusNFCe(id_ecf, xcStat:integer; xMotivo, xnProt, xml_retorno:String; AConsulta: Boolean);
var flag_xml_ret_vazio : Boolean;
begin
  try
    dmCupomFiscal.IBSQL1.Close;
    dmCupomFiscal.IBSQL1.SQL.Clear;
    flag_xml_ret_vazio := Trim(xml_retorno)= '';
    xMotivo := Trim(Copy(xMotivo, 1, 255));
    case xcStat of
      102, 563:begin //Inutilizado, Ja existe pedido Inutilizacao desse numero
        if xcStat = 563 then
        begin
          xcStat:=102;
          xMotivo:='Inutilizacao de numero homologado';
        end;
        dmCupomFiscal.IBSQL1.Close;
        dmCupomFiscal.IBSQL1.SQL.Clear;
        dmCupomFiscal.IBSQL1.SQL.Add('Update est_Ecf Set status=''I'', cod_sit=''05'' where id=:pID');
        dmCupomFiscal.IBSQL1.ParamByName('pID').AsInteger:=id_ecf;
        dmCupomFiscal.IBSQL1.ExecQuery;
        dmCupomFiscal.IBSQL1.Close;
        dmCupomFiscal.Transaction.CommitRetaining;

        dmCupomFiscal.IBSQL1.SQL.Clear;
        dmCupomFiscal.IBSQL1.SQL.Add('Update est_ecf_xml Set cStat=:pcStat, xMotivo=:pxMotivo,'+
                             ' nProt_Inu=:pnProt');
        if (not flag_xml_ret_vazio and not AConsulta) then
          dmCupomFiscal.IBSQL1.SQL.Add(', xml_retorno_inu=:pXML');
        dmCupomFiscal.IBSQL1.SQL.Add(' where id_ecf=:pID_ecf');

        dmCupomFiscal.IBSQL1.ParamByName('pnProt').AsString:=xnProt;
        if (not flag_xml_ret_vazio and not AConsulta) then
          dmCupomFiscal.IBSQL1.ParamByName('pXML').AsString:=xml_retorno;
      end;
      101,135,151,155: begin //Cancelado, Evento registrado e vinculado, Cancelado Fora do prazo
        dmCupomFiscal.IBSQL1.SQL.Add('Update est_ecf_xml Set cStat=:pcStat, xMotivo=:pxMotivo,'+
                     ' nProt_Can=:pnProt');
        if not flag_xml_ret_vazio and not AConsulta then
          dmCupomFiscal.IBSQL1.SQL.Add(', xml_retorno_can=:pXML');
        dmCupomFiscal.IBSQL1.SQL.Add(' where id_ecf=:pID_ECF');
        dmCupomFiscal.IBSQL1.ParamByName('pnProt').AsString:=xnProt;
        if not flag_xml_ret_vazio and not AConsulta then
          dmCupomFiscal.IBSQL1.ParamByName('pXML').AsString:=xml_retorno;
      end;
      110,301,302,303: begin //Denegado
        //Muda para status de D de Denegado pois comandos anteriores ja cancelaram para C a nota
        dmCupomFiscal.IBSQL1.Close;
        dmCupomFiscal.IBSQL1.SQL.Clear;
        dmCupomFiscal.IBSQL1.SQL.Add('Update est_Ecf Set status=''D'', cod_sit=''04'' where id=:pID');
        dmCupomFiscal.IBSQL1.ParamByName('pID').AsInteger:=id_ecf;
        dmCupomFiscal.IBSQL1.ExecQuery;
        dmCupomFiscal.IBSQL1.Close;
        dmCupomFiscal.Transaction.CommitRetaining;

        dmCupomFiscal.IBSQL1.SQL.Clear;
        dmCupomFiscal.IBSQL1.SQL.Add('Update est_ecf_xml Set cStat=:pcStat, xMotivo=:pxMotivo,'+
                             ' nProt_Den=:pnProt');
        if not flag_xml_ret_vazio and not AConsulta then
          dmCupomFiscal.IBSQL1.SQL.Add(', xml_retorno_den=:pXML');
        dmCupomFiscal.IBSQL1.SQL.Add(' where id_ecf=:pID_ECF');
        dmCupomFiscal.IBSQL1.ParamByName('pnProt').AsString:=xnProt;
        if not flag_xml_ret_vazio and not AConsulta then
          dmCupomFiscal.IBSQL1.ParamByName('pXML').AsString:=xml_retorno;
      end;
      100,150,107:begin //Autorizado Uso, Autorizado fora prazo (Conting.), -- VER 107 Reenviar GNFe servico em operacao
        if xcStat = 107 then
        begin
          xcStat:=100;
          xMotivo:='Autorizado o uso da NF-e';
        end;
        dmCupomFiscal.IBSQL1.SQL.Add('Update est_ecf_xml Set cStat=:pcStat, xMotivo=:pxMotivo,'+
                             ' nProt=:pnProt');
        if not flag_xml_ret_vazio and not AConsulta then
          dmCupomFiscal.IBSQL1.SQL.Add(', xml_retorno=:pXML');
        dmCupomFiscal.IBSQL1.SQL.Add(' where id_ecf=:pID_ECF');
        dmCupomFiscal.IBSQL1.ParamByName('pnProt').AsString:=xnProt;
        if not flag_xml_ret_vazio and not AConsulta then
          dmCupomFiscal.IBSQL1.ParamByName('pXML').AsString:=xml_retorno;
      end;
    else
      dmCupomFiscal.IBSQL1.SQL.Add('Update est_ecf_xml Set cStat=:pcStat, xMotivo=:pxMotivo'+
                           ' where id_ecf=:pID_ECF');
    end;
    dmCupomFiscal.IBSQL1.ParamByName('pID_ECF').AsInteger:=id_ecf;
    dmCupomFiscal.IBSQL1.ParamByName('pcStat').AsVariant:=xcStat;
    dmCupomFiscal.IBSQL1.ParamByName('pxMotivo').AsString:=xMotivo;
    dmCupomFiscal.IBSQL1.ExecQuery;
    dmCupomFiscal.IBSQL1.Close;
    dmCupomFiscal.Transaction.CommitRetaining;
  except
    on e:Exception do
    begin
      dmCupomFiscal.Transaction.RollbackRetaining;
      raise EDatabaseError.Create('Ocorreu seguinte erro ao atualizar Status da NFC-e na base de dados. '+e.Message);
    end;
  end;
end;

procedure TfrmNFCe.grdDadosECFDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  if not (qECF.IsEmpty) then
  begin
    if not (gdSelected in State) then
    begin
      if ((Sender as TDBGrid).DataSource.DataSet.RecNo mod 2 = 0) then
        (Sender as TDBGrid).Canvas.Brush.Color := clMoneyGreen;
      (Sender as TDBGrid).Canvas.FillRect(Rect);
      (Sender as TDBGrid).defaultdrawcolumncell(rect,datacol,column,state);
    end;

    if Column.Index=0 then
    begin
      Canvas.FillRect(Rect);
      grdDadosECF.Canvas.FillRect(Rect);
      case qECFCSTAT.AsInteger of
        102: imgListaStatusNFe.Draw(grdDadosECF.Canvas,Rect.Left+01,Rect.Top+1,0);               //Inutilizado
        101,135,151,155: imgListaStatusNFe.Draw(grdDadosECF.Canvas,Rect.Left+01,Rect.Top+1,1);   //Cancelado
        103..105: imgListaStatusNFe.Draw(grdDadosECF.Canvas,Rect.Left+01,Rect.Top+1,2);          //Lote Rec,Proc
        110,301,302,303: imgListaStatusNFe.Draw(grdDadosECF.Canvas,Rect.Left+01,Rect.Top+1,3);   //Denegado
        201..299,304..999: imgListaStatusNFe.Draw(grdDadosECF.Canvas,Rect.Left+01,Rect.Top+1,4); //Rejeitado
        100,150: imgListaStatusNFe.Draw(grdDadosECF.Canvas,Rect.Left+01,Rect.Top+1,5);           //Autorizado Uso
        1: imgListaStatusNFe.Draw(grdDadosECF.Canvas,Rect.Left+01,Rect.Top+1,9);                 //Contingencia
      else
        imgListaStatusNFe.Draw(grdDadosECF.Canvas,Rect.Left+01,Rect.Top+1,6); //Nenhuma acima
      end;
    end;
    if Column.Index=1 then
    begin
      Canvas.FillRect(Rect);
      imgListaStatusNFe.Draw(grdDadosECF.Canvas,Rect.Left+01,Rect.Top+1,8); //Copiar Chave Acesso
    end;
    if Column.Index=2 then
    begin
      Canvas.FillRect(Rect);
      imgListaStatusNFe.Draw(grdDadosECF.Canvas,Rect.Left+01,Rect.Top+1,10);
    end;
  end;
end;

procedure TfrmNFCe.StatusdoServio1Click(Sender: TObject);
var
  lCtrlNFCe: TCtrlNFCe;
begin
  lCtrlNFCe := TCtrlNFCe.Create(dmCupomFiscal.DataBase, dmCupomFiscal.Transaction, lcbEmpresa.KeyValue);
  try
    lCtrlNFCe.VerificarStatusServico();
  finally
    lCtrlNFCe.Free;
    moverXMLProc();
  end;
{  DMComponentes.ACBrNFe1.WebServices.StatusServico.Executar;
  msgInformacao(UTF8Encode(DMComponentes.ACBrNFe1.WebServices.StatusServico.xMotivo), 'Status do Servi�o');
  moverXMLProc();}
end;

procedure TfrmNFCe.btnCopiarClick(Sender: TObject);
begin
  if (qECFCHAVE_NFCE.AsString<>'') then
  begin
    Clipboard.SetTextBuf(Pointer(qECFCHAVE_NFCE.AsString));
    clipboard.Close;
  end;
end;

procedure TfrmNFCe.cancelarECFInutilizacao(numero, ccusto: Integer; serie: String);
begin
  dmCupomFiscal.dbQuery1.Active:=False;
  dmCupomFiscal.dbQuery1.SQL.Clear;
  dmCupomFiscal.dbQuery1.SQL.Add('select id, el_chave from est_ecf where numero=:pNumero and serie=:pSerie and ccusto=:pCCusto'+
                        ' and modelo_doc=''65'' and status=''X''');
  dmCupomFiscal.dbQuery1.ParamByName('pNumero').AsInteger:=numero;
  dmCupomFiscal.dbQuery1.ParamByName('pSerie').AsString:=serie;
  dmCupomFiscal.dbQuery1.ParamByName('pCCusto').AsInteger:=ccusto;
  dmCupomFiscal.dbQuery1.Active:=True;
  dmCupomFiscal.dbQuery1.First;
  if not dmCupomFiscal.dbQuery1.Eof then
  begin
    cancelarECFBaseDados(dmCupomFiscal.dbQuery1.FieldByName('id').AsInteger, dmCupomFiscal.dbQuery1.FieldByName('el_chave').AsInteger);
  end;
end;

procedure TfrmNFCe.enviarEmail(acao, emailPara : String);
var ssl, TLS : boolean;
    iRetorno : Integer;
    lista : TStringList;
    emailCC: Tstrings;
begin
  try
    If Trim(emailPara)='' then Abort;

    emailCC:=TStringList.Create;
    if (Pos(';', emailPara) > 0) then //Varios endere�o para envio
    begin
      iRetorno := ExtractStrings([';'],[' '],PChar(emailPara),emailCC);
      if (iRetorno > 1)then
      begin
        emailPara := emailCC.Strings[0];
        emailCC.Delete(0);
      end;
    end
    else //Apenas um endere�o para envio
      emailCC.Add(emailPara);

    if (qConfigNFCeSSL.Value = 'S') then
      ssl := True
    else
      ssl := False;

    if (qConfigNFCeTLS.Value = 'S') then
      TLS := True
    else
      TLS := False;


    Application.ProcessMessages;
    try
      Sleep(500);
      dmComponentes.ConfiguraAcbrMail(qConfigNFCeSMTP_HOST.value
                                     ,qConfigNFCeSMTP_USUARIO.Value
                                     ,qConfigNFCeSMTP_SENHA.Value
                                     ,qConfigNFCeSMTP_USUARIO.Value
                                     ,qConfigNFCeEMI_RAZAO_SOCIAL.Value
                                     ,qConfigNFCeSMTP_PORTA.AsString
                                     ,ssl,TLS,False,False);
{      dmComponentes.ACBrMail1.Clear;
      dmComponentes.ACBrMail1.From     := qConfigNFCeSMTP_USUARIO.Value;
      dmComponentes.ACBrMail1.FromName := qConfigNFCeEMI_RAZAO_SOCIAL.Value;
      dmComponentes.ACBrMail1.Host     := qConfigNFCeSMTP_HOST.value;  //'smtp.gmail.com';
      dmComponentes.ACBrMail1.Username := qConfigNFCeSMTP_USUARIO.Value;
      dmComponentes.ACBrMail1.Password := qConfigNFCeSMTP_SENHA.Value;
      dmComponentes.ACBrMail1.Port     := IntToStr(qConfigNFCeSMTP_PORTA.Value);  //465 troque pela porta do seu servidor smtp
      //DMAdm.ACBrMail1.AddAddress(Trim(edtDestinatario.Text),''); // '' Pode colocar um nome para o Destinatario

      dmComponentes.ACBrMail1.SetSSL := ssl;
      dmComponentes.ACBrMail1.SetTLS := TLS;
      dmComponentes.ACBrMail1.IsHTML := False; // define que a mensagem � html
      dmComponentes.ACBrMail1.ReadingConfirmation:=False; //Confirmacao de Leitura

      dmComponentes.ACBrMail1.Priority := MP_normal;}
    except
      on e:Exception do
        ShowMessage(e.Message);
    end;
{    DMComponentes.ConfiguraAcbrMail(qConfigNFCeSMTP_HOST.value //host de envio(smtp.gmail.com)
                        , qConfigNFCeSMTP_USUARIO.Value //Usuario do email(suporte@elinfo.com.br)
                        , qConfigNFCeSMTP_SENHA.Value //a senha do email
                        , emailPara //lista de destinatarios
                        , qConfigNFCeEMI_RAZAO_SOCIAL.Value //Quem esta enviando o email
                        , IntToStr(qConfigNFCeSMTP_PORTA.value) //Qual porta usar
                        , SSl //Conexa�o segura
                        , SSL // Usa TLS
                        , True //Pede Confirma��o de leitura
                        , False); //Aguarda envio do email(UserThread = false)
                        }
    DMcomponentes.ACBrNFe1.NotasFiscais.Items[0].EnviarEmail(emailpara
                                                     ,'NFC-e N�'+qECFNUMERO.AsString,
                                                     TStrings(qConfigNFCeEMAIL_MSG.Value)
                                                     , True // Enviar PDF junto
                                                     , emailCC // Lista com emails que serão enviado cópias - TStrings
                                                     , nil); // Lista de anexos - TStrings
    emailCC.Free;
  except
    on E:Exception do
    begin
      logErros(Self, caminhoLog,'Erro ao enviar NF-e por Email', 'Erro ao enviar NF-e por Email','S',E);
    end;
  end;
end;


procedure TfrmNFCe.btnEmailClick(Sender: TObject);
var
  lCtrlNFCe: TCtrlNFCe;
begin
  try
    bmRegistroSelecionado:=qECF.GetBookmark;
    StatusBar1.panels[2].Text:='Enviando E-mail. Por favor aguarde...';
    ativarDesativarTimerRetorno('A');  //Bloqueia para nao mudar NF selecionada
    lCtrlNFCe := TCtrlNFCe.Create(dmCupomFiscal.DataBase,
      dmCupomFiscal.Transaction,lcbEmpresa.KeyValue);
    try
      lCtrlNFCe.Email(qECFID.AsInteger);
    finally
      lCtrlNFCe.Free;
    end;
  finally
    getECF;
    ativarDesativarTimerRetorno('D');
    grdDadosECF.SetFocus;
    StatusBar1.panels[2].Text:='';
    qECF.GotoBookmark(bmRegistroSelecionado);
    qECF.FreeBookmark(bmRegistroSelecionado);
  end;
end;

procedure TfrmNFCe.enviarEmailEvento(acao, emailPara: String);
var iRetorno : Integer;
    emailCC, Evento: Tstrings;
    ssl: Boolean;
begin
  try
    If Trim(emailPara)='' then Abort;

    emailCC:=TStringList.Create;
    if (Pos(';', emailPara) > 0) then //Varios endere�o para envio
    begin
      iRetorno := ExtractStrings([';'],[' '],PChar(emailPara),emailCC);
      if (iRetorno > 1)then
      begin
        emailPara := emailCC.Strings[0];
        emailCC.Delete(0);
      end;
    end
    else //Apenas um endere�o para envio
      emailCC.Add(emailPara);

    if (qConfigNFCeSSL.Value = 'S') then
      ssl := True
    else
      ssl := False;

    DMComponentes.ConfiguraAcbrMail(qConfigNFCeSMTP_HOST.value //host de envio(smtp.gmail.com)
                        , qConfigNFCeSMTP_USUARIO.Value //Usuario do email(suporte@elinfo.com.br)
                        , qConfigNFCeSMTP_SENHA.Value //a senha do email
                        , emailPara //lista de destinatarios
                        , qConfigNFCeEMI_RAZAO_SOCIAL.Value //Quem esta enviando o email
                        , IntToStr(qConfigNFCeSMTP_PORTA.value) //Qual porta usar
                        , SSl //Conexa�o segura
                        , SSL // Usa TLS
                        , True //Pede Confirma��o de leitura
                        , False); //Aguarda envio do email(UserThread = false)

    DMcomponentes.ACBrNFe1.EnviarEmailEvento(emailpara
                          ,'Registro de Evento da NFC-e N�'+qECFNUMERO.AsString,
                          TStrings(qConfigNFCeEMAIL_MSG.Value)
                          , emailCC // Lista com emails que serão enviado cópias - TStrings
                          , Evento); // Lista de anexos - TStrings

    emailCC.Free;
    Evento.Free;
  except
    on E:Exception do
    begin
      logErros(Self, caminhoLog,'Erro ao enviar Evento por Email', 'Erro ao enviar Evento por Email','S',E);
    end;
  end;
end;

function TfrmNFCe.montarCaminhoArquivoXML(): String;
var anoMes, chaveNFe, diretorioNFCe, caminho: string;
begin
  diretorioNFCe := getDiretorioNFCe; //Diretorio da pasta principal da NFCe
  anoMes    := '\'+FormatDateTime('yyyymm', qECFDATA.AsDateTime); //Pasta Mensal
  chaveNFe  := qECFCHAVE_NFCE.AsString;
  case qECFCSTAT.AsInteger of
    100,150: caminho := diretorioNFCe + anoMes + '\NFCe\' +chaveNFe+'-nfe.xml'; //Autorizado
    135: caminho     := diretorioNFCe + anoMes + '\Evento\cancelamento' +'110111' +chaveNFe+'01-ProcEventoNFe.xml';  //Cancelado
  end;
  if(FileExists(caminho))then
    Result := caminho
  else
  begin
    //Notas antigas
    case qECFCSTAT.AsInteger of
      100,150: caminho := diretorioNFCe + anoMes + '\NFe\' +chaveNFe+'-nfe.xml'; //Autorizado
      135: caminho     := diretorioNFCe + anoMes + '\Evento\' +'110111' +chaveNFe+'01-ProcEventoNFe.xml';  //Cancelado
    end;
    Result := caminho;
  end;
end;

{Criado em fun��o separada para n�o precisar toda ver recarregar empresas, e carregar no componente}
procedure TfrmNFCe.setConfigNFCeACBr();
var ok : Boolean;
begin
  if qConfigNFCeLOCAL.IsNull then
  begin
    msgInformacao('Empresa n�o possui configura��o da NFC-e','');
    Abort;
  end
  else
  begin
    LOCAL:=qConfigNFCeLOCAL.Value;
    LOCAL_UF:=qConfigNFCeUF.Value;
    LOCAL_CNPJ:=qConfigNFCeCNPJ.Value;
    LOCAL_SERIE_PADRAO :=qConfigNFCeNFCE_SERIE_PADRAO.Value;
    LOCAL_FORMA_EMISSAO:=qConfigNFCeGERAL_FORMA_EMISSAO.AsString;
    LOCAL_WEBS_AMBIENTE:=qConfigNFCeWEBS_AMBIENTE.AsString;
    LOCAL_IMPRIMIR_DETALHADO:=qConfigNFCeNFCE_DANFE_DETALHADO.AsString;
    LOCAL_ENVIAR_DIRETO:=qConfigNFCeNFCE_ENVIAR_DIRETO.AsString;

    //---Dados Certificado
    dmComponentes.ACBrNFe1.Configuracoes.Certificados.Senha := qConfigNFCeCER_SENHA.Value;
    dmComponentes.ACBrNFe1.Configuracoes.Certificados.ArquivoPFX  := qConfigNFCeCER_CAMINHO.Value;
    dmComponentes.ACBrNFe1.Configuracoes.Certificados.NumeroSerie := qConfigNFCeCER_NSERIE.Value;
    case AnsiIndexStr(qConfigNFCeCER_FORMA_ASSINATURA.Value, ['libNone', 'libOpenSSL',
      'libCapicom','libCapicomDelphiSoap','libWinCrypt', 'libCustom']) of
      0:dmComponentes.ACBrNFe1.Configuracoes.Geral.SSLLib := libNone;
      1:dmComponentes.ACBrNFe1.Configuracoes.Geral.SSLLib := libOpenSSL;
      2:dmComponentes.ACBrNFe1.Configuracoes.Geral.SSLLib := libCapicom;
      3:dmComponentes.ACBrNFe1.Configuracoes.Geral.SSLLib := libCapicomDelphiSoap;
      4:dmComponentes.ACBrNFe1.Configuracoes.Geral.SSLLib := libWinCrypt;
      5:dmComponentes.ACBrNFe1.Configuracoes.Geral.SSLLib := libCustom;
    end;
    case AnsiIndexStr(qConfigNFCeCER_SSLCRYPTLIB.Value, ['cryNone', 'cryOpenSSL','cryCapicom','cryWinCryp']) of
      0:dmComponentes.ACBrNFe1.Configuracoes.Geral.SSLCryptLib := cryNone;
      1:dmComponentes.ACBrNFe1.Configuracoes.Geral.SSLCryptLib := cryOpenSSL;
      2:dmComponentes.ACBrNFe1.Configuracoes.Geral.SSLCryptLib := cryCapicom;
      3:dmComponentes.ACBrNFe1.Configuracoes.Geral.SSLCryptLib := cryWinCrypt;
    end;
    case AnsiIndexStr(qConfigNFCeCER_SSLHTTPLIB.Value, ['httpNone', 'httpWinINet','httpWinHttp','httpOpenSSL','httpIndy']) of
      0:dmComponentes.ACBrNFe1.Configuracoes.Geral.SSLHttpLib := httpNone;
      1:dmComponentes.ACBrNFe1.Configuracoes.Geral.SSLHttpLib := httpWinINet;
      2:dmComponentes.ACBrNFe1.Configuracoes.Geral.SSLHttpLib := httpWinHttp;
      3:dmComponentes.ACBrNFe1.Configuracoes.Geral.SSLHttpLib := httpOpenSSL;
      4:dmComponentes.ACBrNFe1.Configuracoes.Geral.SSLHttpLib := httpIndy;
    end;
    case AnsiIndexStr(qConfigNFCeCER_SSLXMLSIGNLIB.Value, ['xsNone', 'xsXmlSec','xsMsXml','xsMsXmlCapicom']) of
      0:dmComponentes.ACBrNFe1.Configuracoes.Geral.SSLXmlSignLib := xsNone;
      1:dmComponentes.ACBrNFe1.Configuracoes.Geral.SSLXmlSignLib := xsXmlSec;
      2:dmComponentes.ACBrNFe1.Configuracoes.Geral.SSLXmlSignLib := xsMsXml;
      3:dmComponentes.ACBrNFe1.Configuracoes.Geral.SSLXmlSignLib := xsMsXmlCapicom;
    end;
    case AnsiIndexStr(qConfigNFCeCER_SSLTYPE.Value, ['LT_all', 'LT_SSlv2','LT_SSlv3',
      'LT_TLSv1','LT_TLSv1_1','LT_TLSv1_2','LT_SSHv2']) of
      0:dmComponentes.ACBrNFe1.SSL.SSLType := TSSLType(0);
      1:dmComponentes.ACBrNFe1.SSL.SSLType := TSSLType(1);
      2:dmComponentes.ACBrNFe1.SSL.SSLType := TSSLType(2);
      3:dmComponentes.ACBrNFe1.SSL.SSLType := TSSLType(3);
      4:dmComponentes.ACBrNFe1.SSL.SSLType := TSSLType(4);
      5:dmComponentes.ACBrNFe1.SSL.SSLType := TSSLType(5);
      6:dmComponentes.ACBrNFe1.SSL.SSLType := TSSLType(6);
    end;
    //---Configura��es
    dmComponentes.ACBrNFe1.Configuracoes.Geral.ModeloDF := moNFCe;
    dmComponentes.ACBrNFe1.Configuracoes.Geral.VersaoDF := ve310;

    //---Configura��es Arquivos
    dmComponentes.ACBrNFe1.Configuracoes.Arquivos.Salvar          := (qConfigNFCeARQUIVO_SALVAR.Text = 'S'); //Atualiza informa��es no arquivo XML da NFe
    dmComponentes.ACBrNFe1.Configuracoes.Arquivos.SepararPorMes   := (qConfigNFCeARQUIVO_PASTA_MENSAL.Text = 'S'); //Cria pasta AnoMes
    dmComponentes.ACBrNFe1.Configuracoes.Arquivos.AdicionarLiteral:= (qConfigNFCeARQUIVO_ADICIONAR_LITERAL.Text = 'S'); //Adiciona literal de acordo com situa��o (NFe, Can, Inu)
    dmComponentes.ACBrNFe1.Configuracoes.Arquivos.EmissaoPathNFe  := (qConfigNFCeARQUIVO_EMISSAO_PATHNFE.Text = 'S'); //Data Emissao para gerar Pasta Mensal
    dmComponentes.ACBrNFe1.Configuracoes.Arquivos.PathNFe   := qConfigNFCeARQUIVO_PATH_NFE.Value;
    dmComponentes.ACBrNFe1.Configuracoes.Arquivos.PathInu   := qConfigNFCeARQUIVO_PATH_INU.Value;
    dmComponentes.ACBrNFe1.Configuracoes.Arquivos.PathEvento:= qConfigNFCeARQUIVO_PATH_EVENTO.Value;
    dmComponentes.ACBrNFe1.Configuracoes.Arquivos.SepararPorCNPJ    := (qConfigNFCeARQUIVO_SEPARAR_CNPJ.Value='S');
    dmComponentes.ACBrNFe1.Configuracoes.Arquivos.SepararPorModelo  := (qConfigNFCeARQUIVO_SEPARAR_MODELO.Value='S');

    //---Configura��es Geral
    if not DirectoryExists(qConfigNFCeGERAL_PATH_SCHEMAS.Value) then
      msgAviso('Diret�rio dos Schemas n�o encontrado.','');
    dmComponentes.ACBrNFe1.Configuracoes.Geral.Salvar               := (qConfigNFCeGERAL_SALVAR.Text = 'S'); //Salva em arquivos os retornos Gerais
    dmComponentes.ACBrNFe1.Configuracoes.Geral.FormaEmissao         := StrToTpEmis(OK,qConfigNFCeGERAL_FORMA_EMISSAO.AsString);
    dmComponentes.ACBrNFe1.Configuracoes.Arquivos.PathSchemas       := qConfigNFCeGERAL_PATH_SCHEMAS.Value;
    dmComponentes.ACBrNFe1.Configuracoes.Arquivos.PathSalvar        := qConfigNFCeGERAL_PATH_SALVAR.Value;
    dmComponentes.ACBrNFe1.Configuracoes.Geral.ExibirErroSchema     := (qConfigNFCeGERAL_EXIBIR_ERRO_SCHEMA.Text = 'S');
    dmComponentes.ACBrNFe1.Configuracoes.Geral.FormatoAlerta        := qConfigNFCeGERAL_FORMATO_ALERTA.Text;
    //dmComponentes.ACBrNFe1.Configuracoes.Geral.IncluirQRCodeXMLNFCe := (qConfigNFCeNFCE_INCLUIR_QRCODE.Text = 'S');

    //---Configura��es WebServices
    dmComponentes.ACBrNFe1.Configuracoes.WebServices.Ambiente:= StrToTpAmb(Ok, qConfigNFCeWEBS_AMBIENTE.AsString);
    dmComponentes.ACBrNFe1.Configuracoes.WebServices.UF := qConfigNFCeWEBS_UF.Text;
    dmComponentes.ACBrNFe1.Configuracoes.WebServices.Visualizar := (qConfigNFCeWEBS_VISUALIZA_MSG.Text = 'S');
    dmComponentes.ACBrNFe1.Configuracoes.WebServices.ProxyHost := qConfigNFCeWEBS_PROXY_HOST.Text;
    dmComponentes.ACBrNFe1.Configuracoes.WebServices.ProxyPort := qConfigNFCeWEBS_PROXY_PORTA.Text;
    dmComponentes.ACBrNFe1.Configuracoes.WebServices.ProxyUser := qConfigNFCeWEBS_PROXY_USUARIO.Text;
    dmComponentes.ACBrNFe1.Configuracoes.WebServices.ProxyPass := qConfigNFCeWEBS_PROXY_SENHA.Text;

    dmComponentes.ACBrNFe1.Configuracoes.WebServices.TimeOut   := qConfigNFCeWEBS_TIMEOUT.Value;
    dmComponentes.ACBrNFe1.Configuracoes.Webservices.Tentativas := 15;
    dmComponentes.ACBrNFe1.Configuracoes.WebServices.AguardarConsultaRet      := 15000;
    dmComponentes.ACBrNFe1.Configuracoes.WebServices.AjustaAguardaConsultaRet := True;
    dmComponentes.ACBrNFe1.Configuracoes.WebServices.IntervaloTentativas      := 1000;

    //---Configura��es DANFE
    if (dmComponentes.ACBrNFe1.DANFE <> nil) then
    begin
      dmComponentes.ACBrNFeDANFE.Email      := qConfigNFCeE_MAIL.Value;
      dmComponentes.ACBrNFeDANFE.Fax        := qConfigNFCeFAX.Value;
      dmComponentes.ACBrNFeDANFE.Usuario    := LOGIN.usuarioFuncNome;
      dmComponentes.ACBrNFeDANFE.NFeCancelada := (qConfigNFCeDANFE_NFE_CANCELADA.Text = 'S');
      dmComponentes.ACBrNFeDANFE.NumCopias    := 1; // nao possui campo ainda para nfce
      dmComponentes.ACBrNFeDANFE.Impressora   := qConfigNFCeNFCE_DANFE_IMPRESSORA.Value;
      dmComponentes.ACBrNFeDANFE.PathPDF      := qConfigNFCeDANFE_PATH_PDF.Value;
      dmComponentes.ACBrNFeDANFE.Logo         := qConfigNFCeNFCE_DANFE_PATH_LOGO.Text;
      if FilesExists(qConfigNFCeNFCE_DANFE_PATH_LOGO.Text) then
      begin
        Image1.Picture.LoadFromFile(qConfigNFCeNFCE_DANFE_PATH_LOGO.Text);
      end;
      dmComponentes.ACBrNFeDANFE.Sistema               := qConfigNFCeDANFE_SOFTWAREHOUSE.AsString+'  ';
      dmComponentes.ACBrNFeDANFE.ImprimirDescPorc      := (qConfigNFCeDANFE_IMPDESCPORC.Text = 'S');
      dmComponentes.ACBrNFeDANFE.ImprimeDescAcrescItem := (qConfigNFCeDANFE_IMPRIMIRDESCACRESCITEM.Text = 'S');//huelison
      dmComponentes.ACBrNFeDANFE.MostrarPreview     := (qConfigNFCeNFCE_DANFE_MOSTRARPREVIEW.Text = 'S');
      dmComponentes.ACBrNFeDANFE.ProdutosPorPagina  := qConfigNFCeDANFE_PRODUTOSPAG.AsInteger;
      //dmComponentes.ACBrNFeDANFE.EspessuraBorda   := qConfigNFCeDANFE_ESPESSURABORDA.AsInteger;
      dmComponentes.ACBrNFeDANFE.TamanhoFonte_DemaisCampos := qConfigNFCeDANFE_FONTECAMPOS.AsInteger;
      dmComponentes.ACBrNFeDANFE.MargemInferior := qConfigNFCeDANFE_MARGEMINF.AsFloat;
      dmComponentes.ACBrNFeDANFE.MargemSuperior := qConfigNFCeDANFE_MARGEMSUP.AsFloat;
      dmComponentes.ACBrNFeDANFE.MargemDireita  := qConfigNFCeDANFE_MARGEMDIR.AsFloat;
      dmComponentes.ACBrNFeDANFE.MargemEsquerda := qConfigNFCeDANFE_MARGEMESQ.AsFloat;
      dmComponentes.ACBrNFeDANFE.CasasDecimais._qCom    := qConfigNFCeDANFE_DECIMAISQTD.AsInteger;
      dmComponentes.ACBrNFeDANFE.CasasDecimais._vUnCom  := qConfigNFCeDANFE_DECIMAISVALOR.AsInteger;
      dmComponentes.ACBrNFeDANFE.ExibirResumoCanhoto    := (qConfigNFCeDANFE_EXIBERESUMO.Text = 'S');
      dmComponentes.ACBrNFeDANFE.ImprimirTotalLiquido   := (qConfigNFCeDANFE_IMPRIMIRVALLIQ.Text = 'S'); //   huelison
      dmComponentes.ACBrNFeDANFE.FormularioContinuo     := (qConfigNFCeDANFE_PREIMPRESSO.Text = 'S');
      dmComponentes.ACBrNFeDANFE.MostrarStatus          := (qConfigNFCeDANFE_MOSTRARSTATUS.Text = 'S');
      dmComponentes.ACBrNFeDANFE.ExpandirLogoMarca      := (qConfigNFCeDANFE_EXPANDIRLOGO.Text = 'S');//False;
      dmComponentes.ACBrNFeDANFE.LogoemCima             := False;
      dmComponentes.ACBrNFeDANFE.ImprimeLogoLateral     := True;
      dmComponentes.ACBrNFeDANFE.QRCodeLateral          := True;
      //--NFCe
      dmComponentes.ACBrNFeDANFE.ViaConsumidor           := True;
      dmComponentes.ACBrNFeDANFE.TipoDANFE               := tiNFCe;
      dmComponentes.ACBrNFe1.Configuracoes.Geral.IdCSC   := FormatFloat('000000', StrToIntDef(qConfigNFCeNFCE_TOKEN_ID.AsString,1));
      dmComponentes.ACBrNFe1.Configuracoes.Geral.CSC     := qConfigNFCeNFCE_TOKEN.Text; //DBC0B4AB-103F-40DD-90B3-C0F956E79C95
    end;
  end;
end;

procedure TfrmNFCe.lcbEmpresaClick(Sender: TObject);
begin
  setConfigNFCeACBr();
  getECF;
end;


procedure TfrmNFCe.AtualizaInformacoesMercadoriaNFCeItem(AID_NFCe: Integer;
  AID_Mercadoria: String);
var
  QryBusca: TIBQuery;
begin
  QryBusca := TIBQuery.Create(Self);
  try
    QryBusca.active := false;
    QryBusca.Database := dmCupomFiscal.DataBase;
    QryBusca.sql.add('Select M.CEST as M_CEST, M.NCM as M_NCM, E.CEST as E_CEST, '+
                     'E.NCM as E_NCM from EST_MERCADORIAS M '+
                     'left outer join EST_ECF_ITEM E on E.MERCADORIA = M.ID '+
                     'where M.ID = :pM_ID and E.ID_ECF = :pE_ID ');
    QryBusca.ParamByName('pM_ID').AsString := AID_MERCADORIA;
    QryBusca.ParamByName('pE_ID').AsInteger := AID_NFCe;
    QryBusca.Active := True;
    if not(QryBusca.IsEmpty)then
    begin
      if(QryBusca.FieldByName('M_CEST').AsString <> QryBusca.FieldByName('E_CEST').AsString)Then
      begin
        dmCupomFiscal.IBSQL1.Close;
        dmCupomFiscal.IBSQL1.SQL.clear;
        dmCupomFiscal.IBSQL1.sql.Add('Update EST_ECF_ITEM I set '+
          'I.CEST = :pCEST '+
          'where I.ID_ECF = :pE_ID and I.Mercadoria = :pM_ID');
        dmCupomFiscal.IBSQL1.ParamByName('pCEST').AsString :=
          QryBusca.FieldByName('M_CEST').AsString;
        dmCupomFiscal.IBSQL1.ParamByName('pM_ID').AsString := AID_MERCADORIA;
        dmCupomFiscal.IBSQL1.ParamByName('pE_ID').AsInteger := AID_NFCe;
        dmCupomFiscal.IBSQL1.ExecQuery;
        dmCupomFiscal.IBSQL1.Close;
        dmCupomFiscal.Transaction.CommitRetaining;
      end;
      if(QryBusca.FieldByName('M_NCM').AsString <> QryBusca.FieldByName('E_NCM').AsString)Then
      begin
        dmCupomFiscal.IBSQL1.Close;
        dmCupomFiscal.IBSQL1.SQL.clear;
        dmCupomFiscal.IBSQL1.sql.Add('Update EST_ECF_ITEM I set '+
          'I.NCM = :pNCM '+
          'where i.ID_ECF = :pE_ID and i.Mercadoria = :pM_ID');
        dmCupomFiscal.IBSQL1.ParamByName('pNCM').AsString :=
          QryBusca.FieldByName('M_NCM').AsString;
        dmCupomFiscal.IBSQL1.ParamByName('pM_ID').AsString := AID_MERCADORIA;
        dmCupomFiscal.IBSQL1.ParamByName('pE_ID').AsInteger := AID_NFCe;
        dmCupomFiscal.IBSQL1.ExecQuery;
        dmCupomFiscal.IBSQL1.Close;
        dmCupomFiscal.Transaction.CommitRetaining;
      end;
    end;
  finally
    QryBusca.free;
  end;
end;

procedure TfrmNFCe.AtualizarDadosCliente;
var
  QryBusca: TIBQuery;
begin
  QryBusca := TIBQuery.Create(Self);
  try
    QryBusca.active := false;
    QryBusca.Database := dmCupomFiscal.DataBase;
    QryBusca.sql.add('Select c.bairro, c.cep, c.cnpjcpf, c.endereco, c.ie, '+
                     'c.municipio, c.nome, c.numero_end, '+
                     'm.Descricao from clifor c '+
                     'left outer join municipios m on m.id = c.municipio '+
                     'where c.ID = :pID');
    QryBusca.ParamByName('pID').asInteger := qECFCLIFOR.AsInteger;
    QryBusca.Active := True;
    if not(QryBusca.IsEmpty)then
    begin
      dmCupomFiscal.IBSQL1.Close;
      dmCupomFiscal.IBSQL1.SQL.clear;
      dmCupomFiscal.IBSQL1.sql.Add('Update EST_ECF e set '+
        'e.cf_bairro = :pBairro, '+
        'e.cf_cep = :pcep, '+
        'e.cf_cnpj_cpf = :pcnpj_cpf, '+
        'e.cf_ende = :pende, '+
        'e.cf_ie = :pie, '+
        'e.cf_muni = :pmuni, '+
        'e.cf_municipio = :pmunicipio, '+
        'e.cf_nome = :pnome, '+
        'e.cf_numero_end = :pnumero_end '+
        'where e.id = :pID');
      dmCupomFiscal.IBSQL1.ParamByName('pID').AsInteger := qECFID.AsInteger;
      dmCupomFiscal.IBSQL1.ParamByName('pBairro').asString :=
        QryBusca.FieldByName('bairro').asString;
      dmCupomFiscal.IBSQL1.ParamByName('pcep').asString :=
        QryBusca.FieldByName('cep').asString;
      dmCupomFiscal.IBSQL1.ParamByName('pcnpj_cpf').asString :=
        QryBusca.FieldByName('cnpjcpf').asString;
      dmCupomFiscal.IBSQL1.ParamByName('pende').asString :=
        QryBusca.FieldByName('endereco').asString;
      dmCupomFiscal.IBSQL1.ParamByName('pmuni').asString :=
        QryBusca.FieldByName('Descricao').asString;
      dmCupomFiscal.IBSQL1.ParamByName('pmunicipio').AsInteger :=
        QryBusca.FieldByName('Municipio').asInteger;
      dmCupomFiscal.IBSQL1.ParamByName('pnome').asString :=
        QryBusca.FieldByName('nome').asString;
      dmCupomFiscal.IBSQL1.ParamByName('pnumero_end').asString :=
        QryBusca.FieldByName('numero_end').asString;
      dmCupomFiscal.IBSQL1.ParamByName('pie').asString :=
        QryBusca.FieldByName('ie').asString;
      dmCupomFiscal.IBSQL1.ExecQuery;
      dmCupomFiscal.IBSQL1.Close;
      dmCupomFiscal.Transaction.CommitRetaining;
      getECF;
    end;
  finally
    QryBusca.free;
  end;
end;

procedure TfrmNFCe.atualizarStatusCCe(xcStat:integer; chnfe, xMotivo, xnProt, xml_retorno:String; dhEvento:TDateTime);
var flag_xml_ret_vazio : Boolean;
begin
  try
    dmCupomFiscal.IBSQL1.Close;
    dmCupomFiscal.IBSQL1.SQL.Clear;
    flag_xml_ret_vazio := Trim(xml_retorno)= '';
    xMotivo := Trim(Copy(xMotivo, 1, 255));
    case xcStat of
      135,136: begin //Evento registrado e vinculado, Evento registrado mas nao vinculado
        dmCupomFiscal.IBSQL1.SQL.Add('Update est_nf_cce Set cStat=:pcStat, xMotivo=:pxMotivo,'+
                     ' nProt=:pnProt');
        if not flag_xml_ret_vazio then
          dmCupomFiscal.IBSQL1.SQL.Add(', xml_retorno=:pXML');
        if dhEvento <> 0 then
          dmCupomFiscal.IBSQL1.SQL.Add(', Even_Data=:pDataHora');

        dmCupomFiscal.IBSQL1.SQL.Add(' where chnfe=:pCHNFe');
        dmCupomFiscal.IBSQL1.ParamByName('pnProt').AsString:=xnProt;
        if not flag_xml_ret_vazio then
          dmCupomFiscal.IBSQL1.ParamByName('pXML').AsString:=xml_retorno;
        if dhEvento <> 0 then
          dmCupomFiscal.IBSQL1.ParamByName('pDataHora').AsDateTime:=dhEvento;
      end;
    else
      dmCupomFiscal.IBSQL1.SQL.Add('Update est_nf_cce Set cStat=:pcStat, xMotivo=:pxMotivo'+
                           ' where chnfe=:pCHNFe');
    end;
    dmCupomFiscal.IBSQL1.ParamByName('pCHNFe').AsString   := chnfe;
    dmCupomFiscal.IBSQL1.ParamByName('pcStat').AsInteger  := xcStat;
    dmCupomFiscal.IBSQL1.ParamByName('pxMotivo').AsString := xMotivo;
    dmCupomFiscal.IBSQL1.ExecQuery;
    dmCupomFiscal.IBSQL1.Close;
    dmCupomFiscal.Transaction.CommitRetaining;
  except
    on e:Exception do
    begin
      dmCupomFiscal.Transaction.RollbackRetaining;
      raise EDatabaseError.Create('Ocorreu seguinte erro ao Atualizar Status da CC-e na base de dados. '+e.Message);
    end;
  end;
end;

procedure TfrmNFCe.verificarValidadeCertificado(Status : Integer);
var cerDataVenc : TDate;
    cerDiasValidade : Integer;
begin
  cerDataVenc := DMComponentes.ACBrNFe1.SSL.CertDataVenc;
  cerDiasValidade := DaysBetween(Date, cerDataVenc);
  if (Date > cerDataVenc) then
  begin
    logErros(Self,caminhoLog,'Data de validade do certificado digital expirou dia '+dateToStr(cerDataVenc), 'Data de validade do certificado digital expirou dia '+dateToStr(cerDataVenc),'S', nil);
    Abort;
  end
  else
  begin
    if (cerDiasValidade <= 30) and (Status = 0) then
      if(FRENTE_CAIXA.Exibir_Validade_CertDigital)then
        logErros(Self, caminhoLog, 'Certificado digital v�lido at� '+dateToStr(cerDataVenc)+#13+
                'Restam '+IntToStr(cerDiasValidade)+' dias para renova��o',
                'Certificado digital v�lido at� '+dateToStr(cerDataVenc)+#13+
                'Restam '+IntToStr(cerDiasValidade)+' dias para renova��o', 'S',
                nil)
      else
      begin
        logErros(Self, caminhoLog, '',
                'Certificado digital v�lido at� '+dateToStr(cerDataVenc)+#13+
                'Restam '+IntToStr(cerDiasValidade)+' dias para renova��o', 'S',
                nil);
        imgCertificado.Visible := True;
        imgCertificado.Hint := 'Certificado digital v�lido at� '+dateToStr(cerDataVenc);
      end
    else
      if(Status = 1)then
      begin
        if(FRENTE_CAIXA.Exibir_Validade_CertDigital)then
          logErros(Self, caminhoLog, 'Certificado digital v�lido at� '+dateToStr(cerDataVenc)+#13+
                  'Restam '+IntToStr(cerDiasValidade)+' dias para renova��o',
                  'Certificado digital v�lido at� '+dateToStr(cerDataVenc)+#13+
                  'Restam '+IntToStr(cerDiasValidade)+' dias para renova��o', 'S',
                  nil)
        else
        begin
          logErros(Self, caminhoLog, '',
                  'Certificado digital v�lido at� '+dateToStr(cerDataVenc)+#13+
                  'Restam '+IntToStr(cerDiasValidade)+' dias para renova��o', 'S',
                  nil);
          imgCertificado.Visible := True;
          imgCertificado.Hint := 'Certificado digital v�lido at� '+dateToStr(cerDataVenc);
        end
      end
      else
       imgCertificado.Visible := False;
  end;
end;

procedure TfrmNFCe.ValidadeCertificadoDigital1Click(Sender: TObject);
begin
  verificarValidadeCertificado(1);
end;

procedure TfrmNFCe.moverXMLProc();
var diretorioProc, nomeArquivo: String;
begin
  try
    diretorioProc := getDiretorioNFCe +'\PROC\';
    moverVariosArquivos(qConfigNFCeGERAL_PATH_SALVAR.Value, diretorioProc, '.xml' );
  except
    on E:Exception do
      logErros(Self, caminhoLog,'','Erro ao mover arquivo de retorno','S', E);
  end;
end;

procedure TfrmNFCe.GerararquivoXML1Click(Sender: TObject);
var nomeArquivo : string;
begin
  try
    if (qECFID.Value > 0) then
    begin
      setDadosNFCeACBr(qECFID.AsInteger);
      try
        DMComponentes.ACBrNFe1.NotasFiscais.Validar;
      except
        on e:Exception do
        begin
          if Trim(DMComponentes.ACBrNFe1.NotasFiscais.Items[0].ErroValidacaoCompleto)<>'' then
            msgErro(DMComponentes.ACBrNFe1.NotasFiscais.Items[0].ErroValidacaoCompleto,'Erros na valida��o dos dados')
          else
            msgErro(e.Message,'Erros na valida��o dos dados');
        end;
      end;
      nomeArquivo := 'NFCE'+FormatFloat('000',qECFLOCAL_ID.Value)+FormatFloat('000000000',qECFNUMERO.Value)+FormatFloat('000',qECFSERIE.AsInteger)+'-nfe.xml';
      DMComponentes.ACBrNFe1.NotasFiscais[0].GravarXML(nomeArquivo, DMComponentes.ACBrNFe1.Configuracoes.Arquivos.PathSalvar);
      msgInformacao('Arquivo Gerado com sucesso!'+#13+DMComponentes.ACBrNFe1.Configuracoes.Arquivos.PathSalvar+nomearquivo,Application.Title);
    end;
  except
    on E:Exception do
    begin
      logErros(Sender, caminhoLog,e.message+' Erro ao gerar arquivo XML!','Erro ao gerar arquivo XML!','S',E);
    end;
  end;
end;

procedure TfrmNFCe.lcmbModeloDocClick(Sender: TObject);
begin
  getECF;
end;

procedure TfrmNFCe.grdDadosECFMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
  if (grdDadosECF.MouseCoord(X, Y).X in [2]) then
  begin
    grdDadosECF.ShowHint := True;
    grdDadosECF.Hint := 'Copiar chave de acesso para mem�ria tempor�ria';
  end
  else
  begin
    if (grdDadosECF.MouseCoord(X, Y).X in [3]) then
    begin
      grdDadosECF.ShowHint := True;
      grdDadosECF.Hint := 'Atualizar dados do cliente apartir do seu cadastro';
    end
    else
      grdDadosECF.ShowHint := False;
  end;
end;

procedure TfrmNFCe.grdDadosECFMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  nId: Integer;
begin
  {Obs: X representa o n� da coluna (column) e Y representa o n� da linha (row)}
  if (grdDadosECF.MouseCoord(X, Y).X in [2]) then //Coluna 2
  begin
    if (qECFCHAVE_NFCE.AsString<>'') then
    begin
      Clipboard.SetTextBuf(Pointer(qECFCHAVE_NFCE.AsString));
      Clipboard.Close;
      StatusBar1.Panels[2].Text:='Chave de Acesso copiada com sucesso';
    end;
  end
  else
    if (grdDadosECF.MouseCoord(X, Y).X in [3]) then //Coluna 2
    begin
      if (qECFCSTAT.AsInteger <> 102) and (qECFCSTAT.AsInteger <> 101) and (qECFCSTAT.AsInteger <> 135) and
      (qECFCSTAT.AsInteger <> 151) and (qECFCSTAT.AsInteger <> 155) and (qECFCSTAT.AsInteger <> 103) and
      (qECFCSTAT.AsInteger <> 104) and (qECFCSTAT.AsInteger <> 105) and (qECFCSTAT.AsInteger <> 110) and
      (qECFCSTAT.AsInteger <> 301) and (qECFCSTAT.AsInteger <> 302) and (qECFCSTAT.AsInteger <> 303) and
      (qECFCSTAT.AsInteger <> 100) and (qECFCSTAT.AsInteger <> 150) and (qECFCSTAT.AsInteger <> 1) then
      begin
        nId := qECFID.AsInteger;
        AtualizarDadosCliente;
        qECF.Locate('ID',nId,[loCaseInsensitive]);
        StatusBar1.Panels[2].Text:='Dados do cliente atualizados com sucesso';
      end
      else
      begin
        StatusBar1.Panels[2].Text:='';
        msgAviso('Status do NFCe n�o permite atualizar os dados do cliente',Application.Title);
      end;
    end
    else
      StatusBar1.Panels[2].Text:='';
end;

procedure TfrmNFCe.FormShow(Sender: TObject);
begin
  StatusBar1.Panels[0].Text:='Usu�rio: '+LOGIN.usuarioFuncNome;
  dtpInicial.Date:=EL_Data -30;
  dtpFinal.Date:=EL_Data;
  getEmpresas(CCUSTO.codigo);
  getECF;
  EF1.Visible := dmCupomFiscal.TefEstaAtivo;
  //VerificarTransacoesTefNaoImpressas;
end;

procedure TfrmNFCe.FormActivate(Sender: TObject);
begin
  application.ProcessMessages;
  if (ID_NF_ENVIAR > 0) then
  begin
    getEmpresas(ID_CCUSTO_ENVIAR);
    getECF();
    if qECF.Locate('ID', ID_NF_ENVIAR, [loCaseInsensitive]) and (dmCupomFiscal.fStatusLicenca <> 'B') then
    begin
      ID_NF_ENVIAR := 0;
      case AnsiIndexStr(LOCAL_ENVIAR_DIRETO, ['S','N','P']) of
        0: begin
          Application.ProcessMessages;
          btnEnviar.Click;
        end;
        //1: N�o ira fazer o Envio
        2:begin
          if msgPergunta(Format('Enviar NFC-e n�mero/s�rie %s/%s para Sefaz ?',[qECFNUMERO.AsString, qECFSERIE.AsString]),'') then
          begin
            Application.ProcessMessages;
            btnEnviar.Click;
          end;
        end;
      end;
    end
    else
    begin
      msgAviso('NFC-e n�o encontrada para envio'+sLineBreak+sLineBreak+
        'Verifique os filtros da pesquisa e efetue o envio manualmente.','');
    end;
    ID_NF_ENVIAR := 0;
  end;
end;

procedure TfrmNFCe.btnNovoClick(Sender: TObject);
begin
  //Caso filtro esteja diferente deixa todos para o momento que vai enviar aparecer todas
  cmbSituacao.ItemIndex:=0;
  cmbSituacaoClick(Sender);

  if not (buscarCCusto(CCUSTO.codigo)) then //Carrega dados CCusto padr�o nas variaveis
    Application.Terminate;   //Finaliza Programa

  if LOCAL_SERIE_PADRAO <> '' then
  begin
    dmCupomFiscal.dbQuery3.Active:=False;
    dmCupomFiscal.dbQuery3.SQL.Clear;
    dmCupomFiscal.dbQuery3.SQL.Add('select id from Serie_Documentos Where (ID=:pID) and (modelo_doc=''65'') and (Local=:pLocal)');
    dmCupomFiscal.dbQuery3.ParamByName('pID').Value:=LOCAL_SERIE_PADRAO;
    dmCupomFiscal.dbQuery3.ParamByName('pLocal').AsInteger:=LOCAL;
    dmCupomFiscal.dbQuery3.Active:=True;
    dmCupomFiscal.dbQuery3.First;
    if dmCupomFiscal.dbQuery3.Eof then
    begin
      msgErro('S�rie de Documento "'+LOCAL_SERIE_PADRAO+'" referente a NFC-e n�o cadastrada','');
      Abort;
    end;
  end
  else
  begin
    msgInformacao('Favor configurar S�rie Padr�o da NFC-e nas configura��es da Empresa','');
    Abort;
  end;
  case FRENTE_CAIXA.Atividade of
    0: begin  //Lojas em Geral
      Application.CreateForm(TfrmLoja, frmLoja);
      frmLoja.ShowModal;
      frmLoja.Free;
    end;
    1: begin  //Supermercado
      Application.CreateForm(TfrmSupermercado, frmSupermercado);
      frmSupermercado.ShowModal;
      frmSupermercado.Free;
    end;
    2: begin  //Posto
      Application.CreateForm(TfrmPosto, frmPosto);
      frmPosto.ShowModal;
      frmPosto.Free;
    end;
  end;
  getEmpresas(CCUSTO.codigo);
  getECF();
end;

procedure TfrmNFCe.btnRecargaClick(Sender: TObject);
var
  lCtrlTef: TCtrlTef;
begin
  lCtrlTef := TCtrlTef.Create(dmCupomFiscal.DataBase, dmCupomFiscal.Transaction,
    Self, dmCupomFiscal.TefGetImpressora);
  try
    lCtrlTef.EfetuarTransacaoAdmin(tpRecarga);
  finally
    lCtrlTef.Free;
  end;
end;

procedure TfrmNFCe.rgBuscarPorClick(Sender: TObject);
begin
  case rgBuscarPor.ItemIndex of
    0:lblClifor.Caption:='Buscar pelo Nome do cliente';
    1:lblClifor.Caption:='Buscar pelo CNPJ/CPF do cliente';
  end;
end;

function TfrmNFCe.setContingenciaNFCe():Boolean;
var just, chaveCont:String;
begin
  Result:=False;
  if qECFTPEMIS.Value = '9' then
  begin
//    msgInformacao('NFC-e ja esta em conting�ncia','');
    Result:=True;
  end
  else
  begin
    frmContingencia := TfrmContingencia.Create(Self);
    frmContingencia.edtJustificativa.Text := qConfigNFCeJUSTIFICA_DPEC.Value;
    try
      case frmContingencia.ShowModal of
        mrOk:begin
          just := frmContingencia.edtJustificativa.Text;
        end;
        mrCancel,mrAbort:begin
          Abort;
        end;
      end;
    finally
      frmContingencia.Free;
    end; 
    try
      if just = '' then Abort;

      chaveCont:=MontaChaveAcessoNFe_v2(
                UFparaCodigo(qConfigNFCeUF.Value), //Codigo UF
                qECFDATA.Value,        //Data
                OnlyNumber(qConfigNFCeCNPJ.Value), //CNPJ
                qECFMODELO_DOC.AsInteger,  //Modelo Doc
                qECFSERIE.AsInteger,   //Serie
                qECFNUMERO.Value, //Numero NF
                9,  //Tipo Emissao 9-Offline
                qECFNUMERO.Value //Codigo Numerico
                );

      dmCupomFiscal.IBSQL1.Close;
      dmCupomFiscal.IBSQL1.SQL.Clear;
      dmCupomFiscal.IBSQL1.SQL.Add('Update Est_Ecf Set cont_just=:pJust, cont_dt_hr=:pData, tpEmis=:pTpEmis, chave_nfce=:pChave where id=:pID');
      dmCupomFiscal.IBSQL1.ParamByName('pID').AsInteger:=qECFID.Value;
      dmCupomFiscal.IBSQL1.ParamByName('pChave').AsString:=chaveCont;
      dmCupomFiscal.IBSQL1.ParamByName('pTpEmis').AsString:='9'; //Offline
      dmCupomFiscal.IBSQL1.ParamByName('pJust').AsString:=just;
      dmCupomFiscal.IBSQL1.ParamByName('pData').AsDateTime:=Now;
      dmCupomFiscal.IBSQL1.ExecQuery;
      dmCupomFiscal.IBSQL1.Close;
      dmCupomFiscal.Transaction.CommitRetaining;
      Result:=True;
    except
      Result:=False;
    end;
  end;
end;



procedure TfrmNFCe.Contingncia1Click(Sender: TObject);
begin
  if (qECFID.Value > 0) and not(qECF.IsEmpty) then
  begin
    try
      bmRegistroSelecionado:=qECF.GetBookmark;
      setContingenciaNFCe;
    finally
      getECF;
      grdDadosECF.SetFocus;
      qECF.GotoBookmark(bmRegistroSelecionado);
      qECF.FreeBookmark(bmRegistroSelecionado);
    end
  end;
end;

function TfrmNFCe.isContingenciaPendente(numero, ccusto: Integer; serie: String): Boolean;
begin
  dmCupomFiscal.dbQuery2.Active:=False;
  dmCupomFiscal.dbQuery2.SQL.Clear;
  dmCupomFiscal.dbQuery2.SQL.Add('select count(e.id) from est_ecf e'+
          ' left outer join est_ecf_xml x on x.id_ecf=e.id'+
          ' where (e.numero=:pNumero) and (e.serie=:pSerie) and'+
          ' (e.ccusto=:pCCusto) and (x.cstat=1)');
  dmCupomFiscal.dbQuery2.ParamByName('pNumero').AsInteger:=numero;
  dmCupomFiscal.dbQuery2.ParamByName('pSerie').AsString:=serie;
  dmCupomFiscal.dbQuery2.ParamByName('pCcusto').AsInteger:=ccusto;
  dmCupomFiscal.dbQuery2.Active:=True;
  dmCupomFiscal.dbQuery2.First;
  if dmCupomFiscal.dbQuery2.FieldByName('count').AsInteger=0 then
    Result := False
  else
    Result := True;
end;

procedure TfrmNFCe.cmbSituacaoClick(Sender: TObject);
begin
  getECF();
end;

function TfrmNFCe.getCompTef: String;
var
  txt : TStringList;
begin
  txt := TStringList.Create;
  try
    dmCupomFiscal.dbQuery2.Active:=False;
    dmCupomFiscal.dbQuery2.SQL.Clear;
    dmCupomFiscal.dbQuery2.SQL.Add('select cast(MCUPOMREDUZIDO as Varchar(1000)) as xComp from TEF_MOVIMENTO i '+
                                   'where i.EL_Chave=:pEL_Chave and Operacao <> :pOper');
    dmCupomFiscal.dbQuery2.ParamByName('pEL_Chave').AsInteger:=qECFEL_CHAVE.AsInteger;
    dmCupomFiscal.dbQuery2.ParamByName('pOper').AsInteger:=51;
    dmCupomFiscal.dbQuery2.Active:=True;
    dmCupomFiscal.dbQuery2.First;
    while not(dmCupomFiscal.dbQuery2.Eof) do
    begin
      Txt.Add(dmCupomFiscal.dbQuery2.FieldByName('xComp').AsString);
      dmCupomFiscal.dbQuery2.Next;
    end;
    Result := Trim(Txt.Text);
  finally
    Txt.Free;
  end;
end;

function TfrmNFCe.getDescontoNFCeItem(id_ecf: Integer): Currency;
begin
  dmCupomFiscal.dbQuery2.Active:=False;
  dmCupomFiscal.dbQuery2.SQL.Clear;
  dmCupomFiscal.dbQuery2.SQL.Add('select coalesce(sum(i.vlr_desconto_item),0) soma from est_ecf_item i where i.id_ecf=:pID');
  dmCupomFiscal.dbQuery2.ParamByName('pID').AsInteger:=id_ecf;
  dmCupomFiscal.dbQuery2.Active:=True;
  dmCupomFiscal.dbQuery2.First;
  Result := dmCupomFiscal.dbQuery2.FieldByName('soma').AsCurrency;
end;


procedure TfrmNFCe.FormCreate(Sender: TObject);
begin
  btnMostrarOcultarItens.Left:=Trunc((frmNFCe.Width/2)-(btnMostrarOcultarItens.Width/2));
end;

{Criado fun�ao separada pelo fato de precisar varios locais montar esse caminho
que varia de acordo com as configura��es do cliete}
function TfrmNFCe.getDiretorioNFCe: String;
begin
  Result:= qConfigNFCeGERAL_PATH_SALVAR.Value + //Diretorio Principal Salvar
           IfThen(qConfigNFCeARQUIVO_SEPARAR_CNPJ.Value='S', '\'+OnlyNumber(qConfigNFCeCNPJ.Value), '') + //Separar por CNPJ
           IfThen(qConfigNFCeARQUIVO_SEPARAR_MODELO.Value='S', '\NFCe', ''); //Separar por Modelo (65-NFCe) (55-NFe)
end;

procedure TfrmNFCe.ImprimeVinculado(ID : Integer);
begin
  Frm_Imp_Fatura:=TFrm_Imp_Fatura.Create(Application);
  Frm_Imp_Fatura.qFatura.Active := False;
  Frm_Imp_Fatura.qFatura.ParamByName('pID_ECF').asInteger := ID;
  Frm_Imp_Fatura.qFatura.Active := True;
  Frm_Imp_Fatura.qFatura.First;
  if not(Frm_Imp_Fatura.qFatura.IsEmpty)then
  begin
    Frm_Imp_Fatura.qMercadorias.Active := False;
    Frm_Imp_Fatura.qMercadorias.ParamByName('pID_ECF').asInteger := ID;
    Frm_Imp_Fatura.qMercadorias.Active := True;
    Frm_Imp_Fatura.qMercadorias.First;

    dmCupomFiscal.dbQuery1.Active := false;
    dmCupomFiscal.dbQuery1.SQL.clear;
    dmCupomFiscal.dbQuery1.SQL.Add('Select sum(VALOR) as VALOR from EST_ECF_FATURA where id_ecf = :pID');
    dmCupomFiscal.dbQuery1.ParamByName('pID').asInteger := qECFID.AsInteger;
    dmCupomFiscal.dbQuery1.Active := True;
    Frm_Imp_Fatura.RLReport1.PrintDialog:=False;
    Frm_Imp_Fatura.RLReport1.Print();

  end;
  Frm_Imp_Fatura.Free;
end;

procedure TfrmNFCe.ransaoAdministrativa1Click(Sender: TObject);
var
  lCtrlTef: TCtrlTef;
begin
  lCtrlTef := TCtrlTef.Create(dmCupomFiscal.DataBase, dmCupomFiscal.Transaction,
    Self, dmCupomFiscal.TefGetImpressora);
  try
    lCtrlTef.EfetuarTransacaoAdmin(tpOutros);
  finally
    lCtrlTef.Free;
  end;
end;

procedure TfrmNFCe.rdPrintVinculadoNewPage(Sender: TObject;
  Pagina: Integer);
begin
  dmCupomFiscal.dbQuery3.Active:=False;
  dmCupomFiscal.dbQuery3.SQL.Clear;
  dmCupomFiscal.dbQuery3.SQL.Add('select l.descricao, l.cnpj, l.nome_fantasia,'+
      ' l.endereco, l.numero_end, l.Bairro, m.Descricao as municipio, m.UF,'+
      ' l.IE, l.CEP, l.IM from locais l'+
      ' left outer join ccustos c on c.local=l.id'+
      ' left outer join Municipios m on m.ID = l.Municipio'+
      ' where c.id=:pCCusto');
  dmCupomFiscal.dbQuery3.ParamByName('pCCusto').AsInteger := CCUSTO.codigo;
  dmCupomFiscal.dbQuery3.Active:=True;
  dmCupomFiscal.dbQuery3.First;

  rdPrintVinculado.ImpC(01,25,qConfigNFCeEMI_RAZAO_SOCIAL.AsString,[]);
  rdPrintVinculado.ImpF(02,01,dmCupomFiscal.dbQuery3.fieldByName('endereco').AsString+
                              ' '+dmCupomFiscal.dbQuery3.fieldByName('numero_end').AsString+
                              ' '+dmCupomFiscal.dbQuery3.fieldByName('Bairro').AsString,[]);
  rdPrintVinculado.ImpF(03,01,dmCupomFiscal.dbQuery3.fieldByName('CEP').AsString+
                              ' '+dmCupomFiscal.dbQuery3.fieldByName('municipio').AsString+
                              ' '+dmCupomFiscal.dbQuery3.fieldByName('UF').AsString,[]);
  rdPrintVinculado.ImpF(04,01,'CNPJ: '+dmCupomFiscal.dbQuery3.fieldByName('CNPJ').AsString,[]);
  rdPrintVinculado.ImpF(05,01,'IE: '+dmCupomFiscal.dbQuery3.fieldByName('IE').AsString,[]);
  rdPrintVinculado.ImpF(06,01,'IM: '+dmCupomFiscal.dbQuery3.fieldByName('IM').AsString,[]);
  rdPrintVinculado.ImpF(07,01,'------------------------------------------------',[]);
  Linha := 8;
end;

procedure TfrmNFCe.getPagamento;
begin
  qrImpFormaPag.Active := false;
  qrImpFormaPag.ParamByName('pID').asInteger := qECFID.AsInteger;
  qrImpFormaPag.Active := True;
  if not(qrImpFormaPag.IsEmpty)then
  begin
    qrImpFormaPag.First;
    cdsImpFormaPag.Active:=True; //Lauro 19.01.17
    cdsImpFormaPag.EmptyDataSet;
    while not(qrImpFormaPag.Eof)do
    begin
      cdsImpFormaPag.Insert;
      cdsImpFormaPagTPAG.Value   := qrImpFormaPagTPAG.Value;
      cdsImpFormaPagVPAG.Value   := qrImpFormaPagVPAG.Value;
      cdsImpFormaPag.Post;
      qrImpFormaPag.Next;
    end;
  end;
end;

function TfrmNFCe.getDescontoNFCeItemSemServico(id_ecf: Integer): Currency;
begin
  dmCupomFiscal.dbQuery2.Active:=False;
  dmCupomFiscal.dbQuery2.SQL.Clear;
  dmCupomFiscal.dbQuery2.SQL.Add('select coalesce(sum(i.vlr_desconto_item),0) soma from est_ecf_item i where i.id_ecf=:pID and i.vlr_servico = 0');
  dmCupomFiscal.dbQuery2.ParamByName('pID').AsInteger:=id_ecf;
  dmCupomFiscal.dbQuery2.Active:=True;
  dmCupomFiscal.dbQuery2.First;
  Result := dmCupomFiscal.dbQuery2.FieldByName('soma').AsCurrency;
end;


function TfrmNFCe.getDescontoNFCeItemServico(id_ecf: Integer): Currency;
begin
  dmCupomFiscal.dbQuery2.Active:=False;
  dmCupomFiscal.dbQuery2.SQL.Clear;
  dmCupomFiscal.dbQuery2.SQL.Add('select coalesce(sum(i.vlr_desconto_item),0) soma from est_ecf_item i where i.id_ecf=:pID and i.vlr_servico > 0');
  dmCupomFiscal.dbQuery2.ParamByName('pID').AsInteger:=id_ecf;
  dmCupomFiscal.dbQuery2.Active:=True;
  dmCupomFiscal.dbQuery2.First;
  Result := dmCupomFiscal.dbQuery2.FieldByName('soma').AsCurrency;
end;

procedure TfrmNFCe.EnviararquivoXML1Click(Sender: TObject);
begin
  OpenDialog1.Title := 'Selecione a NFC-e';
  OpenDialog1.DefaultExt := '*-nfe.XML';
  OpenDialog1.Filter := 'Arquivos NFC-e (*-nfe.XML)|*-nfe.XML|Arquivos XML (*.XML)|*.XML|Todos os Arquivos (*.*)|*.*';
  OpenDialog1.InitialDir := dmComponentes.ACBrNFe1.Configuracoes.Arquivos.PathSalvar;
  if OpenDialog1.Execute then
  begin
    dmComponentes.ACBrNFe1.NotasFiscais.Clear;
    dmComponentes.ACBrNFe1.NotasFiscais.LoadFromFile(OpenDialog1.FileName);
    dmComponentes.ACBrNFe1.Consultar;
    verificarValidadeCertificado(0);
    try
      //Gera assinatura no XML com certificado digital
      dmComponentes.ACBrNFe1.NotasFiscais.Assinar;
    except
      on e:Exception do
      begin
        msgErro('N�o foi poss�vel assinar o arquivo XML.'+sLineBreak+e.Message, 'Erro ao assinar XML');
        Abort;
      end;
    end;
    try
      //Efetua valida��o com Schemas XML
      DMComponentes.ACBrNFe1.NotasFiscais.Validar;
      if DMComponentes.ACBrNFe1.NotasFiscais.Items[0].Alertas <> '' then
        msgInformacao(DMComponentes.ACBrNFe1.NotasFiscais.Items[0].Alertas, 'Alertas na valida��o dos dados');
    except
      on e:Exception do
      begin
        if Trim(DMComponentes.ACBrNFe1.NotasFiscais.Items[0].ErroValidacaoCompleto)<>'' then
          msgErro(DMComponentes.ACBrNFe1.NotasFiscais.Items[0].ErroValidacaoCompleto,'Erros na valida��o dos dados')
        else
          msgErro(e.Message,'Erros na valida��o dos dados');
        Abort;
      end;
    end;
    try
      DMComponentes.ACBrNFe1.Enviar(0, False, False); //Nao imprimir automatico pois caso ficasse aberto danfe nao atualizava base enquanto nao era fechado
      Application.ProcessMessages;
      msgInformacao('NFC-e enviada com sucesso!',Application.title);
    except
      on e:Exception do
        logErros(Sender, caminhoLog,'Erro ao transmitir NFC-e � Sefaz'+sLineBreak+e.Message, 'Erro ao transmitir NFC-e � Sefaz','S',E);
    end;
  end;
end;

function TfrmNFCe.GetFaturaAdicionais: String;
var
  txt : string;
begin
  if not(cdsImpFormaPag.IsEmpty)then
  begin
    cdsImpFormaPag.First;
    txt := '';
    while not(cdsImpFormaPag.eof) do
    begin
      case cdsImpFormaPagTPAG.AsInteger of
        01: txt := 'Dinheiro';
        02: txt := 'Cheque';
        03: txt := 'Cart�o de Cr�dito';
        04: txt := 'Cart�o de D�bito';
        05: txt := 'Cr�dito Loja';
        10: txt := 'Vale Alimenta��o';
        11: txt := 'Vale Refei��o';
        12: txt := 'Vale Presente';
        13: txt := 'Vale Combust�vel';
        99: txt := 'Outro';
      end;
      txt := txt+'   '+FormatFloat('R$ ###,##0.00',cdsImpFormaPagVPAG.value)+#13;
      cdsImpFormaPag.Next;
      if(cdsImpFormaPag.Eof)then
        result := txt;
    end;
  end;
end;

function TfrmNFCe.getPendentes: Boolean;
begin
  dmCupomFiscal.dbQuery2.Active := False;
  dmCupomFiscal.dbQuery2.SQL.Clear;
  dmCupomFiscal.dbQuery2.SQL.Add('select x.ID_ECF from est_ecf_xml x '+
                            'left outer join est_ecf e on e.id = x.id_ecf '+
                            'where (e.data >= :pDt) and '+
                            '((x.cstat=0) or (x.cstat is null) or '+
                            '(x.cstat between 201 and 299) or '+
                            '(x.cstat between 304 and 999) or (x.cstat = 1))');
  dmCupomFiscal.dbQuery2.ParamByName('pDt').AsDate := dtpInicial.Date;
  dmCupomFiscal.dbQuery2.Active := True;
  dmCupomFiscal.dbQuery2.First;
  Result := not dmCupomFiscal.dbQuery2.IsEmpty;
end;

function TfrmNFCe.TemTef(pID: Integer): Boolean;
var
  lQryBusca: TIBQuery;
begin
  lQryBusca := TIBQuery.Create(nil);
  try
    lQryBusca.Database := dmCupomFiscal.DataBase;
    lQryBusca.SQL.Add('select m.* from Tef_Movimento m, EST_ECF e '+
      'where e.EL_CHAVE = m.EL_CHAVE and e.ID = :pID and STATUS_TRANSACAO = ''X''');
    lQryBusca.ParamByName('pID').asInteger := pID;
    lQryBusca.Open;
    Result := not lQryBusca.IsEmpty;
    lQryBusca.Close;
  finally
    lQryBusca.Free;
  end;
end;

end.


