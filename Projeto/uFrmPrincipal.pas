unit uFrmPrincipal;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, StrUtils, ImgList, Menus, AppEvnts,
  ComCtrls, ToolWin, ExtCtrls, inifiles, ACBrBase, ACBrDevice, ACBrConsts, ACBrUtil,pcnNFe, pcnConversao,
  {$IFDEF Delphi6_UP} StrUtils, DateUtils, Types, {$ELSE} FileCtrl,{$ENDIF}
  Spin, jpeg, OleCtrls, SHDocVw
  {$IFDEF Delphi7}, XPMan{$ENDIF}, ACBrAAC, pngimage,
  XPMan, RLConsts, System.ImageList, MidasLib;

  function EL_Data():TDate;
  function getLocalCCusto(ccusto:integer):Integer;

type
  TfrmPrincipal = class(TForm)
    ApplicationEvents1: TApplicationEvents;
    mmPrincipal: TMainMenu;
    Principal1: TMenuItem;
    Utilitrios1: TMenuItem;
    nbPrincipal: TNotebook;
    ExibirLogECF1: TMenuItem;
    barStatus: TStatusBar;
    pnlLog: TPanel;
    mResp: TMemo;
    mEnviado: TMemo;
    Panel2: TPanel;
    Label1: TLabel;
    Panel3: TPanel;
    Label2: TLabel;
    Configurao1: TMenuItem;
    AlterarCCusto1: TMenuItem;
    Image1: TImage;
    N6: TMenuItem;
    imgLista: TImageList;
    Arquivo1: TMenuItem;
    Importar1: TMenuItem;
    Exportar1: TMenuItem;
    Mercadorias1: TMenuItem;
    NFCe1: TMenuItem;
    popMenImportar: TPopupMenu;
    popMemExportar: TPopupMenu;
    Mercadorias2: TMenuItem;
    NFCe2: TMenuItem;
    ToolBar1: TToolBar;
    ToolButton8: TToolButton;
    btnGerenciarNFCe: TToolButton;
    btnSeparAntesImportar: TToolButton;
    btnImportar: TToolButton;
    btnSeparAntesExportar: TToolButton;
    btnExportar: TToolButton;
    btnSeparAntesConfig: TToolButton;
    btnConfiguracao: TToolButton;
    ToolButton2: TToolButton;
    Panel1: TPanel;
    ExportarXMl: TMenuItem;
    imgFundo: TImage;
    Consulta1: TMenuItem;
    Caixa1: TMenuItem;
    Operaes1: TMenuItem;
    Sangria1: TMenuItem;
    Suprimento1: TMenuItem;
    ResumoVendas1: TMenuItem;
    HistoricodeAutorizaes1: TMenuItem;
    btnExportarXml: TToolButton;
    ToolButton3: TToolButton;
    N1: TMenuItem;
    urno1: TMenuItem;
    N2: TMenuItem;
    procedure FormShow(Sender: TObject);
    procedure ApplicationEvents1Exception(Sender: TObject; E: Exception);
    procedure FormCreate(Sender: TObject);
    procedure ExibirLogECF1Click(Sender: TObject);
    procedure Configurao1Click(Sender: TObject);
    procedure btnConfiguracaoClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure AlterarCCusto1Click(Sender: TObject);
    procedure ApplicationEvents1Message(var Msg: tagMSG; var Handled: Boolean);
    procedure btnGerenciarNFCeClick(Sender: TObject);
    procedure Principal1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure abreFormAguarde(mensagem:String);
    procedure fechaFormAguarde();
    procedure Mercadorias1Click(Sender: TObject);
    procedure NFCe1Click(Sender: TObject);
    procedure Mercadorias2Click(Sender: TObject);
    procedure NFCe2Click(Sender: TObject);
    procedure ExportarXMlClick(Sender: TObject);
    function getTurnoFuncionario(funcionario: Integer): String;
    function Valida_NCM(NCM:String): Boolean;
    procedure Caixa1Click(Sender: TObject);
    procedure Sangria1Click(Sender: TObject);
    procedure Suprimento1Click(Sender: TObject);
    procedure ResumoVendas1Click(Sender: TObject);
    procedure HistoricodeAutorizaes1Click(Sender: TObject);
    procedure btnExportarXmlClick(Sender: TObject);
    procedure urno1Click(Sender: TObject);
  private
    { Private declarations }
    procedure conectarBaseDados();
    procedure carregarLocal();
  public
    { Public declarations }
  end;

  type
    rFrenteCaixa=record
      Qtd_Maxima     : Currency;
      Qtd_Inicial    : Integer;
      Vlr_Maximo     : Currency;
      Forma_Pag      : Integer;
      Operacao       : Integer;
      Abre_Gaveta    : Boolean;
      CR_Baixa_Seletiva     : Boolean;
      CR_Lanca              : Boolean;
      CR_Vinculado          : Boolean;
      Vinculado             : String;
      Truncar               : Boolean;
      Casas_Dec_Quantidade  : Integer;
      Casas_Dec_Valor       : Integer;
      Msg_Simples_Nacional  : String;
      Autorizacao    : Boolean;
      Autorizacao_Abast : Boolean;
      CCusto         : Integer;
      Database       : String;
      Atividade      : Integer;
      MensagemCupom  : string;
      LimiteCredito  : Boolean;
      Sugerir_FormaPag : Boolean;
      IniciePor : Boolean;
      ImprimirVendedor: Boolean;
      ImprimirSaldoPagar: Boolean;
      CalcularJuro : Boolean;
      Caixa : Integer;
      DatabaseRemoto  : String;
      Altera_Valor : Boolean;
      Exibir_Contas_Vencidas : Boolean;
      Comprovante_CredDeb : Boolean;
      Comprovante_CredDeb_Itens : Boolean;
      Pesquisa_CNPJCPF : Boolean;
      Exibir_Acrescimos: Boolean;
      Exibir_Placa: Boolean;
      Exibir_KM: Boolean;
      GAV_Comando: string;
      GAV_Caminho: String;
      GUIL_Comando: String;
      Foco_Fim: Integer;
      Exibir_Vlr_Custo: Boolean;
      Permite_Frota: Boolean;
      Exibir_Validade_CertDigital: Boolean;
      Informar_Vendedor_Fim: Boolean;
      Pasta_Fotos: String;
    end;

  type
    rPosto=record
      Abastecidas_Del     : Boolean;
      Abastecidas_Exibir  : Boolean;
      Imprimir_Media      : Boolean;
      Turno               : String;
      Eventos_Venda       : Boolean;
      Abastecidas_Venda   : Boolean;
      VenderSemAbastecida : Boolean;
      AutorizacaoSemAbastecida : Boolean;
      ParcelasVenda       : Boolean;
      AlterarQuantidade   : Boolean;
    end;

  type
    rECF=record
      Modelo          : Integer;
      Porta           : String;
      TimeOut         : Integer;
      Baud            : Integer;
      SerialParams    : String;
      IntervaloAposComando : Integer;
      TentarNovamente : Boolean;
      BloqueiaMouseTeclado : Boolean;
      ExibeMsgAguarde : Boolean;
      ArredondaPorQtd : Boolean;
      GavetaSinalInvertido : Boolean;
      DescricaoGrande   : Boolean;
      ArredondaItemMFD  : Boolean;
      MensagemAguarde   : String;
      MensagemTrabalhando   : string;
      BarrasLargura     : Integer;
      BarrasAltura      : Integer;
      IgnorarFormatacao : Boolean;
      MostrarCodigo     : Boolean;
      //Var Proprias
      ExibirMsgPoucoPapel : Boolean;
    end;

  type
    rRFD=record //Registro Fita Detalhe
      GerarRFD       : Boolean;
      DirRFD         : String;
      SH_RazaoSocial : String;
      SH_COO         : Integer;
      SH_CNPJ        : String;
      SH_IE          : String;
      SH_IM          : String;
      SH_Aplicativo  : String;
      SH_NumeroAplicativo : String;
      SH_VersaoAplicativo : String;
      SH_Linha1      : String;
      SH_Linha2      : String;
    end;

  type
    rLeitor=record
      Leitor      : Boolean;
      Porta       : String;
      Baud        : Integer;
      UsarFila    : Boolean;
      Data        : Integer;
      Sufixo      : String;
      ExcluirSufixo : Boolean;
      HandShake   : Integer;
      Parity      : Integer;
      Stop        : Integer;
    end;

  type
    rBalanca=record
      Modelo    : Integer;
      Porta     : String;
      Baud      : Integer;
      HandShake : Integer;
      Parity    : Integer;
      Stop      : Integer;
      Data      : Integer;
      TimeOut   : Integer;
    end;

    type
    rPersonalizar=record
      CorFundoCampo          : String;
      CorFundoFoco           : String;
      CorFonte               : String;
      Display                : Boolean;
      MensagemDisplay        : string;
    end;
    type
    rTef=record
      Ativo                  : Boolean;
      Homologador            : String;
      GP                     : Integer;
      Versao                 : string;
      VersaoPG               : string;
      ArqReq                 : string;
      ArqResp                : string;
      ArqSts                 : string;
      ArqTmp                 : string;
      NumCertificacao        : String;
      BloqMouseTeclado       : Boolean;
    end;
    rLicenca=record
      Licenca_Servidor1    : string;
      Licenca_Servidor2    : string;
      Licenca_CaminhoBase1 : string;
      Licenca_CaminhoBase2 : string;
    end;

var
  frmPrincipal: TfrmPrincipal;
  FRENTE_CAIXA : rFrenteCaixa;
  POSTO        : rPosto;
  LEITOR       : rLeitor;
  BALANCA      : rBalanca;
  PERSONALIZAR : rPersonalizar;
  TEF          : rTef;
  Licenca      : rLicenca;

implementation

uses uFrmLoja, uFrmConfiguracao, uDMCupomFiscal, uFuncoes,
  uFrmConfigurarSerial, uDMComponentes, uRotinasGlobais, uFrmSupermercado,
  uFrmLogin, DB, uFrmNFCe, uFrmImportaMercadoria, uFrmAguarde, uFrmExportaNFCe,
  uFrmExportaXmlNFCe, uFrmConsulta_Caixa, uFrmSangriaSuprimento,
  uFrmConsultaResumoVendas, uFrmConsultaHistoricoAutorizacao, UFrmTurno_Geral;

{$R *.dfm}

procedure TfrmPrincipal.conectarBaseDados;
begin
  try
    try
      if Trim(FRENTE_CAIXA.Database) <> '' then
      begin
        If POSTO.Eventos_Venda then
          dmCupomFiscal.eventoAbastecida.Database:=dmCupomFiscal.DataBase
        Else
          dmCupomFiscal.eventoAbastecida.Destroy;      
        //---Habilita Base de Dados Administrador
        dmCupomFiscal.Database.Connected:=False;
        dmCupomFiscal.Transaction.Active:=False;
        dmCupomFiscal.Database.DatabaseName:=FRENTE_CAIXA.Database;
        dmCupomFiscal.Database.Connected:=True;
        dmCupomFiscal.Transaction.Active:=True;
      end
      else
      begin
        msgInformacao('Caminho da base de dados n�o informado!','Aten��o');
        Abort;
      end;
    except
      on e:Exception do
        raise Exception.Create(FRENTE_CAIXA.Database+sLineBreak+sLineBreak+e.Message);
    end;
  except
    on e:Exception do
    begin
      logErros(Self, caminhoLog, 'Erro ao conectar-se � base de dados:'+#13#10+e.Message, 'Erro ao conectar-se � base de dados','S',E);
      Application.Terminate;
      Abort;
    end;
  end;
end;

procedure TfrmPrincipal.FormShow(Sender: TObject);
begin
  frmConfiguracao.lerConfiguracaoINI; //Carrega configura��es do INI
  conectarBaseDados; //Conecta na base de dados conforme caminho carregado do INI
  dmComponentes.setConfigACBr; //Seta configura��o INI nos componentes ACBr
  buscarCCusto(FRENTE_CAIXA.CCusto); //Carrega dados CCusto padr�o nas variaveis
  carregarLocal();
  LOGIN.usuarioCod := 0;

  if FRENTE_CAIXA.DatabaseRemoto = '' then
  begin
    btnSeparAntesImportar.Visible:=False;
    btnSeparAntesExportar.Visible:=False;
    btnImportar.Visible:=False;
    btnExportar.Visible:=False;
  end;

  frmLogin:=TfrmLogin.create(Application);
  frmLogin.ShowModal;
  frmLogin.Free;

  If LOGIN.usuarioCod  <=0 then
    Close
  else
  begin
    //Se bloqueado acessa, mas s� para consulta
    if(dmCupomFiscal.fStatusLicenca = 'B')then
    begin
      Configurao1.Enabled := False;
      btnConfiguracao.Enabled := false;
      Sangria1.Enabled := false;
      Suprimento1.Enabled := False;
    end;
    btnGerenciarNFCe.Click;
  end;
end;

procedure TfrmPrincipal.ApplicationEvents1Exception(Sender: TObject;
  E: Exception);
begin
  logErros(Sender, caminhoLog,'','', 'S',E);
end;

procedure TfrmPrincipal.FormCreate(Sender: TObject);
begin
  Application.OnMessage := ApplicationEvents1Message;
  nbPrincipal.ActivePage := 'padrao';
end;

procedure TfrmPrincipal.ExibirLogECF1Click(Sender: TObject);
begin
  ExibirLogECF1.Checked := not ExibirLogECF1.Checked;
  if ExibirLogECF1.Checked then
    nbPrincipal.ActivePage := 'log'
  else
    nbPrincipal.ActivePage := 'padrao'
end;

procedure TfrmPrincipal.Configurao1Click(Sender: TObject);
begin
  if (getAutorizacao('ABERTURA_FORMULARIO','ABERTURA DE FORMULARIO DE CONFIGURACAO',0)) then
  begin
    Application.CreateForm(TfrmConfiguracao, frmConfiguracao);
    frmConfiguracao.ShowModal;
    frmConfiguracao.Free;
  end;
end;

procedure TfrmPrincipal.btnConfiguracaoClick(Sender: TObject);
begin
  Configurao1.Click;
end;

procedure TfrmPrincipal.btnExportarXmlClick(Sender: TObject);
begin
  ExportarXMlClick(Action);
end;

procedure TfrmPrincipal.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = vk_Escape) then
    Principal1.Click;
end;

procedure TfrmPrincipal.AlterarCCusto1Click(Sender: TObject);
var cc : String;
begin
  cc := IntToStr(CCUSTO.codigo);
  if InputQuery('<< Mudan�a de Centro de Custo >>','Informe o Centro de Custo', cc) then
  begin
    if not buscarCCusto(StrToIntDef(cc,0)) then
    begin
      msgInformacao('Centro de Custo n�o cadastrado','Aten��o');
      Abort;
    end
    else
    begin
      carregarLocal;
      msgInformacao('Mudan�a de Centro de Custo efetuada com �xito','Aten��o');
    end;
  end;
end;

procedure TfrmPrincipal.ApplicationEvents1Message(var Msg: tagMSG;
  var Handled: Boolean);
var  i: SmallInt;
begin
  //---Habilita Rolagem do scroll do mouse no DBGrid
  if Msg.message = WM_MOUSEWHEEL then
  begin
    Msg.message := WM_KEYDOWN;
    Msg.lParam := 0;

    i := HiWord(Msg.wParam);

    if i > 0 then
      Msg.wParam := VK_UP
    else
      Msg.wParam := VK_DOWN;

    Handled := False;
  end;

  //Troca Enter por Tab (TabToEnter)
  if not (Screen.ActiveControl=nil) then
  begin
    If not ((Screen.ActiveControl is TCustomMemo) or (Screen.ActiveForm.ClassName = 'TMessageForm')
    or (Screen.ActiveControl.ClassName='TBitBtn')) //and (Screen.ActiveControl.ClassName<>'TDBLookupComboBox')
    and (Screen.ActiveControl.ClassName<>'TDBGrid') or (Screen.ActiveControl.ClassName='TRealEdit') then
    begin
      if Msg.message = WM_KEYDOWN then
      begin
        if (Msg.wParam = VK_RETURN) then
        begin
          Msg.wParam := VK_CLEAR;
          Keybd_event(VK_TAB,0,0,0);
        end;
      end;
    end;
  end;
end;

procedure TfrmPrincipal.carregarLocal;
begin
  dmCupomFiscal.dbQuery2.Active:=False;
  dmCupomFiscal.dbQuery2.SQL.Clear;
  dmCupomFiscal.dbQuery2.SQL.Add('select l.descricao, l.cnpj, l.nome_fantasia from locais l'+
      ' left outer join ccustos c on c.local=l.id'+
      ' where c.id=:pCCusto');
  dmCupomFiscal.dbQuery2.ParamByName('pCCusto').AsInteger := CCUSTO.codigo;
  dmCupomFiscal.dbQuery2.Active:=True;
  dmCupomFiscal.dbQuery2.First;
  if not (dmCupomFiscal.dbQuery2.Eof) then
  begin
    frmPrincipal.Caption := Format('::. Programa Emissor de NFC-e - %s .::',[dmCupomFiscal.dbQuery2.FieldByName('nome_fantasia').AsString+' - '+dmCupomFiscal.dbQuery2.FieldByName('cnpj').AsString]);
  end
  else
  begin
    frmPrincipal.Caption := '::. Programa Emissor de NFC-e .::';
  end;
end;


function getLocalCCusto(ccusto: integer): Integer;
begin
  dmCupomFiscal.dbQuery2.Active:=False;
  dmCupomFiscal.dbQuery2.SQL.Clear;
  dmCupomFiscal.dbQuery2.SQL.Add('select l.id from locais l'+
                       ' left outer join ccustos c on c.local=l.id'+
                       ' where c.id=:pCCusto');
  dmCupomFiscal.dbQuery2.ParamByName('pCCusto').AsInteger:=ccusto;
  dmCupomFiscal.dbQuery2.Active:=True;
  dmCupomFiscal.dbQuery2.First;
  if not(dmCupomFiscal.dbQuery2.Eof) then
    Result := dmCupomFiscal.dbQuery2.FieldByName('id').AsInteger
  else
    Result := 0;
end;

function EL_Data():TDate;
begin
  // Data do Servidor
  dmCupomFiscal.dbQuery3.Active:=False;
  dmCupomFiscal.dbQuery3.SQL.Clear;
  dmCupomFiscal.dbQuery3.SQL.Add('SELECT CURRENT_DATE as Data FROM RDB$DATABASE');
  dmCupomFiscal.dbQuery3.Active:=True;
  Result:=dmCupomFiscal.dbQuery3.FieldByName('Data').AsDateTime;
end;

procedure TfrmPrincipal.btnGerenciarNFCeClick(Sender: TObject);
begin
  Application.CreateForm(TfrmNFCe, frmNFCe);
  frmNFCe.dtpInicial.Date := EL_Data - 30;
  if(frmNFCe.getPendentes)then
  begin
    msgAviso('Existem NFC-es pendentes. Favor verificar!',Application.Title);
    frmNFCe.cmbSituacao.ItemIndex := 8;
  end;
  frmNFCe.ShowModal;
  frmNFCe.Free;
end;

procedure TfrmPrincipal.Principal1Click(Sender: TObject);
begin
  if msgPergunta('Deseja mesmo encerrar a aplica��o?','Confirmar') then
    Close;
end;

procedure TfrmPrincipal.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  //Seta caixa do funcionaio USO='N'
  setCaixaUso(LOGIN.usuarioCod, 'N');
end;

procedure TfrmPrincipal.abreFormAguarde(mensagem:String);
begin
  frmAguarde := TfrmAguarde.Create(Self, mensagem);
  frmAguarde.Show; //Ir� fazer a Tela de Splash aparecer
  frmAguarde.Refresh; // Faz com que a tela atualize
end;

procedure TfrmPrincipal.fechaFormAguarde;
begin
  frmAguarde.Release; // Elimina a tela depois na mem�ria
  frmAguarde.Free;
  frmAguarde := nil; // Ir� anular a referencia ao ponteiro do objeto
end;

procedure TfrmPrincipal.Mercadorias1Click(Sender: TObject);
begin
  Application.CreateForm(TFrmImportaMercadoria, frmImportaMercadoria);
  frmImportaMercadoria.ShowModal;
  frmImportaMercadoria.Free;
end;

procedure TfrmPrincipal.NFCe1Click(Sender: TObject);
begin
  Application.CreateForm(TfrmExportaNFCe, frmExportaNFCe);
  frmExportaNFCe.ShowModal;
  frmExportaNFCe.Free;
end;

procedure TfrmPrincipal.Mercadorias2Click(Sender: TObject);
begin
  Mercadorias1.Click;
end;

procedure TfrmPrincipal.NFCe2Click(Sender: TObject);
begin
  NFCe1.Click;
end;

procedure TfrmPrincipal.ExportarXMlClick(Sender: TObject);
begin
  Application.CreateForm(TfrmExportaXmlNFCe,frmExportaXmlNFCe);
  frmExportaXmlNFCe.Showmodal;
  frmExportaXmlNFCe.Free;
end;

function TfrmPrincipal.getTurnoFuncionario(funcionario: Integer): String;
begin
  dmCupomFiscal.dbQuery2.Active:=False;
  dmCupomFiscal.dbQuery2.SQL.Clear;
  dmCupomFiscal.dbQuery2.SQL.Add('Select F.Turno From funcionarios_turnos F Where F.Funcionario=:pFuncionario'+
                                ' and (current_time BETWEEN F.Hora_ini AND F.Hora_Fim)');
  dmCupomFiscal.dbQuery2.ParamByName('pFuncionario').AsInteger := funcionario;
  dmCupomFiscal.dbQuery2.Active:=True;
  dmCupomFiscal.dbQuery2.First;
  Result :=dmCupomFiscal.dbQuery2.FieldByName('Turno').AsString;
end;

function TfrmPrincipal.Valida_NCM(NCM:String): Boolean;
begin
  Result := False;
  dmCupomFiscal.dbQuery3.active := false;
  dmCupomFiscal.dbQuery3.SQL.Clear;
  dmCupomFiscal.dbQuery3.SQL.Add('select * from PR_VALIDAR_NCM(:pNCM)');
  dmCupomFiscal.dbQuery3.ParamByName('pNCM').asString := NCM;
  dmCupomFiscal.dbQuery3.Active := True;
  if(dmCupomFiscal.dbQuery3.FieldByName('vResult').asString = 'S')then
    Result := True;
end;

procedure TfrmPrincipal.Caixa1Click(Sender: TObject);
begin
  Application.CreateForm(TFrmConsulta_Caixa, FrmConsulta_Caixa);
  FrmConsulta_Caixa.ShowModal;
  FrmConsulta_Caixa.Free;
end;

procedure TfrmPrincipal.Sangria1Click(Sender: TObject);
begin
  Application.CreateForm(TfrmSangriaSuprimento, frmSangriaSuprimento);
  frmSangriaSuprimento.parametroTipo:='SA';
  frmSangriaSuprimento.Caption:='Sangria';
  frmSangriaSuprimento.ShowModal;
  frmSangriaSuprimento.Free;
end;

procedure TfrmPrincipal.Suprimento1Click(Sender: TObject);
begin
  Application.CreateForm(TfrmSangriaSuprimento, frmSangriaSuprimento);
  frmSangriaSuprimento.parametroTipo:='SU';
  frmSangriaSuprimento.Caption:='Suprimento';
  frmSangriaSuprimento.ShowModal;
  frmSangriaSuprimento.Free;
end;

procedure TfrmPrincipal.urno1Click(Sender: TObject);
begin
  FrmTurno_Geral:=TFrmTurno_Geral.Create(Application);
  FrmTurno_Geral.ShowModal;
  FrmTurno_Geral.Free;

  frmPrincipal.Refresh;
end;

procedure TfrmPrincipal.ResumoVendas1Click(Sender: TObject);
begin
  FrmConsultaResumoVendas:=TFrmConsultaResumoVendas.Create(Application);
  FrmConsultaResumoVendas.ShowModal;
  FrmConsultaResumoVendas.Free;
end;

procedure TfrmPrincipal.HistoricodeAutorizaes1Click(Sender: TObject);
begin
  frmConsultaHistoricoAutorizacao:= TfrmConsultaHistoricoAutorizacao.create(Self);
  try
    frmConsultaHistoricoAutorizacao.ShowModal;
  finally
    frmConsultaHistoricoAutorizacao.free;  
  end;
end;

initialization
//  RLConsts.SetVersion(3,72,'B');
end.

