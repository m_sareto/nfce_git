unit UFrmSintegra;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, ExtCtrls, IniFiles;

type
  TFrmSintegra = class(TForm)
    Panel1: TPanel;
    Label2: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Municipio: TEdit;
    Fax: TEdit;
    Logradouro: TEdit;
    Numero: TEdit;
    Complemento: TEdit;
    Bairro: TEdit;
    CEP: TEdit;
    Nome_Contato: TEdit;
    Telefone: TEdit;
    CodConvenio: TEdit;
    CodNatureza: TEdit;
    CodFinalidade: TEdit;
    Panel2: TPanel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel3: TPanel;
    Enviar: TButton;
    Fechar: TButton;
    Label1: TLabel;
    Data_Inicio_Movimento: TEdit;
    Data_Fim_Movimento: TEdit;
    Label3: TLabel;
    procedure EnviarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmSintegra: TFrmSintegra;
  ini: TIniFile;

implementation

uses UEL_Daruma;

{$R *.dfm}

procedure TFrmSintegra.EnviarClick(Sender: TObject);
var
  Str_Data_Inicio_Movimento: string;
  Str_Data_Fim_Movimento: string;
  Str_Municipio: string;
  Str_Fax: string;
  Str_Cod_Convenio: string;
  Str_Cod_Natureza: string;
  Str_Cod_Finalidade: string;
  Str_Logradouro: string;
  Str_Numero: string;
  Str_Complemento: string;
  Str_Bairro: string;
  Str_CEP: string;
  Str_Nome_Contato: string;
  Str_Telefone: string;

begin
  Str_Data_Inicio_Movimento:= Trim(Data_Inicio_Movimento.Text);
  Str_Data_Fim_Movimento:= Trim(Data_Fim_Movimento.Text);
  Str_Municipio:= Trim(Municipio.Text);
  Str_Fax:= Trim(Fax.Text);
  Str_Cod_Convenio:= Trim(CodConvenio.Text);
  Str_Cod_Natureza:= Trim(CodNatureza.Text);
  Str_Cod_Finalidade:= Trim(CodFinalidade.Text);
  Str_Logradouro:= Trim(Logradouro.Text);
  Str_Numero:= Trim(Numero.Text);
  Str_Complemento:= Trim(Complemento.Text);
  Str_Bairro:= Trim(Bairro.Text);
  Str_CEP:= Trim(CEP.Text);
  Str_Numero:= Trim(Numero.Text);
  Str_Nome_Contato:= Trim(Nome_Contato.Text);
  Str_Telefone:= Trim(Telefone.Text);

  iRetorno:= UEL_Daruma.Daruma_Sintegra_GerarRegistrosArq(Str_Data_Inicio_Movimento, Str_Data_Fim_Movimento, Str_Municipio, Str_Fax, Str_Cod_Convenio, Str_Cod_Natureza, Str_Cod_Finalidade, Str_Logradouro, Str_Numero, Str_Complemento, Str_Bairro,Str_CEP, Str_Nome_Contato, Str_Telefone);
  UEL_Daruma.Daruma_Analisa_iRetorno();
end;

procedure TFrmSintegra.FormShow(Sender: TObject);
begin
  //Verificar se o arquivo existe na mesma pasta do execut�vel
  If FileExists(ExtractFilePath(Application.EXEName)+'Configuracao.ini') then
  begin
    ini:=TIniFile.Create(ExtractFilePath(Application.EXEName)+'Configuracao.ini');
    Municipio.Text:=ini.ReadString('SINTEGRA','Municipio','');
    Fax.Text:=ini.ReadString('SINTEGRA','Fax','(xx00) 0000-0000');
    CodConvenio.Text:=ini.ReadString('SINTEGRA','CodConvenio','3');
    CodNatureza.Text:=ini.ReadString('SINTEGRA','CodNatureza','3');
    CodFinalidade.Text:=ini.ReadString('SINTEGRA','CodFinalidade','1');
    Logradouro.Text:=ini.ReadString('SINTEGRA','Logradouro','');
    Numero.Text:=ini.ReadString('SINTEGRA','Numero','000');
    Complemento.Text:=ini.ReadString('SINTEGRA','Complemento','');
    Bairro.Text:=ini.ReadString('SINTEGRA','Bairro','');
    CEP.Text:=ini.ReadString('SINTEGRA','CEP','00000-000');
    Nome_Contato.Text:=ini.ReadString('SINTEGRA','Nome_Contato','');
    Telefone.Text:=ini.ReadString('SINTEGRA','Telefone','(xx00) 0000-0000');
    ini.Free;
  end
  Else
  begin
   ShowMessage('N�o Foi Possivel Localizar o Arquivo de Configura��o INI.');
   Abort;
  end;
end;

end.
