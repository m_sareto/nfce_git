unit UfrmPesquisaCodigo;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, Mask, DBCtrls, TREdit;

type
  TfrmPesquisaCodigo = class(TForm)
    edtCodigo: TEdit;
    Bevel1: TBevel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    edtDescricao: TEdit;
    reVenda: TRealEdit;
    Label4: TLabel;
    edtICMS: TEdit;
    edtEstoque: TEdit;
    lbl1: TLabel;
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtCodigoExit(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure edtCodigoKeyPress(Sender: TObject; var Key: Char);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmPesquisaCodigo: TfrmPesquisaCodigo;

implementation

uses DB, uDMCupomFiscal, uFrmSupermercado, uFuncoes, uFrmPrincipal;

{$R *.dfm}

procedure TfrmPesquisaCodigo.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  If Key=VK_Escape then Close;
end;

procedure TfrmPesquisaCodigo.edtCodigoExit(Sender: TObject);
begin
  try
    If Trim(edtCodigo.Text)<>'' then
    begin
      DMCupomFiscal.dbQuery1.Active:=False;
      DMCupomFiscal.dbQuery1.SQL.Clear;
      DMCupomFiscal.dbQuery1.SQL.ADD('select Descricao, Ativo, icm_ecf,'+
        ' (Case when ((:pDataE >= M.SM_DtE_Promocao) and'+
        ' (:pDataS <= M.SM_DtS_Promocao)) then'+
        ' M.Sm_Venda_Promocao'+
        ' Else M.Venda end) as Vlr, e.Est_Atual'+
        ' FROM EST_MERCADORIAS M '+
        ' left outer join est_mercadorias_estoque e on e.Mercadoria = m.ID and'+
        ' e.CCusto = :pCC'+
        ' WHERE (M.Id=:pId)');
      DMCupomFiscal.dbQuery1.ParamByName('pId').AsString:=Trim(edtCodigo.Text);
      DMCupomFiscal.dbQuery1.ParamByName('pCC').AsInteger := FRENTE_CAIXA.CCusto;
      DMCupomFiscal.dbQuery1.ParamByName('pDataE').AsDate:=Date;
      DMCupomFiscal.dbQuery1.ParamByName('pDataS').AsDate:=Date;
      DMCupomFiscal.dbQuery1.Active:=True;
      DMCupomFiscal.dbQuery1.First;
      If DMCupomFiscal.dbQuery1.Eof Then
        msgInformacao('Mercadoria n�o cadastrada!','')
      else
      begin
        if dmCupomFiscal.dbQuery1.FieldByName('Ativo').AsString='N' then
          msgInformacao('Mercadoria est� Inativa. Favor verificar cadastro','Aten��o')
        else
        begin
          edtDescricao.Text:=DMCupomFiscal.dbQuery1.FieldByName('Descricao').AsString;
          reVenda.Value:=DMCupomFiscal.dbQuery1.FieldByName('Vlr').AsCurrency;
          edtEstoque.Text := FormatFloat('###,###,##0.000',
            DMCupomFiscal.dbQuery1.FieldByName('Est_Atual').AsCurrency);
          try
            StrToInt(DMCupomFiscal.dbQuery1.FieldByName('icm_ecf').AsString);
            edtICMS.Text:=DMCupomFiscal.dbQuery1.FieldByName('icm_ecf').AsString+'%';
          except
            edtICMS.Text:=DMCupomFiscal.dbQuery1.FieldByName('icm_ecf').AsString;
          end;
        end;
      end;
    end;
  finally
    edtCodigo.Clear;
    edtCodigo.SetFocus;
  end;
end;

procedure TfrmPesquisaCodigo.FormShow(Sender: TObject);
begin
  Left:=60;
  Top:=130;
  Case FRENTE_CAIXA.Casas_Dec_Valor of
    2:begin
      reVenda.FormatSize:='10.2';
      reVenda.Text:=FormatFloat('##0.00',reVenda.value);
    end;
    3:begin
      reVenda.FormatSize:='10.3';
      reVenda.Text:=FormatFloat('##0.000',reVenda.value);
    end;
  end;
{  if(trim(edtCodigo.Text) <> '')then
    edtCodigoExit(action);}
end;

procedure TfrmPesquisaCodigo.edtCodigoKeyPress(Sender: TObject; var Key: Char);
begin
  //if not (Key in['0'..'9',Chr(8)]) then Key:= #0;
end;

procedure TfrmPesquisaCodigo.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action:=caFree;
  frmPesquisaCodigo:=nil;
end;

end.
