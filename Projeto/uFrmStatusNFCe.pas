unit uFrmStatusNFCe;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, pngimage, ACBrGIF;

type
  TfrmStatusNFCe = class(TForm)
    Panel1: TPanel;
    lbl1: TLabel;
    lblStatus: TLabel;
    Panel2: TPanel;
    Image1: TImage;
    procedure FormCreate(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmStatusNFCe: TfrmStatusNFCe;

implementation

uses uFuncoes;

{$R *.dfm}

procedure TfrmStatusNFCe.FormCreate(Sender: TObject);
begin
  //lbl1.Color           := HexToTColor('7b2821');
  //lblStatus.Font.Color := HexToTColor('7b2821');
  //lbl1.Color           := HexToTColor(PERSONALIZAR.corFundoCampo);
  lblStatus.Font.Color := clBlack;
end;

procedure TfrmStatusNFCe.FormKeyPress(Sender: TObject; var Key: Char);
begin
  IF(key = #13)then
    Key := #0;
end;

end.
