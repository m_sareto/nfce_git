unit uFrmImpComanda;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmImpComprovaSangriaSup, Data.DB,
  RLReport, acbrUtil, IBX.IBCustomDataSet, IBX.IBQuery, Datasnap.DBClient;

type
  TfrmImpCompComanda = class(Tform)
    rlVenda: TRLReport;
    RLBand4: TRLBand;
    RLPanel1: TRLPanel;
    RLLabel2: TRLLabel;
    RLMemo1: TRLMemo;
    RLDraw4: TRLDraw;
    RLImage1: TRLImage;
    RLMemo3: TRLMemo;
    RLMemo4: TRLMemo;
    RLBand5: TRLBand;
    rlLabelComanda: TRLLabel;
    RLDraw6: TRLDraw;
    RLLabel3: TRLLabel;
    RLDraw7: TRLDraw;
    RLBand8: TRLBand;
    mLinhaItem: TRLMemo;
    RLBand9: TRLBand;
    RLDraw8: TRLDraw;
    RLLabel4: TRLLabel;
    RLLabel5: TRLLabel;
    RLLabel6: TRLLabel;
    RLLabel7: TRLLabel;
    RLLabel8: TRLLabel;
    RLLabel9: TRLLabel;
    RLBand10: TRLBand;
    RLDraw9: TRLDraw;
    RLDraw10: TRLDraw;
    RLMemo6: TRLMemo;
    procedure lNomeFantasiaBeforePrint(Sender: TObject; var Text: string;
      var PrintIt: Boolean);
    procedure lRazaoSocialBeforePrint(Sender: TObject; var Text: string;
      var PrintIt: Boolean);
    procedure lEmitCNPJ_IE_IMBeforePrint(Sender: TObject; var Text: string;
      var PrintIt: Boolean);
    procedure lEnderecoBeforePrint(Sender: TObject; var Text: string;
      var PrintIt: Boolean);
    procedure FormCreate(Sender: TObject);
  private
    lVlrTotal : Double;
    Function GetUnidade(pID: String): String;
    { Private declarations }
  public
    constructor Create(AOwner: TComponent; lCdsDados: TClientDataSet);
    function GetDadosCabecalho: String;
    function GetDadosEndereco:String;
    Procedure GetLocal;
    { Public declarations }
  end;

var
  frmImpCompComanda: TfrmImpCompComanda;

implementation

Uses
  uDmCupomFiscal, uFrmListaComanda, uRotinasGlobais;

{$R *.dfm}

{ TfrmImpCompComanda }

constructor TfrmImpCompComanda.Create(AOwner: TComponent;
  lCdsDados: TClientDataSet);
var
  LinhaTotal: String;
begin
  inherited Create(AOwner);
  lVlrTotal := 0;
  LinhaTotal := '';
  mLinhaItem.Lines.Clear;
  lCdsDados.First;
  while not(lCdsDados.eof)do
  begin
    mLinhaItem.Lines.Add(IntToStrZero(lCdsDados.FieldByName('ITEM').AsInteger,3) + ' ' +
                             Trim(lCdsDados.FieldByName('MERCADORIA').AsString) + ' ' +
                             Trim(lCdsDados.FieldByName('DESCRICAO').AsString));


    //Centraliza os valores. A fonte dos itens foi mudada para Courier New, Pois esta o espa�o tem o mesmo tamanho dos demais caractere.
    LinhaTotal  := PadLeft(FormatFloat('#,###,##0.00', lCdsDados.FieldByName('QUANTIDADE').Value), 12) +
                   PadCenter(Trim(GetUnidade(lCdsDados.FieldByName('MERCADORIA').AsString)), 5) + ' X ' +
                   PadLeft(FormatFloat('#,###,##0.00', lCdsDados.FieldByName('VLR_UNITARIO').Value), 12) +
                   PadLeft(FormatFloat('#,###,##0.00', lCdsDados.FieldByName('VLR_TOTAL').Value), 12);

    mLinhaItem.Lines.Add(LinhaTotal);
    lVlrTotal := lVlrTotal + lCdsDados.FieldByName('VLR_TOTAL').AsFloat;
    lCdsDados.Next;
  end;
  rllabel5.Caption := FormatFloat('#,###,##0.00',lVlrTotal);
  rllabel7.Caption := FormatFloat('#,###,##0.00',0);
  rllabel9.Caption := FormatFloat('#,###,##0.00',lVlrTotal);
end;

procedure TfrmImpCompComanda.FormCreate(Sender: TObject);
begin
  GetLocal;
end;

function TfrmImpCompComanda.GetDadosCabecalho: String;
var
  CNPJ_IE_IM: String;
begin
  CNPJ_IE_IM := 'CNPJ:'+DMCupomFiscal.dbQuery3.fieldByName('cnpj').AsSTring;
  if (DMCupomFiscal.dbQuery3.fieldByName('IE').AsSTring <> '') then
    CNPJ_IE_IM := CNPJ_IE_IM + ' IE:'+DMCupomFiscal.dbQuery3.fieldByName('IE').AsSTring;
  if (DMCupomFiscal.dbQuery3.fieldByName('IM').AsSTring <> '') then
    CNPJ_IE_IM := CNPJ_IE_IM + ' IM:'+DMCupomFiscal.dbQuery3.fieldByName('IM').AsSTring;
  Result := CNPJ_IE_IM;
end;

function TfrmImpCompComanda.GetDadosEndereco: String;
var
  Endereco, CEP: String;
begin
    // Definindo dados do Cliche //
    Endereco := dmCupomFiscal.dbQuery3.FieldByName('Endereco').asString ;
    if (dmCupomFiscal.dbQuery3.FieldByName('Numero_End').asString <> '') then
      Endereco := Endereco + ', '+dmCupomFiscal.dbQuery3.FieldByName('Numero_End').asString;
    if (dmCupomFiscal.dbQuery3.FieldByName('complemento_end').asString <> '') then
      Endereco := Endereco + ' - '+dmCupomFiscal.dbQuery3.FieldByName('complemento_end').asString;
    if (dmCupomFiscal.dbQuery3.FieldByName('Bairro').asString <> '') then
      Endereco := Endereco + ' - '+dmCupomFiscal.dbQuery3.FieldByName('Bairro').asString;
    if (dmCupomFiscal.dbQuery3.FieldByName('Municipio').asString <> '') then
      Endereco := Endereco + ' - '+dmCupomFiscal.dbQuery3.FieldByName('Descricao').asString;
    if (dmCupomFiscal.dbQuery3.FieldByName('UF').asString <> '') then
      Endereco := Endereco + ' - '+dmCupomFiscal.dbQuery3.FieldByName('UF').asString;
    if (dmCupomFiscal.dbQuery3.FieldByName('Cep').asString <> '') then
      Endereco := Endereco + ' - '+dmCupomFiscal.dbQuery3.FieldByName('Cep').asString;
    if (dmCupomFiscal.dbQuery3.FieldByName('Fone').asString <> '') then
      Endereco := Endereco + ' - FONE: '+dmCupomFiscal.dbQuery3.FieldByName('Fone').asString;
  Result := Endereco;
end;

procedure TfrmImpCompComanda.GetLocal;
begin
  dmCupomFiscal.dbQuery3.Active:=False;
  dmCupomFiscal.dbQuery3.SQL.Clear;
  dmCupomFiscal.dbQuery3.SQL.Add('Select l.descricao, l.cnpj, l.nome_fantasia, l.IE, l.IM, '+
        'l.endereco, l.numero_end, l.complemento_end, l.bairro, m.descricao as Municipio, l.uf, l.cep, l.fone  from locais l '+
        'left outer join ccustos c on c.local=l.id '+
        'left outer join Municipios m on m.id=l.municipio '+
        'where c.id=:pCCusto');
  dmCupomFiscal.dbQuery3.ParamByName('pCCusto').AsInteger := CCUSTO.codigo;
  dmCupomFiscal.dbQuery3.Active:=True;
  dmCupomFiscal.dbQuery3.First;
end;

function TfrmImpCompComanda.GetUnidade(pID: String): String;
var
  lQryBusca: TIBQuery;
begin
  lQryBusca := TIBQuery.Create(Self);
  try
    lQryBusca.Database := dmCupomFiscal.DataBase;
    lQryBusca.sql.Clear;
    lQryBusca.sql.Add('Select UNIDADE from EST_MERCADORIAS where ID = '+QuotedStr(pID));
    lQryBusca.Open;
    Result := lQryBusca.FieldByName('UNIDADE').asString;
    lQryBusca.Close;
  finally
    lQryBusca.Free;
  end;
end;

procedure TfrmImpCompComanda.lEmitCNPJ_IE_IMBeforePrint(Sender: TObject;
  var Text: string; var PrintIt: Boolean);
begin
  Text := GetDadosCabecalho;
end;

procedure TfrmImpCompComanda.lEnderecoBeforePrint(Sender: TObject;
  var Text: string; var PrintIt: Boolean);
begin
  Text := GetDadosEndereco;
end;

procedure TfrmImpCompComanda.lNomeFantasiaBeforePrint(Sender: TObject;
  var Text: string; var PrintIt: Boolean);
begin
  Text := dmCupomFiscal.dbQuery3.FieldByName('Descricao').asString;
end;

procedure TfrmImpCompComanda.lRazaoSocialBeforePrint(Sender: TObject;
  var Text: string; var PrintIt: Boolean);
begin
  Text := dmCupomFiscal.dbQuery3.FieldByName('nome_fantasia').asString;
end;

end.
