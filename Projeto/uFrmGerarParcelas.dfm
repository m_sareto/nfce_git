object frmGerarParcelas: TfrmGerarParcelas
  Left = 430
  Top = 198
  BorderIcons = [biHelp]
  Caption = 'Gera'#231#227'o de Parcelas a Prazo'
  ClientHeight = 253
  ClientWidth = 394
  Color = clWindow
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDesktopCenter
  OnKeyDown = FormKeyDown
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 394
    Height = 253
    Align = alClient
    BevelOuter = bvNone
    Color = clBtnHighlight
    TabOrder = 0
    object Label63: TLabel
      Left = 7
      Top = 38
      Width = 95
      Height = 13
      Caption = 'N'#250'mero de parcelas'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBackground
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label64: TLabel
      Left = 7
      Top = 112
      Width = 100
      Height = 13
      Caption = 'Data primeira parcela'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBackground
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label65: TLabel
      Left = 7
      Top = 63
      Width = 104
      Height = 13
      Caption = 'Valor de cada parcela'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBackground
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label1: TLabel
      Left = 7
      Top = 13
      Width = 110
      Height = 13
      Caption = 'Valor Total a Prazo'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBackground
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label2: TLabel
      Left = 7
      Top = 88
      Width = 66
      Height = 13
      Caption = 'Dias de Prazo'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBackground
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object ed_n_Parcela: TEdit
      Left = 125
      Top = 35
      Width = 28
      Height = 19
      Ctl3D = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      MaxLength = 2
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 1
      Text = '1'
      OnExit = ed_n_ParcelaExit
    end
    object me_Dt_Pri_Parcela: TMaskEdit
      Left = 125
      Top = 109
      Width = 64
      Height = 19
      Ctl3D = False
      EditMask = '!99/99/00;1;_'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      MaxLength = 8
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 4
      Text = '  /  /  '
    end
    object CheckBox3: TCheckBox
      Left = 7
      Top = 136
      Width = 133
      Height = 17
      Alignment = taLeftJustify
      Caption = 'Fixar dia do vencimento'
      Checked = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBackground
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 5
    end
    object re_vl_Parcela: TRealEdit
      Left = 125
      Top = 59
      Width = 73
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Color = clNavy
      Ctl3D = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentCtl3D = False
      ParentFont = False
      ReadOnly = True
      TabOrder = 2
      FormatReal = fNumber
      FormatSize = '10.2'
    end
    object btnSimularParc: TBitBtn
      Left = 9
      Top = 186
      Width = 191
      Height = 27
      Caption = '&Simular Parcelas'
      TabOrder = 6
      OnClick = btnSimularParcClick
    end
    object Panel2: TPanel
      Left = 210
      Top = 219
      Width = 180
      Height = 22
      BorderStyle = bsSingle
      Ctl3D = False
      ParentCtl3D = False
      TabOrder = 7
      object Label66: TLabel
        Left = 12
        Top = 3
        Width = 60
        Height = 13
        Caption = 'SubTotal R$'
      end
      object re_vl_T_Parcelas: TRealEdit
        Left = 82
        Top = -1
        Width = 79
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Color = clNavy
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatReal = fNumber
        FormatSize = '10.2'
      end
    end
    object re_vl_Total_Prazo: TRealEdit
      Left = 125
      Top = 9
      Width = 73
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Color = clNavy
      Ctl3D = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentCtl3D = False
      ParentFont = False
      ReadOnly = True
      TabOrder = 0
      FormatReal = fNumber
      FormatSize = '10.2'
    end
    object btnFechar: TBitBtn
      Left = 9
      Top = 213
      Width = 191
      Height = 27
      Caption = 'Fechar'
      TabOrder = 8
      OnClick = btnFecharClick
    end
    object DBGrid2: TDBGrid
      Left = 210
      Top = 9
      Width = 180
      Height = 211
      Ctl3D = False
      DataSource = dsTemp_ECF_Fatura
      DrawingStyle = gdsClassic
      Options = [dgEditing, dgTitles, dgIndicator, dgColLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
      ParentCtl3D = False
      TabOrder = 9
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnEnter = DBGrid2Enter
      OnExit = DBGrid2Exit
      Columns = <
        item
          Expanded = False
          FieldName = 'DATA'
          Title.Caption = 'Vencimento(s)'
          Width = 72
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'VALOR'
          Title.Alignment = taRightJustify
          Title.Caption = 'Valor R$'
          Width = 76
          Visible = True
        end>
    end
    object ed_Dias: TEdit
      Left = 125
      Top = 85
      Width = 28
      Height = 19
      Ctl3D = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      MaxLength = 2
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 3
      Text = '0'
      OnExit = ed_DiasExit
    end
  end
  object dsTemp_ECF_Fatura: TDataSource
    DataSet = dmCupomFiscal.dbTemp_NFCE_Fatura
    Left = 168
    Top = 144
  end
end
