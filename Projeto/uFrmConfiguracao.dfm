object frmConfiguracao: TfrmConfiguracao
  Left = 257
  Top = 46
  Caption = 'Configura'#231#227'o'
  ClientHeight = 625
  ClientWidth = 1180
  Color = clBtnHighlight
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  WindowState = wsMaximized
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 577
    Width = 1180
    Height = 29
    Align = alBottom
    TabOrder = 0
    object btnSalvar: TBitBtn
      Left = 0
      Top = 1
      Width = 75
      Height = 25
      Caption = 'Salvar'
      Glyph.Data = {
        36060000424D3606000000000000360000002800000020000000100000000100
        18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        CCCCCC3B3B3B6D6D6D7777778181818888888888888181817777776D6D6D3B3B
        3B363636666666FFFFFFFFFFFFFFFFFFCCCCCC3B3B3B6D6D6D77777781818188
        88888888888181817777776D6D6D3B3B3B363636666666FFFFFFFFFFFFCCCCCC
        3535353B3B3BAAAAAA696969696969AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA3B3B
        3B666666353535FFFFFFFFFFFFCCCCCC3535353B3B3BAAAAAA696969696969AA
        AAAAAAAAAAAAAAAAAAAAAAAAAAAA3B3B3B666666353535FFFFFFFFFFFF393939
        696969474747B4B4B45B5B5B5B5B5BB4B4B4B4B4B4B4B4B4B4B4B4B4B4B44747
        47696969393939FFFFFFFFFFFF393939696969474747B4B4B45B5B5B5B5B5BB4
        B4B4B4B4B4B4B4B4B4B4B4B4B4B4474747696969393939FFFFFFFFFFFF3C3C3C
        6D6D6D575757C3C3C3494949494949C3C3C3C3C3C3C3C3C3C3C3C3C3C3C35757
        576D6D6D3C3C3CFFFFFFFFFFFF3C3C3C6D6D6D575757C3C3C3494949494949C3
        C3C3C3C3C3C3C3C3C3C3C3C3C3C35757576D6D6D3C3C3CFFFFFFFFFFFF414141
        727272686868D3D3D33B3B3B3B3B3BD3D3D3D3D3D3D3D3D3D3D3D3D3D3D36868
        68727272414141FFFFFFFFFFFF414141727272686868D3D3D33B3B3B3B3B3BD3
        D3D3D3D3D3D3D3D3D3D3D3D3D3D3686868727272414141FFFFFFFFFFFF454545
        777777777777C3C3C3DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDC3C3C37777
        77777777454545FFFFFFFFFFFF454545777777777777C3C3C3DDDDDDDDDDDDDD
        DDDDDDDDDDDDDDDDDDDDDDC3C3C3777777777777454545FFFFFFFFFFFF4A4A4A
        7D7D7D7D7D7D7D7D7D7D7D7D7D7D7D7D7D7D7D7D7D7D7D7D7D7D7D7D7D7D7D7D
        7D7D7D7D4A4A4AFFFFFFFFFFFF4A4A4A7D7D7D7D7D7D7D7D7D7D7D7D7D7D7D7D
        7D7D7D7D7D7D7D7D7D7D7D7D7D7D7D7D7D7D7D7D4A4A4AFFFFFFFFFFFF4F4F4F
        8282826D6D6D6666666666666666666666666666666666666666666666666D6D
        6D8282824F4F4FFFFFFFFFFFFF4F4F4F8282826D6D6D66666666666666666666
        66666666666666666666666666666D6D6D8282824F4F4FFFFFFFFFFFFF545454
        888888D9CCBFFFEEDDFFEEDDFFEEDDFFEEDDFFEEDDFFEEDDFFEEDDFFEEDDD9CC
        BF888888545454FFFFFFFFFFFF545454888888C9C9C9EBEBEBEBEBEBEBEBEBEB
        EBEBEBEBEBEBEBEBEBEBEBEBEBEBC9C9C9888888545454FFFFFFFFFFFF585858
        8D8D8DFFF1E4FFF1E4FFF1E4FFF1E4FFF1E4FFF1E4FFF1E4FFF1E4FFF1E4FFF1
        E48D8D8D585858FFFFFFFFFFFF5858588D8D8DEFEFEFEFEFEFEFEFEFEFEFEFEF
        EFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEF8D8D8D585858FFFFFFFFFFFF5D5D5D
        929292FFF6EEFFF6EEFFF6EEFFF6EEFFF6EEFFF6EEFFF6EEFFF6EEFFF6EEFFF6
        EE9292925D5D5DFFFFFFFFFFFF5D5D5D929292F5F5F5F5F5F5F5F5F5F5F5F5F5
        F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F5F59292925D5D5DFFFFFFFFFFFF606060
        969696FFFCF8FFFCF8FFFCF8FFFCF8FFFCF8FFFCF8FFFCF8FFFCF8FFFCF8FFFC
        F8969696606060FFFFFFFFFFFF606060969696FBFBFBFBFBFBFBFBFBFBFBFBFB
        FBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFB969696606060FFFFFFFFFFFF646464
        999999FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FF999999646464FFFFFFFFFFFF646464999999FFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF999999646464FFFFFFFFFFFF8C8C8C
        696969FFD0A1FFD6ACFFDCB9FFE2C4FFE6CCFFE6CCFFE2C4FFDCB9FFD6ACFFD0
        A16969698C8C8CFFFFFFFFFFFF8C8C8C696969C7C7C7CDCDCDD5D5D5DCDCDCE1
        E1E1E1E1E1DCDCDCD5D5D5CDCDCDC7C7C76969698C8C8CFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
      NumGlyphs = 2
      TabOrder = 0
      OnClick = btnSalvarClick
    end
    object btnFechar: TBitBtn
      Left = 77
      Top = 1
      Width = 75
      Height = 25
      Caption = 'Fechar'
      Glyph.Data = {
        36060000424D3606000000000000360000002800000020000000100000000100
        18000000000000060000120B0000120B00000000000000000000F9F9F9F1F1F1
        F2F2F2F4F4F3FCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFEFE
        FEEEEEEECECFD0E2E2E3F9F9F9F1F1F1F2F2F2F4F4F4FCFCFCFCFCFCFCFCFCFC
        FCFCFCFCFCFCFCFCFCFCFCFCFCFCFEFEFEEEEEEECFCFCFE2E2E2C9C9C9989898
        9B9B9B9595957E8081807F7F7F80807F80807F80807F8080807F7F7F83847F8A
        9370889A5D8CB298A6B1C9C9C99898989B9B9B9595958080807F7F7F80808080
        80808080808080807F7F7F828282898989868686888888A5A5A5E9E9E9DFDFDF
        E2E1E1C5C6C854657876706B706F6F716F6F716F6F716F70746C66637C91528D
        BC5087B35F9BC995A5B2E9E9E9DFDFDFE1E1E1C6C6C66666666F6F6F6F6F6F6F
        6F6F6F6F6F7070706B6B6B7A7A7A888888838383959595A4A4A4FFFFFFFFFFFF
        FFFFFFF4F4F5596B7F6D66606765646765646765646765656A625B5A768D4C87
        B74C80AA6197C497A5B1FFFFFFFFFFFFFFFFFFF4F4F46C6C6C65656565656565
        65656565656565656161617474748383837C7C7C939393A4A4A4FFFFFFFFFFFF
        E9E9E9CECFD05B697D66625C636160636261636261636162665D565A758B4F8A
        B94D83AF669DCA97A5B1FFFFFFFFFFFFE9E9E9CFCFCF6B6B6B60606061616162
        62626262626262625C5C5C7373738585857F7F7F999999A4A4A4FFFFFFFFFFFF
        7A9E8D3E95785C6178675C595F5F5D605E5D605E5D605E5E62595259748B518D
        BE5084B06B9EC996A5B0FFFFFFFFFFFF8C8C8C6C6C6C6868685D5D5D5E5E5E5E
        5E5E5E5E5E5E5E5E5858587272728989898181819B9B9BA3A3A3B0BBB7649680
        27926701DB9728896A6251525E5A5B5B5B5A5C5B5A5C5C5B5C544E58748A5590
        C24E87B269A1CC97A5B0B6B6B67E7E7E5F5F5F7676765C5C5C5555555B5B5B5B
        5B5B5B5B5B5C5C5C5353537272728C8C8C8181819C9C9CA4A4A47DA2911BE3A8
        00D19900D59E00D0932984625B51525A565758575658585758504A57738A5992
        C24299CC65B2E298A3AE90909087878772727275757571717159595954545457
        57575757575858584F4F4F7171718E8E8E8B8B8BA6A6A6A3A3A3819F903EE6BF
        0ED6AB0BC89D10DCAE1AA676514F4B595154555454555555554C4757738B5B96
        C8409BCE6BAEDA98A3AD9090909B9B9B7D7D7D7474748181816464644E4E4E54
        54545454545555554C4C4C7171719292928B8B8BA5A5A5A3A3A398B3A651C79C
        42B9974CDBBF20AB8142584B574C4F52505052505052515251484357738B5D98
        CA4C97C971B7E498A3ADA6A6A68F8F8F8383839C9C9C6B6B6B4D4D4D50505050
        50505050505252524848487171719494948D8D8DADADADA3A3A3EFF4F2DBEEE3
        5F9E7D2DAD8A3F67695542444F4E4E4E4D4D4E4D4D4E4E4F4C443F55738A5C9C
        D05A91C182B3DD96A3AEF2F2F2E4E4E47E7E7E7373735858584747474E4E4E4D
        4D4D4D4D4D4E4E4E4444447070709797978E8E8EB0B0B0A2A2A2FFFFFFFFFFFF
        C3D9CEA9C6BC595D784C46424B4A4B4B4A4A4B4A4A4B4B4C49413C55728C60A0
        D45994C684B7E197A3ADFFFFFFFFFFFFCECECEB8B8B86666664646464B4B4B4A
        4A4A4A4A4A4B4B4B4141417171719B9B9B909090B3B3B3A2A2A2FFFFFFFFFFFF
        FFFFFFE9EDEC4E5B6F473E374643414643404643404644424438305170885DA0
        D65A97C989BBE597A3ACFFFFFFFFFFFFFFFFFFEBEBEB5E5E5E3D3D3D43434342
        42424242424444443737376D6D6D9B9B9B939393B8B8B8A2A2A2FFFFFFFFFFFF
        FFFFFFE7E7E8445C6F45424144464944464844464845474A3D3A39667F957AB8
        EC5495CB89BBE697A3ACFFFFFFFFFFFFFFFFFFE7E7E75A5A5A42424246464646
        46464646464747473A3A3A7E7E7EB4B4B4919191B8B8B8A2A2A2FFFFFFFFFFFF
        FFFFFFF9F9F9B4C7D6A7C0D3A8BFD3A8BFD3A8BFD3A8BFD3A5BDD1B5C8D7AFBC
        C891C0E796C6EF95A0AAFFFFFFFFFFFFFFFFFFF9F9F9C5C5C5BEBEBEBEBEBEBE
        BEBEBEBEBEBEBEBEBBBBBBC6C6C6BCBCBCBDBDBDC3C3C3A0A0A0FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFC
        FCDEE2E4C1D9ED99A5AFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFCFCE1E1E1D7D7D7A4A4A4}
      NumGlyphs = 2
      TabOrder = 1
      OnClick = btnFecharClick
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 1180
    Height = 577
    Align = alClient
    Color = clWhite
    TabOrder = 1
    object pgcControle: TPageControl
      Left = 1
      Top = 1
      Width = 1178
      Height = 575
      ActivePage = tabPosto
      Align = alClient
      DockSite = True
      HotTrack = True
      TabOrder = 0
      object tabFrenteCaixa: TTabSheet
        Caption = '  Frente de Caixa '
        object Label15: TLabel
          Left = 326
          Top = 405
          Width = 109
          Height = 13
          Caption = 'Caminho Comunica'#231#227'o'
        end
        object Label16: TLabel
          Left = 326
          Top = 443
          Width = 137
          Height = 13
          Caption = 'Comando para abrir a gaveta'
        end
        object Label28: TLabel
          Left = 326
          Top = 482
          Width = 158
          Height = 13
          Caption = 'Comando para acionar a gilhotina'
        end
        object Label29: TLabel
          Left = 14
          Top = 450
          Width = 56
          Height = 13
          Caption = 'Pasta Fotos'
        end
        object Label30: TLabel
          Left = 120
          Top = 450
          Width = 73
          Height = 13
          Alignment = taRightJustify
          Caption = '*Supermercado'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object grpBooleano: TGroupBox
          Left = 521
          Top = 13
          Width = 236
          Height = 407
          TabOrder = 4
          object chkFrenteAbrirGaveta: TCheckBox
            Left = 9
            Top = 337
            Width = 140
            Height = 17
            Hint = 'Abrir Gaveta de dinheiro automaticamente ao finalizar Venda'
            Caption = 'Abrir gaveta autom'#225'tico'
            ParentShowHint = False
            ShowHint = True
            TabOrder = 19
            Visible = False
          end
          object chkFrenteBaixaSeletivaCR: TCheckBox
            Left = 9
            Top = 14
            Width = 137
            Height = 17
            Hint = 'Habilitar Baixa Seletiva do Contas a Receber'
            Caption = 'Baixa seletiva CR'
            TabOrder = 0
          end
          object chkFrenteTruncar: TCheckBox
            Left = 9
            Top = 31
            Width = 97
            Height = 17
            Hint = 'Truncar casas decimais nos itens'
            Caption = 'Truncar valores'
            TabOrder = 1
          end
          object chkFrenteLimiteCredito: TCheckBox
            Left = 9
            Top = 65
            Width = 117
            Height = 17
            Hint = 
              'Habilitar bloqueio de Venda a Prazo caso valor exceda o Limite d' +
              'e cr'#233'dito do cliente'
            Caption = 'Limite de cr'#233'dito'
            ParentShowHint = False
            ShowHint = True
            TabOrder = 3
          end
          object chkFrenteSugerirFPag: TCheckBox
            Left = 9
            Top = 82
            Width = 160
            Height = 17
            Hint = 
              'Define uma Forma Pagamento por padr'#227'o, a qual ira sair seleciona' +
              'da por padr'#227'o no momento de finalizar.'
            Caption = 'Sugerir forma de pagamento'
            ParentShowHint = False
            ShowHint = True
            TabOrder = 4
          end
          object chkImprimirVendedor: TCheckBox
            Left = 9
            Top = 99
            Width = 168
            Height = 17
            Hint = '|Imprimir vendedor nos dados adicionais da NFC-e'
            Caption = 'Imprimir vendedor'
            ParentShowHint = False
            ShowHint = True
            TabOrder = 5
          end
          object chkImprimirSaldoPagar: TCheckBox
            Left = 9
            Top = 354
            Width = 168
            Height = 17
            Hint = 
              'Ao efetuar venda a prazo ira imprimir Saldo do cliente no Compro' +
              'vante de D'#233'bito/Cr'#233'dito'
            Caption = 'Imprimir Saldo devedor'
            ParentShowHint = False
            ShowHint = True
            TabOrder = 20
            Visible = False
          end
          object chkCalcularJuro: TCheckBox
            Left = 9
            Top = 116
            Width = 168
            Height = 17
            Hint = 'Calcular Juro na consulta de Saldo devedor do cliente'
            Caption = 'Calcular Juros consulta Saldo'
            ParentShowHint = False
            ShowHint = True
            TabOrder = 6
          end
          object chkFrenteAlterarValor: TCheckBox
            Left = 9
            Top = 133
            Width = 168
            Height = 17
            Hint = 
              'Permite alterar o valor unit'#225'rio do produto no momento da venda ' +
              '?'
            Caption = 'Habilitar alterar Valor de Venda'
            ParentShowHint = False
            ShowHint = True
            TabOrder = 7
          end
          object chkFrenteExibirContasVencidas: TCheckBox
            Left = 9
            Top = 150
            Width = 187
            Height = 17
            Hint = 
              'Ao informar o Consumidor, caso possua alguma conta vencida ira a' +
              'brir na tela'
            Caption = 'Exibir Contas Vencidas Consumidor'
            ParentShowHint = False
            ShowHint = True
            TabOrder = 8
          end
          object chkDebCred: TCheckBox
            Left = 9
            Top = 167
            Width = 187
            Height = 17
            Hint = 
              'Ao informar o Consumidor, caso possua alguma conta vencida ira a' +
              'brir na tela'
            Caption = 'Imprime Comp. D'#233'bito/Cr'#233'dito'
            ParentShowHint = False
            ShowHint = True
            TabOrder = 9
          end
          object chkPesquisaCliente: TCheckBox
            Left = 9
            Top = 184
            Width = 169
            Height = 17
            Hint = 
              'Para realizar a impress'#227'o '#233' necess'#225'rio estar com o preview desab' +
              'ilitado e a venda ser a prazo.'
            Caption = 'CNPJ/CPF pesquisa Cliente'
            ParentShowHint = False
            ShowHint = True
            TabOrder = 10
          end
          object CHKAcrescimos: TCheckBox
            Left = 9
            Top = 201
            Width = 169
            Height = 17
            Caption = 'Visualizar campo de Acr'#233'scimos'
            ParentShowHint = False
            ShowHint = True
            TabOrder = 11
          end
          object chkPlaca: TCheckBox
            Left = 9
            Top = 218
            Width = 169
            Height = 17
            Caption = 'Visualizar campo Placa'
            ParentShowHint = False
            ShowHint = True
            TabOrder = 12
          end
          object chkKM: TCheckBox
            Left = 9
            Top = 235
            Width = 169
            Height = 17
            Caption = 'Visualizar campo KM'
            ParentShowHint = False
            ShowHint = True
            TabOrder = 13
          end
          object chkCusto: TCheckBox
            Left = 9
            Top = 252
            Width = 168
            Height = 17
            Hint = 
              'Ao efetuar venda a prazo ira imprimir Saldo do cliente no Compro' +
              'vante de D'#233'bito/Cr'#233'dito'
            Caption = 'Mostrar valor de custo'
            ParentShowHint = False
            ShowHint = True
            TabOrder = 14
          end
          object chkFrota: TCheckBox
            Left = 9
            Top = 269
            Width = 212
            Height = 17
            Hint = 'Permite incluir uma placa sem a frota cadastrada'
            Caption = 'Permite vender sem frota cadastrada'
            ParentShowHint = False
            ShowHint = True
            TabOrder = 15
          end
          object chkCertificadoVencendo: TCheckBox
            Left = 9
            Top = 286
            Width = 224
            Height = 17
            Hint = 
              'Se estiver desmarcado essa op'#231#227'o o sistema n'#227'o vai exibir na tel' +
              'a a mensagem do certificado vencendo'
            Caption = 'Exibir mensagem de certificado vencendo'
            ParentShowHint = False
            ShowHint = True
            TabOrder = 16
          end
          object chkItensCompDebito: TCheckBox
            Left = 9
            Top = 303
            Width = 224
            Height = 17
            Hint = 
              'Se imprimir o comprovante de d'#233'bito, vai imprimir os itens do NF' +
              'C-e ou n'#227'o'
            Caption = 'Imprimir itens no comprovante de d'#233'bito'
            ParentShowHint = False
            ShowHint = True
            TabOrder = 17
            Visible = False
          end
          object chkInfVendedor: TCheckBox
            Left = 9
            Top = 320
            Width = 168
            Height = 17
            Hint = 'Ao finalizar sistema pede para informar o vendedor'
            Caption = 'Informar vendedor ao finalizar'
            ParentShowHint = False
            ShowHint = True
            TabOrder = 18
          end
          object chkFrenteLoginAutorizacao: TCheckBox
            Left = 9
            Top = 48
            Width = 120
            Height = 17
            Hint = 
              '|Habilitar solicita'#231#227'o de Autoriza'#231#227'o para determinadas a'#231#245'es no' +
              ' PDV. Somente usu'#225'rios com n'#237'vel X ou inferior a 3 possuem autor' +
              'iza'#231#227'o'#13#10
            Caption = 'Login de autoriza'#231#227'o'
            ParentShowHint = False
            ShowHint = True
            TabOrder = 2
          end
        end
        object rgVinculado: TRadioGroup
          Left = 14
          Top = 7
          Width = 128
          Height = 68
          Hint = '|Tipo de Cupom Vinculado ao usar Forma de Pagamento a Prazo'
          Caption = '| Vinculado |'
          Items.Strings = (
            'S - Imprimir 2 vias '
            'N - Imprimir 1 via')
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
        end
        object grpCasasDecimais: TGroupBox
          Left = 147
          Top = 7
          Width = 180
          Height = 68
          Caption = '| Casas Decimais |'
          TabOrder = 1
          object Label6: TLabel
            Left = 11
            Top = 14
            Width = 55
            Height = 13
            Caption = 'Quantidade'
          end
          object Label7: TLabel
            Left = 95
            Top = 14
            Width = 24
            Height = 13
            Caption = 'Valor'
          end
          object edtFrenteCasaDecQTD: TSpinEdit
            Left = 11
            Top = 28
            Width = 72
            Height = 22
            Hint = 'Casas Decimais na Quantidade do Item do ECF'
            Ctl3D = False
            MaxValue = 3
            MinValue = 0
            ParentCtl3D = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            Value = 0
          end
          object edtFrenteCasaDecVLR: TSpinEdit
            Left = 95
            Top = 28
            Width = 72
            Height = 22
            Hint = 'Casas Decimais no Valor Unit'#225'rio do Item do ECF'
            Ctl3D = False
            MaxValue = 3
            MinValue = 0
            ParentCtl3D = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            Value = 0
          end
        end
        object GroupBox1: TGroupBox
          Left = 14
          Top = 81
          Width = 501
          Height = 311
          TabOrder = 3
          object Label1: TLabel
            Left = 87
            Top = 13
            Width = 59
            Height = 13
            Caption = 'Qtd. M'#225'xima'
          end
          object Label2: TLabel
            Left = 164
            Top = 13
            Width = 63
            Height = 13
            Caption = 'Valor M'#225'ximo'
          end
          object Label3: TLabel
            Left = 10
            Top = 13
            Width = 50
            Height = 13
            Caption = 'Qtd. Inicial'
          end
          object Label4: TLabel
            Left = 10
            Top = 48
            Width = 86
            Height = 13
            Caption = 'Forma Pagamento'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlue
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsUnderline]
            ParentFont = False
            OnClick = Label4Click
          end
          object Label5: TLabel
            Left = 10
            Top = 230
            Width = 46
            Height = 13
            Caption = 'Database'
          end
          object btnDatabase: TSpeedButton
            Left = 464
            Top = 243
            Width = 23
            Height = 19
            Caption = '...'
            NumGlyphs = 2
            OnClick = btnDatabaseClick
          end
          object Label13: TLabel
            Left = 10
            Top = 85
            Width = 47
            Height = 13
            Caption = 'Opera'#231#227'o'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlue
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsUnderline]
            ParentFont = False
            OnClick = Label13Click
          end
          object Label18: TLabel
            Left = 10
            Top = 122
            Width = 76
            Height = 13
            Caption = 'Centro de Custo'
          end
          object Label19: TLabel
            Left = 10
            Top = 162
            Width = 136
            Height = 13
            Caption = 'Mensagem Simples Nacional'
          end
          object Label49: TLabel
            Left = 10
            Top = 196
            Width = 129
            Height = 13
            Caption = 'Mensagem Rodap'#233' Cupom'
          end
          object Label14: TLabel
            Left = 10
            Top = 265
            Width = 86
            Height = 13
            Caption = 'Database Remota'
          end
          object btnDatabaseRemota: TSpeedButton
            Left = 464
            Top = 278
            Width = 23
            Height = 19
            Caption = '...'
            NumGlyphs = 2
            OnClick = btnDatabaseRemotaClick
          end
          object edtFrenteQtdMaxima: TEdit
            Left = 87
            Top = 27
            Width = 67
            Height = 19
            Hint = 'Quantidade m'#225'xima por item a ser vendido no PDV'
            Ctl3D = False
            ParentCtl3D = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
          end
          object edtFrenteQtdInicial: TEdit
            Left = 10
            Top = 27
            Width = 67
            Height = 19
            Hint = 
              'Quantidade inicial padr'#227'o de itens no momento da venda (Posto=0 ' +
              '  -   Loja=1)'
            Ctl3D = False
            ParentCtl3D = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            Text = '1'
          end
          object edtFrenteFormaPag: TEdit
            Left = 10
            Top = 63
            Width = 78
            Height = 19
            Hint = 'Forma de Pagamento padr'#227'o ao Finalizar cupom fiscal'
            Ctl3D = False
            ParentCtl3D = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 3
            OnExit = edtFrenteFormaPagExit
            OnKeyPress = edtFrenteFormaPagKeyPress
          end
          object edtFrenteDatabase: TEdit
            Left = 10
            Top = 243
            Width = 452
            Height = 19
            Hint = 'Caminho da base de dados do sistema'
            Ctl3D = False
            ParentCtl3D = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 8
          end
          object edtFrenteOperacao: TEdit
            Left = 10
            Top = 100
            Width = 78
            Height = 19
            Hint = 'Opera'#231#227'o padr'#227'o ao Finalizar cupom fiscal'
            Ctl3D = False
            ParentCtl3D = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 4
            OnExit = edtFrenteOperacaoExit
            OnKeyPress = edtFrenteFormaPagKeyPress
          end
          object edtFrenteMsgSimplesNac: TEdit
            Left = 10
            Top = 176
            Width = 452
            Height = 19
            Hint = 
              'Mensagem obrigat'#243'ria da legisla'#231#227'o para empresas no regime tribu' +
              't'#225'rio do Simples Nacional'
            Ctl3D = False
            ParentCtl3D = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 6
          end
          object edtFrenteMensagemCupom: TEdit
            Left = 10
            Top = 210
            Width = 452
            Height = 19
            Hint = 'Mensagem a ser impressa no rodap'#233' do cupom fiscal'
            Ctl3D = False
            ParentCtl3D = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 7
          end
          object edtFrenteVlrMaximo: TEdit
            Left = 164
            Top = 27
            Width = 67
            Height = 19
            Hint = 'Valor m'#225'ximo por item a ser vendido no PDV'
            Ctl3D = False
            ParentCtl3D = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 2
          end
          object edtFrenteDatabaseRemoto: TEdit
            Left = 10
            Top = 278
            Width = 452
            Height = 19
            Hint = 'Caminho da base de dados remota do sistema'
            Ctl3D = False
            ParentCtl3D = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 9
          end
          object edtFPagDesc: TEdit
            Left = 88
            Top = 63
            Width = 374
            Height = 19
            Hint = 
              'Mensagem obrigat'#243'ria da legisla'#231#227'o para empresas no regime tribu' +
              't'#225'rio do Simples Nacional'
            Ctl3D = False
            Enabled = False
            ParentCtl3D = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 10
          end
          object edtOpeDesc: TEdit
            Left = 88
            Top = 100
            Width = 374
            Height = 19
            Hint = 
              'Mensagem obrigat'#243'ria da legisla'#231#227'o para empresas no regime tribu' +
              't'#225'rio do Simples Nacional'
            Ctl3D = False
            Enabled = False
            ParentCtl3D = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 11
          end
          object lcmbCCustos: TDBLookupComboBox
            Left = 10
            Top = 135
            Width = 452
            Height = 21
            KeyField = 'ID'
            ListField = 'DESCRICAO'
            ListSource = dmCupomFiscal.dsCcusto
            TabOrder = 5
          end
          object GroupBox9: TGroupBox
            Left = 247
            Top = 10
            Width = 215
            Height = 37
            Caption = '| Pesquisa |'
            TabOrder = 12
            object CheckBox1: TCheckBox
              Left = 17
              Top = 17
              Width = 97
              Height = 17
              Caption = 'Inicie Por'
              TabOrder = 0
            end
          end
        end
        object rgFrenteAtividade: TRadioGroup
          Left = 332
          Top = 7
          Width = 183
          Height = 68
          Caption = '| Atividade |'
          Items.Strings = (
            'Lojas em Geral'
            'Supermercado'
            'Posto de Combust'#237'vel')
          TabOrder = 2
        end
        object rgFocoFim: TRadioGroup
          Left = 14
          Top = 398
          Width = 300
          Height = 42
          Caption = '| Foco ao Finalizar |'
          Columns = 3
          ItemIndex = 0
          Items.Strings = (
            'Desconto'
            'Acr'#233'scimos'
            'Forma Pag.')
          TabOrder = 5
          OnClick = rgFocoFimClick
        end
        object edtGavCaminho: TEdit
          Left = 326
          Top = 420
          Width = 175
          Height = 21
          ParentShowHint = False
          ShowHint = True
          TabOrder = 6
        end
        object edtGavComando: TEdit
          Left = 326
          Top = 458
          Width = 175
          Height = 21
          Hint = 'Adicione o comando para abrir a gaveta na impressora'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 7
        end
        object btnTestar: TBitBtn
          Left = 506
          Top = 458
          Width = 41
          Height = 22
          Glyph.Data = {
            36060000424D3606000000000000360000002800000020000000100000000100
            18000000000000060000120B0000120B00000000000000000000FFFFFF10945F
            10945F10945F10945F10945F10945F10945F10945F10945F10945F10945F1094
            5F10945F10945FFFFFFFFFFFFF55555555555555555555555555555555555555
            5555555555555555555555555555555555555555555555FFFFFF10945F10945F
            10945F10945F10945F10945F10945F10945F10945F10945F10945F10945F1094
            5F10945F10945F10945F55555555555555555555555555555555555555555555
            555555555555555555555555555555555555555555555555555510945F10945F
            10945F10945F10945F10945F10945F10945F10945F10945F10945F10945F1094
            5F10945F10945F10945F55555555555555555555555555555555555555555555
            555555555555555555555555555555555555555555555555555510945F10945F
            10945F10945F10945F10945F10945F10945F10945F10945F10945F10945F1094
            5F10945F10945F10945F55555555555555555555555555555555555555555555
            555555555555555555555555555555555555555555555555555510945F10945F
            10945F10945F10945FFFFFFFFFFFFFFFFFFF10945F10945F10945F10945F1094
            5F10945F10945F10945F555555555555555555555555555555FFFFFFFFFFFFFF
            FFFF55555555555555555555555555555555555555555555555510945F10945F
            10945F10945FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF10945F10945F10945F1094
            5F10945F10945F10945F555555555555555555555555FFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFF55555555555555555555555555555555555555555510945F10945F
            10945FFFFFFFFFFFFFFFFFFF10945FFFFFFFFFFFFFFFFFFF10945F10945F1094
            5F10945F10945F10945F555555555555555555FFFFFFFFFFFFFFFFFF555555FF
            FFFFFFFFFFFFFFFF55555555555555555555555555555555555510945F10945F
            10945F10945FFFFFFF10945F10945F10945FFFFFFFFFFFFFFFFFFF10945F1094
            5F10945F10945F10945F555555555555555555555555FFFFFF55555555555555
            5555FFFFFFFFFFFFFFFFFF55555555555555555555555555555510945F10945F
            10945F10945F10945F10945F10945F10945F10945FFFFFFFFFFFFFFFFFFF1094
            5F10945F10945F10945F55555555555555555555555555555555555555555555
            5555555555FFFFFFFFFFFFFFFFFF55555555555555555555555510945F10945F
            10945F10945F10945F10945F10945F10945F10945F10945FFFFFFFFFFFFFFFFF
            FF10945F10945F10945F55555555555555555555555555555555555555555555
            5555555555555555FFFFFFFFFFFFFFFFFF55555555555555555510945F10945F
            10945F10945F10945F10945F10945F10945F10945F10945F10945FFFFFFF1094
            5F10945F10945F10945F55555555555555555555555555555555555555555555
            5555555555555555555555FFFFFF55555555555555555555555510945F10945F
            10945F10945F10945F10945F10945F10945F10945F10945F10945F10945F1094
            5F10945F10945F10945F55555555555555555555555555555555555555555555
            555555555555555555555555555555555555555555555555555510945F10945F
            10945F10945F10945F10945F10945F10945F10945F10945F10945F10945F1094
            5F10945F10945F10945F55555555555555555555555555555555555555555555
            555555555555555555555555555555555555555555555555555510945F10945F
            10945F10945F10945F10945F10945F10945F10945F10945F10945F10945F1094
            5F10945F10945F10945F55555555555555555555555555555555555555555555
            555555555555555555555555555555555555555555555555555510945F10945F
            10945F10945F10945F10945F10945F10945F10945F10945F10945F10945F1094
            5F10945F10945F10945F55555555555555555555555555555555555555555555
            5555555555555555555555555555555555555555555555555555FFFFFF10945F
            10945F10945F10945F10945F10945F10945F10945F10945F10945F10945F1094
            5F10945F10945FFFFFFFFFFFFF55555555555555555555555555555555555555
            5555555555555555555555555555555555555555555555FFFFFF}
          NumGlyphs = 2
          TabOrder = 9
          OnClick = btnTestarClick
        end
        object edtGuilhoComando: TEdit
          Left = 326
          Top = 497
          Width = 175
          Height = 21
          Hint = 'Adicione o comando para abrir a gaveta na impressora'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 8
        end
        object BitBtn1: TBitBtn
          Left = 506
          Top = 497
          Width = 41
          Height = 22
          Glyph.Data = {
            36060000424D3606000000000000360000002800000020000000100000000100
            18000000000000060000120B0000120B00000000000000000000FFFFFF10945F
            10945F10945F10945F10945F10945F10945F10945F10945F10945F10945F1094
            5F10945F10945FFFFFFFFFFFFF55555555555555555555555555555555555555
            5555555555555555555555555555555555555555555555FFFFFF10945F10945F
            10945F10945F10945F10945F10945F10945F10945F10945F10945F10945F1094
            5F10945F10945F10945F55555555555555555555555555555555555555555555
            555555555555555555555555555555555555555555555555555510945F10945F
            10945F10945F10945F10945F10945F10945F10945F10945F10945F10945F1094
            5F10945F10945F10945F55555555555555555555555555555555555555555555
            555555555555555555555555555555555555555555555555555510945F10945F
            10945F10945F10945F10945F10945F10945F10945F10945F10945F10945F1094
            5F10945F10945F10945F55555555555555555555555555555555555555555555
            555555555555555555555555555555555555555555555555555510945F10945F
            10945F10945F10945FFFFFFFFFFFFFFFFFFF10945F10945F10945F10945F1094
            5F10945F10945F10945F555555555555555555555555555555FFFFFFFFFFFFFF
            FFFF55555555555555555555555555555555555555555555555510945F10945F
            10945F10945FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF10945F10945F10945F1094
            5F10945F10945F10945F555555555555555555555555FFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFF55555555555555555555555555555555555555555510945F10945F
            10945FFFFFFFFFFFFFFFFFFF10945FFFFFFFFFFFFFFFFFFF10945F10945F1094
            5F10945F10945F10945F555555555555555555FFFFFFFFFFFFFFFFFF555555FF
            FFFFFFFFFFFFFFFF55555555555555555555555555555555555510945F10945F
            10945F10945FFFFFFF10945F10945F10945FFFFFFFFFFFFFFFFFFF10945F1094
            5F10945F10945F10945F555555555555555555555555FFFFFF55555555555555
            5555FFFFFFFFFFFFFFFFFF55555555555555555555555555555510945F10945F
            10945F10945F10945F10945F10945F10945F10945FFFFFFFFFFFFFFFFFFF1094
            5F10945F10945F10945F55555555555555555555555555555555555555555555
            5555555555FFFFFFFFFFFFFFFFFF55555555555555555555555510945F10945F
            10945F10945F10945F10945F10945F10945F10945F10945FFFFFFFFFFFFFFFFF
            FF10945F10945F10945F55555555555555555555555555555555555555555555
            5555555555555555FFFFFFFFFFFFFFFFFF55555555555555555510945F10945F
            10945F10945F10945F10945F10945F10945F10945F10945F10945FFFFFFF1094
            5F10945F10945F10945F55555555555555555555555555555555555555555555
            5555555555555555555555FFFFFF55555555555555555555555510945F10945F
            10945F10945F10945F10945F10945F10945F10945F10945F10945F10945F1094
            5F10945F10945F10945F55555555555555555555555555555555555555555555
            555555555555555555555555555555555555555555555555555510945F10945F
            10945F10945F10945F10945F10945F10945F10945F10945F10945F10945F1094
            5F10945F10945F10945F55555555555555555555555555555555555555555555
            555555555555555555555555555555555555555555555555555510945F10945F
            10945F10945F10945F10945F10945F10945F10945F10945F10945F10945F1094
            5F10945F10945F10945F55555555555555555555555555555555555555555555
            555555555555555555555555555555555555555555555555555510945F10945F
            10945F10945F10945F10945F10945F10945F10945F10945F10945F10945F1094
            5F10945F10945F10945F55555555555555555555555555555555555555555555
            5555555555555555555555555555555555555555555555555555FFFFFF10945F
            10945F10945F10945F10945F10945F10945F10945F10945F10945F10945F1094
            5F10945F10945FFFFFFFFFFFFF55555555555555555555555555555555555555
            5555555555555555555555555555555555555555555555FFFFFF}
          NumGlyphs = 2
          TabOrder = 10
          OnClick = btnTestarClick
        end
        object ComboBox1: TComboBox
          Left = 14
          Top = 464
          Width = 181
          Height = 21
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 11
          Text = 'ComboBox1'
        end
      end
      object tabPosto: TTabSheet
        Caption = '  Posto  '
        ImageIndex = 1
        object GroupBox2: TGroupBox
          Left = 10
          Top = 8
          Width = 498
          Height = 165
          TabOrder = 0
          object chkPostoAbastecidasDel: TCheckBox
            Left = 8
            Top = 13
            Width = 121
            Height = 17
            Hint = 'Deletar Abastecida ao Finalizar o cupom fiscal'
            Caption = 'Deletar Abastecidas'
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
          end
          object chkPostoAbastecidasExibir: TCheckBox
            Left = 8
            Top = 30
            Width = 179
            Height = 17
            Hint = 
              'Exibe todas as Abastecidas, inclusive as que ja foram emitido cu' +
              'pom fiscal (Somente para quem possui usar  F7-Tela de abastecida' +
              's separada)'
            Caption = 'Exibir todas Abastecidas'
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
          end
          object chkPostoVenderSemAbastecidas: TCheckBox
            Left = 8
            Top = 48
            Width = 153
            Height = 17
            Hint = 
              'Permite vender produtos do Subtipo "Combust'#237'vel" sem a exist'#234'nci' +
              'a de abastecidas'
            Caption = 'Vender sem abastecidas'
            ParentShowHint = False
            ShowHint = True
            TabOrder = 2
          end
          object chkPostoImprimirMedia: TCheckBox
            Left = 8
            Top = 100
            Width = 92
            Height = 17
            Hint = 'Exibir campo Bico na tela do PDV'
            Caption = 'Imprimir M'#233'dia'
            ParentShowHint = False
            ShowHint = True
            TabOrder = 5
            Visible = False
          end
          object GroupBox15: TGroupBox
            Left = 221
            Top = 13
            Width = 261
            Height = 66
            Caption = '| Op'#231#245'es tela de Venda |'
            TabOrder = 6
            object chkPostoAbastecidasExibirVenda: TCheckBox
              Left = 16
              Top = 19
              Width = 145
              Height = 17
              Hint = 'Exibe abastecidas pedente cupom fiscal na tela de venda'
              Caption = 'Exibir abastecidas Venda'
              ParentShowHint = False
              ShowHint = True
              TabOrder = 0
              OnClick = chkPostoAbastecidasExibirVendaClick
            end
            object chkPostoAbastecidasEventosVenda: TCheckBox
              Left = 16
              Top = 38
              Width = 217
              Height = 17
              Hint = 
                'Cada evento que houver na tabela das abastecidas sistema ira ree' +
                'ler na consulta da tela'
              Caption = 'Atualizar abastecidas via Eventos'
              ParentShowHint = False
              ShowHint = True
              TabOrder = 1
              OnClick = chkPostoAbastecidasEventosVendaClick
            end
          end
          object chkPostoParcelasVenda: TCheckBox
            Left = 8
            Top = 117
            Width = 241
            Height = 17
            Hint = 'Exibir campo Bico na tela do PDV'
            Caption = 'Gera parcelas manualmente venda '#224' prazo'
            ParentShowHint = False
            ShowHint = True
            TabOrder = 7
            Visible = False
          end
          object chkPostoAlterarQuantidade: TCheckBox
            Left = 8
            Top = 82
            Width = 204
            Height = 17
            Hint = 'Permitir alterar a quantidade de uma abastecida'
            Caption = 'Permite alterar quantidade Abastecida'
            ParentShowHint = False
            ShowHint = True
            TabOrder = 4
          end
          object chkPostoAutorizacaoSemAbastecidas: TCheckBox
            Left = 8
            Top = 65
            Width = 223
            Height = 17
            Hint = 
              'Solicita autoriza'#231#227'o para venda de produtos do Subtipo "Combust'#237 +
              'vel" sem a exist'#234'ncia de abastecidas'
            Caption = 'Autoriza'#231#227'o para venda sem abastecidas'
            ParentShowHint = False
            ShowHint = True
            TabOrder = 3
          end
          object chkFrenteLoginAutorizacaoAbast: TCheckBox
            Left = 9
            Top = 134
            Width = 183
            Height = 17
            Hint = '|Habilitar solicita'#231#227'o de Autoriza'#231#227'o para Abastecidas.'#13#10
            Caption = 'Login de autoriza'#231#227'o Abastecidas'
            ParentShowHint = False
            ShowHint = True
            TabOrder = 8
          end
        end
      end
      object tabLeitor: TTabSheet
        Caption = '  Leitor '
        ImageIndex = 3
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object grpLeitorConfig: TGroupBox
          Left = 13
          Top = 14
          Width = 502
          Height = 133
          TabOrder = 0
          DesignSize = (
            502
            133)
          object Label11: TLabel
            Left = 13
            Top = 83
            Width = 57
            Height = 13
            Caption = 'HandShake'
          end
          object Label48: TLabel
            Left = 227
            Top = 83
            Width = 22
            Height = 13
            Caption = 'Stop'
          end
          object Label53: TLabel
            Left = 123
            Top = 83
            Width = 26
            Height = 13
            Caption = 'Parity'
          end
          object Label39: TLabel
            Left = 13
            Top = 45
            Width = 54
            Height = 13
            Caption = 'Porta Serial'
          end
          object Label40: TLabel
            Left = 123
            Top = 45
            Width = 25
            Height = 13
            Caption = 'Baud'
          end
          object Label10: TLabel
            Left = 228
            Top = 45
            Width = 23
            Height = 13
            Anchors = [akTop, akRight]
            Caption = 'Data'
          end
          object Label41: TLabel
            Left = 298
            Top = 45
            Width = 29
            Height = 13
            Anchors = [akTop, akRight]
            Caption = 'Sufixo'
          end
          object chkLeitorUsarFila: TCheckBox
            Left = 298
            Top = 91
            Width = 87
            Height = 17
            Caption = 'Usar Fila'
            TabOrder = 7
          end
          object chkLeitorExcluirSufixo: TCheckBox
            Left = 298
            Top = 107
            Width = 96
            Height = 17
            Caption = 'Excluir Sufixo'
            TabOrder = 8
          end
          object cbbLeitorHandShake: TComboBox
            Left = 13
            Top = 98
            Width = 104
            Height = 21
            Ctl3D = False
            ParentCtl3D = False
            TabOrder = 4
            Items.Strings = (
              'hsNenhum'
              'hsXON_XOFF'
              'hsRTS_CTS'
              'hsDTR_DSR')
          end
          object cbbLeitorStop: TComboBox
            Left = 227
            Top = 98
            Width = 65
            Height = 21
            Ctl3D = False
            ParentCtl3D = False
            TabOrder = 6
            Items.Strings = (
              's1'
              's1eMeio'
              's2')
          end
          object cbbLeitorParity: TComboBox
            Left = 123
            Top = 97
            Width = 98
            Height = 21
            TabOrder = 5
            Items.Strings = (
              'pNone'
              'pOdd'
              'pEven'
              'pMark'
              'pSpace')
          end
          object cbbLeitorPorta: TComboBox
            Left = 13
            Top = 59
            Width = 105
            Height = 21
            Hint = 'Porta Serial de comunica'#231#227'o com Leitor'
            Ctl3D = False
            ParentCtl3D = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            Items.Strings = (
              'COM1'
              'COM2'
              'COM3'
              'COM4'
              'COM5'
              'LPT1'
              'LPT2'
              'LPT3')
          end
          object cbbLeitorBaud: TComboBox
            Left = 123
            Top = 59
            Width = 98
            Height = 21
            Hint = 'Velocidade da porta'
            Ctl3D = False
            ParentCtl3D = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            Items.Strings = (
              '110'
              '300'
              '600'
              '1200'
              '2400'
              '4800'
              '9600'
              '14400'
              '19200'
              '38400'
              '56000'
              '57600'
              '115200')
          end
          object edtLeitorSufixo: TEdit
            Left = 298
            Top = 59
            Width = 80
            Height = 19
            Cursor = crIBeam
            Anchors = [akTop, akRight]
            Ctl3D = False
            ParentCtl3D = False
            TabOrder = 3
          end
          object chbSoft: TCheckBox
            Left = 395
            Top = 54
            Width = 67
            Height = 20
            Caption = 'SoftFlow'
            Checked = True
            State = cbChecked
            TabOrder = 9
            Visible = False
          end
          object chbHard: TCheckBox
            Left = 395
            Top = 74
            Width = 68
            Height = 17
            Caption = 'HardFlow'
            Checked = True
            State = cbChecked
            TabOrder = 10
            Visible = False
          end
          object cbbLeitorData: TComboBox
            Left = 227
            Top = 59
            Width = 65
            Height = 21
            Ctl3D = False
            ParentCtl3D = False
            TabOrder = 2
            Items.Strings = (
              '5'
              '6'
              '7'
              '8')
          end
          object chkLeitor: TCheckBox
            Left = 13
            Top = 21
            Width = 110
            Height = 17
            Hint = 'Habilitar o uso de leitor Serial'
            Caption = 'Ativar Leitor Serial'
            ParentShowHint = False
            ShowHint = True
            TabOrder = 11
          end
        end
        object GroupBox13: TGroupBox
          Left = 13
          Top = 152
          Width = 502
          Height = 113
          Caption = '| Testar Leitura |'
          TabOrder = 1
          object Label58: TLabel
            Left = 12
            Top = 22
            Width = 113
            Height = 13
            Caption = #218'ltimo C'#243'digo de Barras'
          end
          object Label57: TLabel
            Left = 8
            Top = 96
            Width = 322
            Height = 13
            Caption = 
              'Ap'#243's clicar em Ativar, efetuar a leitura do c'#243'digo de barras no ' +
              'Leitor.'
          end
          object sttCodigoBarras: TStaticText
            Left = 12
            Top = 38
            Width = 233
            Height = 24
            AutoSize = False
            BevelKind = bkTile
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -16
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 0
          end
          object btnLeitorDesativar: TBitBtn
            Left = 275
            Top = 52
            Width = 105
            Height = 25
            Caption = 'Desativar'
            Glyph.Data = {
              36060000424D3606000000000000360000002800000020000000100000000100
              18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFF1313F20000F10000F10000F10000EF0000EF0000ED1212EEFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6C6C6C60606060606060
              60606060606060605F5F5F6A6A6AFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFF1313F61A20F53C4CF93A49F83847F83545F83443F73242F7141BF11717
              EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6E6E6E7373738B8B8B89898988
              88888686868585858484846E6E6E6D6D6DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              1313F81D23F94453FA2429F91212F70F0FF60C0CF50909F5161BF53343F7141B
              F11717EFFFFFFFFFFFFFFFFFFFFFFFFF6F6F6F7676769090907A7A7A6E6E6E6B
              6B6B6969696767677070708585856E6E6E6D6D6DFFFFFFFFFFFFFFFFFF1313F9
              1F25FA4A58FB4247FBC9C9FD3B3BF91313F71010F63333F7C5C5FD3035F73444
              F7141BF21717EFFFFFFFFFFFFF7070707878789494948D8D8DDEDEDE8787876E
              6E6E6C6C6C818181DBDBDB8181818585856E6E6E6D6D6DFFFFFFFFFFFF0000FB
              4F5DFD3237FBCBCBFEF2F2FFEBEBFE3B3BF93939F8EAEAFEF1F1FEC5C5FD181D
              F63343F70000EFFFFFFFFFFFFF646464979797838383DFDFDFF7F7F7F3F3F387
              8787858585F2F2F2F6F6F6DBDBDB727272858585606060FFFFFFFFFFFF0000FD
              525FFD2828FC4747FCECECFFF2F2FFECECFFECECFEF1F1FFEAEAFE3434F70B0B
              F53545F80000EFFFFFFFFFFFFF6565659999997D7D7D8F8F8FF4F4F4F7F7F7F4
              F4F4F3F3F3F7F7F7F2F2F2828282696969868686606060FFFFFFFFFFFF0000FD
              5562FE2C2CFD2929FC4848FCEDEDFFF2F2FFF2F2FFECECFE3A3AF91212F70F0F
              F63848F80000F1FFFFFFFFFFFF6565659B9B9B8080807D7D7D909090F4F4F4F7
              F7F7F7F7F7F3F3F38686866E6E6E6B6B6B888888606060FFFFFFFFFFFF0000FD
              5764FE3030FD2D2DFD4B4BFCEDEDFFF2F2FFF2F2FFECECFF3D3DF91616F81313
              F73C4BF80000F1FFFFFFFFFFFF6565659C9C9C828282808080929292F4F4F4F7
              F7F7F7F7F7F4F4F48888887070706E6E6E8A8A8A606060FFFFFFFFFFFF0000FF
              5A67FE3333FE5050FDEDEDFFF3F3FFEDEDFFEDEDFFF2F2FFECECFE3E3EFA1717
              F83F4EF90000F1FFFFFFFFFFFF6666669E9E9E848484959595F4F4F4F8F8F8F4
              F4F4F4F4F4F7F7F7F3F3F38989897171718C8C8C606060FFFFFFFFFFFF0000FF
              5B68FF4347FECFCFFFF3F3FFEDEDFF4C4CFC4A4AFCECECFFF2F2FFCACAFE2A2F
              FA4251FA0000F3FFFFFFFFFFFF6666669F9F9F8F8F8FE2E2E2F8F8F8F4F4F492
              9292919191F4F4F4F7F7F7DFDFDF7E7E7E8F8F8F616161FFFFFFFFFFFF1414FF
              262BFF5D6AFF585BFFCFCFFF5252FE2F2FFD2C2CFD4B4BFCCCCCFE484CFB4957
              FB1D23F91414F6FFFFFFFFFFFF7272727E7E7EA0A0A09B9B9BE2E2E297979781
              8181808080929292E0E0E09090909393937676766E6E6EFFFFFFFFFFFFFFFFFF
              1414FF262BFF5D6AFF4347FF3434FE3232FE3030FD2D2DFD383CFC4F5DFC1F25
              FA1414F8FFFFFFFFFFFFFFFFFFFFFFFF7272727E7E7EA0A0A08F8F8F85858584
              84848282828080808787879797977878786F6F6FFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFF1414FF262BFF5C69FF5B68FF5A67FE5865FE5663FE5461FE2227FC0D0D
              FBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7272727E7E7EA0A0A09F9F9F9E
              9E9E9D9D9D9C9C9C9B9B9B7A7A7A6C6C6CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFF1313FF0000FF0000FF0000FF0000FD0000FD0000FD1313FDFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF71717166666666666666
              6666656565656565656565707070FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
            NumGlyphs = 2
            TabOrder = 2
            OnClick = btnLeitorDesativarClick
          end
          object btnLeitorAtivar: TBitBtn
            Left = 275
            Top = 23
            Width = 105
            Height = 25
            Caption = 'Ativar'
            Glyph.Data = {
              36060000424D3606000000000000360000002800000020000000100000000100
              18000000000000060000120B0000120B00000000000000000000FFFFFF10945F
              10945F10945F10945F10945F10945F10945F10945F10945F10945F10945F1094
              5F10945F10945FFFFFFFFFFFFF55555555555555555555555555555555555555
              5555555555555555555555555555555555555555555555FFFFFF10945F10945F
              10945F10945F10945F10945F10945F10945F10945F10945F10945F10945F1094
              5F10945F10945F10945F55555555555555555555555555555555555555555555
              555555555555555555555555555555555555555555555555555510945F10945F
              10945F10945F10945F10945F10945F10945F10945F10945F10945F10945F1094
              5F10945F10945F10945F55555555555555555555555555555555555555555555
              555555555555555555555555555555555555555555555555555510945F10945F
              10945F10945F10945F10945F10945F10945F10945F10945F10945F10945F1094
              5F10945F10945F10945F55555555555555555555555555555555555555555555
              555555555555555555555555555555555555555555555555555510945F10945F
              10945F10945F10945FFFFFFFFFFFFFFFFFFF10945F10945F10945F10945F1094
              5F10945F10945F10945F555555555555555555555555555555FFFFFFFFFFFFFF
              FFFF55555555555555555555555555555555555555555555555510945F10945F
              10945F10945FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF10945F10945F10945F1094
              5F10945F10945F10945F555555555555555555555555FFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFF55555555555555555555555555555555555555555510945F10945F
              10945FFFFFFFFFFFFFFFFFFF10945FFFFFFFFFFFFFFFFFFF10945F10945F1094
              5F10945F10945F10945F555555555555555555FFFFFFFFFFFFFFFFFF555555FF
              FFFFFFFFFFFFFFFF55555555555555555555555555555555555510945F10945F
              10945F10945FFFFFFF10945F10945F10945FFFFFFFFFFFFFFFFFFF10945F1094
              5F10945F10945F10945F555555555555555555555555FFFFFF55555555555555
              5555FFFFFFFFFFFFFFFFFF55555555555555555555555555555510945F10945F
              10945F10945F10945F10945F10945F10945F10945FFFFFFFFFFFFFFFFFFF1094
              5F10945F10945F10945F55555555555555555555555555555555555555555555
              5555555555FFFFFFFFFFFFFFFFFF55555555555555555555555510945F10945F
              10945F10945F10945F10945F10945F10945F10945F10945FFFFFFFFFFFFFFFFF
              FF10945F10945F10945F55555555555555555555555555555555555555555555
              5555555555555555FFFFFFFFFFFFFFFFFF55555555555555555510945F10945F
              10945F10945F10945F10945F10945F10945F10945F10945F10945FFFFFFF1094
              5F10945F10945F10945F55555555555555555555555555555555555555555555
              5555555555555555555555FFFFFF55555555555555555555555510945F10945F
              10945F10945F10945F10945F10945F10945F10945F10945F10945F10945F1094
              5F10945F10945F10945F55555555555555555555555555555555555555555555
              555555555555555555555555555555555555555555555555555510945F10945F
              10945F10945F10945F10945F10945F10945F10945F10945F10945F10945F1094
              5F10945F10945F10945F55555555555555555555555555555555555555555555
              555555555555555555555555555555555555555555555555555510945F10945F
              10945F10945F10945F10945F10945F10945F10945F10945F10945F10945F1094
              5F10945F10945F10945F55555555555555555555555555555555555555555555
              555555555555555555555555555555555555555555555555555510945F10945F
              10945F10945F10945F10945F10945F10945F10945F10945F10945F10945F1094
              5F10945F10945F10945F55555555555555555555555555555555555555555555
              5555555555555555555555555555555555555555555555555555FFFFFF10945F
              10945F10945F10945F10945F10945F10945F10945F10945F10945F10945F1094
              5F10945F10945FFFFFFFFFFFFF55555555555555555555555555555555555555
              5555555555555555555555555555555555555555555555FFFFFF}
            NumGlyphs = 2
            TabOrder = 1
            OnClick = btnLeitorAtivarClick
          end
        end
      end
      object tabBalCheckout: TTabSheet
        Caption = '  Balan'#231'a Checkout '
        ImageIndex = 4
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object grpBalancaConfig: TGroupBox
          Left = 13
          Top = 17
          Width = 587
          Height = 105
          TabOrder = 0
          object Label52: TLabel
            Left = 12
            Top = 56
            Width = 26
            Height = 13
            Caption = 'Parity'
          end
          object Label54: TLabel
            Left = 270
            Top = 56
            Width = 63
            Height = 13
            Caption = 'Handshaking'
          end
          object Label55: TLabel
            Left = 161
            Top = 56
            Width = 42
            Height = 13
            Caption = 'Stop Bits'
          end
          object Label56: TLabel
            Left = 375
            Top = 56
            Width = 40
            Height = 13
            Caption = 'TimeOut'
          end
          object Label42: TLabel
            Left = 12
            Top = 18
            Width = 35
            Height = 13
            Caption = 'Modelo'
          end
          object Label43: TLabel
            Left = 161
            Top = 18
            Width = 54
            Height = 13
            Caption = 'Porta Serial'
          end
          object Label44: TLabel
            Left = 270
            Top = 18
            Width = 46
            Height = 13
            Caption = 'Baud rate'
          end
          object Label45: TLabel
            Left = 375
            Top = 18
            Width = 43
            Height = 13
            Caption = 'Data Bits'
          end
          object cbbBalancaHandShaking: TComboBox
            Left = 270
            Top = 70
            Width = 99
            Height = 21
            Ctl3D = False
            ParentCtl3D = False
            TabOrder = 6
            Items.Strings = (
              'Nenhum'
              'XON/XOFF'
              'RTS/CTS'
              'DTR/DSR')
          end
          object cbbBalancaParity: TComboBox
            Left = 12
            Top = 70
            Width = 145
            Height = 21
            Ctl3D = False
            ItemIndex = 0
            ParentCtl3D = False
            TabOrder = 4
            Text = 'none'
            Items.Strings = (
              'none'
              'odd'
              'even'
              'mark'
              'space')
          end
          object cbbBalancaStopBits: TComboBox
            Left = 161
            Top = 70
            Width = 104
            Height = 21
            Ctl3D = False
            ParentCtl3D = False
            TabOrder = 5
            Items.Strings = (
              's1'
              's1,5'
              's2'
              '')
          end
          object edtBalancaTimeOut: TEdit
            Left = 375
            Top = 71
            Width = 73
            Height = 19
            Ctl3D = False
            ParentCtl3D = False
            TabOrder = 7
          end
          object cbbBalancaModelo: TComboBox
            Left = 12
            Top = 32
            Width = 145
            Height = 21
            Hint = 'Modelo da Balan'#231'a de Checkout'
            Ctl3D = False
            ParentCtl3D = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            Items.Strings = (
              'Nenhuma'
              'Filizola'
              'Toledo'
              'Toledo2180'
              'Urano'
              'LucasTec'
              'Magna'
              'Magellan')
          end
          object cbbBalancaPorta: TComboBox
            Left = 161
            Top = 32
            Width = 104
            Height = 21
            Hint = 'Porta Serial de comunica'#231#227'o com Balan'#231'a'
            Ctl3D = False
            ParentCtl3D = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            Items.Strings = (
              'COM1'
              'COM2'
              'COM3'
              'COM4'
              'COM5'
              'COM6'
              'COM7'
              'COM8')
          end
          object cbbBalancaBaud: TComboBox
            Left = 270
            Top = 32
            Width = 99
            Height = 21
            Ctl3D = False
            ParentCtl3D = False
            TabOrder = 2
            Items.Strings = (
              '110'
              '300'
              '600'
              '1200'
              '2400'
              '4800'
              '9600'
              '14400'
              '19200'
              '38400'
              '56000'
              '57600')
          end
          object cbbBalancaDataBits: TComboBox
            Left = 375
            Top = 32
            Width = 70
            Height = 21
            Ctl3D = False
            ItemIndex = 3
            ParentCtl3D = False
            TabOrder = 3
            Text = '8'
            Items.Strings = (
              '5'
              '6'
              '7'
              '8')
          end
        end
        object GroupBox10: TGroupBox
          Left = 13
          Top = 127
          Width = 588
          Height = 158
          Caption = '| Testar Pesagem |'
          TabOrder = 1
          object Label50: TLabel
            Left = 12
            Top = 67
            Width = 77
            Height = 13
            Caption = #218'ltima Resposta'
          end
          object Label51: TLabel
            Left = 12
            Top = 22
            Width = 79
            Height = 13
            Caption = #218'ltimo Peso Lido'
          end
          object sttPeso: TStaticText
            Left = 12
            Top = 38
            Width = 233
            Height = 24
            AutoSize = False
            BevelKind = bkTile
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -16
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 0
          end
          object sttResposta: TStaticText
            Left = 12
            Top = 84
            Width = 233
            Height = 36
            AutoSize = False
            BevelKind = bkTile
            TabOrder = 1
          end
          object chbBalancaMonitorar: TCheckBox
            Left = 12
            Top = 125
            Width = 233
            Height = 17
            Caption = 'Monitorar a Balan'#231'a'
            TabOrder = 2
          end
          object btnBalAtivar: TBitBtn
            Left = 290
            Top = 32
            Width = 105
            Height = 25
            Caption = 'Ativar'
            Glyph.Data = {
              36060000424D3606000000000000360000002800000020000000100000000100
              18000000000000060000120B0000120B00000000000000000000FFFFFF10945F
              10945F10945F10945F10945F10945F10945F10945F10945F10945F10945F1094
              5F10945F10945FFFFFFFFFFFFF55555555555555555555555555555555555555
              5555555555555555555555555555555555555555555555FFFFFF10945F10945F
              10945F10945F10945F10945F10945F10945F10945F10945F10945F10945F1094
              5F10945F10945F10945F55555555555555555555555555555555555555555555
              555555555555555555555555555555555555555555555555555510945F10945F
              10945F10945F10945F10945F10945F10945F10945F10945F10945F10945F1094
              5F10945F10945F10945F55555555555555555555555555555555555555555555
              555555555555555555555555555555555555555555555555555510945F10945F
              10945F10945F10945F10945F10945F10945F10945F10945F10945F10945F1094
              5F10945F10945F10945F55555555555555555555555555555555555555555555
              555555555555555555555555555555555555555555555555555510945F10945F
              10945F10945F10945FFFFFFFFFFFFFFFFFFF10945F10945F10945F10945F1094
              5F10945F10945F10945F555555555555555555555555555555FFFFFFFFFFFFFF
              FFFF55555555555555555555555555555555555555555555555510945F10945F
              10945F10945FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF10945F10945F10945F1094
              5F10945F10945F10945F555555555555555555555555FFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFF55555555555555555555555555555555555555555510945F10945F
              10945FFFFFFFFFFFFFFFFFFF10945FFFFFFFFFFFFFFFFFFF10945F10945F1094
              5F10945F10945F10945F555555555555555555FFFFFFFFFFFFFFFFFF555555FF
              FFFFFFFFFFFFFFFF55555555555555555555555555555555555510945F10945F
              10945F10945FFFFFFF10945F10945F10945FFFFFFFFFFFFFFFFFFF10945F1094
              5F10945F10945F10945F555555555555555555555555FFFFFF55555555555555
              5555FFFFFFFFFFFFFFFFFF55555555555555555555555555555510945F10945F
              10945F10945F10945F10945F10945F10945F10945FFFFFFFFFFFFFFFFFFF1094
              5F10945F10945F10945F55555555555555555555555555555555555555555555
              5555555555FFFFFFFFFFFFFFFFFF55555555555555555555555510945F10945F
              10945F10945F10945F10945F10945F10945F10945F10945FFFFFFFFFFFFFFFFF
              FF10945F10945F10945F55555555555555555555555555555555555555555555
              5555555555555555FFFFFFFFFFFFFFFFFF55555555555555555510945F10945F
              10945F10945F10945F10945F10945F10945F10945F10945F10945FFFFFFF1094
              5F10945F10945F10945F55555555555555555555555555555555555555555555
              5555555555555555555555FFFFFF55555555555555555555555510945F10945F
              10945F10945F10945F10945F10945F10945F10945F10945F10945F10945F1094
              5F10945F10945F10945F55555555555555555555555555555555555555555555
              555555555555555555555555555555555555555555555555555510945F10945F
              10945F10945F10945F10945F10945F10945F10945F10945F10945F10945F1094
              5F10945F10945F10945F55555555555555555555555555555555555555555555
              555555555555555555555555555555555555555555555555555510945F10945F
              10945F10945F10945F10945F10945F10945F10945F10945F10945F10945F1094
              5F10945F10945F10945F55555555555555555555555555555555555555555555
              555555555555555555555555555555555555555555555555555510945F10945F
              10945F10945F10945F10945F10945F10945F10945F10945F10945F10945F1094
              5F10945F10945F10945F55555555555555555555555555555555555555555555
              5555555555555555555555555555555555555555555555555555FFFFFF10945F
              10945F10945F10945F10945F10945F10945F10945F10945F10945F10945F1094
              5F10945F10945FFFFFFFFFFFFF55555555555555555555555555555555555555
              5555555555555555555555555555555555555555555555FFFFFF}
            NumGlyphs = 2
            TabOrder = 3
            OnClick = btnBalAtivarClick
          end
          object btnBalDesativar: TBitBtn
            Left = 290
            Top = 61
            Width = 105
            Height = 25
            Caption = 'Desativar'
            Glyph.Data = {
              36060000424D3606000000000000360000002800000020000000100000000100
              18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFF1313F20000F10000F10000F10000EF0000EF0000ED1212EEFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6C6C6C60606060606060
              60606060606060605F5F5F6A6A6AFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFF1313F61A20F53C4CF93A49F83847F83545F83443F73242F7141BF11717
              EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6E6E6E7373738B8B8B89898988
              88888686868585858484846E6E6E6D6D6DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              1313F81D23F94453FA2429F91212F70F0FF60C0CF50909F5161BF53343F7141B
              F11717EFFFFFFFFFFFFFFFFFFFFFFFFF6F6F6F7676769090907A7A7A6E6E6E6B
              6B6B6969696767677070708585856E6E6E6D6D6DFFFFFFFFFFFFFFFFFF1313F9
              1F25FA4A58FB4247FBC9C9FD3B3BF91313F71010F63333F7C5C5FD3035F73444
              F7141BF21717EFFFFFFFFFFFFF7070707878789494948D8D8DDEDEDE8787876E
              6E6E6C6C6C818181DBDBDB8181818585856E6E6E6D6D6DFFFFFFFFFFFF0000FB
              4F5DFD3237FBCBCBFEF2F2FFEBEBFE3B3BF93939F8EAEAFEF1F1FEC5C5FD181D
              F63343F70000EFFFFFFFFFFFFF646464979797838383DFDFDFF7F7F7F3F3F387
              8787858585F2F2F2F6F6F6DBDBDB727272858585606060FFFFFFFFFFFF0000FD
              525FFD2828FC4747FCECECFFF2F2FFECECFFECECFEF1F1FFEAEAFE3434F70B0B
              F53545F80000EFFFFFFFFFFFFF6565659999997D7D7D8F8F8FF4F4F4F7F7F7F4
              F4F4F3F3F3F7F7F7F2F2F2828282696969868686606060FFFFFFFFFFFF0000FD
              5562FE2C2CFD2929FC4848FCEDEDFFF2F2FFF2F2FFECECFE3A3AF91212F70F0F
              F63848F80000F1FFFFFFFFFFFF6565659B9B9B8080807D7D7D909090F4F4F4F7
              F7F7F7F7F7F3F3F38686866E6E6E6B6B6B888888606060FFFFFFFFFFFF0000FD
              5764FE3030FD2D2DFD4B4BFCEDEDFFF2F2FFF2F2FFECECFF3D3DF91616F81313
              F73C4BF80000F1FFFFFFFFFFFF6565659C9C9C828282808080929292F4F4F4F7
              F7F7F7F7F7F4F4F48888887070706E6E6E8A8A8A606060FFFFFFFFFFFF0000FF
              5A67FE3333FE5050FDEDEDFFF3F3FFEDEDFFEDEDFFF2F2FFECECFE3E3EFA1717
              F83F4EF90000F1FFFFFFFFFFFF6666669E9E9E848484959595F4F4F4F8F8F8F4
              F4F4F4F4F4F7F7F7F3F3F38989897171718C8C8C606060FFFFFFFFFFFF0000FF
              5B68FF4347FECFCFFFF3F3FFEDEDFF4C4CFC4A4AFCECECFFF2F2FFCACAFE2A2F
              FA4251FA0000F3FFFFFFFFFFFF6666669F9F9F8F8F8FE2E2E2F8F8F8F4F4F492
              9292919191F4F4F4F7F7F7DFDFDF7E7E7E8F8F8F616161FFFFFFFFFFFF1414FF
              262BFF5D6AFF585BFFCFCFFF5252FE2F2FFD2C2CFD4B4BFCCCCCFE484CFB4957
              FB1D23F91414F6FFFFFFFFFFFF7272727E7E7EA0A0A09B9B9BE2E2E297979781
              8181808080929292E0E0E09090909393937676766E6E6EFFFFFFFFFFFFFFFFFF
              1414FF262BFF5D6AFF4347FF3434FE3232FE3030FD2D2DFD383CFC4F5DFC1F25
              FA1414F8FFFFFFFFFFFFFFFFFFFFFFFF7272727E7E7EA0A0A08F8F8F85858584
              84848282828080808787879797977878786F6F6FFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFF1414FF262BFF5C69FF5B68FF5A67FE5865FE5663FE5461FE2227FC0D0D
              FBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7272727E7E7EA0A0A09F9F9F9E
              9E9E9D9D9D9C9C9C9B9B9B7A7A7A6C6C6CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFF1313FF0000FF0000FF0000FF0000FD0000FD0000FD1313FDFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF71717166666666666666
              6666656565656565656565707070FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
            NumGlyphs = 2
            TabOrder = 4
            OnClick = btnBalDesativarClick
          end
          object btnBalPesar: TBitBtn
            Left = 290
            Top = 90
            Width = 105
            Height = 25
            Caption = 'Ler Peso'
            Glyph.Data = {
              36060000424D3606000000000000360000002800000020000000100000000100
              18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
              FFFFFF6693AD4174934F819F598CA95289AB4986A94280A23B76996693ADFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8B8B8B6C6C6C79797983
              83838080807B7B7B7575756C6C6C8B8B8BFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFF4388ACA1E5FE9ADEF893D8F36BBCDC5EBBE05DC7EF5ED0FA4388ACFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7B7B7BD4D4D4CDCDCDC7
              C7C7A8A8A8A5A5A5ADADADB3B3B37B7B7BFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFF8BC0DA6AB0D36AB0D33882A54CAED43377996AB0D36AB0D38BC0DAFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB5B5B5A1A1A1A1A1A172
              7272959595696969A1A1A1A1A1A1B5B5B5FFFFFFFFFFFFFFFFFFFFFFFFE5EEF2
              CCDDE6E5EFF4FFFFFFFFFFFF3F8EB15ED0FA337799FFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFECECECDADADAEDEDEDFFFFFFFFFFFF7C
              7C7CB3B3B3696969FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6BA0BB397FA2
              397FA2397FA26BA0BBFFFFFF408DB15DCBF433779AFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFF959595717171717171717171959595FFFFFF7C
              7C7CAFAFAF6A6A6AFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF4D94B7A2E6FC
              7AC8E65DC6EE4D94B7FFFFFF418EB15DC6ED357A9DFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFF868686D4D4D4B5B5B5ACACAC868686FFFFFF7D
              7D7DACACAC6C6C6CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF76ADC9539ABE
              6EB3D6539ABE76ADC9FFFFFF4590B35EBFE5387DA1FFFFFFFFFFFFE5EEF2CCDD
              E6E5EEF2FFFFFFFFFFFFFFFFFFA2A2A28C8C8CA6A6A68C8C8CA2A2A2FFFFFF80
              8080A7A7A76F6F6FFFFFFFFFFFFFECECECDADADAECECECFFFFFF9DC0D29EC4D7
              FFFFFF9EC4D79DC0D2FFFFFF4793B560B9DD3C82A5FFFFFF6BA0BB397FA2397F
              A2397FA26BA0BBFFFFFFFFFFFFB9B9B9BDBDBDFFFFFFBDBDBDB9B9B9FFFFFF82
              8282A4A4A4747474FFFFFF959595717171717171717171959595E7F0F45996B4
              FFFFFF5996B4E7F0F4FFFFFF4E97B965B8D94287AAFFFFFF4D94B7A2E6FC7AC8
              E65DC6EE4D94B7FFFFFFFFFFFFEEEEEE8A8A8AFFFFFF8A8A8AEEEEEEFFFFFF88
              8888A4A4A47A7A7AFFFFFF868686D4D4D4B5B5B5ACACAC868686FFFFFF8AB7CD
              75A9C48AB7CDFFFFFFFFFFFF569EBF79C5E2468CAFFFFFFF7CB3CF5AA1C56EB3
              D65AA1C57CB3CFFFFFFFFFFFFFFFFFFFAEAEAE9F9F9FAEAEAEFFFFFFFFFFFF8F
              8F8FB2B2B27E7E7EFFFFFFA8A8A8939393A6A6A6939393A8A8A8FFFFFFE8F1F6
              4B92B5E8F1F6FFFFFFFFFFFF5EA5C592D7F24B92B5FFFFFFA4C8DAA5CBDEFFFF
              FFA5CBDEA4C8DAFFFFFFFFFFFFFFFFFFEFEFEF838383EFEFEFFFFFFFFFFFFF95
              9595C6C6C6838383FFFFFFC1C1C1C3C3C3FFFFFFC3C3C3C1C1C17CB1CC5097BB
              5097BB89B9D1BDD8E6EDF4F863AAC999DDF75097BBFFFFFFE8F1F666A4C3FFFF
              FF66A4C3E8F1F6FFFFFFFFFFFFA6A6A6898989898989B0B0B0D3D3D3F3F3F39A
              9A9ACCCCCC898989FFFFFFEFEFEF989898FFFFFF989898EFEFEFFFFFFFFFFFFF
              E1EEF4A1C8DC6BAAC9579EC15FA6C59ADEF8579EC1FFFFFFFFFFFF96C2D882B7
              D196C2D8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEBEBEBC0C0C09D9D9D90909096
              9696CDCDCD909090FFFFFFFFFFFFB9B9B9ACACACB9B9B9FFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFF67ADCC9EE2FB4B92B5A2CCE1C4DFEDE3F0F662A9
              CCEBF4F9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9D
              9D9DD1D1D1838383C3C3C3DADADAEDEDED9B9B9BF2F2F2FFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFF76BEDEA5E9FF6DB2D5ADD4E77FBCDA6DB2D56DB2
              D56DB2D592C6E0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAE
              AEAED7D7D7A4A4A4CCCCCCAFAFAFA4A4A4A4A4A4A4A4A4BCBCBCFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFF97CBE475B9DC97CBE4FFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0
              C0C0ACACACC0C0C0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
            NumGlyphs = 2
            TabOrder = 5
            OnClick = btnBalPesarClick
          end
        end
      end
      object tabPersonalizar: TTabSheet
        Caption = ' Personaliza'#231#227'o '
        ImageIndex = 5
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object GroupBox14: TGroupBox
          Left = 13
          Top = 118
          Width = 479
          Height = 107
          Caption = '| Temas de Cores |'
          TabOrder = 0
          object lblCordeFundo: TLabel
            Left = 233
            Top = 20
            Width = 108
            Height = 13
            Caption = 'Cor de Fundo Campos:'
          end
          object btnCorFundoCampo: TSpeedButton
            Left = 388
            Top = 15
            Width = 23
            Height = 22
            Hint = 'Clique aqui para escolher a cor de fundo'
            Caption = '...'
            ParentShowHint = False
            ShowHint = True
            OnClick = btnCorFundoCampoClick
          end
          object Label60: TLabel
            Left = 233
            Top = 49
            Width = 94
            Height = 13
            Caption = 'Cor de Fundo Foco:'
          end
          object btnCorFundoFoco: TSpeedButton
            Left = 388
            Top = 44
            Width = 23
            Height = 22
            Hint = 'Clique aqui para escolher a cor de fundo'
            Caption = '...'
            ParentShowHint = False
            ShowHint = True
            OnClick = btnCorFundoFocoClick
          end
          object Label61: TLabel
            Left = 233
            Top = 78
            Width = 64
            Height = 13
            Caption = 'Cor da Fonte:'
          end
          object btnCorFonte: TSpeedButton
            Left = 388
            Top = 73
            Width = 23
            Height = 22
            Hint = 'Clique aqui para escolher a cor de fundo'
            Caption = '...'
            ParentShowHint = False
            ShowHint = True
            OnClick = btnCorFonteClick
          end
          object Label62: TLabel
            Left = 20
            Top = 29
            Width = 91
            Height = 13
            Caption = 'Cores Pr'#233'-definidas'
          end
          object edtCorFundoCampo: TMaskEdit
            Left = 350
            Top = 16
            Width = 34
            Height = 21
            TabStop = False
            BevelInner = bvNone
            BevelOuter = bvNone
            Enabled = False
            ReadOnly = True
            TabOrder = 0
            Text = ''
          end
          object edtCorFundoFoco: TMaskEdit
            Left = 350
            Top = 45
            Width = 34
            Height = 21
            TabStop = False
            BevelInner = bvNone
            BevelOuter = bvNone
            Enabled = False
            ReadOnly = True
            TabOrder = 1
            Text = ''
          end
          object edtCorFonte: TMaskEdit
            Left = 350
            Top = 74
            Width = 34
            Height = 21
            TabStop = False
            BevelInner = bvNone
            BevelOuter = bvNone
            Enabled = False
            ReadOnly = True
            TabOrder = 2
            Text = ''
          end
          object cbbCoresPredefinidos: TComboBox
            Left = 20
            Top = 44
            Width = 177
            Height = 21
            Style = csDropDownList
            TabOrder = 3
            OnChange = cbbCoresPredefinidosChange
            Items.Strings = (
              'Padr'#227'o'
              'Cor 1'
              'Cor 2'
              'Cor 3')
          end
        end
        object GroupBox8: TGroupBox
          Left = 13
          Top = 13
          Width = 479
          Height = 95
          Caption = '| Display Frente de Caixa |'
          TabOrder = 1
          object Label63: TLabel
            Left = 16
            Top = 46
            Width = 104
            Height = 13
            Caption = 'Mensagem do Display'
          end
          object chkPersonalizarDisplay: TCheckBox
            Left = 16
            Top = 21
            Width = 161
            Height = 17
            Caption = 'Ativar mensagem no Display'
            TabOrder = 0
            OnClick = chkPersonalizarDisplayClick
          end
          object edtPersonalizarDisplayMensagem: TEdit
            Left = 16
            Top = 62
            Width = 415
            Height = 19
            Hint = 
              'Mensagem obrigat'#243'ria da legisla'#231#227'o para empresas no regime tribu' +
              't'#225'rio do Simples Nacional'
            Ctl3D = False
            Enabled = False
            ParentCtl3D = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
          end
        end
      end
      object tabTef: TTabSheet
        Caption = '  TEF  '
        ImageIndex = 5
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Label12: TLabel
          Left = 140
          Top = 35
          Width = 76
          Height = 13
          Caption = 'Ver'#227'o Homolog.'
        end
        object Label21: TLabel
          Left = 14
          Top = 35
          Width = 83
          Height = 13
          Caption = 'Selecioanar o GP'
        end
        object Label22: TLabel
          Left = 312
          Top = 35
          Width = 98
          Height = 13
          Caption = 'Registro Certifica'#231#227'o'
        end
        object Label23: TLabel
          Left = 226
          Top = 35
          Width = 73
          Height = 13
          Caption = 'Vers'#227'o PAYGO'
        end
        object lblHomologador: TLabel
          Left = 185
          Top = 276
          Width = 63
          Height = 13
          Caption = 'Homologador'
        end
        object GroupBox4: TGroupBox
          Left = 14
          Top = 80
          Width = 425
          Height = 189
          Caption = '| Par'#226'metros para Comunica'#231#227'o |'
          TabOrder = 5
          object Label8: TLabel
            Left = 10
            Top = 18
            Width = 136
            Height = 13
            Caption = 'Arquivos de Requisi'#231#227'o .001'
          end
          object Label9: TLabel
            Left = 11
            Top = 58
            Width = 128
            Height = 13
            Caption = 'Arquivos de Resposta .001'
          end
          object Label17: TLabel
            Left = 11
            Top = 99
            Width = 120
            Height = 13
            Caption = 'Arquivos de Situa'#231#227'o .sts'
          end
          object Label20: TLabel
            Left = 12
            Top = 139
            Width = 125
            Height = 13
            Caption = 'Arquivos Temporarios .tmp'
          end
          object edtTefPathResp: TEdit
            Left = 10
            Top = 72
            Width = 369
            Height = 21
            TabOrder = 1
          end
          object edtTefPathReq: TEdit
            Left = 10
            Top = 32
            Width = 369
            Height = 21
            TabOrder = 0
          end
          object btnBuscaDirReq: TPngBitBtn
            Left = 382
            Top = 31
            Width = 28
            Height = 22
            Caption = '...'
            TabOrder = 4
            TabStop = False
            OnClick = btnBuscaDirReqClick
          end
          object btnBuscaDirResp: TPngBitBtn
            Left = 382
            Top = 71
            Width = 28
            Height = 22
            Caption = '...'
            TabOrder = 5
            TabStop = False
            OnClick = btnBuscaDirRespClick
          end
          object edtTefPathSts: TEdit
            Left = 11
            Top = 113
            Width = 369
            Height = 21
            TabOrder = 2
          end
          object edtTefPathTmp: TEdit
            Left = 11
            Top = 153
            Width = 369
            Height = 21
            TabOrder = 3
          end
          object PngBitBtn1: TPngBitBtn
            Left = 383
            Top = 153
            Width = 28
            Height = 22
            Caption = '...'
            TabOrder = 6
            TabStop = False
            OnClick = PngBitBtn1Click
          end
          object PngBitBtn2: TPngBitBtn
            Left = 383
            Top = 112
            Width = 28
            Height = 22
            Caption = '...'
            TabOrder = 7
            TabStop = False
            OnClick = PngBitBtn2Click
          end
        end
        object chkTefAtivo: TCheckBox
          Left = 14
          Top = 12
          Width = 97
          Height = 17
          Caption = 'Ativo'
          TabOrder = 0
        end
        object edtTefVersao: TEdit
          Left = 140
          Top = 50
          Width = 76
          Height = 21
          TabOrder = 2
        end
        object cbxGP: TComboBox
          Left = 14
          Top = 50
          Width = 116
          Height = 21
          TabOrder = 1
          Text = 'cbxGP'
        end
        object edtTefRegCert: TEdit
          Left = 312
          Top = 50
          Width = 127
          Height = 21
          TabOrder = 4
        end
        object edtTefVersaoPG: TEdit
          Left = 226
          Top = 50
          Width = 76
          Height = 21
          TabOrder = 3
        end
        object chkTefBloquearMouseTeclado: TCheckBox
          Left = 14
          Top = 276
          Width = 157
          Height = 17
          Caption = 'Bloquear mouse e teclado'
          TabOrder = 6
        end
        object cbbHomologador: TComboBox
          Left = 185
          Top = 290
          Width = 145
          Height = 21
          TabOrder = 7
          Text = 'CliSiTef'
          Items.Strings = (
            'CliSiTef'
            'PayGo')
        end
      end
      object TabSheet1: TTabSheet
        Caption = ' Licen'#231'a '
        ImageIndex = 6
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Label24: TLabel
          Left = 8
          Top = 14
          Width = 48
          Height = 13
          Caption = 'Servidor 1'
        end
        object Label25: TLabel
          Left = 8
          Top = 55
          Width = 48
          Height = 13
          Caption = 'Servidor 2'
        end
        object Label26: TLabel
          Left = 116
          Top = 29
          Width = 6
          Height = 20
          Caption = ':'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label27: TLabel
          Left = 116
          Top = 71
          Width = 6
          Height = 20
          Caption = ':'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object edLicencaServidor1: TEdit
          Left = 8
          Top = 29
          Width = 106
          Height = 19
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 0
        end
        object edLicencaServidor2: TEdit
          Left = 8
          Top = 70
          Width = 106
          Height = 19
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 1
        end
        object edLicencaCaminho2: TEdit
          Left = 125
          Top = 70
          Width = 270
          Height = 19
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 2
        end
        object edLicencaCaminho1: TEdit
          Left = 125
          Top = 29
          Width = 270
          Height = 19
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 3
        end
      end
    end
  end
  object barStatus: TStatusBar
    Left = 0
    Top = 606
    Width = 1180
    Height = 19
    Panels = <
      item
        Width = 50
      end>
  end
  object OpenDialog1: TOpenDialog
    Left = 853
    Top = 449
  end
  object ApplicationEvents1: TApplicationEvents
    OnHint = ApplicationEvents1Hint
    Left = 869
    Top = 33
  end
  object dlgCor: TColorDialog
    Left = 946
    Top = 34
  end
  object ACBrTEFD1: TACBrTEFD
    AutoAtivarGP = False
    EsperaSTS = 7
    TEFDial.ArqTemp = 'C:\TEF_DIAL\req\intpos.tmp'
    TEFDial.ArqReq = 'C:\TEF_DIAL\req\intpos.001'
    TEFDial.ArqSTS = 'C:\TEF_DIAL\resp\intpos.sts'
    TEFDial.ArqResp = 'C:\TEF_DIAL\resp\intpos.001'
    TEFDial.GPExeName = 'C:\TEF_DIAL\tef_dial.exe'
    TEFDisc.ArqTemp = 'C:\TEF_Disc\req\intpos.tmp'
    TEFDisc.ArqReq = 'C:\TEF_Disc\req\intpos.001'
    TEFDisc.ArqSTS = 'C:\TEF_Disc\resp\intpos.sts'
    TEFDisc.ArqResp = 'C:\TEF_Disc\resp\intpos.001'
    TEFDisc.GPExeName = 'C:\TEF_Disc\tef_Disc.exe'
    TEFHiper.ArqTemp = 'c:\HiperTEF\req\IntPos.tmp'
    TEFHiper.ArqReq = 'C:\HiperTEF\req\IntPos.001'
    TEFHiper.ArqSTS = 'C:\HiperTEF\resp\IntPos.sts'
    TEFHiper.ArqResp = 'C:\HiperTEF\resp\IntPos.001'
    TEFHiper.GPExeName = 'C:\HiperTEF\HiperTEF.exe'
    TEFVeSPague.EnderecoIP = 'localhost'
    TEFVeSPague.Porta = '60906'
    TEFVeSPague.TemPendencias = False
    TEFVeSPague.TransacaoCRT = 'Cartao Vender'
    TEFVeSPague.TransacaoCHQ = 'Cheque Consultar'
    TEFVeSPague.TransacaoCNC = 'Administracao Cancelar'
    TEFVeSPague.TransacaoReImpressao = 'Administracao Reimprimir'
    TEFVeSPague.TransacaoPendente = 'Administracao Pendente'
    TEFGPU.ArqTemp = 'C:\TEF_GPU\req\intpos.tmp'
    TEFGPU.ArqReq = 'C:\TEF_GPU\req\intpos.001'
    TEFGPU.ArqSTS = 'C:\TEF_GPU\resp\intpos.sts'
    TEFGPU.ArqResp = 'C:\TEF_GPU\resp\intpos.001'
    TEFGPU.GPExeName = 'C:\TEF_GPU\GPU.exe'
    TEFBanese.ArqTemp = 'C:\bcard\req\pergunta.tmp'
    TEFBanese.ArqReq = 'C:\bcard\req\pergunta.txt'
    TEFBanese.ArqSTS = 'C:\bcard\resp\status.txt'
    TEFBanese.ArqResp = 'C:\bcard\resp\resposta.txt'
    TEFBanese.ArqRespBkp = 'C:\bcard\resposta.txt'
    TEFBanese.ArqRespMovBkp = 'C:\bcard\copiamovimento.txt'
    TEFAuttar.ArqTemp = 'C:\Auttar_TefIP\req\intpos.tmp'
    TEFAuttar.ArqReq = 'C:\Auttar_TefIP\req\intpos.001'
    TEFAuttar.ArqSTS = 'C:\Auttar_TefIP\resp\intpos.sts'
    TEFAuttar.ArqResp = 'C:\Auttar_TefIP\resp\intpos.001'
    TEFAuttar.GPExeName = 'C:\Program Files (x86)\Auttar\IntegradorTEF-IP.exe'
    TEFGood.ArqTemp = 'C:\good\gettemp.dat'
    TEFGood.ArqReq = 'C:\good\getreq.dat'
    TEFGood.ArqSTS = 'C:\good\getstat.dat'
    TEFGood.ArqResp = 'C:\good\getresp.dat'
    TEFGood.GPExeName = 'C:\good\GETGoodMed.exe'
    TEFFoxWin.ArqTemp = 'C:\FwTEF\req\intpos.tmp'
    TEFFoxWin.ArqReq = 'C:\FwTEF\req\intpos.001'
    TEFFoxWin.ArqSTS = 'C:\FwTEF\rsp\intpos.sts'
    TEFFoxWin.ArqResp = 'C:\FwTEF\rsp\intpos.001'
    TEFFoxWin.GPExeName = 'C:\FwTEF\bin\FwTEF.exe'
    TEFCliDTEF.ArqResp = ''
    TEFPetrocard.ArqTemp = 'C:\CardTech\req\intpos.tmp'
    TEFPetrocard.ArqReq = 'C:\CardTech\req\intpos.001'
    TEFPetrocard.ArqSTS = 'C:\CardTech\resp\intpos.sts'
    TEFPetrocard.ArqResp = 'C:\CardTech\resp\intpos.001'
    TEFPetrocard.GPExeName = 'C:\CardTech\sac.exe'
    TEFCrediShop.ArqTemp = 'C:\tef_cshp\req\intpos.tmp'
    TEFCrediShop.ArqReq = 'C:\tef_cshp\req\intpos.001'
    TEFCrediShop.ArqSTS = 'C:\tef_cshp\resp\intpos.sts'
    TEFCrediShop.ArqResp = 'C:\tef_cshp\resp\intpos.001'
    TEFCrediShop.GPExeName = 'C:\tef_cshp\vpos_tef.exe'
    TEFTicketCar.ArqTemp = 'C:\TCS\TX\INTTCS.tmp'
    TEFTicketCar.ArqReq = 'C:\TCS\TX\INTTCS.001'
    TEFTicketCar.ArqSTS = 'C:\TCS\RX\INTTCS.RET'
    TEFTicketCar.ArqResp = 'C:\TCS\RX\INTTCS.001'
    TEFTicketCar.GPExeName = 'C:\TCS\tcs.exe'
    TEFTicketCar.NumLoja = 0
    TEFTicketCar.NumCaixa = 0
    TEFTicketCar.AtualizaPrecos = False
    TEFConvCard.ArqTemp = 'C:\ger_convenio\tx\crtsol.tmp'
    TEFConvCard.ArqReq = 'C:\ger_convenio\tx\crtsol.001'
    TEFConvCard.ArqSTS = 'C:\ger_convenio\rx\crtsol.ok'
    TEFConvCard.ArqResp = 'C:\ger_convenio\rx\crtsol.001'
    TEFConvCard.GPExeName = 'C:\ger_convcard\convcard.exe'
    Left = 961
    Top = 95
  end
  object SaveDialog1: TSaveDialog
    Left = 873
    Top = 90
  end
end
