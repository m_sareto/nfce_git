object frmPesquisaMercadoria: TfrmPesquisaMercadoria
  Left = 320
  Top = 204
  BorderIcons = []
  Caption = 'Pesquisa Mercadoria'
  ClientHeight = 408
  ClientWidth = 780
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDesktopCenter
  OnKeyDown = FormKeyDown
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object DBGrid1: TDBGrid
    Left = 0
    Top = 54
    Width = 780
    Height = 335
    Align = alClient
    Color = clInfoBk
    Ctl3D = False
    DataSource = DataSource1
    DrawingStyle = gdsClassic
    FixedColor = clNavy
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = []
    Options = [dgTitles, dgColLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
    ParentCtl3D = False
    ParentFont = False
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWhite
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnEnter = DBGrid1Enter
    OnKeyDown = DBGrid1KeyDown
    Columns = <
      item
        Expanded = False
        FieldName = 'ID'
        Title.Caption = 'C'#243'digo'
        Width = 85
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DESCRICAO'
        Title.Caption = 'Descri'#231#227'o da Mercadoria'
        Width = 333
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'VENDA'
        Title.Alignment = taRightJustify
        Title.Caption = 'Venda R$'
        Width = 67
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'EST_ATUAL'
        Title.Alignment = taRightJustify
        Title.Caption = 'Estoque'
        Width = 74
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'FABRICANTE_DESC'
        Title.Caption = 'Fabricante'
        Width = 200
        Visible = True
      end>
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 780
    Height = 54
    Align = alTop
    Caption = '[  Filtro para Pesquisa  ]'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    ExplicitLeft = 12
    ExplicitTop = 11
    ExplicitWidth = 645
    object Edit1: TEdit
      Left = 8
      Top = 20
      Width = 544
      Height = 19
      CharCase = ecUpperCase
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 0
      OnChange = Edit1Change
      OnKeyDown = Edit1KeyDown
    end
    object chkIniciarPor: TCheckBox
      Left = 558
      Top = 23
      Width = 73
      Height = 17
      Caption = 'Iniciar por'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 389
    Width = 780
    Height = 19
    Panels = <
      item
        Alignment = taCenter
        Text = 'Esc - Sair sem confirmar'
        Width = 130
      end
      item
        Text = 'F12 - Refaz pesquisa'
        Width = 115
      end
      item
        BiDiMode = bdLeftToRight
        ParentBiDiMode = False
        Text = 'Enter - Confirma'
        Width = 100
      end
      item
        Text = 'F1 - Valor de Custo'
        Width = 50
      end>
    ExplicitWidth = 670
  end
  object DataSource1: TDataSource
    DataSet = qMercadoria
    Left = 152
    Top = 128
  end
  object qMercadoria: TIBQuery
    Database = dmCupomFiscal.DataBase
    Transaction = dmCupomFiscal.Transaction
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'select M.Id, M.Descricao, Me.Est_Atual, M.Custo_Ultimo,'
      
        '(Case when ((:pDataE >= M.SM_DtE_Promocao) and (:pDataS <= M.SM_' +
        'DtS_Promocao)) then'
      '    M.Sm_Venda_Promocao'
      ' Else'
      '    M.Venda'
      ' end) as venda, F.ID||'#39'-'#39'||f.descricao as Fabricante_Desc'
      'From Est_MERCADORIAS M'
      
        'Left Outer Join Est_Mercadorias_Estoque Me on Me.Mercadoria=M.Id' +
        ' and Me.CCusto=:pCC'
      'left outer join fabricantes f on f.id = m.fabricante'
      'Where (M.Descricao Like :pDes) and (M.Ativo='#39'S'#39')'
      'Order By M.Descricao'
      ''
      '')
    Left = 104
    Top = 128
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'pDataE'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'pDataS'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'pCC'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'pDes'
        ParamType = ptUnknown
      end>
    object qMercadoriaID: TIBStringField
      FieldName = 'ID'
      Origin = '"EST_MERCADORIAS"."ID"'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      FixedChar = True
      Size = 13
    end
    object qMercadoriaEST_ATUAL: TIBBCDField
      FieldName = 'EST_ATUAL'
      Origin = '"EST_MERCADORIAS_ESTOQUE"."EST_ATUAL"'
      DisplayFormat = '###,##0.000'
      Precision = 18
      Size = 3
    end
    object qMercadoriaVENDA: TIBBCDField
      FieldName = 'VENDA'
      ProviderFlags = []
      DisplayFormat = '###,##0.000'
      Precision = 18
      Size = 4
    end
    object qMercadoriaCUSTO_ULTIMO: TIBBCDField
      FieldName = 'CUSTO_ULTIMO'
      Origin = '"EST_MERCADORIAS"."CUSTO_ULTIMO"'
      Required = True
      DisplayFormat = '###,##0.000'
      Precision = 18
      Size = 4
    end
    object qMercadoriaDESCRICAO: TIBStringField
      FieldName = 'DESCRICAO'
      Origin = '"EST_MERCADORIAS"."DESCRICAO"'
      Required = True
      Size = 100
    end
    object qMercadoriaFABRICANTE_DESC: TIBStringField
      FieldName = 'FABRICANTE_DESC'
      ProviderFlags = []
      Size = 52
    end
  end
end
