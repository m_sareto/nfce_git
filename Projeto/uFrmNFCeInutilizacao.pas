unit uFrmNFCeInutilizacao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, xmldom, XMLIntf, msxmldom, XMLDoc, Buttons,
  Xmlxform, Grids, DBGrids, DB, DBClient, DBCtrls, IBCustomDataSet, IBQuery,
  UFrmFim, uDMComponentes, uCtrlTef;

type
  TfrmNFCeInutilizacao = class(TForm)
    Label3: TLabel;
    Label4: TLabel;
    edtNrInicial: TEdit;
    btnEnviar: TBitBtn;
    btnFechar: TBitBtn;
    edtSerie: TEdit;
    lblLocal: TLabel;
    dsCCusto: TDataSource;
    qCCusto: TIBQuery;
    qCCustoID: TIntegerField;
    qCCustoDESCRICAO: TIBStringField;
    lcbCCusto: TDBLookupComboBox;
    Label1: TLabel;
    procedure btnEnviarClick(Sender: TObject);
    procedure btnFecharClick(Sender: TObject);
    procedure edtNrInicialKeyPress(Sender: TObject; var Key: Char);
    procedure edtNrInicialExit(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtSerieKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    constructor Create(AOwner: TComponent; id_local:Integer; desc_local:String);
    reintroduce;
    { Public declarations }
  end;

var
  frmNFCeInutilizacao: TfrmNFCeInutilizacao;

  arq: TextFile;
  numero : Integer;

implementation

uses uFuncoes, uFrmPrincipal, uFrmNFCe, uDMCupomFiscal, uRotinasGlobais;

{$R *.dfm}

constructor TfrmNFCeInutilizacao.Create(AOwner: TComponent; id_local:Integer; desc_local:String);
begin
  inherited Create(AOwner);
  lblLocal.Caption:=desc_local;
  qCCusto.Active:=False;
  qCCusto.ParamByName('pLocal').AsInteger:=id_local;
  qCCusto.Active:=True;
  qCCusto.Last;
  qCCusto.First;
  if qCCusto.Eof then
  begin
    msgInformacao('Empresa n�o possui nenhum Centro de Custo vinculado','');
    Close;
  end
  else
  begin
    qCCusto.Locate('ID', CCUSTO.codigo, [loCaseInsensitive]);
    lcbCCusto.KeyValue:=qCCustoID.Value;
  end;
end;


procedure TfrmNFCeInutilizacao.btnEnviarClick(Sender: TObject);
var
  lCtrlTef: TCtrlTef;
begin
  try
    btnEnviar.Enabled:=False;
    if msgPergunta(Format('Confirmar inutiliza��o da Nota Fiscal n�mero/s�rie %s/%s ?', [edtNrInicial.Text, Trim(edtSerie.Text)]),'') then
    begin
      if(dmCupomFiscal.TefEstaAtivo)then
      begin
        lCtrlTef := TCtrlTef.Create(dmCupomFiscal.DataBase, dmCupomFiscal.Transaction,
          Self, dmCupomFiscal.TefGetImpressora);
        try
          lCtrlTef.EfetuarCancelamento(dmCupomFiscal.TefGetDocVinc(
            StrToInt(edtNrInicial.Text), qCCustoID.Value, Trim(edtSerie.Text)));
        finally
          lCtrlTef.Free;
        end;
      end;
      frmNFCe.inutilizarNumeroNFCe(StrToInt(edtNrInicial.Text), qCCustoID.Value, Trim(edtSerie.Text));
      Close;
    end;
  finally
    btnEnviar.Enabled:=True;
  end;
end;


procedure TfrmNFCeInutilizacao.btnFecharClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmNFCeInutilizacao.edtNrInicialKeyPress(Sender: TObject;
  var Key: Char);
begin
  if not (isDigit(Key)) then Key:= #0;
end;

procedure TfrmNFCeInutilizacao.edtNrInicialExit(Sender: TObject);
begin
  if ((Trim(edtNrInicial.Text)='') or (Trim(edtNrInicial.Text)='0')) then
    edtNrInicial.SetFocus;
end;

procedure TfrmNFCeInutilizacao.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  //Sair
  If (key=vk_Escape) then btnFechar.Click;
end;

procedure TfrmNFCeInutilizacao.edtSerieKeyPress(
  Sender: TObject; var Key: Char);
begin
  if not (isDigit(Key)) then Key:= #0;
end;

end.

