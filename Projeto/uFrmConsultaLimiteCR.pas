unit uFrmConsultaLimiteCR;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls;

type
  TfrmConsultaLimiteCR = class(TForm)
    pnlBackground: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    lbData: TLabel;
    Label4: TLabel;
    lbPC: TLabel;
    edtCliente: TEdit;
    edtClienteNome: TEdit;
    pnlLimite: TPanel;
    pnlSaldo: TPanel;
    procedure Label1Click(Sender: TObject);
    procedure edtClienteExit(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtClienteKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmConsultaLimiteCR: TfrmConsultaLimiteCR;

implementation

uses uFrmPesquisaCliente, uDMCupomFiscal, DB, uFrmSupermercado,
  uFrmPrincipal, uFuncoes, DateUtils, uRotinasGlobais;

{$R *.dfm}

procedure TfrmConsultaLimiteCR.Label1Click(Sender: TObject);
begin
  frmPesquisaCliente := TfrmPesquisaCliente.Create(Self);
  try
    if (frmPesquisaCliente.ShowModal=mrOK) then
      edtCliente.Text:= frmPesquisaCliente.qCliFor.FieldByName('ID').AsString;
  finally
    frmPesquisaCliente.Free;
  end;
end;

procedure TfrmConsultaLimiteCR.edtClienteExit(Sender: TObject);
Var vlrLimite:Currency;
    saldo:Currency;
    dias:Integer;
    taxaJuro:Currency;
begin
  vlrLimite:=0;
  saldo:=0;
  dias:=0;
  DMCupomFiscal.dbQuery1.Active:=False;
  DMCupomFiscal.dbQuery1.SQL.Clear;
  DMCupomFiscal.dbQuery1.SQL.Add('Select nome, limite From CliFor Where ID=:pCliFor');
  DMCupomFiscal.dbQuery1.ParamByName('pCliFor').AsInteger:=StrToIntDef(edtCliente.Text,0);
  DMCupomFiscal.dbQuery1.Active:=True;
  DMCupomFiscal.dbQuery1.First;
  If not(DMCupomFiscal.dbQuery1.Eof) then
  begin
    edtClienteNome.Text:=DMCupomFiscal.dbQuery1.FieldByName('Nome').AsString;
    vlrLimite:=DMCupomFiscal.dbQuery1.FieldByName('Limite').AsCurrency;
    //taxaJuro:=DMCupomFiscal.dbQuery1.FieldByName('juro').AsCurrency;
    pnlLimite.Caption:='Limite R$  '+FormatFloat('###,##0.00',vlrLimite);

    //---Prote��o ao Credito
    DMCupomFiscal.dbQuery1.Active:=False;
    DMCupomFiscal.dbQuery1.SQL.Clear;
    DMCupomFiscal.dbQuery1.SQL.Add('Select Count(*) As XX From CliFor_PC Where (CliFor=:pCliFor) and (Ativo=''S'')');
    DMCupomFiscal.dbQuery1.ParamByName('pCliFor').AsInteger:=StrToIntDef(edtCliente.Text,0);
    DMCupomFiscal.dbQuery1.Active:=True;
    DMCupomFiscal.dbQuery1.First;
    if DMCupomFiscal.dbQuery1.FieldByName('XX').AsInteger>0 then
    begin
      lbPC.Font.Color:=clRed;
      lbPC.Caption:='Sim';
    end
    Else
    begin
      lbPC.Font.Color:=clBlack;
      lbPC.Caption:='N�o';
    end;

    { //-- Anderson dia 08/07/2014
    if FRENTE_CAIXA.CR_Baixa_Seletiva then //---Baixa Seletiva
    begin
      DMCupomFiscal.dbQuery1.Active:=False;
      DMCupomFiscal.dbQuery1.SQL.Clear;
      DMCupomFiscal.dbQuery1.SQL.Add('Select data_ven, resta From CR_Movimento Where CliFor=:pCliFor and resta > 0');
      DMCupomFiscal.dbQuery1.ParamByName('pCliFor').AsInteger:=StrToIntDef(edtCliente.Text,0);
      DMCupomFiscal.dbQuery1.Active:=True;
      DMCupomFiscal.dbQuery1.First;
      while not DMCupomFiscal.dbQuery1.Eof do
      begin
        //Fun��o calculaJuros calcula somente o juros deve ser somado o valor que resta
        saldo := saldo + DMCupomFiscal.dbQuery1.FieldByName('resta').AsCurrency + calculaJuros(taxaJuro, DMCupomFiscal.dbQuery1.FieldByName('resta').AsCurrency, DMCupomFiscal.dbQuery1.FieldByName('data_ven').AsDateTime);
        DMCupomFiscal.dbQuery1.Next;
      end;
    end
    Else  //D�bito e Credito
    begin
      //-- Verifica Se Faz Mais de 30 Dias que N�o Existe Credito
      DMCupomFiscal.dbQuery1.SQL.Clear;
      DMCupomFiscal.dbQuery1.SQL.Add('Select Max(Data_Ope) as Data_Credito'+
                      ' From Cr_Movimento CR '+
                      ' LEFT OUTER JOIN Formas_Pagamento Fp ON (Fp.ID=Cr.Forma_Pag)'+
                      ' Where (Fp.CR_DC=''C'') and (CR.CliFor=:pCliFor)');
      DMCupomFiscal.dbQuery1.ParamByName('pCliFor').AsInteger:=StrToIntDef(edtCliente.Text,0);
      DMCupomFiscal.dbQuery1.Active:=True;
      DMCupomFiscal.dbQuery1.First;
        If Trim(DMCupomFiscal.dbQuery1.FieldByName('Data_Credito').Text)>'' then
        begin
          Dias:=Date - DMCupomFiscal.dbQuery1.FieldByName('Data_Credito').AsVariant;
          If (Dias > 30) then lbData.Caption:=IntTOStr(Dias);
        end;

      //---Conta Corrente
      DMCupomFiscal.dbQuery1.Active:=False;
      DMCupomFiscal.dbQuery1.SQL.Clear;
      DMCupomFiscal.dbQuery1.SQL.Add('Select Sum(case when (F.CR_DC=''C'') then Valor else -Valor end) as TSaldo'+
      ' From CR_Movimento CR'+
      ' Left Outer join Formas_Pagamento F on F.id=CR.Forma_Pag'+
      ' Where CliFor=:pCliFor');
      DMCupomFiscal.dbQuery1.ParamByName('pCliFor').AsInteger:=StrToIntDef(edtCliente.Text,0);
      DMCupomFiscal.dbQuery1.Active:=True;
      DMCupomFiscal.dbQuery1.First;
      Saldo:=DMCupomFiscal.dbQuery1.FieldByName('TSaldo').AsCurrency;
    end;}
    saldo := getSaldoCliente(StrToIntDef(edtCliente.Text,0), True);

    If vlrLimite>=Saldo then
      pnlSaldo.Color:=HexToTColor(PERSONALIZAR.corFundoCampo)
    Else
      pnlSaldo.Color:=clRed;
    pnlSaldo.Caption:='Saldo R$  '+FormatFloat('###,##0.00',Saldo);
  end
  else
  begin
    edtCliente.SetFocus;
    msgInformacao('Cliente n�o cadastrado','');
  end
end;

procedure TfrmConsultaLimiteCR.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
   //Clientes
   If (key=vk_F12) and (edtCliente.Focused) then Label1Click(Action);
   //Sair
   If (key=VK_ESCAPE) then Close;
end;

procedure TfrmConsultaLimiteCR.edtClienteKeyPress(Sender: TObject;
  var Key: Char);
begin
  if not (Key in['0'..'9',Chr(8)]) then Key:= #0;
end;

end.
