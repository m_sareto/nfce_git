unit uFrm_Imp_Fatura;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, RLReport, DB, IBCustomDataSet, Printers, RLPrinters,IBQuery;

type
  TFrm_Imp_Fatura = class(TForm)
    RLReport1: TRLReport;
    RLGroup2: TRLGroup;
    RLBand2: TRLBand;
    RLMemo1: TRLMemo;
    RLBand3: TRLBand;
    RLLabel1: TRLLabel;
    RLLabel3: TRLLabel;
    RLLabel4: TRLLabel;
    RLLabel5: TRLLabel;
    RLLabel6: TRLLabel;
    RLLabel7: TRLLabel;
    qFatura: TIBQuery;
    qFaturaID_ECF: TIntegerField;
    qFaturaDATA: TDateField;
    qFaturaVALOR: TIBBCDField;
    qFaturaNDUP: TIBStringField;
    qFaturaPARCELA: TIntegerField;
    DataSource1: TDataSource;
    RLBand6: TRLBand;
    RLDraw1: TRLDraw;
    RLDraw14: TRLDraw;
    lEnderecoCanc: TRLMemo;
    lEmitCNPJ_IE_IM_Camc: TRLLabel;
    lRazaoSocialCanc: TRLMemo;
    lNomeFantasiaCanc: TRLMemo;
    Data: TRLLabel;
    RLLabel10: TRLLabel;
    RLDraw3: TRLDraw;
    RLSubDetail1: TRLSubDetail;
    RLBand4: TRLBand;
    RLData_Venc: TRLDBText;
    RLValor_Venc: TRLDBText;
    RLBand5: TRLBand;
    RLDraw4: TRLDraw;
    RLTotal: TRLLabel;
    RLLabel9: TRLLabel;
    RLMemo2: TRLMemo;
    RLDraw2: TRLDraw;
    RLLabel8: TRLLabel;
    RLMemo3: TRLMemo;
    RLLabel2: TRLLabel;
    RLBand1: TRLBand;
    RLLabel11: TRLLabel;
    RLLabel12: TRLLabel;
    RLLabel13: TRLLabel;
    RLLabel14: TRLLabel;
    RLBand7: TRLBand;
    RLDBText1: TRLDBText;
    RLDBText2: TRLDBText;
    qMercadorias: TIBQuery;
    DataSource2: TDataSource;
    qMercadoriasVLR: TFloatField;
    qMercadoriasQTD: TIBBCDField;
    qMercadoriasTOTAL: TIBBCDField;
    RLDBText3: TRLDBText;
    RLDBText4: TRLDBText;
    RLDraw5: TRLDraw;
    qMercadoriasDESCRICAO: TIBStringField;
    procedure lNomeFantasiaCancBeforePrint(Sender: TObject;
      var Text: String; var PrintIt: Boolean);
    procedure RLReport1BeforePrint(Sender: TObject; var PrintIt: Boolean);
    procedure lEnderecoCancBeforePrint(Sender: TObject; var Text: String;
      var PrintIt: Boolean);
    procedure lEmitCNPJ_IE_IM_CamcBeforePrint(Sender: TObject;
      var Text: String; var PrintIt: Boolean);
    procedure lRazaoSocialCancBeforePrint(Sender: TObject;
      var Text: String; var PrintIt: Boolean);
    procedure DataBeforePrint(Sender: TObject; var Text: String;
      var PrintIt: Boolean);
    procedure RLLabel3BeforePrint(Sender: TObject; var Text: String;
      var PrintIt: Boolean);
    procedure RLLabel8BeforePrint(Sender: TObject; var Text: String;
      var PrintIt: Boolean);
    procedure RLTotalBeforePrint(Sender: TObject; var Text: String;
      var PrintIt: Boolean);
    procedure RLLabel10BeforePrint(Sender: TObject; var Text: String;
      var PrintIt: Boolean);
    procedure RLLabel5BeforePrint(Sender: TObject; var Text: String;
      var PrintIt: Boolean);
    procedure RLMemo3BeforePrint(Sender: TObject; var Text: String;
      var PrintIt: Boolean);
    procedure RLLabel2BeforePrint(Sender: TObject; var Text: string;
      var PrintIt: Boolean);
    procedure RLSubDetail1BeforePrint(Sender: TObject; var PrintIt: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Frm_Imp_Fatura: TFrm_Imp_Fatura;

implementation

uses uRotinasGlobais, uDMCupomFiscal, uFrmNFCe,
  uDMComponentes, uFrmPrincipal;

{$R *.dfm}

procedure TFrm_Imp_Fatura.lNomeFantasiaCancBeforePrint(Sender: TObject;
  var Text: String; var PrintIt: Boolean);
begin
  Text := dmCupomFiscal.dbQuery3.FieldByName('nome_fantasia').asString;
end;

procedure TFrm_Imp_Fatura.RLReport1BeforePrint(Sender: TObject;
  var PrintIt: Boolean);
begin
  RLPrinter.PrinterName := dmComponentes.ACBrNFeDANFe.Impressora ;
  dmCupomFiscal.dbQuery3.Active:=False;
  dmCupomFiscal.dbQuery3.SQL.Clear;
  dmCupomFiscal.dbQuery3.SQL.Add('Select l.descricao, l.cnpj, l.nome_fantasia, l.IE, l.IM, '+
        'l.endereco, l.numero_end, l.complemento_end, l.bairro, m.descricao as Municipio, l.uf, l.cep, l.fone  from locais l '+
        'left outer join ccustos c on c.local=l.id '+
        'left outer join Municipios m on m.id=l.municipio '+
        'where c.id=:pCCusto');
  dmCupomFiscal.dbQuery3.ParamByName('pCCusto').AsInteger := CCUSTO.codigo;
  dmCupomFiscal.dbQuery3.Active:=True;
  dmCupomFiscal.dbQuery3.First;

  dmCupomFiscal.dbQuery2.Active:=False;
  dmCupomFiscal.dbQuery2.SQL.Clear;
  dmCupomFiscal.dbQuery2.SQL.Add('Select l.nome, l.cnpjcpf, l.ie, l.endereco, '+
    ' l.numero_end, l.ID, l.complemento_end, l.bairro, m.descricao as Municipio,'+
    ' l.uf, l.cep, l.fone, e.Data, e.Vlr_Total, e.Numero, F.Nome as Func_Nome,'+
    ' e.CF_KM, e.CF_PLACA,'+
    ' f.ID as Func_Cod'+
    ' from clifor l'+
    ' left outer join Municipios m on m.id=l.municipio'+
    ' left outer join Est_ecf e on e.Clifor = l.ID'+
    ' Left outer join Funcionarios F on F.ID = e.Funcionario'+
    ' where e.id =:pID');
  dmCupomFiscal.dbQuery2.ParamByName('pID').AsInteger := qFaturaID_ECF.asInteger;
  dmCupomFiscal.dbQuery2.Active:=True;
  dmCupomFiscal.dbQuery2.First;
end;

procedure TFrm_Imp_Fatura.RLSubDetail1BeforePrint(Sender: TObject;
  var PrintIt: Boolean);
begin
  printIT := FRENTE_CAIXA.Comprovante_CredDeb_Itens;
end;

procedure TFrm_Imp_Fatura.lEnderecoCancBeforePrint(Sender: TObject;
  var Text: String; var PrintIt: Boolean);
  var endereco : string;
begin
    Endereco := dmCupomFiscal.dbQuery3.FieldByName('Endereco').asString ;
    if (dmCupomFiscal.dbQuery3.FieldByName('Numero_End').asString <> '') then
      Endereco := Endereco + ', '+dmCupomFiscal.dbQuery3.FieldByName('Numero_End').asString;
    if (dmCupomFiscal.dbQuery3.FieldByName('complemento_end').asString <> '') then
      Endereco := Endereco + ' - '+dmCupomFiscal.dbQuery3.FieldByName('complemento_end').asString;
    if (dmCupomFiscal.dbQuery3.FieldByName('Bairro').asString <> '') then
      Endereco := Endereco + ' - '+dmCupomFiscal.dbQuery3.FieldByName('Bairro').asString;
    if (dmCupomFiscal.dbQuery3.FieldByName('Municipio').asString <> '') then
      Endereco := Endereco + ' - '+dmCupomFiscal.dbQuery3.FieldByName('Municipio').asString;
    if (dmCupomFiscal.dbQuery3.FieldByName('UF').asString <> '') then
      Endereco := Endereco + ' - '+dmCupomFiscal.dbQuery3.FieldByName('UF').asString;
    if (dmCupomFiscal.dbQuery3.FieldByName('Cep').asString <> '') then
      Endereco := Endereco + ' - '+dmCupomFiscal.dbQuery3.FieldByName('Cep').asString;
    if (dmCupomFiscal.dbQuery3.FieldByName('Fone').asString <> '') then
      Endereco := Endereco + ' - FONE: '+dmCupomFiscal.dbQuery3.FieldByName('Fone').asString;
  TEXT := Endereco;
end;

procedure TFrm_Imp_Fatura.lEmitCNPJ_IE_IM_CamcBeforePrint(Sender: TObject;
  var Text: String; var PrintIt: Boolean);
   VAR  CNPJ_IE_IM:string;
begin
  CNPJ_IE_IM := 'CNPJ:'+DMCupomFiscal.dbQuery3.fieldByName('cnpj').AsSTring;
  if (DMCupomFiscal.dbQuery3.fieldByName('IE').AsSTring <> '') then
    CNPJ_IE_IM := CNPJ_IE_IM + ' IE:'+DMCupomFiscal.dbQuery3.fieldByName('IE').AsSTring;
  if (DMCupomFiscal.dbQuery3.fieldByName('IM').AsSTring <> '') then
    CNPJ_IE_IM := CNPJ_IE_IM + ' IM:'+DMCupomFiscal.dbQuery3.fieldByName('IM').AsSTring;
  TEXT := CNPJ_IE_IM;
end;

procedure TFrm_Imp_Fatura.lRazaoSocialCancBeforePrint(Sender: TObject;
  var Text: String; var PrintIt: Boolean);
begin
  Text := dmCupomFiscal.dbQuery3.FieldByName('Descricao').asString;
end;

procedure TFrm_Imp_Fatura.DataBeforePrint(Sender: TObject;
  var Text: String; var PrintIt: Boolean);
begin
  text:=dmCupomFiscal.dbQuery2.FieldByName('Data').AsString;
  //text:=frmNFCe.qECFDATA.AsString;
end;

procedure TFrm_Imp_Fatura.RLLabel3BeforePrint(Sender: TObject;
  var Text: String; var PrintIt: Boolean);
begin
  text:=FormatFloat('##,##0.00',  dmCupomFiscal.dbQuery2.FieldByName('VLR_TOTAL').AsCurrency);
  //text:=FormatFloat('##,##0.00',frmNFCe.qECFVLR_TOTAL.AsCurrency);
end;

procedure TFrm_Imp_Fatura.RLLabel8BeforePrint(Sender: TObject;
  var Text: String; var PrintIt: Boolean);
begin
  text:=  dmCupomFiscal.dbQuery2.FieldByName('ID').AsString+'/'+
    dmCupomFiscal.dbQuery2.FieldByName('NOme').AsString;
  //text:=frmNFCe.qECFCLIFOR_NOME.AsString;
end;

procedure TFrm_Imp_Fatura.RLTotalBeforePrint(Sender: TObject;
  var Text: String; var PrintIt: Boolean);
begin
  text:=FormatFloat('##,##0.00',dmCupomFiscal.dbQuery1.fieldbyName('VALOR').asCurrency);
end;

procedure TFrm_Imp_Fatura.RLLabel10BeforePrint(Sender: TObject;
  var Text: String; var PrintIt: Boolean);
begin
  //text:='Num: '+frmNFCe.qECFNUMERO.AsString;
  text:='Num: '+dmCupomFiscal.dbQuery2.FieldByName('Numero').AsString;
end;

procedure TFrm_Imp_Fatura.RLLabel2BeforePrint(Sender: TObject; var Text: string;
  var PrintIt: Boolean);
begin
  Text := 'Operador: ' +
    dmCupomFiscal.dbQuery2.FieldByName('Func_Cod').asString+' - '+
    dmCupomFiscal.dbQuery2.FieldByName('Func_Nome').asString;
end;

procedure TFrm_Imp_Fatura.RLLabel5BeforePrint(Sender: TObject;
  var Text: String; var PrintIt: Boolean);
begin
  text:=FormatFloat('##,##0.00',dmCupomFiscal.dbQuery2.FieldByName('Vlr_Total').AsCurrency
                            -dmCupomFiscal.dbQuery1.fieldbyName('VALOR').asCurrency);

{  text:=FormatFloat('##,##0.00',frmNFCe.qECFVLR_TOTAL.AsCurrency
                            -dmCupomFiscal.dbQuery1.fieldbyName('VALOR').asCurrency);}
end;

procedure TFrm_Imp_Fatura.RLMemo3BeforePrint(Sender: TObject;
  var Text: String; var PrintIt: Boolean);
  var endereco : string;
begin
  endereco:='';
  if(dmCupomFiscal.dbQuery2.FieldByName('cnpjCPF').asString <> '') then
    endereco:='CPF/CNPJ: '+DMCupomFiscal.dbQuery2.fieldByName('cnpjCPF').AsSTring+sLineBreak;
  Endereco := endereco + dmCupomFiscal.dbQuery2.FieldByName('Endereco').asString ;
  if (dmCupomFiscal.dbQuery2.FieldByName('Numero_End').asString <> '') then
    Endereco := Endereco + ', '+dmCupomFiscal.dbQuery2.FieldByName('Numero_End').asString;
  if (dmCupomFiscal.dbQuery2.FieldByName('complemento_end').asString <> '') then
    Endereco := Endereco + ' - '+dmCupomFiscal.dbQuery2.FieldByName('complemento_end').asString;
  if (dmCupomFiscal.dbQuery2.FieldByName('Bairro').asString <> '') then
    Endereco := Endereco + ' - '+dmCupomFiscal.dbQuery2.FieldByName('Bairro').asString;
  if (dmCupomFiscal.dbQuery2.FieldByName('Municipio').asString <> '') then
    Endereco := Endereco + ' - '+dmCupomFiscal.dbQuery2.FieldByName('Municipio').asString;
  if (dmCupomFiscal.dbQuery2.FieldByName('UF').asString <> '') then
    Endereco := Endereco + ' - '+dmCupomFiscal.dbQuery2.FieldByName('UF').asString;
  if (dmCupomFiscal.dbQuery2.FieldByName('Cep').asString <> '') then
    Endereco := Endereco + ' - '+dmCupomFiscal.dbQuery2.FieldByName('Cep').asString;
  if (dmCupomFiscal.dbQuery2.FieldByName('Fone').asString <> '') then
    Endereco := Endereco + ' - FONE: '+dmCupomFiscal.dbQuery2.FieldByName('Fone').asString;
  TEXT := Endereco;
  Endereco := '';
  if (dmCupomFiscal.dbQuery2.FieldByName('CF_KM').asString <> '') then
    Endereco := Endereco +#13+#13+'KM:' + dmCupomFiscal.dbQuery2.FieldByName('CF_KM').asString;
  if (dmCupomFiscal.dbQuery2.FieldByName('CF_Placa').asString <> '') then
    if(Endereco = '')Then
      Endereco := Endereco +#13+#13+'Placa:' + dmCupomFiscal.dbQuery2.FieldByName('CF_Placa').asString
    else
      Endereco := Endereco +'    Placa:' + dmCupomFiscal.dbQuery2.FieldByName('CF_Placa').asString;
  Text := Text + Endereco;
end;

end.


