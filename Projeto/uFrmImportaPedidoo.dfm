object frmImportaPedido: TfrmImportaPedido
  Left = 213
  Top = 57
  Width = 909
  Height = 621
  BorderIcons = []
  Caption = 'Importar Pr'#233'-Venda para Cupom Fiscal'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  OnClose = FormClose
  OnKeyDown = FormKeyDown
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 893
    Height = 583
    Align = alClient
    Color = clWhite
    TabOrder = 0
    object grdDados: TDBGrid
      Left = 1
      Top = 121
      Width = 891
      Height = 224
      Align = alTop
      Ctl3D = False
      DataSource = dsPedido
      FixedColor = clWhite
      Options = [dgTitles, dgColumnResize, dgColLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
      ParentCtl3D = False
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'ID'
          Title.Caption = 'Pedido'
          Width = 52
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DATA'
          Title.Caption = 'Data'
          Width = 79
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CLIENTE'
          Title.Caption = 'Cliente'
          Width = 265
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'OPERACAO'
          Title.Caption = 'Opera'#231#227'o'
          Width = 226
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FORMA_PAG'
          Title.Caption = 'Forma Pagamento'
          Visible = False
        end
        item
          Expanded = False
          FieldName = 'VLR_PRODUTO'
          Title.Alignment = taRightJustify
          Title.Caption = 'Valor Produtos'
          Width = 86
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'VLR_DESCONTO'
          Title.Alignment = taRightJustify
          Title.Caption = 'Valor Desconto'
          Width = 88
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'VLR_TOTAL'
          Title.Alignment = taRightJustify
          Title.Caption = 'Valor Total'
          Width = 74
          Visible = True
        end>
    end
    object pnlRodape: TPanel
      Left = 1
      Top = 553
      Width = 891
      Height = 29
      Align = alBottom
      Ctl3D = False
      ParentBackground = False
      ParentCtl3D = False
      TabOrder = 1
      object btnCupom: TBitBtn
        Left = 695
        Top = 2
        Width = 93
        Height = 25
        Caption = 'Emitir Cupom'
        TabOrder = 0
        OnClick = btnCupomClick
        Glyph.Data = {
          F6010000424DF601000000000000F60000002800000010000000100000000100
          08000000000000010000120B0000120B00003000000030000000418020004180
          260041882600579141006F9A5E004F883A004888330077AD660041882D005791
          4800579A480048913A004F9141006FAD660041883A003A773300488841004F91
          4800579A4F005EA4570033802D003A883300489A41004F9A48003A883A004191
          410057A457004880480066A4660077AD7700ADCBAD00BAD2BA00D5E0D500579A
          5E0080B788005EA46F006FAD800077B7880088C19A006FB7880077B791006FB7
          910088C1A40026CC800078E9C200FFFFFF00FFFFFF00000000002E2E2E2E2E2E
          2E2E2E2E2E2E2E2E2E2E2E2E2E2E20070201010207202E2E2E2E2E2E2E07080A
          130D0D130A08072E2E2E2E2E070B131E1E1518161A130B072E2E2E200812191E
          2D1E0E0E19171308202E2E070C11111E2D2D1E1111171203072E2E011414141E
          2D2D2D1E14141013012E2E000F0F0F1E2D2D2D2D1F0F0F04002E2E061B1B1B1E
          2D2D2D2D2D2C1B1D012E2E062121211F2D2D2D2D2B21211D062E2E071223231F
          2D2D2D2B23232323072E2E200624271F2D2D2B2727272706202E2E2E0709252B
          2D2B2929282809072E2E2E2E2E07062B2B262A221C05072E2E2E2E2E2E2E2007
          0501010507202E2E2E2E2E2E2E2E2E2E2E2E2E2E2E2E2E2E2E2E}
      end
      object btnFechar: TBitBtn
        Left = 789
        Top = 2
        Width = 93
        Height = 25
        Caption = 'Fechar'
        TabOrder = 1
        OnClick = btnFecharClick
        Glyph.Data = {
          36060000424D3606000000000000360000002800000020000000100000000100
          18000000000000060000120B0000120B00000000000000000000F9F9F9F1F1F1
          F2F2F2F4F4F3FCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFEFE
          FEEEEEEECECFD0E2E2E3F9F9F9F1F1F1F2F2F2F4F4F4FCFCFCFCFCFCFCFCFCFC
          FCFCFCFCFCFCFCFCFCFCFCFCFCFCFEFEFEEEEEEECFCFCFE2E2E2C9C9C9989898
          9B9B9B9595957E8081807F7F7F80807F80807F80807F8080807F7F7F83847F8A
          9370889A5D8CB298A6B1C9C9C99898989B9B9B9595958080807F7F7F80808080
          80808080808080807F7F7F828282898989868686888888A5A5A5E9E9E9DFDFDF
          E2E1E1C5C6C854657876706B706F6F716F6F716F6F716F70746C66637C91528D
          BC5087B35F9BC995A5B2E9E9E9DFDFDFE1E1E1C6C6C66666666F6F6F6F6F6F6F
          6F6F6F6F6F7070706B6B6B7A7A7A888888838383959595A4A4A4FFFFFFFFFFFF
          FFFFFFF4F4F5596B7F6D66606765646765646765646765656A625B5A768D4C87
          B74C80AA6197C497A5B1FFFFFFFFFFFFFFFFFFF4F4F46C6C6C65656565656565
          65656565656565656161617474748383837C7C7C939393A4A4A4FFFFFFFFFFFF
          E9E9E9CECFD05B697D66625C636160636261636261636162665D565A758B4F8A
          B94D83AF669DCA97A5B1FFFFFFFFFFFFE9E9E9CFCFCF6B6B6B60606061616162
          62626262626262625C5C5C7373738585857F7F7F999999A4A4A4FFFFFFFFFFFF
          7A9E8D3E95785C6178675C595F5F5D605E5D605E5D605E5E62595259748B518D
          BE5084B06B9EC996A5B0FFFFFFFFFFFF8C8C8C6C6C6C6868685D5D5D5E5E5E5E
          5E5E5E5E5E5E5E5E5858587272728989898181819B9B9BA3A3A3B0BBB7649680
          27926701DB9728896A6251525E5A5B5B5B5A5C5B5A5C5C5B5C544E58748A5590
          C24E87B269A1CC97A5B0B6B6B67E7E7E5F5F5F7676765C5C5C5555555B5B5B5B
          5B5B5B5B5B5C5C5C5353537272728C8C8C8181819C9C9CA4A4A47DA2911BE3A8
          00D19900D59E00D0932984625B51525A565758575658585758504A57738A5992
          C24299CC65B2E298A3AE90909087878772727275757571717159595954545457
          57575757575858584F4F4F7171718E8E8E8B8B8BA6A6A6A3A3A3819F903EE6BF
          0ED6AB0BC89D10DCAE1AA676514F4B595154555454555555554C4757738B5B96
          C8409BCE6BAEDA98A3AD9090909B9B9B7D7D7D7474748181816464644E4E4E54
          54545454545555554C4C4C7171719292928B8B8BA5A5A5A3A3A398B3A651C79C
          42B9974CDBBF20AB8142584B574C4F52505052505052515251484357738B5D98
          CA4C97C971B7E498A3ADA6A6A68F8F8F8383839C9C9C6B6B6B4D4D4D50505050
          50505050505252524848487171719494948D8D8DADADADA3A3A3EFF4F2DBEEE3
          5F9E7D2DAD8A3F67695542444F4E4E4E4D4D4E4D4D4E4E4F4C443F55738A5C9C
          D05A91C182B3DD96A3AEF2F2F2E4E4E47E7E7E7373735858584747474E4E4E4D
          4D4D4D4D4D4E4E4E4444447070709797978E8E8EB0B0B0A2A2A2FFFFFFFFFFFF
          C3D9CEA9C6BC595D784C46424B4A4B4B4A4A4B4A4A4B4B4C49413C55728C60A0
          D45994C684B7E197A3ADFFFFFFFFFFFFCECECEB8B8B86666664646464B4B4B4A
          4A4A4A4A4A4B4B4B4141417171719B9B9B909090B3B3B3A2A2A2FFFFFFFFFFFF
          FFFFFFE9EDEC4E5B6F473E374643414643404643404644424438305170885DA0
          D65A97C989BBE597A3ACFFFFFFFFFFFFFFFFFFEBEBEB5E5E5E3D3D3D43434342
          42424242424444443737376D6D6D9B9B9B939393B8B8B8A2A2A2FFFFFFFFFFFF
          FFFFFFE7E7E8445C6F45424144464944464844464845474A3D3A39667F957AB8
          EC5495CB89BBE697A3ACFFFFFFFFFFFFFFFFFFE7E7E75A5A5A42424246464646
          46464646464747473A3A3A7E7E7EB4B4B4919191B8B8B8A2A2A2FFFFFFFFFFFF
          FFFFFFF9F9F9B4C7D6A7C0D3A8BFD3A8BFD3A8BFD3A8BFD3A5BDD1B5C8D7AFBC
          C891C0E796C6EF95A0AAFFFFFFFFFFFFFFFFFFF9F9F9C5C5C5BEBEBEBEBEBEBE
          BEBEBEBEBEBEBEBEBBBBBBC6C6C6BCBCBCBDBDBDC3C3C3A0A0A0FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFC
          FCDEE2E4C1D9ED99A5AFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFCFCE1E1E1D7D7D7A4A4A4}
        NumGlyphs = 2
      end
    end
    object pnlCabecalho: TPanel
      Left = 1
      Top = 1
      Width = 891
      Height = 100
      Align = alTop
      Color = clWhite
      ParentBackground = False
      TabOrder = 2
      object Label11: TLabel
        Left = 14
        Top = 3
        Width = 4
        Height = 16
        Color = clBlack
        Font.Charset = ARABIC_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Verdana'
        Font.Style = [fsUnderline]
        ParentColor = False
        ParentFont = False
        Transparent = True
      end
      object Label34: TLabel
        Left = 15
        Top = 46
        Width = 33
        Height = 13
        Caption = 'Pedido'
      end
      object Label35: TLabel
        Left = 88
        Top = 65
        Width = 6
        Height = 13
        Caption = 'a'
      end
      object Label30: TLabel
        Left = 234
        Top = 46
        Width = 53
        Height = 13
        Caption = 'Per'#237'odo de'
      end
      object Label31: TLabel
        Left = 325
        Top = 65
        Width = 15
        Height = 13
        Caption = 'at'#233
      end
      object Label1: TLabel
        Left = 15
        Top = 7
        Width = 32
        Height = 13
        Caption = 'Cliente'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsUnderline]
        ParentFont = False
        OnClick = Label1Click
      end
      object edtCliente: TEdit
        Left = 15
        Top = 21
        Width = 51
        Height = 20
        Ctl3D = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        MaxLength = 6
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 0
        OnEnter = edtClienteEnter
        OnExit = edtClienteExit
        OnKeyPress = edtClienteKeyPress
      end
      object edtClienteNome: TEdit
        Left = 64
        Top = 21
        Width = 370
        Height = 20
        TabStop = False
        Ctl3D = False
        Enabled = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        MaxLength = 6
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 1
      end
      object edtPedidoIni: TEdit
        Left = 15
        Top = 61
        Width = 68
        Height = 19
        Ctl3D = False
        MaxLength = 7
        ParentCtl3D = False
        TabOrder = 2
        Text = '0'
        OnKeyPress = edtPedidoIniKeyPress
      end
      object edtPedidoFim: TEdit
        Left = 103
        Top = 61
        Width = 68
        Height = 19
        Ctl3D = False
        MaxLength = 7
        ParentCtl3D = False
        TabOrder = 3
        Text = '999999'
        OnKeyPress = edtPedidoFimKeyPress
      end
      object dtpInicial: TDateTimePicker
        Left = 236
        Top = 59
        Width = 85
        Height = 21
        Date = 40430.000000000000000000
        Time = 40430.000000000000000000
        TabOrder = 4
      end
      object dtpFinal: TDateTimePicker
        Left = 350
        Top = 59
        Width = 86
        Height = 21
        Date = 40430.999305555550000000
        Time = 40430.999305555550000000
        TabOrder = 5
      end
      object btnAtualizar: TBitBtn
        Left = 480
        Top = 55
        Width = 75
        Height = 25
        Caption = 'Atualizar'
        TabOrder = 6
        OnClick = btnAtualizarClick
        Glyph.Data = {
          36060000424D3606000000000000360000002800000020000000100000000100
          18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFF3C915EFFFFFFFFFFFF489A6A429766429766489A6AFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF656565FFFFFFFFFFFF7070706B
          6B6B6B6B6B707070FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFF2B875099CBAD49A06E95DAB795DAB795D8B595D7B4489A6ACCF2DEFEFE
          FEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF575757B1B1B1737373B7B7B7B7
          B7B7B6B6B6B6B6B6707070DFDFDFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFF2B875099F0C593DCB740D48A40D18893E2B993E2B995D7B42B8750FFFF
          FFFDFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF575757C5C5C5B7B7B78A8A8A88
          8888BABABABABABAB6B6B6575757FFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFF2B875099F0C53FD48840D08797D8B62B87502B87502B875089BC9E2B87
          50FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF575757C5C5C5898989888888B7
          B7B7575757575757575757A2A2A2575757FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFF2B8750B0F3D398EEC399F0C52B8750FFFFFFFFFFFFFFFFFF429A682B87
          50FFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFF575757D2D2D2C3C3C3C5C5C557
          5757FFFFFFFFFFFFFFFFFF6D6D6D575757FFFFFFFEFEFEFFFFFFFFFFFFFFFFFF
          FFFFFF2B87502B87502B87502B87502B87502B8750FFFFFFFEFEFEFFFFFF4498
          67FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF57575757575757575757575757
          5757575757FFFFFFFEFEFEFFFFFF6D6D6DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFF449867FFFFFFFEFEFEFFFFFF2B87502B87502B87502B87502B87
          502B8750FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6D6D6DFFFFFFFEFEFEFF
          FFFF575757575757575757575757575757575757FFFFFFFFFFFFFFFFFFFFFFFF
          FEFEFEFFFFFF2B8750429A68FFFFFFFFFFFFFFFFFF2B875098EEC395E5BD94E0
          B92B8750FFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFF5757576D6D6DFFFFFFFF
          FFFFFFFFFF575757C3C3C3BDBDBDBABABA575757FFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFF2B875089BC9E2B87502B87502B875097D8B640D0873FD48895E5
          BD2B8750FFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFF575757A2A2A257575757
          5757575757B7B7B7888888898989BDBDBD575757FFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFEFEFEFFFFFF2B8750B7EED393E2B993E2B940D18840D48A93DCB7BBF2
          D62B8750FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFF575757D3D3D3BA
          BABABABABA8888888A8A8AB7B7B7D6D6D6575757FFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFEFEFECCF2DE439D6ABBF2D6B7EED3BBF2D6BBF2D649A06E99CB
          AD2B8750FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEDFDFDF6F6F6FD6
          D6D6D3D3D3D6D6D6D6D6D6737373B1B1B1575757FFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF489A6A429766429766489A6AFFFFFFFFFF
          FF3C915EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF70
          70706B6B6B6B6B6B707070FFFFFFFFFFFF656565FFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
        NumGlyphs = 2
      end
    end
    object Panel3: TPanel
      Left = 1
      Top = 345
      Width = 891
      Height = 20
      Align = alTop
      Caption = 'Itens do Pedido'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentBackground = False
      ParentFont = False
      TabOrder = 3
    end
    object Panel2: TPanel
      Left = 1
      Top = 101
      Width = 891
      Height = 20
      Align = alTop
      Caption = 'Pedido'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentBackground = False
      ParentFont = False
      TabOrder = 4
    end
    object DBGrid1: TDBGrid
      Left = 1
      Top = 365
      Width = 891
      Height = 188
      Align = alClient
      Ctl3D = False
      DataSource = dsPedidoItem
      FixedColor = clWhite
      Options = [dgTitles, dgColumnResize, dgColLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
      ParentCtl3D = False
      TabOrder = 5
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'ITEM'
          Title.Caption = 'Item'
          Width = 52
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MERCADORIA'
          Title.Caption = 'C'#243'digo'
          Width = 106
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MERC_DESCRICAO'
          Title.Caption = 'Descri'#231#227'o mercadoria'
          Width = 329
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'UNIDADE'
          Title.Caption = 'Unidade'
          Width = 47
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'QUANTIDADE'
          Title.Alignment = taRightJustify
          Title.Caption = 'Quantidade'
          Width = 73
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'VLR_UNITARIO'
          Title.Alignment = taRightJustify
          Title.Caption = 'Valor Unit'#225'rio'
          Width = 86
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'VLR_DESCONTO_ITEM'
          Title.Alignment = taRightJustify
          Title.Caption = 'Valor Desconto'
          Width = 91
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'VLR_TOTAL'
          Title.Alignment = taRightJustify
          Title.Caption = 'Valor Total'
          Width = 84
          Visible = True
        end>
    end
    object Panel4: TPanel
      Left = 356
      Top = 241
      Width = 185
      Height = 33
      Caption = 'Aguarde!  Imprimindo Item...'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentBackground = False
      ParentFont = False
      TabOrder = 6
      Visible = False
    end
  end
  object dsPedido: TDataSource
    DataSet = qurPedido
    OnDataChange = dsPedidoDataChange
    Left = 752
    Top = 8
  end
  object qurPedido: TIBQuery
    Database = dmCupomFiscal.DataBase
    Transaction = dmCupomFiscal.Transaction
    SQL.Strings = (
      
        'Select p.id, p.el_chave, p.data, p.ccusto, p.vlr_produto, p.vlr_' +
        'total, p.status, p.observacao, p.clifor, p.vlr_desconto,'
      'p.clifor||'#39'/'#39'||p.cf_nome cliente,'
      'p.operacao||'#39'/'#39'||o.descricao operacao,'
      'p.forma_pag||'#39'/'#39'||fp.descricao forma_pag from  est_pedidos p'
      'left outer join clifor c on c.id=p.clifor'
      'left outer join operacoes o on o.id=p.operacao'
      'left outer join formas_pagamento fp on fp.id=p.forma_pag'
      'where (p.id between :pPedidoI and :pPedidoF)'
      'and (p.data between :pDataI and :pDataF)'
      'and (p.status='#39'X'#39')')
    Left = 784
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'pPedidoI'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'pPedidoF'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'pDataI'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'pDataF'
        ParamType = ptUnknown
      end>
    object qurPedidoID: TIntegerField
      FieldName = 'ID'
      Origin = '"EST_PEDIDOS"."ID"'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object qurPedidoDATA: TDateField
      FieldName = 'DATA'
      Origin = '"EST_PEDIDOS"."DATA"'
      Required = True
    end
    object qurPedidoCCUSTO: TIntegerField
      FieldName = 'CCUSTO'
      Origin = '"EST_PEDIDOS"."CCUSTO"'
      Required = True
    end
    object qurPedidoVLR_PRODUTO: TIBBCDField
      FieldName = 'VLR_PRODUTO'
      Origin = '"EST_PEDIDOS"."VLR_PRODUTO"'
      DisplayFormat = '#,##0.00'
      Precision = 18
      Size = 2
    end
    object qurPedidoVLR_DESCONTO: TIBBCDField
      FieldName = 'VLR_DESCONTO'
      Origin = '"EST_PEDIDOS"."VLR_DESCONTO"'
      DisplayFormat = '#,##0.00'
      Precision = 18
      Size = 2
    end
    object qurPedidoVLR_TOTAL: TIBBCDField
      FieldName = 'VLR_TOTAL'
      Origin = '"EST_PEDIDOS"."VLR_TOTAL"'
      DisplayFormat = '#,##0.00'
      Precision = 18
      Size = 2
    end
    object qurPedidoSTATUS: TIBStringField
      FieldName = 'STATUS'
      Origin = '"EST_PEDIDOS"."STATUS"'
      Size = 1
    end
    object qurPedidoOBSERVACAO: TIBStringField
      FieldName = 'OBSERVACAO'
      Origin = '"EST_PEDIDOS"."OBSERVACAO"'
      Size = 1000
    end
    object qurPedidoCLIENTE: TIBStringField
      FieldName = 'CLIENTE'
      ProviderFlags = []
      Size = 52
    end
    object qurPedidoOPERACAO: TIBStringField
      FieldName = 'OPERACAO'
      ProviderFlags = []
      Size = 72
    end
    object qurPedidoFORMA_PAG: TIBStringField
      FieldName = 'FORMA_PAG'
      ProviderFlags = []
      Size = 52
    end
    object qurPedidoCLIFOR: TIntegerField
      FieldName = 'CLIFOR'
      Origin = '"EST_PEDIDOS"."CLIFOR"'
      Required = True
    end
    object qurPedidoEL_CHAVE: TIntegerField
      FieldName = 'EL_CHAVE'
      Origin = '"EST_PEDIDOS"."EL_CHAVE"'
    end
  end
  object dsPedidoItem: TDataSource
    DataSet = qurPedidoItem
    Left = 728
    Top = 48
  end
  object qurPedidoItem: TIBQuery
    Database = dmCupomFiscal.DataBase
    Transaction = dmCupomFiscal.Transaction
    SQL.Strings = (
      
        'select i.*, m.descricao merc_descricao, m.unidade from est_pedid' +
        'os_item i'
      'left outer join est_mercadorias m on m.id=i.mercadoria'
      'where i.id_pedido= :pID')
    Left = 760
    Top = 48
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'pID'
        ParamType = ptUnknown
      end>
    object qurPedidoItemID_PEDIDO: TIntegerField
      FieldName = 'ID_PEDIDO'
      Origin = '"EST_PEDIDOS_ITEM"."ID_PEDIDO"'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object qurPedidoItemITEM: TIntegerField
      FieldName = 'ITEM'
      Origin = '"EST_PEDIDOS_ITEM"."ITEM"'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object qurPedidoItemMERCADORIA: TIBStringField
      FieldName = 'MERCADORIA'
      Origin = '"EST_PEDIDOS_ITEM"."MERCADORIA"'
      Required = True
      Size = 13
    end
    object qurPedidoItemQUANTIDADE: TIBBCDField
      FieldName = 'QUANTIDADE'
      Origin = '"EST_PEDIDOS_ITEM"."QUANTIDADE"'
      DisplayFormat = '#,##0.000'
      Precision = 18
      Size = 3
    end
    object qurPedidoItemVLR_UNITARIO: TIBBCDField
      FieldName = 'VLR_UNITARIO'
      Origin = '"EST_PEDIDOS_ITEM"."VLR_UNITARIO"'
      DisplayFormat = '#,##0.00'
      Precision = 18
      Size = 3
    end
    object qurPedidoItemVLR_DESCONTO: TIBBCDField
      FieldName = 'VLR_DESCONTO'
      Origin = '"EST_PEDIDOS_ITEM"."VLR_DESCONTO"'
      DisplayFormat = '#,##0.00'
      Precision = 18
      Size = 2
    end
    object qurPedidoItemVLR_TOTAL: TIBBCDField
      FieldName = 'VLR_TOTAL'
      Origin = '"EST_PEDIDOS_ITEM"."VLR_TOTAL"'
      DisplayFormat = '#,##0.00'
      Precision = 18
      Size = 2
    end
    object qurPedidoItemMERC_DESCRICAO: TIBStringField
      FieldName = 'MERC_DESCRICAO'
      Origin = '"EST_MERCADORIAS"."DESCRICAO"'
      Size = 50
    end
    object qurPedidoItemUNIDADE: TIBStringField
      FieldName = 'UNIDADE'
      Origin = '"EST_MERCADORIAS"."UNIDADE"'
      Size = 2
    end
    object qurPedidoItemVLR_DESCONTO_ITEM: TIBBCDField
      FieldName = 'VLR_DESCONTO_ITEM'
      Origin = '"EST_PEDIDOS_ITEM"."VLR_DESCONTO_ITEM"'
      DisplayFormat = '#,##0.00'
      Precision = 18
      Size = 2
    end
  end
end
