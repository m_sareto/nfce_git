unit uFrmImportaPedido;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, IBCustomDataSet, IBQuery, ExtCtrls, Grids, DBGrids,
  StdCtrls, Buttons, ComCtrls;

type
  TfrmImportaPedido = class(TForm)
    Panel1: TPanel;
    grdDados: TDBGrid;
    pnlRodape: TPanel;
    btnCupom: TBitBtn;
    btnFechar: TBitBtn;
    dsPedido: TDataSource;
    qurPedido: TIBQuery;
    pnlCabecalho: TPanel;
    Label11: TLabel;
    edtCliente: TEdit;
    edtClienteNome: TEdit;
    Label34: TLabel;
    edtPedidoIni: TEdit;
    Label35: TLabel;
    edtPedidoFim: TEdit;
    Label30: TLabel;
    dtpInicial: TDateTimePicker;
    Label31: TLabel;
    dtpFinal: TDateTimePicker;
    Label1: TLabel;
    btnAtualizar: TBitBtn;
    qurPedidoID: TIntegerField;
    qurPedidoDATA: TDateField;
    qurPedidoCCUSTO: TIntegerField;
    qurPedidoVLR_PRODUTO: TIBBCDField;
    qurPedidoVLR_DESCONTO: TIBBCDField;
    qurPedidoVLR_TOTAL: TIBBCDField;
    qurPedidoSTATUS: TIBStringField;
    qurPedidoOBSERVACAO: TIBStringField;
    qurPedidoCLIENTE: TIBStringField;
    qurPedidoOPERACAO: TIBStringField;
    qurPedidoFORMA_PAG: TIBStringField;
    Panel3: TPanel;
    Panel2: TPanel;
    DBGrid1: TDBGrid;
    dsPedidoItem: TDataSource;
    qurPedidoItem: TIBQuery;
    qurPedidoItemID_PEDIDO: TIntegerField;
    qurPedidoItemITEM: TIntegerField;
    qurPedidoItemMERCADORIA: TIBStringField;
    qurPedidoItemQUANTIDADE: TIBBCDField;
    qurPedidoItemVLR_UNITARIO: TIBBCDField;
    qurPedidoItemVLR_DESCONTO: TIBBCDField;
    qurPedidoItemVLR_TOTAL: TIBBCDField;
    qurPedidoItemMERC_DESCRICAO: TIBStringField;
    qurPedidoItemUNIDADE: TIBStringField;
    Panel4: TPanel;
    qurPedidoCLIFOR: TIntegerField;
    qurPedidoEL_CHAVE: TIntegerField;
    qurPedidoItemVLR_DESCONTO_ITEM: TIBBCDField;
    procedure btnCupomClick(Sender: TObject);
    procedure btnFecharClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnAtualizarClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dsPedidoDataChange(Sender: TObject; Field: TField);
    procedure Label1Click(Sender: TObject);
    procedure edtClienteExit(Sender: TObject);
    procedure edtClienteKeyPress(Sender: TObject; var Key: Char);
    procedure edtPedidoIniKeyPress(Sender: TObject; var Key: Char);
    procedure edtPedidoFimKeyPress(Sender: TObject; var Key: Char);
    procedure edtClienteEnter(Sender: TObject);
  private
    { Private declarations }
    procedure filtrarPedidos();
    procedure filtrarPedidosItem();
  public
    { Public declarations }
  end;

var
  frmImportaPedido: TfrmImportaPedido;

implementation

uses uFuncoes, uDMCupomFiscal, uFrmPrincipal, uFrmLoja,
  uFrmPesquisaCliente, uRotinasGlobais, uDMComponentes;

{$R *.dfm}

procedure TfrmImportaPedido.btnCupomClick(Sender: TObject);
var Obs : string;
begin
  DMCupomFiscal.dbQuery1.Active:=False;
  DMCupomFiscal.dbQuery1.SQL.Clear;
  DMCupomFiscal.dbQuery1.SQL.Add('Select Count(i.id_nf) as xFlag FROM est_nf_item i, est_nf n'+
                                 ' Where n.id=i.id_nf and n.status=''X'' and i.id_pedido=:pID');
  DMCupomFiscal.dbQuery1.ParamByName('pID').AsInteger:=qurPedidoID.AsInteger;
  DMCupomFiscal.dbQuery1.Active:=True;
  DMCupomFiscal.dbQuery1.First;
  If DMCupomFiscal.dbQuery1.FieldByName('xFlag').AsInteger > 0 then
  begin
    msgAviso('Esta Pr�-Venda j� possui Nota Fiscal vinculada','');
  end
  else
  begin
    DMCupomFiscal.dbQuery1.Active:=False;
    DMCupomFiscal.dbQuery1.SQL.Clear;
    DMCupomFiscal.dbQuery1.SQL.Add('Select Count(*) as xFlag FROM Est_ECF Where EL_Chave=:pEL_Chave and status=''X''');
    DMCupomFiscal.dbQuery1.ParamByName('pEL_Chave').AsInteger:=qurPedidoEL_CHAVE.AsInteger;
    DMCupomFiscal.dbQuery1.Active:=True;
    DMCupomFiscal.dbQuery1.First;
    If DMCupomFiscal.dbQuery1.FieldByName('xFlag').AsInteger > 0 then
    begin
      msgAviso('Esta Pr�-Venda j� possui Cupom Fiscal vinculado','');
      DMCupomFiscal.dbQuery1.Active:=False;
    end
    else
    begin
      try
        try
          Panel4.Visible:=True;
          qurPedidoItem.First;
          While not(qurPedidoItem.Eof) do
          begin
            frmLoja.edtMercadoria.Text  := qurPedidoItemMERCADORIA.AsString;;
            frmLoja.reQuantidade.Value  := qurPedidoItemQUANTIDADE.AsCurrency;
            frmLoja.reVlrUnitario.Value := qurPedidoItemVLR_UNITARIO.AsCurrency;
            frmLoja.reVlrDesconto.Value := qurPedidoItemVLR_DESCONTO_ITEM.AsCurrency;
            frmLoja.reTotal.Value       := qurPedidoItemVLR_TOTAL.AsCurrency;
            frmLoja.ELChavePedido       := qurPedidoEL_CHAVE.AsInteger;
            frmLoja.edtMercadoriaExit(Self);
            frmLoja.subtotal;
            frmLoja.btnVende.Click;
            Application.ProcessMessages;
            qurPedidoItem.Next;
          end;
        finally
          Panel4.Visible:=False;
          frmLoja.ELChavePedido   := 0;
          //frmLoja.edtCliente.Text := qurPedidoCLIFOR.AsString;
          //frmLoja.edtClienteExit(Sender);
          frmImportaPedido.Visible:=False;   //Para n�o ficar aparecendo na tela quando executa cancelarCupom
          btnFechar.Click;
          Application.ProcessMessages;
        end
      except
        if cancelarCupomFiscal(True) then
          msgInformacao('Cupom Fiscal cancelado devido erros na importa��o!','');
      end;
    end;
  end;
end;

procedure TfrmImportaPedido.btnFecharClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmImportaPedido.FormShow(Sender: TObject);
begin
  dtpInicial.Date:=Date;
  dtpFinal.Date:=Date;
  filtrarPedidos;
end;

procedure TfrmImportaPedido.filtrarPedidos;
begin
  qurPedido.Active := False;
  qurPedido.SQL.Clear;
  {(select sum(i.vlr_desconto + i.vlr_desconto_item) vlr_desconto from est_pedidos_item i'+
   ' where i.id_pedido=p.id)}
  qurPedido.SQL.Add('Select p.id, p.el_chave, p.data, p.ccusto, p.vlr_produto, p.vlr_total,'+
                       ' p.status, p.observacao, p.clifor, p.vlr_desconto,'+
                       ' p.clifor||''/''||p.cf_nome cliente,'+
                       ' p.operacao||''/''||o.descricao operacao,'+
                       ' p.forma_pag||''/''||fp.descricao forma_pag from  est_pedidos p'+
                       ' left outer join clifor c on c.id=p.clifor'+
                       ' left outer join operacoes o on o.id=p.operacao'+
                       ' left outer join formas_pagamento fp on fp.id=p.forma_pag'+
                       ' where (p.id between :pPedidoI and :pPedidoF)'+
                       ' and (p.data between :pDataI and :pDataF)'+
                       ' and (p.status=''X'')');
  if (StrToIntDef(edtCliente.Text, 0) > 0) then
  begin
    qurPedido.SQL.Add(' and (p.clifor = :pCliente)');
    qurPedido.ParamByName('pCliente').AsInteger := StrToInt(edtCliente.Text);
  end;
  qurPedido.SQL.Add(' order by p.data desc, p.clifor');

  qurPedido.ParamByName('pPedidoI').AsInteger := StrToInt(edtPedidoIni.Text);
  qurPedido.ParamByName('pPedidoF').AsInteger := StrToInt(edtPedidoFim.Text);
  qurPedido.ParamByName('pDataI').AsDate  := dtpInicial.Date;
  qurPedido.ParamByName('pDataF').AsDate  := dtpFinal.Date;
  qurPedido.Active := True;
  qurPedido.First;
  if (qurPedido.Eof) then
  begin
    msgInformacao('Nenhum Pedido encontrado!','');
  end;
end;

procedure TfrmImportaPedido.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  qurPedido.Close;
end;

procedure TfrmImportaPedido.btnAtualizarClick(Sender: TObject);
begin
  filtrarPedidos;
end;

procedure TfrmImportaPedido.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  //Fechar
  if Key=vk_escape then btnFechar.Click;

  //Pesquisa cliente
  if (key=vk_F12) and (edtCliente.Focused) then Label1Click(Sender);
end;

procedure TfrmImportaPedido.filtrarPedidosItem;
begin
  qurPedidoItem.Active := False;
  qurPedidoItem.SQL.Clear;
  qurPedidoItem.SQL.Add('select i.*, m.descricao merc_descricao, m.unidade from est_pedidos_item i'+
                ' left outer join est_mercadorias m on m.id=i.mercadoria'+
                ' where i.id_pedido=:pID'+
                ' order by i.item');
  qurPedidoItem.ParamByName('pID').AsInteger := qurPedidoID.AsInteger;
  qurPedidoItem.Active := True;
  qurPedidoItem.First;
end;


procedure TfrmImportaPedido.dsPedidoDataChange(Sender: TObject;
  Field: TField);
begin
  filtrarPedidosItem;
end;

procedure TfrmImportaPedido.Label1Click(Sender: TObject);
begin
  frmPesquisaCliente := TfrmPesquisaCliente.Create(Self);
  try
    if (frmPesquisaCliente.ShowModal=mrOK) then
      edtCliente.Text:= FrmPesquisaCliente.qCliFor.FieldByName('ID').AsString;
  finally
    FrmPesquisaCliente.Free;
  end;
end;

procedure TfrmImportaPedido.edtClienteExit(Sender: TObject);
begin
  If Trim(edtCliente.Text)<>'' then
  begin
    DMCupomFiscal.dbQuery1.Active:=False;
    DMCupomFiscal.dbQuery1.SQL.Clear;
    DMCupomFiscal.dbQuery1.SQL.Add('Select Nome, TPreco, Limite FROM CliFor Where ID=:pID');
    DMCupomFiscal.dbQuery1.ParamByName('pID').AsInteger:=StrToIntDef(edtCliente.Text,0);
    DMCupomFiscal.dbQuery1.Active:=True;
    DMCupomFiscal.dbQuery1.First;
    If not(DMCupomFiscal.dbQuery1.Eof) then
    begin
      edtClienteNome.Text:=DMCupomFiscal.dbQuery1.FieldByName('Nome').AsString;
      filtrarPedidos;
    end
    Else
    begin
      msgAviso('Cliente n�o cadastrado','');
      edtCliente.SetFocus;
    end;
  end;
end;

procedure TfrmImportaPedido.edtClienteKeyPress(Sender: TObject;
  var Key: Char);
begin
  if not (somenteNumeros(Key)) then
    Key := #0; //Retorna nullo
end;
procedure TfrmImportaPedido.edtPedidoIniKeyPress(Sender: TObject;
  var Key: Char);
begin
  if not(somenteNumeros(Key)) then
    Key:= #0;
end;

procedure TfrmImportaPedido.edtPedidoFimKeyPress(Sender: TObject;
  var Key: Char);
begin
  if not(somenteNumeros(Key)) then
    Key:= #0;
end;

procedure TfrmImportaPedido.edtClienteEnter(Sender: TObject);
begin
  edtClienteNome.Text:='';
end;

end.
