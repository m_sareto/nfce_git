unit uFrmCheque;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Grids, DBGrids, Mask, StdCtrls, TREdit, DBCtrls,
  Buttons, Provider, DB, DBClient, ACBrBase, ACBrValidador,
  IBCustomDataSet, IBQuery;

type
  TfrmCheque = class(TForm)
    DBGrid2: TDBGrid;
    Panel1: TPanel;
    btFechar: TBitBtn;
    Panel2: TPanel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    Label5: TLabel;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    Label7: TLabel;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    btExcluir: TBitBtn;
    btSalva: TBitBtn;
    btNovo: TBitBtn;
    btCancelar: TBitBtn;
    Label9: TLabel;
    dsCheque_Total: TDataSource;
    dsCheque: TDataSource;
    DBEdit9: TDBEdit;
    qrCheque_Total: TIBQuery;
    qrCheque_TotalTOTAL: TIBBCDField;
    btEditar: TBitBtn;
    Image2: TImage;
    lblDescricaoValor1: TLabel;
    lblDia: TLabel;
    lblMes: TLabel;
    lblAno: TLabel;
    procedure btFecharClick(Sender: TObject);
    procedure btNovoClick(Sender: TObject);
    procedure btSalvaClick(Sender: TObject);
    procedure btExcluirClick(Sender: TObject);
    procedure DBEdit6Exit(Sender: TObject);
    procedure DBEdit4Exit(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure DBEdit8Exit(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure dsChequeStateChange(Sender: TObject);
    procedure GetCheque;
    procedure GetTotal;
    procedure btEditarClick(Sender: TObject);
    procedure DBEdit8Change(Sender: TObject);
    procedure dsChequeDataChange(Sender: TObject; Field: TField);
  private
    { Private declarations }
    procedure MostraDataPorExtenso();
  public
    { Public declarations }
  end;

var
  frmCheque: TfrmCheque;

implementation

uses uDMCupomFiscal, uFrmFim, uFrmPrincipal, uDMComponentes, uFuncoes,
  Math;

{$R *.dfm}

procedure TfrmCheque.btFecharClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmCheque.btNovoClick(Sender: TObject);
begin
  DMCupomFiscal.dbTemp_NFCe_FormaPag_Cheque.Open;
  DMCupomFiscal.dbTemp_NFCe_FormaPag_Cheque.Insert;
  dmCupomFiscal.dbTemp_NFCe_FormaPag_ChequeID.Value := 0;
  dmCupomFiscal.dbTemp_NFCe_FormaPag_ChequeCaixa.Value := FormatFloat('0000', FRENTE_CAIXA.Caixa);
  DMCupomFiscal.dbTemp_NFCe_FormaPag_ChequeDta_Vencimento.Value:=Date;
  DMCupomFiscal.dbTemp_NFCe_FormaPag_ChequeValor.Value:=0;

  DBEdit1.SetFocus;
end;

procedure TfrmCheque.btSalvaClick(Sender: TObject);
begin
  iF DMCupomFiscal.dbTemp_NFCe_FormaPag_Cheque.State in [dsInsert, dsEdit] then
  begin
    DMCupomFiscal.dbTemp_NFCe_FormaPag_Cheque.Post;
    DMCupomFiscal.Transaction.CommitRetaining;
    DMCupomFiscal.dbTemp_NFCe_FormaPag_Cheque.Close;
    GetCheque;
    GetTotal;
  end;
  btNovo.SetFocus;
end;

procedure TfrmCheque.btEditarClick(Sender: TObject);
begin
  if not(dmCupomFiscal.dbTemp_NFCe_FormaPag_Cheque.eof) then
  begin
    DMCupomFiscal.dbTemp_NFCe_FormaPag_Cheque.Open;
    DMCupomFiscal.dbTemp_NFCe_FormaPag_Cheque.Edit;
    DBEdit1.SetFocus;
  end;
end;

procedure TfrmCheque.btExcluirClick(Sender: TObject);
begin
  If(msgPergunta('Excluir registro selecionado?',Application.Title))then
  begin
    DMCupomFiscal.dbTemp_NFCe_FormaPag_Cheque.Delete;
    DMCupomFiscal.Transaction.CommitRetaining;
    DMCupomFiscal.dbTemp_NFCe_FormaPag_Cheque.Close;
    GetCheque;
    GetTotal;
  end;
  btNovo.SetFocus;
end;

procedure TfrmCheque.DBEdit6Exit(Sender: TObject);
var
  Txt : string;
begin
  if not (dmCupomFiscal.dbTemp_NFCe_FormaPag_Cheque.State in [dsInsert,dsEdit])then abort;
  if(Trim(DBEdit6.Text) = '')then
  begin
    msgAviso('CNPJ/CPF informado � inv�lido',application.title);
    DBEdit6.SetFocus;
    Abort;
  end;

  Txt:=limpaString(DBEdit6.Text);
  If Length(Txt)>11 then   //CNPJ
    dmComponentes.ACBrValidador1.TipoDocto := docCNPJ
  Else
    dmComponentes.ACBrValidador1.TipoDocto := docCPF;

  dmComponentes.ACBrValidador1.Documento   := Trim(DBEdit6.Text);
  dmComponentes.ACBrValidador1.Complemento := '';
  if not (dmComponentes.ACBrValidador1.Validar) then
  begin
    DBEdit6.SetFocus;
    msgInformacao(dmComponentes.ACBrValidador1.MsgErro, 'Aten��o');
  end
  else
  begin
    dmCupomFiscal.dbTemp_NFCe_FormaPag_ChequeCNPJCPF.Value := dmComponentes.ACBrValidador1.Formatar;
    DBEdit6.Text := dmComponentes.ACBrValidador1.Formatar;
  end;
  DMCupomFiscal.dbQuery1.Active:=False;
  DMCupomFiscal.dbQuery1.SQL.Clear;
  DMCupomFiscal.dbQuery1.SQL.Add('Select Count(*) as Vezes From Ch_Movimento Where (Situacao=2) and (CNPJCPF=:pCNPJCPF)');
  DMCupomFiscal.dbQuery1.ParamByName('pCNPJCPF').AsString:=Trim(DBEdit6.Text);
  DMCupomFiscal.dbQuery1.Active:=True;
  DMCupomFiscal.dbQuery1.First;
  if DMCupomFiscal.dbQuery1.FieldByName('Vezes').AsInteger>0 then
    ShowMessage('<<  A T E N � � O  >>'+#13+'Este CNPJ/CPF Possue Cheque(s) DEVOLVIDOS: '+
  DMCupomFiscal.dbQuery1.FieldByName('Vezes').AsString);
end;

procedure TfrmCheque.DBEdit4Exit(Sender: TObject);
begin
  DMCupomFiscal.dbQuery1.Active:=False;
  DMCupomFiscal.dbQuery1.SQL.Clear;
  DMCupomFiscal.dbQuery1.SQL.Add('Select Count(*) as Vezes From Ch_Movimento Where (Banco=:pBanco) and (Agencia=:pAgencia) and (Conta=:pConta) and (Cheque=:pCheque)');
  DMCupomFiscal.dbQuery1.ParamByName('pBanco').AsInteger:=StrToIntDef(DBEdit1.Text,0);
  DMCupomFiscal.dbQuery1.ParamByName('pAgencia').AsString:=Trim(DBEdit2.Text);
  DMCupomFiscal.dbQuery1.ParamByName('pConta').AsString:=Trim(DBEdit3.Text);
  DMCupomFiscal.dbQuery1.ParamByName('pCheque').AsString:=Trim(DBEdit4.Text);
  DMCupomFiscal.dbQuery1.Active:=True;
  DMCupomFiscal.dbQuery1.First;
  if DMCupomFiscal.dbQuery1.FieldByName('Vezes').AsInteger>0 then
  begin
    ShowMessage('Este Cheque j� se encontra em nossa base de dados.');
    DBEdit1.SetFocus;
  End;
end;

procedure TfrmCheque.btCancelarClick(Sender: TObject);
begin
  DMCupomFiscal.dbTemp_NFCe_FormaPag_Cheque.Cancel;
  DMCupomFiscal.Transaction.RollbackRetaining;
  DMCupomFiscal.dbTemp_NFCe_FormaPag_Cheque.Close;
  GetCheque;
  GetTotal;
  btNovo.SetFocus;
end;

procedure TfrmCheque.DBEdit8Change(Sender: TObject);
var
  lSaldo: String;
begin
  lSaldo := DBEdit8.Text;
  lSaldo := StringReplace(lSaldo,'.','',[rfReplaceAll]);
  lblDescricaoValor1.Visible := ((lSaldo <> '0') and (lSaldo <> ''));
  if(lblDescricaoValor1.Visible)Then
    lblDescricaoValor1.Caption := valorExtenso(StrToCurr(lSaldo));
end;

procedure TfrmCheque.DBEdit8Exit(Sender: TObject);
begin
  If DMCupomFiscal.dbTemp_NFCe_FormaPag_ChequeValor.Value<=0 then
  begin
    ShowMessage('Valor deve ser Maior que Zero.');
    DBEdit8.SetFocus;
  end;
end;

procedure TfrmCheque.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  If (Key=vk_escape) then Close;
end;

procedure TfrmCheque.FormShow(Sender: TObject);
begin
  GetCheque; 
  GetTotal;
end;

procedure TfrmCheque.dsChequeDataChange(Sender: TObject; Field: TField);
begin
  MostraDataPorExtenso;
end;

procedure TfrmCheque.dsChequeStateChange(Sender: TObject);
begin
  btNovo.Enabled    := not(DMCupomFiscal.dbTemp_NFCe_FormaPag_Cheque.State in [dsInsert,dsEdit]);
  btSalva.Enabled   := (DMCupomFiscal.dbTemp_NFCe_FormaPag_Cheque.State in [dsInsert,dsEdit]);
  btExcluir.Enabled := not(DMCupomFiscal.dbTemp_NFCe_FormaPag_Cheque.State in [dsInsert,dsEdit])
                       and(DMCupomFiscal.dbTemp_NFCe_FormaPag_Cheque.State in [dsBrowse])
                       and not(DMCupomFiscal.dbTemp_NFCe_FormaPag_Cheque.eof);
  btEditar.Enabled  := not(DMCupomFiscal.dbTemp_NFCe_FormaPag_Cheque.State in [dsInsert,dsEdit])
                       and(DMCupomFiscal.dbTemp_NFCe_FormaPag_Cheque.State in [dsBrowse])
                       and not(DMCupomFiscal.dbTemp_NFCe_FormaPag_Cheque.eof);
  btCancelar.Enabled:= (DMCupomFiscal.dbTemp_NFCe_FormaPag_Cheque.State in [dsInsert,dsEdit]);
  btFechar.Enabled  := not(DMCupomFiscal.dbTemp_NFCe_FormaPag_Cheque.State in [dsInsert,dsEdit]);
end;

procedure TfrmCheque.GetCheque;
begin
  dmCupomFiscal.dbTemp_NFCe_FormaPag_Cheque.Close;
  dmCupomFiscal.dbTemp_NFCe_FormaPag_Cheque.ParamByName('pCaixa').asString  := FormatFloat('0000', FRENTE_CAIXA.Caixa);
  dmCupomFiscal.dbTemp_NFCe_FormaPag_Cheque.Open;
end;

procedure TfrmCheque.GetTotal;
begin
  qrCheque_Total.Active := false;
  qrCheque_Total.ParamByName('pCaixa').asString  := FormatFloat('0000', FRENTE_CAIXA.Caixa);
  qrCheque_Total.Active := True;
end;

procedure TfrmCheque.MostraDataPorExtenso;
begin
  if(dmCupomFiscal.dbTemp_NFCe_FormaPag_Cheque.State in [dsEdit, dsInsert])then
  begin
    lblDia.Visible := True;
    lblDia.Caption := FormatDateTime('dd',dmCupomFiscal.dbTemp_NFCe_FormaPag_ChequeDTA_VENCIMENTO.AsDateTime);
    lblMes.Visible := True;
    lblMes.Caption := MesExtenso(dmCupomFiscal.dbTemp_NFCe_FormaPag_ChequeDTA_VENCIMENTO.AsDateTime);
    lblAno.Visible := True;
    lblAno.Caption := FormatDateTime('yyyy', dmCupomFiscal.dbTemp_NFCe_FormaPag_ChequeDTA_VENCIMENTO.AsDateTime);
  end
  else
  begin
    lblDia.Visible := False;
    lblMes.Visible := False;
    lblAno.Visible := False;
  end;
end;

end.
