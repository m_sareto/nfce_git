unit uFrmPesquisaMercadoria;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, Grids, DBGrids, DB, IBCustomDataSet, IBQuery,
  ComCtrls, jpeg, ExtCtrls;

type
  TfrmPesquisaMercadoria = class(TForm)
    DataSource1: TDataSource;
    qMercadoria: TIBQuery;
    DBGrid1: TDBGrid;
    GroupBox1: TGroupBox;
    Edit1: TEdit;
    StatusBar1: TStatusBar;
    chkIniciarPor: TCheckBox;
    qMercadoriaID: TIBStringField;
    qMercadoriaEST_ATUAL: TIBBCDField;
    qMercadoriaVENDA: TIBBCDField;
    qMercadoriaCUSTO_ULTIMO: TIBBCDField;
    qMercadoriaDESCRICAO: TIBStringField;
    qMercadoriaFABRICANTE_DESC: TIBStringField;
    procedure FormShow(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGrid1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Edit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGrid1Enter(Sender: TObject);
    procedure Edit1Change(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    constructor Create(AOwner: TComponent);
    reintroduce;
  end;

var
  frmPesquisaMercadoria: TfrmPesquisaMercadoria;

implementation

uses Math, uDMCupomFiscal, uFrmLoja, uFrmSupermercado, uFrmPrincipal,
  uFuncoes, uRotinasGlobais;

{$R *.dfm}
constructor TfrmPesquisaMercadoria.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
end;

procedure TfrmPesquisaMercadoria.FormShow(Sender: TObject);
begin
  Edit1.SetFocus;
  chkIniciarPor.Checked := FRENTE_CAIXA.IniciePor;
end;

procedure TfrmPesquisaMercadoria.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 case Key of
    vk_Escape: ModalResult := mrCancel;
    vk_F12: FrmPesquisaMercadoria.Edit1.SetFocus;
  end;
end;

procedure TfrmPesquisaMercadoria.DBGrid1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (key = vk_Return) and not (DataSource1.DataSet.IsEmpty) then
    ModalResult := mrOK;

  if key=VK_F1 then
    msgInformacao('Valor do �ltimo Custo R$: '+FormatFloat('###,##0.00',qMercadoria.FieldByName('Custo_Ultimo').AsCurrency),'');
end;

procedure TfrmPesquisaMercadoria.Edit1Change(Sender: TObject);
begin
  if Trim(Edit1.Text)<>'' then
  begin
    qMercadoria.Active:=False;
    qMercadoria.SQL.Clear;
    qMercadoria.SQL.Add('select M.Id, Trim(M.Descricao) Descricao, Me.Est_Atual, M.Custo_Ultimo,'+
              ' (Case when ((:pDataE >= M.SM_DtE_Promocao) and (:pDataS <= M.SM_DtS_Promocao)) then'+
              ' M.Sm_Venda_Promocao'+
              ' Else'+
              ' M.Venda'+
              ' end) as venda,  F.ID||''-''||f.descricao as Fabricante_Desc'+
              ' From Est_MERCADORIAS M'+
              ' Left Outer Join Est_Mercadorias_Estoque Me on Me.Mercadoria=M.Id and Me.CCusto=:pCC'+
              ' Left Outer Join fabricantes f on f.id = m.fabricante');
    if chkIniciarPor.Checked then
    begin
      qMercadoria.SQL.Add(' Where (M.Descricao >= :pDes)');
      qMercadoria.ParamByName('pDes').AsString:= Trim(Edit1.Text);
    end
    else
    begin
      qMercadoria.SQL.Add(' Where (M.Descricao Like :pDes)');
      qMercadoria.ParamByName('pDes').AsString:= '%'+Trim(Edit1.Text)+'%';
    end;
    qMercadoria.SQL.Add(' and (me.ccusto is not null) and (M.Ativo=''S'') Order By M.Descricao');
    qMercadoria.ParamByName('pCC').AsInteger:= CCUSTO.codigo;
    qMercadoria.ParamByName('pDataE').AsDate:=Date;
    qMercadoria.ParamByName('pDataS').AsDate:=Date;
    qMercadoria.Active:=True;
    qMercadoria.First;
  end;
end;

procedure TfrmPesquisaMercadoria.Edit1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (key in [vk_Down, vk_Up]) and not (DataSource1.DataSet.IsEmpty) then
  DBGrid1.SetFocus;
end;

procedure TfrmPesquisaMercadoria.DBGrid1Enter(Sender: TObject);
begin
  {if Trim(Edit1.Text)<>'' then
  begin
    qMercadoria.Active:=False;
    qMercadoria.SQL.Clear;
    qMercadoria.SQL.Add('select M.Id, Trim(M.Descricao) Descricao, Me.Est_Atual, M.Custo_Ultimo,'+
              ' (Case when ((:pDataE >= M.SM_DtE_Promocao) and (:pDataS <= M.SM_DtS_Promocao)) then'+
              ' M.Sm_Venda_Promocao'+
              ' Else'+
              ' M.Venda'+
              ' end) as venda'+
              ' From Est_MERCADORIAS M'+
              ' Left Outer Join Est_Mercadorias_Estoque Me on Me.Mercadoria=M.Id and Me.CCusto=:pCC');
    if chkIniciarPor.Checked then
    begin
      qMercadoria.SQL.Add(' Where (M.Descricao >= :pDes)');
      qMercadoria.ParamByName('pDes').AsString:= Trim(Edit1.Text);
    end
    else
    begin
      qMercadoria.SQL.Add(' Where (M.Descricao Like :pDes)');
      qMercadoria.ParamByName('pDes').AsString:= '%'+Trim(Edit1.Text)+'%';
    end;
    qMercadoria.SQL.Add(' and (me.ccusto is not null) and (M.Ativo=''S'') Order By M.Descricao');
    qMercadoria.ParamByName('pCC').AsInteger:= CCUSTO.codigo;
    qMercadoria.ParamByName('pDataE').AsDate:=Date;
    qMercadoria.ParamByName('pDataS').AsDate:=Date;
    qMercadoria.Active:=True;
    qMercadoria.First;
  end;}
end;

end.



