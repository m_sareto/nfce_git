object frmFechamentoAbastecidas: TfrmFechamentoAbastecidas
  Left = 361
  Top = 48
  BorderIcons = [biSystemMenu]
  Caption = 'Fechamento Di'#225'rio dos Abastecimentos'
  ClientHeight = 470
  ClientWidth = 581
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDesktopCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 43
    Width = 581
    Height = 427
    ActivePage = TabSheet2
    Align = alClient
    TabOrder = 1
    object TabSheet1: TTabSheet
      Caption = '   LMC   '
      object grdLMC: TDBGrid
        Left = 0
        Top = 0
        Width = 573
        Height = 371
        Align = alClient
        Color = clWhite
        Ctl3D = False
        DataSource = dsLMC
        DefaultDrawing = False
        DrawingStyle = gdsClassic
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
        ParentCtl3D = False
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnDrawColumnCell = grdLMCDrawColumnCell
        OnDblClick = grdLMCDblClick
        OnKeyDown = grdLMCKeyDown
        Columns = <
          item
            Expanded = False
            FieldName = 'Mercadoria'
            Title.Caption = 'C'#243'digo'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clBlack
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 48
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Descricao'
            Title.Caption = 'Descri'#231#227'o da Mercadoria'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clBlack
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Qtd_Venda'
            Title.Alignment = taCenter
            Title.Caption = 'Volume LT ECF'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clBlack
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 91
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Qtd_Leitura'
            Title.Alignment = taCenter
            Title.Caption = 'Volume LT LMC'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clBlack
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 97
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Qtd_Diferenca'
            Title.Alignment = taCenter
            Title.Caption = 'Volume LT Diferen'#231'a'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clBlack
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 124
            Visible = True
          end>
      end
      object Panel2: TPanel
        Left = 0
        Top = 371
        Width = 573
        Height = 28
        Align = alBottom
        ParentBackground = False
        TabOrder = 1
        DesignSize = (
          573
          28)
        object Label2: TLabel
          Left = 9
          Top = 7
          Width = 34
          Height = 13
          Caption = '[ OK ]'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clLime
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label3: TLabel
          Left = 55
          Top = 7
          Width = 124
          Height = 13
          Caption = '[ Falta Cupom Fiscal ]'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label4: TLabel
          Left = 194
          Top = 7
          Width = 220
          Height = 13
          Caption = '[ Venda Cupom Fiscal maior que LMC ]'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlue
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object btCupom_LMC: TBitBtn
          Left = 466
          Top = 1
          Width = 93
          Height = 25
          Anchors = [akTop, akRight]
          Caption = 'Emitir Cupom'
          Glyph.Data = {
            F6010000424DF601000000000000F60000002800000010000000100000000100
            08000000000000010000120B0000120B00003000000030000000418020004180
            260041882600579141006F9A5E004F883A004888330077AD660041882D005791
            4800579A480048913A004F9141006FAD660041883A003A773300488841004F91
            4800579A4F005EA4570033802D003A883300489A41004F9A48003A883A004191
            410057A457004880480066A4660077AD7700ADCBAD00BAD2BA00D5E0D500579A
            5E0080B788005EA46F006FAD800077B7880088C19A006FB7880077B791006FB7
            910088C1A40026CC800078E9C200FFFFFF00FFFFFF00000000002E2E2E2E2E2E
            2E2E2E2E2E2E2E2E2E2E2E2E2E2E20070201010207202E2E2E2E2E2E2E07080A
            130D0D130A08072E2E2E2E2E070B131E1E1518161A130B072E2E2E200812191E
            2D1E0E0E19171308202E2E070C11111E2D2D1E1111171203072E2E011414141E
            2D2D2D1E14141013012E2E000F0F0F1E2D2D2D2D1F0F0F04002E2E061B1B1B1E
            2D2D2D2D2D2C1B1D012E2E062121211F2D2D2D2D2B21211D062E2E071223231F
            2D2D2D2B23232323072E2E200624271F2D2D2B2727272706202E2E2E0709252B
            2D2B2929282809072E2E2E2E2E07062B2B262A221C05072E2E2E2E2E2E2E2007
            0501010507202E2E2E2E2E2E2E2E2E2E2E2E2E2E2E2E2E2E2E2E}
          TabOrder = 0
          OnClick = btCupom_LMCClick
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = '   Concentrador   '
      ImageIndex = 1
      object grdConcentrador: TDBGrid
        Left = 0
        Top = 0
        Width = 573
        Height = 348
        Hint = 
          'Para selecionar os itens podem ser usados uma das seguintes op'#231#245 +
          'es:'#13#10'-Selecione um registro e pressione a tecla enter;'#13#10'-Selecio' +
          'ne um registro e pressione a tecla espa'#231'o;'#13#10'-pressione o titulo ' +
          'da primeira coluna para selecionar ou inverter a sele'#231#227'o;'#13#10#13#10'Reg' +
          'istros ja inseridos no NFCe n'#227'o poder'#227'o ser desmarcados. Para ta' +
          'l, exclua o mesmo do NFCe pendente.'
        Align = alClient
        Ctl3D = True
        DataSource = dsConcentrador
        DrawingStyle = gdsClassic
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
        ParentCtl3D = False
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnDrawColumnCell = grdConcentradorDrawColumnCell
        OnKeyDown = grdConcentradorKeyDown
        OnMouseUp = grdConcentradorMouseUp
        OnTitleClick = grdConcentradorTitleClick
        Columns = <
          item
            Expanded = False
            FieldName = 'Flag'
            Title.Alignment = taCenter
            Title.Caption = '*'
            Width = 24
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Mercadoria'
            Width = 65
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Descricao'
            Title.Alignment = taCenter
            Title.Caption = 'Descri'#231#227'o da Mercadoria'
            Width = 208
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Quantidade'
            Title.Alignment = taCenter
            Width = 75
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Valor'
            Title.Alignment = taCenter
            Width = 79
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Total'
            Title.Alignment = taCenter
            Width = 80
            Visible = True
          end>
      end
      object Panel3: TPanel
        Left = 0
        Top = 371
        Width = 573
        Height = 28
        Align = alBottom
        Color = clSilver
        ParentBackground = False
        TabOrder = 1
        DesignSize = (
          573
          28)
        object Label5: TLabel
          Left = 17
          Top = 7
          Width = 167
          Height = 13
          Caption = '* Selecionar todos/Inverter sele'#231#227'o'
        end
        object btCupom_Concentrador: TBitBtn
          Left = 442
          Top = 2
          Width = 93
          Height = 24
          Anchors = [akTop, akRight]
          Caption = 'Emitr Cupom'
          Enabled = False
          Glyph.Data = {
            F6010000424DF601000000000000F60000002800000010000000100000000100
            08000000000000010000120B0000120B00003000000030000000418020004180
            260041882600579141006F9A5E004F883A004888330077AD660041882D005791
            4800579A480048913A004F9141006FAD660041883A003A773300488841004F91
            4800579A4F005EA4570033802D003A883300489A41004F9A48003A883A004191
            410057A457004880480066A4660077AD7700ADCBAD00BAD2BA00D5E0D500579A
            5E0080B788005EA46F006FAD800077B7880088C19A006FB7880077B791006FB7
            910088C1A40026CC800078E9C200FFFFFF00FFFFFF00000000002E2E2E2E2E2E
            2E2E2E2E2E2E2E2E2E2E2E2E2E2E20070201010207202E2E2E2E2E2E2E07080A
            130D0D130A08072E2E2E2E2E070B131E1E1518161A130B072E2E2E200812191E
            2D1E0E0E19171308202E2E070C11111E2D2D1E1111171203072E2E011414141E
            2D2D2D1E14141013012E2E000F0F0F1E2D2D2D2D1F0F0F04002E2E061B1B1B1E
            2D2D2D2D2D2C1B1D012E2E062121211F2D2D2D2D2B21211D062E2E071223231F
            2D2D2D2B23232323072E2E200624271F2D2D2B2727272706202E2E2E0709252B
            2D2B2929282809072E2E2E2E2E07062B2B262A221C05072E2E2E2E2E2E2E2007
            0501010507202E2E2E2E2E2E2E2E2E2E2E2E2E2E2E2E2E2E2E2E}
          TabOrder = 0
          OnClick = btCupom_ConcentradorClick
        end
        object chkSelecionarTodos: TCheckBox
          Left = 312
          Top = 6
          Width = 107
          Height = 17
          Caption = 'Selecionar Todos'
          TabOrder = 1
          Visible = False
          OnClick = chkSelecionarTodosClick
        end
      end
      object Panel4: TPanel
        Left = 189
        Top = 152
        Width = 185
        Height = 33
        Caption = 'Aguarde!  Imprimindo Item...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 2
        Visible = False
      end
      object Panel6: TPanel
        Left = 0
        Top = 348
        Width = 573
        Height = 23
        Align = alBottom
        TabOrder = 3
        object reTotal: TRealEdit
          Left = 472
          Top = 0
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatReal = fNumber
          FormatSize = '10.3'
        end
        object reUnitario: TRealEdit
          Left = 393
          Top = 0
          Width = 79
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatReal = fNumber
          FormatSize = '10.3'
        end
        object reQuantidade: TRealEdit
          Left = 318
          Top = 0
          Width = 75
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatReal = fNumber
          FormatSize = '10.3'
        end
      end
    end
    object TabSheet3: TTabSheet
      Caption = '   Leitura dos Encerrantes'
      ImageIndex = 2
      object grdEncerrantes: TDBGrid
        Left = 0
        Top = 230
        Width = 573
        Height = 135
        Align = alTop
        DataSource = dsCdsEncerrante
        DrawingStyle = gdsClassic
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Merc_Descricao'
            Title.Caption = 'Combust'#237'vel'
            Width = 200
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Saida_ECF'
            Title.Alignment = taCenter
            Title.Caption = 'Vol Sa'#237'da ECF'
            Width = 122
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Saida_Abastecimento'
            Title.Alignment = taCenter
            Title.Caption = 'Vol Sa'#237'da Abastecida'
            Width = 140
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Diferenca'
            Title.Alignment = taCenter
            Title.Caption = 'Diferen'#231'a'
            Width = 84
            Visible = True
          end>
      end
      object Panel5: TPanel
        Left = 0
        Top = 206
        Width = 573
        Height = 24
        Align = alTop
        BevelInner = bvLowered
        BevelOuter = bvSpace
        Caption = 'Diferen'#231'a de venda por Mercadoria'
        Color = clWhite
        TabOrder = 1
      end
      object DBGrid1: TDBGrid
        Left = 0
        Top = 0
        Width = 573
        Height = 206
        Align = alTop
        DataSource = dsEncerrante
        DrawingStyle = gdsClassic
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
        TabOrder = 2
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnCellClick = DBGrid1CellClick
        Columns = <
          item
            Expanded = False
            FieldName = 'MERCADORIA'
            Title.Caption = 'Mercadoria'
            Width = 67
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DESCRICAO'
            Title.Caption = 'Combust'#237'vel'
            Width = 200
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'BOMBA'
            Title.Alignment = taCenter
            Title.Caption = 'Bico'
            Width = 37
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'INI_ENCERRANTE'
            Title.Alignment = taCenter
            Title.Caption = 'Leitura Inicial'
            Width = 121
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'FIM_ENCERRANTE'
            Title.Alignment = taCenter
            Title.Caption = 'Leitura Final'
            Width = 119
            Visible = True
          end>
      end
      object chbUltimaLeitEncerrante: TCheckBox
        Left = 3
        Top = 382
        Width = 369
        Height = 17
        Caption = 
          #218'ltima leitura encerrante de cada combust'#237'vel sem condi'#231#227'o de Da' +
          'ta'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 3
      end
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 581
    Height = 43
    Align = alTop
    BorderStyle = bsSingle
    ParentBackground = False
    TabOrder = 0
    DesignSize = (
      577
      39)
    object Label1: TLabel
      Left = 17
      Top = 13
      Width = 23
      Height = 13
      Caption = 'Data'
      Color = clBlack
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentColor = False
      ParentFont = False
      Transparent = True
    end
    object mkData: TMaskEdit
      Left = 50
      Top = 10
      Width = 60
      Height = 19
      Ctl3D = False
      EditMask = '!99/99/00;1;_'
      MaxLength = 8
      ParentCtl3D = False
      TabOrder = 0
      Text = '  /  /  '
    end
    object btFecha: TBitBtn
      Left = 473
      Top = 8
      Width = 77
      Height = 23
      Anchors = [akTop, akRight]
      Caption = 'Fechar'
      Glyph.Data = {
        36060000424D3606000000000000360000002800000020000000100000000100
        18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFECF2F6CCDBE8A5C1D680A7
        C56394B8165E931D6397FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFF1F1F1DADADABEBEBEA3A3A38E8E8E5656565B5B5B999999717171
        5454545151514F4F4F4C4C4C4A4A4A47474745454525679D3274A83D7CAF4784
        B54E8ABA3E7EAD2065989999997171715454545151514F4F4F4C4C4C4A4A4A47
        47474545456262626E6E6E7777777F7F7F8585857777775D5D5DFFFFFFFFFFFF
        585858A2A2A2A2A2A2A3A3A3A4A4A4A4A4A4A5A5A52F6FA578ABD278ABD373A7
        D169A0CD407FAE23679AFFFFFFFFFFFF585858A2A2A2A2A2A2A3A3A3A4A4A4A4
        A4A4A5A5A56B6B6BA6A6A6A7A7A7A3A3A39C9C9C797979606060FFFFFFFFFFFF
        5C5C5CA1A1A13C7340A0A1A1A3A3A3A3A3A3A4A4A43674AA7DAFD45B9AC95495
        C75896C84180AE26699DFFFFFFFFFFFF5C5C5CA1A1A1535353A1A1A1A3A3A3A3
        A3A3A4A4A4717171AAAAAA9494948F8F8F919191797979636363FFFFFFFFFFFF
        606060A0A0A03D7641367139A2A2A2A2A2A2A3A3A33D79B082B3D7629FCC5A9A
        C95E9BCA4381AF2C6DA0FFFFFFFFFFFF606060A0A0A05555554E4E4EA2A2A2A2
        A2A2A3A3A3777777AEAEAE9999999393939595957B7B7B68686837823E347E3B
        3179372E7534499150468F4C39733DA1A1A1A2A2A2457EB488B7D967A3CF619E
        CC639FCC4583B13171A45656565353534F4F4F4C4C4C676767646464515151A1
        A1A1A2A2A27D7D7DB2B2B29D9D9D9898989999997D7D7D6C6C6C3B874289CB92
        84C88D80C6887BC38377C17F478F4D3B743FA1A1A14C84BA8DBBDB6EA8D166A6
        D15FB4DF4785B13775A95B5B5BA5A5A5A1A1A19E9E9E99999996969665656553
        5353A1A1A1838383B5B5B5A1A1A19E9E9EA3A3A37E7E7E7171713E8B468FCE99
        7DC68778C38173C07C74C07C79C28149904F547F575489BF94BFDD75ADD463B8
        E14BD4FF428BB83D7AAD5E5E5EAAAAAA9C9C9C98989894949494949498989867
        6767666666898989BABABAA6A6A6A6A6A6AEAEAE80808076767641904A94D29F
        91D09A8DCD9689CB9284C88D519858417C469F9F9F5A8EC498C3E07CB3D774AF
        D65EC4ED4B88B3457FB2626262AFAFAFACACACA8A8A8A5A5A5A1A1A16F6F6F5A
        5A5A9F9F9F8F8F8FBDBDBDABABABA7A7A7ACACAC8181817C7C7C44944D42914B
        3F8D483D89455DA4655AA06145834B9E9E9E9E9E9E6092C99EC7E283B8DA7DB4
        D77EB3D74F89B44B84B76666666363636060605D5D5D7B7B7B7777775F5F5F9E
        9E9E9E9E9E949494C1C1C1B0B0B0ACACACACACAC838383828282FFFFFFFFFFFF
        7777779A9A9A3D8A45498A4F9C9C9C9D9D9D9D9D9D6696CCA2CBE389BDDC83B9
        DA84B9DA518BB55289BCFFFFFFFFFFFF7777779A9A9A5D5D5D6464649C9C9C9D
        9D9D9D9D9D989898C4C4C4B5B5B5B1B1B1B1B1B1858585888888FFFFFFFFFFFF
        7A7A7A999999529159999A999B9B9B9C9C9C9C9C9C6C9AD0A7CEE58FC1DF89BD
        DC8BBDDC538DB65A8EC2FFFFFFFFFFFF7A7A7A9999996D6D6D9999999B9B9B9C
        9C9C9C9C9C9D9D9DC8C8C8B9B9B9B5B5B5B5B5B58686868E8E8EFFFFFFFFFFFF
        7D7D7D9999999999999A9A9A9A9A9A9B9B9B9B9B9B6F9DD3AAD1E7ABD1E798C7
        E191C2DE568FB76093C6FFFFFFFFFFFF7D7D7D9999999999999A9A9A9A9A9A9B
        9B9B9B9B9BA0A0A0CACACACBCBCBBFBFBFBABABA888888939393FFFFFFFFFFFF
        8080807E7E7E7C7C7C7A7A7A777777757575727272719ED46F9ED687B2DCABD3
        E8A9D0E65890B86797CBFFFFFFFFFFFF8080807E7E7E7C7C7C7A7A7A77777775
        7575727272A2A2A2A2A2A2B2B2B2CBCBCBC9C9C98A8A8A989898FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF84ACDC6D9C
        D485B1DA5A91B96D9CCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFAFAFAFA0A0A0B0B0B08B8B8B9E9E9EFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFB1CAE86C9CD3709ED2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCCCCCC9F9F9FA1A1A1}
      NumGlyphs = 2
      TabOrder = 2
      OnClick = btFechaClick
    end
    object btPesquisa: TBitBtn
      Left = 125
      Top = 8
      Width = 77
      Height = 23
      Caption = 'Pesquisar'
      Glyph.Data = {
        36060000424D3606000000000000360000002800000020000000100000000100
        18000000000000060000120B0000120B00000000000000000000FFFFFFCDCDCD
        AAAAAAF5F5F5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFCDCDCDAAAAAAF5F5F5FFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0B9B9
        C6C6C6A3A3A3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFBBBBBBC6C6C6A3A3A3FFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE3DFDF
        D2C6C6A9AAAAC2C2C2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFDFDFDFC8C8C8AAAAAAC2C2C2FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        DED9D9D6CFCF909191E4E4E4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDADADAD0D0D0919191E4E4E4FFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFDDD8D8CFCCCC8B8B8BF9F9F9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD9D9D9CDCDCD8B8B8BF9F9F9FF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFE1DDDDBBB9B99D9D9EC2C2C2A1A1A1989898A2A2A2DDDDDDFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDDDDDDB9B9B99D9D9DC2
        C2C2A1A1A1989898A2A2A2DDDDDDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFC0BDBEB2B2B0DEDBD1F8F8ECFAF9F3E3E1DE969594A6A6
        A6FDFDFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBEBEBEB1B1B1D8
        D8D8F4F4F4F7F7F7E0E0E0959595A6A6A6FDFDFDFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFF8F8F8B4B2B0F6ECE0FFFEF2FFFFF7FFFFFAFFFFFDFFFFFFA5A2
        A1B2B3B3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8F8F8B2B2B2E9E9E9F9
        F9F9FCFCFCFDFDFDFEFEFEFFFFFFA2A2A2B3B3B3FFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFC0C1C1E3D2C7FFF5E9FFFCECFFFCF3FFFDF6FFFEF7FFFFFBFAF0
        F1808180F2F2F2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0C0C0D1D1D1F2F2F2F6
        F6F6F9F9F9FBFBFBFCFCFCFDFDFDF2F2F2808080F2F2F2FFFFFFFFFFFFFFFFFF
        FFFFFFFEFEFEC1BEBCF2DDCEFFF6E3FFFAEAFFFAEFFFFBF3FFFCF5FFFEF7FFFC
        F9AAA5A5CBCBCBFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEBEBEBEDBDBDBF0F0F0F4
        F4F4F7F7F7F9F9F9FAFAFAFCFCFCFBFBFBA6A6A6CBCBCBFFFFFFFFFFFFFFFFFF
        FFFFFFF9F9F9C1BAB5F6E1D0FFF5E1FFF7E7FFF8EDFFF9F1FFFAF2FFFCF4FFFB
        F4C0B7B4BBBBBBFFFFFFFFFFFFFFFFFFFFFFFFF9F9F9BABABADEDEDEEFEFEFF2
        F2F2F5F5F5F7F7F7F8F8F8F9F9F9F9F9F9B7B7B7BBBBBBFFFFFFFFFFFFFFFFFF
        FFFFFFFEFEFED0CCC9EAD6C2FFF3E1FFF1DFFFF1E3FFF2E5FFF4E7FFF7EBFFF8
        EBB2ACA6D1D2D2FFFFFFFFFFFFFFFFFFFFFFFFFEFEFECCCCCCD2D2D2EEEEEEEC
        ECECEEEEEEEFEFEFF1F1F1F4F4F4F4F4F4ABABABD2D2D2FFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFE0E1E1CCB8A5FBEEDAFFF9E8FFF5E9FFF5EBFFF5EBFFF8EFFBEF
        E1939392FAFAFAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE1E1E1B4B4B4E9E9E9F3
        F3F3F2F2F2F3F3F3F3F3F3F6F6F6ECECEC939393FAFAFAFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFAFAFACDC8C5CEBEAAFBF3E1FFF8ECFFF8EFFFF8EFFDF2E4B6B1
        ACCFCFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFAFAC8C8C8BABABAED
        EDEDF5F5F5F6F6F6F6F6F6EFEFEFB0B0B0CFCFCFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFF6F6F6CFCDCBCEC7BAE4DCD0EAE1D7E1D9D0B1AEACCECE
        CEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6F6F6CCCCCCC3
        C3C3D9D9D9DFDFDFD7D7D7AEAEAECECECEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE6E6E6CACACAC4C4C4C9C9C9FAFAFAFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE6
        E6E6CACACAC4C4C4C9C9C9FAFAFAFFFFFFFFFFFFFFFFFFFFFFFF}
      NumGlyphs = 2
      TabOrder = 1
      OnClick = btPesquisaClick
    end
  end
  object cdsLMC: TClientDataSet
    PersistDataPacket.Data = {
      E80000009619E0BD010000001800000006000000000003000000E80004466C61
      6701004900000001000557494454480200020001000A4D65726361646F726961
      0100490000000100055749445448020002000D000944657363726963616F0100
      490000000100055749445448020002001E00095174645F56656E646108000400
      0000010007535542545950450200490006004D6F6E6579000B5174645F4C6569
      74757261080004000000010007535542545950450200490006004D6F6E657900
      0D5174645F4469666572656E6361080004000000010007535542545950450200
      490006004D6F6E6579000000}
    Active = True
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'Flag'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'Mercadoria'
        DataType = ftString
        Size = 13
      end
      item
        Name = 'Descricao'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'Qtd_Venda'
        DataType = ftCurrency
      end
      item
        Name = 'Qtd_Leitura'
        DataType = ftCurrency
      end
      item
        Name = 'Qtd_Diferenca'
        DataType = ftCurrency
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 68
    Top = 116
    object cdsLMCMercadoria: TStringField
      FieldName = 'Mercadoria'
      Size = 13
    end
    object cdsLMCDescricao: TStringField
      FieldName = 'Descricao'
      Size = 30
    end
    object cdsLMCQtd_Venda: TCurrencyField
      FieldName = 'Qtd_Venda'
      DisplayFormat = '###,##0.000'
    end
    object cdsLMCQtd_Leitura: TCurrencyField
      FieldName = 'Qtd_Leitura'
      DisplayFormat = '###,##0.000'
    end
    object cdsLMCQtd_Diferenca: TCurrencyField
      FieldName = 'Qtd_Diferenca'
      DisplayFormat = '###,##0.000'
    end
    object cdsLMCFlag: TStringField
      FieldName = 'Flag'
      Size = 1
    end
  end
  object dsLMC: TDataSource
    AutoEdit = False
    DataSet = cdsLMC
    Left = 185
    Top = 197
  end
  object dsConcentrador: TDataSource
    AutoEdit = False
    DataSet = cdsConcentrador
    Left = 225
    Top = 199
  end
  object cdsConcentrador: TClientDataSet
    Aggregates = <>
    AggregatesActive = True
    FieldDefs = <
      item
        Name = 'Mercadoria'
        DataType = ftString
        Size = 13
      end
      item
        Name = 'Descricao'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'Quantidade'
        DataType = ftCurrency
      end
      item
        Name = 'Valor'
        DataType = ftCurrency
      end
      item
        Name = 'Total'
        DataType = ftCurrency
      end
      item
        Name = 'Abastecida'
        DataType = ftInteger
      end
      item
        Name = 'Flag'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'Bico'
        DataType = ftInteger
      end
      item
        Name = 'Temp_ECF_Item'
        DataType = ftBoolean
      end
      item
        Name = 'CHECK'
        DataType = ftBoolean
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    AfterPost = cdsConcentradorAfterPost
    Left = 186
    Top = 118
    object cdsConcentradorMercadoria: TStringField
      DisplayWidth = 11
      FieldName = 'Mercadoria'
      Size = 13
    end
    object cdsConcentradorQuantidade: TCurrencyField
      DisplayWidth = 12
      FieldName = 'Quantidade'
      DisplayFormat = '##,##0.000'
    end
    object cdsConcentradorValor: TCurrencyField
      DisplayWidth = 12
      FieldName = 'Valor'
      DisplayFormat = '##0.000'
    end
    object cdsConcentradorTotal: TCurrencyField
      DisplayWidth = 12
      FieldName = 'Total'
      DisplayFormat = '##,##0.00'
    end
    object cdsConcentradorAbastecida: TIntegerField
      DisplayWidth = 12
      FieldName = 'Abastecida'
    end
    object cdsConcentradorFlag: TStringField
      Alignment = taCenter
      DisplayWidth = 4
      FieldName = 'Flag'
      Size = 1
    end
    object cdsConcentradorBico: TIntegerField
      DisplayWidth = 12
      FieldName = 'Bico'
    end
    object cdsConcentradorTemp_ECF_Item: TBooleanField
      FieldName = 'Temp_ECF_Item'
    end
    object cdsConcentradorDescricao: TStringField
      FieldName = 'Descricao'
      Size = 30
    end
    object cdsConcentradorCHECK: TBooleanField
      FieldName = 'CHECK'
    end
  end
  object qEncerrante: TIBQuery
    Database = dmCupomFiscal.DataBase
    Transaction = dmCupomFiscal.Transaction
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'SELECT M.Descricao, m.id mercadoria, A.Bomba,'
      'MIN(A.Lt_Encerrante - A.litro) AS ini_Encerrante,'
      'MAX(A.Lt_Encerrante) AS Fim_Encerrante,'
      'MAX(A.Lt_Encerrante) - MIN(A.Lt_Encerrante - A.litro) AS Vendas'
      'FROM est_abastecimentos A'
      'LEFT OUTER JOIN Est_Mercadorias_Bico MB ON MB.ID=A.Bomba'
      'LEFT OUTER JOIN Est_Mercadorias M ON M.ID=MB.Mercadoria'
      'WHERE CAST(A.Data_hora AS DATE)=:pData'
      'GROUP BY M.Descricao, m.id, A.Bomba'
      'ORDER BY M.Descricao')
    Left = 269
    Top = 119
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'pData'
        ParamType = ptUnknown
      end>
    object qEncerranteBOMBA: TIntegerField
      FieldName = 'BOMBA'
      Origin = '"EST_ABASTECIMENTOS"."BOMBA"'
      Required = True
    end
    object qEncerranteINI_ENCERRANTE: TIBBCDField
      FieldName = 'INI_ENCERRANTE'
      ProviderFlags = []
      DisplayFormat = '##,###,##0'
      Precision = 18
      Size = 3
    end
    object qEncerranteFIM_ENCERRANTE: TIBBCDField
      FieldName = 'FIM_ENCERRANTE'
      ProviderFlags = []
      DisplayFormat = '##,###,##0'
      Precision = 18
      Size = 3
    end
    object qEncerranteVENDAS: TIBBCDField
      FieldName = 'VENDAS'
      ProviderFlags = []
      DisplayFormat = '###,##0'
      Precision = 18
      Size = 3
    end
    object qEncerranteMERCADORIA: TIBStringField
      FieldName = 'MERCADORIA'
      ProviderFlags = []
      FixedChar = True
      Size = 13
    end
    object qEncerranteDESCRICAO: TIBStringField
      FieldName = 'DESCRICAO'
      Origin = '"EST_MERCADORIAS"."DESCRICAO"'
      Size = 100
    end
  end
  object dsEncerrante: TDataSource
    DataSet = qEncerrante
    Left = 275
    Top = 199
  end
  object cdsEncerrante: TClientDataSet
    PersistDataPacket.Data = {
      D80000009619E0BD010000001800000005000000000003000000D80013536169
      64615F41626173746563696D656E746F08000400000001000753554254595045
      0200490006004D6F6E6579000953616964615F45434608000400000001000753
      5542545950450200490006004D6F6E657900094469666572656E636108000400
      0000010007535542545950450200490006004D6F6E6579000A4D65726361646F
      7269610100490000000100055749445448020002000D000E4D6572635F446573
      63726963616F01004900000001000557494454480200020064000000}
    Active = True
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'Saida_Abastecimento'
        DataType = ftCurrency
      end
      item
        Name = 'Saida_ECF'
        DataType = ftCurrency
      end
      item
        Name = 'Diferenca'
        DataType = ftCurrency
      end
      item
        Name = 'Mercadoria'
        DataType = ftString
        Size = 13
      end
      item
        Name = 'Merc_Descricao'
        DataType = ftString
        Size = 100
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 380
    Top = 129
    object cdsEncerranteSaida_ECF: TCurrencyField
      FieldName = 'Saida_ECF'
      currency = False
    end
    object cdsEncerranteDiferenca: TCurrencyField
      FieldName = 'Diferenca'
      currency = False
    end
    object cdsEncerranteMercadoria: TStringField
      FieldName = 'Mercadoria'
      Size = 13
    end
    object cdsEncerranteSaida_Abastecimento: TCurrencyField
      FieldName = 'Saida_Abastecimento'
      currency = False
    end
    object cdsEncerranteMerc_Descricao: TStringField
      FieldName = 'Merc_Descricao'
      Size = 100
    end
  end
  object dsCdsEncerrante: TDataSource
    DataSet = cdsEncerrante
    Left = 355
    Top = 209
  end
  object imgListAbastecidas: TImageList
    Left = 528
    Top = 87
    Bitmap = {
      494C010102000500040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000005F5F5F005F5F5F000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000005F5F5F0000000000000000005F5F5F0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00005F5F5F00000000000000000000000000000000005F5F5F00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000005F5F
      5F000000000000000000000000000000000000000000000000005F5F5F000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000005F5F5F000000
      000000000000000000005F5F5F005F5F5F000000000000000000000000005F5F
      5F00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000005F5F5F000000
      0000000000005F5F5F0000000000000000005F5F5F0000000000000000000000
      00005F5F5F000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000005F5F
      5F005F5F5F00000000000000000000000000000000005F5F5F00000000000000
      0000000000005F5F5F0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000005F5F5F000000
      0000000000005F5F5F0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000005F5F
      5F005F5F5F000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF0000000000000000007FFE7FFE00000000
      7FFE7FFE000000007CFE7FFE00000000787E7FFE00000000703E7FFE00000000
      601E7FFE00000000400E7FFE0000000043067FFE0000000067827FFE00000000
      7FC27FFE000000007FE67FFE000000007FFE7FFE000000007FFE7FFE00000000
      7FFE7FFE00000000000000000000000000000000000000000000000000000000
      000000000000}
  end
end
