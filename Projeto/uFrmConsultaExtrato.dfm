object frmConsultaExtrato: TfrmConsultaExtrato
  Left = 262
  Top = 166
  Width = 892
  Height = 490
  BorderIcons = [biSystemMenu]
  Caption = 'Consulta - Extrato de Vendas de Mercadorias no ECF'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDesktopCenter
  OnKeyDown = FormKeyDown
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object DBGrid1: TDBGrid
    Left = 0
    Top = 37
    Width = 884
    Height = 426
    Align = alClient
    Ctl3D = False
    DataSource = DataSource1
    FixedColor = clWhite
    Options = [dgTitles, dgColumnResize, dgColLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    ParentCtl3D = False
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'DATA'
        Title.Alignment = taCenter
        Title.Caption = 'Data'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'MERCADORIA'
        Title.Alignment = taCenter
        Title.Caption = 'C'#243'digo'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DESCRICAO'
        Title.Alignment = taCenter
        Title.Caption = 'Descri'#231#227'o da Mercadoria'
        Width = 318
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'QTD'
        Title.Alignment = taRightJustify
        Title.Caption = 'Quantidade'
        Width = 69
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'VLR'
        Title.Alignment = taRightJustify
        Title.Caption = 'Valor R$'
        Width = 66
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'VLR_DESCONTO'
        Title.Alignment = taRightJustify
        Title.Caption = 'Desconto R$'
        Width = 73
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TOTAL'
        Title.Alignment = taRightJustify
        Title.Caption = 'Total R$'
        Width = 74
        Visible = True
      end
      item
        Alignment = taRightJustify
        Expanded = False
        FieldName = 'FPAG_DESCRICAO'
        Title.Alignment = taRightJustify
        Title.Caption = 'Forma Pagamento'
        Width = 109
        Visible = True
      end>
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 884
    Height = 37
    Align = alTop
    Color = clBtnHighlight
    Ctl3D = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentCtl3D = False
    ParentFont = False
    TabOrder = 0
    object Label1: TLabel
      Left = 8
      Top = 11
      Width = 53
      Height = 13
      Caption = 'Data Inicial'
    end
    object mkDt_Final: TMaskEdit
      Left = 72
      Top = 9
      Width = 57
      Height = 19
      Ctl3D = False
      EditMask = '!99/99/00;1;_'
      MaxLength = 8
      ParentCtl3D = False
      TabOrder = 0
      Text = '  /  /  '
      OnExit = mkDt_FinalExit
    end
  end
  object DataSource1: TDataSource
    DataSet = dmCupomFiscal.qConsulta_Ext_Cli
    Left = 312
    Top = 200
  end
end
