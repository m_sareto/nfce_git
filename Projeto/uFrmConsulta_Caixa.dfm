object FrmConsulta_Caixa: TFrmConsulta_Caixa
  Left = 457
  Top = 129
  Caption = 'Consulta Caixa'
  ClientHeight = 608
  ClientWidth = 992
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDesktopCenter
  OnKeyDown = FormKeyDown
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 62
    Width = 86
    Height = 16
    Caption = 'Venda Bruta'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 16
    Top = 94
    Width = 67
    Height = 16
    Caption = 'Desconto'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label3: TLabel
    Left = 16
    Top = 126
    Width = 123
    Height = 16
    Caption = 'Venda Liqu'#237'da (+)'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label4: TLabel
    Left = 16
    Top = 158
    Width = 148
    Height = 16
    Caption = 'Meios de Pagamento'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label5: TLabel
    Left = 16
    Top = 190
    Width = 42
    Height = 16
    Caption = 'Troco'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label6: TLabel
    Left = 16
    Top = 222
    Width = 109
    Height = 16
    Caption = 'Suprimento ( + )'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label7: TLabel
    Left = 16
    Top = 254
    Width = 82
    Height = 16
    Caption = 'Sangria ( - )'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label8: TLabel
    Left = 72
    Top = 315
    Width = 142
    Height = 24
    Caption = 'Saldo do Caixa'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label12: TLabel
    Left = 16
    Top = 286
    Width = 148
    Height = 16
    Caption = 'Contas a Receber (+)'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object reVBruta: TRealEdit
    Left = 176
    Top = 59
    Width = 103
    Height = 21
    Alignment = taRightJustify
    Ctl3D = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentCtl3D = False
    ParentFont = False
    TabOrder = 1
    FormatReal = fNumber
    FormatSize = '10.2'
  end
  object reVDesconto: TRealEdit
    Left = 176
    Top = 91
    Width = 103
    Height = 21
    Alignment = taRightJustify
    Ctl3D = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentCtl3D = False
    ParentFont = False
    TabOrder = 2
    FormatReal = fNumber
    FormatSize = '10.2'
  end
  object reVLiquido: TRealEdit
    Left = 176
    Top = 123
    Width = 103
    Height = 21
    Alignment = taRightJustify
    Ctl3D = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentCtl3D = False
    ParentFont = False
    TabOrder = 3
    FormatReal = fNumber
    FormatSize = '10.2'
  end
  object reVRecebido: TRealEdit
    Left = 176
    Top = 155
    Width = 103
    Height = 21
    Alignment = taRightJustify
    Ctl3D = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentCtl3D = False
    ParentFont = False
    TabOrder = 4
    FormatReal = fNumber
    FormatSize = '10.2'
  end
  object reVTroco: TRealEdit
    Left = 176
    Top = 187
    Width = 103
    Height = 21
    Alignment = taRightJustify
    Ctl3D = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentCtl3D = False
    ParentFont = False
    TabOrder = 5
    FormatReal = fNumber
    FormatSize = '10.2'
  end
  object reVSuprimento: TRealEdit
    Left = 176
    Top = 219
    Width = 103
    Height = 21
    Alignment = taRightJustify
    Ctl3D = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentCtl3D = False
    ParentFont = False
    TabOrder = 6
    FormatReal = fNumber
    FormatSize = '10.2'
  end
  object reVSangria: TRealEdit
    Left = 176
    Top = 251
    Width = 103
    Height = 21
    Alignment = taRightJustify
    Ctl3D = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentCtl3D = False
    ParentFont = False
    TabOrder = 7
    FormatReal = fNumber
    FormatSize = '10.2'
  end
  object reVSaldo: TRealEdit
    Left = 72
    Top = 340
    Width = 144
    Height = 29
    TabStop = False
    Alignment = taRightJustify
    Ctl3D = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentCtl3D = False
    ParentFont = False
    ReadOnly = True
    TabOrder = 9
    FormatReal = fNumber
    FormatSize = '10.2'
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 992
    Height = 41
    Align = alTop
    TabOrder = 0
    ExplicitWidth = 981
    object Label9: TLabel
      Left = 115
      Top = 15
      Width = 36
      Height = 13
      Caption = 'Periodo'
    end
    object Label10: TLabel
      Left = 16
      Top = 15
      Width = 26
      Height = 13
      Caption = 'Caixa'
    end
    object Label11: TLabel
      Left = 311
      Top = 15
      Width = 35
      Height = 13
      Caption = '* Turno'
    end
    object Label13: TLabel
      Left = 218
      Top = 15
      Width = 16
      Height = 13
      Caption = 'At'#233
    end
    object btConsulta: TBitBtn
      Left = 425
      Top = 8
      Width = 91
      Height = 25
      Caption = 'Consultar'
      Glyph.Data = {
        36060000424D3606000000000000360000002800000020000000100000000100
        18000000000000060000120B0000120B00000000000000000000FFFFFFCDCDCD
        AAAAAAF5F5F5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFCDCDCDAAAAAAF5F5F5FFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0B9B9
        C6C6C6A3A3A3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFBBBBBBC6C6C6A3A3A3FFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE3DFDF
        D2C6C6A9AAAAC2C2C2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFDFDFDFC8C8C8AAAAAAC2C2C2FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        DED9D9D6CFCF909191E4E4E4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDADADAD0D0D0919191E4E4E4FFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFDDD8D8CFCCCC8B8B8BF9F9F9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD9D9D9CDCDCD8B8B8BF9F9F9FF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFE1DDDDBBB9B99D9D9EC2C2C2A1A1A1989898A2A2A2DDDDDDFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDDDDDDB9B9B99D9D9DC2
        C2C2A1A1A1989898A2A2A2DDDDDDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFC0BDBEB2B2B0DEDBD1F8F8ECFAF9F3E3E1DE969594A6A6
        A6FDFDFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBEBEBEB1B1B1D8
        D8D8F4F4F4F7F7F7E0E0E0959595A6A6A6FDFDFDFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFF8F8F8B4B2B0F6ECE0FFFEF2FFFFF7FFFFFAFFFFFDFFFFFFA5A2
        A1B2B3B3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8F8F8B2B2B2E9E9E9F9
        F9F9FCFCFCFDFDFDFEFEFEFFFFFFA2A2A2B3B3B3FFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFC0C1C1E3D2C7FFF5E9FFFCECFFFCF3FFFDF6FFFEF7FFFFFBFAF0
        F1808180F2F2F2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0C0C0D1D1D1F2F2F2F6
        F6F6F9F9F9FBFBFBFCFCFCFDFDFDF2F2F2808080F2F2F2FFFFFFFFFFFFFFFFFF
        FFFFFFFEFEFEC1BEBCF2DDCEFFF6E3FFFAEAFFFAEFFFFBF3FFFCF5FFFEF7FFFC
        F9AAA5A5CBCBCBFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEBEBEBEDBDBDBF0F0F0F4
        F4F4F7F7F7F9F9F9FAFAFAFCFCFCFBFBFBA6A6A6CBCBCBFFFFFFFFFFFFFFFFFF
        FFFFFFF9F9F9C1BAB5F6E1D0FFF5E1FFF7E7FFF8EDFFF9F1FFFAF2FFFCF4FFFB
        F4C0B7B4BBBBBBFFFFFFFFFFFFFFFFFFFFFFFFF9F9F9BABABADEDEDEEFEFEFF2
        F2F2F5F5F5F7F7F7F8F8F8F9F9F9F9F9F9B7B7B7BBBBBBFFFFFFFFFFFFFFFFFF
        FFFFFFFEFEFED0CCC9EAD6C2FFF3E1FFF1DFFFF1E3FFF2E5FFF4E7FFF7EBFFF8
        EBB2ACA6D1D2D2FFFFFFFFFFFFFFFFFFFFFFFFFEFEFECCCCCCD2D2D2EEEEEEEC
        ECECEEEEEEEFEFEFF1F1F1F4F4F4F4F4F4ABABABD2D2D2FFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFE0E1E1CCB8A5FBEEDAFFF9E8FFF5E9FFF5EBFFF5EBFFF8EFFBEF
        E1939392FAFAFAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE1E1E1B4B4B4E9E9E9F3
        F3F3F2F2F2F3F3F3F3F3F3F6F6F6ECECEC939393FAFAFAFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFAFAFACDC8C5CEBEAAFBF3E1FFF8ECFFF8EFFFF8EFFDF2E4B6B1
        ACCFCFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFAFAC8C8C8BABABAED
        EDEDF5F5F5F6F6F6F6F6F6EFEFEFB0B0B0CFCFCFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFF6F6F6CFCDCBCEC7BAE4DCD0EAE1D7E1D9D0B1AEACCECE
        CEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6F6F6CCCCCCC3
        C3C3D9D9D9DFDFDFD7D7D7AEAEAECECECEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE6E6E6CACACAC4C4C4C9C9C9FAFAFAFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE6
        E6E6CACACAC4C4C4C9C9C9FAFAFAFFFFFFFFFFFFFFFFFFFFFFFF}
      NumGlyphs = 2
      TabOrder = 4
      OnClick = btConsultaClick
    end
    object mkDataI: TMaskEdit
      Left = 155
      Top = 11
      Width = 55
      Height = 19
      Ctl3D = False
      EditMask = '!99/99/00;1;_'
      MaxLength = 8
      ParentCtl3D = False
      TabOrder = 1
      Text = '  /  /  '
    end
    object edCaixa: TEdit
      Left = 48
      Top = 11
      Width = 49
      Height = 19
      TabStop = False
      Ctl3D = False
      ParentCtl3D = False
      ReadOnly = True
      TabOrder = 0
      Text = '0'
    end
    object edTurno: TEdit
      Left = 350
      Top = 11
      Width = 35
      Height = 19
      Ctl3D = False
      MaxLength = 3
      ParentCtl3D = False
      TabOrder = 3
      Text = '0'
    end
    object btFecha: TBitBtn
      Left = 890
      Top = 8
      Width = 73
      Height = 25
      Caption = 'Fechar'
      Glyph.Data = {
        36060000424D3606000000000000360000002800000020000000100000000100
        18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC6A18CC38E68
        C08B66BE8864BB8561B9835FB47E5CB27C5AB17B58AE7957AD7656AB7554A973
        53A97151C6A18CFFFFFFA0A0A08989898787878484848181817F7F7F7B7B7B79
        79797878787676767474747373737171716F6F6FA0A0A0FFFFFFC8926CD5FFDA
        D5FFDAD6FFDBD8FFDDDAFFDF1D5E20A3BCA4E3EBE4FBFCFBFFFFFFFFFFFFFFFF
        FFFFFFFFA97251FFFFFF8E8E8EE7E7E7E7E7E7E7E7E7E9E9E9EAEAEA383838AD
        ADADE6E6E6FBFBFBFFFFFFFFFFFFFFFFFFFFFFFF707070FFFFFFCA946ED5FFDA
        D5FFDAD2FCD759975EDAFFDF226525A5BFA6E3EBE4FBFCFBFFFFFFFFFFFFFFFF
        FFFFFFFFAA7353FFFFFF909090E7E7E7E7E7E7E4E4E4737373EAEAEA3D3D3DB0
        B0B0E6E6E6FBFBFBFFFFFFFFFFFFFFFFFFFFFFFF717171FFFFFFCC976FD5FFDA
        D5FFDA4D9554377F3CDAFFDF276D2CAAC4ACE5ECE5FAFBFAFFFFFFFFFFFFFFFF
        FFFFFFFFAC7554FFFFFF929292E7E7E7E7E7E76B6B6B555555EAEAEA444444B5
        B5B5E8E8E8FAFAFAFFFFFFFFFFFFFFFFFFFFFFFF737373FFFFFFD19C73D5FFDA
        5FA9665DA66458A15F347E3A579A5DA9C5ABE4ECE4FBFCFBFFFFFFFFFFFFFFFF
        FFFFFFFFB07A58FFFFFF969696E7E7E77E7E7E7C7C7C777777535353737373B5
        B5B5E7E7E7FBFBFBFFFFFFFFFFFFFFFFFFFFFFFF777777FFFFFFD49E7558AC62
        69B4718ECC968BCB9287C88E5FA165ACC9AEE5EEE6FBFCFBFFFFFFFFFFFFFFFF
        FFFFFFFFB27C5AFFFFFF9898987C7C7C898989A8A8A8A6A6A6A2A2A27B7B7BB8
        B8B8E9E9E9FBFBFBFFFFFFFFFFFFFFFFFFFFFFFF797979FFFFFFD5A076D5FFDA
        6CBB756BB67476BC7E53A05B64A76BB2CFB5E7EFE7FAFCFAFFFFFFFFFFFFFFFF
        FFFFFFFFB57E5CFFFFFF9A9A9AE7E7E78D8D8D8B8B8B949494737373808080BE
        BEBEEAEAEAFBFBFBFFFFFFFFFFFFFFFFFFFFFFFF7B7B7BFFFFFFD8A279D5FFDA
        D5FFDA69B97154A95EBAEABF5BA463B2D2B5E8F1E9FBFCFBFFFFFFFFFFFFFFFF
        FFFFFFFFB7815EFFFFFF9C9C9CE7E7E7E7E7E78B8B8B787878CECECE7A7A7ABF
        BFBFECECECFBFBFBFFFFFFFFFFFFFFFFFFFFFFFF7E7E7EFFFFFFD9A379D5FFDA
        D5FFDBD3FDD87BC683D9FFDE4DA155B5D6B9E8F2E9FBFDFBFFFFFFFFFFFFFFFF
        FFFFFFFFBA8560FFFFFF9D9D9DE7E7E7E7E7E7E5E5E59B9B9BE9E9E9707070C3
        C3C3ECECECFCFCFCFFFFFFFFFFFFFFFFFFFFFFFF818181FFFFFFDBA47AD5FFDA
        D5FFDBD6FFDBD7FFDCD9FFDE54AB5EB8DABCE9F3EAFBFDFBFFFFFFFFFFFFFFFF
        FFFFFFFFBD8763FFFFFF9E9E9EE7E7E7E7E7E7E7E7E7E8E8E8E9E9E9797979C6
        C6C6EDEDEDFCFCFCFFFFFFFFFFFFFFFFFFFFFFFF838383FFFFFFDCA77BDCA77B
        DCA77BDCA77BDCA77BDCA77BDCA77BDCA77BDCA77BDCA77BDCA77BDCA77BDCA7
        7BDCA77BC08B66FFFFFFA0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0
        A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0878787FFFFFFDDAD86E8B992
        E8B992E8B992E8B992E8B992E8B992E8B992E8B992E8B992E8B992E8B992E8B9
        92E8B992C19170FFFFFFA7A7A7B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3
        B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B38E8E8EFFFFFFDBC3B6DEB492
        DCA77BDCA67ADAA47AD8A279D5A076D49E75D29D73CF9A72CE9970CB966FC994
        6CC79E80DBC3B6FFFFFFC3C3C3AFAFAFA0A0A09F9F9F9E9E9E9C9C9C9A9A9A98
        98989797979595959393939191918F8F8F9B9B9BC3C3C3FFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
      NumGlyphs = 2
      TabOrder = 6
      OnClick = btFechaClick
    end
    object mkDataF: TMaskEdit
      Left = 240
      Top = 11
      Width = 55
      Height = 19
      Ctl3D = False
      EditMask = '!99/99/00;1;_'
      MaxLength = 8
      ParentCtl3D = False
      TabOrder = 2
      Text = '  /  /  '
    end
    object btImprimir: TBitBtn
      Left = 662
      Top = 8
      Width = 91
      Height = 25
      Caption = 'Impress'#227'o'
      Glyph.Data = {
        36060000424D3606000000000000360000002800000020000000100000000100
        18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
        FFFFFFC89662CA9865CA9765CA9765CA9765CA9764C99764C99764CA9865C895
        62FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8B8B8B8E8E8E8D8D8D8D8D8D8D
        8D8D8D8D8D8D8D8D8D8D8D8E8E8E8B8B8BFFFFFFFFFFFFFFFFFFA1A1A17A7A7A
        585858C79561F9F7F6F9F1ECF9F1EBF8F0E9F7EDE6F4EAE1F2E8DEFAF8F6C794
        612424244B4B4B969696A1A1A17A7A7A5858588A8A8AF7F7F7F1F1F1F0F0F0EF
        EFEFECECECE8E8E8E6E6E6F8F8F88A8A8A2424244B4B4B9696966B6B6BA7A7A7
        B5B5B5818181AFACAAC5C0BDC5C0BDC5C0BDC5C0BDC5C0BDC5C0BDADAAA82C2C
        2CB5B5B59B9B9B2323236B6B6BA7A7A7B5B5B5818181ACACACC0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0AAAAAA2C2C2CB5B5B59B9B9B232323707070B5B5B5
        B5B5B59595958181818181817979796E6E6E6161615252524343434242426E6E
        6EB5B5B5B5B5B5252525707070B5B5B5B5B5B59595958181818181817979796E
        6E6E6161615252524343434242426E6E6EB5B5B5B5B5B5252525757575BBBBBB
        BBBBBB8D8D8DD4D4D4B9B9B9B9B9B9B9B9B9B9B9B9B9B9B9B9B9B9D3D3D38383
        83BBBBBBBBBBBB2A2A2A757575BBBBBBBBBBBB8D8D8DD4D4D4B9B9B9B9B9B9B9
        B9B9B9B9B9B9B9B9B9B9B9D3D3D3838383BBBBBBBBBBBB2A2A2A7A7A7AD7D7D7
        D7D7D7979797D8D8D8BFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFD7D7D78E8E
        8ED7D7D7D7D7D73F3F3F7A7A7AD7D7D7D7D7D7979797D8D8D8BFBFBFBFBFBFBF
        BFBFBFBFBFBFBFBFBFBFBFD7D7D78E8E8ED7D7D7D7D7D73F3F3F7E7E7EF9F9F9
        F9F9F9ABABABDFDFDFCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBDFDFDFA3A3
        A3F9F9F9F9F9F96161617E7E7EF9F9F9F9F9F9ABABABDFDFDFCBCBCBCBCBCBCB
        CBCBCBCBCBCBCBCBCBCBCBDFDFDFA3A3A3F9F9F9F9F9F9616161848484FCFCFC
        FCFCFCCBCBCBF2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2C6C6
        C6FCFCFCFCFCFC717171848484FCFCFCFCFCFCCBCBCBF2F2F2F2F2F2F2F2F2F2
        F2F2F2F2F2F2F2F2F2F2F2F2F2F2C6C6C6FCFCFCFCFCFC717171979797D2D2D2
        E8E8E87D7D7D7D7D7D7D7D7D7D7D7D7D7D7D7D7D7D7D7D7D7D7D7D7D7D7D7D7D
        7DE8E8E8C4C4C46D6D6D979797D2D2D2E8E8E87D7D7D7D7D7D7D7D7D7D7D7D7D
        7D7D7D7D7D7D7D7D7D7D7D7D7D7D7D7D7DE8E8E8C4C4C46D6D6DDDDDDD9A9A9A
        CCCCCCC78B4EF9F4EDFEE8D8FEE8D7FDE5D3FCE4D1FAE0C7F9DDC3FAF4EDC785
        4AC3C3C3747474CDCDCDDDDDDD9A9A9ACCCCCC7F7F7FF2F2F2E6E6E6E6E6E6E3
        E3E3E1E1E1DBDBDBD8D8D8F2F2F27B7B7BC3C3C3747474CDCDCDFFFFFFCECECE
        878787C5894CF9F4EFFEE7D7FDE7D5FCE6D2FBE1CCF8DCC2F6DABDFAF4EFC483
        48616161BCBCBCFFFFFFFFFFFFCECECE8787877D7D7DF3F3F3E5E5E5E4E4E4E2
        E2E2DEDEDED7D7D7D4D4D4F3F3F3787878616161BCBCBCFFFFFFFFFFFFFFFFFF
        FBFBFBC68C4FF9F4F0FCE6D3FDE7D3FBE3CDFAE0C8F5D6BBF3D4B5F8F4F0C485
        4AF9F9F9FFFFFFFFFFFFFFFFFFFFFFFFFBFBFB7F7F7FF3F3F3E3E3E3E3E3E3DF
        DFDFDCDCDCD1D1D1CECECEF3F3F37A7A7AF9F9F9FFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFC88D51F9F5F1FCE3CFFCE4CFFAE1CAF9DDC4F4E9DFF7F2ECF5EFE9C380
        48FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF808080F4F4F4E0E0E0E0E0E0DD
        DDDDD9D9D9E7E7E7F1F1F1EEEEEE777777FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFC88D52F9F5F1FCE3CDFBE3CDF9E0C8F8DCC2FDFBF8FCE6CDE2B684D5A8
        84FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF828282F4F4F4DFDFDFDFDFDFDB
        DBDBD7D7D7FAFAFAE0E0E0ABABABA3A3A3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFC5884DF7F2ECF8F4EEF8F3EDF8F3EDF8F2ECF2E6D7E2B27DDB9569FDFB
        FAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7D7D7DF1F1F1F2F2F2F2F2F2F2
        F2F2F1F1F1E2E2E2A6A6A6919191FBFBFBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFE8CEB9D7AA7CC88C50C88C4FCA9155CB9055C5894DDDAF8DFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCBCBCBA0A0A080808080808084
        84848484847E7E7EABABABFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
      NumGlyphs = 2
      TabOrder = 5
      OnClick = btImprimirClick
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 589
    Width = 992
    Height = 19
    Panels = <
      item
        Text = '*  Turno ZERO para Todos'
        Width = 150
      end
      item
        Text = 'Impress'#227'o ser'#225' no ECF'
        Width = 50
      end>
    ExplicitTop = 435
    ExplicitWidth = 981
  end
  object reVCR: TRealEdit
    Left = 176
    Top = 283
    Width = 103
    Height = 21
    Alignment = taRightJustify
    Ctl3D = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentCtl3D = False
    ParentFont = False
    TabOrder = 8
    FormatReal = fNumber
    FormatSize = '10.2'
  end
  object Panel2: TPanel
    Left = 308
    Top = 41
    Width = 684
    Height = 548
    Align = alRight
    Caption = 'Panel2'
    TabOrder = 11
    ExplicitLeft = 297
    ExplicitHeight = 394
    object DBGrid3: TDBGrid
      Left = 1
      Top = 269
      Width = 682
      Height = 278
      Align = alClient
      DataSource = DataSource3
      DrawingStyle = gdsClassic
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'DESCRICAO'
          Title.Caption = 'Combust'#237'vel'
          Width = 380
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CONCENTRADOR_LT'
          Title.Alignment = taCenter
          Title.Caption = 'Concentrador Lt'
          Width = 89
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CUPOM_LT'
          Title.Alignment = taCenter
          Title.Caption = 'Cupom Lt'
          Width = 89
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Diferenca'
          Title.Alignment = taCenter
          Title.Caption = 'Diferen'#231'a Lt'
          Width = 86
          Visible = True
        end>
    end
    object DBGrid1: TDBGrid
      Left = 1
      Top = 20
      Width = 682
      Height = 230
      Align = alTop
      DataSource = DataSource1
      DrawingStyle = gdsClassic
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'MERCADORIA'
          Title.Caption = 'Mercadoria'
          Width = 67
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DESCRICAO'
          Title.Caption = 'Combust'#237'vel'
          Width = 200
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'BOMBA'
          Title.Alignment = taCenter
          Title.Caption = 'Bico'
          Width = 37
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'INI_ENCERRANTE'
          Title.Alignment = taCenter
          Title.Caption = 'Leitura Inicial'
          Width = 121
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FIM_ENCERRANTE'
          Title.Alignment = taCenter
          Title.Caption = 'Leitura Final'
          Width = 119
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'VENDAS'
          Title.Alignment = taCenter
          Title.Caption = 'Volume Lt'
          Width = 101
          Visible = True
        end>
    end
    object Panel3: TPanel
      Left = 1
      Top = 1
      Width = 682
      Height = 19
      Align = alTop
      Caption = 'Abastecidas'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
    end
    object Panel4: TPanel
      Left = 1
      Top = 250
      Width = 682
      Height = 19
      Align = alTop
      Caption = 'Abastecidas / Cupom'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
    end
  end
  object RLReport1: TRLReport
    Left = 285
    Top = 222
    Width = 283
    Height = 718
    Margins.LeftMargin = 2.000000000000000000
    Margins.TopMargin = 0.000000000000000000
    Margins.RightMargin = 2.000000000000000000
    Margins.BottomMargin = 0.000000000000000000
    Borders.Sides = sdCustom
    Borders.DrawLeft = False
    Borders.DrawTop = False
    Borders.DrawRight = False
    Borders.DrawBottom = False
    DataSource = DataSource1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = []
    PageSetup.PaperSize = fpCustom
    PageSetup.PaperWidth = 75.000000000000000000
    PageSetup.PaperHeight = 190.000000000000000000
    Transparent = False
    Visible = False
    object RLBand6: TRLBand
      Left = 8
      Top = 67
      Width = 267
      Height = 14
      AutoSize = True
      BandType = btTitle
      object RLMemo3: TRLMemo
        Left = 133
        Top = 0
        Width = 133
        Height = 14
        Alignment = taRightJustify
        Behavior = [beSiteExpander]
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object RLMemo2: TRLMemo
        Left = 0
        Top = 0
        Width = 230
        Height = 14
        Behavior = [beSiteExpander]
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
    end
    object RLBand8: TRLBand
      Left = 8
      Top = 0
      Width = 267
      Height = 67
      AutoSize = True
      BandType = btTitle
      BeforePrint = RLBand8BeforePrint
      object RLLabel7: TRLLabel
        Left = 0
        Top = 0
        Width = 267
        Height = 18
        Align = faTop
        Alignment = taCenter
        Caption = 'Nome Fantasia'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Layout = tlCenter
        ParentFont = False
      end
      object RLLabel8: TRLLabel
        Left = 0
        Top = 18
        Width = 267
        Height = 12
        Align = faTop
        Alignment = taCenter
        Caption = 'Raz'#195#163'o Social'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object RLLabel9: TRLLabel
        Left = 0
        Top = 30
        Width = 267
        Height = 12
        Align = faTop
        Alignment = taCenter
        Caption = 
          'CNPJ: 22.222.222/22222-22  IE:223.233.344.233 IM:2323.222.333.23' +
          '3'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        Layout = tlBottom
        ParentFont = False
      end
      object RLMemo1: TRLMemo
        Left = 0
        Top = 42
        Width = 267
        Height = 17
        Align = faTop
        Alignment = taCenter
        Behavior = [beSiteExpander]
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        Lines.Strings = (
          'Endere'#231'o')
        ParentFont = False
      end
      object RLDraw8: TRLDraw
        Left = 0
        Top = 59
        Width = 267
        Height = 8
        Align = faTop
        DrawKind = dkLine
        Pen.Width = 2
      end
    end
  end
  object IBSQL1: TIBSQL
    Database = dmCupomFiscal.DataBase
    SQL.Strings = (
      
        'Execute Procedure Pr_Est_ECF_Consulta_Caixa(:pCaixa, :pDataI, :p' +
        'DataF,:pTurno)')
    Transaction = dmCupomFiscal.Transaction
    Left = 427
    Top = 119
  end
  object DataSource1: TDataSource
    DataSet = qAbastecidas
    Left = 427
    Top = 158
  end
  object qCupom: TIBQuery
    Database = dmCupomFiscal.DataBase
    Transaction = dmCupomFiscal.Transaction
    OnCalcFields = qCupomCalcFields
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'Select M.ID, M.descricao'
      ',('
      'Select Sum(Ei.qtd) from est_ecf E'
      'Left Outer Join est_ecf_item Ei on Ei.id_ecf=E.id'
      
        'Where Ei.mercadoria=M.id and E.data>=:pDataIni and E.data<=:pDat' +
        'aFim'
      'and E.Status='#39'X'#39
      ') as Cupom_Lt'
      ',('
      'Select Sum(A.litro) from est_abastecimentos A'
      'Left Outer Join est_mercadorias_bico B on B.id=A.bomba'
      
        'Where B.mercadoria=M.id and cast(A.data_hora as Date)>=cast(:pDa' +
        'taIni as date) and cast(A.data_hora as Date)<=Cast(:pDataFim as ' +
        'Date)'
      ') as Concentrador_Lt'
      'From est_mercadorias M'
      'Where M.subtipo_item='#39'CO'#39
      'Group By 1,2')
    Left = 427
    Top = 69
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'pDataIni'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'pDataFim'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'pDataIni'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'pDataFim'
        ParamType = ptUnknown
      end>
    object qCupomCUPOM_LT: TIBBCDField
      FieldName = 'CUPOM_LT'
      ProviderFlags = []
      DisplayFormat = '###,##0.000'
      Precision = 18
      Size = 3
    end
    object qCupomCONCENTRADOR_LT: TIBBCDField
      FieldName = 'CONCENTRADOR_LT'
      ProviderFlags = []
      DisplayFormat = '###,##0.000'
      Precision = 18
      Size = 3
    end
    object qCupomDiferenca: TCurrencyField
      FieldKind = fkCalculated
      FieldName = 'Diferenca'
      DisplayFormat = '###,##0.000'
      Calculated = True
    end
    object qCupomDESCRICAO: TIBStringField
      FieldName = 'DESCRICAO'
      Origin = '"EST_MERCADORIAS"."DESCRICAO"'
      Required = True
      Size = 100
    end
  end
  object DataSource3: TDataSource
    AutoEdit = False
    DataSet = qCupom
    Left = 482
    Top = 69
  end
  object qAbastecidas: TIBQuery
    Database = dmCupomFiscal.DataBase
    Transaction = dmCupomFiscal.Transaction
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'SELECT M.Descricao, m.id mercadoria, A.Bomba,'
      'MIN(A.Lt_Encerrante - A.litro) AS ini_Encerrante,'
      'MAX(A.Lt_Encerrante) AS Fim_Encerrante,'
      'MAX(A.Lt_Encerrante) - MIN(A.Lt_Encerrante - A.litro) AS Vendas'
      'FROM est_abastecimentos A'
      'LEFT OUTER JOIN Est_Mercadorias_Bico MB ON MB.ID=A.Bomba'
      'LEFT OUTER JOIN Est_Mercadorias M ON M.ID=MB.Mercadoria'
      'WHERE (CAST(A.Data_hora AS DATE) between :pDataI and :pDataF)'
      'and (A.Turno between :pTurnoI and :pTurnoF)'
      'GROUP BY M.Descricao, m.id, A.Bomba'
      'ORDER BY A.Bomba asc')
    Left = 360
    Top = 159
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'pDataI'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'pDataF'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'pTurnoI'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'pTurnoF'
        ParamType = ptUnknown
      end>
    object qAbastecidasMERCADORIA: TIBStringField
      FieldName = 'MERCADORIA'
      ProviderFlags = []
      FixedChar = True
      Size = 13
    end
    object qAbastecidasBOMBA: TIntegerField
      FieldName = 'BOMBA'
      Origin = '"EST_ABASTECIMENTOS"."BOMBA"'
      Required = True
    end
    object qAbastecidasINI_ENCERRANTE: TIBBCDField
      FieldName = 'INI_ENCERRANTE'
      ProviderFlags = []
      DisplayFormat = '###,##0.000'
      Precision = 18
      Size = 3
    end
    object qAbastecidasFIM_ENCERRANTE: TIBBCDField
      FieldName = 'FIM_ENCERRANTE'
      ProviderFlags = []
      DisplayFormat = '###,##0.000'
      Precision = 18
      Size = 3
    end
    object qAbastecidasVENDAS: TIBBCDField
      FieldName = 'VENDAS'
      ProviderFlags = []
      DisplayFormat = '###,##0.000'
      Precision = 18
      Size = 3
    end
    object qAbastecidasDESCRICAO: TIBStringField
      FieldName = 'DESCRICAO'
      Origin = '"EST_MERCADORIAS"."DESCRICAO"'
      Size = 100
    end
  end
  object qLocal: TIBQuery
    Database = dmCupomFiscal.DataBase
    Transaction = dmCupomFiscal.Transaction
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      
        ' select lc.*,nf.NFCE_DANFE_IMPRESSORA as impressora,m.descricao ' +
        'as des_muni from locais lc'
      'left outer join ccustos cc on cc.local=lc.id'
      'left outer join locais_config_nfe nf on nf.local=lc.id'
      'left outer join municipios m on m.id=lc.municipio'
      ''
      'where cc.id=:pCC')
    Left = 509
    Top = 170
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'pCC'
        ParamType = ptUnknown
      end>
    object qLocalID: TIntegerField
      FieldName = 'ID'
      Origin = '"LOCAIS"."ID"'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object qLocalDESCRICAO: TIBStringField
      FieldName = 'DESCRICAO'
      Origin = '"LOCAIS"."DESCRICAO"'
      Required = True
      Size = 40
    end
    object qLocalCNPJ: TIBStringField
      FieldName = 'CNPJ'
      Origin = '"LOCAIS"."CNPJ"'
      Required = True
      Size = 18
    end
    object qLocalIE: TIBStringField
      FieldName = 'IE'
      Origin = '"LOCAIS"."IE"'
      Required = True
      Size = 14
    end
    object qLocalPAIS: TIntegerField
      FieldName = 'PAIS'
      Origin = '"LOCAIS"."PAIS"'
      Required = True
    end
    object qLocalMUNICIPIO: TIntegerField
      FieldName = 'MUNICIPIO'
      Origin = '"LOCAIS"."MUNICIPIO"'
      Required = True
    end
    object qLocalENDERECO: TIBStringField
      FieldName = 'ENDERECO'
      Origin = '"LOCAIS"."ENDERECO"'
      Required = True
      Size = 40
    end
    object qLocalBAIRRO: TIBStringField
      FieldName = 'BAIRRO'
      Origin = '"LOCAIS"."BAIRRO"'
      Required = True
      Size = 40
    end
    object qLocalCEP: TIBStringField
      FieldName = 'CEP'
      Origin = '"LOCAIS"."CEP"'
      Required = True
      Size = 9
    end
    object qLocalUF: TIBStringField
      FieldName = 'UF'
      Origin = '"LOCAIS"."UF"'
      FixedChar = True
      Size = 2
    end
    object qLocalFONE: TIBStringField
      FieldName = 'FONE'
      Origin = '"LOCAIS"."FONE"'
      Size = 15
    end
    object qLocalFAX: TIBStringField
      FieldName = 'FAX'
      Origin = '"LOCAIS"."FAX"'
      Size = 15
    end
    object qLocalNUMERO_END: TIBStringField
      FieldName = 'NUMERO_END'
      Origin = '"LOCAIS"."NUMERO_END"'
      Required = True
      Size = 6
    end
    object qLocalCOMPLEMENTO_END: TIBStringField
      FieldName = 'COMPLEMENTO_END'
      Origin = '"LOCAIS"."COMPLEMENTO_END"'
      Size = 40
    end
    object qLocalNOME_FANTASIA: TIBStringField
      FieldName = 'NOME_FANTASIA'
      Origin = '"LOCAIS"."NOME_FANTASIA"'
      Required = True
      Size = 40
    end
    object qLocalIM: TIBStringField
      FieldName = 'IM'
      Origin = '"LOCAIS"."IM"'
      Size = 15
    end
    object qLocalCNAE: TIBStringField
      FieldName = 'CNAE'
      Origin = '"LOCAIS"."CNAE"'
      Size = 7
    end
    object qLocalE_MAIL: TIBStringField
      FieldName = 'E_MAIL'
      Origin = '"LOCAIS"."E_MAIL"'
      Size = 40
    end
    object qLocalALI_SIMPLESN_ICM: TIBBCDField
      FieldName = 'ALI_SIMPLESN_ICM'
      Origin = '"LOCAIS"."ALI_SIMPLESN_ICM"'
      Precision = 9
      Size = 2
    end
    object qLocalCRT: TIntegerField
      FieldName = 'CRT'
      Origin = '"LOCAIS"."CRT"'
      Required = True
    end
    object qLocalCALCULA_PESO: TIBStringField
      FieldName = 'CALCULA_PESO'
      Origin = '"LOCAIS"."CALCULA_PESO"'
      Required = True
      FixedChar = True
      Size = 1
    end
    object qLocalDATA_EXP: TDateField
      FieldName = 'DATA_EXP'
      Origin = '"LOCAIS"."DATA_EXP"'
    end
    object qLocalANOMES: TIntegerField
      FieldName = 'ANOMES'
      Origin = '"LOCAIS"."ANOMES"'
    end
    object qLocalDATA_MOV: TDateField
      FieldName = 'DATA_MOV'
      Origin = '"LOCAIS"."DATA_MOV"'
    end
    object qLocalNFS_INICIAL: TIntegerField
      FieldName = 'NFS_INICIAL'
      Origin = '"LOCAIS"."NFS_INICIAL"'
    end
    object qLocalNFS_FINAL: TIntegerField
      FieldName = 'NFS_FINAL'
      Origin = '"LOCAIS"."NFS_FINAL"'
    end
    object qLocalNFS_AUTORIZACAO: TIBStringField
      FieldName = 'NFS_AUTORIZACAO'
      Origin = '"LOCAIS"."NFS_AUTORIZACAO"'
    end
    object qLocalCTE_RNTRC: TIBStringField
      FieldName = 'CTE_RNTRC'
      Origin = '"LOCAIS"."CTE_RNTRC"'
      Size = 8
    end
    object qLocalTURNO: TIBStringField
      FieldName = 'TURNO'
      Origin = '"LOCAIS"."TURNO"'
      Size = 2
    end
    object qLocalVLR_IRRF: TIBBCDField
      FieldName = 'VLR_IRRF'
      Origin = '"LOCAIS"."VLR_IRRF"'
      Precision = 18
      Size = 2
    end
    object qLocalANP_ARI: TIBStringField
      FieldName = 'ANP_ARI'
      Origin = '"LOCAIS"."ANP_ARI"'
      Size = 10
    end
    object qLocalDES_MUNI: TIBStringField
      FieldName = 'DES_MUNI'
      Origin = '"MUNICIPIOS"."DESCRICAO"'
      Size = 40
    end
    object qLocalIMPRESSORA: TIBStringField
      FieldName = 'IMPRESSORA'
      Origin = '"LOCAIS_CONFIG_NFE"."NFCE_DANFE_IMPRESSORA"'
      Size = 50
    end
  end
end
