unit uFrmConsultaCR;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, DB, IBCustomDataSet, IBQuery, Grids, DBGrids, StdCtrls,
  TREdit;

type
  TfrmConsultaCR = class(TForm)
    DBGrid1: TDBGrid;
    Panel1: TPanel;
    DataSource1: TDataSource;
    reValor: TRealEdit;
    reResta: TRealEdit;
    Label1: TLabel;
    procedure FormShow(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmConsultaCR: TfrmConsultaCR;

implementation

uses uDMCupomFiscal;

{$R *.dfm}

procedure TfrmConsultaCR.FormShow(Sender: TObject);
Var Valor,Resta:Currency;
begin
Valor:=0;
Resta:=0;

  dmCupomFiscal.qConsulta_CR.First;
  While not dmCupomFiscal.qConsulta_CR.eof do
  begin
    Valor:=Valor+dmCupomFiscal.qConsulta_CRValor.AsCurrency;
    Resta:=Resta+dmCupomFiscal.qConsulta_CRResta.AsCurrency;
    dmCupomFiscal.qConsulta_CR.next;
  end;
reValor.text:=FormatFloat('###,##0.00',Valor);
reResta.text:=FormatFloat('###,##0.00',Resta);
end;

procedure TfrmConsultaCR.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  If (Key=vk_escape) or (Key = vk_return) then Close;
end;

end.
