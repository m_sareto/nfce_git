unit uFrmListaComanda;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Datasnap.Provider,
  Datasnap.DBClient, Vcl.Grids, Vcl.DBGrids, Vcl.StdCtrls, Vcl.ComCtrls,
  Vcl.ExtCtrls, IBX.IBCustomDataSet, IBX.IBQuery, System.ImageList, Vcl.ImgList,
  Vcl.Mask;

type
  TFrmListaComandas = class(TForm)
    dbgListaComandas: TDBGrid;
    cdsListaComandas: TClientDataSet;
    dsComandas: TDataSource;
    qryListaComandas: TIBQuery;
    cdsListaComandasID: TWideStringField;
    cdsListaComandasDATA_HORA: TDateTimeField;
    cdsListaComandasVLR_TOTAL: TBCDField;
    cdsListaComandasOBSERVACAO: TWideStringField;
    cdsListaComandasSTATUS: TWideStringField;
    pnlTop: TPanel;
    stbRodape: TStatusBar;
    pnlRodape: TPanel;
    BtnOK: TButton;
    btnCancelar: TButton;
    cdsListaComandasNOME: TWideStringField;
    imgListComandas: TImageList;
    edtComIni: TEdit;
    edtFunIni: TEdit;
    edtComFim: TEdit;
    edtFunFim: TEdit;
    edtDtIni: TMaskEdit;
    edtDtFim: TMaskEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    btnPesquisar: TButton;
    cdsListaComandasID_FUNCIONARIO: TIntegerField;
    Label7: TLabel;
    lblQtd: TLabel;
    Label9: TLabel;
    lblValor: TLabel;
    cdsListaComandasPAGO: TStringField;
    ckbComandasPagas: TCheckBox;
    Panel1: TPanel;
    dgbListaMercadorias: TDBGrid;
    dsMercadorias: TDataSource;
    cdsMercadorias: TClientDataSet;
    cdsMercadoriasMERCADORIA: TStringField;
    cdsMercadoriasDESCRICAO: TStringField;
    cdsMercadoriasQUANTIDADE: TCurrencyField;
    cdsMercadoriasVLR_UNITARIO: TCurrencyField;
    cdsMercadoriasITEM: TStringField;
    cdsMercadoriasVLR_TOTAL: TCurrencyField;
    cdsMercadoriasPAGO: TIntegerField;
    cdsMercadoriasID_COMANDA: TStringField;
    cdsListaComandasRESTA: TCurrencyField;
    qryListaComandasID: TIBStringField;
    qryListaComandasDATA_HORA: TDateTimeField;
    qryListaComandasVLR_TOTAL: TIBBCDField;
    qryListaComandasOBSERVACAO: TIBStringField;
    qryListaComandasID_FUNCIONARIO: TIntegerField;
    qryListaComandasSTATUS: TIBStringField;
    qryListaComandasPAGO: TIBStringField;
    qryListaComandasNOME: TIBStringField;
    qryListaComandasRESTA: TIBBCDField;
    cdsImpressao: TClientDataSet;
    cdsImpressaoMERCADORIA: TStringField;
    cdsImpressaoDESCRICAO: TStringField;
    cdsImpressaoQUANTIDADE: TCurrencyField;
    cdsImpressaoVLR_UNITARIO: TCurrencyField;
    cdsImpressaoITEM: TStringField;
    cdsImpressaoVLR_TOTAL: TCurrencyField;
    procedure dbgListaComandasDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure dbgListaComandasKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure dbgListaComandasMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure dbgListaComandasTitleClick(Column: TColumn);
    procedure BtnOKClick(Sender: TObject);
    procedure btnPesquisarClick(Sender: TObject);
    procedure Label2Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure btnCancelarClick(Sender: TObject);
    procedure dsComandasUpdateData(Sender: TObject);
    procedure dgbListaMercadoriasDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure dgbListaMercadoriasMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure dgbListaMercadoriasKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dgbListaMercadoriasTitleClick(Column: TColumn);
  private
    Vlr: Currency;
    Qtd: Integer;
    Procedure Pesquisar;
    Procedure DeletarComandasPreInseridas();
    procedure MoverComandasInseridas(pID: String; pEL_Chave: Integer);
    function VerificarComandaVinculada(ID_Comanda: String): String;
    function VerificaItemPreInserido(pID, pItem: String): Boolean;
    procedure Imprimir(lCdsDados: TClientDataSet);
  public
    { Public declarations }
  end;

var
  FrmListaComandas: TFrmListaComandas;

implementation

Uses
  uDmCupomFiscal, uFrmLoja, uFrmPesquisa, ufuncoes, uFrmPrincipal, uFrmImpComanda;

{$R *.dfm}

{ TFrmListaComandas }
procedure TFrmListaComandas.btnCancelarClick(Sender: TObject);
begin
  Close;
end;

procedure TFrmListaComandas.btnPesquisarClick(Sender: TObject);
begin
  Pesquisar;
end;

procedure TFrmListaComandas.BtnOKClick(Sender: TObject);
var
  lQryBusca: TIBQuery;
  lQryDelete: TIBQuery;
  lQryUpdate: TIBQuery;
  lImprime: Boolean;
begin
  lImprime := false;
  cdsMercadorias.Filter := 'PAGO = ''1''';
  cdsMercadorias.Filtered := True;
  cdsMercadorias.First;
  if not(cdsMercadorias.IsEmpty)Then
  begin
    cdsMercadorias.First;
    while not(cdsMercadorias.Eof) do
    begin
      if not(VerificaItemPreInserido(cdsMercadoriasID_COMANDA.asString,cdsMercadoriasITEM.AsString))Then
      begin
        if(cdsListaComandas.Locate('ID',cdsMercadoriasID_COMANDA.asString,[loCaseInsensitive]))then
          if(cdsListaComandasPAGO.Value = '1')Then
          begin
            lImprime := True;
            dmCupomFiscal.ConfirmarPagamentoItemComanda(cdsMercadoriasID_COMANDA.asString,
              cdsMercadoriasITEM.AsString, 'X');
            try
              cdsImpressao.Insert;
              cdsImpressaoMERCADORIA.AsString     := cdsMercadoriasMERCADORIA.AsString;
              cdsImpressaoDESCRICAO.AsString      := cdsMercadoriasDESCRICAO.AsString;
              cdsImpressaoQUANTIDADE.AsCurrency   := cdsMercadoriasQUANTIDADE.AsCurrency;
              cdsImpressaoVLR_UNITARIO.AsCurrency := cdsMercadoriasVLR_UNITARIO.AsCurrency;
              cdsImpressaoITEM.asString           := cdsMercadoriasITEM.AsString;
              cdsImpressaoVLR_TOTAL.AsCurrency    := cdsMercadoriasVLR_TOTAL.AsCurrency;
              cdsImpressao.Post;
            Except
              on E:Exception do
                msgErro('Erro ao selecionar dados para impress�o'+sLineBreak+E.Message, Application.Title);
            end;
            if(dmCupomFiscal.VerificaComandaPaga(cdsMercadoriasID_COMANDA.asString))then
            begin
              dmCupomFiscal.MoverComandasInseridas(cdsMercadoriasID_COMANDA.asString,0);
              dmCupomFiscal.DeletarComandasPagas(cdsMercadoriasID_COMANDA.asString);
            end;
          end
          else
          begin
            frmLoja.edtMercadoria.Text   := cdsMercadoriasMERCADORIA.AsString;
            frmLoja.reQuantidade.Value   := cdsMercadoriasQUANTIDADE.AsCurrency;
            frmLoja.reVlrUnitario.Value  := cdsMercadoriasVLR_UNITARIO.AsCurrency;
            frmLoja.reVlrDesconto.Value  := 0;
            frmLoja.reTotal.Value        := cdsMercadoriasVLR_TOTAL.AsCurrency;
            frmLoja.ID_Comanda           := cdsMercadoriasID_COMANDA.AsString;
            frmLoja.ITEM_Comanda         := cdsMercadoriasITEM.AsString;
            frmLoja.edtMercadoriaExit(Self);
            frmLoja.subtotal;
            frmLoja.btnVende.Click;
          end;
      end;
      cdsMercadorias.Next;
      if(cdsMercadorias.Eof) and (lImprime) then
        Imprimir(cdsImpressao);
    end;
  end;
  {cdsListaComandas.Filter := 'STATUS = ''1'' or PAGO = ''1''';
  cdsListaComandas.Filtered := True;
  cdsListaComandas.First;
  if not(cdsListaComandas.IsEmpty)Then
  begin
    DeletarComandasPreInseridas();
    while not(cdsListaComandas.Eof) do
    begin
      if(cdsListaComandasStatus.Value = '1')Then
      begin
        lQryBusca := TIBQuery.Create(Self);
        lQryBusca.Database := dmCupomFiscal.DataBase;
        try
          lQryBusca.Close;
          lQryBusca.sql.Clear;
          lQryBusca.sql.Add('Select * from temp_bar_Comandas_item where ID = :pID');
          lQryBusca.ParamByName('pID').AsString := cdsListaComandasID.asSTring;
          lQryBusca.Open;
          if(lQryBusca.IsEmpty)then
          begin
            lQryBusca.Close;
            lQryBusca.sql.Clear;
            lQryBusca.sql.Add('Select * from bar_Comandas_item where ID = :pID');
            lQryBusca.ParamByName('pID').AsString := cdsListaComandasID.asSTring;
            lQryBusca.Open;
          end;
          lQryBusca.First;
          lQryBusca.FetchAll;
          if not(lQryBusca.IsEmpty)then
          begin
            while not(lQryBusca.Eof)do
            begin
              frmLoja.edtMercadoria.Text   := Trim(lQryBusca.FieldByName('MERCADORIA').AsString);
              frmLoja.reQuantidade.Value   := lQryBusca.FieldByName('QUANTIDADE').AsCurrency;
              frmLoja.reVlrUnitario.Value  := lQryBusca.FieldByName('VLR_UNITARIO').AsCurrency;
              frmLoja.reVlrDesconto.Value  := 0;
              frmLoja.reTotal.Value        := lQryBusca.FieldByName('VLR_TOTAL').AsCurrency;
              frmLoja.ID_Comanda           := cdsListaComandasID.asString;
              frmLoja.edtMercadoriaExit(Self);
              frmLoja.subtotal;
              frmLoja.btnVende.Click;
              lQryBusca.Next;
            end;
          end;
        finally
          lQryBusca.Free;
        end;
      end
      else
      begin
        MoverComandasInseridas(cdsListaComandasID.asSTring, 0);
        lQryDelete := TIBQuery.Create(Self);
        try
          lQryDelete.Database := dmCupomFiscal.DataBase;
          lQryDelete.SQL.add('Delete from TEMP_BAR_COMANDAS where ID = '+QuotedStr(cdsListaComandasID.asSTring));
          lQryDelete.ExecSQL;
          lQryDelete.Close;
          dmCupomFiscal.Transaction.CommitRetaining;
        finally
          lQryDelete.Free;
        end;
      end;
      cdsListaComandas.Next;
    end;
  end;}
end;

procedure TFrmListaComandas.dbgListaComandasDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
var
  lCaixa: String;
begin
  if not(cdsListaComandas.IsEmpty) then
  begin
    lCaixa := VerificarComandaVinculada(cdsListaComandasID.AsString);
    if(lCaixa <> '') and (lCaixa <> FormatFloat('0000',FRENTE_CAIXA.Caixa))Then
    begin
      dbgListaComandas.Canvas.Brush.Color := HexToTColor('E8E8E8');
      dbgListaComandas.Canvas.Font.Color := HexToTColor('9C9C9C');
      dbgListaComandas.Canvas.FillRect(Rect);
      dbgListaComandas.DefaultDrawColumnCell(Rect, DataCol, Column, State);
    end;
    if LowerCase(Column.FieldName) = 'status' then
    begin
      dbgListaComandas.Canvas.Brush.Color := dbgListaComandas.Color;
      dbgListaComandas.Canvas.FillRect(Rect);
      dbgListaComandas.DefaultDrawColumnCell(Rect,DataCol,Column,State);
      dbgListaComandas.Canvas.FillRect(Rect);
      imgListComandas.Draw(dbgListaComandas.Canvas,Rect.Left+02,Rect.Top+1,
        strToInt(cdsListaComandasSTATUS.Value));
    end;
    if LowerCase(Column.FieldName) = 'pago' then
    begin
      dbgListaComandas.Canvas.Brush.Color := dbgListaComandas.Color;
      dbgListaComandas.Canvas.FillRect(Rect);
      dbgListaComandas.DefaultDrawColumnCell(Rect,DataCol,Column,State);
      dbgListaComandas.Canvas.FillRect(Rect);
      imgListComandas.Draw(dbgListaComandas.Canvas,Rect.Left+02,Rect.Top+1,
        strToInt(cdsListaComandasPAGO.Value));
    end;
  end;
end;

procedure TFrmListaComandas.dbgListaComandasKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  lCaixa : String;
begin
  if not(cdsListaComandas.IsEmpty)then
  begin
    if(Key = VK_SPACE)then
    begin
      //Verifica se a comanda ja n�o esta inserida em outro caixa...
      lCaixa := VerificarComandaVinculada(cdsListaComandasID.AsString);
      if(lCaixa = '') or (lCaixa = FormatFloat('0000',FRENTE_CAIXA.Caixa))Then
      begin
        cdsListaComandas.Edit;
        if(cdsListaComandasSTATUS.Value = '0')Then
        begin
          cdsListaComandasSTATUS.Value := '1';
          Inc(Qtd);
          Vlr := Vlr + cdsListaComandasVLR_TOTAL.Value;
          lblQtd.Caption := IntToStr(Qtd);
          lblValor.Caption := FormatFloat('R$ ###,##0.00', vlr);
        end
        else
        begin
          cdsListaComandasSTATUS.Value := '0';
          Dec(Qtd);
          Vlr := Vlr - cdsListaComandasVLR_TOTAL.Value;
          lblQtd.Caption := IntToStr(Qtd);
          lblValor.Caption := FormatFloat('R$ ###,##0.00', vlr);
        end;
        cdsListaComandas.Post;
      end;
    end;
  end;
end;

procedure TFrmListaComandas.dbgListaComandasMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  lCaixa: String;
begin
  if not(cdsListaComandas.IsEmpty)then
  begin
    if (dbgListaComandas.MouseCoord(X, Y).X in [1]) and
      not (dbgListaComandas.MouseCoord(X, Y).Y in [0]) then
    begin
      lCaixa := VerificarComandaVinculada(cdsListaComandasID.AsString);
      if(lCaixa = '') or (lCaixa = FormatFloat('0000',FRENTE_CAIXA.Caixa))Then
      begin
        cdsListaComandas.Edit;
        if(cdsListaComandasSTATUS.Value = '0')Then
        begin
          cdsListaComandasSTATUS.Value := '1';
          Inc(Qtd);
          Vlr := Vlr + cdsListaComandasVLR_TOTAL.Value;
          lblQtd.Caption := IntToStr(Qtd);
          lblValor.Caption := FormatFloat('R$ ###,##0.00', vlr);
        end
        else
        begin
          cdsListaComandasSTATUS.Value := '0';
          Dec(Qtd);
          Vlr := Vlr - cdsListaComandasVLR_TOTAL.Value;
          lblQtd.Caption := IntToStr(Qtd);
          lblValor.Caption := FormatFloat('R$ ###,##0.00', vlr);
        end;
        cdsListaComandas.Post;
      end;
    end;
    if (dbgListaComandas.MouseCoord(X, Y).X in [6]) and
      not (dbgListaComandas.MouseCoord(X, Y).Y in [0]) then
    begin
      lCaixa := VerificarComandaVinculada(cdsListaComandasID.AsString);
      if(lCaixa = '') or (lCaixa = FormatFloat('0000',FRENTE_CAIXA.Caixa))Then
      begin
        cdsListaComandas.Edit;
        if(cdsListaComandasPAGO.Value = '0')Then
          cdsListaComandasPago.Value := '1'
        else
          cdsListaComandasPago.Value := '0';
        cdsListaComandas.Post;
      end;
    end;
  end;
end;

procedure TFrmListaComandas.dbgListaComandasTitleClick(Column: TColumn);
var
  lCaixa: String;
begin
  if not(cdsListaComandas.IsEmpty)then
  begin
    if LowerCase(Column.FieldName) = 'status' then
    begin
      cdsListaComandas.First;
      while not(cdsListaComandas.eof) do
      begin
        lCaixa := VerificarComandaVinculada(cdsListaComandasID.AsString);
        if(lCaixa = '') or (lCaixa = FormatFloat('0000',FRENTE_CAIXA.Caixa))Then
        begin
          cdsListaComandas.Edit;
          if(cdsListaComandasSTATUS.Value = '0')Then
          begin
            cdsListaComandasSTATUS.Value := '1';
            Inc(Qtd);
            Vlr := Vlr + cdsListaComandasVLR_TOTAL.Value;
            lblQtd.Caption := IntToStr(Qtd);
            lblValor.Caption := FormatFloat('R$ ###,##0.00', vlr);
          end
          else
          begin
            cdsListaComandasSTATUS.Value := '0';
            Dec(Qtd);
            Vlr := Vlr - cdsListaComandasVLR_TOTAL.Value;
            lblQtd.Caption := IntToStr(Qtd);
            lblValor.Caption := FormatFloat('R$ ###,##0.00', vlr);
          end;
          cdsListaComandas.Post;
        end;
        cdsListaComandas.next;
      end;
    end;
  end;
end;

procedure TFrmListaComandas.dgbListaMercadoriasDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if not(cdsMercadorias.IsEmpty) then
  begin
    if LowerCase(Column.FieldName) = 'pago' then
    begin
      dgbListaMercadorias.Canvas.Brush.Color := dgbListaMercadorias.Color;
      dgbListaMercadorias.Canvas.FillRect(Rect);
      dgbListaMercadorias.DefaultDrawColumnCell(Rect,DataCol,Column,State);
      dgbListaMercadorias.Canvas.FillRect(Rect);
      imgListComandas.Draw(dgbListaMercadorias.Canvas,Rect.Left+02,Rect.Top+1,
        cdsMercadoriasPAGO.Value);
    end;
  end;

end;

procedure TFrmListaComandas.dgbListaMercadoriasKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if not(cdsMercadorias.IsEmpty)then
  begin
    if(Key = VK_SPACE)then
    begin
      cdsMercadorias.Edit;
      if(cdsMercadoriasPAGO.Value = 0)Then
        cdsMercadoriasPAGO.Value := 1
      else
        cdsMercadoriasPAGO.Value := 0;
      cdsMercadorias.Post;
    end;
  end;
end;

procedure TFrmListaComandas.dgbListaMercadoriasMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if not(cdsMercadorias.IsEmpty)then
  begin
    if (dgbListaMercadorias.MouseCoord(X, Y).X in [1]) and
      not (dgbListaMercadorias.MouseCoord(X, Y).Y in [0]) then
    begin
      cdsMercadorias.Edit;
      if(cdsMercadoriasPAGO.Value = 0)Then
        cdsMercadoriasPAGO.Value := 1
      else
        cdsMercadoriasPAGO.Value := 0;
      cdsMercadorias.Post;
    end;
  end;
end;

procedure TFrmListaComandas.dgbListaMercadoriasTitleClick(Column: TColumn);
begin
  if not(cdsMercadorias.IsEmpty)then
  begin
    cdsMercadorias.first;
    while not(cdsMercadorias.Eof) do
    begin
      cdsMercadorias.Edit;
      if(cdsMercadoriasPAGO.Value = 0)Then
        cdsMercadoriasPAGO.Value := 1
      else
        cdsMercadoriasPAGO.Value := 0;
      cdsMercadorias.Post;
      cdsMercadorias.Next;
    end;
  end;
end;

procedure TFrmListaComandas.DeletarComandasPreInseridas;
var
  lQryDelete: TIBQuery;
begin
  lQryDelete := TIBQuery.Create(Self);
  try
    lQryDelete.Database := dmCupomFiscal.DataBase;
    lQryDelete.SQL.add('Delete from TEMP_NFCE_ITEM where Caixa = :pCaixa and ID_COMANDA is not null');
    lQryDelete.ParamByName('pCaixa').asString := FormatFloat('0000',FRENTE_CAIXA.Caixa);
    lQryDelete.ExecSQL;
    lQryDelete.Close;
  finally
    lQryDelete.Free;
  end;
end;

procedure TFrmListaComandas.dsComandasUpdateData(Sender: TObject);
var
  lQrBusca: TIBQuery;
begin
  if((cdsListaComandasStatus.Value = '1') or (cdsListaComandasPAGO.Value = '1'))Then
  begin
    while cdsMercadorias.Locate('ID_COMANDA',cdsListaComandasID.AsString,[loCaseInsensitive]) do
      cdsMercadorias.Delete;
    lQrBusca := TIBQuery.create(nil);
    try
      lQrBusca.Database := dmCupomFiscal.DataBase;
      lQrBusca.SQL.add('Select i.*, m.Descricao from temp_Bar_Comandas_Item i '+
        'Left outer join est_mercadorias m on m.ID = i.Mercadoria where i.ID =:pID and ((i.pago <> ''S'' and i.pago <> ''X'') or i.Pago is null)');
      lQrBusca.ParamByName('pID').asInteger := cdsListaComandasID.AsInteger;
      lQrBusca.Open;
      if not(lQrBusca.IsEmpty)Then
      begin
        lQrBusca.First;
        while not(lQrBusca.eof) do
        begin
          cdsMercadorias.Open;
          cdsMercadorias.Insert;
          cdsMercadoriasMERCADORIA.AsString     := lQrBusca.FieldByName('MERCADORIA').asString;
          cdsMercadoriasDESCRICAO.AsString      := lQrBusca.FieldByName('DESCRICAO').asString;
          cdsMercadoriasQUANTIDADE.AsCurrency   := lQrBusca.FieldByName('QUANTIDADE').AsCurrency;
          cdsMercadoriasVLR_UNITARIO.AsCurrency := lQrBusca.FieldByName('VLR_UNITARIO').asCurrency;
          cdsMercadoriasVLR_TOTAL.AsCurrency    := lQrBusca.FieldByName('VLR_TOTAL').asCurrency;
          cdsMercadoriasID_COMANDA.asString     := lQrBusca.FieldByName('ID').asString;
          cdsMercadoriasITEM.asString           := lQrBusca.FieldByName('ITEM').asString;
          if(VerificaItemPreInserido(cdsMercadoriasID_COMANDA.asString,cdsMercadoriasITEM.asString)or
          (cdsListaComandasPAGO.Value = '1'))then
            cdsMercadoriasPAGO.Value := 1
          else
            cdsMercadoriasPAGO.Value := 0;
          cdsMercadorias.Post;
          lQrBusca.Next;
        end;
      end;
      lQrBusca.Close;
    finally
      lQrBusca.Free;
    end;
  end
  else
  begin
    if((cdsListaComandasStatus.Value <> '1') and (cdsListaComandasPAGO.Value <> '1'))Then
    begin
      while cdsMercadorias.Locate('ID_COMANDA',cdsListaComandasID.AsString,[loCaseInsensitive]) do
        cdsMercadorias.Delete;
    end;
  end;
end;

procedure TFrmListaComandas.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  //Funcionario
  If (key=vk_F12) and (edtFunIni.Focused) then Label2Click(Action);

  //Sair
  If (key=VK_ESCAPE) then Close;
end;

procedure TFrmListaComandas.FormShow(Sender: TObject);
begin
  Qtd := 0;
  Vlr := 0;
  edtDtIni.Text := DateToStr(now-1);
  edtDtFim.Text := DateToStr(now);
  Pesquisar;
end;

procedure TFrmListaComandas.Imprimir(lCdsDados: TClientDataSet);
var
  lFrmImp : TfrmImpCompComanda;
begin
  lFrmImp := TfrmImpCompComanda.Create(Self, lCdsDados);
  try
    lFrmImp.rlVenda.Preview();
  finally
    lFrmImp.Free
  end;
end;

procedure TFrmListaComandas.Label2Click(Sender: TObject);
begin
  frmPesquisa := tfrmPesquisa.Create(Self,'Select id, nome From Funcionarios Where (Nome >=:pTexto) and (Ativo=''S'') Order By Nome','Select Id, Nome From Funcionarios Where (Nome like :pTexto) and (Ativo=''S'') Order By Nome');

  frmPesquisa.DBGrid1.Columns.Add;
  frmPesquisa.DBGrid1.Columns[0].Title.Caption:='C�digo';
  frmPesquisa.DBGrid1.Columns[0].Width:=42;
  frmPesquisa.DBGrid1.Columns[0].FieldName:='id';

  frmPesquisa.DBGrid1.Columns.Add;
  frmPesquisa.DBGrid1.Columns[1].Title.Caption:='Nome';
  frmPesquisa.DBGrid1.Columns[1].Width:=337;
  frmPesquisa.DBGrid1.Columns[1].FieldName:='Nome';
  try
    if (frmPesquisa.ShowModal=mrOK) then
    begin
      edtFunIni.Text := FrmPesquisa.IBQuery1.FieldByName('id').Value;
      edtFunFim.Text := FrmPesquisa.IBQuery1.FieldByName('id').Value;
    end;
  finally
    frmPesquisa.Free;
  end;
end;

procedure TFrmListaComandas.MoverComandasInseridas(pID: String; pEL_Chave: integer);
begin
  dmCupomFiscal.MoverComandasInseridas(pID, pEL_Chave);
end;

procedure TFrmListaComandas.Pesquisar;
begin
  qryListaComandas.Close;
  qryListaComandas.Sql.clear;
  if not(ckbComandasPagas.Checked)then
  begin
    qryListaComandas.sql.add('Select t.*, ''0'' as Status, ''0'' as Pago, F.Nome, sum(i.Vlr_Total) as Resta from TEMP_BAR_COMANDAS t '+
      'left outer join Funcionarios F on f.ID = t.ID_FUNCIONARIO '+
      'left outer join TEMP_BAR_COMANDAS_ITEM i on i.ID = t.ID and (i.Pago = ''N'' or i.Pago is null) '+
      'where Cast(t.Data_Hora as Date) >= :pdtI and Cast(t.Data_Hora as Date) <= :pdtF '+
      'and t.ID_funcionario >= :pFunI and t.ID_funcionario <= :pFunF ');
  end
  else
  begin
    qryListaComandas.sql.add('Select t.*, ''0'' as Status, ''0'' as Pago, F.Nome, sum(i.Vlr_Total) as Resta from BAR_COMANDAS t '+
      'left outer join Funcionarios F on f.ID = t.ID_FUNCIONARIO '+
      'left outer join TEMP_BAR_COMANDAS_ITEM i on i.ID = t.ID '+
      'where Cast(t.Data_Hora_AB as Date) >= :pdtI and Cast(t.Data_Hora_AB as Date) <= :pdtF '+
      'and t.ID_funcionario >= :pFunI and t.ID_funcionario <= :pFunF and t.EL_CHAVE = 0 ');
  end;
  if(trim(edtComIni.Text) <> '') and (trim(edtComFim.Text) <> '')Then
    qryListaComandas.sql.add('and Trim(t.ID) >= :pIdI and Trim(t.ID) <= :pIdF ');
  qryListaComandas.sql.add('Group by 1,2,3,4,5,6,7,8');
  qryListaComandas.paramByName('pdtI').asDate := StrToDate(edtDtIni.Text);
  qryListaComandas.paramByName('pdtF').asDate := StrToDate(edtDtFim.Text);
  qryListaComandas.paramByName('pFunI').asInteger := StrToInt(edtFunIni.Text);
  qryListaComandas.paramByName('pFunF').asInteger := StrToInt(edtFunFim.Text);
  if(trim(edtComIni.Text) <> '') and (trim(edtComFim.Text) <> '')Then
  begin
    qryListaComandas.paramByName('pIdI').asString  := trim(edtComIni.Text);
    qryListaComandas.paramByName('pIdF').asString  := trim(edtComFim.Text) ;
  end;
  if not(ckbComandasPagas.Checked)then
    qryListaComandas.sql.add('Order By T.Data_Hora')
  else
    qryListaComandas.sql.add('Order By T.Data_Hora_AB');
  qryListaComandas.Open;
  qryListaComandas.FetchAll;
  qryListaComandas.first;
  cdsListaComandas.EmptyDataSet;
  while not(qryListaComandas.eof) do
  begin
    cdsListaComandas.Open;
    cdsListaComandas.Insert;
    cdsListaComandasID.asString := qryListaComandas.fieldByName('ID').asString;
    if not(ckbComandasPagas.Checked)then
      cdsListaComandasDATA_HORA.AsDateTime := qryListaComandas.fieldByName('DATA_HORA').AsDateTime
    else
      cdsListaComandasDATA_HORA.AsDateTime := qryListaComandas.fieldByName('DATA_HORA_AB').AsDateTime;
    cdsListaComandasVLR_TOTAL.AsCurrency := qryListaComandas.fieldByName('VLR_TOTAL').AsCurrency;
    cdsListaComandasOBSERVACAO.asString := qryListaComandas.fieldByName('OBSERVACAO').AsString;
    if(dmCupomFiscal.dbTemp_NFCE_Item.Locate('ID_COMANDA',cdsListaComandasID.AsString,[loCaseInsensitive]))then
    begin
      cdsListaComandasSTATUS.asString := '1';
      Inc(Qtd);
      lblQtd.Caption := IntToStr(Qtd);
      Vlr := Vlr + cdsListaComandasVLR_TOTAL.Value;
      lblValor.Caption := FormatFloat('R$ ###,##0.00', vlr);
    end
    else
      cdsListaComandasSTATUS.asString := '0';
    if not(ckbComandasPagas.Checked)then
      cdsListaComandasPAGO.AsString := qryListaComandas.fieldByName('Pago').asString
    else
      cdsListaComandasPAGO.AsString := '1';
    cdsListaComandasNOME.asString := qryListaComandas.fieldByName('Nome').asString;
    cdsListaComandasID_FUNCIONARIO.Value := qryListaComandas.fieldByName('ID_FUNCIONARIO').AsInteger;
    cdsListaComandasRESTA.Value := qryListaComandas.fieldByName('RESTA').AsCurrency;
    cdsListaComandas.Post;
    qryListaComandas.next;
  end;
end;

function TFrmListaComandas.VerificaItemPreInserido(pID, pItem: String): Boolean;
var
  lQrBusca: TIBQuery;
begin
  lQrBusca := TIBQuery.Create(Self);
  Try
    lQrBusca.Database := dmCupomFiscal.DataBase;
    lQrBusca.SQL.Add('Select * from TEMP_NFCE_ITEM where ID_COMANDA = :pID and COMANDA_ITEM = :pITEM');
    lQrBusca.ParamByName('pID').asString := pID;
    lQrBusca.ParamByName('pItem').asString := pItem;
    lQrBusca.Open;
    Result := not lQrBusca.IsEmpty;
    lQrBusca.Close;
  Finally
    lQrBusca.Free;
  End;
end;

function TFrmListaComandas.VerificarComandaVinculada(
  ID_Comanda: String): String;
var
  lQrBusca: TIBQuery;
begin
  lQrBusca := TIBQuery.Create(Self);
  Try
    lQrBusca.Database := dmCupomFiscal.DataBase;
    lQrBusca.SQL.Add('Select Caixa from TEMP_NFCE_ITEM '+
      'where ID_COMANDA ='+QuotedStr(ID_Comanda));
    lQrBusca.Open;
    Result := lQrBusca.FieldByName('Caixa').asString;
    lQrBusca.Close;
  Finally
    lQrBusca.Free;
  End;
end;

end.

