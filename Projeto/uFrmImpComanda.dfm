object frmImpCompComanda: TfrmImpCompComanda
  Left = 68
  Top = 22
  Caption = 'Impress'#227'o de Comanda'
  ClientHeight = 622
  ClientWidth = 906
  Color = clWindow
  Ctl3D = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = True
  Position = poDesigned
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object rlVenda: TRLReport
    Left = 100
    Top = 8
    Width = 280
    Height = 1512
    Margins.LeftMargin = 0.610000000000000000
    Margins.TopMargin = 2.000000000000000000
    Margins.RightMargin = 0.610000000000000000
    Margins.BottomMargin = 0.000000000000000000
    AllowedBands = [btHeader, btDetail, btSummary, btFooter]
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -9
    Font.Name = 'Arial'
    Font.Style = []
    PageSetup.PaperSize = fpCustom
    PageSetup.PaperWidth = 74.000000000000000000
    PageSetup.PaperHeight = 400.000000000000000000
    PrintDialog = False
    ShowProgress = False
    object RLBand4: TRLBand
      Left = 2
      Top = 8
      Width = 276
      Height = 81
      AutoSize = True
      BandType = btHeader
      object RLPanel1: TRLPanel
        Left = 0
        Top = 0
        Width = 276
        Height = 81
        Align = faTop
        AutoExpand = True
        AutoSize = True
        object RLLabel2: TRLLabel
          Left = 0
          Top = 31
          Width = 276
          Height = 12
          Align = faTop
          Alignment = taCenter
          Caption = 
            'CNPJ: 22.222.222/22222-22  IE:223.233.344.233 IM:2323.222.333.23' +
            '3'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Layout = tlBottom
          ParentFont = False
          BeforePrint = lEmitCNPJ_IE_IMBeforePrint
        end
        object RLMemo1: TRLMemo
          Left = 0
          Top = 43
          Width = 276
          Height = 30
          Align = faTop
          Alignment = taCenter
          Behavior = [beSiteExpander]
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Lines.Strings = (
            'Endere'#231'o')
          ParentFont = False
          BeforePrint = lEnderecoBeforePrint
        end
        object RLDraw4: TRLDraw
          Left = 0
          Top = 73
          Width = 276
          Height = 8
          Align = faTop
          DrawKind = dkLine
          Pen.Width = 2
        end
        object RLImage1: TRLImage
          Left = 0
          Top = 0
          Width = 276
          Height = 1
          Align = faTop
          AutoSize = True
          Center = True
          Scaled = True
          Transparent = False
        end
        object RLMemo3: TRLMemo
          Left = 0
          Top = 1
          Width = 276
          Height = 18
          Align = faTop
          Alignment = taCenter
          Behavior = [beSiteExpander]
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Layout = tlCenter
          Lines.Strings = (
            'Nome Fantasia')
          ParentFont = False
          BeforePrint = lNomeFantasiaBeforePrint
        end
        object RLMemo4: TRLMemo
          Left = 0
          Top = 19
          Width = 276
          Height = 12
          Align = faTop
          Alignment = taCenter
          Behavior = [beSiteExpander]
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Lines.Strings = (
            'Raz'#227'o Social')
          ParentFont = False
          BeforePrint = lRazaoSocialBeforePrint
        end
      end
    end
    object RLBand5: TRLBand
      Left = 2
      Top = 89
      Width = 276
      Height = 44
      AutoSize = True
      BandType = btHeader
      object rlLabelComanda: TRLLabel
        Left = 0
        Top = 0
        Width = 276
        Height = 16
        Align = faTop
        Alignment = taCenter
        Caption = 'Comprovante de emiss'#227'o de Comanda'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object RLDraw6: TRLDraw
        Left = 0
        Top = 16
        Width = 276
        Height = 8
        Align = faTop
        DrawKind = dkLine
        Pen.Width = 2
      end
      object RLLabel3: TRLLabel
        Left = 0
        Top = 24
        Width = 276
        Height = 12
        Align = faTop
        Caption = '#ITEM|COD|DESC|QTD|UN| VL UN R$|(VLTR R$)*|VL ITEM R$'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        Layout = tlBottom
        ParentFont = False
      end
      object RLDraw7: TRLDraw
        Left = 0
        Top = 36
        Width = 276
        Height = 8
        Align = faTop
        DrawKind = dkLine
        Pen.Width = 2
      end
    end
    object RLBand8: TRLBand
      Left = 2
      Top = 133
      Width = 276
      Height = 24
      AutoSize = True
      BandType = btHeader
      object mLinhaItem: TRLMemo
        Left = 0
        Top = 0
        Width = 276
        Height = 24
        Align = faTop
        Behavior = [beSiteExpander]
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Courier New'
        Font.Style = [fsBold]
        Lines.Strings = (
          '9999999999999 DESCRICAO DO PRODUTO 99,999 UN x 999,999 (99,99)')
        ParentFont = False
      end
    end
    object RLBand9: TRLBand
      Left = 2
      Top = 157
      Width = 276
      Height = 40
      BandType = btHeader
      object RLDraw8: TRLDraw
        Left = 0
        Top = 0
        Width = 276
        Height = 3
        Align = faTop
        DrawKind = dkLine
        Pen.Width = 2
      end
      object RLLabel4: TRLLabel
        Left = 2
        Top = 3
        Width = 102
        Height = 12
        Caption = 'VALOR SUB-TOTAL R$'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object RLLabel5: TRLLabel
        Left = 226
        Top = 3
        Width = 44
        Height = 11
        Alignment = taRightJustify
        Caption = '99.999,99'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object RLLabel6: TRLLabel
        Left = 2
        Top = 15
        Width = 104
        Height = 12
        Caption = 'VALOR DESCONTO R$'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object RLLabel7: TRLLabel
        Left = 226
        Top = 15
        Width = 44
        Height = 11
        Alignment = taRightJustify
        Caption = '99.999,99'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object RLLabel8: TRLLabel
        Left = 2
        Top = 27
        Width = 80
        Height = 12
        Caption = 'VALOR TOTAL R$'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object RLLabel9: TRLLabel
        Left = 226
        Top = 27
        Width = 44
        Height = 11
        Alignment = taRightJustify
        Caption = '99.999,99'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
    end
    object RLBand10: TRLBand
      Left = 2
      Top = 197
      Width = 276
      Height = 42
      BandType = btHeader
      object RLDraw9: TRLDraw
        Left = 0
        Top = 0
        Width = 276
        Height = 2
        Align = faTop
        DrawKind = dkLine
        Pen.Width = 2
      end
      object RLDraw10: TRLDraw
        Left = 0
        Top = 40
        Width = 276
        Height = 2
        Align = faBottom
        DrawKind = dkLine
        Pen.Style = psClear
        Pen.Width = 2
      end
      object RLMemo6: TRLMemo
        Left = 72
        Top = 9
        Width = 132
        Height = 24
        Align = faCenter
        Alignment = taCenter
        Behavior = [beSiteExpander]
        Lines.Strings = (
          'DOCUMENTO N'#195'O FISCAL'
          'EXIJA NOTA FISCAL')
      end
    end
  end
end
