unit uFrmClifor;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.DBCtrls,
  Vcl.Mask, Vcl.Buttons, Data.DB, acbrValidador, PngBitBtn, ACBrTEFDClass,
  IBX.IBCustomDataSet, IBX.IBQuery, IBX.IBSQL, uFrmListaMotorista,
  ACBrNFeWebServices, uCtrlNFCe;

type
  TfrmClifor = class(TForm)
    pnlCabecalho: TPanel;
    pnlCentral: TPanel;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    Label10: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label13: TLabel;
    Label11: TLabel;
    Label6: TLabel;
    lcmbMunicipio: TDBLookupComboBox;
    edtCEP: TDBEdit;
    edtEndereco: TDBEdit;
    edtNumeroEnd: TDBEdit;
    edtBairro: TDBEdit;
    lbRecado: TLabel;
    rgIndicadorIEDest: TDBRadioGroup;
    edtCPFCNPJ: TDBEdit;
    Label12: TLabel;
    edtIE: TDBEdit;
    Label16: TLabel;
    edtPlaca: TDBEdit;
    lblPlaca: TLabel;
    edtKM: TDBEdit;
    lblKM: TLabel;
    edtClienteNome: TDBEdit;
    Label9: TLabel;
    dsTempECF: TDataSource;
    pnlRodape: TPanel;
    Label7: TLabel;
    Label8: TLabel;
    pnlFinalizar: TPanel;
    btnSalvar: TBitBtn;
    pnlFinalizarOrganiza: TPanel;
    btnFinalizar: TSpeedButton;
    btnIdentConsumi: TSpeedButton;
    lblNCaixa: TDBText;
    lblData: TDBText;
    cmbUF: TComboBox;
    btnLimpaDados: TPngBitBtn;
    SpeedButton1: TSpeedButton;
    btnCadastroRapido: TSpeedButton;
    edtMotorista: TDBEdit;
    lblMotorista: TLabel;
    btnCadastrarCNPJ: TPngBitBtn;
    procedure FormResize(Sender: TObject);
    procedure btnFinalizarClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure btnCancelarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure edtClienteNomeEnter(Sender: TObject);
    procedure edtClienteNomeExit(Sender: TObject);
    procedure edtCPFCNPJExit(Sender: TObject);
    procedure edtIEExit(Sender: TObject);
    procedure edtPlacaExit(Sender: TObject);
    procedure edtKMExit(Sender: TObject);
    procedure edtCEPExit(Sender: TObject);
    procedure edtEnderecoExit(Sender: TObject);
    procedure edtNumeroEndExit(Sender: TObject);
    procedure edtBairroExit(Sender: TObject);
    procedure cmbUFExit(Sender: TObject);
    procedure btnSalvarClick(Sender: TObject);
    procedure Label9Click(Sender: TObject);
    procedure lblPlacaClick(Sender: TObject);
    procedure btnLimpaDadosClick(Sender: TObject);
    procedure cmbUFClick(Sender: TObject);
    procedure btnIdentConsumiClick(Sender: TObject);
    procedure btnCadastroRapidoClick(Sender: TObject);
    procedure dsTempECFDataChange(Sender: TObject; Field: TField);
    procedure edtMotoristaExit(Sender: TObject);
    procedure lblMotoristaClick(Sender: TObject);
    procedure edtMotoristaEnter(Sender: TObject);
    procedure btnCadastrarCNPJClick(Sender: TObject);
  private
    procedure esconderComponentes;
    procedure DesabilitaHabilitaCampos(acao:String);
    procedure setCores;
    Function ValidarInformacoes(): Boolean;
    Function ValidarInformacoesPreCadastro(pException: Boolean): Boolean;
    Function geraID: Integer;
    Function getIndIE(pFJ, pIE: String): String;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmClifor: TfrmClifor;

implementation

Uses
  uFrmLoja, uFrmSupermercado, uFrmPosto, uFuncoes, uFrmPrincipal, uDMCupomFiscal,
  uDMComponentes, uRotinasGlobais, uFrmPesquisaCliente, uFrmPesquisaGenerica,
  uFrmFim, uFrmPesquisaPlaca, uFrmCliforPreCadastro, uFrmPesquisa, uFrmNFCe;

{$R *.dfm}

procedure TfrmClifor.btnCadastrarCNPJClick(Sender: TObject);
var
  lCtrlNFCe: TCtrlNFCe;
  lQryInsere: TIBQuery;
  xID: Integer;
begin
  if(edtCPFCNPJ.Text = '')Then
    raise Exception.Create('CNPJ/CPF n�o informado');
  Try
    lCtrlNFCe := TCtrlNFCe.Create(dmCupomFiscal.DataBase, dmCupomFiscal.Transaction,
      frmNFCe.qConfigNFCeLOCAL.AsInteger);
    Try
      if(lCtrlNFCe.ConsultarCliente(edtCPFCNPJ.Text, cmbUF.Text))then
      begin
        lQryInsere := TIBQuery.Create(nil);
        try
          lQryInsere.Database := dmCupomFiscal.DataBase;
          lQryInsere.SQL.Add('INSERT INTO Clifor(ID, CF, NOME, MUNICIPIO, PAIS, BAIRRO, ENDERECO, CEP, '+
            'LIMITE, JURO, DIASPRAZO, LEITE_NF, LEITE_BONUS, LEITE_PGTO,LEITE_ROTA_COLETA,LEITE_VALOR,DT_ADMISSAO, '+
            'CARTEIRA_CLIENTE, IE, UF, ATIVO, CONTRIBUINTE, CNPJCPF, FONE, FAX, FJ, RG, '+
            'E_MAIL, NUMERO_END, TPRECO, ESTADO_CIVIL, CELULAR, CCusto, INDIEDEST, APROVADO, NF_FATURA, NF_FRENTE_CAIXA) '+
            'VALUES (:ID, ''C'', :NOME, :MUNICIPIO, :PAIS, :BAIRRO, '+
            ':ENDERECO, :CEP, :LIMITE, 0, :DIASPRAZO, ''N'', ''N'', ''N'', 0, 0, :DTADMISAO, 0,'+
            ':IE, :UF, ''S'', ''S'', :CNPJCPF, :FONE, null, :FJ, :RG, :E_MAIL, '+
            ':NUMERO_END, 0, 0, :CELULAR, :CCUSTO, :INDIE, ''S'', ''N'', ''N'')');
          xID := geraID;
          lQryInsere.ParamByName('ID').AsInteger := xID;
          lQryInsere.ParamByName('NOME').AsString := lCtrlNFCe.ACBrNFe.WebServices.ConsultaCadastro.RetConsCad.InfCad.Items[0].xNome;
          lQryInsere.ParamByName('MUNICIPIO').AsInteger := lCtrlNFCe.ACBrNFe.WebServices.ConsultaCadastro.RetConsCad.InfCad.Items[0].cMun;
          lQryInsere.ParamByName('PAIS').AsInteger := 1058;
          lQryInsere.ParamByName('BAIRRO').AsString := lCtrlNFCe.ACBrNFe.WebServices.ConsultaCadastro.RetConsCad.InfCad.Items[0].xBairro;
          lQryInsere.ParamByName('ENDERECO').AsString := lCtrlNFCe.ACBrNFe.WebServices.ConsultaCadastro.RetConsCad.InfCad.Items[0].xLgr;
          lQryInsere.ParamByName('CEP').AsString := formataCEP(IntToStr(lCtrlNFCe.ACBrNFe.WebServices.ConsultaCadastro.RetConsCad.InfCad.Items[0].CEP));
          lQryInsere.ParamByName('LIMITE').AsCurrency := 0;
          lQryInsere.ParamByName('DIASPRAZO').AsInteger := 30;
          lQryInsere.ParamByName('DTADMISAO').AsDateTime := Now;
          lQryInsere.ParamByName('IE').AsString := lCtrlNFCe.ACBrNFe.WebServices.ConsultaCadastro.RetConsCad.InfCad.Items[0].IE;
          lQryInsere.ParamByName('UF').AsString := lCtrlNFCe.ACBrNFe.WebServices.ConsultaCadastro.RetConsCad.InfCad.Items[0].UF;
          If Trim(lCtrlNFCe.ACBrNFe.WebServices.ConsultaCadastro.RetConsCad.InfCad.Items[0].CNPJ)<> '' then   //CNPJ
          begin
            lQryInsere.ParamByName('FJ').AsString := 'J';
            lQryInsere.ParamByName('CNPJCPF').AsString := formataCPFCNPJ(lCtrlNFCe.ACBrNFe.WebServices.ConsultaCadastro.RetConsCad.InfCad.Items[0].CNPJ,'J');
            lQryInsere.ParamByName('INDIE').AsString := getIndIE('J',lCtrlNFCe.ACBrNFe.WebServices.ConsultaCadastro.RetConsCad.InfCad.Items[0].IE);
          end
          Else
          begin
            lQryInsere.ParamByName('FJ').AsString := 'F';
            lQryInsere.ParamByName('CNPJCPF').AsString := formataCPFCNPJ(lCtrlNFCe.ACBrNFe.WebServices.ConsultaCadastro.RetConsCad.InfCad.Items[0].CPF,'F');
            lQryInsere.ParamByName('INDIE').AsString := getIndIE('F',lCtrlNFCe.ACBrNFe.WebServices.ConsultaCadastro.RetConsCad.InfCad.Items[0].IE);
          end;
          lQryInsere.ParamByName('NUMERO_END').AsString := lCtrlNFCe.ACBrNFe.WebServices.ConsultaCadastro.RetConsCad.InfCad.Items[0].nro;
          lQryInsere.ParamByName('CCUSTO').AsInteger := CCUSTO.codigo;
          lQryInsere.ExecSQL;
          dmCupomFiscal.Transaction.CommitRetaining;
          lQryInsere.Close;
        finally
          lQryInsere.Free;
        end;
      end
      else
        msgErro(lCtrlNFCe.ACBrNFe.WebServices.ConsultaCadastro.RetConsCad.xMotivo, Application.Title);
    finally
      lCtrlNFCe.Free;
    End;
  except
    on E:Exception do
    begin
      logErros(Sender, caminholog, 'Erro ao cadastrar cliente','Erro ao cadastrar cliente','S',E);
    end;
  End;
end;

procedure TfrmClifor.btnCadastroRapidoClick(Sender: TObject);
var
  lQryInsere: TIBQuery;
  lCtrlPreCadastro: TfrmCliforPreCadastro;
  xID: Integer;
begin
  if(dmCupomFiscal.dbTemp_NFCECLIFOR.Value = 0)Then
  begin
    ValidarInformacoesPreCadastro(True);
    lCtrlPreCadastro := TfrmCliforPreCadastro.Create(Self);
    try
      try
        case lCtrlPreCadastro.ShowModal of
        mrOk:begin
            lQryInsere := TIBQuery.Create(nil);
            try
              lQryInsere.Database := dmCupomFiscal.DataBase;
              lQryInsere.SQL.Add('INSERT INTO Clifor(ID, CF, NOME, MUNICIPIO, PAIS, BAIRRO, ENDERECO, CEP, '+
                'LIMITE, JURO, DIASPRAZO, LEITE_NF, LEITE_BONUS, LEITE_PGTO,LEITE_ROTA_COLETA,LEITE_VALOR,DT_ADMISSAO, '+
                'CARTEIRA_CLIENTE, IE, UF, ATIVO, CONTRIBUINTE, CNPJCPF, FONE, FAX, FJ, RG, '+
                'E_MAIL, NUMERO_END, TPRECO, ESTADO_CIVIL, CELULAR, CCusto, INDIEDEST, APROVADO, NF_FATURA, NF_FRENTE_CAIXA) '+
                'VALUES (:ID, ''C'', :NOME, :MUNICIPIO, :PAIS, :BAIRRO, '+
                ':ENDERECO, :CEP, :LIMITE, 0, :DIASPRAZO, ''N'', ''N'', ''N'', 0, 0, :DTADMISAO, 0,'+
                ':IE, :UF, ''S'', ''S'', :CNPJCPF, :FONE, null, :FJ, :RG, :E_MAIL, '+
                ':NUMERO_END, 0, 0, :CELULAR, :CCUSTO, :INDIE, ''S'', ''N'', ''N'')');
              xID := geraID;
              lQryInsere.ParamByName('ID').AsInteger := xID;
              lQryInsere.ParamByName('NOME').AsString := edtClienteNome.Text;
              lQryInsere.ParamByName('MUNICIPIO').AsInteger := lcmbMunicipio.KeyValue;
              if(cmbUF.Text <> 'EX')Then
                lQryInsere.ParamByName('PAIS').AsInteger := 1058;
              lQryInsere.ParamByName('BAIRRO').AsString := edtBairro.Text;
              lQryInsere.ParamByName('ENDERECO').AsString := edtEndereco.Text;
              lQryInsere.ParamByName('CEP').AsString := edtCEP.Text;
              lQryInsere.ParamByName('LIMITE').AsCurrency := lCtrlPreCadastro.edtLimite.Value;
              lQryInsere.ParamByName('DIASPRAZO').AsInteger := StrToIntDef(lCtrlPreCadastro.edtDiasPrazo.Text,0);
              lQryInsere.ParamByName('DTADMISAO').AsDateTime := Now;
              lQryInsere.ParamByName('IE').AsString := edtIE.Text;
              lQryInsere.ParamByName('UF').AsString := cmbUF.Text;
              lQryInsere.ParamByName('CNPJCPF').AsString := edtCPFCNPJ.Text;
              lQryInsere.ParamByName('FONE').AsString := lCtrlPreCadastro.edtFone.Text;
              If Length(edtCPFCNPJ.Text)>11 then   //CNPJ
                lQryInsere.ParamByName('FJ').AsString := 'J'
              Else
                lQryInsere.ParamByName('FJ').AsString := 'F';
              lQryInsere.ParamByName('RG').AsString := lCtrlPreCadastro.edtRg.Text;
              lQryInsere.ParamByName('E_MAIL').AsString := lCtrlPreCadastro.edtEmail.Text;
              lQryInsere.ParamByName('NUMERO_END').AsString := edtNumeroEnd.Text;
              lQryInsere.ParamByName('CELULAR').AsString := lCtrlPreCadastro.edtCelular.Text;
              lQryInsere.ParamByName('CCUSTO').AsInteger := CCUSTO.codigo;
              lQryInsere.ParamByName('INDIE').AsString := dmCupomFiscal.dbTemp_NFCEINDIEDEST.AsString;
              lQryInsere.ExecSQL;
              lQryInsere.Close;
            finally
              lQryInsere.Free;
            end;
            lbRecado.Caption:= '?';
            if not(dmCupomFiscal.dbTemp_NFCE.State in [dsEdit]) then
              dmCupomFiscal.dbTemp_NFCE.Edit;
            dmCupomFiscal.dbTemp_NFCECLIFOR.AsInteger := xID;
            dmCupomFiscal.dbTemp_NFCE.Post;
            dmCupomFiscal.Transaction.CommitRetaining;
            msgInformacao('Cliente cadastrado com sucesso',Application.Title);
          end;
        end;
      Except
        on E:Exception do
          logErros(Sender, caminhoLog, 'Erro ao cadastrar cliente', 'Erro ao cadastrar cliente','S',E);
      end;
    finally
      lCtrlPreCadastro.Free;
    end;
  end;
end;

procedure TfrmClifor.btnCancelarClick(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

procedure TfrmClifor.btnFinalizarClick(Sender: TObject);
begin
  if(ValidarInformacoes)then
  begin
    if(ValidarInformacoesPreCadastro(False)) and not(dmCupomFiscal.dbTemp_NFCECLIFOR.AsInteger > 0)Then
    begin
      if(msgPergunta('Cliente informado n�o est� cadastrado'+sLineBreak+sLineBreak+
        'Deseja salvar o pr�-cadastro do cliente?',Application.Title))Then
        btnCadastroRapidoClick(Action);
    end;
    btnSalvarClick(Action);
    ModalResult := mrOk;
  end;
end;

procedure TfrmClifor.btnIdentConsumiClick(Sender: TObject);
begin
  if(edtClienteNome.Enabled)then
    edtClienteNome.SetFocus;
end;

procedure TfrmClifor.btnLimpaDadosClick(Sender: TObject);
begin
  if(msgPergunta('Limpar os dados do Cliente?','Confirma��o'))then
  begin
    try
      if not (dmCupomFiscal.dbTemp_NFCE.State in [dsEdit]) then
        dmCupomFiscal.dbTemp_NFCE.Edit;
      dmCupomFiscal.dbTemp_NFCECLIFOR.Value := 0;
      dmCupomFiscal.dbTemp_NFCECF_PLACA.Clear;
      dmCupomFiscal.dbTemp_NFCECF_NOME.Clear;
      dmCupomFiscal.dbTemp_NFCECF_ENDE.Clear;
      dmCupomFiscal.dbTemp_NFCECF_NUMERO_END.Clear;
      dmCupomFiscal.dbTemp_NFCECF_BAIRRO.Clear;
      dmCupomFiscal.dbTemp_NFCECF_CEP.Clear;
      cmbUF.Text := copy(dmCupomFiscal.getMunicipioLocal(FRENTE_CAIXA.CCusto),1,2);
      cmbUFExit(Action);
      dmCupomFiscal.getMunicipios(cmbUF.Text);
      dmCupomFiscal.dbTemp_NFCECF_MUNICIPIO.Clear;
      dmCupomFiscal.dbTemp_NFCECF_CNPJ_CPF.Clear;
      dmCupomFiscal.dbTemp_NFCECF_IE.Clear;
      dmCupomFiscal.dbTemp_NFCEINDIEDEST.Value  := '9'; //9-N�o contribuinte
      dmCupomFiscal.dbTemp_NFCE.Post;
      dmCupomFiscal.Transaction.CommitRetaining;
      dmCupomFiscal.getTemp_NFCE;
      if(edtClienteNome.Enabled)then
        edtClienteNome.SetFocus;
    except
      on E:Exception do
        logErros(Sender, caminholog, 'Erro ao limpar dados do cliente.','Erro ao limpar dados do cliente.','S',E);
    end;
  end;
end;

procedure TfrmClifor.btnSalvarClick(Sender: TObject);
begin
  if not (dmCupomFiscal.dbTemp_NFCE.State in [dsEdit]) then
    dmCupomFiscal.dbTemp_NFCE.Edit;
  dmCupomFiscal.dbTemp_NFCE.Post;
  dmCupomFiscal.Transaction.CommitRetaining;
end;

procedure TfrmClifor.cmbUFClick(Sender: TObject);
begin
  dmCupomFiscal.getMunicipios(cmbUF.Text);
end;

procedure TfrmClifor.cmbUFExit(Sender: TObject);
begin
  dmCupomFiscal.getMunicipios(cmbUF.Text);
end;

procedure TfrmClifor.DesabilitaHabilitaCampos(acao: String);
begin
  if(UpperCase(acao) = 'H')then
  begin
    edtClienteNome.Enabled := True;
    edtCPFCNPJ.Enabled := True;
    edtIE.Enabled := True;
    btnLimpaDados.Enabled := True;
  end
  else
  begin
    //Atividade 1  - Supermercado n�o bloqueia
    //Flag_Cliente - Caso cliente possua pre�o diferenciado em algum produto
    {lauro 23.01.17
    if dmCupomFiscal.ExistePagamentoPrazo(FRENTE_CAIXA.Caixa) or
      ((FRENTE_CAIXA.Atividade <> 1) and
      (dmCupomFiscal.VerificarNFCePossuiItem(FRENTE_CAIXA.Caixa)) and
      (dmCupomFiscal.Flag_Cliente)) then
    }
    if ((FRENTE_CAIXA.Atividade <> 1) and not(dmCupomFiscal.Flag_Cliente)) then
    begin
      edtClienteNome.Enabled := False;
      edtCPFCNPJ.Enabled := False;
      edtIE.Enabled := False;
      btnLimpaDados.Enabled := False;
    end;
  end;
end;

procedure TfrmClifor.dsTempECFDataChange(Sender: TObject; Field: TField);
begin
  btnCadastroRapido.Enabled := (dmCupomFiscal.dbTemp_NFCECLIFOR.AsInteger = 0);
end;

procedure TfrmClifor.edtBairroExit(Sender: TObject);
begin
  Cor_Campo(Sender, HexToTColor(PERSONALIZAR.corFundoCampo));
end;

procedure TfrmClifor.edtCEPExit(Sender: TObject);
var
  txt:string;
begin
  Cor_Campo(Sender, HexToTColor(PERSONALIZAR.corFundoCampo));
  if Trim(edtCEP.Text) <> '' then
  begin
    if(dmComponentes.ValidarCEP(edtCEP.Text, cmbUF.Text))then
      edtCEP.Text := dmComponentes.ACBrValidador1.Formatar
    else
    begin
      edtCEP.SetFocus;
      edtCEP.SelectAll;
    end;
  end;
end;

procedure TfrmClifor.edtClienteNomeEnter(Sender: TObject);
begin
  if sender is TDBEdit then
    Cor_Campo(Sender, HexToTColor(PERSONALIZAR.corFundoFoco));
end;

procedure TfrmClifor.edtClienteNomeExit(Sender: TObject);
begin
  Cor_Campo(Sender, HexToTColor(PERSONALIZAR.corFundoCampo));
  if (StrToIntDef(edtClienteNome.Text,0) > 0) or (Copy(Trim(edtClienteNome.Text),1,1)='-') then
  begin
    DMCupomFiscal.dbQuery1.Active:=False;
    DMCupomFiscal.dbQuery1.SQL.Clear;
    if (Copy(Trim(edtClienteNome.Text),1,1)='-') then
    begin
      DMCupomFiscal.dbQuery1.SQL.Add('Select CF.id, CF.Nome, CF.Endereco, cf.numero_end, cf.bairro, cf.cep, CF.Recado, CF.municipio, cf.uf,'+
                                     ' M.Descricao M_Des, CF.CNPJCPF, CF.IE, CF.DiasPrazo'+
                                     ' FROM CliFor CF'+
                                     ' Left Outer Join Municipios M on M.ID=CF.Municipio'+
                                     ' Left Outer Join clifor_frota fr on fr.clifor=cf.id'+
                                     ' Where (fr.placa=:pPlaca) and (Ativo=''S'')');
      DMCupomFiscal.dbQuery1.ParamByName('pPlaca').AsString:=Copy(Trim(edtClienteNome.Text),2,7);
    end
    else
    begin
      DMCupomFiscal.dbQuery1.SQL.Add('Select CF.id, CF.Nome, CF.Endereco, cf.numero_end, cf.bairro, cf.cep, CF.Recado, CF.municipio, cf.uf,'+
                                     'M.Descricao M_Des, CF.CNPJCPF, CF.IE, CF.DiasPrazo'+
                                     ' FROM CliFor CF'+
                                     ' Left Outer Join Municipios M on M.ID=CF.Municipio'+
                                     ' Where (CF.ID=:pCod) and (Ativo=''S'')');
      DMCupomFiscal.dbQuery1.ParamByName('pCod').AsInteger:=StrToIntDef(edtClienteNome.Text,0);
    end;
    DMCupomFiscal.dbQuery1.Active:=True;
    If DMCupomFiscal.dbQuery1.Eof then
    begin
      msgAviso('Cliente n�o cadastrado!','');
      edtClienteNome.SetFocus;
      edtClienteNome.SelectAll;

    end
    else
    begin
      //dmCupomFiscal.getTemp_NFCE;
      if(FRENTE_CAIXA.Exibir_Contas_Vencidas)then
        if not(crContasVencidas(DMCupomFiscal.dbQuery1.FieldByName('ID').AsInteger))then
        begin
          edtClienteNome.SetFocus;
          edtClienteNome.SelectAll;
          abort;
        end;
      if not(dmCupomFiscal.dbTemp_NFCE.State in [dsEdit]) then
        dmCupomFiscal.dbTemp_NFCE.Edit;
      if (Copy(Trim(edtClienteNome.Text),1,1)='-') then
        dmCupomFiscal.dbTemp_NFCECF_PLACA.Value := Copy(Trim(edtClienteNome.Text),2,7);
      lbRecado.Caption:=DMCupomFiscal.dbQuery1.FieldByName('Recado').AsString;
      dmCupomFiscal.dbTemp_NFCECLIFOR.Value        := DMCupomFiscal.dbQuery1.FieldByName('ID').AsInteger;
      dmCupomFiscal.dbTemp_NFCECF_NOME.Value       := DMCupomFiscal.dbQuery1.FieldByName('Nome').AsString;
      dmCupomFiscal.dbTemp_NFCECF_ENDE.Value       := Copy(DMCupomFiscal.dbQuery1.FieldByName('Endereco').AsString,1,40);
      dmCupomFiscal.dbTemp_NFCECF_NUMERO_END.Value := DMCupomFiscal.dbQuery1.FieldByName('numero_End').AsString;
      dmCupomFiscal.dbTemp_NFCECF_BAIRRO.Value     := DMCupomFiscal.dbQuery1.FieldByName('bairro').AsString;
      dmCupomFiscal.dbTemp_NFCECF_CEP.Value        := DMCupomFiscal.dbQuery1.FieldByName('cep').AsString;
      cmbUF.Text := DMCupomFiscal.dbQuery1.FieldByName('UF').AsString;
      cmbUFExit(Sender);
      dmCupomFiscal.dbTemp_NFCECF_MUNICIPIO.Value  := DMCupomFiscal.dbQuery1.FieldByName('municipio').AsInteger;
      dmCupomFiscal.dbTemp_NFCECF_CNPJ_CPF.Value   := DMCupomFiscal.dbQuery1.FieldByName('CNPJcpf').AsString;
      dmCupomFiscal.dbTemp_NFCECF_IE.Value         := DMCupomFiscal.dbQuery1.FieldByName('IE').AsString;
      dmCupomFiscal.dbTemp_NFCE.Post;

    end;
  end;
end;

procedure TfrmClifor.edtCPFCNPJExit(Sender: TObject);
var
  Txt : string;
begin
  Cor_Campo(Sender, HexToTColor(PERSONALIZAR.corFundoCampo));
  //Validar o CNPJ
  if (trim(edtCPFCNPJ.Text) <> '') and (FRENTE_CAIXA.Pesquisa_CNPJCPF)then
  begin
    if(dmComponentes.ValidarCNPJ(edtCPFCNPJ.Text))then
    begin
      edtCPFCNPJ.Text := dmComponentes.ACBrValidador1.Formatar;
      if(dmCupomFiscal.dbTemp_NFCECLIFOR.AsInteger <= 0)then
      begin
        dmCupomFiscal.dbQuery3.Active := False;
        dmCupomFiscal.dbQuery3.SQL.Clear;
        dmCupomFiscal.dbQuery3.SQL.Add('Select * from Clifor where CNPJCPF = :pID');
        dmCupomFiscal.dbQuery3.ParamByName('pID').AsString := edtCPFCNPJ.Text;
        dmCupomFiscal.dbQuery3.Active := True;
        if not(dmCupomFiscal.dbQuery3.IsEmpty)Then
        begin
          if(msgPergunta('O CNPJ/CPF informado pertence a um cliente j� cadastrado.'+#13+
                         'C�digo: '+dmCupomFiscal.dbQuery3.FieldByName('ID').AsString+#13+
                         'Nome: '+dmCupomFiscal.dbQuery3.FieldByName('NOME').AsString+#13+#13+
                         'Deseja vincular dados do cadastro ao NFC-e?', 'Confirma��o'))then
          begin
            //dmCupomFiscal.getTemp_NFCE;
            if not(dmCupomFiscal.dbTemp_NFCE.State in [dsEdit]) then
              dmCupomFiscal.dbTemp_NFCE.Edit;
            lbRecado.Caption := DMCupomFiscal.dbQuery3.FieldByName('Recado').AsString;
            dmCupomFiscal.dbTemp_NFCECLIFOR.Value        := DMCupomFiscal.dbQuery3.FieldByName('ID').AsInteger;
            dmCupomFiscal.dbTemp_NFCECF_NOME.Value       := DMCupomFiscal.dbQuery3.FieldByName('Nome').AsString;
            dmCupomFiscal.dbTemp_NFCECF_ENDE.Value       := Copy(DMCupomFiscal.dbQuery3.FieldByName('Endereco').AsString,1,40);
            dmCupomFiscal.dbTemp_NFCECF_NUMERO_END.Value := DMCupomFiscal.dbQuery3.FieldByName('numero_End').AsString;
            dmCupomFiscal.dbTemp_NFCECF_BAIRRO.Value     := DMCupomFiscal.dbQuery3.FieldByName('bairro').AsString;
            dmCupomFiscal.dbTemp_NFCECF_CEP.Value        := DMCupomFiscal.dbQuery3.FieldByName('cep').AsString;
            cmbUF.Text := DMCupomFiscal.dbQuery3.FieldByName('UF').AsString;
            cmbUFExit(Sender);
            dmCupomFiscal.dbTemp_NFCECF_MUNICIPIO.Value  := DMCupomFiscal.dbQuery3.FieldByName('municipio').AsInteger;
            dmCupomFiscal.dbTemp_NFCECF_CNPJ_CPF.Value   := DMCupomFiscal.dbQuery3.FieldByName('CNPJcpf').AsString;
            dmCupomFiscal.dbTemp_NFCECF_IE.Value         := DMCupomFiscal.dbQuery3.FieldByName('IE').AsString;
          end;
        end;
      end;
    end;
  end;
end;

procedure TfrmClifor.edtEnderecoExit(Sender: TObject);
begin
  Cor_Campo(Sender, HexToTColor(PERSONALIZAR.corFundoCampo));
end;

procedure TfrmClifor.edtIEExit(Sender: TObject);
begin
  Cor_Campo(Sender, HexToTColor(PERSONALIZAR.corFundoCampo));
end;

procedure TfrmClifor.edtKMExit(Sender: TObject);
begin
  Cor_Campo(Sender, HexToTColor(PERSONALIZAR.corFundoCampo));
end;

procedure TfrmClifor.edtMotoristaEnter(Sender: TObject);
var
  lQryBusca: TIBQuery;
  lFrmMotorista: TfrmListaMotoristas;
begin
  if sender is TDBEdit then
    Cor_Campo(Sender, HexToTColor(PERSONALIZAR.corFundoFoco));
  if not(dmCupomFiscal.dbTemp_NFCECF_PLACA.IsNull)Then
  begin
    lQryBusca := TIBQuery.Create(nil);
    try
      lQryBusca.Database := dmCupomFiscal.DataBase;
      lQryBusca.SQL.Add('Select CF_MOTORISTA from EST_ECF where CF_PLACA = :pPlaca group by CF_MOTORISTA');
      lQryBusca.ParamByName('pPlaca').asString := dmCupomFiscal.dbTemp_NFCECF_PLACA.AsString;
      lQryBusca.Open;
      if not(lQryBusca.IsEmpty)Then
      begin
        lQryBusca.first;
        lFrmMotorista := TfrmListaMotoristas.Create(nil);
        try
          lFrmMotorista.cdsDados.Open;
          lFrmMotorista.cdsDados.EmptyDataSet;
          while not(lQryBusca.Eof)do
          begin
            lFrmMotorista.cdsDados.Insert;
            lFrmMotorista.cdsDadosNOME.AsString := lQryBusca.FieldByName('CF_MOTORISTA').asString;
            lFrmMotorista.cdsDados.Post;
            lQryBusca.Next;
          end;
          case lFrmMotorista.ShowModal of
          mrOk: begin
              if not(dmCupomFiscal.dbTemp_NFCE.State in [dsEdit, dsInsert])Then
                dmCupomFiscal.dbTemp_NFCE.Edit;
              dmCupomFiscal.dbTemp_NFCECF_MOTORISTA.AsString := lFrmMotorista.cdsDadosNOME.AsString;
            end;
          end;
        finally
          lFrmMotorista.Free;
        end;
      end;
      lQryBusca.Close;
    finally
      lQryBusca.Free;
    end;
  end;
end;

procedure TfrmClifor.edtMotoristaExit(Sender: TObject);
begin
  Cor_Campo(Sender, HexToTColor(PERSONALIZAR.corFundoCampo));
end;

procedure TfrmClifor.edtNumeroEndExit(Sender: TObject);
begin
  Cor_Campo(Sender, HexToTColor(PERSONALIZAR.corFundoCampo));
end;

procedure TfrmClifor.edtPlacaExit(Sender: TObject);
begin
  Cor_Campo(Sender, HexToTColor(PERSONALIZAR.corFundoCampo));
  if(dmCupomFiscal.dbTemp_NFCECF_PLACA.AsString <> '')then
  begin
    dmCupomFiscal.dbQuery1.Close;
    dmCupomFiscal.dbQuery1.SQL.Clear;
    dmCupomFiscal.dbQuery1.SQL.Add('Select cf.clifor, c.nome from clifor_Frota cf '+
                                   'left outer join clifor c on c.id = cf.clifor '+
                                   'where cf.placa = :pPlaca');
    dmCupomFiscal.dbQuery1.ParamByName('pPlaca').Value := trim(edtPlaca.Text);
    dmCupomFiscal.dbQuery1.Active:=True;
    dmCupomFiscal.dbQuery1.First;
    if not(dmCupomFiscal.dbQuery1.eof) then
    begin
      if (dmCupomFiscal.dbTemp_NFCECLIFOR.AsInteger <>
        dmCupomFiscal.dbQuery1.FieldByName('Clifor').AsInteger) then
      begin
{        if msgPergunta('Cliente informado ['+dmCupomFiscal.dbTemp_NFCECLIFOR.AsString+'].'+#13+
                       'Eta placa esta cadastada para a frota do cliente ['+dmCupomFiscal.dbQuery1.FieldByName('Clifor').AsString+'].'+#13+
                       'Deseja usar o cliente da frota?','')}
        if(edtClienteNome.Enabled)then // Quer dizer que n�o possui pre�o espeial
        begin
          if msgPergunta('A placa informada pertence a frota de outro cliente'+#13+
                         dmCupomFiscal.dbQuery1.FieldByName('nome').asString+#13+#13+
                         'Deseja usar o cliente da frota?','')then
          begin
            dmCupomFiscal.dbTemp_NFCECF_NOME.AsString := dmCupomFiscal.dbQuery1.FieldByName('Clifor').AsString;
            edtClienteNome.SetFocus;
          end
          Else
          begin
            edtPlaca.SetFocus;
            edtPlaca.SelectAll;
            Abort;
          end;
        end;
      end
    end;

    if (not(ValidaFrota(dmCupomFiscal.dbTemp_NFCECLIFOR.AsInteger, Trim(edtPlaca.Text)))
        and not(FRENTE_CAIXA.Permite_Frota)) then
    begin
      msgInformacao('Este cliente possui frota. '+
                    'Esta placa n�o consta em sua frota.'+#13+#13+
                    'Verifique a placa e tente novamente!','Aten��o');
      edtPlaca.Clear;
      edtPlaca.SetFocus;
      Abort;
    end
  end;
end;

procedure TfrmClifor.EsconderComponentes;
begin
  if not(FRENTE_CAIXA.Exibir_Placa)Then
  begin
    lblPlaca.Visible := False;
    edtPlaca.Visible := False;
  end;
  if not(FRENTE_CAIXA.Exibir_KM)Then
  begin
    lblKM.Visible    := False;
    edtKM.Visible    := False;
  end;
end;

procedure TfrmClifor.FormCreate(Sender: TObject);
begin
  setCores;
  dmCupomFiscal.getTemp_NFCE();
end;

procedure TfrmClifor.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if(key = VK_ESCAPE)then ModalResult := mrCancel;

  if(key = VK_F1)then btnFinalizarClick(Action);

  if(key = VK_F12) and (edtClienteNome.Focused) then Label9Click(Action);

  if(key = VK_F12) and (edtPlaca.Focused) then lblPlacaClick(Action);

  if(key = VK_F9) then btnIdentConsumiClick(Action);

  if(key = VK_F11) then btnCadastroRapidoClick(Action);

  if(key = VK_F12) and (edtMotorista.Focused) then lblMotoristaClick(Action);
end;

procedure TfrmClifor.FormResize(Sender: TObject);
begin
  //Centraliza painel
  pnlCentral.Left:=Trunc((frmClifor.Width/2)-(pnlCentral.Width/2));
  pnlCentral.Top :=Trunc((frmClifor.Height/2)-(pnlCentral.Height/2)-(pnlCabecalho.Height/2)-
  (pnlFinalizar.Height/2)-(pnlRodape.Height/2));

  //Centraliza bot�es de Finaliza��o
  pnlFinalizarOrganiza.Top  := (pnlFinalizar.ClientHeight - pnlFinalizarOrganiza.Height) div 2;
  pnlFinalizarOrganiza.Left := (pnlFinalizar.ClientWidth - pnlFinalizarOrganiza.Width) div 2;
end;

procedure TfrmClifor.FormShow(Sender: TObject);
var
  UFRecado: String;
  UFMunicipio: String;
begin
  EsconderComponentes;
  //Retorna a UF e o recado para o cliente se tiver
  if(dmCupomFiscal.dbTemp_NFCECLIFOR.Value > 0)then
  begin
    UFRecado := dmCupomFiscal.getDadosCliente(dmCupomFiscal.dbTemp_NFCECLIFOR.AsInteger);
    cmbUF.Text := Copy(UFRecado,1,2);
    dmCupomFiscal.getMunicipios(cmbUF.Text);
    lbRecado.Caption := trim(Copy(UFRecado,3,53));
    DesabilitaHabilitaCampos('D');
  end
  else  //Pega os dados dos locais
  begin
    UFMunicipio := dmCupomFiscal.getMunicipioLocal(dmCupomFiscal.dbTemp_NFCECCUSTO.Value);
    cmbUF.Text := Copy(UFMunicipio,1,2);;
    dmCupomFiscal.getMunicipios(cmbUF.Text);
    lcmbMunicipio.KeyValue := StrToInt(trim(Copy(UFMunicipio,3,7)));
    DesabilitaHabilitaCampos('H');
  end;
end;

function TfrmClifor.geraID: Integer;
var
  lQryBusca: TIBSQL;
begin
  lQryBusca := TIBSQL.Create(nil);
  try
    lQryBusca.Database := dmCupomFiscal.DataBase;
    lQryBusca.SQL.Clear;
    lQryBusca.SQL.Add('select gen_id(Clifor, 1) as xID from rdb$database');
    lQryBusca.ExecQuery;
    Result:=lQryBusca.fieldbyname('xID').AsInteger;
    lQryBusca.Close;
  Finally
    lQryBusca.Free
  end;
end;

procedure TfrmClifor.lblMotoristaClick(Sender: TObject);
begin
  frmPesquisa := tfrmPesquisa.Create(Self,'Select id, NOME From MOTORISTAS Where (NOME >=:pTexto) Order By NOME',
    'Select Id, NOME From MOTORISTAS Where (NOME like :pTexto) Order By NOME');

  frmPesquisa.DBGrid1.Columns.Add;
  frmPesquisa.DBGrid1.Columns[0].Title.Caption:='C�digo';
  frmPesquisa.DBGrid1.Columns[0].Width:=42;
  frmPesquisa.DBGrid1.Columns[0].FieldName:='id';

  frmPesquisa.DBGrid1.Columns.Add;
  frmPesquisa.DBGrid1.Columns[1].Title.Caption:='Nome/Raz�o Social';
  frmPesquisa.DBGrid1.Columns[1].Width:=337;
  frmPesquisa.DBGrid1.Columns[1].FieldName:='NOME';
  try
    if (frmPesquisa.ShowModal=mrOK) then
    begin
      if not(dmCupomFiscal.dbTemp_NFCE.State in [dsEdit, dsInsert]) then
        dmCupomFiscal.dbTemp_NFCE.Edit;
      dmCupomFiscal.dbTemp_NFCECF_MOTORISTA.AsString := trim(Copy(FrmPesquisa.IBQuery1.FieldByName('NOME').Value,1,30));
    end;
  finally
    frmPesquisa.Free;
  end;
end;

procedure TfrmClifor.Label9Click(Sender: TObject);
begin
  FrmPesquisaCliente := TFrmPesquisaCliente.Create(Self);
  try
    if (FrmPesquisaCliente.ShowModal=mrOK) then
    begin
      //Vai incluir o cliente quando sair do campo
      dmCupomFiscal.dbTemp_NFCE.Edit;
      dmCupomFiscal.dbTemp_NFCECF_NOME.Value := FrmPesquisaCliente.qCliFor.FieldByName('ID').AsString;
    end;
  finally
    FrmPesquisaCliente.Free;
  end;
end;

procedure TfrmClifor.lblPlacaClick(Sender: TObject);
begin
  FrmPesquisaPlaca := TFrmPesquisaPlaca.Create(Self);
  try
    if (FrmPesquisaPlaca.ShowModal=mrOK) then
    begin
      dmCupomFiscal.dbTemp_NFCE.Edit;
      dmCupomFiscal.dbTemp_NFCECF_PLACA.Value := FrmPesquisaPlaca.IBQuery1.FieldByName('Placa').AsString;
    end;
  finally
    FrmPesquisaPlaca.Free;
  end;
end;

procedure TfrmClifor.setCores;
begin
  pnlCabecalho.Color        :=  HexToTColor(PERSONALIZAR.corFundoCampo);
  pnlCabecalho.Font.Color   :=  HexToTColor(PERSONALIZAR.corFonte);
  pnlFinalizar.Color        :=  HexToTColor(PERSONALIZAR.corFundoCampo);
  pnlRodape.Color           :=  HexToTColor(PERSONALIZAR.corFundoCampo);
  Label7.Font.Color         :=  HexToTColor(PERSONALIZAR.corFonte);
  Label8.Font.Color         :=  HexToTColor(PERSONALIZAR.corFonte);
  lblData.Font.Color        :=  HexToTColor(PERSONALIZAR.corFonte);
  lblNCaixa.Font.Color      :=  HexToTColor(PERSONALIZAR.corFonte);
  edtClienteNome.Color      :=  HexToTColor(PERSONALIZAR.corFundoCampo);
  edtClienteNome.Font.Color :=  HexToTColor(PERSONALIZAR.corFonte);
  edtCPFCNPJ.Color          :=  HexToTColor(PERSONALIZAR.corFundoCampo);
  edtCPFCNPJ.Font.Color     :=  HexToTColor(PERSONALIZAR.corFonte);
  edtIE.Color               :=  HexToTColor(PERSONALIZAR.corFundoCampo);
  edtIE.Font.Color          :=  HexToTColor(PERSONALIZAR.corFonte);
  edtPlaca.Color            :=  HexToTColor(PERSONALIZAR.corFundoCampo);
  edtPlaca.Font.Color       :=  HexToTColor(PERSONALIZAR.corFonte);
  edtKM.Color               :=  HexToTColor(PERSONALIZAR.corFundoCampo);
  edtKM.Font.Color          :=  HexToTColor(PERSONALIZAR.corFonte);
  edtCEP.Color              :=  HexToTColor(PERSONALIZAR.corFundoCampo);
  edtCEP.Font.Color         :=  HexToTColor(PERSONALIZAR.corFonte);
  edtEndereco.Color         :=  HexToTColor(PERSONALIZAR.corFundoCampo);
  edtEndereco.Font.Color    :=  HexToTColor(PERSONALIZAR.corFonte);
  edtNumeroEnd.Color        :=  HexToTColor(PERSONALIZAR.corFundoCampo);
  edtNumeroEnd.Font.Color   :=  HexToTColor(PERSONALIZAR.corFonte);
  edtBairro.Color           :=  HexToTColor(PERSONALIZAR.corFundoCampo);
  edtBairro.Font.Color      :=  HexToTColor(PERSONALIZAR.corFonte);
  edtMotorista.Color        :=  HexToTColor(PERSONALIZAR.corFundoCampo);
  edtMotorista.Font.Color   :=  HexToTColor(PERSONALIZAR.corFonte);
end;

function TfrmClifor.ValidarInformacoes: Boolean;
var
  Txt : String;
begin
  Result := True;
  if (trim(edtCPFCNPJ.Text) <> '') and not(dmComponentes.ValidarCNPJ(edtCPFCNPJ.Text))then
  begin
    edtCPFCNPJ.SetFocus;
    edtCPFCNPJ.SelectAll;
    Result := false;
  end;
  if(dmCupomFiscal.dbTemp_NFCEVLR_TOTAL.Value >= 10000) and
    not(dmCupomFiscal.dbTemp_NFCECLIFOR.Value > 0)then
  begin
    edtClienteNome.SetFocus;
    edtClienteNome.SelectAll;
    msgInformacao('Para valores superiores ou iguais a R$10.000,00 � obrigat�rio informar o consumidor.','Informa��o');
    Result := False;
  end;
  if((trim(edtClienteNome.Text) <> '') or (trim(edtIE.Text)<> '')) and
    (Trim(edtCPFCNPJ.Text)='')then
  begin
    edtCPFCNPJ.SetFocus;
    edtCPFCNPJ.SelectAll;
    msgInformacao('Obrigat�rio identificar o CNPJCPF quando informar outros dados do cliente.','Informa��o');
    Result := False;
  end;
  if Trim(dmCupomFiscal.dbTemp_NFCECF_CEP.AsString) <> '' then
  begin
    if not dmComponentes.ValidarCEP(dmCupomFiscal.dbTemp_NFCECF_CEP.AsString,
      cmbUF.Text) then
    begin
      msgErro('CEP informado � inv�lido','Erro');
      edtCEP.SetFocus;
      edtCEP.SelectAll;
      Result := False;
    end;
  end;
end;

Function TfrmClifor.ValidarInformacoesPreCadastro(pException: Boolean): Boolean;
begin
  Result := True;
  if(trim(edtClienteNome.Text)= '')Then
  begin
    Result := False;
    if(pException)Then
    begin
      edtClienteNome.SetFocus;
      raise Exception.Create('Nome do cliente � obrigat�rio');
    end;
  end;
  if(trim(edtCPFCNPJ.Text)= '')Then
  begin
    Result := False;
    if(pException)Then
    begin
      edtCPFCNPJ.SetFocus;
      raise Exception.Create('CNPJ/CPF do cliente � obrigat�rio');
    end;
  end;
  if(trim(edtIE.Text)= '')Then
  begin
    Result := False;
    if(pException)Then
    begin
      edtIE.SetFocus;
      raise Exception.Create('Inscri��o Estadual do cliente � obrigat�ria');
    end;
  end;
  if(trim(edtCEP.Text)= '')Then
  begin
    Result := False;
    if(pException)Then
    begin
      edtCEP.SetFocus;
      raise Exception.Create('Cep do cliente � obrigat�rio');
    end;
  end;
  if(trim(edtEndereco.Text)= '')Then
  begin
    Result := False;
    if(pException)Then
    begin
      edtEndereco.SetFocus;
      raise Exception.Create('Endere�o do cliente � obrigat�rio');
    end;
  end;
  if(trim(edtNumeroEnd.Text)= '')Then
  begin
    Result := False;
    if(pException)Then
    begin
      edtNumeroEnd.SetFocus;
      raise Exception.Create('N�mero do endere�o do cliente � obrigat�rio');
    end;
  end;
  if(trim(edtBairro.Text)= '')Then
  begin
    Result := False;
    if(pException)Then
    begin
      edtBairro.SetFocus;
      raise Exception.Create('Bairro do cliente � obrigat�rio');
    end;
  end;
  if(trim(lcmbMunicipio.Text) = '')Then
  begin
    Result := False;
    if(pException)Then
    begin
      lcmbMunicipio.SetFocus;
      raise Exception.Create('Munic�pio do cliente � obrigat�rio');
    end;
  end;
end;

function TfrmClifor.getIndIE(pFJ, pIE: String): String;
begin
  if (pIE='ISENTO') and (pFJ='J') then
    Result :='2' //Contribuinte Isento
  else
    if (pIE='ISENTO') and (pFJ='F') then
      Result:='9' //Nao Contribuinte
    else
      if (pIE<>'ISENTO') then
        Result:='1'; //Contribuinte
end;


end.

