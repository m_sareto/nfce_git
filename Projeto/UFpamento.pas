unit UFpamento;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, DBClient, ExtCtrls, StdCtrls, Buttons, Mask, TREdit, Grids,
  DBGrids;

type
  TForm1 = class(TForm)
    Label56: TLabel;
    Label58: TLabel;
    Label61: TLabel;
    Label62: TLabel;
    Label63: TLabel;
    Label64: TLabel;
    Label65: TLabel;
    DBGrid2: TDBGrid;
    ed_Cr_Historico: TEdit;
    Re_Vl_Entrada: TRealEdit;
    re_vl_Resta_Parcela: TRealEdit;
    ed_n_Parcela: TEdit;
    me_Dt_Pri_Parcela: TMaskEdit;
    CheckBox3: TCheckBox;
    re_vl_Parcela: TRealEdit;
    BitBtn13: TBitBtn;
    Panel1: TPanel;
    Label66: TLabel;
    re_vl_T_Parcelas: TRealEdit;
    CDSPar: TClientDataSet;
    CDSParData: TDateTimeField;
    CDSParValor: TFloatField;
    DsPar: TDataSource;
    Panel2: TPanel;
    Label17: TLabel;
    Label18: TLabel;
    Edit7: TEdit;
    procedure Edit7Exit(Sender: TObject);
    procedure ed_Cr_HistoricoExit(Sender: TObject);
    procedure Re_Vl_EntradaExit(Sender: TObject);
    procedure ed_n_ParcelaExit(Sender: TObject);
    procedure DBGrid2Exit(Sender: TObject);
    procedure CDSParAfterPost(DataSet: TDataSet);
    procedure BitBtn13Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

uses UDM;

{$R *.dfm}

procedure TForm1.Edit7Exit(Sender: TObject);
begin
Label18.Caption:='Nome do Funcion�rio...';
     If Edit7.Text<>'' then
     begin
     DM.QLixo2.Active:=False;
     DM.QLixo2.SQL.Clear;
     DM.QLixo2.SQL.Add('Select * FROM Funcionarios Where Id=:pId');
     DM.QLixo2.ParamByName('pId').AsString:=Trim(Edit7.Text);
     DM.QLixo2.Active:=True;
          If DM.QLixo2.Eof then
          begin
               ShowMessage('N�o Cadastrado...');
               Edit7.SetFocus;
               Abort;
          end
          ELSE
          Label18.Caption:=Copy(DM.QLixo2.FieldByName('Nome').AsString,1,20);
     end
     ELSE
     begin
     Edit7.SetFocus;
     Abort;
     end;
end;

procedure TForm1.ed_Cr_HistoricoExit(Sender: TObject);
begin
     If trim(ed_Cr_Historico.Text)='' then ed_Cr_Historico.Text:='0';

     DM.QLixo1.Active:=False;
     DM.QLixo1.SQL.Clear;
     DM.QLixo1.SQL.Add('Select * From Cr_Historicos Where Id=:pId');
     DM.QLixo1.ParamByName('pId').AsString:=Trim(ed_Cr_Historico.Text);
     DM.QLixo1.Active:=True;
     DM.QLixo1.First;
     If not(DM.QLixo1.Eof) then
     begin
          Label58.Caption:=DM.QLixo1.FieldByName('Descricao').AsString;
//          wH_Conta:=DM.QLixo1.FieldByName('Conta').AsInteger;
          if (DM.QLixo1.FieldByName('DC').AsString='C') then
          begin
          ShowMessage('Deve-ser informado um hist�rico de D�bito.');
          ed_Cr_Historico.SetFocus;
          end;
     end
     else
     begin
          ShowMessage('Historico N�o Cadastrado...');
          ed_Cr_Historico.SetFocus;
     end;
end;

procedure TForm1.Re_Vl_EntradaExit(Sender: TObject);
begin
//     re_vl_Resta_Parcela.value:=RealEdit7.value-Re_Vl_Entrada.Value;
     Re_Vl_Entrada.Text:=FormatCurr('###,##0.00',Re_Vl_Entrada.value);
end;

procedure TForm1.ed_n_ParcelaExit(Sender: TObject);
begin
     me_Dt_Pri_Parcela.Text:=DateToStr(Date);
     if StrToInt(ed_n_Parcela.Text)<=0 then
          Re_Vl_Entrada.SetFocus
     else
          re_vl_Parcela.Value:=re_vl_Resta_Parcela.value/(StrToInt(ed_n_Parcela.text));
end;

procedure TForm1.DBGrid2Exit(Sender: TObject);
begin
{
     if (RealEdit7.Value-Re_Vl_Entrada.Value)<>re_vl_T_Parcelas.Value then
     begin
     ShowMessage('Existe uma diferen�a entre o valor das Parcelas e Total do Pedido');
     DBGrid2.SetFocus;
     abort;
     end;
     }
end;

procedure TForm1.CDSParAfterPost(DataSet: TDataSet);
begin
re_vl_T_Parcelas.Value:=0;
     CDSPar.First;
     While not(CDSPar.Eof) do
     begin
          re_vl_T_Parcelas.Value:=re_vl_T_Parcelas.Value+CDSParValor.Value;
          CDSPar.Next;
     end;
end;

procedure TForm1.BitBtn13Click(Sender: TObject);
Var  i:Integer;
     Data:TDate;
     yAno, yMes, yDia : word;
     xAno, xMes, xDia : word;
     Dif:Currency; { Diferenca entre as Parcelas }
begin
Dif:=0;
i:=0;
     {
     DecodeDate (StrToDate(me_Dt_Pri_Parcela.text) , yAno, ymes, yDia);

     if wCF_DiasPrazo=0 then
     begin
          ShowMessage('Cliente n�o possue dias de prazo no seu cadastro.'+#13+'Sera usado prazo de 30 dias.');
          wCF_DiasPrazo:=30;
     end;

     CDSPar.First;
     While Not(CDSPar.eof) do CDSPar.Delete;

     re_vl_Parcela.Value:=re_vl_Resta_Parcela.value/(StrToInt(ed_n_Parcela.text));
     Data:=StrToDate(me_Dt_Pri_Parcela.text);

     {  Diferenca  dos Centavos  }
     //Dif:=RealEdit7.Value - (re_vl_Parcela.Value*StrToInt(ed_n_Parcela.text))-Re_Vl_Entrada.Value;

     for i:= 1 to StrToInt(ed_n_Parcela.text) do
     begin
          CDSPar.Insert;
          CDSParData.Value:=Data;
          CDSParValor.Value:=re_vl_Parcela.Value+Dif;
          CDSPar.Post;

          //Data:=Data+wCF_DiasPrazo;

          if CheckBox3.Checked then
          begin
          DecodeDate (Data, xAno, xmes, xDia);
          //Data:=IncDay(Data,yDia-xDia)
          end;

          Dif:=0;
     end;

//BitBtn1.SetFocus;

end;

end.
