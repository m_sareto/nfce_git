{$I ACBr.inc}
unit uFrmConfiguracao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, Buttons, ComCtrls, ShellAPI, OleCtrls, FileCtrl,
  IniFiles, SHDocVw, pcnconversao, Tredit, Mask, Spin,
  StrUtils,  ACBrBAL, ACBrDevice, ACBrECF, AppEvnts, Vcl.DBCtrls, PngBitBtn,
  ACBrTEFDClass, ACBrBase, ACBrTEFD, uFrm_Conf_Redes;
const
  SELDIRHELP = 1000;

type
  TfrmConfiguracao = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    pgcControle: TPageControl;
    tabFrenteCaixa: TTabSheet;
    tabPosto: TTabSheet;
    btnSalvar: TBitBtn;
    btnFechar: TBitBtn;
    OpenDialog1: TOpenDialog;
    GroupBox2: TGroupBox;
    tabLeitor: TTabSheet;
    tabBalCheckout: TTabSheet;
    grpBooleano: TGroupBox;
    chkFrenteAbrirGaveta: TCheckBox;
    chkFrenteBaixaSeletivaCR: TCheckBox;
    chkFrenteTruncar: TCheckBox;
    rgVinculado: TRadioGroup;
    grpCasasDecimais: TGroupBox;
    edtFrenteCasaDecQTD: TSpinEdit;
    Label6: TLabel;
    edtFrenteCasaDecVLR: TSpinEdit;
    Label7: TLabel;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    btnDatabase: TSpeedButton;
    edtFrenteQtdMaxima: TEdit;
    edtFrenteQtdInicial: TEdit;
    edtFrenteFormaPag: TEdit;
    edtFrenteDatabase: TEdit;
    edtFrenteOperacao: TEdit;
    Label13: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    edtFrenteMsgSimplesNac: TEdit;
    chkPostoAbastecidasDel: TCheckBox;
    chkPostoAbastecidasExibir: TCheckBox;
    Label49: TLabel;
    edtFrenteMensagemCupom: TEdit;
    chkFrenteLimiteCredito: TCheckBox;
    edtFrenteVlrMaximo: TEdit;
    barStatus: TStatusBar;
    ApplicationEvents1: TApplicationEvents;
    tabPersonalizar: TTabSheet;
    GroupBox14: TGroupBox;
    lblCordeFundo: TLabel;
    edtCorFundoCampo: TMaskEdit;
    btnCorFundoCampo: TSpeedButton;
    dlgCor: TColorDialog;
    Label60: TLabel;
    edtCorFundoFoco: TMaskEdit;
    btnCorFundoFoco: TSpeedButton;
    Label61: TLabel;
    edtCorFonte: TMaskEdit;
    btnCorFonte: TSpeedButton;
    cbbCoresPredefinidos: TComboBox;
    Label62: TLabel;
    grpBalancaConfig: TGroupBox;
    Label52: TLabel;
    Label54: TLabel;
    Label55: TLabel;
    Label56: TLabel;
    Label42: TLabel;
    Label43: TLabel;
    Label44: TLabel;
    Label45: TLabel;
    cbbBalancaHandShaking: TComboBox;
    cbbBalancaParity: TComboBox;
    cbbBalancaStopBits: TComboBox;
    edtBalancaTimeOut: TEdit;
    cbbBalancaModelo: TComboBox;
    cbbBalancaPorta: TComboBox;
    cbbBalancaBaud: TComboBox;
    cbbBalancaDataBits: TComboBox;
    GroupBox10: TGroupBox;
    Label50: TLabel;
    Label51: TLabel;
    sttPeso: TStaticText;
    sttResposta: TStaticText;
    chbBalancaMonitorar: TCheckBox;
    btnBalAtivar: TBitBtn;
    btnBalDesativar: TBitBtn;
    btnBalPesar: TBitBtn;
    grpLeitorConfig: TGroupBox;
    Label11: TLabel;
    Label48: TLabel;
    Label53: TLabel;
    Label39: TLabel;
    Label40: TLabel;
    Label10: TLabel;
    Label41: TLabel;
    chkLeitorUsarFila: TCheckBox;
    chkLeitorExcluirSufixo: TCheckBox;
    cbbLeitorHandShake: TComboBox;
    cbbLeitorStop: TComboBox;
    cbbLeitorParity: TComboBox;
    cbbLeitorPorta: TComboBox;
    cbbLeitorBaud: TComboBox;
    edtLeitorSufixo: TEdit;
    chbSoft: TCheckBox;
    chbHard: TCheckBox;
    cbbLeitorData: TComboBox;
    chkLeitor: TCheckBox;
    GroupBox13: TGroupBox;
    Label58: TLabel;
    Label57: TLabel;
    sttCodigoBarras: TStaticText;
    btnLeitorDesativar: TBitBtn;
    btnLeitorAtivar: TBitBtn;
    GroupBox8: TGroupBox;
    chkPersonalizarDisplay: TCheckBox;
    Label63: TLabel;
    edtPersonalizarDisplayMensagem: TEdit;
    chkFrenteSugerirFPag: TCheckBox;
    chkImprimirVendedor: TCheckBox;
    chkImprimirSaldoPagar: TCheckBox;
    chkCalcularJuro: TCheckBox;
    Label14: TLabel;
    edtFrenteDatabaseRemoto: TEdit;
    btnDatabaseRemota: TSpeedButton;
    chkFrenteAlterarValor: TCheckBox;
    chkPostoVenderSemAbastecidas: TCheckBox;
    chkPostoImprimirMedia: TCheckBox;
    GroupBox15: TGroupBox;
    chkPostoAbastecidasExibirVenda: TCheckBox;
    chkPostoAbastecidasEventosVenda: TCheckBox;
    chkFrenteExibirContasVencidas: TCheckBox;
    chkDebCred: TCheckBox;
    chkPesquisaCliente: TCheckBox;
    edtFPagDesc: TEdit;
    edtOpeDesc: TEdit;
    lcmbCCustos: TDBLookupComboBox;
    rgFrenteAtividade: TRadioGroup;
    tabTef: TTabSheet;
    GroupBox4: TGroupBox;
    Label8: TLabel;
    Label9: TLabel;
    edtTefPathResp: TEdit;
    edtTefPathReq: TEdit;
    btnBuscaDirReq: TPngBitBtn;
    btnBuscaDirResp: TPngBitBtn;
    chkTefAtivo: TCheckBox;
    edtTefVersao: TEdit;
    Label12: TLabel;
    cbxGP: TComboBox;
    Label17: TLabel;
    edtTefPathSts: TEdit;
    Label20: TLabel;
    edtTefPathTmp: TEdit;
    PngBitBtn1: TPngBitBtn;
    PngBitBtn2: TPngBitBtn;
    Label21: TLabel;
    ACBrTEFD1: TACBrTEFD;
    SaveDialog1: TSaveDialog;
    Label22: TLabel;
    edtTefRegCert: TEdit;
    edtTefVersaoPG: TEdit;
    Label23: TLabel;
    chkTefBloquearMouseTeclado: TCheckBox;
    GroupBox9: TGroupBox;
    CheckBox1: TCheckBox;
    CHKAcrescimos: TCheckBox;
    TabSheet1: TTabSheet;
    Label24: TLabel;
    edLicencaServidor1: TEdit;
    Label25: TLabel;
    edLicencaServidor2: TEdit;
    edLicencaCaminho2: TEdit;
    edLicencaCaminho1: TEdit;
    Label26: TLabel;
    Label27: TLabel;
    rgFocoFim: TRadioGroup;
    chkPlaca: TCheckBox;
    chkKM: TCheckBox;
    chkCusto: TCheckBox;
    chkFrota: TCheckBox;
    chkPostoParcelasVenda: TCheckBox;
    cbbHomologador: TComboBox;
    lblHomologador: TLabel;
    chkPostoAlterarQuantidade: TCheckBox;
    chkCertificadoVencendo: TCheckBox;
    chkItensCompDebito: TCheckBox;
    Label15: TLabel;
    edtGavCaminho: TEdit;
    Label16: TLabel;
    edtGavComando: TEdit;
    btnTestar: TBitBtn;
    Label28: TLabel;
    edtGuilhoComando: TEdit;
    BitBtn1: TBitBtn;
    Label29: TLabel;
    ComboBox1: TComboBox;
    Label30: TLabel;
    chkInfVendedor: TCheckBox;
    chkPostoAutorizacaoSemAbastecidas: TCheckBox;
    chkFrenteLoginAutorizacao: TCheckBox;
    chkFrenteLoginAutorizacaoAbast: TCheckBox;
    procedure btnDatabaseClick(Sender: TObject);
    procedure gravarConfiguracaoINI();
    procedure lerConfiguracaoINI();
    procedure btnSalvarClick(Sender: TObject);
    procedure setConfigTela();
    procedure btnFecharClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnLeitorDesativarClick(Sender: TObject);
    procedure btnLeitorAtivarClick(Sender: TObject);
    procedure btnBalAtivarClick(Sender: TObject);
    procedure btnBalDesativarClick(Sender: TObject);
    procedure btnBalPesarClick(Sender: TObject);
    procedure ApplicationEvents1Hint(Sender: TObject);
    procedure btnCorFundoCampoClick(Sender: TObject);
    procedure btnCorFundoFocoClick(Sender: TObject);
    procedure btnCorFonteClick(Sender: TObject);
    procedure cbbCoresPredefinidosChange(Sender: TObject);
    procedure chkPersonalizarDisplayClick(Sender: TObject);
    procedure btnDatabaseRemotaClick(Sender: TObject);
    procedure chkPostoAbastecidasExibirVendaClick(Sender: TObject);
    procedure chkPostoAbastecidasEventosVendaClick(Sender: TObject);
    procedure edtFrenteFormaPagKeyPress(Sender: TObject; var Key: Char);
    procedure edtFrenteOperacaoExit(Sender: TObject);
    procedure edtFrenteFormaPagExit(Sender: TObject);
    procedure btnBuscaDirReqClick(Sender: TObject);
    procedure btnBuscaDirRespClick(Sender: TObject);
    procedure btnTestarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure PngBitBtn2Click(Sender: TObject);
    procedure PngBitBtn1Click(Sender: TObject);
    procedure rgFocoFimClick(Sender: TObject);
    procedure Label13Click(Sender: TObject);
    procedure Label4Click(Sender: TObject);
  private
    procedure ListaSubPastas(Diretorio : String);
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmConfiguracao: TfrmConfiguracao;

implementation

uses uFuncoes, uDMCupomFiscal, uFrmPrincipal, uDMComponentes, ACBrUtil, TypInfo,
  uFrmConfigurarSerial, uRotinasGlobais, uFrmPesquisaGenerica;

{$R *.dfm}

procedure TfrmConfiguracao.btnDatabaseClick(Sender: TObject);
begin
  OpenDialog1.Title := 'Selecione o arquivo da base de dados';
  OpenDialog1.DefaultExt := '*.fdb';
  OpenDialog1.Filter := 'Arquivos Firebird (*.fdb)|*.fdb|Todos os Arquivos (*.*)|*.*';
  OpenDialog1.InitialDir := caminhoExe;
  if OpenDialog1.Execute then
  begin
    edtFrenteDatabase.Text := OpenDialog1.FileName;
  end;
end;

procedure TfrmConfiguracao.gravarConfiguracaoINI;
Var ini     : TIniFile ;
    StreamMemo : TMemoryStream;
begin
  //Cria arquivo INI
  try
    ini := TIniFile.Create(caminhoExe +'\Configuracao.ini');

    //FRENTE_CAIXA
    ini.WriteFloat('FRENTE_CAIXA', 'Qtd_Maxima', StrToCurr(edtFrenteQtdMaxima.Text));
    ini.WriteInteger('FRENTE_CAIXA', 'Qtd_Inicial', StrToInt(edtFrenteQtdInicial.Text));
    ini.WriteFloat('FRENTE_CAIXA', 'Vlr_Maximo', StrToCurr(edtFrenteVlrMaximo.Text));
    ini.WriteInteger('FRENTE_CAIXA', 'Forma_Pag', StrToInt(edtFrenteFormaPag.Text));
    ini.WriteInteger('FRENTE_CAIXA', 'Operacao',StrToInt(edtFrenteOperacao.Text));
    ini.WriteBool('FRENTE_CAIXA', 'Abre_Gaveta', chkFrenteAbrirGaveta.Checked);
    ini.WriteBool('FRENTE_CAIXA', 'CR_Baixa_Seletiva', chkFrenteBaixaSeletivaCR.Checked);
    case rgVinculado.ItemIndex of
      0: ini.WriteString('FRENTE_CAIXA', 'Vinculado', 'S'); //2 vias
      1: ini.WriteString('FRENTE_CAIXA', 'Vinculado', 'N'); //1 via
    end;
    ini.WriteBool('FRENTE_CAIXA', 'Truncar', chkFrenteTruncar.Checked);
    ini.WriteInteger('FRENTE_CAIXA', 'Casas_Dec_Quantidade', edtFrenteCasaDecQTD.Value);
    ini.WriteInteger('FRENTE_CAIXA', 'Casas_Dec_Valor', edtFrenteCasaDecVLR.Value);
    ini.WriteString('FRENTE_CAIXA', 'Msg_Simples_Nacional', edtFrenteMsgSimplesNac.Text);
    ini.WriteInteger('FRENTE_CAIXA', 'CCusto', StrToInt(lcmbCCustos.KeyValue));
    ini.WriteString('FRENTE_CAIXA', 'Database', edtFrenteDatabase.Text);
    ini.WriteBool('FRENTE_CAIXA', 'Autorizacao', chkFrenteLoginAutorizacao.Checked);
    ini.WriteBool('FRENTE_CAIXA', 'Autorizacao_Abast', chkFrenteLoginAutorizacaoAbast.Checked);
    ini.WriteInteger('FRENTE_CAIXA', 'Atividade', rgFrenteAtividade.ItemIndex);
    ini.WriteString('FRENTE_CAIXA', 'MensagemCupom', Trim(edtFrenteMensagemCupom.Text));
    ini.WriteBool('FRENTE_CAIXA', 'IniciePor', CheckBox1.Checked);
    ini.WriteBool('FRENTE_CAIXA', 'LimiteCredito', chkFrenteLimiteCredito.Checked);
    ini.WriteBool('FRENTE_CAIXA', 'Sugerir_FormaPag', chkFrenteSugerirFPag.Checked);
    ini.WriteBool('FRENTE_CAIXA', 'ImprimirVendedor', chkImprimirVendedor.Checked);
    ini.WriteBool('FRENTE_CAIXA', 'ImprimirSaldoPagar', chkImprimirSaldoPagar.Checked);
    ini.WriteBool('FRENTE_CAIXA', 'CalcularJuro', chkCalcularJuro.Checked);
    ini.WriteString('FRENTE_CAIXA', 'DatabaseRemoto', edtFrenteDatabaseRemoto.Text);
    ini.WriteBool('FRENTE_CAIXA', 'Altera_Valor', chkFrenteAlterarValor.Checked);
    ini.WriteBool('FRENTE_CAIXA', 'Exibir_Contas_Vencidas', chkFrenteExibirContasVencidas.Checked);
    ini.WriteBool('FRENTE_CAIXA', 'Comprovante_CredDeb', chkDebCred.Checked);
    ini.WriteBool('FRENTE_CAIXA', 'Comprovante_CredDeb_Itens', chkItensCompDebito.Checked);
    ini.WriteBool('FRENTE_CAIXA', 'Pesquisa_CNPJCPF',chkPesquisaCliente.Checked);
    ini.WriteBool('FRENTE_CAIXA', 'Exibir_Acrescimos',CHKAcrescimos.Checked);
    ini.WriteBool('FRENTE_CAIXA', 'Exibir_Placa',chkPlaca.Checked);
    ini.WriteBool('FRENTE_CAIXA', 'Exibir_KM',chkKM.Checked);
    ini.WriteString('FRENTE_CAIXA', 'GAV_Comando', Trim(edtGavComando.Text));
    ini.WriteString('FRENTE_CAIXA', 'GAV_Caminho', Trim(edtGavCaminho.Text));
    ini.WriteInteger('FRENTE_CAIXA', 'Foco_Finalizar', rgFocoFim.ItemIndex);
    ini.WriteBool('FRENTE_CAIXA', 'Exibir_Vlr_Custo', chkCusto.Checked);
    ini.WriteBool('FRENTE_CAIXA', 'Permite_Frota', chkFrota.Checked);
    ini.WriteBool('FRENTE_CAIXA', 'Exibir_Validade_CertDigital', chkCertificadoVencendo.Checked);
    ini.writeString('FRENTE_CAIXA', 'Pasta_Fotos', ComboBox1.Text);
    ini.writeBool('FRENTE_CAIXA', 'Informar_Vendedor_Fim', chkInfVendedor.Checked);

    //POSTO
    ini.WriteBool('POSTO', 'Abastecidas_Del', chkPostoAbastecidasDel.Checked);
    ini.WriteBool('POSTO', 'Abastecidas_Exibir', chkPostoAbastecidasExibir.Checked);
    ini.WriteBool('POSTO', 'Eventos_Venda', chkPostoAbastecidasEventosVenda.Checked);
    ini.WriteBool('POSTO', 'Abastecidas_Venda', chkPostoAbastecidasExibirVenda.Checked);
    //ini.WriteBool('POSTO', 'Imprimir_Media', chkPostoImprimirMedia.Checked);
    ini.WriteBool('POSTO', 'VenderSemAbastecida', chkPostoVenderSemAbastecidas.Checked);
    ini.WriteBool('POSTO', 'GerarParcelasManualmente', chkPostoParcelasVenda.Checked);
    ini.WriteBool('POSTO', 'AlterarQuantidadeAbastecida', chkPostoAlterarQuantidade.Checked);
    ini.WriteBool('POSTO', 'AutorizacaoSemAbastecida', chkPostoAutorizacaoSemAbastecidas.Checked);

    //Leitor
    ini.WriteBool('LEITOR', 'Leitor', chkLeitor.Checked);
    ini.WriteString('LEITOR', 'Porta', cbbLeitorPorta.Text);
    ini.WriteInteger('LEITOR', 'Baud', StrToInt(cbbLeitorBaud.Text));
    ini.WriteBool('LEITOR', 'UsarFila', chkLeitorUsarFila.Checked);
    ini.WriteInteger('LEITOR', 'Data', StrToInt(cbbLeitorData.Text));
    ini.WriteString('LEITOR', 'Sufixo', edtLeitorSufixo.Text);
    ini.WriteBool('LEITOR', 'ExcluirSufixo', chkLeitorExcluirSufixo.Checked);
    ini.WriteInteger('LEITOR', 'HandShake', cbbLeitorHandShake.ItemIndex);
    ini.WriteInteger('LEITOR', 'Parity', cbbLeitorParity.ItemIndex);
    ini.WriteInteger('LEITOR', 'Stop', cbbLeitorStop.ItemIndex);


    //Balanca
    ini.WriteInteger('BALANCA', 'Modelo', cbbBalancaModelo.ItemIndex);
    ini.WriteString('BALANCA', 'Porta', cbbBalancaPorta.Text);
    ini.WriteInteger('BALANCA', 'Baud', StrToInt(cbbBalancaBaud.Text));
    ini.WriteInteger('BALANCA', 'HandShake', cbbBalancaHandShaking.ItemIndex);
    ini.WriteInteger('BALANCA', 'Parity', cbbBalancaParity.ItemIndex);
    ini.WriteInteger('BALANCA', 'Stop', cbbBalancaStopBits.ItemIndex);
    ini.WriteInteger('BALANCA', 'Data', StrToInt(cbbBalancaDataBits.Text));
    ini.WriteInteger('BALANCA', 'TimeOut', StrToInt(edtBalancaTimeOut.Text));

    //Personaliza��o
    ini.WriteString('PERSONALIZAR', 'CorFundoCampo', TColorToHex(edtCorFundoCampo.Color));
    ini.WriteString('PERSONALIZAR', 'CorFundoFoco', TColorToHex(edtCorFundoFoco.Color));
    ini.WriteString('PERSONALIZAR', 'CorFonte', TColorToHex(edtCorFonte.Color));
    ini.WriteBool('PERSONALIZAR', 'Display', chkPersonalizarDisplay.Checked);
    ini.WriteString('PERSONALIZAR', 'DisplayMensagem', edtPersonalizarDisplayMensagem.Text);

    //Tef
    ini.WriteBool('TEF', 'ATIVO', chkTefAtivo.Checked);
    ini.WriteBool('TEF', 'BloqMouseTeclado', chkTefBloquearMouseTeclado.Checked);
    ini.WriteInteger('TEF', 'GP', cbxGP.ItemIndex);
    ini.WriteString('TEF', 'Versao', edtTefVersao.Text);
    ini.WriteString('TEF', 'VersaoPG', edtTefVersaoPG.Text);
    ini.WriteString('TEF', 'ArqReq', edtTefPathReq.Text);
    ini.WriteString('TEF', 'ArqResp', edtTefPathResp.Text);
    ini.WriteString('TEF', 'ArqSts', edtTefPathSts.Text);
    ini.WriteString('TEF', 'ArqTmp', edtTefPathTmp.Text);
    ini.WriteString('TEF', 'Certificacao', edtTefRegCert.Text);
    ini.WriteString('TEF', 'Homologador', cbbHomologador.Text);

    //Conexao
    ini.WriteString('Licenca', 'Servidor1',edLicencaServidor1.Text);
    ini.WriteString('Licenca', 'Caminho1', edLicencaCaminho1.Text );
    ini.WriteString('Licenca', 'Servidor2',edLicencaServidor2.Text);
    ini.WriteString('Licenca', 'Caminho2', edLicencaCaminho2.Text );
  finally
    ini.Free;
  end
end;


procedure TfrmConfiguracao.Label13Click(Sender: TObject);
begin
  frmPesquisaGenerica := tfrmPesquisaGenerica.Create(Self,'Operacoes');
  try
    if (frmPesquisaGenerica.ShowModal=mrOK) then
    begin
      edtFrenteOperacao.Text := FrmPesquisaGenerica.IBQuery1.FieldByName('id').AsString;
    end;
  finally
    frmPesquisaGenerica.Free;
  end;
end;

procedure TfrmConfiguracao.Label4Click(Sender: TObject);
begin
  frmPesquisaGenerica := tfrmPesquisaGenerica.Create(Self,'Formas_Pagamento');
  try
    if (frmPesquisaGenerica.ShowModal=mrOK) then
    begin
      edtFrenteFormaPag.Text := FrmPesquisaGenerica.IBQuery1.FieldByName('id').AsString;
    end;
  finally
    frmPesquisaGenerica.Free;
  end;
end;

procedure TfrmConfiguracao.lerConfiguracaoINI;
var ini : TIniFile;
begin
  try   ////Le valores do arquivo INI e insere nas variaveis de registro
    ini := TIniFile.Create(caminhoExe +'\Configuracao.ini');
    //FRENTE_CAIXA
    FRENTE_CAIXA.Qtd_Maxima   := ini.ReadFloat('FRENTE_CAIXA', 'Qtd_Maxima', 1000);
    FRENTE_CAIXA.Qtd_Inicial  := ini.ReadInteger('FRENTE_CAIXA', 'Qtd_Inicial', 1);
    FRENTE_CAIXA.Vlr_Maximo   := ini.ReadFloat('FRENTE_CAIXA', 'Vlr_Maximo', 1000);
    FRENTE_CAIXA.Forma_Pag    := ini.ReadInteger('FRENTE_CAIXA', 'Forma_Pag', 100);
    FRENTE_CAIXA.Operacao     := ini.ReadInteger('FRENTE_CAIXA', 'Operacao',0);
    FRENTE_CAIXA.Abre_Gaveta  := ini.ReadBool('FRENTE_CAIXA', 'Abre_Gaveta', False);
    FRENTE_CAIXA.CR_Baixa_Seletiva := ini.ReadBool('FRENTE_CAIXA', 'CR_Baixa_Seletiva', True);
    FRENTE_CAIXA.Vinculado    := ini.ReadString('FRENTE_CAIXA', 'Vinculado', 'S');
    FRENTE_CAIXA.Truncar      := ini.ReadBool('FRENTE_CAIXA', 'Truncar', False);
    FRENTE_CAIXA.Casas_Dec_Quantidade := ini.ReadInteger('FRENTE_CAIXA', 'Casas_Dec_Quantidade', 3);
    FRENTE_CAIXA.Casas_Dec_Valor      := ini.ReadInteger('FRENTE_CAIXA', 'Casas_Dec_Valor', 2);
    FRENTE_CAIXA.Msg_Simples_Nacional := ini.ReadString('FRENTE_CAIXA', 'Msg_Simples_Nacional', 'ICMS a ser recolhido conforme LC 123/2006 - Simples Nacional'); //ICMS a ser recolhido conforme LC 123/2006 - Simples Nacional
    FRENTE_CAIXA.CCusto       := ini.ReadInteger('FRENTE_CAIXA', 'CCusto', 101);
    FRENTE_CAIXA.Database     := ini.ReadString('FRENTE_CAIXA', 'Database', 'C:\ELINFO\ADMINISTRADOR\DADOS\ADMINISTRADOR.FDB');
    FRENTE_CAIXA.Autorizacao  := ini.ReadBool('FRENTE_CAIXA', 'Autorizacao', False);
    FRENTE_CAIXA.Autorizacao_Abast := ini.ReadBool('FRENTE_CAIXA', 'Autorizacao_Abast', True);
    FRENTE_CAIXA.Atividade    := ini.ReadInteger('FRENTE_CAIXA', 'Atividade', 0);
    FRENTE_CAIXA.MensagemCupom := ini.ReadString('FRENTE_CAIXA', 'MensagemCupom', 'Obrigado pela Prefer�ncia!');
    FRENTE_CAIXA.LimiteCredito := ini.ReadBool('FRENTE_CAIXA', 'LimiteCredito', False);
    FRENTE_CAIXA.Sugerir_FormaPag := ini.ReadBool('FRENTE_CAIXA', 'Sugerir_FormaPag', True);
    FRENTE_CAIXA.IniciePor := ini.ReadBool('FRENTE_CAIXA', 'IniciePor', True);
    FRENTE_CAIXA.ImprimirVendedor := ini.ReadBool('FRENTE_CAIXA', 'ImprimirVendedor', False);
    FRENTE_CAIXA.ImprimirSaldoPagar := ini.ReadBool('FRENTE_CAIXA', 'ImprimirSaldoPagar', False);
    FRENTE_CAIXA.CalcularJuro := ini.ReadBool('FRENTE_CAIXA', 'CalcularJuro', False);
    FRENTE_CAIXA.DatabaseRemoto  := ini.ReadString('FRENTE_CAIXA', 'DatabaseRemoto', '');
    FRENTE_CAIXA.Altera_Valor := ini.ReadBool('FRENTE_CAIXA', 'Altera_Valor', True);
    FRENTE_CAIXA.Exibir_Contas_Vencidas := ini.ReadBool('FRENTE_CAIXA', 'Exibir_Contas_Vencidas', True);
    FRENTE_CAIXA.Comprovante_CredDeb := ini.ReadBool('FRENTE_CAIXA','Comprovante_CredDeb',True);
    FRENTE_CAIXA.Comprovante_CredDeb_Itens := ini.ReadBool('FRENTE_CAIXA', 'Comprovante_CredDeb_Itens', False);
    FRENTE_CAIXA.Pesquisa_CNPJCPF := ini.ReadBool('FRENTE_CAIXA','Pesquisa_CNPJCPF',True);
    FRENTE_CAIXA.Exibir_Acrescimos := ini.ReadBool('FRENTE_CAIXA','Exibir_Acrescimos',True);
    FRENTE_CAIXA.Exibir_Placa := ini.ReadBool('FRENTE_CAIXA','Exibir_Placa',False);
    FRENTE_CAIXA.Exibir_KM := ini.ReadBool('FRENTE_CAIXA','Exibir_KM',False);
    FRENTE_CAIXA.GAV_Comando := ini.ReadString('FRENTE_CAIXA','GAV_Comando','');
    FRENTE_CAIXA.GAV_Caminho := ini.ReadString('FRENTE_CAIXA','GAV_Caminho','');
    FRENTE_CAIXA.Foco_Fim := ini.ReadInteger('FRENTE_CAIXA', 'Foco_Finalizar', 2);
    FRENTE_CAIXA.Exibir_Vlr_Custo := ini.ReadBool('FRENTE_CAIXA', 'Exibir_Vlr_Custo', False);
    FRENTE_CAIXA.Permite_Frota := ini.ReadBool('FRENTE_CAIXA', 'Permite_Frota', False);
    FRENTE_CAIXA.Exibir_Validade_CertDigital := ini.ReadBool('FRENTE_CAIXA', 'Exibir_Validade_CertDigital', True);
    FRENTE_CAIXA.Pasta_Fotos       := ini.ReadString('FRENTE_CAIXA', 'Pasta_Fotos', 'Geral');
    FRENTE_CAIXA.Informar_Vendedor_Fim := ini.ReadBool('FRENTE_CAIXA', 'Informar_Vendedor_Fim', False);

    //POSTO
    POSTO.Abastecidas_Del     := ini.ReadBool('POSTO', 'Abastecidas_Del', False);
    POSTO.Abastecidas_Exibir  := ini.ReadBool('POSTO', 'Abastecidas_Exibir', False);
    POSTO.Eventos_Venda       := ini.ReadBool('POSTO', 'Eventos_Venda', False);
    POSTO.Abastecidas_Venda   := ini.ReadBool('POSTO', 'Abastecidas_Venda', False);
    POSTO.VenderSemAbastecida := ini.ReadBool('POSTO', 'VenderSemAbastecida', True);
    POSTO.AutorizacaoSemAbastecida := ini.ReadBool('POSTO', 'AutorizacaoSemAbastecida', False);
    Posto.ParcelasVenda       := ini.ReadBool('POSTO', 'GerarParcelasManualmente', False);
    Posto.AlterarQuantidade   := ini.ReadBool('POSTO', 'AlterarQuantidadeAbastecida', False);
    //POSTO.Imprimir_Media     := ini.ReadBool('POSTO', 'Imprimir_Media', False);

    //Leitor
    LEITOR.Leitor        := ini.ReadBool('LEITOR', 'Leitor', False);
    LEITOR.Porta         := ini.ReadString('LEITOR', 'Porta', '');
    LEITOR.Baud          := ini.ReadInteger('LEITOR', 'Baud', 9600);
    LEITOR.UsarFila      := ini.ReadBool('LEITOR', 'UsarFila', False);
    LEITOR.Data          := ini.ReadInteger('LEITOR', 'Data', 8);
    LEITOR.Sufixo        := ini.ReadString('LEITOR', 'Sufixo', '#13');
    LEITOR.ExcluirSufixo := ini.ReadBool('LEITOR', 'ExcluirSufixo', True);
    LEITOR.HandShake     := ini.ReadInteger('LEITOR', 'HandShake', 0);
    LEITOR.Parity        := ini.ReadInteger('LEITOR', 'Parity', 0);
    LEITOR.Stop          := ini.ReadInteger('LEITOR', 'Stop', 0);

    //Balanca
    BALANCA.Modelo       := ini.ReadInteger('BALANCA', 'Modelo', 0);
    BALANCA.Porta        := ini.ReadString('BALANCA', 'Porta', '');
    BALANCA.Baud         := ini.ReadInteger('BALANCA', 'Baud', 9600);
    BALANCA.HandShake    := ini.ReadInteger('BALANCA', 'HandShake', 0);
    BALANCA.Parity       := ini.ReadInteger('BALANCA', 'Parity', 0);
    BALANCA.Stop         := ini.ReadInteger('BALANCA', 'Stop', 0);
    BALANCA.Data         := ini.ReadInteger('BALANCA', 'Data', 8);
    BALANCA.TimeOut      := ini.ReadInteger('BALANCA', 'TimeOut', 2000);

    //Personalizacao
    PERSONALIZAR.corFundoCampo := ini.ReadString('PERSONALIZAR', 'CorFundoCampo', '007FFF');
    PERSONALIZAR.corFundoFoco  := ini.ReadString('PERSONALIZAR', 'CorFundoFoco', '0C98E8');
    PERSONALIZAR.corFonte      := ini.ReadString('PERSONALIZAR', 'CorFonte', 'FFFFFF');
    PERSONALIZAR.display       := ini.ReadBool('PERSONALIZAR', 'Display', False);
    PERSONALIZAR.mensagemDisplay := ini.ReadString('PERSONALIZAR', 'DisplayMensagem', '');

    //Tef
    TEF.Ativo    := ini.ReadBool('TEF', 'Ativo', False);
    TEF.GP       := ini.ReadInteger('TEF', 'GP', 0);
    TEF.Versao   := ini.ReadString('TEF', 'Versao', '');
    TEF.VersaoPG := ini.ReadString('TEF', 'VersaoGP', '210');
    TEF.ArqReq   := ini.ReadString('TEF', 'ArqReq', 'C:\PayGO\req\intpos.001');
    TEF.ArqResp  := ini.ReadString('TEF', 'ArqResp', 'C:\PayGO\resp\intpos.001');
    TEF.ArqSts   := ini.ReadString('TEF', 'ArqSts', 'C:\PayGO\resp\intpos.sts');
    TEF.ArqTmp   := ini.ReadString('TEF', 'ArqTmp', 'C:\PayGO\req\intpos.tmp');
    TEF.NumCertificacao  := ini.ReadString('TEF', 'Certificacao', '');
    TEF.BloqMouseTeclado := ini.ReadBool('TEF', 'BloqMouseTeclado', False);
    TEF.Homologador     := ini.ReadString('TEF', 'Homologador', 'PayGO');;

    //Licenca
    Licenca.Licenca_Servidor1:=ini.ReadString('LICENCA','Servidor1','189.73.175.41');
    Licenca.Licenca_CaminhoBase1:=ini.ReadString('LICENCA','Caminho1','C:\HelpDesk\Dados\versao.fdb');
    Licenca.Licenca_Servidor2:=ini.ReadString('LICENCA','Servidor2','177.70.75.134');
    Licenca.Licenca_CaminhoBase2:=ini.ReadString('LICENCA','Caminho2','C:\HelpDesk\Dados\versao.fdb');
  finally
    ini.Free;
  end;

  if (not FileExists(caminhoExe+'\Configuracao.ini')) then
  begin
    if MessageDlg('Arquivo de configura��o do sistema n�o encontrado!'+#13+
      'Criar Arquivo de configura��o?',mtconfirmation,[mbyes,mbno],MB_ICONEXCLAMATION) = mryes then
    begin
      setConfigTela;
      frmConfiguracao.ShowModal;
      frmConfiguracao.Free;
    end
    else
    begin
      Application.Terminate;
      Abort;
    end;
  end;
end;

procedure TfrmConfiguracao.ListaSubPastas(Diretorio: String);
var
  F : TSearchRec;
  Retorno : Integer;
begin
  Retorno := FindFirst(Diretorio + '\*.*', faAnyFile, F);
  ComboBox1.Items.Clear;
  try
    while Retorno = 0 do
    begin
      if (F.Name <> '.') and (F.Name <> '..') then
      begin
        if TemAtributo(F.Attr, faDirectory) then
          ComboBox1.Items.Add(F.Name)
      end;
        Retorno := FindNext(F)
    end;
  finally
    FindClose(F);
  end;
end;

procedure TfrmConfiguracao.PngBitBtn1Click(Sender: TObject);
begin
  SaveDialog1.Title      := 'Exportar arquivo tmp';
  SaveDialog1.FileName   := 'intpos.tmp';
  SaveDialog1.DefaultExt := '.tmp';
  SaveDialog1.Filter     := 'Arquivos tmp|*.tmp';

  if SaveDialog1.Execute then
    edtTefPathTmp.text := SaveDialog1.filename;
end;

procedure TfrmConfiguracao.PngBitBtn2Click(Sender: TObject);
begin
  SaveDialog1.Title      := 'Exportar arquivo sts';
  SaveDialog1.FileName   := 'intpos.sts';
  SaveDialog1.DefaultExt := '.sts';
  SaveDialog1.Filter     := 'Arquivos sts|*.sts';

  if SaveDialog1.Execute then
    edtTefPathSts.Text := SaveDialog1.filename;
end;

procedure TfrmConfiguracao.rgFocoFimClick(Sender: TObject);
begin
  if(rgFocoFim.ItemIndex = 1)then
  begin
    CHKAcrescimos.Checked := True;
    CHKAcrescimos.Enabled := false;
  end
  else
    CHKAcrescimos.Enabled := True;
end;

procedure TfrmConfiguracao.btnSalvarClick(Sender: TObject);
begin
  gravarConfiguracaoINI;
  msgInformacao('Configura��es salvas com sucesso!','Aviso');
  lerConfiguracaoINI;
  dmComponentes.setConfigACBr;
end;

procedure TfrmConfiguracao.btnTestarClick(Sender: TObject);
begin
  showMessage('Caminho: '+edtGavCaminho.Text);
  showMessage('Comando: '+edtGavComando.Text);
  dmCupomFiscal.AcionarPeriferico(edtGavCaminho.Text, edtGavComando.Text);
end;

procedure TfrmConfiguracao.setConfigTela;
begin
  //FRENTE_CAIXA
  edtFrenteQtdMaxima.Text  := CurrToStr(FRENTE_CAIXA.Qtd_Maxima);
  edtFrenteQtdInicial.Text := CurrToStr(FRENTE_CAIXA.Qtd_Inicial);
  edtFrenteVlrMaximo.Text  := CurrToStr(FRENTE_CAIXA.Vlr_Maximo);
  edtFrenteFormaPag.Text   := IntToStr(FRENTE_CAIXA.Forma_Pag);
  edtFrenteFormaPagExit(Action);
  edtFrenteOperacao.Text   := IntToStr(FRENTE_CAIXA.Operacao);
  edtFrenteOperacaoExit(Action);
  chkFrenteAbrirGaveta.Checked := FRENTE_CAIXA.Abre_Gaveta;
  chkFrenteBaixaSeletivaCR.Checked := FRENTE_CAIXA.CR_Baixa_Seletiva;
  case AnsiIndexStr(FRENTE_CAIXA.Vinculado,['S','C','N']) of
    0: rgVinculado.ItemIndex := 0;
    1: rgVinculado.ItemIndex := 1;
    2: rgVinculado.ItemIndex := 2;
  end;
  chkFrenteTruncar.Checked := FRENTE_CAIXA.Truncar;
  edtFrenteCasaDecQTD.Value := FRENTE_CAIXA.Casas_Dec_Quantidade;
  edtFrenteCasaDecVLR.Value := FRENTE_CAIXA.Casas_Dec_Valor;
  edtFrenteMsgSimplesNac.Text := FRENTE_CAIXA.Msg_Simples_Nacional;

  ComboBox1.Text := FRENTE_CAIXA.Pasta_Fotos;

  dmCupomFiscal.AbrirCcustos;
  lcmbCCustos.KeyValue := FRENTE_CAIXA.CCusto;

  edtFrenteDatabase.Text := FRENTE_CAIXA.Database;
  chkFrenteLoginAutorizacao.Checked := FRENTE_CAIXA.Autorizacao;
  chkFrenteLoginAutorizacaoAbast.Checked := FRENTE_CAIXA.Autorizacao_Abast;
  rgFrenteAtividade.ItemIndex := FRENTE_CAIXA.Atividade;
  edtFrenteMensagemCupom.Text := FRENTE_CAIXA.MensagemCupom;
  chkFrenteLimiteCredito.Checked := FRENTE_CAIXA.LimiteCredito;
  chkFrenteSugerirFPag.Checked := FRENTE_CAIXA.Sugerir_FormaPag;
  CheckBox1.Checked:= FRENTE_CAIXA.IniciePor;
  chkImprimirVendedor.Checked   := FRENTE_CAIXA.ImprimirVendedor;
  chkImprimirSaldoPagar.Checked := FRENTE_CAIXA.ImprimirSaldoPagar;
  chkCalcularJuro.Checked       := FRENTE_CAIXA.CalcularJuro;
  edtFrenteDatabaseRemoto.Text  := FRENTE_CAIXA.DatabaseRemoto;
  chkFrenteAlterarValor.Checked := FRENTE_CAIXA.Altera_Valor;
  chkFrenteExibirContasVencidas.Checked := FRENTE_CAIXA.Exibir_Contas_Vencidas;
  chkDebCred.Checked            := FRENTE_CAIXA.Comprovante_CredDeb;
  chkItensCompDebito.Checked    := FRENTE_CAIXA.Comprovante_CredDeb_Itens;
  chkPesquisaCliente.checked    := FRENTE_CAIXA.Pesquisa_CNPJCPF;
  CHKAcrescimos.checked         := FRENTE_CAIXA.Exibir_Acrescimos;
  chkPlaca.checked              := FRENTE_CAIXA.Exibir_Placa;
  chkKM.checked                 := FRENTE_CAIXA.Exibir_KM;
  chkFrota.Checked              := FRENTE_CAIXA.Permite_Frota;
  edtGavCaminho.Text            := FRENTE_CAIXA.GAV_Caminho;
  edtGavComando.Text            := FRENTE_CAIXA.GAV_Comando;
  rgFocoFim.ItemIndex           := FRENTE_CAIXA.Foco_Fim;
  chkCertificadoVencendo.Checked := FRENTE_CAIXA.Exibir_Validade_CertDigital;
  chkInfVendedor.Checked        := FRENTE_CAIXA.Informar_Vendedor_Fim;

  if(rgFocoFim.ItemIndex = 1)then
  begin
    CHKAcrescimos.Checked := True;
    CHKAcrescimos.Enabled := false;
  end
  else
    CHKAcrescimos.Enabled := True;
  chkCusto.Checked := FRENTE_CAIXA.Exibir_Vlr_Custo;

  //POSTO
  chkPostoAbastecidasDel.Checked := POSTO.Abastecidas_Del;
  chkPostoAbastecidasExibir.Checked := POSTO.Abastecidas_Exibir;
  chkPostoAbastecidasEventosVenda.Checked := POSTO.Eventos_Venda;
  chkPostoAbastecidasExibirVenda.Checked := POSTO.Abastecidas_Venda;
  //chkPostoImprimirMedia.Checked := POSTO.Imprimir_Media;
  chkPostoVenderSemAbastecidas.Checked := POSTO.VenderSemAbastecida;
  chkPostoParcelasVenda.Checked := Posto.ParcelasVenda;
  chkPostoAlterarQuantidade.Checked := POSTO.AlterarQuantidade;
  chkPostoAutorizacaoSemAbastecidas.Checked := POSTO.AutorizacaoSemAbastecida;

  //Leitor
  chkLeitor.Checked   := LEITOR.Leitor;
  cbbLeitorPorta.Text := LEITOR.Porta;
  cbbLeitorBaud.Text  := IntToStr(LEITOR.Baud);
  chkLeitorUsarFila.Checked := LEITOR.UsarFila;
  cbbLeitorData.Text  := IntToStr(LEITOR.Data);
  edtLeitorSufixo.Text := LEITOR.Sufixo;
  chkLeitorExcluirSufixo.Checked := LEITOR.ExcluirSufixo;
  cbbLeitorHandShake.ItemIndex := LEITOR.HandShake;
  cbbLeitorStop.ItemIndex := LEITOR.Stop;
  cbbLeitorParity.ItemIndex := LEITOR.Parity;
  //Balanca
  cbbBalancaModelo.ItemIndex := BALANCA.Modelo;
  cbbBalancaPorta.Text := BALANCA.Porta;
  cbbBalancaBaud.Text  := IntToStr(BALANCA.Baud);
  cbbBalancaHandShaking.ItemIndex := BALANCA.HandShake;
  cbbBalancaParity.ItemIndex := BALANCA.Parity;
  cbbBalancaStopBits.ItemIndex := BALANCA.Stop;
  cbbBalancaDataBits.Text := IntToStr(BALANCA.Data);
  edtBalancaTimeOut.Text  := IntToStr(BALANCA.TimeOut);
  //Personaliza��o
  edtCorFundoCampo.Color:=HexToTColor(PERSONALIZAR.CorFundoCampo);
  edtCorFundoFoco.Color:=HexToTColor(PERSONALIZAR.CorFundoFoco);
  edtCorFonte.Color:=HexToTColor(PERSONALIZAR.CorFonte);
  chkPersonalizarDisplay.Checked:=PERSONALIZAR.Display;
  edtPersonalizarDisplayMensagem.Text:=PERSONALIZAR.MensagemDisplay;

  //Tef
  chkTefAtivo.Checked := TEF.Ativo;
  cbxGP.ItemIndex := TEF.GP;
  edtTefVersao.Text   := TEF.Versao;
  edtTefVersaoPG.Text := TEF.VersaoPG;
  edtTefPathResp.Text := TEF.ArqResp;
  edtTefPathReq.Text  := TEF.ArqReq;
  edtTefPathSts.Text  := TEF.ArqSts;
  edtTefPathTmp.Text  := TEF.ArqTmp;
  edtTefRegCert.Text  := TEF.NumCertificacao;
  chkTefBloquearMouseTeclado.Checked := TEF.BloqMouseTeclado;
  cbbHomologador.Text := TEF.Homologador;

  //Licen�a
  edLicencaServidor1.Text:= Licenca.Licenca_Servidor1;
  edLicencaServidor2.Text:= Licenca.Licenca_Servidor1;
  edLicencaCaminho1.Text:= Licenca.Licenca_CaminhoBase1;
  edLicencaCaminho2.Text:= Licenca.Licenca_CaminhoBase2;
end;

procedure TfrmConfiguracao.btnFecharClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmConfiguracao.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=vk_Escape then Close;

  if(key=vk_f12) and (edtFrenteFormaPag.Focused) Then Label4Click(action);

  if(key=vk_f12) and (edtFrenteOperacao.Focused) Then Label13Click(action);
end;


procedure TfrmConfiguracao.FormShow(Sender: TObject);
begin
  pgcControle.ActivePage    := tabFrenteCaixa;
  ListaSubPastas(ExtractFilePath(Application.EXEName)+'Foto');
  setConfigTela;       //Setar dados do INI nos campos
end;

procedure TfrmConfiguracao.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := caFree;       // Libera o formul�rio da mem�ria
  frmConfiguracao := Nil; // Deixa o formul�rio vazio
end;

procedure TfrmConfiguracao.FormCreate(Sender: TObject);
var
  I: TACBrTEFDTipo;
begin
  cbxGP.Items.Clear ;
  For I := Low(TACBrTEFDTipo) to High(TACBrTEFDTipo) do
     cbxGP.Items.Add( GetEnumName(TypeInfo(TACBrTEFDTipo), integer(I) ) ) ;
  cbxGP.Items[0] := 'Todos' ;
  cbxGP.ItemIndex := 0 ;
end;

procedure TfrmConfiguracao.btnLeitorDesativarClick(Sender: TObject);
begin
  //Desativa conex�o
  dmComponentes.ACBrLCB1.Desativar;

  btnLeitorAtivar.Enabled    := True;
  grpLeitorConfig.Enabled    := True;
  btnLeitorDesativar.Enabled := False;
end;

procedure TfrmConfiguracao.btnLeitorAtivarClick(Sender: TObject);
begin
  if (dmComponentes.ACBrLCB1.Ativo) then
    dmComponentes.ACBrLCB1.Desativar;

  dmComponentes.ACBrLCB1.Porta       := cbbLeitorPorta.Text;
  dmComponentes.ACBrLCB1.Sufixo      := edtLeitorSufixo.Text;
  dmComponentes.ACBrLCB1.UsarFila    := chkLeitorUsarFila.Checked;
  dmComponentes.ACBrLCB1.Device.Baud := StrToInt(cbbLeitorBaud.text);
  dmComponentes.ACBrLCB1.Device.Data := StrToInt(cbbLeitorData.Text);
  dmComponentes.ACBrLCB1.ExcluirSufixo := chkLeitorExcluirSufixo.Checked;
  dmComponentes.ACBrLCB1.Intervalo   := 300;   //Ver
  dmComponentes.ACBrLCB1.Device.HandShake := TACBrHandShake(cbbLeitorHandShake.ItemIndex);
  dmComponentes.ACBrLCB1.Device.HardFlow  := False;;
  dmComponentes.ACBrLCB1.Device.SoftFlow  := False;
  dmComponentes.ACBrLCB1.Device.Parity := TACBrSerialParity(cbbLeitorParity.ItemIndex);
  dmComponentes.ACBrLCB1.Device.Stop   := TACBrSerialStop(cbbLeitorStop.ItemIndex);

  //Ativa conex�o
  dmComponentes.ACBrLCB1.Ativar;

  btnLeitorAtivar.Enabled    := False;
  grpLeitorConfig.Enabled    := False;
  btnLeitorDesativar.Enabled := True;
end;

procedure TfrmConfiguracao.btnBalAtivarClick(Sender: TObject);
begin
  //Se houver conec��o aberta(Ativa), Fecha a conec��o
  if not(dmComponentes.ACBrBAL1.Ativo) then
    dmComponentes.ACBrBAL1.Desativar;

    //Configura porta de comunica��o
  dmComponentes.ACBrBAL1.Modelo           := TACBrBALModelo(cbbBalancaModelo.ItemIndex);     //Toledo, Filizola, ...
  dmComponentes.ACBrBAL1.Device.HandShake := TACBrHandShake(cbbBalancaHandShaking.ItemIndex);  //
  dmComponentes.ACBrBAL1.Device.Parity    := TACBrSerialParity(cbbBalancaParity.ItemIndex);
  dmComponentes.ACBrBAL1.Device.Stop      := TACBrSerialStop(cbbBalancaStopBits.ItemIndex);
  dmComponentes.ACBrBAL1.Device.Data      := StrToInt(cbbBalancaDataBits.text);
  dmComponentes.ACBrBAL1.Device.Baud      := StrToInt(cbbBalancaBaud.Text );
  dmComponentes.ACBrBAL1.Device.Porta     := cbbBalancaPorta.Text;

  //Ativa conex�o com a balan�a
  dmComponentes.ACBrBAL1.Ativar;

  btnBalAtivar.Enabled      := False;
  grpBalancaConfig.Enabled := False;
  btnBalDesativar.Enabled   := True;
  btnBalPesar.Enabled       := true;
end;

procedure TfrmConfiguracao.btnBalDesativarClick(Sender: TObject);
begin
  //Desativa conex�o
  dmComponentes.ACBrBAL1.Desativar;

  btnBalAtivar.Enabled      := True;
  grpBalancaConfig.Enabled := True;
  btnBalDesativar.Enabled   := False;
  btnBalPesar.Enabled       := False;
end;

procedure TfrmConfiguracao.btnBalPesarClick(Sender: TObject);
Var timeOut : Integer ;
begin
  try
    timeOut := StrToInt(edtBalancaTimeOut.Text) ;
  except
    timeOut := 2000 ;
  end ;
  sttPeso.Caption := FormatFloat('###,##0.000', dmComponentes.ACBrBAL1.LePeso(timeOut));
end;

procedure TfrmConfiguracao.btnBuscaDirReqClick(Sender: TObject);
begin
  SaveDialog1.Title      := 'Exportar arquivo 001';
  SaveDialog1.FileName   := 'intpos.001';
  SaveDialog1.DefaultExt := '.001';
  SaveDialog1.Filter     := 'Arquivos 001|*.001';

  if SaveDialog1.Execute then
    edtTefPathReq.Text := SaveDialog1.filename;
end;

procedure TfrmConfiguracao.btnBuscaDirRespClick(Sender: TObject);
begin
  SaveDialog1.Title      := 'Exportar arquivo 001';
  SaveDialog1.FileName   := 'intpos.001';
  SaveDialog1.DefaultExt := '.001';
  SaveDialog1.Filter     := 'Arquivos 001|*.001';

  if SaveDialog1.Execute then
    edtTefPathResp.Text := SaveDialog1.filename;
end;

procedure TfrmConfiguracao.ApplicationEvents1Hint(Sender: TObject);
begin
  barStatus.Panels[0].Text := ' '+Application.Hint;
end;

procedure TfrmConfiguracao.btnCorFundoCampoClick(Sender: TObject);
begin
  if InputQuery('Sele��o de cores', 'Informe uma cor Hexadecimal ou Cancelar para selecionar na janela de cores', PERSONALIZAR.corFundoCampo) then
  begin
    edtCorFundoCampo.Color := HexToTColor(PERSONALIZAR.corFundoCampo);
  end
  else
  begin
    if dlgCor.Execute then
    begin
      edtCorFundoCampo.Color := dlgCor.Color;
    end;
  end;
end;

procedure TfrmConfiguracao.btnCorFundoFocoClick(Sender: TObject);
begin
  if InputQuery('Sele��o de cores', 'Informe uma cor Hexadecimal ou Cancelar para selecionar na janela de cores', PERSONALIZAR.corFundoFoco) then
  begin
    edtCorFundoFoco.Color := HexToTColor(PERSONALIZAR.corFundoFoco);
  end
  else
  begin
    if dlgCor.Execute then
    begin
      edtCorFundoFoco.Color := dlgCor.Color;
    end;
  end
end;

procedure TfrmConfiguracao.btnCorFonteClick(Sender: TObject);
begin
  if InputQuery('Sele��o de cores', 'Informe uma cor Hexadecimal ou Cancelar para selecionar na janela de cores', PERSONALIZAR.corFonte) then
  begin
    edtCorFonte.Color := HexToTColor(PERSONALIZAR.corFonte);
  end
  else
  begin
    if dlgCor.Execute then
    begin
      edtCorFonte.Color := dlgCor.Color;
    end;
  end;
end;

procedure TfrmConfiguracao.cbbCoresPredefinidosChange(Sender: TObject);
begin
  case (cbbCoresPredefinidos.ItemIndex) of
    0:begin
      edtCorFundoCampo.Color:=HexToTColor('007FFF');
      edtCorFundoFoco.Color:=HexToTColor('0C98E8');
      edtCorFonte.Color:=HexToTColor('FFFFFF');
    end;
    1:begin
      edtCorFundoCampo.Color:=HexToTColor('334025');
      edtCorFundoFoco.Color:=HexToTColor('677F4B');
      edtCorFonte.Color:=HexToTColor('9ABF70');
    end;
    2:begin
      edtCorFundoCampo.Color:=HexToTColor('E8370B');
      edtCorFundoFoco.Color:=HexToTColor('FF600C');
      edtCorFonte.Color:=HexToTColor('FFFFFF');
    end;
    3:begin
      edtCorFundoCampo.Color:=HexToTColor('003F7F');
      edtCorFundoFoco.Color:=HexToTColor('005FBF');
      edtCorFonte.Color:=HexToTColor('FFFFFF');
    end;
  end
end;

procedure TfrmConfiguracao.chkPersonalizarDisplayClick(Sender: TObject);
begin
  edtPersonalizarDisplayMensagem.Enabled := chkPersonalizarDisplay.Checked;
end;

procedure TfrmConfiguracao.btnDatabaseRemotaClick(Sender: TObject);
begin
  OpenDialog1.Title := 'Selecione o arquivo da base de dados';
  OpenDialog1.DefaultExt := '*.fdb';
  OpenDialog1.Filter := 'Arquivos Firebird (*.fdb)|*.fdb|Todos os Arquivos (*.*)|*.*';
  OpenDialog1.InitialDir := caminhoExe;
  if OpenDialog1.Execute then
  begin
    edtFrenteDatabaseRemoto.Text := OpenDialog1.FileName;
  end;
end;

procedure TfrmConfiguracao.chkPostoAbastecidasExibirVendaClick(
  Sender: TObject);
begin
  if chkPostoAbastecidasExibirVenda.Checked then
    chkPostoAbastecidasEventosVenda.Checked:=True
  else
    chkPostoAbastecidasEventosVenda.Checked:=False;
end;

procedure TfrmConfiguracao.edtFrenteFormaPagExit(Sender: TObject);
begin
  edtFPagDesc.Text := dmCupomFiscal.getDescFormaPag(StrToIntDef(edtFrenteFormaPag.Text,0));
end;

procedure TfrmConfiguracao.edtFrenteFormaPagKeyPress(Sender: TObject;
  var Key: Char);
begin
  if not(somenteNumeros(Key))then
    Key := #0;
end;

procedure TfrmConfiguracao.edtFrenteOperacaoExit(Sender: TObject);
begin
  edtOpeDesc.Text := dmCupomFiscal.getDescOperacao(StrToIntDef(edtFrenteOperacao.Text,0));
end;

procedure TfrmConfiguracao.chkPostoAbastecidasEventosVendaClick(
  Sender: TObject);
begin
  if chkPostoAbastecidasEventosVenda.Checked then
    chkPostoAbastecidasExibirVenda.Checked:=True
  else
    chkPostoAbastecidasExibirVenda.Checked:=False;
end;

end.

