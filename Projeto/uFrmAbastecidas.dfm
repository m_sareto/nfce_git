object frmAbastecidas: TfrmAbastecidas
  Left = 290
  Top = 139
  BorderIcons = []
  Caption = 'Abastecidas'
  ClientHeight = 513
  ClientWidth = 712
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDesktopCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object grdAbastecidas: TDBGrid
    Left = 0
    Top = 0
    Width = 712
    Height = 482
    Align = alClient
    Color = clWhite
    Ctl3D = False
    DataSource = DataSource1
    DrawingStyle = gdsClassic
    FixedColor = clWhite
    Options = [dgTitles, dgColumnResize, dgColLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
    ParentCtl3D = False
    PopupMenu = ppmAbastecidas
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnCellClick = grdAbastecidasCellClick
    OnDrawColumnCell = grdAbastecidasDrawColumnCell
    OnKeyDown = grdAbastecidasKeyDown
    OnMouseMove = grdAbastecidasMouseMove
    Columns = <
      item
        Expanded = False
        FieldName = 'ID'
        Title.Alignment = taCenter
        Title.Caption = 'Abastecida'
        Title.Color = clBtnFace
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Data_Hora'
        Title.Alignment = taCenter
        Title.Caption = 'Data/Hora'
        Title.Color = clBtnFace
        Width = 141
        Visible = True
      end
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'BOMBA'
        Title.Alignment = taCenter
        Title.Caption = 'Bomba'
        Title.Color = clBtnFace
        Width = 39
        Visible = True
      end
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'PRODUTO'
        Title.Alignment = taCenter
        Title.Caption = 'Bico'
        Title.Color = clBtnFace
        Width = 37
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PRECO'
        Title.Alignment = taCenter
        Title.Caption = 'Pre'#231'o'
        Title.Color = clBtnFace
        Width = 76
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'LITRO'
        Title.Alignment = taCenter
        Title.Caption = 'Litro'
        Title.Color = clBtnFace
        Width = 72
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DINHEIRO'
        Title.Alignment = taCenter
        Title.Caption = 'Dinheiro'
        Title.Color = clBtnFace
        Width = 71
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Frentista'
        Title.Color = clBtnFace
        Width = 148
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Pago'
        Title.Color = clBtnFace
        Visible = True
      end>
  end
  object Panel1: TPanel
    Left = 0
    Top = 482
    Width = 712
    Height = 31
    Align = alBottom
    TabOrder = 1
    object CheckBox1: TCheckBox
      Left = 396
      Top = 8
      Width = 169
      Height = 17
      Alignment = taLeftJustify
      Caption = 'Excluir Abastecida Selecionada'
      TabOrder = 0
      OnClick = CheckBox1Click
    end
    object BitBtn1: TBitBtn
      Left = 592
      Top = 3
      Width = 80
      Height = 25
      Caption = 'Fechar'
      Glyph.Data = {
        36060000424D3606000000000000360000002800000020000000100000000100
        18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFECF2F6CCDBE8A5C1D680A7
        C56394B8165E931D6397FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFF1F1F1DADADABEBEBEA3A3A38E8E8E5656565B5B5B999999717171
        5454545151514F4F4F4C4C4C4A4A4A47474745454525679D3274A83D7CAF4784
        B54E8ABA3E7EAD2065989999997171715454545151514F4F4F4C4C4C4A4A4A47
        47474545456262626E6E6E7777777F7F7F8585857777775D5D5DFFFFFFFFFFFF
        585858A2A2A2A2A2A2A3A3A3A4A4A4A4A4A4A5A5A52F6FA578ABD278ABD373A7
        D169A0CD407FAE23679AFFFFFFFFFFFF585858A2A2A2A2A2A2A3A3A3A4A4A4A4
        A4A4A5A5A56B6B6BA6A6A6A7A7A7A3A3A39C9C9C797979606060FFFFFFFFFFFF
        5C5C5CA1A1A13C7340A0A1A1A3A3A3A3A3A3A4A4A43674AA7DAFD45B9AC95495
        C75896C84180AE26699DFFFFFFFFFFFF5C5C5CA1A1A1535353A1A1A1A3A3A3A3
        A3A3A4A4A4717171AAAAAA9494948F8F8F919191797979636363FFFFFFFFFFFF
        606060A0A0A03D7641367139A2A2A2A2A2A2A3A3A33D79B082B3D7629FCC5A9A
        C95E9BCA4381AF2C6DA0FFFFFFFFFFFF606060A0A0A05555554E4E4EA2A2A2A2
        A2A2A3A3A3777777AEAEAE9999999393939595957B7B7B68686837823E347E3B
        3179372E7534499150468F4C39733DA1A1A1A2A2A2457EB488B7D967A3CF619E
        CC639FCC4583B13171A45656565353534F4F4F4C4C4C676767646464515151A1
        A1A1A2A2A27D7D7DB2B2B29D9D9D9898989999997D7D7D6C6C6C3B874289CB92
        84C88D80C6887BC38377C17F478F4D3B743FA1A1A14C84BA8DBBDB6EA8D166A6
        D15FB4DF4785B13775A95B5B5BA5A5A5A1A1A19E9E9E99999996969665656553
        5353A1A1A1838383B5B5B5A1A1A19E9E9EA3A3A37E7E7E7171713E8B468FCE99
        7DC68778C38173C07C74C07C79C28149904F547F575489BF94BFDD75ADD463B8
        E14BD4FF428BB83D7AAD5E5E5EAAAAAA9C9C9C98989894949494949498989867
        6767666666898989BABABAA6A6A6A6A6A6AEAEAE80808076767641904A94D29F
        91D09A8DCD9689CB9284C88D519858417C469F9F9F5A8EC498C3E07CB3D774AF
        D65EC4ED4B88B3457FB2626262AFAFAFACACACA8A8A8A5A5A5A1A1A16F6F6F5A
        5A5A9F9F9F8F8F8FBDBDBDABABABA7A7A7ACACAC8181817C7C7C44944D42914B
        3F8D483D89455DA4655AA06145834B9E9E9E9E9E9E6092C99EC7E283B8DA7DB4
        D77EB3D74F89B44B84B76666666363636060605D5D5D7B7B7B7777775F5F5F9E
        9E9E9E9E9E949494C1C1C1B0B0B0ACACACACACAC838383828282FFFFFFFFFFFF
        7777779A9A9A3D8A45498A4F9C9C9C9D9D9D9D9D9D6696CCA2CBE389BDDC83B9
        DA84B9DA518BB55289BCFFFFFFFFFFFF7777779A9A9A5D5D5D6464649C9C9C9D
        9D9D9D9D9D989898C4C4C4B5B5B5B1B1B1B1B1B1858585888888FFFFFFFFFFFF
        7A7A7A999999529159999A999B9B9B9C9C9C9C9C9C6C9AD0A7CEE58FC1DF89BD
        DC8BBDDC538DB65A8EC2FFFFFFFFFFFF7A7A7A9999996D6D6D9999999B9B9B9C
        9C9C9C9C9C9D9D9DC8C8C8B9B9B9B5B5B5B5B5B58686868E8E8EFFFFFFFFFFFF
        7D7D7D9999999999999A9A9A9A9A9A9B9B9B9B9B9B6F9DD3AAD1E7ABD1E798C7
        E191C2DE568FB76093C6FFFFFFFFFFFF7D7D7D9999999999999A9A9A9A9A9A9B
        9B9B9B9B9BA0A0A0CACACACBCBCBBFBFBFBABABA888888939393FFFFFFFFFFFF
        8080807E7E7E7C7C7C7A7A7A777777757575727272719ED46F9ED687B2DCABD3
        E8A9D0E65890B86797CBFFFFFFFFFFFF8080807E7E7E7C7C7C7A7A7A77777775
        7575727272A2A2A2A2A2A2B2B2B2CBCBCBC9C9C98A8A8A989898FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF84ACDC6D9C
        D485B1DA5A91B96D9CCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFAFAFAFA0A0A0B0B0B08B8B8B9E9E9EFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFB1CAE86C9CD3709ED2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCCCCCC9F9F9FA1A1A1}
      NumGlyphs = 2
      TabOrder = 1
      OnClick = BitBtn1Click
    end
  end
  object Panel2: TPanel
    Left = 184
    Top = 224
    Width = 313
    Height = 41
    Caption = 'Aguarde, Carregando Dados...'
    Font.Charset = ANSI_CHARSET
    Font.Color = clRed
    Font.Height = -16
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
    Visible = False
  end
  object DataSource1: TDataSource
    DataSet = cdsAbastecidas
    Left = 173
    Top = 70
  end
  object cdsAbastecidas: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'ID'
        Attributes = [faRequired]
        DataType = ftInteger
      end
      item
        Name = 'BOMBA'
        Attributes = [faRequired]
        DataType = ftInteger
      end
      item
        Name = 'PRODUTO'
        Attributes = [faRequired]
        DataType = ftString
        Size = 13
      end
      item
        Name = 'PRECO'
        DataType = ftBCD
        Precision = 18
        Size = 3
      end
      item
        Name = 'LITRO'
        DataType = ftBCD
        Precision = 18
        Size = 3
      end
      item
        Name = 'DINHEIRO'
        DataType = ftBCD
        Precision = 18
        Size = 2
      end
      item
        Name = 'NIVEPRECO'
        DataType = ftInteger
      end
      item
        Name = 'Data_Hora'
        DataType = ftDateTime
      end
      item
        Name = 'Status'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'Frentista'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'Temp_ECF_Item'
        DataType = ftBoolean
      end
      item
        Name = 'Pago'
        DataType = ftBoolean
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 104
    Top = 72
    object cdsAbastecidasID: TIntegerField
      FieldName = 'ID'
      Origin = '"ABASTECIMENTOS"."ID"'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object cdsAbastecidasBOMBA: TIntegerField
      FieldName = 'BOMBA'
      Origin = '"ABASTECIMENTOS"."BOMBA"'
      Required = True
    end
    object cdsAbastecidasPRODUTO: TStringField
      FieldName = 'PRODUTO'
      Origin = '"ABASTECIMENTOS"."PRODUTO"'
      Required = True
      Size = 13
    end
    object cdsAbastecidasPRECO: TBCDField
      FieldName = 'PRECO'
      Origin = '"ABASTECIMENTOS"."PRECO"'
      DisplayFormat = '###,##0.000'
      Precision = 18
      Size = 3
    end
    object cdsAbastecidasLITRO: TBCDField
      FieldName = 'LITRO'
      Origin = '"ABASTECIMENTOS"."LITRO"'
      DisplayFormat = '###,##0.000'
      Precision = 18
      Size = 3
    end
    object cdsAbastecidasDINHEIRO: TBCDField
      FieldName = 'DINHEIRO'
      Origin = '"ABASTECIMENTOS"."DINHEIRO"'
      DisplayFormat = '###,##0.00'
      Precision = 18
      Size = 2
    end
    object cdsAbastecidasNIVEPRECO: TIntegerField
      FieldName = 'NIVEPRECO'
      Origin = '"ABASTECIMENTOS"."NIVEPRECO"'
    end
    object cdsAbastecidasData_Hora: TDateTimeField
      FieldName = 'Data_Hora'
    end
    object cdsAbastecidasStatus: TStringField
      FieldName = 'Status'
      Size = 1
    end
    object cdsAbastecidasFrentista: TStringField
      FieldName = 'Frentista'
      Size = 10
    end
    object cdsAbastecidasTemp_ECF_Item: TBooleanField
      FieldName = 'Temp_ECF_Item'
    end
    object cdsAbastecidasPago: TBooleanField
      FieldName = 'Pago'
    end
  end
  object PopupMenu1: TPopupMenu
    Left = 243
    Top = 71
    object Pago1: TMenuItem
      Caption = '&Pago'
      OnClick = Pago1Click
    end
    object Pagar1: TMenuItem
      Caption = #192' P&agar'
      OnClick = Pagar1Click
    end
  end
  object ppmAbastecidas: TPopupMenu
    Left = 140
    Top = 152
    object UsoeConsumo1: TMenuItem
      Caption = 'Uso e Consumo'
      ImageIndex = 5
      OnClick = UsoeConsumo1Click
    end
    object Aferio1: TMenuItem
      Caption = 'Aferi'#231#227'o'
      OnClick = Aferio1Click
    end
  end
end
