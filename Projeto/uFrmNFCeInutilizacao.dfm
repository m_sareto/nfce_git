object frmNFCeInutilizacao: TfrmNFCeInutilizacao
  Left = 434
  Top = 278
  Caption = 'Pedido de Inutilliza'#231#227'o NFC-e'
  ClientHeight = 165
  ClientWidth = 362
  Color = clBtnHighlight
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDesktopCenter
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object Label3: TLabel
    Left = 12
    Top = 81
    Width = 24
    Height = 13
    Caption = 'S'#233'rie'
  end
  object Label4: TLabel
    Left = 59
    Top = 81
    Width = 37
    Height = 13
    Caption = 'N'#250'mero'
  end
  object lblLocal: TLabel
    Left = 12
    Top = 13
    Width = 56
    Height = 16
    Caption = 'lblLocal'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label1: TLabel
    Left = 12
    Top = 41
    Width = 76
    Height = 13
    Caption = 'Centro de Custo'
  end
  object edtSerie: TEdit
    Left = 12
    Top = 95
    Width = 35
    Height = 19
    Ctl3D = False
    MaxLength = 2
    ParentCtl3D = False
    TabOrder = 0
    Text = '0'
    OnKeyPress = edtSerieKeyPress
  end
  object edtNrInicial: TEdit
    Left = 59
    Top = 95
    Width = 60
    Height = 19
    Ctl3D = False
    MaxLength = 7
    ParentCtl3D = False
    TabOrder = 1
    Text = '0'
    OnExit = edtNrInicialExit
    OnKeyPress = edtNrInicialKeyPress
  end
  object btnEnviar: TBitBtn
    Left = 8
    Top = 129
    Width = 72
    Height = 25
    Caption = 'Enviar'
    Glyph.Data = {
      36060000424D3606000000000000360000002800000020000000100000000100
      18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFD5E0D577AD6641882641802641802641882677AD66D5E0D5FFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD9D9D98686865353534F
      4F4F4F4F4F535353868686D9D9D9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFF77AD6641882D579A485EA4576FAD666FAD665EA457579A4841882D77AD
      66FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8686865555556C6C6C77777784
      84848484847777776C6C6C555555868686FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      77AD6648913A5EA457ADCBADADCBAD3A88333A883A489A4157A4575EA4574891
      3A77AD66FFFFFFFFFFFFFFFFFFFFFFFF868686606060777777B9B9B9B9B9B956
      5656595959666666767676777777606060868686FFFFFFFFFFFFFFFFFFD5E0D5
      41882D579A4F419141ADCBADFFFFFFADCBAD41883A41883A4191414F9A485EA4
      5741882DD5E0D5FFFFFFFFFFFFD9D9D95555556F6F6F616161B9B9B9FFFFFFB9
      B9B95B5B5B5B5B5B6161616A6A6A777777555555D9D9D9FFFFFFFFFFFF77AD66
      4F91414F91484F9148ADCBADFFFFFFFFFFFFADCBAD4F91484F91484F9A48579A
      4F57914177AD66FFFFFFFFFFFF868686646464676767676767B9B9B9FFFFFFFF
      FFFFB9B9B96767676767676A6A6A6F6F6F656565868686FFFFFFFFFFFF418026
      33802D33802D33802DADCBADFFFFFFFFFFFFFFFFFFADCBAD33802D33802D4888
      415EA457418026FFFFFFFFFFFF4F4F4F4F4F4F4F4F4F4F4F4FB9B9B9FFFFFFFF
      FFFFFFFFFFB9B9B94F4F4F4F4F4F5F5F5F7777774F4F4FFFFFFFFFFFFF418020
      3A77333A77333A7733ADCBADFFFFFFFFFFFFFFFFFFFFFFFFBAD2BA3A77333A77
      336F9A5E418020FFFFFFFFFFFF4D4D4D505050505050505050B9B9B9FFFFFFFF
      FFFFFFFFFFFFFFFFC4C4C45050505050507979794D4D4DFFFFFFFFFFFF488833
      488048488048488048ADCBADFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF78E9C24880
      4877AD77418026FFFFFFFFFFFF5959595E5E5E5E5E5E5E5E5EB9B9B9FFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFB4B4B45E5E5E8D8D8D4F4F4FFFFFFFFFFFFF488833
      579A5E579A5E579A5EBAD2BAFFFFFFFFFFFFFFFFFFFFFFFF26CC80579A5E579A
      5E77AD77488833FFFFFFFFFFFF595959737373737373737373C4C4C4FFFFFFFF
      FFFFFFFFFFFFFFFF7A7A7A7373737373738D8D8D595959FFFFFFFFFFFF77AD66
      579A4F5EA46F5EA46FBAD2BAFFFFFFFFFFFFFFFFFF26CC805EA46F5EA46F5EA4
      6F5EA46F77AD66FFFFFFFFFFFF8686866F6F6F7D7D7D7D7D7DC4C4C4FFFFFFFF
      FFFFFFFFFF7A7A7A7D7D7D7D7D7D7D7D7D7D7D7D868686FFFFFFFFFFFFD5E0D5
      4888336FAD806FB788BAD2BAFFFFFFFFFFFF26CC806FB7886FB7886FB7886FB7
      88488833D5E0D5FFFFFFFFFFFFD9D9D95959598B8B8B919191C4C4C4FFFFFFFF
      FFFF7A7A7A919191919191919191919191595959D9D9D9FFFFFFFFFFFFFFFFFF
      77AD6657914877B78826CC80FFFFFF26CC806FB7916FB79177B79177B7915791
      4877AD66FFFFFFFFFFFFFFFFFFFFFFFF8686866868689494947A7A7AFFFFFF7A
      7A7A939393939393969696969696686868868686FFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFF77AD6648883326CC8026CC8088C19A88C1A480B78866A4664F883A77AD
      66FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8686865959597A7A7A7A7A7AA2
      A2A2A4A4A49898987F7F7F5D5D5D868686FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFD5E0D577AD664F883A4180264180264F883A77AD66D5E0D5FFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD9D9D98686865D5D5D4F
      4F4F4F4F4F5D5D5D868686D9D9D9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
    NumGlyphs = 2
    TabOrder = 2
    OnClick = btnEnviarClick
  end
  object btnFechar: TBitBtn
    Left = 84
    Top = 129
    Width = 72
    Height = 25
    Caption = 'Fechar'
    Glyph.Data = {
      36060000424D3606000000000000360000002800000020000000100000000100
      18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFECF2F6CCDBE8A5C1D680A7
      C56394B8165E931D6397FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFF1F1F1DADADABEBEBEA3A3A38E8E8E5656565B5B5B999999717171
      5454545151514F4F4F4C4C4C4A4A4A47474745454525679D3274A83D7CAF4784
      B54E8ABA3E7EAD2065989999997171715454545151514F4F4F4C4C4C4A4A4A47
      47474545456262626E6E6E7777777F7F7F8585857777775D5D5DFFFFFFFFFFFF
      585858A2A2A2A2A2A2A3A3A3A4A4A4A4A4A4A5A5A52F6FA578ABD278ABD373A7
      D169A0CD407FAE23679AFFFFFFFFFFFF585858A2A2A2A2A2A2A3A3A3A4A4A4A4
      A4A4A5A5A56B6B6BA6A6A6A7A7A7A3A3A39C9C9C797979606060FFFFFFFFFFFF
      5C5C5CA1A1A13C7340A0A1A1A3A3A3A3A3A3A4A4A43674AA7DAFD45B9AC95495
      C75896C84180AE26699DFFFFFFFFFFFF5C5C5CA1A1A1535353A1A1A1A3A3A3A3
      A3A3A4A4A4717171AAAAAA9494948F8F8F919191797979636363FFFFFFFFFFFF
      606060A0A0A03D7641367139A2A2A2A2A2A2A3A3A33D79B082B3D7629FCC5A9A
      C95E9BCA4381AF2C6DA0FFFFFFFFFFFF606060A0A0A05555554E4E4EA2A2A2A2
      A2A2A3A3A3777777AEAEAE9999999393939595957B7B7B68686837823E347E3B
      3179372E7534499150468F4C39733DA1A1A1A2A2A2457EB488B7D967A3CF619E
      CC639FCC4583B13171A45656565353534F4F4F4C4C4C676767646464515151A1
      A1A1A2A2A27D7D7DB2B2B29D9D9D9898989999997D7D7D6C6C6C3B874289CB92
      84C88D80C6887BC38377C17F478F4D3B743FA1A1A14C84BA8DBBDB6EA8D166A6
      D15FB4DF4785B13775A95B5B5BA5A5A5A1A1A19E9E9E99999996969665656553
      5353A1A1A1838383B5B5B5A1A1A19E9E9EA3A3A37E7E7E7171713E8B468FCE99
      7DC68778C38173C07C74C07C79C28149904F547F575489BF94BFDD75ADD463B8
      E14BD4FF428BB83D7AAD5E5E5EAAAAAA9C9C9C98989894949494949498989867
      6767666666898989BABABAA6A6A6A6A6A6AEAEAE80808076767641904A94D29F
      91D09A8DCD9689CB9284C88D519858417C469F9F9F5A8EC498C3E07CB3D774AF
      D65EC4ED4B88B3457FB2626262AFAFAFACACACA8A8A8A5A5A5A1A1A16F6F6F5A
      5A5A9F9F9F8F8F8FBDBDBDABABABA7A7A7ACACAC8181817C7C7C44944D42914B
      3F8D483D89455DA4655AA06145834B9E9E9E9E9E9E6092C99EC7E283B8DA7DB4
      D77EB3D74F89B44B84B76666666363636060605D5D5D7B7B7B7777775F5F5F9E
      9E9E9E9E9E949494C1C1C1B0B0B0ACACACACACAC838383828282FFFFFFFFFFFF
      7777779A9A9A3D8A45498A4F9C9C9C9D9D9D9D9D9D6696CCA2CBE389BDDC83B9
      DA84B9DA518BB55289BCFFFFFFFFFFFF7777779A9A9A5D5D5D6464649C9C9C9D
      9D9D9D9D9D989898C4C4C4B5B5B5B1B1B1B1B1B1858585888888FFFFFFFFFFFF
      7A7A7A999999529159999A999B9B9B9C9C9C9C9C9C6C9AD0A7CEE58FC1DF89BD
      DC8BBDDC538DB65A8EC2FFFFFFFFFFFF7A7A7A9999996D6D6D9999999B9B9B9C
      9C9C9C9C9C9D9D9DC8C8C8B9B9B9B5B5B5B5B5B58686868E8E8EFFFFFFFFFFFF
      7D7D7D9999999999999A9A9A9A9A9A9B9B9B9B9B9B6F9DD3AAD1E7ABD1E798C7
      E191C2DE568FB76093C6FFFFFFFFFFFF7D7D7D9999999999999A9A9A9A9A9A9B
      9B9B9B9B9BA0A0A0CACACACBCBCBBFBFBFBABABA888888939393FFFFFFFFFFFF
      8080807E7E7E7C7C7C7A7A7A777777757575727272719ED46F9ED687B2DCABD3
      E8A9D0E65890B86797CBFFFFFFFFFFFF8080807E7E7E7C7C7C7A7A7A77777775
      7575727272A2A2A2A2A2A2B2B2B2CBCBCBC9C9C98A8A8A989898FFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF84ACDC6D9C
      D485B1DA5A91B96D9CCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFAFAFAFA0A0A0B0B0B08B8B8B9E9E9EFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFB1CAE86C9CD3709ED2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCCCCCC9F9F9FA1A1A1}
    NumGlyphs = 2
    TabOrder = 3
    OnClick = btnFecharClick
  end
  object lcbCCusto: TDBLookupComboBox
    Left = 12
    Top = 55
    Width = 330
    Height = 21
    KeyField = 'ID'
    ListField = 'ID;DESCRICAO'
    ListFieldIndex = 1
    ListSource = dsCCusto
    TabOrder = 4
  end
  object dsCCusto: TDataSource
    DataSet = qCCusto
    Left = 328
    Top = 122
  end
  object qCCusto: TIBQuery
    Database = dmCupomFiscal.DataBase
    Transaction = dmCupomFiscal.Transaction
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'select id, descricao from CCUSTOS where LOCAL=:pLocal')
    Left = 296
    Top = 122
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'pLocal'
        ParamType = ptUnknown
      end>
    object qCCustoID: TIntegerField
      FieldName = 'ID'
      Origin = '"CCUSTOS"."ID"'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object qCCustoDESCRICAO: TIBStringField
      FieldName = 'DESCRICAO'
      Origin = '"CCUSTOS"."DESCRICAO"'
      Required = True
      Size = 40
    end
  end
end
