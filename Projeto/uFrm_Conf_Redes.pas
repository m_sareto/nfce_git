unit uFrm_Conf_Redes;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ShellCtrls, StdCtrls, Buttons, ExtCtrls, ComCtrls,
  uFrmPrincipal, PngBitBtn, UFrmFim, uDMCupomFiscal;

type
  TfrmTefConfigRedes = class(TForm)
    StatusBar1: TStatusBar;
    mmRetorno: TMemo;
    Panel1: TPanel;
    btnInicializar: TPngBitBtn;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmTefConfigRedes: TfrmTefConfigRedes;

implementation

uses
  ACBrTEFDClass, uDMComponentes;

{$R *.dfm}

procedure TfrmTefConfigRedes.FormShow(Sender: TObject);
begin
  //Faz o alinhamento do bot�o
  btnInicializar.Left:=Trunc((Panel1.Width/2)-(btnInicializar.Width/2));
  btnInicializar.Top :=Trunc((Panel1.Height/2)-(btnInicializar.Height/2));
end;

end.
