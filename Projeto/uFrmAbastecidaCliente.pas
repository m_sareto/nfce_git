unit uFrmAbastecidaCliente;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, Buttons, DB, IBCustomDataSet, IBQuery,
  Tredit;

type
  TfrmAbastecidaCliente = class(TForm)
    Bevel1: TBevel;
    edtCliente: TEdit;
    Label11: TLabel;
    edtClienteNome: TEdit;
    btnOK: TBitBtn;
    btnCancelar: TBitBtn;
    qurClifor: TIBQuery;
    edtBico: TEdit;
    edtMercadoriaDesc: TEdit;
    reLitros: TRealEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    procedure Label11Click(Sender: TObject);
    procedure edtClienteExit(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnOKClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
    constructor Create(AOwner: TComponent; CLIFOR, BICO:Integer; MERCADORIA_DESC:String; LITROS:Currency);
    reintroduce;
  end;

var
  frmAbastecidaCliente: TfrmAbastecidaCliente;

implementation

uses uFrmPesquisaCliente, uFuncoes, uDMCupomFiscal;

{$R *.dfm}

procedure TfrmAbastecidaCliente.Label11Click(Sender: TObject);
begin
   FrmPesquisaCliente := TFrmPesquisaCliente.Create(Self);
   try
   if (FrmPesquisaCliente.ShowModal=mrOK) then
      edtCliente.Text:= FrmPesquisaCliente.qCliFor.FieldByName('ID').AsString;
   finally
      FrmPesquisaCliente.Free;
   end;
end;

procedure TfrmAbastecidaCliente.edtClienteExit(Sender: TObject);
begin
  If (Trim(edtCliente.Text)<>'') then
  begin
    qurClifor.Active:=False;
    qurClifor.SQL.Clear;
    qurClifor.SQL.Add('Select Id, Nome, Ativo FROM CliFor Where ID=:pID');
    qurClifor.ParamByName('pID').AsInteger:=StrToIntDef(edtCliente.Text,0);
    qurClifor.Active:=True;
    qurClifor.First;
    If not(qurClifor.Eof) then
    begin
      if qurClifor.FieldByName('Ativo').AsString = 'N' then
      begin
        edtCliente.Clear;
        edtClienteNome.Clear;
        msgAviso('Cliente '+qurClifor.FieldByName('Nome').AsString+' esta Inativo','');
      end
      else
        edtClienteNome.Text:=qurClifor.FieldByName('Nome').AsString;
    end
    else
    begin
      edtCliente.SetFocus;
      msgAviso('Cliente n�o cadastrado','');
    end;
  end
  else
    edtClienteNome.Clear;
end;

procedure TfrmAbastecidaCliente.btnCancelarClick(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

procedure TfrmAbastecidaCliente.btnOKClick(Sender: TObject);
begin
  if (Trim(edtCliente.Text) = '') then
    ModalResult := mrIgnore
  else
    if (StrToIntDef(edtCliente.Text,0) > 0) then
      ModalResult := mrOK;
end;

constructor TfrmAbastecidaCliente.Create(AOwner: TComponent; CLIFOR, BICO:Integer; MERCADORIA_DESC:String; LITROS:Currency);
begin
  inherited Create(AOwner);
  edtBico.Text := IntToStr(BICO);
  edtMercadoriaDesc.Text := MERCADORIA_DESC;
  reLitros.Value := LITROS;
  if CLIFOR <> 0 then
  begin
    edtCliente.Text:=IntToStr(CLIFOR);
    edtClienteExit(Action);
  end;
end;

procedure TfrmAbastecidaCliente.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  If (key=vk_F12) and (edtCliente.Focused) then
  begin
    Label11Click(action);
  end;
end;

end.
