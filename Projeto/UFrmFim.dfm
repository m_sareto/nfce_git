object frmFim: TfrmFim
  Left = 286
  Top = 129
  Align = alClient
  BorderIcons = [biMinimize, biMaximize, biHelp]
  Caption = '.:: Frente de Caixa ::.'
  ClientHeight = 645
  ClientWidth = 1275
  Color = clWindow
  Ctl3D = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDesigned
  WindowState = wsMaximized
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnlFundo: TPanel
    Left = 0
    Top = 41
    Width = 534
    Height = 542
    BevelOuter = bvNone
    BorderWidth = 1
    Color = clBlack
    TabOrder = 0
    object pnlGeral: TPanel
      Left = 1
      Top = 1
      Width = 532
      Height = 540
      Align = alClient
      BevelOuter = bvNone
      Color = clWhite
      ParentBackground = False
      TabOrder = 0
      object pagControle: TPageControl
        Left = 0
        Top = 0
        Width = 532
        Height = 540
        ActivePage = tabPrincipal
        Align = alClient
        MultiLine = True
        TabOrder = 0
        TabPosition = tpLeft
        TabStop = False
        object tabPrincipal: TTabSheet
          Caption = 'F7 - NFC-e'
          object Panel2: TPanel
            Left = 7
            Top = 0
            Width = 492
            Height = 32
            Alignment = taLeftJustify
            Caption = 'Desconto Parcial R$'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -24
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentBackground = False
            ParentFont = False
            TabOrder = 0
            object edtVlrDescontoParcial: TEdit
              Left = 357
              Top = 1
              Width = 134
              Height = 30
              TabStop = False
              Align = alRight
              BiDiMode = bdRightToLeft
              Ctl3D = False
              Enabled = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -19
              Font.Name = 'Arial'
              Font.Style = [fsBold]
              ParentBiDiMode = False
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 0
              ExplicitHeight = 28
            end
          end
          object pnlRecebido: TPanel
            Left = 7
            Top = 469
            Width = 492
            Height = 32
            Alignment = taLeftJustify
            Caption = 'Recebido R$'
            Color = clMoneyGreen
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -24
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentBackground = False
            ParentFont = False
            TabOrder = 6
            object edtVlrRecebido: TDBEdit
              Left = 357
              Top = 1
              Width = 134
              Height = 30
              Align = alRight
              Ctl3D = False
              DataField = 'VLR_RECEBIDO'
              DataSource = dsTempECF
              Enabled = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -19
              Font.Name = 'Arial'
              Font.Style = [fsBold]
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 0
              ExplicitHeight = 28
            end
          end
          object pnlTroco: TPanel
            Left = 7
            Top = 501
            Width = 492
            Height = 32
            Alignment = taLeftJustify
            Caption = 'Troco R$'
            Color = clMoneyGreen
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -24
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentBackground = False
            ParentFont = False
            TabOrder = 7
            object edtVlrTroco: TDBEdit
              Left = 357
              Top = 1
              Width = 134
              Height = 30
              Align = alRight
              Ctl3D = False
              DataField = 'VLR_TROCO'
              DataSource = dsTempECF
              Enabled = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -19
              Font.Name = 'Arial'
              Font.Style = [fsBold]
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 0
              ExplicitHeight = 28
            end
          end
          object pnlSubtotal: TPanel
            Left = 7
            Top = 405
            Width = 492
            Height = 32
            Alignment = taLeftJustify
            Caption = 'Subtotal R$'
            Color = clMoneyGreen
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -24
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentBackground = False
            ParentFont = False
            TabOrder = 4
            object edtVlrSubtotal: TDBEdit
              Left = 357
              Top = 1
              Width = 134
              Height = 30
              Align = alRight
              Ctl3D = False
              DataField = 'VLR_SUBTOTAL'
              DataSource = dsTempECF
              Enabled = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -19
              Font.Name = 'Arial'
              Font.Style = [fsBold]
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 0
              ExplicitHeight = 28
            end
          end
          object pnlTotal: TPanel
            Left = 7
            Top = 437
            Width = 492
            Height = 32
            Alignment = taLeftJustify
            Caption = 'Total R$'
            Color = clMoneyGreen
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -24
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentBackground = False
            ParentFont = False
            TabOrder = 5
            object edtVlrTotal: TDBEdit
              Left = 357
              Top = 1
              Width = 134
              Height = 30
              Align = alRight
              Ctl3D = False
              DataField = 'VLR_TOTAL'
              DataSource = dsTempECF
              Enabled = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -19
              Font.Name = 'Arial'
              Font.Style = [fsBold]
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 0
              ExplicitHeight = 28
            end
          end
          object pnlDesconto: TPanel
            Left = 7
            Top = 32
            Width = 492
            Height = 32
            Alignment = taLeftJustify
            Caption = 'Desconto R$'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -24
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentBackground = False
            ParentFont = False
            TabOrder = 1
            object lblDescPerc: TLabel
              Left = 167
              Top = 11
              Width = 35
              Height = 16
              Caption = '0,00%'
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Arial'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object edtVlrDesconto: TDBEdit
              Left = 357
              Top = 1
              Width = 134
              Height = 30
              Align = alRight
              Ctl3D = False
              DataField = 'VLR_DESCONTO'
              DataSource = dsTempECF
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -19
              Font.Name = 'Arial'
              Font.Style = [fsBold]
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 0
              OnEnter = edtVlrDescontoEnter
              OnExit = edtVlrDescontoExit
              ExplicitHeight = 28
            end
          end
          object pnlPagamento: TPanel
            Left = 8
            Top = 101
            Width = 491
            Height = 302
            BevelOuter = bvNone
            Color = clWhite
            ParentBackground = False
            TabOrder = 3
            object Label19: TLabel
              Left = 67
              Top = -3
              Width = 150
              Height = 16
              Caption = 'Forma de Pagamento'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object Label20: TLabel
              Left = 299
              Top = -3
              Width = 61
              Height = 16
              Caption = 'Valor R$'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object Label2: TLabel
              Left = 0
              Top = -3
              Width = 51
              Height = 16
              Caption = 'C'#243'digo'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object grdTemp_ECF_FormaPag: TDBGrid
              Left = 0
              Top = 45
              Width = 491
              Height = 257
              TabStop = False
              Align = alBottom
              Ctl3D = False
              DataSource = dsTemp_ECF_FormaPag
              DrawingStyle = gdsClassic
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -16
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Options = [dgTitles, dgColumnResize, dgColLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 4
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -13
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = [fsBold]
              OnDrawColumnCell = grdTemp_ECF_FormaPagDrawColumnCell
              OnDblClick = grdTemp_ECF_FormaPagDblClick
              OnMouseUp = grdTemp_ECF_FormaPagMouseUp
              Columns = <
                item
                  Expanded = False
                  Width = 27
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'TPAG_DESCRICAO'
                  Title.Caption = 'Forma de Pagamento'
                  Width = 318
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'VPAG'
                  Title.Alignment = taRightJustify
                  Title.Caption = 'Valor R$'
                  Width = 113
                  Visible = True
                end>
            end
            object lcmbFormaPagamento: TDBLookupComboBox
              Left = 67
              Top = 13
              Width = 228
              Height = 30
              Ctl3D = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -19
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              KeyField = 'ID'
              ListField = 'DESCRICAO'
              ListSource = dsFormaPagamento
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 1
              TabStop = False
            end
            object btnPagar: TBitBtn
              Left = 413
              Top = 13
              Width = 77
              Height = 30
              Caption = 'Pagar'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              Glyph.Data = {
                36060000424D3606000000000000360000002800000020000000100000000100
                18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFEFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFABE7AF8CE693FAFFFBFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC4C4C4B1B1B1FC
                FCFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFAAE1AF52CC5A78EE8198EA9DF8FFF9FFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC1C1C1848484A9A9A9BA
                BABAFBFBFBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFA1D7A53AC14460FF6F67FF7659F16676E07DF9FEF9FFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB7B7B7727272A3A3A3A7A7A798
                9898A2A2A2FBFBFBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                A1CDA42BB53752E9607AEF847AF28559F5684AE8576ED676F4FCF5FFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB3B3B3656565919191ABABABACACAC9A
                9A9A8C8C8C999999F7F7F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFCFB2
                29A33552D85F8BE4938EE69688E59072E77D49E75940D74D63BF6AF6FBF7FFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFBCBCBC5C5C5C8A8A8AB0B0B0B3B3B3AFAFAFA3
                A3A38B8B8B7F7F7F898989F8F8F8FFFFFFFFFFFFFFFFFFFFFFFFB5D6B862B568
                74D27D9EDFA498DF9F57C96174D27C7DDE876BDC763AD84932C53E55A65CF4F8
                F4FFFFFFFFFFFFFFFFFFC3C3C38484849B9B9BB9B9B9B6B6B68787879B9B9BA6
                A6A69A9A9A7C7C7C6F6F6F777777F6F6F6FFFFFFFFFFFFFFFFFFD4E9D680C787
                B4DDB8A9DAAD57C162C6ECCAF8FDF969CE7271D27A61D16B29C93823B02F5B95
                5FF4F7F4FFFFFFFFFFFFDDDDDD9E9E9EC5C5C5BDBDBD848484D6D6D6FAFAFA93
                93939A9A9A9090906C6C6C5E5E5E737373F5F5F5FFFFFFFFFFFFFFFFFFDDF1DE
                67C56F6AC372C6EAC8FFFFFFFFFFFFF9FDFA60C5696BC87457C56214B72413A6
                21578C5CF8FAF8FFFFFFFFFFFFE5E5E58E8E8E8F8F8FD5D5D5FFFFFFFFFFFFFB
                FBFB8A8A8A9292928585855858585151516D6D6DF9F9F9FFFFFFFFFFFFFFFFFF
                DBF2DDCFEDD1FFFFFFFFFFFFFFFFFFFFFFFFF4FBF560BD696ABA7153B35C089D
                160B941672A077ECF1EDFFFFFFFFFFFFE5E5E5DBDBDBFFFFFFFFFFFFFFFFFFFF
                FFFFF7F7F78787878B8B8B7B7B7B464646444444858585EEEEEEFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6FBF65FBC6670B07754A0
                5C0A80131A7722BED0C0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFF8F8F88686868B8B8B7474743B3B3B414141C6C6C6FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7FCF762BF697AB1
                7F579E5F9AC09EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFF9F9F9898989919191757575AAAAAAFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3FAF36CBF
                72A3D0A7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFF6F6F68E8E8EB6B6B6FFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
              NumGlyphs = 2
              ParentFont = False
              TabOrder = 3
              OnClick = btnPagarClick
            end
            object edtFormaPag: TEdit
              Left = 0
              Top = 13
              Width = 63
              Height = 30
              Ctl3D = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -19
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 0
              OnChange = edtFormaPagChange
              OnEnter = edtFormaPagEnter
              OnExit = edtFormaPagExit
              OnKeyPress = edtFormaPagKeyPress
            end
            object edtPagValor: TRealEdit
              Left = 299
              Top = 13
              Width = 111
              Height = 30
              Alignment = taRightJustify
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -19
              Font.Name = 'Arial'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 2
              FormatReal = fNumber
              FormatSize = '10.2'
              OnExit = edtPagValorExit
            end
          end
          object pnlAcrescimo: TPanel
            Left = 7
            Top = 64
            Width = 492
            Height = 32
            Alignment = taLeftJustify
            Caption = 'Acr'#233'scimos R$'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -24
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentBackground = False
            ParentFont = False
            TabOrder = 2
            object lblAcrescPerc: TLabel
              Left = 191
              Top = 11
              Width = 35
              Height = 16
              Caption = '0,00%'
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Arial'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object edtVlrAcrescimos: TDBEdit
              Left = 357
              Top = 1
              Width = 134
              Height = 30
              Align = alRight
              Ctl3D = False
              DataField = 'VLR_OUTRO'
              DataSource = dsTempECF
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -19
              Font.Name = 'Arial'
              Font.Style = [fsBold]
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 0
              OnEnter = edtVlrAcrescimosEnter
              OnExit = edtVlrAcrescimosExit
              ExplicitHeight = 28
            end
          end
        end
        object tabOpcoes: TTabSheet
          Caption = 'F8 - Op'#231#245'es'
          ImageIndex = 1
          object Label21: TLabel
            Left = 14
            Top = 424
            Width = 152
            Height = 13
            Caption = 'Informa'#231#245'es Adicionais ao Fisco'
          end
          object Label22: TLabel
            Left = 14
            Top = 351
            Width = 139
            Height = 13
            Caption = 'Informa'#231#245'es Complementares'
          end
          object Label14: TLabel
            Left = 14
            Top = 167
            Width = 35
            Height = 13
            Caption = 'Modelo'
            FocusControl = edtModelo
          end
          object Label15: TLabel
            Left = 70
            Top = 167
            Width = 24
            Height = 13
            Caption = 'S'#233'rie'
            Enabled = False
            FocusControl = edtSerie
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlue
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsUnderline]
            ParentFont = False
            OnClick = Label15Click
          end
          object Label17: TLabel
            Left = 163
            Top = 167
            Width = 23
            Height = 13
            Caption = 'Data'
            FocusControl = edtData
          end
          object Label18: TLabel
            Left = 267
            Top = 167
            Width = 23
            Height = 13
            Caption = 'Hora'
            FocusControl = edtHora
          end
          object Label1: TLabel
            Left = 13
            Top = 202
            Width = 47
            Height = 13
            Caption = 'Opera'#231#227'o'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlue
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsUnderline]
            ParentFont = False
            OnClick = Label1Click
          end
          object Label27: TLabel
            Left = 13
            Top = 240
            Width = 76
            Height = 13
            Caption = 'Centro de Custo'
            Enabled = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlue
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsUnderline]
            ParentFont = False
            OnClick = Label27Click
          end
          object Label3: TLabel
            Left = 13
            Top = 276
            Width = 46
            Height = 13
            Caption = 'Vendedor'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlue
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsUnderline]
            ParentFont = False
            OnClick = Label3Click
          end
          object Label4: TLabel
            Left = 13
            Top = 311
            Width = 45
            Height = 13
            Caption = 'Conv'#234'nio'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlue
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsUnderline]
            ParentFont = False
            OnClick = Label4Click
          end
          object rgAmbiente: TDBRadioGroup
            Left = 14
            Top = 0
            Width = 222
            Height = 33
            Caption = '| Ambiente |'
            Columns = 2
            DataField = 'TPAMB'
            DataSource = dsTempECF
            Enabled = False
            Items.Strings = (
              '1 - Produ'#231#227'o'
              '2 - Homologa'#231#227'o')
            TabOrder = 10
            Values.Strings = (
              '1'
              '2')
          end
          object rgIndicadorPresenca: TDBRadioGroup
            Left = 14
            Top = 38
            Width = 473
            Height = 64
            Caption = '| Indicador de presen'#231'a do consumidor |'
            Columns = 2
            DataField = 'INDPRES'
            DataSource = dsTempECF
            Items.Strings = (
              '1 - Presencial'
              '2 - N'#227'o presencial, pela Internet'
              '3 - N'#227'o presencial, Teleatendimento'
              '4 - NFC-e com entrega a domic'#237'lio'
              '9 - N'#227'o presencial, outros')
            TabOrder = 11
            Values.Strings = (
              '1'
              '2'
              '3'
              '4'
              '9')
          end
          object rgModalidadeFrete: TDBRadioGroup
            Left = 13
            Top = 108
            Width = 474
            Height = 55
            Caption = '| Modalidade do Frete |'
            Columns = 2
            DataField = 'MODFRETE'
            DataSource = dsTempECF
            Items.Strings = (
              '0 - Por conta do emitente'
              '1 - Por conta do destinat'#225'rio/remetente'
              '2 - Por conta de terceiros'
              '9 - Sem frete')
            TabOrder = 12
            Values.Strings = (
              '0'
              '1'
              '2'
              '9')
          end
          object memInfCpl: TDBMemo
            Left = 14
            Top = 365
            Width = 473
            Height = 54
            Ctl3D = False
            DataField = 'INFCPL'
            DataSource = dsTempECF
            ParentCtl3D = False
            TabOrder = 8
          end
          object memInfAdFisco: TDBMemo
            Left = 14
            Top = 439
            Width = 473
            Height = 54
            Ctl3D = False
            DataField = 'INFADFISCO'
            DataSource = dsTempECF
            ParentCtl3D = False
            TabOrder = 9
          end
          object edtModelo: TDBEdit
            Left = 14
            Top = 182
            Width = 49
            Height = 19
            Ctl3D = False
            DataField = 'MODELO_DOC'
            DataSource = dsTempECF
            Enabled = False
            ParentCtl3D = False
            TabOrder = 0
          end
          object edtSerie: TDBEdit
            Left = 70
            Top = 182
            Width = 57
            Height = 19
            Ctl3D = False
            DataField = 'SERIE'
            DataSource = dsTempECF
            Enabled = False
            ParentCtl3D = False
            TabOrder = 1
            OnKeyPress = edtSerieKeyPress
          end
          object edtData: TDBEdit
            Left = 163
            Top = 182
            Width = 97
            Height = 19
            Ctl3D = False
            DataField = 'DATA'
            DataSource = dsTempECF
            Enabled = False
            ParentCtl3D = False
            TabOrder = 2
          end
          object edtHora: TDBEdit
            Left = 267
            Top = 182
            Width = 73
            Height = 19
            Ctl3D = False
            DataField = 'HORA'
            DataSource = dsTempECF
            Enabled = False
            ParentCtl3D = False
            TabOrder = 3
          end
          object rgIndicadorPag: TDBRadioGroup
            Left = 240
            Top = 0
            Width = 247
            Height = 33
            Caption = '| Ind. de Pag. |'
            Columns = 3
            DataField = 'INDPAG'
            DataSource = dsTempECF
            Items.Strings = (
              '0 - A Vista'
              '1 - A Prazo'
              '2 -  Outros')
            TabOrder = 13
            Values.Strings = (
              '0'
              '1'
              '2')
          end
          object edtOperacao: TDBEdit
            Left = 13
            Top = 218
            Width = 45
            Height = 19
            Ctl3D = False
            DataField = 'OPERACAO'
            DataSource = dsTempECF
            ParentCtl3D = False
            TabOrder = 4
            OnExit = edtOperacaoExit
          end
          object edtOperacaoDesc: TDBEdit
            Left = 57
            Top = 218
            Width = 285
            Height = 19
            TabStop = False
            Color = clBtnFace
            Ctl3D = False
            DataField = 'OPERACAO_DESC'
            DataSource = dsTempECF
            ParentCtl3D = False
            ReadOnly = True
            TabOrder = 14
          end
          object edtCCusto: TDBEdit
            Left = 13
            Top = 255
            Width = 45
            Height = 19
            Ctl3D = False
            DataField = 'CCUSTO'
            DataSource = dsTempECF
            Enabled = False
            ParentCtl3D = False
            TabOrder = 5
            OnExit = edtCCustoExit
          end
          object edtCCustoDescricao: TDBEdit
            Left = 57
            Top = 255
            Width = 285
            Height = 19
            TabStop = False
            Color = clBtnFace
            Ctl3D = False
            DataField = 'CCUSTO_DESC'
            DataSource = dsTempECF
            ParentCtl3D = False
            ReadOnly = True
            TabOrder = 15
          end
          object edtFuncionario: TDBEdit
            Left = 13
            Top = 291
            Width = 45
            Height = 19
            Ctl3D = False
            DataField = 'FUNCIONARIO'
            DataSource = dsTempECF
            ParentCtl3D = False
            TabOrder = 6
            OnExit = edtFuncionarioExit
          end
          object edtFuncionarioNome: TDBEdit
            Left = 57
            Top = 291
            Width = 285
            Height = 19
            TabStop = False
            Color = clBtnFace
            Ctl3D = False
            DataField = 'FUNCIONARIO_NOME'
            DataSource = dsTempECF
            ParentCtl3D = False
            ReadOnly = True
            TabOrder = 16
          end
          object chkNFe: TCheckBox
            Left = 348
            Top = 183
            Width = 139
            Height = 17
            TabStop = False
            Alignment = taLeftJustify
            Caption = 'Gerar NF  Vinculada ?'
            TabOrder = 17
          end
          object edtConvenio: TDBEdit
            Left = 13
            Top = 326
            Width = 45
            Height = 19
            Ctl3D = False
            DataField = 'CONVENIO'
            DataSource = dsTempECF
            ParentCtl3D = False
            TabOrder = 7
            OnExit = edtConvenioExit
          end
          object edtConvenioDesc: TEdit
            Left = 57
            Top = 326
            Width = 285
            Height = 19
            Enabled = False
            TabOrder = 18
          end
        end
      end
    end
  end
  object pnlCabecalho: TPanel
    Left = 0
    Top = 0
    Width = 1275
    Height = 42
    Align = alTop
    BevelOuter = bvNone
    Caption = 'Pagamento'
    Color = clHotLight
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -35
    Font.Name = 'Arial'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
  end
  object pnlRodape: TPanel
    Left = 0
    Top = 621
    Width = 1275
    Height = 24
    Align = alBottom
    Color = clHotLight
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentBackground = False
    ParentFont = False
    TabOrder = 2
    DesignSize = (
      1275
      24)
    object Label7: TLabel
      Left = 5
      Top = 3
      Width = 49
      Height = 19
      Caption = 'Caixa:'
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -16
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      Transparent = True
    end
    object Label8: TLabel
      Left = 1140
      Top = 3
      Width = 41
      Height = 19
      Anchors = [akRight, akBottom]
      Caption = 'Data:'
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -16
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      Transparent = True
      ExplicitLeft = 741
    end
    object lblNCaixa: TDBText
      Left = 63
      Top = 4
      Width = 65
      Height = 17
      DataField = 'CAIXA'
      DataSource = dsTempECF
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -16
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblData: TDBText
      Left = 1188
      Top = 4
      Width = 84
      Height = 17
      Anchors = [akRight, akBottom]
      DataField = 'DATA'
      DataSource = dsTempECF
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -16
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      ExplicitLeft = 1143
    end
  end
  object pnlFinalizar: TPanel
    Left = 0
    Top = 580
    Width = 1275
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    Color = clHotLight
    ParentBackground = False
    TabOrder = 3
    object btnSalvar: TBitBtn
      Left = 641
      Top = 9
      Width = 53
      Height = 26
      Caption = 'Salvar'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      NumGlyphs = 2
      ParentFont = False
      TabOrder = 0
      Visible = False
      OnClick = btnSalvarClick
    end
    object pnlFinalizarOrganiza: TPanel
      Left = 46
      Top = 2
      Width = 445
      Height = 36
      BevelOuter = bvNone
      ParentBackground = False
      ParentColor = True
      TabOrder = 1
      object btnFinalizar: TSpeedButton
        Left = 0
        Top = -1
        Width = 88
        Height = 37
        Hint = 'Pressione F1 ou clique aqui para Finalizar o Cupom Fiscal'
        Caption = 'Finalizar'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Glyph.Data = {
          B6040000424DB604000000000000360000002800000018000000100000000100
          18000000000080040000120B0000120B00000000000000000000E1E1E1979696
          91908F9291908A898881807F918F8F9291909291909291909291909291909291
          9092919091908F8483828281808A898893919173717068666591908F979696E1
          E1E1A5A4A4E8E6E6E0DFDEDFDEDEDFDEDEE1E0E0DFDEDEDFDEDEDFDEDEDFDEDE
          DFDEDEDFDEDEDFDEDEDFDEDEDFDEDEDFDEDEDFDEDEDFDEDEDFDEDEDFDEDEDFDE
          DEE0DFDEE7E6E6A4A4A3A3A2A2F6F6F7D7D6D6C8C8C6C9C9C8CCCCCBC9C9C8C9
          C9C8C9C9C8C9C9C8C9C9C8C9C9C8C9C9C8C9C9C8C9C9C8C9C9C8C9C9C8C9C9C8
          C9C9C8C9C9C8C8C8C6D7D6D6F6F6F6A2A1A0A1A0A0EEEEEEF2F3F3FBFBFBFAFA
          FAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFA
          FAFAFAFAFAFAFAFAFAFAFAFAFAFAFBFBFBF2F2F2EFEEEEA1A0A0A2A1A0EAEAEA
          F3F3F3FFFFFFFDFDFDFDFDFDC1C2C2868787FDFDFDFDFDFDFDFDFDFDFDFDFDFD
          FDFDFDFDDFDFDF868787DFDFDFFDFDFDFDFDFDFDFDFDFFFFFFF1F0F0EBEAE9A2
          A1A0A2A2A1E7E7E6F1F1F1FBFBFBF8F8F8F8F8F88384840F1111F9F9F9F9F9F9
          F9F9F9F9F9F9F8F8F8F8F8F8BEBFBF0F1111BEBEBEF9F8F8F9F9F9F9F9F9FCFC
          FCEFEEEEE7E6E6A2A2A1A3A2A2E5E4E3EFEEEDF8F8F7F5F4F3F7F6F78283830F
          1111F5F5F4F7F6F6F6F6F6F6F6F6F6F6F5F5F5F4BBBCBB0F1111BCBCBCF4F4F4
          F6F5F5F7F6F6FAF9F9ECECECE3E3E2A3A2A2A3A3A2E1E1E0EBEBEBF5F5F4F2F1
          F1F6F5F58282820F1111BBBABABBBBBBBBBABAD8D8D8F5F4F4F4F4F4BBBBBB0F
          1111BBBBBBF4F3F3F4F4F3F4F2F2F7F7F6EAEAEAE0E0DFA3A3A2A4A3A3DDDDDD
          E9E8E8F5F4F4F4F4F4F6F6F58282820F11110F11110F11110F1111818282F5F5
          F4F5F4F3BBBBBB0F1111BBBBBBF4F4F3F5F4F4F2F1F1F5F4F4E8E8E8DCDDDCA4
          A3A3A4A3A3DCDBDAEAE9E9F6F5F5F4F4F3F5F5F48182820F1111F4F3F3F4F4F3
          F2F2F1F3F3F2F5F5F42B2C2C5657570F1111BBBBBAF3F3F2F4F3F3F0EFEFF2F1
          F1E6E6E5DAD9D8A4A4A3A5A4A3D9D8D7E8E8E7F5F5F4F4F3F2F5F5F48182820F
          1111B9B9B9BABAB9B8B8B8C6C8C7F4F4F47273730F11110F1111BABABAF2F2F1
          F2F1F1EEEEEDEFEFEEE4E3E3D7D5D5A5A4A4A5A5A4D5D3D3E4E4E3F2F2F1EFEF
          EEF3F3F28081810F11110F11110F11110F11110F1111F2F2F2F0EFEE9B9B9A0F
          1111B8B8B8EFEFEEEEEDEDEAE9E8ECECEBE2E2E1D3D2D2A5A5A4A6A5A5CFCECD
          E1E0E0EEEEEDEDECEBEEEDEDE0DFDFB4B4B3B2B3B2B3B3B2B1B1B0C2C1C0EEEE
          EDEBEAE9EAE9E8C4C4C4DEDEDDEAEAE9E9E8E7E7E6E5ECEBEADFDFDECFCECDA6
          A5A5A6A6A5D5D4D4ECEBEBF5F5F5F2F2F2F2F1F1EFEFEFEFEFEFF1F0F0F3F3F3
          F4F4F4F1F1F2EFEFEFEFEFEFF0F0F0F0F0EFEFEFEFF0F0EFF2F1F1F4F4F4F6F6
          F6ECEBEBD5D4D4A6A6A5AFAFAEE9E9E8F0F0F0EBEBEBECEBEBEDEDEDECECECEC
          ECECECEBEBEBEBEBEBEBEBECECEBECECECECECECECEBEBECECECECECECECEBEB
          ECEBEBEBEBEBEBEBEBF0F0F0E9E9E8AFAFAEE1E1E1CECDCCD2D1D0D1D0CECDCD
          CBCDCCCAD1D0CED1D0CED1D0CED1D0CED1D0CED1D0CED1D0CED1D0CED1D0CED1
          D0CED1D0CED1D0CED1D0CEC8C7C5C5C4C2D2D1D0CECDCCE1E1E1}
        Layout = blGlyphTop
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        Spacing = 2
        OnClick = btnFinalizarClick
      end
      object btnDesconto: TSpeedButton
        Left = 89
        Top = -1
        Width = 88
        Height = 37
        Hint = 'Pressione F2 ou clique aqui para Cancelar o '#250'ltimo Cupom Fiscal'
        Caption = 'Desconto'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Glyph.Data = {
          B6040000424DB604000000000000360000002800000018000000100000000100
          18000000000080040000120B0000120B00000000000000000000E1E1E1979696
          91908F9291908A898881807F918F8F9291909291909291909291909291909291
          9092919091908F8483828281808A898893919173717068666591908F979696E1
          E1E1A5A4A4E8E6E6E0DFDEDFDEDEDFDEDEE1E0E0DFDEDEDFDEDEDFDEDEDFDEDE
          DFDEDEDFDEDEDFDEDEDFDEDEDFDEDEDFDEDEDFDEDEDFDEDEDFDEDEDFDEDEDFDE
          DEE0DFDEE7E6E6A4A4A3A3A2A2F6F6F7D7D6D6C8C8C6C9C9C8CCCCCBC9C9C8C9
          C9C8C9C9C8C9C9C8C9C9C8C9C9C8C9C9C8C9C9C8C9C9C8C9C9C8C9C9C8C9C9C8
          C9C9C8C9C9C8C8C8C6D7D6D6F6F6F6A2A1A0A1A0A0EEEEEEF2F3F3FBFBFBFAFA
          FAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFA
          FAFAFAFAFAFAFAFAFAFAFAFAFAFAFBFBFBF2F2F2EFEEEEA1A0A0A2A1A0EAEAEA
          F3F3F3FFFFFFFDFDFDFDFDFDC1C2C2868787FDFDFDFDFDFDFDFDFDFDFDFDA3A4
          A4868787868787868787868787C1C2C2FDFDFDFDFDFDFFFFFFF1F0F0EBEAE9A2
          A1A0A2A2A1E7E7E6F1F1F1FBFBFBF8F8F8F8F8F88384840F1111F9F9F9F9F9F9
          F9F9F9F9F9F98384840F11112B2D2D494A4A494A4AA1A1A1F9F9F9F9F9F9FCFC
          FCEFEEEEE7E6E6A2A2A1A3A2A2E5E4E3EFEEEDF8F8F7F5F4F3F7F6F78283830F
          1111F5F5F4F7F6F6F6F6F6F6F6F6E8E8E7393B3B1D1E1ECBCBCBF6F5F5F4F4F4
          F6F5F5F7F6F6FAF9F9ECECECE3E3E2A3A2A2A3A3A2E1E1E0EBEBEBF5F5F4F2F1
          F1F6F5F58282820F1111BBBABABBBBBBBBBABAD8D8D8F5F4F4E6E6E6393B3B1D
          1E1EC9C9C9F4F3F3F4F4F3F4F2F2F7F7F6EAEAEAE0E0DFA3A3A2A4A3A3DDDDDD
          E9E8E8F5F4F4F4F4F4F6F6F58282820F11110F11110F11110F1111818282F5F5
          F4F5F4F3E7E6E63A3B3B1C1E1ED7D8D7F5F4F4F2F1F1F5F4F4E8E8E8DCDDDCA4
          A3A3A4A3A3DCDBDAEAE9E9F6F5F5F4F4F3F5F5F48182820F1111F4F3F3F4F4F3
          F2F2F1F3F3F2BBBCBB818281F4F3F2D7D8D80F1111818281F4F3F3F0EFEFF2F1
          F1E6E6E5DAD9D8A4A4A3A5A4A3D9D8D7E8E8E7F5F5F4F4F3F2F5F5F48182820F
          1111B9B9B9BABAB9B8B8B8C6C8C7BBBBBB0F11119D9D9DBBBBBB0F11119D9D9D
          F2F1F1EEEEEDEFEFEEE4E3E3D7D5D5A5A4A4A5A5A4D5D3D3E4E4E3F2F2F1EFEF
          EEF3F3F28081810F11110F11110F11110F11110F1111F2F2F25556550F11110F
          1111555656EFEFEEEEEDEDEAE9E8ECECEBE2E2E1D3D2D2A5A5A4A6A5A5CFCECD
          E1E0E0EEEEEDEDECEBEEEDEDE0DFDFB4B4B3B2B3B2B3B3B2B1B1B0C2C1C0EEEE
          EDEBEAE9DCDBDBD2D2D1ECECEBEAEAE9E9E8E7E7E6E5ECEBEADFDFDECFCECDA6
          A5A5A6A6A5D5D4D4ECEBEBF5F5F5F2F2F2F2F1F1EFEFEFEFEFEFF1F0F0F3F3F3
          F4F4F4F1F1F2EFEFEFEFEFEFF0F0F0F0F0EFEFEFEFF0F0EFF2F1F1F4F4F4F6F6
          F6ECEBEBD5D4D4A6A6A5AFAFAEE9E9E8F0F0F0EBEBEBECEBEBEDEDEDECECECEC
          ECECECEBEBEBEBEBEBEBEBECECEBECECECECECECECEBEBECECECECECECECEBEB
          ECEBEBEBEBEBEBEBEBF0F0F0E9E9E8AFAFAEE1E1E1CECDCCD2D1D0D1D0CECDCD
          CBCDCCCAD1D0CED1D0CED1D0CED1D0CED1D0CED1D0CED1D0CED1D0CED1D0CED1
          D0CED1D0CED1D0CED1D0CEC8C7C5C5C4C2D2D1D0CECDCCE1E1E1}
        Layout = blGlyphTop
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        Spacing = 2
      end
      object btnFormaPag: TSpeedButton
        Left = 179
        Top = -1
        Width = 88
        Height = 37
        Hint = 
          'Pressione F3 ou clique aqui para Cancelar um determinado item do' +
          ' Cupom Fiscal'
        Caption = 'Forma Pag.'
        Glyph.Data = {
          B6040000424DB604000000000000360000002800000018000000100000000100
          18000000000080040000120B0000120B00000000000000000000E1E1E1979696
          91908F9291908A898881807F918F8F9291909291909291909291909291909291
          9092919091908F8483828281808A898893919173717068666591908F979696E1
          E1E1A5A4A4E8E6E6E0DFDEDFDEDEDFDEDEE1E0E0DFDEDEDFDEDEDFDEDEDFDEDE
          DFDEDEDFDEDEDFDEDEDFDEDEDFDEDEDFDEDEDFDEDEDFDEDEDFDEDEDFDEDEDFDE
          DEE0DFDEE7E6E6A4A4A3A3A2A2F6F6F7D7D6D6C8C8C6C9C9C8CCCCCBC9C9C8C9
          C9C8C9C9C8C9C9C8C9C9C8C9C9C8C9C9C8C9C9C8C9C9C8C9C9C8C9C9C8C9C9C8
          C9C9C8C9C9C8C8C8C6D7D6D6F6F6F6A2A1A0A1A0A0EEEEEEF2F3F3FBFBFBFAFA
          FAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFA
          FAFAFAFAFAFAFAFAFAFAFAFAFAFAFBFBFBF2F2F2EFEEEEA1A0A0A2A1A0EAEAEA
          F3F3F3FFFFFFFDFDFDFDFDFDC1C2C2868787FDFDFDFDFDFDFDFDFDFDFDFDFDFD
          FDDFDFDF777878868787DFDFDFFDFDFDFDFDFDFDFDFDFFFFFFF1F0F0EBEAE9A2
          A1A0A2A2A1E7E7E6F1F1F1FBFBFBF8F8F8F8F8F88384840F1111F9F9F9F9F9F9
          F9F9F9F9F9F9DBDBDB0F11112B2D2D2B2D2D0F1111DCDBDBF9F9F9F9F9F9FCFC
          FCEFEEEEE7E6E6A2A2A1A3A2A2E5E4E3EFEEEDF8F8F7F5F4F3F7F6F78283830F
          1111F5F5F4F7F6F6F6F6F6F6F6F68283831D1E1EE7E7E6E8E8E81D1E1E737474
          F6F5F5F7F6F6FAF9F9ECECECE3E3E2A3A2A2A3A3A2E1E1E0EBEBEBF5F5F4F2F1
          F1F6F5F58282820F1111BBBABABBBBBBBBBABAD8D8D8F5F4F4F4F4F4F4F4F4BB
          BBBB0F11118F9090F4F4F3F4F2F2F7F7F6EAEAEAE0E0DFA3A3A2A4A3A3DDDDDD
          E9E8E8F5F4F4F4F4F4F6F6F58282820F11110F11110F11110F1111818282F5F5
          F4F5F4F34849490F11112B2D2DE6E6E5F5F4F4F2F1F1F5F4F4E8E8E8DCDDDCA4
          A3A3A4A3A3DCDBDAEAE9E9F6F5F5F4F4F3F5F5F48182820F1111F4F3F3F4F4F3
          F2F2F1F3F3F2D8D8D8BABABAC9C8C86466660F1111D6D7D6F4F3F3F0EFEFF2F1
          F1E6E6E5DAD9D8A4A4A3A5A4A3D9D8D7E8E8E7F5F5F4F4F3F2F5F5F48182820F
          1111B9B9B9BABAB9B8B8B8C6C8C79E9F9F0F1111ABABAB8182820F1111C7C8C7
          F2F1F1EEEEEDEFEFEEE4E3E3D7D5D5A5A4A4A5A5A4D5D3D3E4E4E3F2F2F1EFEF
          EEF3F3F28081810F11110F11110F11110F11110F1111F2F2F26364630F11110F
          1111636464EFEFEEEEEDEDEAE9E8ECECEBE2E2E1D3D2D2A5A5A4A6A5A5CFCECD
          E1E0E0EEEEEDEDECEBEEEDEDE0DFDFB4B4B3B2B3B2B3B3B2B1B1B0C2C1C0EEEE
          EDEBEAE9CFCECDE0E0DFECECEBEAEAE9E9E8E7E7E6E5ECEBEADFDFDECFCECDA6
          A5A5A6A6A5D5D4D4ECEBEBF5F5F5F2F2F2F2F1F1EFEFEFEFEFEFF1F0F0F3F3F3
          F4F4F4F1F1F2EFEFEFEFEFEFF0F0F0F0F0EFEFEFEFF0F0EFF2F1F1F4F4F4F6F6
          F6ECEBEBD5D4D4A6A6A5AFAFAEE9E9E8F0F0F0EBEBEBECEBEBEDEDEDECECECEC
          ECECECEBEBEBEBEBEBEBEBECECEBECECECECECECECEBEBECECECECECECECEBEB
          ECEBEBEBEBEBEBEBEBF0F0F0E9E9E8AFAFAEE1E1E1CECDCCD2D1D0D1D0CECDCD
          CBCDCCCAD1D0CED1D0CED1D0CED1D0CED1D0CED1D0CED1D0CED1D0CED1D0CED1
          D0CED1D0CED1D0CED1D0CEC8C7C5C5C4C2D2D1D0CECDCCE1E1E1}
        Layout = blGlyphTop
        ParentShowHint = False
        ShowHint = True
        Spacing = 2
      end
      object btnConsumidor: TSpeedButton
        Left = 268
        Top = -1
        Width = 88
        Height = 37
        Caption = 'Consumidor'
        Glyph.Data = {
          42060000424D4206000000000000420000002800000018000000100000000100
          20000300000000060000232E0000232E000000000000000000000000FF0000FF
          0000FF000000E1E1E1FF979696FF91908FFF929190FF8A8988FF81807FFF918F
          8FFF929190FF929190FF929190FF929190FF929190FF929190FF929190FF9190
          8FFF848382FF828180FF8A8988FF939191FF737170FF686665FF91908FFF9796
          96FFE1E1E1FFA5A4A4FFE8E6E6FFE0DFDEFFDFDEDEFFDFDEDEFFE1E0E0FFDFDE
          DEFFDFDEDEFFDFDEDEFFDFDEDEFFDFDEDEFFDFDEDEFFDFDEDEFFDFDEDEFFDFDE
          DEFFDFDEDEFFDFDEDEFFDFDEDEFFDFDEDEFFDFDEDEFFDFDEDEFFE0DFDEFFE7E6
          E6FFA4A4A3FFA3A2A2FFF6F6F7FFD7D6D6FFC8C8C6FFC9C9C8FFCCCCCBFFC9C9
          C8FFC9C9C8FFC9C9C8FFC9C9C8FFC9C9C8FFC9C9C8FFC9C9C8FFC9C9C8FFC9C9
          C8FFC9C9C8FFC9C9C8FFC9C9C8FFC9C9C8FFC9C9C8FFC8C8C6FFD7D6D6FFF6F6
          F6FFA2A1A0FFA1A0A0FFEEEEEEFFF2F3F3FFFBFBFBFFFAFAFAFFFAFAFAFFFAFA
          FAFFFAFAFAFFFAFAFAFFFAFAFAFFFAFAFAFFFAFAFAFFFAFAFAFFFAFAFAFFFAFA
          FAFFFAFAFAFFFAFAFAFFFAFAFAFFFAFAFAFFFAFAFAFFFBFBFBFFF2F2F2FFEFEE
          EEFFA1A0A0FFA2A1A0FFEAEAEAFFF3F3F3FFFFFFFFFFFDFDFDFFFDFDFDFFC1C2
          C2FF868787FFFDFDFDFFFDFDFDFFFDFDFDFFFDFDFDFFEBEBEAFFECECEBFFD2D2
          D1FFEAE9E8FFEBEAE9FFEEEEEDFFFDFDFDFFFDFDFDFFFFFFFFFFF1F0F0FFEBEA
          E9FFA2A1A0FFA2A2A1FFE7E7E6FFF1F1F1FFFBFBFBFFF8F8F8FFF8F8F8FF8384
          84FF0F1111FFF9F9F9FFF9F9F9FFF9F9F9FFF9F9F9FFEFEFEEFF474848FF0F11
          11FF2A2C2CFF7F807FFFF2F2F2FFF9F9F9FFF9F9F9FFFCFCFCFFEFEEEEFFE7E6
          E6FFA2A2A1FFA3A2A2FFE5E4E3FFEFEEEDFFF8F8F7FFF5F4F3FFF7F6F7FF8283
          83FF0F1111FFF5F5F4FFF7F6F6FFF6F6F6FFF6F6F6FF9D9D9DFF0F1111FFBBBB
          BBFF808181FF0F1111FFBBBBBBFFF6F5F5FFF7F6F6FFFAF9F9FFECECECFFE3E3
          E2FFA3A2A2FFA3A3A2FFE1E1E0FFEBEBEBFFF5F5F4FFF2F1F1FFF6F5F5FF8282
          82FF0F1111FFBBBABAFFBBBBBBFFBBBABAFFD8D8D8FFF2F2F1FFACADACFF6466
          66FF818281FF0F1111FF828382FFF4F4F3FFF4F2F2FFF7F7F6FFEAEAEAFFE0E0
          DFFFA3A3A2FFA4A3A3FFDDDDDDFFE9E8E8FFF5F4F4FFF4F4F4FFF6F6F5FF8282
          82FF0F1111FF0F1111FF0F1111FF0F1111FF818282FFD6D7D6FF0F1111FF2B2D
          2DFF1D1E1EFF0F1111FF828382FFF5F4F4FFF2F1F1FFF5F4F4FFE8E8E8FFDCDD
          DCFFA4A3A3FFA4A3A3FFDCDBDAFFEAE9E9FFF6F5F5FFF4F4F3FFF5F5F4FF8182
          82FF0F1111FFF4F3F3FFF4F4F3FFF2F2F1FFF3F3F2FF737373FF1C1E1EFFF5F4
          F4FFD7D8D8FF0F1111FF828282FFF4F3F3FFF0EFEFFFF2F1F1FFE6E6E5FFDAD9
          D8FFA4A4A3FFA5A4A3FFD9D8D7FFE8E8E7FFF5F5F4FFF4F3F2FFF5F5F4FF8182
          82FF0F1111FFB9B9B9FFBABAB9FFB8B8B8FFC6C8C7FF828383FF1D1E1EFFF6F6
          F6FFE7E7E6FF0F1111FF919191FFF2F1F1FFEEEEEDFFEFEFEEFFE4E3E3FFD7D5
          D5FFA5A4A4FFA5A5A4FFD5D3D3FFE4E4E3FFF2F2F1FFEFEFEEFFF3F3F2FF8081
          81FF0F1111FF0F1111FF0F1111FF0F1111FF0F1111FFCECECEFF1D1F1FFF2B2D
          2DFF2B2D2DFF1D1F1FFFDBDBDBFFEEEDEDFFEAE9E8FFECECEBFFE2E2E1FFD3D2
          D2FFA5A5A4FFA6A5A5FFCFCECDFFE1E0E0FFEEEEEDFFEDECEBFFEEEDEDFFE0DF
          DFFFB4B4B3FFB2B3B2FFB3B3B2FFB1B1B0FFC2C1C0FFFDFDFDFFC1C2C2FF7778
          78FFA3A4A4FFFDFDFDFFFDFDFDFFE9E8E7FFE7E6E5FFECEBEAFFDFDFDEFFCFCE
          CDFFA6A5A5FFA6A6A5FFD5D4D4FFECEBEBFFF5F5F5FFF2F2F2FFF2F1F1FFEFEF
          EFFFEFEFEFFFF1F0F0FFF3F3F3FFF4F4F4FFF1F1F2FFF9F9F9FFF9F9F9FFF9F9
          F9FFF9F9F9FFF9F9F9FFF9F9F9FFF2F1F1FFF4F4F4FFF6F6F6FFECEBEBFFD5D4
          D4FFA6A6A5FFAFAFAEFFE9E9E8FFF0F0F0FFEBEBEBFFECEBEBFFEDEDEDFFECEC
          ECFFECECECFFECEBEBFFEBEBEBFFEBEBEBFFECECEBFFECECECFFECECECFFECEB
          EBFFECECECFFECECECFFECEBEBFFECEBEBFFEBEBEBFFEBEBEBFFF0F0F0FFE9E9
          E8FFAFAFAEFFE1E1E1FFCECDCCFFD2D1D0FFD1D0CEFFCDCDCBFFCDCCCAFFD1D0
          CEFFD1D0CEFFD1D0CEFFD1D0CEFFD1D0CEFFD1D0CEFFD1D0CEFFD1D0CEFFD1D0
          CEFFD1D0CEFFD1D0CEFFD1D0CEFFD1D0CEFFC8C7C5FFC5C4C2FFD2D1D0FFCECD
          CCFFE1E1E1FF}
        Layout = blGlyphTop
        ParentShowHint = False
        ShowHint = True
        Spacing = 2
        OnClick = btnConsumidorClick
      end
      object btnCancelar: TSpeedButton
        Left = 357
        Top = -1
        Width = 88
        Height = 37
        Hint = 'Pressione Esc ou clique aqui para fechar o Frente de Caixa'
        Caption = 'Cancelar'
        Glyph.Data = {
          B6040000424DB604000000000000360000002800000018000000100000000100
          18000000000080040000120B0000120B00000000000000000000E1E1E1979696
          91908F9291908A898881807F918F8F9291909291909291909291909291909291
          9092919091908F8483828281808A898893919173717068666591908F979696E1
          E1E1A5A4A4E8E6E6E0DFDEDFDEDEDFDEDEE1E0E0DFDEDEDFDEDEDFDEDEDFDEDE
          DFDEDEDFDEDEDFDEDEDFDEDEDFDEDEDFDEDEDFDEDEDFDEDEDFDEDEDFDEDEDFDE
          DEE0DFDEE7E6E6A4A4A3A3A2A2F6F6F7D7D6D6C8C8C6C9C9C8CCCCCBC9C9C8C9
          C9C8C9C9C8C9C9C8C9C9C8C9C9C8C9C9C8C9C9C8C9C9C8C9C9C8C9C9C8C9C9C8
          C9C9C8C9C9C8C8C8C6D7D6D6F6F6F6A2A1A0A1A0A0EEEEEEF2F3F3FBFBFBFAFA
          FAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFA
          FAFAFAFAFAFAFAFAFAFAFAFAFAFAFBFBFBF2F2F2EFEEEEA1A0A0A2A1A0EAEAEA
          F3F3F3C3C3C3868787868787868787868787868787C1C2C2DFDFDF9495957778
          78949595FDFDFDFDFDFDDFDFDF686969A3A4A4EEEEEEFFFFFFF1F0F0EBEAE9A2
          A1A0A2A2A1E7E7E6F1F1F18586860F1111494A4A494A4A494A4A494A4AA1A2A2
          494A4A1D1F1F494A4A0F1111848585AFB0B00F11113A3C3C1D1F1F494A4AFCFC
          FCEFEEEEE7E6E6A2A2A1A3A2A2E5E4E3EFEEED8384840F1111F7F6F7F6F6F5F5
          F5F4F5F5F4F7F6F66566669FA0A09FA09F1D1E1E5658576566663A3B3BF4F4F4
          9F9F9F2B2D2DDDDCDCECECECE3E3E2A3A2A2A3A3A2E1E1E0EBEBEB8283820F11
          11BCBBBBBBBBBBBBBBBBBBBABAF4F4F47373730F11110F11112B2D2DC9C9C948
          4949565757F4F3F3F4F4F3F4F2F2F7F7F6EAEAEAE0E0DFA3A3A2A4A3A3DDDDDD
          E9E8E88282820F11110F11110F11110F11110F1111F4F4F40F1111565757CACA
          C94849499E9F9F5757572B2D2DD7D8D78282821C1E1EF5F4F4E8E8E8DCDDDCA4
          A3A3A4A3A3DCDBDAEAE9E98283830F1111BBBBBBBBBBBBBABABAC9C8C8F4F4F3
          7273730F11110F11111C1E1EC9C8C8C9CAC91C1E1E0F11110F1111717272F2F1
          F1E6E6E5DAD9D8A4A4A3A5A4A3D9D8D7E8E8E78283820F1111BBBBBBBBBABAB9
          BAB9B9B9B9E5E4E3F1F0F0B8BAB9BBBBBBD6D6D5F2F1F1F4F4F4F3F3F2B9BAB9
          D6D5D5EEEEEDEFEFEEE4E3E3D7D5D5A5A4A4A5A5A4D5D3D3E4E4E38081810F11
          110F11110F11110F11110F1111B6B7B6EBEAEAEEEEEDF2F2F2F0EFEEEFEEEDF3
          F2F2F1F0F0EFEFEEEEEDEDEAE9E8ECECEBE2E2E1D3D2D2A5A5A4A6A5A5CFCECD
          E1E0E0D2D2D1B5B5B4B7B6B6B6B6B6B4B4B3B2B3B2DCDBDBE7E6E5EBEAE9EEEE
          EDEBEAE9EAE9E8EEEEEDECECEBEAEAE9E9E8E7E7E6E5ECEBEADFDFDECFCECDA6
          A5A5A6A6A5D5D4D4ECEBEBF5F5F5F2F2F2F2F1F1EFEFEFEFEFEFF1F0F0F3F3F3
          F4F4F4F1F1F2EFEFEFEFEFEFF0F0F0F0F0EFEFEFEFF0F0EFF2F1F1F4F4F4F6F6
          F6ECEBEBD5D4D4A6A6A5AFAFAEE9E9E8F0F0F0EBEBEBECEBEBEDEDEDECECECEC
          ECECECEBEBEBEBEBEBEBEBECECEBECECECECECECECEBEBECECECECECECECEBEB
          ECEBEBEBEBEBEBEBEBF0F0F0E9E9E8AFAFAEE1E1E1CECDCCD2D1D0D1D0CECDCD
          CBCDCCCAD1D0CED1D0CED1D0CED1D0CED1D0CED1D0CED1D0CED1D0CED1D0CED1
          D0CED1D0CED1D0CED1D0CEC8C7C5C5C4C2D2D1D0CECDCCE1E1E1}
        Layout = blGlyphTop
        ParentShowHint = False
        ShowHint = True
        Spacing = 2
        OnClick = btnCancelarClick
      end
    end
  end
  object pnlLegenda: TPanel
    Left = 1108
    Top = 42
    Width = 167
    Height = 538
    Align = alRight
    TabOrder = 4
    object GridPanel1: TGridPanel
      Left = 1
      Top = 1
      Width = 165
      Height = 536
      Align = alClient
      ColumnCollection = <
        item
          SizeStyle = ssAbsolute
          Value = 30.000000000000000000
        end
        item
          Value = 100.000000000000000000
        end>
      ControlCollection = <
        item
          Column = 0
          Control = Image1
          Row = 1
        end
        item
          Column = 0
          Control = Image2
          Row = 2
        end
        item
          Column = 0
          Control = Image3
          Row = 3
        end
        item
          Column = 0
          Control = Image4
          Row = 4
        end
        item
          Column = 0
          Control = Image5
          Row = 5
        end
        item
          Column = 0
          ColumnSpan = 2
          Control = Label6
          Row = 0
        end
        item
          Column = 1
          Control = Label9
          Row = 1
        end>
      RowCollection = <
        item
          SizeStyle = ssAbsolute
          Value = 30.000000000000000000
        end
        item
          SizeStyle = ssAbsolute
          Value = 30.000000000000000000
        end
        item
          SizeStyle = ssAbsolute
          Value = 30.000000000000000000
        end
        item
          SizeStyle = ssAbsolute
          Value = 30.000000000000000000
        end
        item
          SizeStyle = ssAbsolute
          Value = 30.000000000000000000
        end
        item
          SizeStyle = ssAbsolute
          Value = 30.000000000000000000
        end
        item
          Value = 100.000000000000000000
        end>
      TabOrder = 0
      Visible = False
      object Image1: TImage
        Left = 1
        Top = 31
        Width = 30
        Height = 30
        Align = alClient
        Center = True
        Picture.Data = {
          07544269746D6170B6040000424DB60400000000000036000000280000001800
          000010000000010018000000000080040000120B0000120B0000000000000000
          0000E1E1E197969691908F9291908A898881807F918F8F929190929190929190
          92919092919092919092919091908F8483828281808A89889391917371706866
          6591908F979696E1E1E1A5A4A4E8E6E6E0DFDEDFDEDEDFDEDEE1E0E0DFDEDEDF
          DEDEDFDEDEDFDEDEDFDEDEDFDEDEDFDEDEDFDEDEDFDEDEDFDEDEDFDEDEDFDEDE
          DFDEDEDFDEDEDFDEDEE0DFDEE7E6E6A4A4A3A3A2A2F6F6F7D7D6D6C8C8C6C9C9
          C8CCCCCBC9C9C8C9C9C8C9C9C8C9C9C8C9C9C8C9C9C8C9C9C8C9C9C8C9C9C8C9
          C9C8C9C9C8C9C9C8C9C9C8C9C9C8C8C8C6D7D6D6F6F6F6A2A1A0A1A0A0EEEEEE
          F2F3F3FBFBFBFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFA
          FAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFBFBFBF2F2F2EFEEEEA1
          A0A0A2A1A0EAEAEAF3F3F3FFFFFFFDFDFDFDFDFDC1C2C2868787FDFDFDFDFDFD
          FDFDFDFDFDFDFDFDFDFDFDFDDFDFDF868787DFDFDFFDFDFDFDFDFDFDFDFDFFFF
          FFF1F0F0EBEAE9A2A1A0A2A2A1E7E7E6F1F1F1FBFBFBF8F8F8F8F8F88384840F
          1111F9F9F9F9F9F9F9F9F9F9F9F9F8F8F8F8F8F8BEBFBF0F1111BEBEBEF9F8F8
          F9F9F9F9F9F9FCFCFCEFEEEEE7E6E6A2A2A1A3A2A2E5E4E3EFEEEDF8F8F7F5F4
          F3F7F6F78283830F1111F5F5F4F7F6F6F6F6F6F6F6F6F6F6F5F5F5F4BBBCBB0F
          1111BCBCBCF4F4F4F6F5F5F7F6F6FAF9F9ECECECE3E3E2A3A2A2A3A3A2E1E1E0
          EBEBEBF5F5F4F2F1F1F6F5F58282820F1111BBBABABBBBBBBBBABAD8D8D8F5F4
          F4F4F4F4BBBBBB0F1111BBBBBBF4F3F3F4F4F3F4F2F2F7F7F6EAEAEAE0E0DFA3
          A3A2A4A3A3DDDDDDE9E8E8F5F4F4F4F4F4F6F6F58282820F11110F11110F1111
          0F1111818282F5F5F4F5F4F3BBBBBB0F1111BBBBBBF4F4F3F5F4F4F2F1F1F5F4
          F4E8E8E8DCDDDCA4A3A3A4A3A3DCDBDAEAE9E9F6F5F5F4F4F3F5F5F48182820F
          1111F4F3F3F4F4F3F2F2F1F3F3F2F5F5F42B2C2C5657570F1111BBBBBAF3F3F2
          F4F3F3F0EFEFF2F1F1E6E6E5DAD9D8A4A4A3A5A4A3D9D8D7E8E8E7F5F5F4F4F3
          F2F5F5F48182820F1111B9B9B9BABAB9B8B8B8C6C8C7F4F4F47273730F11110F
          1111BABABAF2F2F1F2F1F1EEEEEDEFEFEEE4E3E3D7D5D5A5A4A4A5A5A4D5D3D3
          E4E4E3F2F2F1EFEFEEF3F3F28081810F11110F11110F11110F11110F1111F2F2
          F2F0EFEE9B9B9A0F1111B8B8B8EFEFEEEEEDEDEAE9E8ECECEBE2E2E1D3D2D2A5
          A5A4A6A5A5CFCECDE1E0E0EEEEEDEDECEBEEEDEDE0DFDFB4B4B3B2B3B2B3B3B2
          B1B1B0C2C1C0EEEEEDEBEAE9EAE9E8C4C4C4DEDEDDEAEAE9E9E8E7E7E6E5ECEB
          EADFDFDECFCECDA6A5A5A6A6A5D5D4D4ECEBEBF5F5F5F2F2F2F2F1F1EFEFEFEF
          EFEFF1F0F0F3F3F3F4F4F4F1F1F2EFEFEFEFEFEFF0F0F0F0F0EFEFEFEFF0F0EF
          F2F1F1F4F4F4F6F6F6ECEBEBD5D4D4A6A6A5AFAFAEE9E9E8F0F0F0EBEBEBECEB
          EBEDEDEDECECECECECECECEBEBEBEBEBEBEBEBECECEBECECECECECECECEBEBEC
          ECECECECECECEBEBECEBEBEBEBEBEBEBEBF0F0F0E9E9E8AFAFAEE1E1E1CECDCC
          D2D1D0D1D0CECDCDCBCDCCCAD1D0CED1D0CED1D0CED1D0CED1D0CED1D0CED1D0
          CED1D0CED1D0CED1D0CED1D0CED1D0CED1D0CEC8C7C5C5C4C2D2D1D0CECDCCE1
          E1E1}
        ExplicitTop = 1
      end
      object Image2: TImage
        Left = 1
        Top = 61
        Width = 30
        Height = 30
        Align = alClient
        ExplicitLeft = 60
        ExplicitTop = 1
        ExplicitWidth = 105
      end
      object Image3: TImage
        Left = 1
        Top = 91
        Width = 30
        Height = 30
        Align = alClient
        ExplicitLeft = 60
        ExplicitTop = 1
        ExplicitWidth = 105
      end
      object Image4: TImage
        Left = 1
        Top = 121
        Width = 30
        Height = 30
        Align = alClient
        ExplicitLeft = 60
        ExplicitTop = 1
        ExplicitWidth = 105
      end
      object Image5: TImage
        Left = 1
        Top = 151
        Width = 30
        Height = 30
        Align = alClient
        ExplicitLeft = 60
        ExplicitTop = 1
        ExplicitWidth = 105
      end
      object Label6: TLabel
        Left = 1
        Top = 1
        Width = 163
        Height = 30
        Align = alClient
        Alignment = taCenter
        Caption = 'LEGENDA'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Layout = tlCenter
        ExplicitWidth = 61
        ExplicitHeight = 16
      end
      object Label9: TLabel
        Left = 31
        Top = 31
        Width = 133
        Height = 30
        Align = alClient
        Caption = '   Finalizar'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        Layout = tlCenter
        ExplicitWidth = 60
        ExplicitHeight = 13
      end
    end
  end
  object cdsTotalizadorRateio: TClientDataSet
    PersistDataPacket.Data = {
      680000009619E0BD010000001800000003000000000003000000680014544F54
      414C495A41444F525F564C525F44455343080004000000000010544F54414C49
      5A41444F525F534F4D4108000400000000000349434D01004900000001000557
      494454480200020002000000}
    Active = True
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'TOTALIZADOR_VLR_DESC'
        DataType = ftFloat
      end
      item
        Name = 'TOTALIZADOR_SOMA'
        DataType = ftFloat
      end
      item
        Name = 'ICM'
        DataType = ftString
        Size = 2
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 156
    Top = 252
    object cdsTotalizadorRateioICM: TStringField
      FieldName = 'ICM'
      Size = 2
    end
    object cdsTotalizadorRateioTOTALIZADOR_VLR_DESC: TFloatField
      FieldName = 'TOTALIZADOR_VLR_DESC'
    end
    object cdsTotalizadorRateioTOTALIZADOR_SOMA: TFloatField
      FieldName = 'TOTALIZADOR_SOMA'
    end
  end
  object dsTempECF: TDataSource
    DataSet = dmCupomFiscal.dbTemp_NFCE
    OnStateChange = dsTempECFStateChange
    OnDataChange = dsTempECFDataChange
    Left = 720
    Top = 170
  end
  object dsFormaPagamento: TDataSource
    DataSet = cdsFormaPagamento
    Left = 720
    Top = 117
  end
  object dsTemp_ECF_FormaPag: TDataSource
    DataSet = dmCupomFiscal.dbTemp_NFCE_FormaPag
    Left = 721
    Top = 223
  end
  object cdsFormaPagamento: TClientDataSet
    PersistDataPacket.Data = {
      5C0000009619E0BD0100000018000000030000000000030000005C0002494404
      000100000000000944455343524943414F010049000000010005574944544802
      0002001400045450414701004900000001000557494454480200020002000000}
    Active = True
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'ID'
        DataType = ftInteger
      end
      item
        Name = 'DESCRICAO'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'TPAG'
        DataType = ftString
        Size = 2
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 585
    Top = 118
    object cdsFormaPagamentoID: TIntegerField
      FieldName = 'ID'
    end
    object cdsFormaPagamentoDESCRICAO: TStringField
      FieldName = 'DESCRICAO'
    end
    object cdsFormaPagamentoTPAG: TStringField
      FieldName = 'TPAG'
      Size = 2
    end
  end
  object qurBandeiraCartao: TIBQuery
    Database = dmCupomFiscal.DataBase
    Transaction = dmCupomFiscal.Transaction
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      
        'select * from FORMAS_PAGAMENTO_CARD_TBAND where FORMA_PAG=:pForm' +
        'aPag')
    Left = 160
    Top = 205
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'pFormaPag'
        ParamType = ptUnknown
      end>
    object qurBandeiraCartaoFORMA_PAG: TIntegerField
      FieldName = 'FORMA_PAG'
      Origin = '"FORMAS_PAGAMENTO_CARD_TBAND"."FORMA_PAG"'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object qurBandeiraCartaoCARD_TBAND: TIBStringField
      FieldName = 'CARD_TBAND'
      Origin = '"FORMAS_PAGAMENTO_CARD_TBAND"."CARD_TBAND"'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      Size = 2
    end
    object qurBandeiraCartaoDESCRICAO: TIBStringField
      FieldName = 'DESCRICAO'
      Origin = '"FORMAS_PAGAMENTO_CARD_TBAND"."DESCRICAO"'
      Required = True
      Size = 40
    end
  end
  object imgLista: TImageList
    Left = 156
    Top = 298
    Bitmap = {
      494C010102000500040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00524A
      4200393121003931290042312900423129004231290042312900423929004231
      29005A4A4200FFFFFF00FFFFFF00FFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF006B635A00FFFF
      FF00FFFFFF00FFFFFF00FFF7EF00F7EFE700E7DED600DECEC600CEC6B500CEBD
      B500D6CEC6006B635A00FFFFFF00FFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFF5555550003030300ACACAC00FFFFFFFF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF006B5A5200FFFF
      FF00D6CEC600F7F7F700F7EFE700ADA59C00E7DED600D6C6BD00A59C9400C6BD
      AD00D6C6BD006B635A00FFFFFF00FFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF5858
      5800000000000000000011111100FFFFFFFF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF006B635A00FFFF
      FF00D6CEC600F7F7F700F7EFE700B5A59C00E7DED600D6C6BD00AD9C9400C6BD
      AD00D6C6BD006B635A00FFFFFF00FFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF5B5B5B000000
      00000000000003030300ACACAC00FFFFFFFF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF006B635A00FFFF
      FF00D6CEC600F7F7F700F7EFE700B5A59C00E7DED600D6C6BD00AD9C9400C6BD
      AD00D6C6BD00736B6300FFFFFF00FFFFFF00FFFFFFFFFFFFFFFFFFFFFFFF8787
      87002E2E2E00060606000A0A0A003D3D3D00A2A2A2005F5F5F00000000000000
      000002020200A8A8A800FFFFFFFFFFFFFFFF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00736B6300FFFF
      FF00D6CEC600F7F7F700F7EFE700B5A59C00E7DED600D6C6BD00AD9C9400C6BD
      AD00CEC6BD007B6B6300FFFFFF00FFFFFF00FFFFFFFFDEDEDE00262626000000
      0000000000000000000000000000000000000000000000000000000000000202
      0200A5A5A500FFFFFFFFFFFFFFFFFFFFFFFF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF007B6B6300FFFF
      FF00D6CEC600F7F7F700F7EFE700B5A59C00E7DED600D6C6BD00AD9C9400C6BD
      AD00CEC6BD007B736B00FFFFFF00FFFFFF00FFFFFFFF27272700000000002E2E
      2E00B6B6B600FFFFFFFFFFFFFFFFA2A2A200191919000000000000000000A2A2
      A200FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF007B736B00FFFF
      FF00D6CEC600F7F7F700F7EFE700B5A59C00E7DED600D6C6BD00AD9C9400C6BD
      AD00CEC6BD0084736B00FFFFFF00FFFFFF008A8A8A00000000002E2E2E00FFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF1212120000000000BEBE
      BE00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF0084736B00FFFF
      FF00D6CEC600F7F7F700F7EFE700B5A59C00E7DED600D6C6BD00AD9C9400C6BD
      AD00CEC6BD00847B7300FFFFFF00FFFFFF003131310000000000B9B9B900FFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8B8B8B00000000006060
      6000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00847B7300FFFF
      FF00D6CEC600FFFFF700F7EFEF00B5A59C00E7DEDE00D6C6BD00ADA59400C6BD
      B500CEC6BD008C847300FFFFFF00FFFFFF000606060001010100FFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCECECE00000000003333
      3300FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00847B73008C7B7300DEDE
      DE00ADA5A500ADA59C00ADA59C00A59C94009C948C009C948400948C8400948C
      7B00B5ADA50094847B00847B7300FFFFFF000606060000000000FFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCECECE00000000003434
      3400FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00847B7300FFF7EF00EFE7
      DE00E7DED600E7D6CE00DED6C600D6CEBD00CEC6BD00C6BDB500BDB5A500B5AD
      9C00AD9C9400A59C8C00847B7300FFFFFF003030300000000000B7B7B700FFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8D8D8D00000000005E5E
      5E00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00847B7300FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00DED6CE00847B7300FFFFFF0089898900000000002E2E2E00FFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF1414140000000000B8B8
      B800FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00847B7300847B
      7300847B7300847B7300847B7300847B7300847B7300847B7300847B7300847B
      7300847B7300847B7300FFFFFF00FFFFFF00FFFFFFFF25252500000000002D2D
      2D00B6B6B600FFFFFFFFFFFFFFFFA5A5A5001A1A1A00000000004A4A4A00FFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00847B7300FFF7F700FFF7EF00FFF7EF00FFF7F700847B7300FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFFFFDDDDDD00242424000000
      000000000000000000000000000000000000000000003F3F3F00FFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00847B7300847B7300847B7300847B7300FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFFFFFFFFFFFFFFFFFFFF8484
      84002C2C2C000404040009090900383838009B9B9B00FFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000}
  end
end
