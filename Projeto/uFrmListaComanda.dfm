object FrmListaComandas: TFrmListaComandas
  Left = 0
  Top = 0
  Caption = 'Listar Comandas'
  ClientHeight = 625
  ClientWidth = 779
  Color = clWindow
  Ctl3D = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDesktopCenter
  OnKeyDown = FormKeyDown
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object dbgListaComandas: TDBGrid
    Left = 0
    Top = 87
    Width = 779
    Height = 235
    Align = alTop
    Color = clInfoBk
    DataSource = dsComandas
    DrawingStyle = gdsClassic
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Arial'
    Font.Style = []
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
    ParentFont = False
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    OnDrawColumnCell = dbgListaComandasDrawColumnCell
    OnKeyDown = dbgListaComandasKeyDown
    OnMouseUp = dbgListaComandasMouseUp
    OnTitleClick = dbgListaComandasTitleClick
    Columns = <
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'STATUS'
        Title.Alignment = taCenter
        Title.Caption = '*'
        Width = 23
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ID'
        Title.Caption = 'Comanda'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'VLR_TOTAL'
        Title.Alignment = taRightJustify
        Title.Caption = 'Valor Total'
        Width = 80
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NOME'
        Title.Caption = 'Funcion'#225'rio'
        Width = 100
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DATA_HORA'
        Title.Caption = 'Data/Hora'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PAGO'
        Title.Alignment = taCenter
        Title.Caption = 'PG'
        Width = 23
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'OBSERVACAO'
        Title.Caption = 'Observa'#231#227'o'
        Visible = True
      end>
  end
  object pnlTop: TPanel
    Left = 0
    Top = 0
    Width = 779
    Height = 87
    Align = alTop
    TabOrder = 0
    object Label1: TLabel
      Left = 24
      Top = 15
      Width = 75
      Height = 13
      Caption = 'Comanda Inicial'
    end
    object Label2: TLabel
      Left = 14
      Top = 38
      Width = 85
      Height = 13
      Caption = 'Funcion'#225'rio Inicial'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsUnderline]
      ParentFont = False
      OnClick = Label2Click
    end
    object Label3: TLabel
      Left = 46
      Top = 61
      Width = 53
      Height = 13
      Caption = 'Data Inicial'
    end
    object Label4: TLabel
      Left = 242
      Top = 15
      Width = 22
      Height = 13
      Caption = 'Final'
    end
    object Label5: TLabel
      Left = 242
      Top = 38
      Width = 22
      Height = 13
      Caption = 'Final'
    end
    object Label6: TLabel
      Left = 242
      Top = 61
      Width = 22
      Height = 13
      Caption = 'Final'
    end
    object edtComIni: TEdit
      Left = 106
      Top = 12
      Width = 102
      Height = 19
      TabOrder = 0
    end
    object edtFunIni: TEdit
      Left = 105
      Top = 36
      Width = 102
      Height = 19
      TabOrder = 2
      Text = '1'
    end
    object edtComFim: TEdit
      Left = 274
      Top = 12
      Width = 102
      Height = 19
      TabOrder = 1
    end
    object edtFunFim: TEdit
      Left = 274
      Top = 35
      Width = 102
      Height = 19
      TabOrder = 3
      Text = '9999999'
    end
    object edtDtIni: TMaskEdit
      Left = 106
      Top = 58
      Width = 102
      Height = 19
      EditMask = '!99/99/00;1;_'
      MaxLength = 8
      TabOrder = 4
      Text = '  /  /  '
    end
    object edtDtFim: TMaskEdit
      Left = 274
      Top = 58
      Width = 102
      Height = 19
      EditMask = '!99/99/00;1;_'
      MaxLength = 8
      TabOrder = 5
      Text = '  /  /  '
    end
    object btnPesquisar: TButton
      Left = 434
      Top = 34
      Width = 101
      Height = 43
      Caption = 'Pesquisar'
      TabOrder = 7
      OnClick = btnPesquisarClick
    end
    object ckbComandasPagas: TCheckBox
      Left = 434
      Top = 13
      Width = 143
      Height = 17
      Caption = 'Exibir comandas pagas'
      TabOrder = 6
    end
  end
  object stbRodape: TStatusBar
    Left = 0
    Top = 606
    Width = 779
    Height = 19
    Panels = <
      item
        Text = '* Selecionar todos os registros'
        Width = 180
      end
      item
        Width = 50
      end>
  end
  object pnlRodape: TPanel
    Left = 0
    Top = 557
    Width = 779
    Height = 49
    Align = alBottom
    TabOrder = 3
    object Label7: TLabel
      Left = 21
      Top = 7
      Width = 227
      Height = 16
      Caption = 'Quantidade de comandas selecionadas:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object lblQtd: TLabel
      Left = 260
      Top = 7
      Width = 7
      Height = 16
      Caption = '0'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label9: TLabel
      Left = 21
      Top = 25
      Width = 227
      Height = 16
      Caption = 'Valor total das comandas selecionadas:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object lblValor: TLabel
      Left = 260
      Top = 25
      Width = 44
      Height = 16
      Caption = 'R$ 0,00'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object BtnOK: TButton
      Left = 506
      Top = 6
      Width = 91
      Height = 32
      Caption = 'OK'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Arial'
      Font.Style = []
      ModalResult = 1
      ParentFont = False
      TabOrder = 0
      OnClick = BtnOKClick
    end
    object btnCancelar: TButton
      Left = 603
      Top = 6
      Width = 91
      Height = 32
      Caption = 'Cancelar'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Arial'
      Font.Style = []
      ModalResult = 2
      ParentFont = False
      TabOrder = 1
      OnClick = btnCancelarClick
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 322
    Width = 779
    Height = 24
    Align = alTop
    Caption = 'Itens das comandas selecionadas'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 4
  end
  object dgbListaMercadorias: TDBGrid
    Left = 0
    Top = 346
    Width = 779
    Height = 211
    Align = alClient
    DataSource = dsMercadorias
    DrawingStyle = gdsClassic
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Arial'
    Font.Style = []
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
    ParentFont = False
    TabOrder = 5
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    OnDrawColumnCell = dgbListaMercadoriasDrawColumnCell
    OnKeyDown = dgbListaMercadoriasKeyDown
    OnMouseUp = dgbListaMercadoriasMouseUp
    OnTitleClick = dgbListaMercadoriasTitleClick
    Columns = <
      item
        Expanded = False
        FieldName = 'PAGO'
        Title.Alignment = taCenter
        Title.Caption = '*'
        Width = 22
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'MERCADORIA'
        Title.Caption = 'Mercadoria'
        Width = 90
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DESCRICAO'
        Title.Caption = 'Descri'#231#227'o'
        Width = 370
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'QUANTIDADE'
        Title.Alignment = taRightJustify
        Title.Caption = 'Quantidade'
        Width = 80
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'VLR_UNITARIO'
        Title.Alignment = taRightJustify
        Title.Caption = 'Valor Unit'#225'rio'
        Width = 90
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'VLR_TOTAL'
        Title.Alignment = taRightJustify
        Title.Caption = 'Valor Total'
        Width = 85
        Visible = True
      end>
  end
  object cdsListaComandas: TClientDataSet
    PersistDataPacket.Data = {
      150100009619E0BD010000001800000009000000000003000000150102494401
      004A0004000100055749445448020002003C0009444154415F484F5241080008
      000400000009564C525F544F54414C0B0005000000020008444543494D414C53
      0200020002000557494454480200020012000A4F42534552564143414F01004A
      00000001000557494454480200020064000653544154555301004A0020000100
      055749445448020002000200044E4F4D4501004A000000010005574944544802
      00020050000E49445F46554E43494F4E4152494F040001000400000004504147
      4F01004900000001000557494454480200020001000552455354410800040000
      00010007535542545950450200490006004D6F6E6579000000}
    Active = True
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'ID'
        Attributes = [faRequired]
        DataType = ftWideString
        Size = 30
      end
      item
        Name = 'DATA_HORA'
        Attributes = [faRequired]
        DataType = ftDateTime
      end
      item
        Name = 'VLR_TOTAL'
        DataType = ftBCD
        Precision = 18
        Size = 2
      end
      item
        Name = 'OBSERVACAO'
        DataType = ftWideString
        Size = 50
      end
      item
        Name = 'STATUS'
        Attributes = [faFixed]
        DataType = ftWideString
        Size = 1
      end
      item
        Name = 'NOME'
        DataType = ftWideString
        Size = 40
      end
      item
        Name = 'ID_FUNCIONARIO'
        Attributes = [faRequired]
        DataType = ftInteger
      end
      item
        Name = 'PAGO'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'RESTA'
        DataType = ftCurrency
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 484
    Top = 140
    object cdsListaComandasID: TWideStringField
      FieldName = 'ID'
      Origin = '"TEMP_BAR_COMANDAS"."ID"'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      Size = 30
    end
    object cdsListaComandasDATA_HORA: TDateTimeField
      FieldName = 'DATA_HORA'
      Origin = '"TEMP_BAR_COMANDAS"."DATA_HORA"'
      Required = True
    end
    object cdsListaComandasVLR_TOTAL: TBCDField
      FieldName = 'VLR_TOTAL'
      Origin = '"TEMP_BAR_COMANDAS"."VLR_TOTAL"'
      currency = True
      Precision = 18
      Size = 2
    end
    object cdsListaComandasOBSERVACAO: TWideStringField
      FieldName = 'OBSERVACAO'
      Origin = '"TEMP_BAR_COMANDAS"."OBSERVACAO"'
      Size = 50
    end
    object cdsListaComandasSTATUS: TWideStringField
      FieldName = 'STATUS'
      ProviderFlags = []
      FixedChar = True
      Size = 1
    end
    object cdsListaComandasNOME: TWideStringField
      FieldName = 'NOME'
      Origin = '"FUNCIONARIOS"."NOME"'
      Size = 40
    end
    object cdsListaComandasID_FUNCIONARIO: TIntegerField
      FieldName = 'ID_FUNCIONARIO'
      Origin = '"TEMP_BAR_COMANDAS"."ID_FUNCIONARIO"'
      Required = True
    end
    object cdsListaComandasPAGO: TStringField
      FieldName = 'PAGO'
      Size = 1
    end
    object cdsListaComandasRESTA: TCurrencyField
      FieldName = 'RESTA'
    end
  end
  object dsComandas: TDataSource
    DataSet = cdsListaComandas
    OnUpdateData = dsComandasUpdateData
    Left = 584
    Top = 132
  end
  object qryListaComandas: TIBQuery
    Database = dmCupomFiscal.DataBase
    Transaction = dmCupomFiscal.Transaction
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      
        'Select t.*, '#39'0'#39' as Status, '#39'0'#39' as Pago, F.Nome, sum(i.Vlr_Total)' +
        ' as Resta'
      ' from TEMP_BAR_COMANDAS t'
      'left outer join Funcionarios F on f.ID = ID_FUNCIONARIO'
      'left outer join TEMP_BAR_COMANDAS_ITEM i on i.ID = t.ID'
      'Group by 1,2,3,4,5,6,7,8')
    Left = 672
    Top = 129
    object qryListaComandasID: TIBStringField
      FieldName = 'ID'
      Origin = '"TEMP_BAR_COMANDAS"."ID"'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      Size = 30
    end
    object qryListaComandasDATA_HORA: TDateTimeField
      FieldName = 'DATA_HORA'
      Origin = '"TEMP_BAR_COMANDAS"."DATA_HORA"'
    end
    object qryListaComandasVLR_TOTAL: TIBBCDField
      FieldName = 'VLR_TOTAL'
      Origin = '"TEMP_BAR_COMANDAS"."VLR_TOTAL"'
      Precision = 18
      Size = 2
    end
    object qryListaComandasOBSERVACAO: TIBStringField
      FieldName = 'OBSERVACAO'
      Origin = '"TEMP_BAR_COMANDAS"."OBSERVACAO"'
      Size = 50
    end
    object qryListaComandasID_FUNCIONARIO: TIntegerField
      FieldName = 'ID_FUNCIONARIO'
      Origin = '"TEMP_BAR_COMANDAS"."ID_FUNCIONARIO"'
      Required = True
    end
    object qryListaComandasSTATUS: TIBStringField
      FieldName = 'STATUS'
      ProviderFlags = []
      FixedChar = True
      Size = 1
    end
    object qryListaComandasPAGO: TIBStringField
      FieldName = 'PAGO'
      ProviderFlags = []
      FixedChar = True
      Size = 1
    end
    object qryListaComandasNOME: TIBStringField
      FieldName = 'NOME'
      Origin = '"FUNCIONARIOS"."NOME"'
      Size = 40
    end
    object qryListaComandasRESTA: TIBBCDField
      FieldName = 'RESTA'
      ProviderFlags = []
      Precision = 18
      Size = 2
    end
  end
  object imgListComandas: TImageList
    Left = 408
    Top = 190
    Bitmap = {
      494C010102000500040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000005F5F5F005F5F5F000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000005F5F5F0000000000000000005F5F5F0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00005F5F5F00000000000000000000000000000000005F5F5F00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000005F5F
      5F000000000000000000000000000000000000000000000000005F5F5F000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000005F5F5F000000
      000000000000000000005F5F5F005F5F5F000000000000000000000000005F5F
      5F00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000005F5F5F000000
      0000000000005F5F5F0000000000000000005F5F5F0000000000000000000000
      00005F5F5F000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000005F5F
      5F005F5F5F00000000000000000000000000000000005F5F5F00000000000000
      0000000000005F5F5F0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000005F5F5F000000
      0000000000005F5F5F0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000005F5F
      5F005F5F5F000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF0000000000000000007FFE7FFE00000000
      7FFE7FFE000000007FFE7CFE000000007FFE787E000000007FFE703E00000000
      7FFE601E000000007FFE400E000000007FFE4306000000007FFE678200000000
      7FFE7FC2000000007FFE7FE6000000007FFE7FFE000000007FFE7FFE00000000
      7FFE7FFE00000000000000000000000000000000000000000000000000000000
      000000000000}
  end
  object dsMercadorias: TDataSource
    DataSet = cdsMercadorias
    Left = 478
    Top = 356
  end
  object cdsMercadorias: TClientDataSet
    PersistDataPacket.Data = {
      120100009619E0BD01000000180000000800000000000300000012010A4D4552
      4341444F5249410100490000000100055749445448020002000D000944455343
      524943414F01004900000001000557494454480200020064000A5155414E5449
      44414445080004000000010007535542545950450200490006004D6F6E657900
      0C564C525F554E49544152494F08000400000001000753554254595045020049
      0006004D6F6E65790009564C525F544F54414C08000400000001000753554254
      5950450200490006004D6F6E657900044954454D010049000000010005574944
      5448020002000400045041474F04000100000000000A49445F434F4D414E4441
      0100490000000100055749445448020002001E000000}
    Active = True
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'MERCADORIA'
        DataType = ftString
        Size = 13
      end
      item
        Name = 'DESCRICAO'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'QUANTIDADE'
        DataType = ftCurrency
      end
      item
        Name = 'VLR_UNITARIO'
        DataType = ftCurrency
      end
      item
        Name = 'VLR_TOTAL'
        DataType = ftCurrency
      end
      item
        Name = 'ITEM'
        DataType = ftString
        Size = 4
      end
      item
        Name = 'PAGO'
        DataType = ftInteger
      end
      item
        Name = 'ID_COMANDA'
        DataType = ftString
        Size = 30
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 566
    Top = 358
    object cdsMercadoriasMERCADORIA: TStringField
      FieldName = 'MERCADORIA'
      Size = 13
    end
    object cdsMercadoriasDESCRICAO: TStringField
      FieldName = 'DESCRICAO'
      Size = 100
    end
    object cdsMercadoriasQUANTIDADE: TCurrencyField
      FieldName = 'QUANTIDADE'
      DisplayFormat = '###,##0.00'
      currency = False
    end
    object cdsMercadoriasVLR_UNITARIO: TCurrencyField
      FieldName = 'VLR_UNITARIO'
    end
    object cdsMercadoriasITEM: TStringField
      FieldName = 'ITEM'
      Size = 4
    end
    object cdsMercadoriasVLR_TOTAL: TCurrencyField
      FieldName = 'VLR_TOTAL'
    end
    object cdsMercadoriasPAGO: TIntegerField
      FieldName = 'PAGO'
    end
    object cdsMercadoriasID_COMANDA: TStringField
      FieldName = 'ID_COMANDA'
      Size = 30
    end
  end
  object cdsImpressao: TClientDataSet
    PersistDataPacket.Data = {
      E60000009619E0BD010000001800000006000000000003000000E6000A4D4552
      4341444F5249410100490000000100055749445448020002000D000944455343
      524943414F01004900000001000557494454480200020064000A5155414E5449
      44414445080004000000010007535542545950450200490006004D6F6E657900
      0C564C525F554E49544152494F08000400000001000753554254595045020049
      0006004D6F6E657900044954454D010049000000010005574944544802000200
      040009564C525F544F54414C0800040000000100075355425459504502004900
      06004D6F6E6579000000}
    Active = True
    Aggregates = <>
    Params = <>
    Left = 261
    Top = 416
    object cdsImpressaoMERCADORIA: TStringField
      FieldName = 'MERCADORIA'
      Size = 13
    end
    object cdsImpressaoDESCRICAO: TStringField
      FieldName = 'DESCRICAO'
      Size = 100
    end
    object cdsImpressaoQUANTIDADE: TCurrencyField
      FieldName = 'QUANTIDADE'
      DisplayFormat = '###,##0.00'
      currency = False
    end
    object cdsImpressaoVLR_UNITARIO: TCurrencyField
      FieldName = 'VLR_UNITARIO'
    end
    object cdsImpressaoITEM: TStringField
      FieldName = 'ITEM'
      Size = 4
    end
    object cdsImpressaoVLR_TOTAL: TCurrencyField
      FieldName = 'VLR_TOTAL'
    end
  end
end
