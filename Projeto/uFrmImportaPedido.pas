unit uFrmImportaPedido;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, IBCustomDataSet, IBQuery, ExtCtrls, Grids, DBGrids,
  StdCtrls, Buttons, ComCtrls, DBClient, pngimage, ImgList, StrUtils,
  System.ImageList;

type
  TfrmImportaPedido = class(TForm)
    Panel1: TPanel;
    grdDados: TDBGrid;
    pnlRodape: TPanel;
    btnCupom: TBitBtn;
    btnFechar: TBitBtn;
    dsPedido: TDataSource;
    qurPedido: TIBQuery;
    pnlCabecalho: TPanel;
    Label11: TLabel;
    edtCliente: TEdit;
    edtClienteNome: TEdit;
    Label34: TLabel;
    edtPedidoIni: TEdit;
    Label35: TLabel;
    edtPedidoFim: TEdit;
    Label30: TLabel;
    dtpInicial: TDateTimePicker;
    Label31: TLabel;
    dtpFinal: TDateTimePicker;
    Label1: TLabel;
    btnAtualizar: TBitBtn;
    qurPedidoID: TIntegerField;
    qurPedidoDATA: TDateField;
    qurPedidoCCUSTO: TIntegerField;
    qurPedidoVLR_PRODUTO: TIBBCDField;
    qurPedidoVLR_DESCONTO: TIBBCDField;
    qurPedidoVLR_TOTAL: TIBBCDField;
    qurPedidoSTATUS: TIBStringField;
    qurPedidoOBSERVACAO: TIBStringField;
    qurPedidoCLIENTE: TIBStringField;
    qurPedidoOPERACAO: TIBStringField;
    qurPedidoFORMA_PAG: TIBStringField;
    Panel3: TPanel;
    Panel2: TPanel;
    DBGrid1: TDBGrid;
    dsPedidoItem: TDataSource;
    qurPedidoItem: TIBQuery;
    qurPedidoItemID_PEDIDO: TIntegerField;
    qurPedidoItemITEM: TIntegerField;
    qurPedidoItemMERCADORIA: TIBStringField;
    qurPedidoItemQUANTIDADE: TIBBCDField;
    qurPedidoItemVLR_UNITARIO: TIBBCDField;
    qurPedidoItemVLR_DESCONTO: TIBBCDField;
    qurPedidoItemVLR_TOTAL: TIBBCDField;
    qurPedidoItemUNIDADE: TIBStringField;
    Panel4: TPanel;
    qurPedidoCLIFOR: TIntegerField;
    qurPedidoEL_CHAVE: TIntegerField;
    qurPedidoItemVLR_DESCONTO_ITEM: TIBBCDField;
    cdsPedidosItem: TClientDataSet;
    cdsPedidosItemID_PEDIDO: TIntegerField;
    cdsPedidosItemITEM: TIntegerField;
    cdsPedidosItemMERCADORIA: TStringField;
    cdsPedidosItemQUANTIDADE: TCurrencyField;
    cdsPedidosItemVLR_UNITARIO: TCurrencyField;
    cdsPedidosItemVLR_DESCONTO: TCurrencyField;
    cdsPedidosItemVLR_TOTAL: TCurrencyField;
    cdsPedidosItemUNIDADE: TStringField;
    cdsPedidosItemVLR_DESCONTO_ITEM: TCurrencyField;
    cdsPedidosItemQUANTIDADE_DISP: TCurrencyField;
    qurPedidoItemEST_ATUAL: TIBBCDField;
    Image1: TImage;
    qurPedidoCHAVE_ECF: TIBStringField;
    imgLista: TImageList;
    StatusBar1: TStatusBar;
    qurPedidoCHAVE_NFE: TIBStringField;
    qurPedidoItemNCM: TIBStringField;
    cdsPedidosItemNCM: TStringField;
    cdsNCM: TClientDataSet;
    cdsNCMID_PEDIDO: TIntegerField;
    cdsNCMMERCADORIA: TStringField;
    cdsNCMNCM: TStringField;
    qurPedidoItemMERC_DESCRICAO: TIBStringField;
    cdsPedidosItemMERC_DESCRICAO: TStringField;
    cdsNCMDESCRICAO: TStringField;
    procedure btnCupomClick(Sender: TObject);
    procedure btnFecharClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnAtualizarClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dsPedidoDataChange(Sender: TObject; Field: TField);
    procedure Label1Click(Sender: TObject);
    procedure edtClienteExit(Sender: TObject);
    procedure edtClienteKeyPress(Sender: TObject; var Key: Char);
    procedure edtPedidoIniKeyPress(Sender: TObject; var Key: Char);
    procedure edtPedidoFimKeyPress(Sender: TObject; var Key: Char);
    procedure edtClienteEnter(Sender: TObject);
    procedure DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    Function contralaInsercao():Boolean;
    procedure grdDadosDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure StatusBar1DrawPanel(StatusBar: TStatusBar;
      Panel: TStatusPanel; const Rect: TRect);
    procedure grdDadosMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
  private
    { Private declarations }
    procedure filtrarPedidos();
    procedure filtrarPedidosItem();
  public
    { Public declarations }
  end;

var
  frmImportaPedido: TfrmImportaPedido;

implementation

uses uFuncoes, uDMCupomFiscal, uFrmPrincipal, uFrmLoja,
  uFrmPesquisaCliente, uRotinasGlobais, uDMComponentes, uFrmNcm;

{$R *.dfm}

procedure TfrmImportaPedido.btnCupomClick(Sender: TObject);
var Obs : string;
  FlagImp: Boolean;
begin
  btnCupom.Enabled := False;
  try
    if(cdsPedidosItem.IsEmpty)then
    begin
      msgInformacao('N�o existem itens para inserir',Application.title);
      Abort;
    end;
    try
      try
        FlagImp := False;
        Panel4.Visible:=True;
        Application.ProcessMessages;
        cdsPedidosItem.First;
        While not(cdsPedidosItem.Eof)do
        begin
          if not(cdsNCM.Locate('MERCADORIA',cdsPedidosItemMERCADORIA.AsString,[loCaseInsensitive]))then
          begin
            if(cdsPedidosItemQUANTIDADE_DISP.value > 0) or not(dmCupomFiscal.PermiteEstoqueNegativoCCusto(CCUSTO.codigo)) then
            begin
              frmLoja.edtMercadoria.Text   := Trim(cdsPedidosItemMERCADORIA.AsString);
              if(cdsPedidosItemQUANTIDADE.value <= cdsPedidosItemQUANTIDADE_DISP.value)
                or not(dmCupomFiscal.PermiteEstoqueNegativoCCusto(CCUSTO.codigo))then
                frmLoja.reQuantidade.Value := cdsPedidosItemQUANTIDADE.AsCurrency
              else
                frmLoja.reQuantidade.Value := cdsPedidosItemQUANTIDADE_DISP.AsCurrency;
              frmLoja.reVlrUnitario.Value  := cdsPedidosItemVLR_UNITARIO.AsCurrency;
              frmLoja.reVlrDesconto.Value  := cdsPedidosItemVLR_DESCONTO_ITEM.AsCurrency;
              frmLoja.reTotal.Value        := cdsPedidosItemVLR_TOTAL.AsCurrency;
              frmLoja.ELChavePedido        := qurPedidoEL_CHAVE.AsInteger;
              frmLoja.edtMercadoriaExit(Self);
              frmLoja.subtotal;
              frmLoja.btnVende.Click;
              FlagImp := True;
              Application.ProcessMessages;
            end;
          end;
          cdsPedidosItem.Next;
        end;
        if(Flagimp)Then //Importou algum Item
        begin
          dmCupomFiscal.dbTemp_NFCE.Edit;
          dmCupomFiscal.dbTemp_NFCEVLR_DESCONTO.Value :=
            dmCupomFiscal.dbTemp_NFCEVLR_DESCONTO.Value +
            dmCupomFiscal.GetDescontoPedido(qurPedidoID.AsInteger);
          dmCupomFiscal.dbTemp_NFCE.Post;
          dmCupomFiscal.Transaction.CommitRetaining;
        end;
      finally
        Panel4.Visible:=False;
        frmLoja.ELChavePedido   := 0;
        frmImportaPedido.Visible:=False;   //Para n�o ficar aparecendo na tela quando executa cancelarCupom
        btnFechar.Click;
        Application.ProcessMessages;
      end
    except
      if cancelarCupomFiscal(True) then
        msgInformacao('Cupom Fiscal cancelado devido erros na importa��o!','');
    end;
  finally
    btnCupom.Enabled := True;
  end;
end;

procedure TfrmImportaPedido.btnFecharClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmImportaPedido.FormShow(Sender: TObject);
begin
  dtpFinal.Date:=Date;
  if(Trim(edtCliente.Text) = '0')then
    dtpInicial.Date := Date-2
  else
    dtpInicial.Date := Date-30;
  filtrarPedidos;
end;

procedure TfrmImportaPedido.filtrarPedidos;
begin
{  qurPedido.SQL.Add('Select p.id, p.el_chave, p.data, p.ccusto, p.vlr_produto, p.vlr_total,'+
                    ' p.status, p.observacao, p.clifor, p.vlr_desconto,'+
                    ' p.clifor||''/''||p.cf_nome cliente,'+
                    ' p.operacao||''/''||o.descricao operacao,'+
                    ' p.forma_pag||''/''||fp.descricao forma_pag,'+
                    ' (case when (select count(cf.id) from est_ecf cf where cf.el_chave = p.el_chave and cf.Status = ''X'') > 0 then ''S'' else ''N'' end) as CHAVE_ECF'+
                    ' from  est_pedidos p'+
                    ' left outer join clifor c on c.id=p.clifor'+
                    ' left outer join operacoes o on o.id=p.operacao'+
                    ' left outer join formas_pagamento fp on fp.id=p.forma_pag'+
                    ' where (p.id between :pPedidoI and :pPedidoF)'+
                    ' and (p.data between :pDataI and :pDataF)'+
                    ' and (p.status=''X'') and (p.ES = ''S'')');
}
  qurPedido.Active := False;
  qurPedido.SQL.Clear;
  qurPedido.SQL.Add('Select p.id, p.el_chave, p.data, p.ccusto, p.vlr_produto, p.vlr_total,'+
                    ' p.status, p.observacao, p.clifor, p.vlr_desconto,'+
                    ' p.clifor||''/''||p.cf_nome cliente,'+
                    ' p.operacao||''/''||o.descricao operacao,'+
                    ' p.forma_pag||''/''||fp.descricao forma_pag,'+
                    ' (case when (select count(cf.id) from est_ecf cf where cf.el_chave = p.el_chave and cf.Status = ''X'') > 0 then ''S'' else ''N'' end) as CHAVE_ECF,'+
                    ' (case when (Select Count(i.id_nf) FROM est_nf_item i,est_nf nf Where nf.id = i.id_nf and nf.status = ''X'' and i.id_pedido=p.id) > 0 then ''S'' else ''N'' end) as Chave_NFE'+
                    ' from  est_pedidos p'+
                    ' left outer join clifor c on c.id=p.clifor'+
                    ' left outer join operacoes o on o.id=p.operacao'+
                    ' left outer join formas_pagamento fp on fp.id=p.forma_pag'+
                    ' where (p.id between :pPedidoI and :pPedidoF)'+
                    ' and (p.data between :pDataI and :pDataF)'+
                    ' and (p.status=''X'') and (p.ES = ''S'') and (p.Ccusto = :pCC)');
  if (StrToIntDef(edtCliente.Text, 0) > 0) then
  begin
    qurPedido.SQL.Add(' and (p.clifor = :pCliente)');
    qurPedido.ParamByName('pCliente').AsInteger := StrToInt(edtCliente.Text);
  end;
  qurPedido.SQL.Add(' order by p.ID Desc, p.data desc, p.clifor');
  qurPedido.ParamByName('pDataI').AsDate      := dtpInicial.Date;
  qurPedido.ParamByName('pDataF').AsDate      := dtpFinal.Date;
  qurPedido.ParamByName('pPedidoI').AsInteger := StrToInt(edtPedidoIni.Text);
  qurPedido.ParamByName('pPedidoF').AsInteger := StrToInt(edtPedidoFim.Text);
  qurPedido.ParamByName('pCC').AsInteger      := CCUSTO.codigo;
  qurPedido.Active := True;
  qurPedido.First;
  if (qurPedido.Eof) then
  begin
    if not(cdsPedidosItem.IsEmpty)then
      cdsPedidosItem.EmptyDataSet;
    btnCupom.Enabled := False;
    msgInformacao('Nenhum Pedido encontrado!','');
  end;
end;

procedure TfrmImportaPedido.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  qurPedido.Close;
end;

procedure TfrmImportaPedido.btnAtualizarClick(Sender: TObject);
begin
  filtrarPedidos;
end;

procedure TfrmImportaPedido.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Atual, Item_Atual : Integer;
begin
  //Fechar
  if Key=vk_escape then btnFechar.Click;

  //Pesquisa cliente
  if (key=vk_F12) and (edtCliente.Focused) then Label1Click(Sender);

  if(key = vk_f8) and not(cdsNCM.IsEmpty) then
  begin
    Atual := qurPedidoID.AsInteger;
    Item_Atual := qurPedidoItemITEM.Value;
    Application.CreateForm(TFrmNcm, FrmNCM);
    FrmNcm.ShowModal;
    frmNcm.free;
    filtrarPedidos;
    qurPedido.Locate('ID',Atual,[loCaseInsensitive]);
    filtrarPedidosItem;
    qurPedidoItem.Locate('Item',Item_Atual,[loCaseInsensitive]);
  end;
end;

procedure TfrmImportaPedido.filtrarPedidosItem;
begin
  qurPedidoItem.Active := False;
  qurPedidoItem.SQL.Clear;
  qurPedidoItem.SQL.Add('select i.*, m.descricao merc_descricao, m.unidade, me.est_Atual, M.NCM from est_pedidos_item i'+
                ' left outer join est_pedidos p on p.id = i.id_pedido'+
                ' left outer join est_mercadorias m on m.id=i.mercadoria'+
                ' left outer join est_mercadorias_estoque me on me.mercadoria = m.id and me.ccusto = p.ccusto'+
                ' where i.id_pedido=:pID'+
                ' order by i.item');
  qurPedidoItem.ParamByName('pID').AsInteger := qurPedidoID.AsInteger;
  qurPedidoItem.Active := True;
  qurPedidoItem.First;
  if not(qurPedidoItem.IsEmpty)then
  begin
    cdsPedidosItem.Open;
    cdsPedidosItem.EmptyDataSet;
    cdsNCM.EmptyDataSet;
    while not(qurPedidoItem.Eof)do
    begin
      cdsPedidosItem.Append;
      cdsPedidosItemID_PEDIDO.value         := qurPedidoItemID_PEDIDO.AsInteger;
      cdsPedidosItemITEM.value              := qurPedidoItemITEM.AsInteger;
      cdsPedidosItemMERCADORIA.value        := qurPedidoItemMERCADORIA.asString;
      cdsPedidosItemQUANTIDADE.value        := qurPedidoItemQUANTIDADE.AsCurrency;
      cdsPedidosItemVLR_UNITARIO.value      := qurPedidoItemVLR_UNITARIO.AsCurrency;
      cdsPedidosItemVLR_DESCONTO.value      := qurPedidoItemVLR_DESCONTO.AsCurrency;
      cdsPedidosItemVLR_TOTAL.value         := qurPedidoItemVLR_TOTAL.AsCurrency;
      cdsPedidosItemMERC_DESCRICAO.value    := qurPedidoItemMERC_DESCRICAO.asString;
      cdsPedidosItemUNIDADE.value           := qurPedidoItemUNIDADE.asString;
      cdsPedidosItemVLR_DESCONTO_ITEM.value := qurPedidoItemVLR_DESCONTO_ITEM.AsCurrency;
      cdsPedidosItemQUANTIDADE_DISP.value   := qurPedidoItemEST_ATUAL.AsCurrency;
      cdsPedidosItemNCM.Value               := qurPedidoItemNCM.AsString;
      //if(qurPedidoItemNCM.IsNull) or (qurPedidoItemNCM.Value = '00000000')then // Condi��o Menor quer 8 Digitos
      if not(ValidarNCM(qurPedidoItemNCM.Value))then // Condi��o Menor quer 8 Digitos
      begin
        cdsNCM.Open;
        cdsNCM.Insert;
        cdsNCMID_PEDIDO.Value  := qurPedidoID.Value;
        cdsNCMMERCADORIA.Value := qurPedidoItemMERCADORIA.Value;
        cdsNCMDESCRICAO.Value  := qurPedidoItemMERC_DESCRICAO.asString;
        cdsNCMNCM.Value        := qurPedidoItemNCM.Value;
        cdsNCM.Post;
      end;
      cdsPedidosItem.Post;
      qurPedidoItem.Next;
    end;
  end;
end;


procedure TfrmImportaPedido.dsPedidoDataChange(Sender: TObject;
  Field: TField);
begin
  filtrarPedidosItem;
  btnCupom.Enabled := contralaInsercao();
  StatusBar1.Panels.Add;
  StatusBar1.Panels.Delete(3);
end;

procedure TfrmImportaPedido.Label1Click(Sender: TObject);
begin
  frmPesquisaCliente := TfrmPesquisaCliente.Create(Self);
  try
    if (frmPesquisaCliente.ShowModal=mrOK) then
      edtCliente.Text:= FrmPesquisaCliente.qCliFor.FieldByName('ID').AsString;
  finally
    FrmPesquisaCliente.Free;
  end;
end;

procedure TfrmImportaPedido.edtClienteExit(Sender: TObject);
begin
  If(Trim(edtCliente.Text)<>'0')then
  begin
    DMCupomFiscal.dbQuery1.Active:=False;
    DMCupomFiscal.dbQuery1.SQL.Clear;
    DMCupomFiscal.dbQuery1.SQL.Add('Select Nome, TPreco, Limite FROM CliFor Where ID=:pID');
    DMCupomFiscal.dbQuery1.ParamByName('pID').AsInteger:=StrToIntDef(edtCliente.Text,0);
    DMCupomFiscal.dbQuery1.Active:=True;
    DMCupomFiscal.dbQuery1.First;
    If not(DMCupomFiscal.dbQuery1.Eof) then
    begin
      edtClienteNome.Text:=DMCupomFiscal.dbQuery1.FieldByName('Nome').AsString;
      filtrarPedidos;
    end
    Else
    begin
      msgAviso('Cliente n�o cadastrado','');
      edtCliente.SetFocus;
    end;
  end
  else
  begin
    edtClienteNome.Text := 'Todos os clientes...';
    filtrarPedidos;
  end;
end;

procedure TfrmImportaPedido.edtClienteKeyPress(Sender: TObject;
  var Key: Char);
begin
  if not (somenteNumeros(Key)) then
    Key := #0; //Retorna nullo
end;
procedure TfrmImportaPedido.edtPedidoIniKeyPress(Sender: TObject;
  var Key: Char);
begin
  if not(somenteNumeros(Key)) then
    Key:= #0;
end;

procedure TfrmImportaPedido.edtPedidoFimKeyPress(Sender: TObject;
  var Key: Char);
begin
  if not(somenteNumeros(Key)) then
    Key:= #0;
end;

procedure TfrmImportaPedido.edtClienteEnter(Sender: TObject);
begin
  edtClienteNome.Text:='';
end;

procedure TfrmImportaPedido.DBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  if not(cdsPedidosItem.IsEmpty)then
  begin
    with DBGrid1 do
    begin
      if(cdsPedidosItemNCM.IsNull) or (cdsPedidosItemNCM.Value = '00000000')then // Ver Condicao de 8 Digitos
      begin
        if AnsiLowerCase(Column.FieldName) = 'ncm' then
          Canvas.Brush.Color := clSilver;
      end;
      if((cdsPedidosItemQUANTIDADE_DISP.value < cdsPedidosItemQUANTIDADE.value) and
      (cdsPedidosItemQUANTIDADE_DISP.value >0) and (dmCupomFiscal.PermiteEstoqueNegativoCCusto(CCUSTO.codigo)))then
      begin
        Canvas.Brush.Color := clYellow;
        Canvas.Font.Color  := clBlack;
      end
      else
        if((cdsPedidosItemQUANTIDADE_DISP.value <= 0) and (cdsPedidosItemQUANTIDADE.value > 0)) and
        (dmCupomFiscal.PermiteEstoqueNegativoCCusto(CCUSTO.codigo))then
        begin
          if (gdSelected in State) then
            Canvas.Font.Color := clBlack
          else
            Canvas.Font.Color := clWhite;
          Canvas.Brush.Color := clRed;
        end;
      Canvas.FillRect(Rect);
      DefaultDrawColumnCell(Rect,DataCol,Column,State);
    end;
  end;
end;

Function TfrmImportaPedido.contralaInsercao : Boolean;
begin
  if not(qurPedido.IsEmpty)then
  begin
    if(qurPedidoCHAVE_ECF.AsString = 'S')or(qurPedidoCHAVE_NFE.AsString = 'S')then
      Result := False
    else
    begin
      if not(cdsPedidosItem.IsEmpty)then
      begin
        cdsPedidosItem.First;
        while not(cdsPedidosItem.eof)do
        begin
          if((cdsPedidosItemQUANTIDADE_DISP.value <= 0) and (cdsPedidosItemQUANTIDADE.value > 0))
          and (dmCupomFiscal.PermiteEstoqueNegativoCCusto(CCUSTO.codigo))then
          begin
            cdsPedidosItem.Next;
          end
          else
          begin
            if not(cdsNCM.Locate('MERCADORIA',cdsPedidosItemMERCADORIA.value,[loCaseInsensitive])) then
            begin
              Result := True;
              Break;
            end
            else
              cdsPedidosItem.Next;
          end;
          if(cdsPedidosItem.Eof)then
            Result := False;
        end;
      end
      else
        Result := False;
    end;
  end
  else
    Result := False;
end;

procedure TfrmImportaPedido.grdDadosDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  if not(qurPedido.IsEmpty)then
  begin
    with(grdDados)do
    begin
      if(Column.Index = 0 )then
      begin
        Canvas.FillRect(Rect);
        Canvas.Brush.Color := clWhite;
        DefaultDrawColumnCell(Rect,DataCol,Column,State);
        case AnsiIndexStr(qurPedidoCHAVE_NFE.AsString,['S','N']) of
        0:imgLista.Draw(Canvas,Rect.Left+01,Rect.Top+1,1); //Com Royalties
        1:begin
            case AnsiIndexStr(qurPedidoCHAVE_ECF.AsString,['S','N']) of
            0:imgLista.Draw(Canvas,Rect.Left+01,Rect.Top+1,1); //Com Royalties
            1:imgLista.Draw(Canvas,Rect.Left+01,Rect.Top+1,0); //Sem Royalties
            end;
          end;
        end;
      end;
    end;
  end;
end;

procedure TfrmImportaPedido.StatusBar1DrawPanel(StatusBar: TStatusBar;
  Panel: TStatusPanel; const Rect: TRect);
begin
  with StatusBar1.Canvas do
  begin
    FillRect(Rect);
    case(Panel.Index)of
    0:begin
        case AnsiIndexStr(qurPedidoCHAVE_NFE.AsString,['S','N']) of
        0:begin
          imgLista.Draw(StatusBar1.Canvas,Rect.Left+01,Rect.Top+1,1); //Cancelado
          TextOut(Rect.Left + 20, Rect.Top + 1,' - Esta Pr� venda ja esta vinculada a um documento fiscal');
        end;
        1:begin
            case AnsiIndexStr(qurPedidoCHAVE_ECF.AsString,['S','N']) of
              0:begin
                imgLista.Draw(StatusBar1.Canvas,Rect.Left+01,Rect.Top+1,1); //Cancelado
                TextOut(Rect.Left + 20, Rect.Top + 1,' - Esta Pr� venda ja esta vinculada a um documento fiscal');
              end;
              1:begin
                imgLista.Draw(StatusBar1.Canvas,Rect.Left+01,Rect.Top+1,0); //Finalizado
                TextOut(Rect.Left + 20, Rect.Top + 1,' - Pedido liberado para pr�-Venda');
              end;
            end;
          end;
        end;
      end;
    1:begin
        if(cdsNCM.Locate('ID_PEDIDO',cdsPedidosItemID_PEDIDO.Value,[loCaseInsensitive]))then
          TextOut(Rect.Left + 1, Rect.Top + 1,'Existem mercadorias com NCM Inv�lido')
        else
          TextOut(Rect.Left + 1, Rect.Top + 1,'');
      end;
    2:begin
        if(cdsNCM.Locate('ID_PEDIDO',cdsPedidosItemID_PEDIDO.Value,[loCaseInsensitive]))then
          TextOut(Rect.Left + 1, Rect.Top + 1,'F8 - Ajusta NCM')
        else
          TextOut(Rect.Left + 1, Rect.Top + 1,'');
      end
    end;
  end;
end;

procedure TfrmImportaPedido.grdDadosMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  Mensagem : String;
begin
  Mensagem := '';
  try
    Panel4.Caption := 'Pesquisando...';
    Panel4.Visible := True;
    Application.ProcessMessages;
    if (grdDados.MouseCoord(X, Y).X in [1]) then //Coluna 2
    begin
      if(qurPedidoCHAVE_ECF.AsString = 'S')then
      begin
        dmCupomFiscal.dbQuery1.Active := False;
        dmCupomFiscal.dbQuery1.SQL.Clear;
        dmCupomFiscal.dbQuery1.SQL.Add('Select ID, Caixa, Cupom, Data, Vlr_TOtal from est_ecf where EL_Chave = :pChave');
        dmCupomFiscal.dbQuery1.ParamByName('pChave').AsInteger := qurPedidoEL_CHAVE.AsInteger;
        dmCupomFiscal.dbQuery1.Active := True;
        IF not(dmCupomFiscal.dbQuery1.IsEmpty)Then
        begin
          Mensagem := 'Existe um cupom fiscal vinculado a essa Pr�-Venda!'+#13+
                      'C�digo: '+dmCupomFiscal.dbQuery1.fieldByName('ID').asString+#13+
                      'Caixa: '+dmCupomFiscal.dbQuery1.fieldByName('CAIXA').asString+#13+
                      'Cupom: '+dmCupomFiscal.dbQuery1.fieldByName('CUPOM').asString+#13+
                      'Data: '+dmCupomFiscal.dbQuery1.fieldByName('DATA').asString+#13+
                      'Valor: '+FormatFloat('R$ ###,##0.00',dmCupomFiscal.dbQuery1.fieldByName('Vlr_Total').asCurrency);
        end;
      end;
      if(qurPedidoCHAVE_NFE.AsString = 'S')then
      begin
        if(Mensagem <> '')then
          Mensagem := Mensagem +#13+'-------------------------------------------------'+#13;
        dmCupomFiscal.dbQuery2.Active := False;
        dmCupomFiscal.dbQuery2.SQL.Clear;
        dmCupomFiscal.dbQuery2.SQL.Add('Select nf.ID, nf.Numero, nf.Serie, nf.Data, nf.Vlr_NotaFiscal from est_NF nf '+
                                       'left outer join est_Nf_Item i on i.id_nf = nf.id where i.id_pedido = :pID');
        dmCupomFiscal.dbQuery2.ParamByName('pID').AsInteger := qurPedidoID.AsInteger;
        dmCupomFiscal.dbQuery2.Active := True;
        IF not(dmCupomFiscal.dbQuery2.IsEmpty)Then
        begin
          if(Mensagem = '')Then
            Mensagem := 'Existe uma Nota fiscal vinculada a essa Pr�-Venda!'+#13+
                        'C�digo: '+dmCupomFiscal.dbQuery2.fieldByName('ID').asString+#13+
                        'N�mero: '+dmCupomFiscal.dbQuery2.fieldByName('Numero').asString+#13+
                        'S�rie: '+dmCupomFiscal.dbQuery2.fieldByName('Serie').asString+#13+
                        'Data: '+dmCupomFiscal.dbQuery2.fieldByName('DATA').asString+#13+
                        'Valor: '+FormatFloat('R$ ###,##0.00',dmCupomFiscal.dbQuery2.fieldByName('Vlr_NotaFiscal').asCurrency)
          else
            Mensagem :=Mensagem + 'Existe uma Nota fiscal vinculada a essa Pr�-Venda!'+#13+
                                  'C�digo: '+dmCupomFiscal.dbQuery2.fieldByName('ID').asString+#13+
                                  'N�mero: '+dmCupomFiscal.dbQuery2.fieldByName('Numero').asString+#13+
                                  'S�rie: '+dmCupomFiscal.dbQuery2.fieldByName('Serie').asString+#13+
                                  'Data: '+dmCupomFiscal.dbQuery2.fieldByName('DATA').asString+#13+
                                  'Valor: '+FormatFloat('R$ ###,##0.00',dmCupomFiscal.dbQuery2.fieldByName('Vlr_NotaFiscal').asCurrency)

        end;
      end;
      if(Mensagem <> '')then
         msgInformacao(Mensagem,Application.Title)
      else
        msgInformacao('N�o existe documento fiscal vinculado a essa Pr�-Venda',Application.Title)
    end;
  finally
    begin
      Panel4.Caption := 'Aguarde!  Imprimindo Item...';
      Panel4.Visible := False;
    end;
  end;
end;  
end.


