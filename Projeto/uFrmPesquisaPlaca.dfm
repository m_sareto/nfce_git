object FrmPesquisaPlaca: TFrmPesquisaPlaca
  Left = 0
  Top = 0
  Caption = 'Pesquisar Placa'
  ClientHeight = 409
  ClientWidth = 419
  Color = clWindow
  Ctl3D = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDesktopCenter
  OnKeyDown = FormKeyDown
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel3: TBevel
    Left = 0
    Top = 0
    Width = 419
    Height = 17
    Align = alTop
    Shape = bsBottomLine
    ExplicitWidth = 412
  end
  object Label32: TLabel
    Left = 10
    Top = 9
    Width = 41
    Height = 13
    Caption = 'Filtro...'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlue
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Edit1: TEdit
    Left = 10
    Top = 25
    Width = 300
    Height = 19
    CharCase = ecUpperCase
    Ctl3D = False
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentCtl3D = False
    ParentFont = False
    TabOrder = 0
    OnChange = Edit1Change
  end
  object CheckBox1: TCheckBox
    Left = 324
    Top = 26
    Width = 65
    Height = 17
    Caption = 'Inicie por'
    Checked = True
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    State = cbChecked
    TabOrder = 1
  end
  object DBGrid1: TDBGrid
    Left = 0
    Top = 49
    Width = 419
    Height = 341
    Align = alBottom
    Color = clWhite
    Ctl3D = False
    DataSource = DataSource1
    DrawingStyle = gdsClassic
    FixedColor = clNavy
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    Options = [dgTitles, dgColLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
    ParentCtl3D = False
    ParentFont = False
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWhite
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnDblClick = DBGrid1DblClick
    OnKeyDown = DBGrid1KeyDown
    Columns = <
      item
        Expanded = False
        FieldName = 'PLACA'
        Title.Caption = 'Placa'
        Width = 66
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DESCRICAO'
        Title.Caption = 'Descri'#231#227'o'
        Width = 325
        Visible = True
      end>
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 390
    Width = 419
    Height = 19
    Panels = <
      item
        Alignment = taCenter
        Text = 'Esc - Sair sem confirmar'
        Width = 128
      end
      item
        Text = 'F12 - Refaz pesquisa'
        Width = 112
      end
      item
        BiDiMode = bdLeftToRight
        ParentBiDiMode = False
        Text = 'Enter - Confirma'
        Width = 50
      end>
  end
  object IBQuery1: TIBQuery
    Database = dmCupomFiscal.DataBase
    Transaction = dmCupomFiscal.Transaction
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'Select M.Placa, M.DESCRICAO from CLIFOR_FROTA M')
    Left = 104
    Top = 129
    object IBQuery1PLACA: TIBStringField
      FieldName = 'PLACA'
      Origin = '"CLIFOR_FROTA"."PLACA"'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      Size = 7
    end
    object IBQuery1DESCRICAO: TIBStringField
      FieldName = 'DESCRICAO'
      Origin = '"CLIFOR_FROTA"."DESCRICAO"'
    end
  end
  object DataSource1: TDataSource
    DataSet = IBQuery1
    Left = 152
    Top = 129
  end
end
