unit uFrmGerarParcelas;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, DBClient, ExtCtrls, StdCtrls, Buttons, Mask, TREdit, Grids,
  DBGrids, DBCtrls;

type
  TfrmGerarParcelas = class(TForm)
    Panel1: TPanel;
    Label63: TLabel;
    Label64: TLabel;
    Label65: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    ed_n_Parcela: TEdit;
    me_Dt_Pri_Parcela: TMaskEdit;
    CheckBox3: TCheckBox;
    re_vl_Parcela: TRealEdit;
    btnSimularParc: TBitBtn;
    Panel2: TPanel;
    Label66: TLabel;
    re_vl_T_Parcelas: TRealEdit;
    re_vl_Total_Prazo: TRealEdit;
    btnFechar: TBitBtn;
    DBGrid2: TDBGrid;
    ed_Dias: TEdit;
    dsTemp_ECF_Fatura: TDataSource;
    procedure ed_n_ParcelaExit(Sender: TObject);
    procedure DBGrid2Exit(Sender: TObject);
    procedure btnSimularParcClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btnFecharClick(Sender: TObject);
    procedure DBGrid2Enter(Sender: TObject);
    procedure Confere_Parcelas;
    procedure ed_DiasExit(Sender: TObject);
  private
    { Private declarations }
    Data:TDate;
  public
    { Public declarations }
  end;

var
  frmGerarParcelas: TfrmGerarParcelas;

     
implementation

uses DateUtils, UFrmFim, uDMCupomFiscal, uFuncoes, uFrmPrincipal;

{$R *.dfm}

procedure TfrmGerarParcelas.ed_n_ParcelaExit(Sender: TObject);
begin
  if StrToIntDef(ed_n_Parcela.Text,0)=0 then
    ed_n_Parcela.SetFocus                  
  else
    re_vl_Parcela.Value := re_vl_Total_Prazo.value / (StrToIntDef(ed_n_Parcela.text,1));
end;

procedure TfrmGerarParcelas.DBGrid2Exit(Sender: TObject);
begin
  Confere_Parcelas;
end;

procedure TfrmGerarParcelas.btnSimularParcClick(Sender: TObject);
Var  i, tag:Integer;
     yAno, yMes, yDia : word;
     xAno, xMes, xDia : word;
     Dif:Currency; { Diferenca entre as Parcelas }

begin
  //Verifica data informada
  IF(StrToDate(me_Dt_Pri_Parcela.Text) < Date)then
  begin
    if not msgPergunta('Data da primeira parcela � menor que a data atual! Desejas Prosseguir ?','') then
    begin
      me_Dt_Pri_Parcela.SetFocus;
      Abort;
    end;
  end;

  btnSimularParc.Enabled:=False;
  re_vl_T_Parcelas.Value := 0;
  Dif:=0;
  i:=0;
  tag:=0;
  DecodeDate (StrToDate(me_Dt_Pri_Parcela.text) , yAno, ymes, yDia);

  //Limpa tabela Temp
  dmCupomFiscal.dbTemp_NFCE_Fatura.Close;
  dmCupomFiscal.IBSQL1.Close;
  dmCupomFiscal.IBSQL1.SQL.Clear;
  dmCupomFiscal.IBSQL1.SQL.Add('Delete From temp_nfce_Fatura Where Caixa=:pCaixa');
  dmCupomFiscal.IBSQL1.ParamByName('pCaixa').AsString := FormatFloat('0000', FRENTE_CAIXA.Caixa);
  dmCupomFiscal.IBSQL1.ExecQuery;
  dmCupomFiscal.getTemp_NFCE_Fatura();

  //Diferenca em Centavos
  Dif := re_vl_Total_Prazo.Value - (re_vl_Parcela.Value * StrToIntDef(ed_n_Parcela.text,1));
  Data:=StrToDate(me_Dt_Pri_Parcela.text);
  for i := 1 to StrToIntDef(ed_n_Parcela.text,1) do
  begin
    tag := tag + 1;
    dmCupomFiscal.dbTemp_NFCE_Fatura.Append;
    DMCupomFiscal.dbTemp_NFCE_FaturaCAIXA.Value   := FormatFloat('0000', FRENTE_CAIXA.Caixa);
    DMCupomFiscal.dbTemp_NFCE_FaturaDATA.Value    := Data;
    DMCupomFiscal.dbTemp_NFCE_FaturaVALOR.Value   := re_vl_Parcela.Value + Dif;
    DMCupomFiscal.dbTemp_NFCE_FaturaPARCELA.Value := tag;
    DMCupomFiscal.dbTemp_NFCE_Fatura.Post;

    re_vl_T_Parcelas.Value := re_vl_T_Parcelas.Value + DMCupomFiscal.dbTemp_NFCE_FaturaVALOR.Value;
    Dif:=0;
    if (CheckBox3.Checked) then
      Data:=IncMonth(Data,1)
    ELSE
      Data:=IncDay(Data, StrToIntDef(ed_Dias.Text,0));
  end;
  dmCupomFiscal.Transaction.CommitRetaining;
  btnSimularParc.Enabled:=True;
  btnFechar.SetFocus;
end;

procedure TfrmGerarParcelas.FormShow(Sender: TObject);
begin
  dmCupomFiscal.getTemp_NFCE_Fatura();
  re_vl_Total_Prazo.Value := dmCupomFiscal.GetTemp_Valor_Crediario;
  me_Dt_Pri_Parcela.Text  := DateToStr(Date);
  ed_dias.Text            := IntToStr(dmCupomFiscal.getDiasPrazo(dmCupomFiscal.dbTemp_NFCECLIFOR.AsInteger));
end;

procedure TfrmGerarParcelas.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  //Esc
  If (key=vk_Escape) then btnFechar.Click;
end;

procedure TfrmGerarParcelas.btnFecharClick(Sender: TObject);
begin
  if not(dmCupomFiscal.dbTemp_NFCE_Fatura.IsEmpty)then
    ModalResult := mrOK
  else
    ModalResult := mrCancel;
end;

procedure TfrmGerarParcelas.DBGrid2Enter(Sender: TObject);
begin
  Confere_Parcelas;
end;

procedure TfrmGerarParcelas.Confere_Parcelas;
begin
  DMCupomFiscal.dbTemp_NFCE_Fatura.First;
  If not(DMCupomFiscal.dbTemp_NFCE_Fatura.eof) then
  begin
    re_vl_T_Parcelas.Value := 0;
    While not(DMCupomFiscal.dbTemp_NFCE_Fatura.Eof) do
    begin
      re_vl_T_Parcelas.Value := re_vl_T_Parcelas.Value + DMCupomFiscal.dbTemp_NFCE_FaturaVALOR.Value;
      DMCupomFiscal.dbTemp_NFCE_Fatura.Next;
    end;    
    if ((re_vl_Total_Prazo.Value) <> (re_vl_T_Parcelas.Value)) then
    begin
      msgAviso('Existe uma diferen�a entre o somat�rio do valor das parcelas e total a prazo','Aten��o');
      DBGrid2.SetFocus;
      Abort;
    end;
  end;
end;

procedure TfrmGerarParcelas.ed_DiasExit(Sender: TObject);
begin
  if ((StrToIntDef(ed_n_Parcela.Text,0) > 1) and (StrToIntDef(ed_Dias.Text,0) = 0)) then
  begin
    msgAviso('N�mero de parcelas superior a 1. Dias de prazo deve ser superior a 0!','');
    ed_Dias.SetFocus;
  end
  else
  begin
    Data := IncDay(Date, StrToIntDef(ed_Dias.Text,0));
    me_Dt_Pri_Parcela.Text := DateToStr(Data);
  end;
end;


end.
