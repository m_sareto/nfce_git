unit uFrmSangriaSuprimento;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, TREdit, ExtCtrls, ComCtrls, Grids, DBGrids,
  DB, IBCustomDataSet, IBQuery, Mask;

type
  TfrmSangriaSuprimento = class(TForm)
    Panel1: TPanel;
    pnlBotoes: TPanel;
    btnOK: TBitBtn;
    btnFechar: TBitBtn;
    Label1: TLabel;
    reValor: TRealEdit;
    Label3: TLabel;
    edtDescricao: TEdit;
    Label5: TLabel;
    edtFPag: TEdit;
    edtFormaPagDesc: TEdit;
    DBGrid1: TDBGrid;
    qSup_San: TIBQuery;
    dsSup_San: TDataSource;
    qSup_SanEL_CHAVE: TIntegerField;
    qSup_SanCAIXA: TIBStringField;
    qSup_SanCCUSTO: TIntegerField;
    qSup_SanFUNCIONARIO: TIntegerField;
    qSup_SanFORMA_PAG: TIntegerField;
    qSup_SanTURNO: TIBStringField;
    qSup_SanOBSERVACAO: TIBStringField;
    qSup_SanDATA: TDateField;
    qSup_SanVALOR: TIBBCDField;
    Panel2: TPanel;
    MaskEdit1: TMaskEdit;
    MaskEdit2: TMaskEdit;
    Label2: TLabel;
    Label4: TLabel;
    btnAtualizar: TBitBtn;
    btnImprimir: TBitBtn;
    qSup_SanNOME: TIBStringField;
    qSup_SanDESC_FORMAPAG: TIBStringField;
    procedure btnFecharClick(Sender: TObject);
    procedure btnOKClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure Label5Click(Sender: TObject);
    procedure edtFPagExit(Sender: TObject);
    procedure imprime();
    procedure btnAtualizarClick(Sender: TObject);
    procedure btnImprimirClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    parametroTipo: String; //Parametro que identifica se � Sangria ou Suprimento
  end;

var
  frmSangriaSuprimento: TfrmSangriaSuprimento;

implementation

uses uRotinasGlobais, uDMCupomFiscal, uDMComponentes, uFrmPesquisaGenerica,
  uFuncoes, uFrmPrincipal, uFrmImpComprovaSangriaSup;

{$R *.dfm}

procedure TfrmSangriaSuprimento.btnFecharClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmSangriaSuprimento.btnOKClick(Sender: TObject);
begin
  try
    btnOK.Enabled:=False;
    If (reValor.Value>0) then
    begin
      try
        if parametroTipo='SU' then //Suprimento
        begin
          if not dmCupomFiscal.dbECF_San_Sup.Transaction.InTransaction then
            dmCupomFiscal.dbECF_San_Sup.Transaction.StartTransaction;
          dmCupomFiscal.dbECF_San_Sup.Open;
          dmCupomFiscal.dbECF_San_Sup.Insert;
          dmCupomFiscal.dbECF_San_SupEL_CHAVE.Value:=0; // Trigger
          dmCupomFiscal.dbECF_San_SupCAIXA.Value:=FormatFloat('0000',FRENTE_CAIXA.Caixa);
          dmCupomFiscal.dbECF_San_SupData.Value:= Now;
          dmCupomFiscal.dbECF_San_SupCCUSTO.Value:=CCUSTO.codigo;
          dmCupomFiscal.dbECF_San_SupFUNCIONARIO.Value:=LOGIN.usuarioCod;
          dmCupomFiscal.dbECF_San_SupFORMA_PAG.Value:=StrToIntDef(edtFPag.Text,0);
          dmCupomFiscal.dbECF_San_SupOBSERVACAO.Value:=edtDescricao.Text;
          dmCupomFiscal.dbECF_San_SupValor.Value:=reValor.Value;
          dmCupomFiscal.dbECF_San_Sup.Post;
          dmCupomFiscal.dbECF_San_Sup.Close;
{          suprimento(reValor.Value, edtDescricao.Text+#10+
                    'Funcion�rio: '+IntToStr(LOGIN.usuarioCod)+'/'+LOGIN.usuarioFuncNome+#10+
                    'Turno: '+frmPrincipal.getTurnoFuncionario(LOGIN.usuarioCod));}
          dmCupomFiscal.dbECF_San_Sup.Transaction.Commit;
          imprime();           
        end
        Else                      //Sangria
        begin
          if not dmCupomFiscal.dbECF_San_Sup.Transaction.InTransaction then
            dmCupomFiscal.dbECF_San_Sup.Transaction.StartTransaction;
          dmCupomFiscal.dbECF_San_Sup.Open;
          dmCupomFiscal.dbECF_San_Sup.Insert;
          dmCupomFiscal.dbECF_San_SupEL_CHAVE.Value:=0; // Trigger
          dmCupomFiscal.dbECF_San_SupCAIXA.Value:=FormatFloat('0000',FRENTE_CAIXA.Caixa);
          dmCupomFiscal.dbECF_San_SupData.Value:= Now;
          dmCupomFiscal.dbECF_San_SupCCUSTO.Value:=CCUSTO.codigo;
          dmCupomFiscal.dbECF_San_SupFUNCIONARIO.Value:=LOGIN.usuarioCod;
          dmCupomFiscal.dbECF_San_SupFORMA_PAG.Value:=StrToIntDef(edtFPag.Text,0);
          dmCupomFiscal.dbECF_San_SupOBSERVACAO.Value:=edtDescricao.Text;
          dmCupomFiscal.dbECF_San_SupValor.Value:= -(reValor.Value);
          dmCupomFiscal.dbECF_San_Sup.Post;
          dmCupomFiscal.dbECF_San_Sup.Close;
{          sangria(reValor.Value, edtDescricao.Text+#10+
                  'Funcion�rio: '+IntToStr(LOGIN.usuarioCod)+'/'+LOGIN.usuarioFuncNome+#10+
                  'Turno: '+frmPrincipal.getTurnoFuncionario(LOGIN.usuarioCod));}
          dmCupomFiscal.dbECF_San_Sup.Transaction.Commit;
          imprime();
        end;
        btnAtualizarClick(Action);
        reValor.Value := 0;
        edtDescricao.Clear;
        edtFPag.Clear;
        edtFormaPagDesc.Clear;
      except
        on E: Exception do
        begin
          if dmCupomFiscal.dbECF_San_Sup.Transaction.InTransaction then
            dmCupomFiscal.dbECF_San_Sup.Transaction.Rollback;
          logErros(Self, caminhoLog, 'Ocorreu o seguinte erro: '+e.Message, 'Ocorreu o seguinte erro: ', 'S', e);
        end;
      end;
    end
    else
      msgAviso('O Valor deve ser diferente de zero',Application.title)
  finally
    btnOK.Enabled:=True;
    //Close;
  end;
end;

procedure TfrmSangriaSuprimento.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key = vk_Escape then btnFechar.Click;

  //Forma Pgto
  If (key=vk_F12) and (edtFPag.Focused) then Label5Click(Action);
end;

procedure TfrmSangriaSuprimento.FormShow(Sender: TObject);
begin
  MaskEdit1.Text := DateToStr(Now -30);
  MaskEdit2.Text := DateToStr(Now);  
  If parametroTipo='SU' then
  Begin
    frmSangriaSuprimento.Caption:= 'SUPRIMENTO (+) Adicionar dinheiro no Caixa';
    edtDescricao.Text:='Efetuada Suprimento.';
    with qSup_San do
    begin
      Active := False;
      SQL.Clear;
      SQL.Add('Select ss.*, f.nome, fp.Descricao as Desc_FormaPag From Est_ECF_Sangria_Suprimento ss '+
              'Left outer join funcionarios f on f.id = ss.funcionario '+
              'Left outer join Formas_Pagamento fp on fp.ID = ss.FORMA_PAG '+
              'where Valor > 0 and ss.Data >= :pDtIni and ss.Data<= :pDtFim '+
              'Order by Data Desc');
      ParamByName('pDtIni').AsDateTime := StrToDate(MaskEdit1.Text);
      ParamByName('pDtFim').AsDateTime := StrToDate(MaskEdit2.Text);
      Active := True;
    end;
  end
  Else
  begin
    frmSangriaSuprimento.Caption:= 'SANGRIA (-) Retirada de dinheiro do Caixa';
    edtDescricao.Text:='Efetuada Sangria.';
    with qSup_San do
    begin
      Active := False;
      SQL.Clear;
      SQL.Add('Select ss.*, f.nome, fp.Descricao as Desc_FormaPag From Est_ECF_Sangria_Suprimento ss '+
              'Left outer join funcionarios f on f.id = ss.funcionario '+
              'Left outer join Formas_Pagamento fp on fp.ID = ss.FORMA_PAG '+
              'where Valor < 0 and ss.Data >= :pDtIni and ss.Data<= :pDtFim '+
              'Order by Data Desc');
      ParamByName('pDtIni').AsDateTime := StrToDate(MaskEdit1.Text);
      ParamByName('pDtFim').AsDateTime := StrToDate(MaskEdit2.Text);
      Active := True;
    end;
  end;
end;

procedure TfrmSangriaSuprimento.Label5Click(Sender: TObject);
begin
  frmPesquisaGenerica := tfrmPesquisaGenerica.Create(Self,'Formas_Pagamento');
  try
    if (frmPesquisaGenerica.ShowModal=mrOK) then
      edtFPag.Text:= FrmPesquisaGenerica.IBQuery1.FieldByName('id').AsString;
  finally
    frmPesquisaGenerica.Free;
  end;
end;

procedure TfrmSangriaSuprimento.edtFPagExit(Sender: TObject);
begin
  If StrToIntDef(edtFPag.Text,0)<0 then Abort;

  DMCupomFiscal.dbQuery1.Active:=False;
  DMCupomFiscal.dbQuery1.SQL.Clear;
  DMCupomFiscal.dbQuery1.SQL.Add('Select ID, Descricao FROM Formas_Pagamento Where ID=:pID');
  DMCupomFiscal.dbQuery1.ParamByName('pID').AsInteger:=StrToIntDef(edtFPag.Text,0);
  DMCupomFiscal.dbQuery1.Active:=True;
  DMCupomFiscal.dbQuery1.First;
  If not(DMCupomFiscal.dbQuery1.Eof) then
    edtFormaPagDesc.Text:=DMCupomFiscal.dbQuery1.FieldByName('Descricao').AsString
  Else
  begin
    msgAviso('Forma de Pagamento n�o cadastrada!','');
    edtFPag.SetFocus;
  end;
end;

//Tipo = SA ou SU
procedure TfrmSangriaSuprimento.imprime();
begin
  Application.CreateForm(TfrmImpCompSangSup,frmImpCompSangSup);
  if(parametroTipo='SA')then
    frmImpCompSangSup.RLLabel1.Caption := 'Comprovante de Sangria'
  else
    frmImpCompSangSup.RLLabel1.Caption := 'Comprovante de Suprimento';
  frmImpCompSangSup.RLLabel2.Caption  := FormatFloat('R$ ###,##0.00',reValor.Value);
  frmImpCompSangSup.RLLabel3.Caption  := edtDescricao.Text;
  frmImpCompSangSup.RLLabel4.Caption  := IntToStr(LOGIN.usuarioCod)+' - '+LOGIN.usuarioFuncNome;
  frmImpCompSangSup.RLLabel12.Caption := edtFPag.Text + ' - ' + edtFormaPagDesc.Text;
  frmImpCompSangSup.RLLabel13.Caption := DateToStr(Now);
  frmImpCompSangSup.RLLabel6.Caption  := frmPrincipal.getTurnoFuncionario(LOGIN.usuarioCod);
  frmImpCompSangSup.rlVenda.Preview();
  frmImpCompSangSup.Free
end;

procedure TfrmSangriaSuprimento.btnAtualizarClick(Sender: TObject);
begin
  with qSup_San do
  begin
    Active := false;
    ParamByName('pDtIni').AsDateTime := StrToDate(MaskEdit1.Text);
    ParamByName('pDtFim').AsDateTime := StrToDate(MaskEdit2.Text);
    Active := True;
  end;
end;

procedure TfrmSangriaSuprimento.btnImprimirClick(Sender: TObject);
begin
  Application.CreateForm(TfrmImpCompSangSup,frmImpCompSangSup);
  if(parametroTipo='SA')then
  begin
    frmImpCompSangSup.RLLabel1.Caption := 'Comprovante de Sangria';
    frmImpCompSangSup.RLLabel2.Caption  := FormatFloat('R$ ###,##0.00',qSup_SanVALOR.AsCurrency*-1);
  end
  else
  begin
    frmImpCompSangSup.RLLabel1.Caption := 'Comprovante de Suprimento';
    frmImpCompSangSup.RLLabel2.Caption  := FormatFloat('R$ ###,##0.00',qSup_SanVALOR.AsCurrency);
  end;
  frmImpCompSangSup.RLLabel3.Caption  := qSup_SanOBSERVACAO.AsString;
  frmImpCompSangSup.RLLabel4.Caption  := IntToStr(qSup_SanFUNCIONARIO.Value)+' - '+qSup_SanNOME.AsString;
  frmImpCompSangSup.RLLabel6.Caption  := frmPrincipal.getTurnoFuncionario(StrToIntDef(qSup_SanTURNO.AsSTring,0));
  frmImpCompSangSup.RLLabel12.Caption := qSup_SanFORMA_PAG.AsString+' - '+qSup_SanDESC_FORMAPAG.value;
  frmImpCompSangSup.RLLabel13.Caption := DateToStr(qSup_SanDATA.Value);
  frmImpCompSangSup.rlVenda.Preview();
  frmImpCompSangSup.Free
end;

end.
