object FrmConsultaResumoVendas: TFrmConsultaResumoVendas
  Left = 502
  Top = 297
  BorderIcons = [biSystemMenu]
  Caption = 'Rela'#231#227'o Vendas de Mercadorias'
  ClientHeight = 166
  ClientWidth = 328
  Color = clBtnHighlight
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel
    Left = 47
    Top = 84
    Width = 51
    Height = 13
    Caption = 'Periodo de'
  end
  object Label5: TLabel
    Left = 64
    Top = 52
    Width = 34
    Height = 13
    Caption = #185' Turno'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label1: TLabel
    Left = 43
    Top = 20
    Width = 55
    Height = 13
    Caption = 'Funcion'#225'rio'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlue
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsUnderline]
    ParentFont = False
    OnClick = Label1Click
  end
  object Label4: TLabel
    Left = 156
    Top = 21
    Width = 133
    Height = 13
    Caption = 'Nome do Funcion'#225'rio...'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 147
    Width = 328
    Height = 19
    Panels = <
      item
        Text = #185' N'#227'o informar para todos'
        Width = 140
      end
      item
        Text = 'Pode ser impresso em Bobina'
        Width = 30
      end>
    ExplicitTop = 148
    ExplicitWidth = 410
  end
  object BitBtn1: TBitBtn
    Left = 41
    Top = 112
    Width = 65
    Height = 25
    Caption = 'Ok'
    TabOrder = 4
    OnClick = BitBtn1Click
  end
  object BitBtn2: TBitBtn
    Left = 106
    Top = 112
    Width = 65
    Height = 25
    Caption = 'Sair'
    TabOrder = 5
    OnClick = BitBtn2Click
  end
  object MaskEdit1: TMaskEdit
    Left = 108
    Top = 81
    Width = 53
    Height = 19
    Ctl3D = False
    EditMask = '!99/99/00;1;_'
    MaxLength = 8
    ParentCtl3D = False
    TabOrder = 3
    Text = '01/01/00'
  end
  object Edit3: TEdit
    Left = 108
    Top = 49
    Width = 30
    Height = 19
    CharCase = ecUpperCase
    Ctl3D = False
    MaxLength = 2
    ParentCtl3D = False
    TabOrder = 2
  end
  object edFun: TEdit
    Left = 108
    Top = 17
    Width = 45
    Height = 19
    Ctl3D = False
    MaxLength = 6
    ParentCtl3D = False
    TabOrder = 1
    Text = '0'
    OnExit = edFunExit
  end
  object RLReport1: TRLReport
    Left = 728
    Top = 26
    Width = 283
    Height = 718
    Margins.LeftMargin = 2.000000000000000000
    Margins.TopMargin = 0.000000000000000000
    Margins.RightMargin = 2.000000000000000000
    Margins.BottomMargin = 0.000000000000000000
    Borders.Sides = sdCustom
    Borders.DrawLeft = False
    Borders.DrawTop = False
    Borders.DrawRight = False
    Borders.DrawBottom = False
    DataSource = DataSource1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = []
    PageSetup.PaperSize = fpCustom
    PageSetup.PaperWidth = 75.000000000000000000
    PageSetup.PaperHeight = 190.000000000000000000
    Transparent = False
    Visible = False
    object RLBand1: TRLBand
      Left = 8
      Top = 84
      Width = 267
      Height = 32
      Margins.TopMargin = 1.000000000000000000
      BandType = btTitle
      Borders.Sides = sdCustom
      Borders.DrawLeft = False
      Borders.DrawTop = False
      Borders.DrawRight = False
      Borders.DrawBottom = True
      Borders.FixedBottom = True
      object RLLabel3: TRLLabel
        Left = 0
        Top = 4
        Width = 267
        Height = 14
        Align = faClientTop
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        BeforePrint = RLLabel3BeforePrint
      end
      object RLLabel4: TRLLabel
        Left = 133
        Top = 17
        Width = 134
        Height = 14
        Align = faClientBottom
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        BeforePrint = RLLabel4BeforePrint
      end
      object RLLabel5: TRLLabel
        Left = 0
        Top = 17
        Width = 133
        Height = 14
        Align = faClientBottom
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        BeforePrint = RLLabel5BeforePrint
      end
    end
    object RLBand4: TRLBand
      Left = 8
      Top = 169
      Width = 267
      Height = 14
      BandType = btSummary
      Borders.Sides = sdCustom
      Borders.DrawLeft = False
      Borders.DrawTop = False
      Borders.DrawRight = False
      Borders.DrawBottom = True
      Borders.FixedBottom = True
    end
    object RLGroup1: TRLGroup
      Left = 8
      Top = 116
      Width = 267
      Height = 53
      DataFields = 'DESCRICAO1'
      object RLBand2: TRLBand
        Left = 0
        Top = 38
        Width = 267
        Height = 14
        AutoSize = True
        object RLDBText1: TRLDBText
          Left = 0
          Top = 0
          Width = 209
          Height = 14
          Align = faClientTop
          AutoSize = False
          DataField = 'DESCRICAO'
          DataSource = DataSource1
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Calibri'
          Font.Style = []
          Layout = tlCenter
          ParentFont = False
          Text = ''
        end
        object RLDBText2: TRLDBText
          Left = 209
          Top = 0
          Width = 58
          Height = 14
          Align = faRightTop
          Alignment = taRightJustify
          AutoSize = False
          DataField = 'QTD'
          DataSource = DataSource1
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Calibri'
          Font.Style = []
          Layout = tlCenter
          ParentFont = False
          Text = ''
        end
      end
      object RLBand3: TRLBand
        Left = 0
        Top = 22
        Width = 267
        Height = 16
        BandType = btTitle
        object RLLabel1: TRLLabel
          Left = 0
          Top = 0
          Width = 202
          Height = 14
          Align = faClientTop
          Caption = 'Descri'#231#227'o'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object RLLabel2: TRLLabel
          Left = 202
          Top = 0
          Width = 65
          Height = 14
          Align = faRightTop
          Caption = 'Quantidade'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
      end
      object RLBand5: TRLBand
        Left = 0
        Top = 0
        Width = 267
        Height = 22
        Margins.TopMargin = 2.000000000000000000
        AutoSize = True
        BandType = btTitle
        Borders.Sides = sdCustom
        Borders.DrawLeft = False
        Borders.DrawTop = False
        Borders.DrawRight = False
        Borders.DrawBottom = False
        object RLDBText3: TRLDBText
          Left = 0
          Top = 8
          Width = 267
          Height = 14
          Align = faTop
          DataField = 'DESCRICAO1'
          DataSource = DataSource1
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          Text = ''
        end
      end
    end
    object RLBand6: TRLBand
      Left = 8
      Top = 67
      Width = 267
      Height = 17
      BandType = btTitle
      object RLMemo2: TRLMemo
        Left = 0
        Top = 0
        Width = 267
        Height = 14
        Align = faTop
        Behavior = [beSiteExpander]
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        BeforePrint = RLMemo2BeforePrint
      end
    end
    object RLBand7: TRLBand
      Left = 8
      Top = 183
      Width = 267
      Height = 14
      BandType = btSummary
      Borders.Sides = sdCustom
      Borders.DrawLeft = False
      Borders.DrawTop = False
      Borders.DrawRight = False
      Borders.DrawBottom = False
      object RLSystemInfo1: TRLSystemInfo
        Left = 226
        Top = -1
        Width = 41
        Height = 13
        Alignment = taRightJustify
        Text = ''
        Transparent = False
      end
    end
    object RLBand8: TRLBand
      Left = 8
      Top = 0
      Width = 267
      Height = 67
      AutoSize = True
      BandType = btTitle
      BeforePrint = RLBand8BeforePrint
      object RLLabel7: TRLLabel
        Left = 0
        Top = 0
        Width = 267
        Height = 18
        Align = faTop
        Alignment = taCenter
        Caption = 'Nome Fantasia'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Layout = tlCenter
        ParentFont = False
      end
      object RLLabel8: TRLLabel
        Left = 0
        Top = 18
        Width = 267
        Height = 12
        Align = faTop
        Alignment = taCenter
        Caption = 'Raz'#195#163'o Social'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object RLLabel9: TRLLabel
        Left = 0
        Top = 30
        Width = 267
        Height = 12
        Align = faTop
        Alignment = taCenter
        Caption = 
          'CNPJ: 22.222.222/22222-22  IE:223.233.344.233 IM:2323.222.333.23' +
          '3'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        Layout = tlBottom
        ParentFont = False
      end
      object RLMemo1: TRLMemo
        Left = 0
        Top = 42
        Width = 267
        Height = 17
        Align = faTop
        Alignment = taCenter
        Behavior = [beSiteExpander]
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        Lines.Strings = (
          'Endere'#231'o')
        ParentFont = False
      end
      object RLDraw8: TRLDraw
        Left = 0
        Top = 59
        Width = 267
        Height = 8
        Align = faTop
        DrawKind = dkLine
        Pen.Width = 2
      end
    end
  end
  object IBQAdm1: TIBQuery
    Database = dmCupomFiscal.DataBase
    Transaction = dmCupomFiscal.Transaction
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    Left = 193
    Top = 48
  end
  object IBQRel: TIBQuery
    Database = dmCupomFiscal.DataBase
    Transaction = dmCupomFiscal.Transaction
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      
        'Select i.mercadoria, m.descricao,  f.nome, sum(i.qtd) as QTD,m.s' +
        'ubgrupo,sb.descricao'
      'from est_ecf_item i'
      'Left outer join est_ecf e on e.id = i.id_ecf'
      'left outer join est_mercadorias m on m.id = i.mercadoria'
      'Left outer join Funcionarios f on f.id = e.funcionario'
      'left outer join est_subgrupos sb on sb.id=m.subgrupo'
      'where (e.funcionario = :pFun)'
      'and (e.turno=:pTurno )'
      'and (e.data = :pData)'
      'group by 1,2,3,5,6'
      'order by 5,3,2')
    Left = 304
    Top = 224
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'pFun'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'pTurno'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'pData'
        ParamType = ptUnknown
      end>
    object IBQRelMERCADORIA: TIBStringField
      FieldName = 'MERCADORIA'
      Origin = '"EST_ECF_ITEM"."MERCADORIA"'
      Required = True
      Size = 13
    end
    object IBQRelNOME: TIBStringField
      FieldName = 'NOME'
      Origin = '"FUNCIONARIOS"."NOME"'
      Size = 40
    end
    object IBQRelQTD: TIBBCDField
      FieldName = 'QTD'
      ProviderFlags = []
      DisplayFormat = '##,##0.00'
      Precision = 18
      Size = 3
    end
    object IBQRelSUBGRUPO: TIBStringField
      FieldName = 'SUBGRUPO'
      Origin = '"EST_MERCADORIAS"."SUBGRUPO"'
      Size = 6
    end
    object IBQRelDESCRICAO1: TIBStringField
      FieldName = 'DESCRICAO1'
      Origin = '"EST_SUBGRUPOS"."DESCRICAO"'
      Size = 30
    end
    object IBQRelDESCRICAO: TIBStringField
      FieldName = 'DESCRICAO'
      Origin = '"EST_MERCADORIAS"."DESCRICAO"'
      Size = 100
    end
  end
  object DataSource1: TDataSource
    DataSet = IBQRel
    Left = 280
    Top = 312
  end
  object qLocal: TIBQuery
    Database = dmCupomFiscal.DataBase
    Transaction = dmCupomFiscal.Transaction
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      
        ' select lc.*,nf.NFCE_DANFE_IMPRESSORA as impressora,m.descricao ' +
        'as des_muni from locais lc'
      'left outer join ccustos cc on cc.local=lc.id'
      'left outer join locais_config_nfe nf on nf.local=lc.id'
      'left outer join municipios m on m.id=lc.municipio'
      ''
      'where cc.id=:pCC')
    Left = 528
    Top = 448
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'pCC'
        ParamType = ptUnknown
      end>
    object qLocalID: TIntegerField
      FieldName = 'ID'
      Origin = '"LOCAIS"."ID"'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object qLocalDESCRICAO: TIBStringField
      FieldName = 'DESCRICAO'
      Origin = '"LOCAIS"."DESCRICAO"'
      Required = True
      Size = 40
    end
    object qLocalCNPJ: TIBStringField
      FieldName = 'CNPJ'
      Origin = '"LOCAIS"."CNPJ"'
      Required = True
      Size = 18
    end
    object qLocalIE: TIBStringField
      FieldName = 'IE'
      Origin = '"LOCAIS"."IE"'
      Required = True
      Size = 14
    end
    object qLocalPAIS: TIntegerField
      FieldName = 'PAIS'
      Origin = '"LOCAIS"."PAIS"'
      Required = True
    end
    object qLocalMUNICIPIO: TIntegerField
      FieldName = 'MUNICIPIO'
      Origin = '"LOCAIS"."MUNICIPIO"'
      Required = True
    end
    object qLocalENDERECO: TIBStringField
      FieldName = 'ENDERECO'
      Origin = '"LOCAIS"."ENDERECO"'
      Required = True
      Size = 40
    end
    object qLocalBAIRRO: TIBStringField
      FieldName = 'BAIRRO'
      Origin = '"LOCAIS"."BAIRRO"'
      Required = True
      Size = 40
    end
    object qLocalCEP: TIBStringField
      FieldName = 'CEP'
      Origin = '"LOCAIS"."CEP"'
      Required = True
      Size = 9
    end
    object qLocalUF: TIBStringField
      FieldName = 'UF'
      Origin = '"LOCAIS"."UF"'
      FixedChar = True
      Size = 2
    end
    object qLocalFONE: TIBStringField
      FieldName = 'FONE'
      Origin = '"LOCAIS"."FONE"'
      Size = 15
    end
    object qLocalFAX: TIBStringField
      FieldName = 'FAX'
      Origin = '"LOCAIS"."FAX"'
      Size = 15
    end
    object qLocalNUMERO_END: TIBStringField
      FieldName = 'NUMERO_END'
      Origin = '"LOCAIS"."NUMERO_END"'
      Required = True
      Size = 6
    end
    object qLocalCOMPLEMENTO_END: TIBStringField
      FieldName = 'COMPLEMENTO_END'
      Origin = '"LOCAIS"."COMPLEMENTO_END"'
      Size = 40
    end
    object qLocalNOME_FANTASIA: TIBStringField
      FieldName = 'NOME_FANTASIA'
      Origin = '"LOCAIS"."NOME_FANTASIA"'
      Required = True
      Size = 40
    end
    object qLocalIM: TIBStringField
      FieldName = 'IM'
      Origin = '"LOCAIS"."IM"'
      Size = 15
    end
    object qLocalCNAE: TIBStringField
      FieldName = 'CNAE'
      Origin = '"LOCAIS"."CNAE"'
      Size = 7
    end
    object qLocalE_MAIL: TIBStringField
      FieldName = 'E_MAIL'
      Origin = '"LOCAIS"."E_MAIL"'
      Size = 40
    end
    object qLocalALI_SIMPLESN_ICM: TIBBCDField
      FieldName = 'ALI_SIMPLESN_ICM'
      Origin = '"LOCAIS"."ALI_SIMPLESN_ICM"'
      Precision = 9
      Size = 2
    end
    object qLocalCRT: TIntegerField
      FieldName = 'CRT'
      Origin = '"LOCAIS"."CRT"'
      Required = True
    end
    object qLocalCALCULA_PESO: TIBStringField
      FieldName = 'CALCULA_PESO'
      Origin = '"LOCAIS"."CALCULA_PESO"'
      Required = True
      FixedChar = True
      Size = 1
    end
    object qLocalDATA_EXP: TDateField
      FieldName = 'DATA_EXP'
      Origin = '"LOCAIS"."DATA_EXP"'
    end
    object qLocalANOMES: TIntegerField
      FieldName = 'ANOMES'
      Origin = '"LOCAIS"."ANOMES"'
    end
    object qLocalDATA_MOV: TDateField
      FieldName = 'DATA_MOV'
      Origin = '"LOCAIS"."DATA_MOV"'
    end
    object qLocalNFS_INICIAL: TIntegerField
      FieldName = 'NFS_INICIAL'
      Origin = '"LOCAIS"."NFS_INICIAL"'
    end
    object qLocalNFS_FINAL: TIntegerField
      FieldName = 'NFS_FINAL'
      Origin = '"LOCAIS"."NFS_FINAL"'
    end
    object qLocalNFS_AUTORIZACAO: TIBStringField
      FieldName = 'NFS_AUTORIZACAO'
      Origin = '"LOCAIS"."NFS_AUTORIZACAO"'
    end
    object qLocalCTE_RNTRC: TIBStringField
      FieldName = 'CTE_RNTRC'
      Origin = '"LOCAIS"."CTE_RNTRC"'
      Size = 8
    end
    object qLocalTURNO: TIBStringField
      FieldName = 'TURNO'
      Origin = '"LOCAIS"."TURNO"'
      Size = 2
    end
    object qLocalVLR_IRRF: TIBBCDField
      FieldName = 'VLR_IRRF'
      Origin = '"LOCAIS"."VLR_IRRF"'
      Precision = 18
      Size = 2
    end
    object qLocalANP_ARI: TIBStringField
      FieldName = 'ANP_ARI'
      Origin = '"LOCAIS"."ANP_ARI"'
      Size = 10
    end
    object qLocalDES_MUNI: TIBStringField
      FieldName = 'DES_MUNI'
      Origin = '"MUNICIPIOS"."DESCRICAO"'
      Size = 40
    end
    object qLocalIMPRESSORA: TIBStringField
      FieldName = 'IMPRESSORA'
      Origin = '"LOCAIS_CONFIG_NFE"."NFCE_DANFE_IMPRESSORA"'
      Size = 50
    end
  end
end
