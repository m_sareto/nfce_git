unit uFrmPesquisaPlaca;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Vcl.StdCtrls, Vcl.ComCtrls,
  Vcl.Grids, Vcl.DBGrids, Vcl.ExtCtrls, IBX.IBCustomDataSet, IBX.IBQuery;

type
  TFrmPesquisaPlaca = class(TForm)
    Bevel3: TBevel;
    Edit1: TEdit;
    CheckBox1: TCheckBox;
    DBGrid1: TDBGrid;
    StatusBar1: TStatusBar;
    Label32: TLabel;
    IBQuery1: TIBQuery;
    DataSource1: TDataSource;
    IBQuery1PLACA: TIBStringField;
    IBQuery1DESCRICAO: TIBStringField;
    procedure DBGrid1DblClick(Sender: TObject);
    procedure Edit1Change(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure DBGrid1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
    constructor Create(AOwner: TComponent);
    reintroduce;
  end;

var
  FrmPesquisaPlaca: TFrmPesquisaPlaca;

implementation

uses Math, uDMCupomFiscal, uFrmLoja, uFrmSupermercado, uFrmPrincipal,
  uFuncoes, uRotinasGlobais;

{$R *.dfm}

constructor TFrmPesquisaPlaca.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
end;

procedure TFrmPesquisaPlaca.DBGrid1DblClick(Sender: TObject);
begin
  if not (DataSource1.DataSet.IsEmpty) then
    ModalResult := mrOK;
end;

procedure TFrmPesquisaPlaca.DBGrid1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (key = vk_Return) and not (DataSource1.DataSet.IsEmpty) then
    ModalResult := mrOK;
end;

procedure TFrmPesquisaPlaca.Edit1Change(Sender: TObject);
begin
  if Trim(Edit1.Text)<>'' then
  begin
    IBQuery1.Active:=False;
    IBQuery1.SQL.Clear;
    IBQuery1.SQL.Add('Select M.Placa, M.DESCRICAO from CLIFOR_FROTA M');
    if CheckBox1.Checked then
    begin
      if(dmCupomFiscal.dbTemp_NFCECLIFOR.asInteger > 0)then
        IBQuery1.SQL.Add(' Where (M.Descricao >= :pDes) and (M.CLIFOR = :pClifor)')
      else
        IBQuery1.SQL.Add(' Where (M.Descricao >= :pDes)');
      IBQuery1.ParamByName('pDes').AsString:= Trim(Edit1.Text);
    end
    else
    begin
      if(dmCupomFiscal.dbTemp_NFCECLIFOR.asInteger > 0)then
        IBQuery1.SQL.Add(' Where (M.Descricao Like :pDes) and (M.CLIFOR = :pClifor)')
      else
        IBQuery1.SQL.Add(' Where (M.Descricao Like :pDes)');
      IBQuery1.ParamByName('pDes').AsString:= '%'+Trim(Edit1.Text)+'%';
    end;
    IBQuery1.SQL.Add(' Order By M.Descricao');
    if(dmCupomFiscal.dbTemp_NFCECLIFOR.asInteger > 0)then
      IBQuery1.ParamByName('pClifor').AsInteger:= dmCupomFiscal.dbTemp_NFCECLIFOR.asInteger;
    IBQuery1.Active:=True;
    IBQuery1.First;
  end;
end;

procedure TFrmPesquisaPlaca.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 case Key of
    vk_Escape: ModalResult := mrCancel;
    vk_F12: Edit1.SetFocus;
  end;

end;

procedure TFrmPesquisaPlaca.FormShow(Sender: TObject);
begin
  Edit1.SetFocus;
  CheckBox1.Checked := FRENTE_CAIXA.IniciePor;
end;

end.
