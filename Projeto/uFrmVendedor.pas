unit uFrmVendedor;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.Buttons,
  Data.DB, IBX.IBCustomDataSet, IBX.IBQuery;

type
  TfrmVendedor = class(TForm)
    pnlBotoes: TPanel;
    edtFunc: TEdit;
    edtFuncNome: TEdit;
    Label11: TLabel;
    btnOK: TBitBtn;
    btnCancelar: TBitBtn;
    procedure btnOKClick(Sender: TObject);
    procedure edtFuncExit(Sender: TObject);
    procedure edtFuncKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure Label11Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure edtFuncEnter(Sender: TObject);
  private
    procedure BuscarFuncionario;
  public
    { Public declarations }
  end;

var
  frmVendedor: TfrmVendedor;

implementation

uses
  uDmCupomFiscal, uFuncoes, uFrmPrincipal, uFrmPesquisa;

{$R *.dfm}

procedure TfrmVendedor.btnOKClick(Sender: TObject);
begin
  dmCupomFiscal.dbTemp_NFCE.Edit;
  dmCupomFiscal.dbTemp_NFCEFUNCIONARIO.AsInteger := StrToInt(edtFunc.Text);
  dmCupomFiscal.dbTemp_NFCE.Post;
  dmCupomFiscal.Transaction.CommitRetaining;
end;

procedure TfrmVendedor.BuscarFuncionario;
var
  lQryBusca: TIBQuery;
begin
  lQryBusca := TIBQuery.Create(nil);
  try
    lQryBusca.Database := dmCupomFiscal.DataBase;
    lQryBusca.SQL.Add('Select Nome from Funcionarios where ID = '+QuotedStr(edtFunc.Text));
    lQryBusca.Open;
    if not(lQryBusca.IsEmpty)Then
      edtFuncNome.Text := lQryBusca.FieldByName('Nome').AsString
    else
    begin
      edtFuncNome.Text := 'Funcion�rio n�o cadastrado';
      msgAviso('Funcion�rio n�o cadastrado', Application.Title);
      edtFunc.SetFocus;
    end;
    lQryBusca.Close;
  finally
    lQryBusca.Free;
  end;
end;

procedure TfrmVendedor.edtFuncEnter(Sender: TObject);
begin
  Cor_Campo(Sender, HexToTColor(PERSONALIZAR.corFundoFoco));
end;

procedure TfrmVendedor.edtFuncExit(Sender: TObject);
begin
  BuscarFuncionario;
end;

procedure TfrmVendedor.edtFuncKeyPress(Sender: TObject; var Key: Char);
begin
  if not(somenteNumeros(key))Then
    Key := #0;
end;

procedure TfrmVendedor.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if(key = VK_ESCAPE) Then btnCancelar.Click;

  if(key = VK_F12) and (edtFunc.Focused) Then Label11Click(Action);
end;

procedure TfrmVendedor.FormShow(Sender: TObject);
begin
  BuscarFuncionario;
  edtFunc.SetFocus;
end;

procedure TfrmVendedor.Label11Click(Sender: TObject);
begin
  frmPesquisa := tfrmPesquisa.Create(Self,'Select id, nome From Funcionarios Where (Nome >=:pTexto) and (Ativo=''S'') Order By Nome','Select Id, Nome From Funcionarios Where (Nome like :pTexto) and (Ativo=''S'') Order By Nome');

  frmPesquisa.DBGrid1.Columns.Add;
  frmPesquisa.DBGrid1.Columns[0].Title.Caption:='C�digo';
  frmPesquisa.DBGrid1.Columns[0].Width:=42;
  frmPesquisa.DBGrid1.Columns[0].FieldName:='id';

  frmPesquisa.DBGrid1.Columns.Add;
  frmPesquisa.DBGrid1.Columns[1].Title.Caption:='Nome';
  frmPesquisa.DBGrid1.Columns[1].Width:=337;
  frmPesquisa.DBGrid1.Columns[1].FieldName:='Nome';
  try
    if (frmPesquisa.ShowModal=mrOK) then
      edtFunc.Text := FrmPesquisa.IBQuery1.FieldByName('id').AsString;
  finally
    frmPesquisa.Free;
  end;

end;

end.
