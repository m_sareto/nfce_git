unit uFrmTefCancelamento;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Datasnap.DBClient,
  Vcl.StdCtrls, System.Actions, Vcl.ActnList, Vcl.Buttons, PngBitBtn,
  Vcl.ExtCtrls, Vcl.Grids, Vcl.DBGrids, System.ImageList, Vcl.ImgList;

type
  TFrmTefCancelamento = class(TForm)
    DBGrid1: TDBGrid;
    Panel1: TPanel;
    edtCodCancelamento: TEdit;
    PngBitBtn1: TPngBitBtn;
    PngBitBtn2: TPngBitBtn;
    ActionList1: TActionList;
    actCancelar: TAction;
    actSair: TAction;
    Label1: TLabel;
    Label2: TLabel;
    cdsDados: TClientDataSet;
    cdsDadosNSU: TStringField;
    cdsDadosREDE: TStringField;
    cdsDadosVALOR: TFloatField;
    cdsDadosCODIGO: TIntegerField;
    cdsDadosOPERACAO: TIntegerField;
    dsDados: TDataSource;
    cdsDadosEL_CHAVE: TIntegerField;
    cdsDadosCAUTORIZACAO: TIntegerField;
    imgListaStatusNFe: TImageList;
    cdsDadosMCUPOMREDUZIDO: TBlobField;
    procedure FormShow(Sender: TObject);
    procedure actCancelarExecute(Sender: TObject);
    procedure DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure actSairExecute(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure PngBitBtn1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure PngBitBtn2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmTefCancelamento: TFrmTefCancelamento;

implementation

Uses
  UFrmFim, uDMComponentes, uDMCupomFiscal, ufrmImpTefComprovantes;

{$R *.dfm}

procedure TFrmTefCancelamento.actCancelarExecute(Sender: TObject);
var
  Comp : TfrmImpTefComprovantes;
begin
  if(trim(edtCodCancelamento.Text) <> '')Then
  begin
    if(cdsDados.Locate('CODIGO',StrToInt(edtCodCancelamento.Text), [loCaseInsensitive]))then
    begin
      if(cdsDadosOPERACAO.Value = 1)then
      begin
{        Comp := TFrmImpTefComprovantes.create(Self);
        try
          Comp.Memo1.Lines.Text := cdsDadosMCUPOMREDUZIDO.AsString;
          if(Comp.rlVenda.Prepare)then
            Comp.rlVenda.Print;
        finally
          Comp.Free;
        end;}
        if (DMComponentes.ACBrTEFD1.CNC(cdsDadosREDE.asString,cdsDadosNSU.asString,
          Now,cdsDadosVALOR.AsFloat))then
        begin
          DMComponentes.ACBrTEFD1.ConfirmarTransacoesPendentes;
          Sleep(1000);
          dmCupomFiscal.IBSQL1.Close;
          dmCupomFiscal.IBSQL1.SQL.Clear;
          dmCupomFiscal.IBSQL1.SQL.Add('Update TEF_MOVIMENTO set OPERACAO = ''51'' where '+
                                       'REDE = :pREDE and NSU= :pNSU');
          dmCupomFiscal.IBSQL1.ParamByName('pREDE').AsString := cdsDadosREDE.asString;
          dmCupomFiscal.IBSQL1.ParamByName('pNSU').asString  := cdsDadosNSU.asString;
          dmCupomFiscal.IBSQL1.ExecQuery;
          dmCupomFiscal.IBSQL1.Close;
          dmCupomFiscal.Transaction.CommitRetaining;
          if(dmCupomFiscal.dbTemp_NFCEEL_CHAVE.Value = cdsDadosEL_CHAVE.AsInteger)then
          begin
            dmCupomFiscal.dbTemp_NFCE_FormaPag.Edit;
            dmCupomFiscal.dbTemp_NFCE_FormaPagVPAG.Value :=
              dmCupomFiscal.TefGetValorFormaPag(dmCupomFiscal.dbQuery1.FieldByName('FORMA_PAG').AsInteger,
              dmCupomFiscal.dbQuery1.FieldByName('DOCFISCAL').AsInteger);
            dmCupomFiscal.dbTemp_NFCE_FormaPag.Post;
            dmCupomFiscal.Transaction.CommitRetaining;
          end;
          cdsDados.Edit;
          cdsDadosOPERACAO.Value := 51;
          cdsDados.Post;
          dmCupomFiscal.Transaction.CommitRetaining;
          edtCodCancelamento.SetFocus;
          edtCodCancelamento.SelectAll;
        end;
      end;
    end
    else
    begin
      MessageDlg('C�digo n�o encontrado',mtInformation,[mbOK],0);
    end;
  end;
end;

procedure TFrmTefCancelamento.actSairExecute(Sender: TObject);
begin
  ModalResult := mrClose;
end;

procedure TFrmTefCancelamento.DBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if not(cdsDados.IsEmpty)then
  begin
    if Column.Index=0 then
    begin
      DBGrid1.Canvas.FillRect(Rect);
      case cdsDadosOPERACAO.AsInteger of
        1: imgListaStatusNFe.Draw(DBGrid1.Canvas,Rect.Left+01,Rect.Top+1,1);               //Inutilizado
        51: imgListaStatusNFe.Draw(DBGrid1.Canvas,Rect.Left+01,Rect.Top+1,0);   //Cancelado
      end;
    end;
  end;
end;

procedure TFrmTefCancelamento.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if(key = VK_ESCAPE)then actSair.Execute;
end;

procedure TFrmTefCancelamento.FormShow(Sender: TObject);
begin
  edtCodCancelamento.SetFocus;
end;

procedure TFrmTefCancelamento.PngBitBtn1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if(Key = VK_RETURN)then actCancelar.Execute;
end;

procedure TFrmTefCancelamento.PngBitBtn2KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if(Key = VK_RETURN)then actSair.Execute;
end;

end.
