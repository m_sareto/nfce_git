{http://acbr.sourceforge.net/drupal/?q=node/24 }

unit uFrmPosto;

interface

uses
  Windows, Messages, Variants, DBCtrls, DBClient, Grids, Math,
  AppEvnts, SysUtils, Forms, Types, Classes, ACBrLCB, ACBrBase, StdCtrls, StrUtils,
  Controls, ExtCtrls, ACBrUtil, ACBrECFBematech, TypInfo, ActiveX, MSHTML, IniFiles,
  DB, OleCtrls, SHDocVw, DBGrids, Buttons, Tredit,

  ACBrPAFClass, ACBrRFD, ACBrDevice, ACBrECFClass, ACBrConsts,ACBrECF,
  {$IFDEF Delphi6_UP} StrUtils, DateUtils, Types, {$ELSE} FileCtrl,{$ENDIF}
  Graphics,
  Dialogs, ComCtrls, Menus, Spin, jpeg
  {$IFDEF Delphi7},XPMan{$ENDIF}, ACBrAAC, Mask, IBDatabase,
  IBCustomDataSet, IBQuery, ACBrValidador, ToolWin, ImgList, System.ImageList;

  //Abrir gaveta Epson
  Function IniciaPorta(pszPorta:PAnsiChar):Integer; StdCall; External 'InterfaceEpsonNF.dll';
  function AcionaGaveta():Integer; StdCall; External 'InterfaceEpsonNF.dll';
  function FechaPorta():Integer; StdCall; External 'InterfaceEpsonNF.dll';
  function Le_Status():Integer; StdCall; External 'InterfaceEpsonNF.dll';

type
  TfrmPosto = class(TForm)
    barStatus: TStatusBar;
    pnlRodape: TPanel;
    DsItem: TDataSource;
    ApplicationEvents1: TApplicationEvents;
    pnlDadosIF: TPanel;
    pnlBotoes: TPanel;
    pnlFundo: TPanel;
    pnlBackground: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label6: TLabel;
    Label10: TLabel;
    la_Estoque: TLabel;
    Label7: TLabel;
    Label9: TLabel;
    Label11: TLabel;
    Label4: TLabel;
    Label13: TLabel;
    lblDescPerc: TLabel;
    edtMercadoria: TEdit;
    reTotal: TRealEdit;
    btnVende: TBitBtn;
    btnCancela: TBitBtn;
    reVlrUnitario: TRealEdit;
    reSubtotal: TRealEdit;
    reVlrDesconto: TRealEdit;
    edtBico: TEdit;
    edtCliente: TEdit;
    edtClienteNome: TEdit;
    BitBtn3: TBitBtn;
    Panel2: TPanel;
    DBGrid1: TDBGrid;
    mBobina: TMemo;
    wbBobina: TWebBrowser;
    reQuantidade: TRealEdit;
    pnlAbastecidas: TPanel;
    pnlCabecalho: TPanel;
    grdAbastecidas: TDBGrid;
    dsAbastecidas: TDataSource;
    qurAbastecidas: TIBQuery;
    qurAbastecidasID: TIntegerField;
    qurAbastecidasMERCADORIA: TIBStringField;
    qurAbastecidasPRECO: TIBBCDField;
    qurAbastecidasLITRO: TIBBCDField;
    qurAbastecidasDINHEIRO: TIBBCDField;
    qurAbastecidasBICO: TIntegerField;
    qurAbastecidasABASTECIDA: TIntegerField;
    qurAbastecidasPAGO: TIBStringField;
    qurAbastecidasCLIFOR: TIntegerField;
    qurAbastecidasDATA_HORA: TDateTimeField;
    qurAbastecidasFRENTISTA: TIBStringField;
    cdsAbastecidas: TClientDataSet;
    cdsAbastecidasID: TIntegerField;
    cdsAbastecidasMERCADORIA: TStringField;
    cdsAbastecidasPRECO: TBCDField;
    cdsAbastecidasLITRO: TBCDField;
    cdsAbastecidasDINHEIRO: TBCDField;
    cdsAbastecidasBICO: TIntegerField;
    cdsAbastecidasTEMP_ECF_ITEM: TBooleanField;
    cdsAbastecidasCLIFOR: TIntegerField;
    cdsAbastecidasDATA_HORA: TDateTimeField;
    cdsAbastecidasPAGO: TBooleanField;
    cdsAbastecidasABASTECIDA: TIntegerField;
    cdsAbastecidasFRENTISTA: TStringField;
    imgListAbastecidas: TImageList;
    pnlBotoesOrganiza: TPanel;
    btnFinalizarCupom: TSpeedButton;
    btnCancelarCupom: TSpeedButton;
    btnCancelarItem: TSpeedButton;
    btnPedido: TSpeedButton;
    btnGaveta: TSpeedButton;
    btnConsultarCR: TSpeedButton;
    btnLerAbastecidas: TSpeedButton;
    btnFechamentoDiario: TSpeedButton;
    btnConsultaPreco: TSpeedButton;
    btnPesquisa: TSpeedButton;
    btnSair: TSpeedButton;
    pnlDadosIFOrganiza: TPanel;
    lblOperador: TLabel;
    lblNumCaixa: TLabel;
    lblCCusto: TLabel;
    lblTurno: TLabel;
    lblData: TLabel;
    ppmAbastecidas: TPopupMenu;
    UsoeConsumo1: TMenuItem;
    tmrScroll: TTimer;
    pnlMensagem: TPanel;
    txtMensagem: TLabel;
    dsEcf: TDataSource;
    btnConsumidor: TSpeedButton;
    qurAbastecidasMERC_DESCRICAO: TIBStringField;
    cdsAbastecidasMERC_DESCRICAO: TStringField;
    lblCusto: TLabel;
    procedure edtMercadoriaExit(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure edtMercadoriaEnter(Sender: TObject);
    procedure reVlrUnitarioExit(Sender: TObject);
    procedure btnCancelaClick(Sender: TObject);
    procedure reVlrDescontoExit(Sender: TObject);
    procedure btnVendeClick(Sender: TObject);
    procedure reQuantidadeEnter(Sender: TObject);
    procedure Label1Click(Sender: TObject);
    procedure subtotal;
    procedure edtBicoExit(Sender: TObject);
    procedure Mer_Bico;
    procedure edtClienteExit(Sender: TObject);
    procedure edtClienteEnter(Sender: TObject);
    procedure Label11Click(Sender: TObject);
    procedure reSubtotalExit(Sender: TObject);
    procedure tratarBotoes;
    Procedure FormataQtd;
    Procedure FormataVlrUni;
    Procedure LimpaCampos;

    procedure Label13Click(Sender: TObject);
    procedure DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure edtClienteClick(Sender: TObject);

    procedure BloqueiaVenda;
    procedure LiberaVenda;
    procedure ApplicationEvents1Hint(Sender: TObject);
    procedure btnSairClick(Sender: TObject);
    procedure btnFechamentoDiarioClick(Sender: TObject);
    procedure btnLerAbastecidasClick(Sender: TObject);
    procedure btnCancelarItemClick(Sender: TObject);
    procedure btnCancelarCupomClick(Sender: TObject);
    procedure btnFinalizarCupomClick(Sender: TObject);
    procedure edtCCustoClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnConsultarCRClick(Sender: TObject);
    procedure btnConsultaPrecoClick(Sender: TObject);
    procedure btnPesquisaClick(Sender: TObject);
    procedure reQuantidadeExit(Sender: TObject);
    procedure edtBicoEnter(Sender: TObject);
    procedure reVlrUnitarioEnter(Sender: TObject);
    procedure reVlrDescontoEnter(Sender: TObject);
    procedure reSubtotalEnter(Sender: TObject);
    procedure btnPedidoClick(Sender: TObject);
    procedure btnGavetaClick(Sender: TObject);
    procedure getAbastecidas;
    procedure grdAbastecidasCellClick(Column: TColumn);
    procedure grdAbastecidasDblClick(Sender: TObject);
    procedure grdAbastecidasDrawColumnCell(Sender: TObject;
      const Rect: TRect; DataCol: Integer; Column: TColumn;
      State: TGridDrawState);
    procedure edtBicoKeyPress(Sender: TObject; var Key: Char);
    procedure BuscaMercadoria();
    procedure UsoeConsumo1Click(Sender: TObject);
    procedure tmrScrollTimer(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure DsItemDataChange(Sender: TObject; Field: TField);
    procedure dsEcfDataChange(Sender: TObject; Field: TField);
    procedure btnConsumidorClick(Sender: TObject);
    procedure ppmAbastecidasPopup(Sender: TObject);
    procedure edtMercadoriaChange(Sender: TObject);

  private
    ncm : String;
    vlrDesconto : Currency;
    { Private declarations }
    procedure arrendarCampos();
    procedure setCores();

  public
    { Public declarations }
      Sender_Libera:String;
      Abastecimento, ELChavePedido:Integer;
      sVlr:Real;
  end;


var
  frmPosto: TfrmPosto;

implementation

uses uFrmPesquisaMercadoria, uDMCupomFiscal, uFrmCancelaItem,
  uFrmPesquisaCliente, UFrmFim, uFrmConsultaCR, uFrmAbastecidas,
  uFrmConsultaExtrato, uFrmFechamentoAbastecidas, uFuncoes,
  uFrmConfigurarSerial, uDMComponentes, uFrmPrincipal,
  uFrmConfiguracao, uRotinasGlobais, uFrmConsultaLimiteCR,
  UfrmPesquisaCodigo, uFrmImportaPedido, uFrmLogin, uFrmNFCe,
  uFrmAbastecidaCliente, uFrmAjustarNcm, uFrmClifor;

{$R *.dfm}

Procedure TfrmPosto.formataQtd;
begin
  Case FRENTE_CAIXA.Casas_Dec_Quantidade of
    1:begin
      reQuantidade.FormatSize:='10.1';
      reQuantidade.Text:=FormatFloat('##0.0',reQuantidade.value);
    end;
    2:begin
      reQuantidade.FormatSize:='10.2';
      reQuantidade.Text:=FormatFloat('##0.00',reQuantidade.value);
    end;
    3:begin
      reQuantidade.FormatSize:='10.3';
      reQuantidade.Text:=FormatFloat('##0.000',reQuantidade.value);
    end;
    4:begin
      reQuantidade.FormatSize:='10.4';
      reQuantidade.Text:=FormatFloat('##0.0000',reQuantidade.value);
    end;
  end;
end;

Procedure TfrmPosto.formataVlrUni;
begin
  Case FRENTE_CAIXA.Casas_Dec_Valor of
    1:begin
      reVlrUnitario.FormatSize:='10.1';
      reVlrUnitario.Text:=FormatFloat('##0.0',reVlrUnitario.value);
    end;
    2:begin
      reVlrUnitario.FormatSize:='10.2';
      reVlrUnitario.Text:=FormatFloat('##0.00',reVlrUnitario.value);
    end;
    3:begin
      reVlrUnitario.FormatSize:='10.3';
      reVlrUnitario.Text:=FormatFloat('##0.000',reVlrUnitario.value);
    end;
    4:begin
      reVlrUnitario.FormatSize:='10.4';
      reVlrUnitario.Text:=FormatFloat('##0.0000',reVlrUnitario.value);
    end;
  end;
end;

procedure TfrmPosto.edtMercadoriaExit(Sender: TObject);
begin
  Cor_Campo(Sender, HexToTColor(PERSONALIZAR.corFundoCampo));
  If (Length(Trim(edtMercadoria.Text)) > 13) or (Trim(edtMercadoria.Text)='') then
  begin
    Mer_Bico;
  end
  else
  begin
    DMCupomFiscal.dbQuery1.Active:=False;
    DMCupomFiscal.dbQuery1.SQL.Clear;
    DMCupomFiscal.dbQuery1.SQL.Add('SELECT M.ID, M.Unidade, M.CST_Pis, M.CST_Cofins, M.Descricao, M.Sucinto, M.Venda, M.Ativo, M.servico,'+
                            ' M.ECF_CST_ICM, M.ECF_CFOP, M.NCM, M.SUBTIPO_ITEM, M.Desconto,'+
                            ' M.SM_Venda_Promocao, M.Custo_Ultimo, M.Custo_Medio, M.ICM_ECF, M.ISSQN, ME.Est_Atual, MCF.Venda MCF_Venda,'+
                            ' m.ID_Codigo, m.Estoque_Vinculado'+
                            ' FROM Est_Mercadorias M'+
                            ' Left Outer Join Est_Mercadorias_Estoque ME on ME.Mercadoria=M.Id and Me.CCusto=:pCC'+
                            ' Left Outer Join Est_Mercadorias_CliFor MCF on MCF.Mercadoria=M.Id and MCF.CliFor=:pCF'+
                            ' WHERE (M.ID=:pCodigo)');
    DMCupomFiscal.dbQuery1.ParamByName('pCodigo').AsString:=Trim(edtMercadoria.Text);
    DMCupomFiscal.dbQuery1.ParamByName('pCC').AsInteger:=CCUSTO.codigo;
    DMCupomFiscal.dbQuery1.ParamByName('pCF').AsInteger:=StrToIntDef(edtCliente.Text,0);
    DMCupomFiscal.dbQuery1.Active:=True;
    DMCupomFiscal.dbQuery1.First;
    If not(DMCupomFiscal.dbQuery1.Eof) then
    begin
      //Verifica se mercadoria est� Ativa
      If DMCupomFiscal.dbQuery1.FieldByName('Ativo').AsString='N' then
      begin
        msgInformacao('Mercadoria est� Inativa. Favor verificar cadastro','Aten��o');
        edtMercadoria.Clear;
        Mer_Bico;
      end
      else
      begin
        ncm := DMCupomFiscal.dbQuery1.FieldByName('NCM').AsString;
        If not (frmPrincipal.Valida_NCM(ncm)) then
        begin
          if not(msgPergunta('NCM informado na mercadoria n�o existe na base de dados.'+#13+
                             'A partir de 01/01/2016 o NCM obrigatoriamente deve'+#13+
                             'existir na base de dados disponibilizada pelo governo.'+#13+#13+
                             'NCM: '+ncm+#13+
                             'Deseja ajustar o cadastro da mercadoria e tentar novamente?'
                             ,Application.Title))then
          begin
            edtMercadoria.SetFocus;
            Abort;
          end
          else
          begin
            try
              frmAjustarNcm := TfrmAjustarNcm.Create(Self, dmCupomFiscal.dbQuery1.fieldByName('NCM').asString,
                               dmCupomFiscal.dbQuery1.fieldByName('Descricao').asString,
                               dmCupomFiscal.dbQuery1.fieldByName('ID').asString);
              case frmAjustarNcm.ShowModal of
              mrOK:begin //se OK ent�o cliente atualizou o NCM
                  if(frmAjustarNcm.yNCM_NOVO <> dmCupomFiscal.dbQuery1.fieldByName('NCM').asString)then
                    ncm := frmAjustarNcm.yNCM_NOVO;
                end;
              mrCancel:begin
                  edtMercadoria.SetFocus;
                  Abort;
                end;
              mrAbort:begin
                  edtMercadoria.SetFocus;
                  Abort;
                end
              end;
            finally
              frmAjustarNcm.Free;
            end;
          end;
        end;

        la_Estoque.Caption:=FormatFloat('###,##0.000',DMCupomFiscal.dbQuery1.FieldByName('Est_Atual').AsCurrency);
        MERCADORIA.subtipo_item := DMCupomFiscal.dbQuery1.FieldByName('subtipo_item').AsString;
        MERCADORIA.servico := dmCupomFiscal.dbQuery1.FieldByName('servico').AsString='S';
        MERCADORIA.descricao := dmCupomFiscal.dbQuery1.FieldByName('Descricao').AsString;
        MERCADORIA.Est_Vinculado := dmCupomFiscal.dbQuery1.FieldByName('Estoque_Vinculado').AsString;
        //Verifica se Estoque n�o esta nulo
        if DMCupomFiscal.dbQuery1.FieldByName('Est_Atual').IsNull then
        begin
          Mer_Bico;
          msgInformacao('Mercadoria n�o possui registro de estoque para centro de custo '+IntToStr(CCUSTO.codigo),'');
        end
        else
        begin
          //Verifica o Estoque - Centro de Custo Permite venda com Estoque Negativo?
          MERCADORIA.descricao := dmCupomFiscal.dbQuery1.FieldByName('Descricao').AsString;
          If not(MERCADORIA.servico) and not(verificarEstoque(Trim(edtMercadoria.Text),
            MERCADORIA.Est_Vinculado,
            MERCADORIA.subtipo_item, DMCupomFiscal.dbQuery1.FieldByName('Est_Atual').AsCurrency,
            reQuantidade.Value)) Then
          begin
            Mer_Bico;
          end
          else
          begin
            //Mercadoria  com Pre�o Exclusivo para Cliente
            If DMCupomFiscal.dbQuery1.FieldByName('MCF_Venda').AsCurrency <> 0 then
            begin
              if(DMCupomFiscal.dbQuery1.FieldByName('MCF_Venda').AsCurrency > 0)Then
                reVlrUnitario.Text:=FormatFloat('#,##0.000',DMCupomFiscal.dbQuery1.FieldByName('MCF_Venda').AsCurrency)
              else
              begin
                if(trim(edtBico.Text) = '')Then
                  reVlrUnitario.Text:=FormatFloat('#,##0.000',DMCupomFiscal.dbQuery1.FieldByName('Venda').AsCurrency -
                    ((DMCupomFiscal.dbQuery1.FieldByName('Venda').AsCurrency * (DMCupomFiscal.dbQuery1.FieldByName('MCF_Venda').AsCurrency*-1))/100))
                else
                  reVlrUnitario.Text:=FormatFloat('#,##0.000',reVlrUnitario.Value -
                    ((reVlrUnitario.Value * (DMCupomFiscal.dbQuery1.FieldByName('MCF_Venda').AsCurrency*-1))/100));
              end;
            end
            else
            begin
              //Pre�o de Tabela
              If (reVlrUnitario.Value=0) and (TAB_PRECO.TabPreco>0) then
              begin
                DMCupomFiscal.dbQuery2.Active:=False;
                DMCupomFiscal.dbQuery2.SQL.Clear;
                DMCupomFiscal.dbQuery2.SQL.Add('Select Vlr_Venda From Est_Mercadorias_Preco WHERE (Mercadoria=:pMer) and (TPreco=:pTP)');
                DMCupomFiscal.dbQuery2.ParamByName('pMer').AsString:=Trim(edtMercadoria.Text);
                DMCupomFiscal.dbQuery2.ParamByName('pTp').AsInteger:=TAB_PRECO.TabPreco;
                DMCupomFiscal.dbQuery2.Active:=True;
                DMCupomFiscal.dbQuery2.First;
                if DMCupomFiscal.dbQuery2.FieldByName('Vlr_Venda').AsCurrency>0 then
                  reVlrUnitario.Value := DMCupomFiscal.dbQuery2.FieldByName('Vlr_Venda').AsCurrency
                Else
                begin
                  //reVlrUnitario.Text:=FormatFloat('#,##0.0000',DMCupomFiscal.dbQuery1.FieldByName('Venda').AsCurrency);
                  reVlrUnitario.Value:=DMCupomFiscal.dbQuery1.FieldByName('Venda').AsCurrency;
                  msgInformacao('Cliente possui tabela de Pre�o Especial.'+#13+
                             'Mercadoria n�o possui Pre�o de Venda para esta Tabela.'+#13+
                             'Sera sugerido o valor NORMAL de venda da Mercadoria','Aviso');
                end;
              end
              else
              begin
                // Preco Normal
                If (Trim(edtBico.Text)='')then
                  reVlrUnitario.Value:=DMCupomFiscal.dbQuery1.FieldByName('Venda').AsCurrency;
              end;
            end;

            MERCADORIA.codigo     := Trim(edtMercadoria.Text);
            MERCADORIA.descricao  := DMCupomFiscal.dbQuery1.FieldByName('descricao').AsString;
            edtMercadoria.Text    := MERCADORIA.descricao;
            MERCADORIA.unidade    := DMCupomFiscal.dbQuery1.FieldByName('Unidade').AsString;
            MERCADORIA.icms       := DMCupomFiscal.dbQuery1.FieldByName('ICM_ECF').AsString;
            MERCADORIA.aliqISS    := DMCupomFiscal.dbQuery1.FieldByName('ISSQN').AsCurrency;
            MERCADORIA.cstPIS     := DMCupomFiscal.dbQuery1.FieldByName('Cst_Pis').AsString;
            MERCADORIA.cstCOFINS  := DMCupomFiscal.dbQuery1.FieldByName('Cst_Cofins').AsString;
            MERCADORIA.estoque    := DMCupomFiscal.dbQuery1.FieldByName('Est_Atual').AsCurrency;
            MERCADORIA.cstICMS    := DMCupomFiscal.dbQuery1.FieldByName('ECF_CST_ICM').AsString;
            MERCADORIA.cfop       := DMCupomFiscal.dbQuery1.FieldByName('ECF_CFOP').AsInteger;
            MERCADORIA.ncm        := ncm;
            MERCADORIA.percDescontoMax := DMCupomFiscal.dbQuery1.FieldByName('desconto').AsCurrency;
            if(FRENTE_CAIXA.Exibir_Vlr_Custo)then
              lblCusto.Caption := FormatFloat('R$ ###,##0.000', DMCupomFiscal.dbQuery1.FieldByName('Custo_Ultimo').AsCurrency);
            if Abastecimento = 0 then //Indica que esta sendo informado manualmente
            begin
              if not(POSTO.VenderSemAbastecida) then //Nao permite vender sem abastecida?
              begin
                if MERCADORIA.subtipo_item = 'CO' then
                begin
                  msgInformacao('N�o � poss�vel efetuar venda de combust�vel sem abastecimento !','Aten��o');
                  edtMercadoria.Clear;
                  Mer_Bico;
                  Abort;
                end;
              end
              else
              begin
                if((POSTO.AutorizacaoSemAbastecida))then
                begin
                  if MERCADORIA.subtipo_item = 'CO' then
                  begin
                    if msgPergunta('Deseja efetuar venda de combust�vel sem abastecimento?','.::Abastecidas::.') then
                    begin
                      if not getAutorizacaoIncondicional('Combustivel sem Abastecida', 'Autoriza��o para venda de combust�vel sem abastecida', 0,0) then
                      begin
                        msgInformacao('N�o � poss�vel efetuar venda de combust�vel sem abastecimento !','Aten��o');
                        edtMercadoria.Clear;
                        Mer_Bico;
                        Abort;
                      end;
                    end
                    else
                    begin
                      msgInformacao('N�o � poss�vel efetuar venda de combust�vel sem abastecimento !','Aten��o');
                      edtMercadoria.Clear;
                      Mer_Bico;
                      Abort;
                    end;
                  end;
                end;
              end;
              reQuantidade.Enabled := True;
            end;
          end;
        end;
      end;
    end
    Else
    begin
      msgInformacao('Mercadoria n�o cadastrada. Favor verificar!','Aten��o');
      Mer_Bico;
    end;
  end;
end;

procedure TfrmPosto.SubTotal;
begin
  //Verifica se tem estoque disponivel caso n�o permite venda com estoque Zero
  If not(MERCADORIA.servico) and not (verificarEstoque(MERCADORIA.codigo,
    MERCADORIA.Est_Vinculado, //Lauro 11.07.17
    MERCADORIA.subtipo_item, MERCADORIA.estoque, reQuantidade.Value)) Then
  begin
    reQuantidade.SetFocus;
  end
  else
  begin
    //Arredondar/Truncar Quantidade
    If FRENTE_CAIXA.Truncar then
    begin
      reQuantidade.Value := TruncarDecimal(reQuantidade.Value, FRENTE_CAIXA.Casas_Dec_Quantidade);
      sVlr := (reQuantidade.Value * reVlrUnitario.Value);
      reSubtotal.Value := TruncarDecimal(sVlr,2);
    end
    Else
    begin
      reQuantidade.Value := ArredondaDecimal(reQuantidade.Value, FRENTE_CAIXA.Casas_Dec_Quantidade);
      sVlr := (reQuantidade.Value * reVlrUnitario.Value);
      reSubtotal.Value := ArredondaDecimal(sVlr,2);
    end;

    FormataQtd; //Fomata casa decimais na quantidade;
    FormataVlrUni; //Fomata casa decimais Valor Unitario

    //Diminui valor de Desconto
    If reVlrDesconto.Value > 0 then  // verr
      reSubtotal.Value := reSubtotal.Value - reVlrDesconto.Value;

    reVlrDesconto.Text:=FormatFloat('##0.00',reVlrDesconto.value);
    reSubtotal.Text:=FormatFloat('#,##0.00',reSubtotal.value);

    tratarBotoes;
    //reVlrDesconto.Value:=0;
  end;
end;

procedure TfrmPosto.tmrScrollTimer(Sender: TObject);
begin
  txtMensagem.Left := txtMensagem.Left - 1;
  if(txtMensagem.Left <= pnlMensagem.Left - txtMensagem.Width)Then
    txtMensagem.Left := pnlMensagem.Width;
end;

procedure TfrmPosto.tratarBotoes;
begin
  if reSubtotal.Value > 0 then
  begin
    btnVende.Enabled:=True;
    btnCancela.Enabled:=True;
  end
  Else
  begin
    btnVende.Enabled:=False;
    btnCancela.Enabled:=True;
  end;
end;

procedure TfrmPosto.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  //Fechar Tela
  if (Key=vk_escape) then
  begin
    btnSair.Click;
  end;

  //Finalizar Cupom - F1
  if (Key=Vk_F1) then
  begin
    btnFinalizarCupom.Click;
  end;

  //Cancelar Cupom - F2
  if (Key=Vk_F2) then
  begin
    btnCancelarCupom.Click;
  end;

  //Cancelar Item - F3
  if (Key=Vk_F3) then
  begin
    btnCancelarItem.Click;
  end;

  //Pedido - F4
  if (Key=Vk_F4) then
  begin
    btnPedido.Click;
  end;

  //Abrir Gaveta - F5
  if (Key=Vk_F5) then
  begin
    btnGaveta.Click;
  end;

  //Consultar CR - F6
  if (Key=Vk_F6) then
  begin
    btnConsultarCR.Click;
  end;

  //Pesquisa Abastecidas - F7
  If (key=vk_F7) then
    btnLerAbastecidas.Click;

  //Fechamento Diario - F8
  If (key=vk_F8) and (btnFechamentoDiario.Enabled) then
    btnFechamentoDiario.Click;

  //Pesquisa Mercadorias - F12
  If (key=vk_F9) then
  begin
    btnConsumidor.Click;
  end;

  //Consulta de Preco - F11
  If (key=vk_F11) then
    btnConsultaPreco.Click;

  //Pesquisa Mercadorias - F12
  If (key=vk_F12) then
  begin
    btnPesquisa.Click;
  end;

  //Alterar Operador - Ctrol + O
  if (Shift = [ssCtrl]) then
  begin
    //Saber c�digo da Tecla
    //ShowMessage(Format('O c�digo da tecla pressionada �: %d', [Key]));
    if Key=79 then
    begin
      if reTotal.Value > 0 then
      begin
        msgInformacao('N�o � poss�vel alterar o Operador ap�s iniciar a venda','');
      end
      else
      begin
        setCaixaUso(LOGIN.usuarioCod, 'N');
        frmLogin:=TfrmLogin.create(Application);
        frmLogin.ShowModal;
        frmLogin.Free;
        If LOGIN.usuarioCod  <=0 then
        begin
          msgErro('Operador n�o identificado','');
          Close;
        end
        else
        begin
          setDadosRodape;
        end;
      end;
    end;
  end;
end;

procedure TfrmPosto.FormResize(Sender: TObject);
begin
  pnlBotoesOrganiza.Left:=Trunc((frmPosto.Width/2)-(pnlBotoesOrganiza.Width/2)+(btnPedido.Width/2));
  pnlDadosIFOrganiza.Left:=Trunc((frmPosto.Width/2)-(pnlDadosIFOrganiza.Width/2));
//lauro 20.01.17  Application.ProcessMessages; //Removido ficava a tela finalizar aberta no F1 quando dava erro(ceriticao....)
  if(pnlAbastecidas.Visible)then
  begin
    pnlAbastecidas.Left := Trunc((pnlFundo.Width/2)-(pnlBackground.Width/2)-(pnlAbastecidas.Width/2));
    pnlBackground.Left:= Trunc((pnlFundo.Width/2)-(pnlBackground.Width/2)+(pnlAbastecidas.Width/2))
  end
  else
    pnlBackground.Left:= Trunc((pnlFundo.Width/2)-(pnlBackground.Width/2));

{Loja tem isso Lauro 20.01.17
  if(dmCupomFiscal.dbTemp_NFCECLIFOR.Value > 0)Then
  begin
    edtCliente.Text := dmCupomFiscal.dbTemp_NFCECLIFOR.AsString;
    edtClienteNome.Text := dmCupomFiscal.dbTemp_NFCECF_NOME.AsString;
  end;
}
end;

procedure TfrmPosto.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  dmCupomFiscal.dbTemp_NFCE_Item.Close;
  dmCupomFiscal.dbAbastecida.Close;
  Action := caFree; // Libera o formul�rio da mem�ria
  frmPosto := Nil;   // Deixa o formul�rio vazio
end;

procedure TfrmPosto.edtMercadoriaChange(Sender: TObject);
begin
  if(Trim(edtMercadoria.Text) = '')Then
    reVlrUnitario.Value := 0;
end;

procedure TfrmPosto.edtMercadoriaEnter(Sender: TObject);
begin
  Cor_Campo(Sender, HexToTColor(PERSONALIZAR.corFundoFoco));
  if Sender_Libera <> 'venda' then
  begin
    Sender_Libera:='';
    if Trim(edtBico.Text)='' then
    begin
      edtMercadoria.Clear;
      LimpaCampos;
    end;
  end;
end;

procedure TfrmPosto.limpaCampos;
begin
  //reVlrDesconto.Text:='0,00';
  reQuantidade.Value:=FRENTE_CAIXA.Qtd_Inicial;
  formataQtd;    //Formata Quantidade
  reVlrUnitario.Value:=0;
  reVlrDesconto.Value:=0;
  lblDescPerc.Caption:='0,00%';
  FormataVlrUni; //Formata Valor Unitario
  reSubtotal.Text:='0,00';
  la_Estoque.Caption:='Estoque';
  lblCusto.Caption := 'Custo';
  Abastecimento:=0;
  btnVende.Enabled:=False;
  btnCancela.Enabled:=False;
end;

procedure TfrmPosto.reVlrUnitarioExit(Sender: TObject);
begin
  if (Trim(reVlrUnitario.Text)='') then
  begin
    reVlrUnitario.SetFocus;
    msgAviso('Favor informar o Valor Unit�rio', '');
  end
  else
  begin
    Cor_Campo(Sender, HexToTColor(PERSONALIZAR.corFundoCampo));
    if valorUnitarioMaximoExcedeu(reVlrUnitario.Value) then
      reVlrUnitario.SetFocus
    else
      SubTotal;
  end;
end;

procedure TfrmPosto.btnCancelaClick(Sender: TObject);
begin
  try
    Mer_Bico;
  finally
    reQuantidade.Enabled := True;
  end;
end;

procedure TfrmPosto.reVlrDescontoExit(Sender: TObject);
var pDes : Currency;
begin
  Cor_Campo(Sender, HexToTColor(PERSONALIZAR.corFundoCampo));
  if (Trim(reVlrDesconto.Text)='') then reVlrDesconto.Value:=0;

  if (reSubtotal.Value > 0)then
  begin
    if (reVlrDesconto.Value < 0) then //Se informado em percentual Sinal -
      reVlrDesconto.Value := (reQuantidade.Value * reVlrUnitario.Value) * -(reVlrDesconto.Value)/100;

    pDes := (reVlrDesconto.Value / reSubtotal.Value) * 100;
    lblDescPerc.Caption :=FormatCurr('##0.00%',pDes);
    //reVlrDesconto.Text  := FormatFloat('#,##0.00', reVlrDesconto.value);

    If (reVlrDesconto.Value >= (reQuantidade.Value * reVlrUnitario.Value)) then
    begin
      msgInformacao('Valor do desconto deve-ser inferior ao subtotal do Item!','');
      reVlrDesconto.Value:=0;
      reVlrDesconto.SetFocus;
    end
    else
    begin
      if MERCADORIA.percDescontoMax >= pDes then
      begin
        if(reVlrDesconto.Value <> vlrDesconto) and (reVlrDesconto.Value > 0)then
        begin
          if (getAutorizacao('DESCONTO_NFCE','DAR DESCONTO NO ITEM DA NFCe',reVlrDesconto.Value)) then
          begin
            If ((reQuantidade.Value * reVlrUnitario.Value) > 0) and (reVlrDesconto.Value > 0) then
              reSubtotal.Value := (reQuantidade.Value * reVlrUnitario.Value) - reVlrDesconto.Value;
            subtotal;
          end
          else
          begin
            application.ProcessMessages;
            reVlrDesconto.Value:=vlrDesconto;
            pDes := (reVlrDesconto.Value / reSubtotal.Value) * 100;
            lblDescPerc.Caption := FormatCurr('##0.00%',pDes);
            application.ProcessMessages;
          end;
        end
        else
          SubTotal;
      end
      else
      begin
        msgInformacao(Format('Percentual m�ximo de desconto da mercadoria excedido.'+sLineBreak+
                            'Desconto m�ximo: %f %%'+sLineBreak+
                            'Desconto concedido: %f %%',[MERCADORIA.percDescontoMax, pDes]),'');
        reVlrDesconto.Value:=0;
        reVlrDesconto.SetFocus;
      end;
    end;
  end
  else
  begin
    if reVlrDesconto.Value <> 0 then
      reVlrDesconto.Value := 0;
  end;
  reVlrDesconto.Text  := FormatFloat('#,##0.00', reVlrDesconto.value);
end;

procedure TfrmPosto.btnVendeClick(Sender: TObject);
begin
  try
    //Valida��o caso mercadoria n�o possua NCM nao deixar Vender
    if frmPrincipal.Valida_NCM(MERCADORIA.ncm) then
    begin
      //Se n�o possuir registro temp_nfce insere
      if not(possuiRegistroTempECF(FRENTE_CAIXA.Caixa)) then
      begin
        try
          inserirTempNFCe();
        except
          on e:exception do
          begin
            dmCupomFiscal.dbTemp_NFCE.Transaction.RollbackRetaining;
            raise Exception.Create('[TEMP_NFCE] - Erro ao inserir registro na base de dados. '+#13+e.Message);
            //logErros(Self, caminhoLog, '[TEMP_NFCE] - Erro ao inserir registro na base de dados. '+e.Message, '[TEMP_NFCE] - Erro ao inserir registro na base de dados' , 'S', E);
          end;
        end;
      end;
      Sender_Libera          := 'venda';
      btnVende.Enabled       := False;
      btnCancela.Enabled     := False;
      MERCADORIA.vlrUnitario := reVlrUnitario.Value;
      MERCADORIA.quantidade  := reQuantidade.Value;
      MERCADORIA.vlrDesconto := reVlrDesconto.Value;
      try
        try
          DMCupomFiscal.dbTemp_NFCE_Item.Open;
          DMCupomFiscal.dbTemp_NFCE_Item.Append;
          DMCupomFiscal.dbTemp_NFCE_ItemCAIXA.Value      := FormatFloat('0000', FRENTE_CAIXA.caixa);
          DMCupomFiscal.dbTemp_NFCE_ItemITEM.Value       := FormatFloat('0000',0); //Ver controle item
          DMCupomFiscal.dbTemp_NFCE_ItemQUANTIDADE.Value := MERCADORIA.quantidade;
          DMCupomFiscal.dbTemp_NFCE_ItemVLR_UNITARIO.Value := MERCADORIA.vlrUnitario;
          DMCupomFiscal.dbTemp_NFCE_ItemVLR_TOTAL.Value  := reSubtotal.Value;
          DMCupomFiscal.dbTemp_NFCE_ItemQTD_CAN.Value    := 0;
          dmCupomFiscal.dbTemp_NFCE_ItemVLR_DESCONTO.Value := 0;
          dmCupomFiscal.dbTemp_NFCE_ItemVLR_DESCONTO_ITEM.Value := MERCADORIA.vlrDesconto;
          DMCupomFiscal.dbTemp_NFCE_ItemBICO.Value       := StrToIntDef(edtBico.Text,0);
          DMCupomFiscal.dbTemp_NFCE_ItemABASTECIDA.Value := Abastecimento;
          DMCupomFiscal.dbTemp_NFCE_ItemMERCADORIA.Value := Trim(MERCADORIA.codigo);
          DMCupomFiscal.dbTemp_NFCE_ItemM_DESCRICAO.Value:= Trim(MERCADORIA.descricao);
          DMCupomFiscal.dbTemp_NFCE_ItemVLR_CUSTO_ATUAL.Value := 0; //Trigger busca
          DMCupomFiscal.dbTemp_NFCE_ItemICM.Value        := MERCADORIA.icms;
          DMCupomFiscal.dbTemp_NFCE_ItemCST_PIS.Value    := MERCADORIA.cstPIS;
          DMCupomFiscal.dbTemp_NFCE_ItemCST_COFINS.Value := MERCADORIA.cstCOFINS;
          DMCupomFiscal.dbTemp_NFCE_ItemCST_ICM.Value    := MERCADORIA.cstICMS;
          DMCupomFiscal.dbTemp_NFCE_ItemCFOP.Value       := MERCADORIA.cfop;
          DMCupomFiscal.dbTemp_NFCE_ItemNCM.Value        := MERCADORIA.ncm;
          DMCupomFiscal.dbTemp_NFCE_ItemUNIDADE.Value    := MERCADORIA.unidade;
          DMCupomFiscal.dbTemp_NFCE_ItemINDTOT.Value     := '1'; //--Inclui valor no Total da NF
          DMCupomFiscal.dbTemp_NFCE_ItemVLR_BC_ICM.Value := 0;
          DMCupomFiscal.dbTemp_NFCE_ItemALI_ICM.Value    := 0;
          DMCupomFiscal.dbTemp_NFCE_ItemVLR_ICM.Value    := 0;
          DMCupomFiscal.dbTemp_NFCE_ItemVLR_FRETE.Value  := 0;
          DMCupomFiscal.dbTemp_NFCE_ItemVLR_SEG.Value    := 0;
          DMCupomFiscal.dbTemp_NFCE_ItemVLR_OUTRO.Value  := 0;
          DMCupomFiscal.dbTemp_NFCE_ItemVLR_BC_PIS.Value := 0;
          DMCupomFiscal.dbTemp_NFCE_ItemALI_PIS.Value    := 0;
          DMCupomFiscal.dbTemp_NFCE_ItemVLR_PIS.Value    := 0;
          DMCupomFiscal.dbTemp_NFCE_ItemVLR_BC_COFINS.Value := 0;
          DMCupomFiscal.dbTemp_NFCE_ItemALI_COFINS.Value := 0;
          DMCupomFiscal.dbTemp_NFCE_ItemVLR_COFINS.Value := 0;
          DMCupomFiscal.dbTemp_NFCE_ItemVLR_TOTTRIB.Value:= 0;
          DMCupomFiscal.dbTemp_NFCE_ItemVLR_TOTTRIB_MUN.Value:= 0;
          DMCupomFiscal.dbTemp_NFCE_ItemVLR_TOTTRIB_EST.Value:= 0;
          DMCupomFiscal.dbTemp_NFCE_ItemKIT.value := 'N';
          DMCupomFiscal.dbTemp_NFCE_Item.Post;
          DMCupomFiscal.dbTemp_NFCE_Item.Transaction.CommitRetaining;
        except
          on E:Exception do
          begin
            DMCupomFiscal.dbTemp_NFCE_Item.Transaction.RollbackRetaining;
            getTemp_NFCE_Item;
            raise Exception.Create('[TEMP_NFCE_ITEM] - Erro ao inserir item na base de dados.'+#13+e.Message);
          end;
        end;
      except
        on e: Exception do
        begin
          logErros(Self, caminhoLog, e.Message+#13+'Mercadoria: '+MERCADORIA.codigo+'-'+MERCADORIA.descricao, 'Erro ao vender Item. Mercadoria: '+MERCADORIA.codigo+'-'+MERCADORIA.descricao, 'S', E);
          if (ELChavePedido > 0) then
            raise Exception.Create('Erro');//Erro para retornar para tela de importa��o de pedidos para tratar o cancelamento do cupom
        end;
      end;
    end;
  finally
    btnVende.Enabled:=True;
    Abastecimento    := 0;
    getTemp_NFCE_Item;
    Sender_Libera:='';
    Mer_Bico;
    if POSTO.Abastecidas_Venda then
      btnLerAbastecidas.Click;
    reQuantidade.Enabled := True;
  end;
end;

procedure TfrmPosto.reQuantidadeEnter(Sender: TObject);
begin
  Cor_Campo(Sender, HexToTColor(PERSONALIZAR.corFundoFoco));
  If Trim(edtMercadoria.Text)='' then
  begin
    Mer_Bico;
    Abort;
  end;
end;

procedure TfrmPosto.Label1Click(Sender: TObject);
begin
  FrmPesquisaMercadoria := TFrmPesquisaMercadoria.Create(Self);
  try
    if (FrmPesquisaMercadoria.ShowModal=mrOK) then
      edtMercadoria.Text:= Trim(FrmPesquisaMercadoria.qMercadoria.FieldByName('Id').AsString);
  finally
    FrmPesquisaMercadoria.Free;
  end;
end;

procedure TfrmPosto.edtBicoExit(Sender: TObject);
begin
  Cor_Campo(Sender, HexToTColor(PERSONALIZAR.corFundoCampo));
  If Trim(edtBico.Text) <> '' then
  begin
    //Captura Mercadoria e Valor de Venda associado ao Bico
    DMCupomFiscal.dbQuery1.Active:=False;
    DMCupomFiscal.dbQuery1.SQL.Clear;
    DMCupomFiscal.dbQuery1.SQL.Add('Select Mercadoria, Venda FROM Est_Mercadorias_Bico Where ID=:pID');
    DMCupomFiscal.dbQuery1.ParamByName('pID').AsInteger:=StrToIntDef(edtBico.Text,0);
    DMCupomFiscal.dbQuery1.Active:=True;
    DMCupomFiscal.dbQuery1.First;
    If not(DMCupomFiscal.dbQuery1.Eof) then
    begin
      if reSubtotal.Value = 0 then //merda da colina
      begin
        edtMercadoria.Text := DMCupomFiscal.dbQuery1.FieldByName('Mercadoria').AsString;
        reVlrUnitario.Value := DMCupomFiscal.dbQuery1.FieldByName('Venda').AsCurrency;
      end;
    end
    Else
    begin
      msgInformacao('Bico n�o cadastrado','Aten��o');
      edtBico.SetFocus;
    end;
  end;
end;

procedure TfrmPosto.Mer_Bico;
begin
  //Lauro 20.01.17 Ver estas condicoes
  if (Sender_Libera='edtCliente') then   //anderson aqui
    edtCliente.SetFocus
  Else
  begin
{ Lauro 20.01.17 Removido pois sempre q for posto vai ter bico!
    If edtBico.Enabled then
      edtBico.SetFocus
    Else
      edtMercadoria.SetFocus;
}
    edtBico.SetFocus
  end;
  edtMercadoria.Clear;
  edtBico.Clear;
  LimpaCampos;  
end;


procedure TfrmPosto.ppmAbastecidasPopup(Sender: TObject);
begin
  ppmAbastecidas.Items.Items[0].Enabled := not(cdsAbastecidas.IsEmpty);
end;

procedure TfrmPosto.edtClienteExit(Sender: TObject);
begin
  edtCliente.Color := clWhite;
  edtCliente.font.Color := clBlack;
  TAB_PRECO.TabPreco := 0;
  If (Trim(edtCliente.Text)<>'') then
  begin
    DMCupomFiscal.dbQuery1.Active:=False;
    DMCupomFiscal.dbQuery1.SQL.Clear;
    DMCupomFiscal.dbQuery1.SQL.Add('Select c.ID as CLIFOR,c.Nome, c.TPreco, c.Limite, c.endereco as CF_END, '+
                                   'm.descricao as CF_MUNI, c.cnpjcpf as CF_CNPJ_CPF, '+
                                   'c.ie as CF_IE, c.numero_end as CF_NUMERO_END, '+
                                   'c.municipio as CF_MUNICIPIO, c.Bairro as CF_BAIRRO, '+
                                   'c.cep as CF_CEP, c.indiedest as INDIEDEST '+
                                   'FROM CliFor c '+
                                   'left outer join municipios m on m.id = c.municipio '+
                                   'Where c.ID=:pID and Ativo = ''S''');
    DMCupomFiscal.dbQuery1.ParamByName('pID').AsInteger:=StrToIntDef(edtCliente.Text,0);
    DMCupomFiscal.dbQuery1.Active:=True;
    DMCupomFiscal.dbQuery1.First;
    If not(DMCupomFiscal.dbQuery1.Eof) then
    begin
      edtClienteNome.Text := DMCupomFiscal.dbQuery1.FieldByName('Nome').AsString;
      TAB_PRECO.TabPreco  := DMCupomFiscal.dbQuery1.FieldByName('TPreco').AsInteger;
      Sender_Libera:='';
      Mer_Bico;
      //Apartir desse momento n�o altera mais o cliente, somente se o NFCE n�o
      //tiver mais itens adicionados
      //Se n�o possuir registro temp_nfce insere
      if not(possuiRegistroTempECF(FRENTE_CAIXA.Caixa)) then
      begin
        try
          inserirTempNFCe();
        except
          on e:exception do
          begin
            dmCupomFiscal.dbTemp_NFCE.Transaction.RollbackRetaining;
            raise Exception.Create('[TEMP_NFCE] - Erro ao inserir registro na base de dados. '+#13+e.Message);
            //logErros(Self, caminhoLog, '[TEMP_NFCE] - Erro ao inserir registro na base de dados. '+e.Message, '[TEMP_NFCE] - Erro ao inserir registro na base de dados' , 'S', E);
          end;
        end;
      end;
      dmCupomFiscal.getTemp_NFCE;
      if not(dmCupomFiscal.dbTemp_NFCE.State in [dsEdit, dsInsert])then
        dmCupomFiscal.dbTemp_NFCE.Edit;
      dmCupomFiscal.dbTemp_NFCECLIFOR.Value        := DMCupomFiscal.dbQuery1.FieldByName('CLIFOR').AsInteger;
      dmCupomFiscal.dbTemp_NFCECF_NOME.Value       := DMCupomFiscal.dbQuery1.FieldByName('Nome').AsString;
      dmCupomFiscal.dbTemp_NFCECF_ENDE.Value       := DMCupomFiscal.dbQuery1.FieldByName('CF_END').AsString;
      dmCupomFiscal.dbTemp_NFCECF_CNPJ_CPF.Value   := DMCupomFiscal.dbQuery1.FieldByName('CF_CNPJ_CPF').AsString;
      dmCupomFiscal.dbTemp_NFCECF_IE.Value         := DMCupomFiscal.dbQuery1.FieldByName('CF_IE').AsString;
      dmCupomFiscal.dbTemp_NFCECF_NUMERO_END.Value := DMCupomFiscal.dbQuery1.FieldByName('CF_NUMERO_END').AsString;
      dmCupomFiscal.dbTemp_NFCECF_MUNICIPIO.Value  := DMCupomFiscal.dbQuery1.FieldByName('CF_MUNICIPIO').AsInteger;
      dmCupomFiscal.dbTemp_NFCECF_CEP.Value        := DMCupomFiscal.dbQuery1.FieldByName('CF_CEP').AsString;
      dmCupomFiscal.dbTemp_NFCECF_BAIRRO.Value     := DMCupomFiscal.dbQuery1.FieldByName('CF_BAIRRO').AsString;
      dmCupomFiscal.dbTemp_NFCEINDIEDEST.Value     := DMCupomFiscal.dbQuery1.FieldByName('INDIEDEST').AsString;
      dmCupomFiscal.dbTemp_NFCE.Post;
      //Caso for diferente deleta formas de pagamento
      if(dmCupomFiscal.dbTemp_NFCECLIFOR.NewValue <> dmCupomFiscal.dbTemp_NFCECLIFOR.OldValue)then
      begin
        dmCupomFiscal.IBSQL1.close;
        dmCupomFiscal.IBSQL1.SQL.Clear;
        dmCupomFiscal.IBSQL1.SQL.Add('Delete from TEMP_NFCE_FORMAPAG where CAIXA ='+FormatFloat('0000',FRENTE_CAIXA.Caixa));
        dmCupomFiscal.IBSQL1.Prepare;
        dmCupomFiscal.IBSQL1.ExecQuery;
        dmCupomFiscal.IBSQL1.Close;
        dmCupomFiscal.Transaction.CommitRetaining;
      end;
      {If (crExcedeuLimite(StrToIntDef(edtCliente.Text,0), reTotal.Value, DMCupomFiscal.dbQuery1.FieldByName('Limite').AsCurrency)) then
      begin
        edtCliente.SetFocus;
        Abort;
      end;}
      if(FRENTE_CAIXA.Exibir_Contas_Vencidas)then
        if not(crContasVencidas(dmCupomFiscal.dbTemp_NFCECLIFOR.Value))then
        begin
          Sender_Libera:='edtCliente';
          edtCliente.Enabled:=true;
          edtCliente.SetFocus;
          edtCliente.SelectAll;
          abort;
        end;
        //crContasVencidas(dmCupomFiscal.dbTemp_NFCECLIFOR.Value);
    end
    Else
    begin
      TAB_PRECO.TabPreco:=0;
      if not(dmCupomFiscal.dbTemp_NFCE.IsEmpty)Then
        dmCupomFiscal.LimparDadosCliente;
      msgAviso('Cliente n�o cadastrado/Inativo','');
      if(edtCliente.Enabled)then
      begin
        edtCliente.Clear;
        edtCliente.SetFocus;
      end;
    end;
  end
  else
  begin
    if not(dmCupomFiscal.dbTemp_NFCE.IsEmpty)Then
      dmCupomFiscal.LimparDadosCliente;
    edtCliente.Text:='';
    edtClienteNome.Text:='';
    Sender_Libera:='';
    Mer_Bico;
  end;
end;

procedure TfrmPosto.edtClienteEnter(Sender: TObject);
begin
  Cor_Campo(Sender, HexToTColor(PERSONALIZAR.corFundoFoco));
  edtClienteNome.Clear;
end;

procedure TfrmPosto.Label11Click(Sender: TObject);
begin
   FrmPesquisaCliente := TFrmPesquisaCliente.Create(Self);
   try
   if (FrmPesquisaCliente.ShowModal=mrOK) then
      edtCliente.Text:= FrmPesquisaCliente.qCliFor.FieldByName('ID').AsString;
   finally
      FrmPesquisaCliente.Free;
   end;
end;

procedure TfrmPosto.reSubtotalExit(Sender: TObject);
begin
  Cor_Campo(Sender, HexToTColor(PERSONALIZAR.corFundoCampo));
  //----------------- Venda Total ---------------------------------------------
  if (reQuantidade.Value <= 0)  and (reSubtotal.Value > 0) then
  begin
    reQuantidade.Value:=(reSubtotal.Value/reVlrUnitario.Value);
    If FRENTE_CAIXA.Truncar then
      reQuantidade.Value:=reQuantidade.Value + 0.001;
  end;
  //---------------------------------------------------------------------------
  SubTotal;
  tratarBotoes;
  if reSubtotal.Value > 0 then
    btnVende.SetFocus;
end;


procedure TfrmPosto.Label13Click(Sender: TObject);
begin
  If StrToIntDef(edtCliente.Text,0)>0 then
  begin
    FrmConsultaExtrato:=TFrmConsultaExtrato.create(Application);
    FrmConsultaExtrato.ShowModal;
    FrmConsultaExtrato.Free;
  end
  Else
  begin
    Sender_Libera:='edtCliente';
    edtCliente.SetFocus;
    msgInformacao('N�o foi informado o cliente!','');
  end;
end;

procedure TfrmPosto.DBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  If DMCupomFiscal.dbTemp_NFCE_ItemQUANTIDADE.AsCurrency = 0 then
  begin
    Dbgrid1.Canvas.Font.Color:= clRed; // coloque aqui a cor desejada
    //Dbgrid1.Canvas.Font.Style:=[fsItalic];
  end;
  Dbgrid1.DefaultDrawDataCell(Rect, dbgrid1.columns[datacol].field, State);

  //Deixa Grid Zebrado ----------------
  {if State = [] then
  begin
    if dmCupomFiscal.dbTemp_NFCE_Item.RecNo mod 2 = 1 then
      DBGrid1.Canvas.Brush.Color := clMoneyGreen
    else
      DBGrid1.Canvas.Brush.Color := clWhite;
  end;
  DBGrid1.DefaultDrawColumnCell(Rect, DataCol, Column, State);}

end;

procedure TfrmPosto.dsEcfDataChange(Sender: TObject; Field: TField);
begin
  if(dmCupomFiscal.dbTemp_NFCECLIFOR.AsInteger > 0)then
  begin
    edtCliente.Text := dmCupomFiscal.dbTemp_NFCECLIFOR.AsString;
    edtClienteNome.Text := dmCupomFiscal.dbTemp_NFCECF_NOME.AsString;
  end;
end;

procedure TfrmPosto.DsItemDataChange(Sender: TObject; Field: TField);
begin
{lauro 23.01.17
edtCliente.Enabled := not ((dmCupomFiscal.Flag_Cliente) or (dmCupomFiscal.ExistePagamentoPrazo(FRENTE_CAIXA.Caixa)));
}
  dmCupomFiscal.Flag_Cliente := dmCupomFiscal.ClientePossuiPrecoEsp('');
  edtCliente.Enabled := (dmCupomFiscal.Flag_Cliente);
end;

procedure TfrmPosto.edtClienteClick(Sender: TObject);
begin
  // Armazena no do edtCliente em variavel para poder sair do campo edtMercadoria
  if sender is TEdit then Sender_Libera:=TEdit(Sender).Name;
  Mer_Bico; //--- Anderson 22/08/13
end;


procedure TfrmPosto.BloqueiaVenda;
begin
  btnVende.Caption := 'Aguarde...';
  btnVende.Enabled := False;
  Application.ProcessMessages;
end;

procedure TfrmPosto.LiberaVenda;
begin
  btnVende.Caption := 'OK';
  btnVende.Enabled := True;
end;

procedure TfrmPosto.arrendarCampos;
begin
  ArredondarComponente(edtMercadoria,10);
  ArredondarComponente(edtBico,10);
  ArredondarComponente(reQuantidade,10);
  ArredondarComponente(reTotal,10);
  ArredondarComponente(reVlrUnitario,10);
  ArredondarComponente(reSubtotal,10);
  ArredondarComponente(reVlrDesconto,10);
end;

procedure TfrmPosto.ApplicationEvents1Hint(Sender: TObject);
begin
  barStatus.Panels[0].Text := ' '+Application.Hint;
end;

procedure TfrmPosto.btnSairClick(Sender: TObject);
begin
  if edtMercadoria.Text='' then
    if msgPergunta('Sair do Frente de Caixa ?','') then
      close;
end;

procedure TfrmPosto.btnFechamentoDiarioClick(Sender: TObject);
begin
  edtMercadoria.SetFocus;
  FrmFechamentoAbastecidas:=TFrmFechamentoAbastecidas.create(Application);
  FrmFechamentoAbastecidas.ShowModal;
  FrmFechamentoAbastecidas.Free;
end;

procedure TfrmPosto.btnLerAbastecidasClick(Sender: TObject);
begin
  try
    try
      Label4.Visible:=True;
      if (POSTO.Abastecidas_Venda) then
      begin
        getAbastecidas;
      end
      else
      begin
        edtBico.SetFocus;
        frmAbastecidas:=TFrmAbastecidas.create(Application);
        //FrmAbastecidas := TFrmAbastecidas.Create(Self);
        if (FrmAbastecidas.ShowModal=mrOK) then
        begin
          Abastecimento:=FrmAbastecidas.cdsAbastecidas.FieldByName('Id').AsInteger;

          DMCupomFiscal.dbQuery1.Active:=False;
          DMCupomFiscal.dbQuery1.SQL.Clear;
          If POSTO.Abastecidas_Exibir then
            DMCupomFiscal.dbQuery1.SQL.Add('SELECT Mercadoria, Litro, Preco FROM Est_Abastecimentos WHERE (Status<>''E'') and (Id=:pId)')
          Else
            DMCupomFiscal.dbQuery1.SQL.Add('SELECT Mercadoria, Litro, Preco FROM Est_Abastecimentos WHERE (Status=''X'') and (Id=:pId)');
          DMCupomFiscal.dbQuery1.ParamByName('pId').AsInteger:=Abastecimento;
          DMCupomFiscal.dbQuery1.Active:=True;
          DMCupomFiscal.dbQuery1.First;

          If not(DMCupomFiscal.dbQuery1.Eof) then
          begin
            Case DMCupomFiscal.dbQuery1.FieldByName('Mercadoria').AsInteger of
              1:edtBico.Text:='1';
              2:edtBico.Text:='2';

              3:edtBico.Text:='3';
              4:edtBico.Text:='4';
              5:edtBico.Text:='5';
              6:edtBico.Text:='6';

              7:edtBico.Text:='7';
              8:edtBico.Text:='8';

              9:edtBico.Text:='9';
              10:edtBico.Text:='10';

              11:edtBico.Text:='11';
              12:edtBico.Text:='12';
            end;
            reQuantidade.Text:=FormatFloat('###,##0.000',DMCupomFiscal.dbQuery1.FieldByName('Litro').AsCurrency);
            reQuantidade.Enabled := POSTO.AlterarQuantidade;//posto machado altera��o para deixar editar
            reVlrUnitario.Text:=FormatFloat('###,##0.000',DMCupomFiscal.dbQuery1.FieldByName('Preco').AsCurrency);
          end;
        end;
        FrmAbastecidas.Free;
      end;
    except
      on E:Exception do
      begin
        logErros(Sender, caminhoLog,'Ocorreu seguinte erro ao ler abastecidas:'+sLineBreak+e.Message, 'Erro ao ler abastecidas','S',E);
      end;
    end;
  finally
    Label4.Visible:=False;
    Application.ProcessMessages;
  end;
end;

procedure TfrmPosto.btnCancelarItemClick(Sender: TObject);
begin
  if (reTotal.Value > 0) then
  begin
    FrmCancelaItem:=TFrmCancelaItem.Create(Application);
    FrmCancelaItem.ShowModal;
    FrmCancelaItem.Free;

    //lauro 23.01.17 dmCupomFiscal.Flag_Cliente := (dmCupomFiscal.dbTemp_NFCECLIFOR.Value > 0) and    (dmCupomFiscal.ClientePossuiPrecoEsp(''));
    dmCupomFiscal.Flag_Cliente := dmCupomFiscal.ClientePossuiPrecoEsp('');
    edtCliente.Enabled :=(dmCupomFiscal.Flag_Cliente);

    getTemp_NFCE_Item;

    Mer_Bico;
    if POSTO.Abastecidas_Venda then
      btnLerAbastecidas.Click;
  end;
end;

procedure TfrmPosto.btnCancelarCupomClick(Sender: TObject);
begin
  if (reTotal.Value > 0) or not(dmCupomFiscal.dbTemp_NFCE.IsEmpty)then
  begin
    if(dmCupomFiscal.TefEstaAtivo)then
    begin
      if(dmCupomFiscal.ExisteComprovanteTef(dmCupomFiscal.dbTemp_NFCEEL_CHAVE.AsInteger))then  //Lauro 25.10 passa el_Chave
      begin
        msgInformacao('Existe forma de pagamento TEF para essa NFC-e.'+sLineBreak+sLineBreak+
          'Voc� deve finalizar a NFC-e e depois cancelar as transa��es tef',Application.Title);
        Abort;
      end;
    end;
    cancelarCupomFiscal;
    Mer_Bico;
    if POSTO.Abastecidas_Venda then
      btnLerAbastecidas.Click;
  end;
end;

procedure TfrmPosto.btnFinalizarCupomClick(Sender: TObject);
begin
  if (reTotal.Value > 0) then
  begin
    dmCupomFiscal.getTemp_NFCE;
    try
      //Se n�o vinculou o cliente ou o valor da venda � igual ou superior a 10000 abre a tela
      if(dmCupomFiscal.dbTemp_NFCECLIFOR.Value = 0) or
        (dmCupomFiscal.dbTemp_NFCEVLR_TOTAL.Value >= 10000) then
      begin
        frmClifor:=TfrmClifor.create(Application);
        try
          case frmClifor.ShowModal of
          mrOk:begin
              dmCupomFiscal.Transaction.CommitRetaining;
              dmCupomFiscal.getTemp_NFCE;
            end;
          mrCancel:begin
              dmCupomFiscal.Transaction.RollbackRetaining;
//              dmCupomFiscal.getTemp_NFCE;
              //Mauricio 16.02.17 Descomentei o abort
              //Nao pode comentar o abort se n�o o usuario edita as infos do cliente
              //e preciona esc o sistema n�o grava e vai pra tela de finaliza��o
              Abort;//Lauro 20.01.17
            end;
          end;
        finally
          frmClifor.Free;
        end;
      end;
      FrmFim:=TFrmFim.create(Application);
      try
        case FrmFim.ShowModal of
        mrOk:begin
            dmCupomFiscal.Transaction.CommitRetaining;
          end;
        mrCancel:begin
            dmCupomFiscal.Transaction.RollbackRetaining;
            Abort;
          end;
        end;
      finally
        FrmFim.Free;
      end;
    finally
      dmCupomFiscal.getTemp_NFCE;
      if(dmCupomFiscal.dbTemp_NFCECLIFOR.Value<=0)then
      begin
        edtCliente.Clear;
        edtClienteNome.Clear;
      end;
      getTemp_NFCE_Item;
      //lauro 23.01.17  ver se precisa!!! edtCliente.Enabled := not ((dmCupomFiscal.Flag_Cliente) or (dmCupomFiscal.ExistePagamentoPrazo(FRENTE_CAIXA.Caixa)));
    end;
    Mer_Bico;
  end;
end;

procedure TfrmPosto.edtCCustoClick(Sender: TObject);
begin
  if sender is TEdit then Sender_Libera:=TEdit(Sender).Name;
  Mer_Bico;
end;

procedure TfrmPosto.FormShow(Sender: TObject);
begin
  try
    setDadosRodape;
    dmCupomFiscal.getTemp_NFCE;
    dmCupomFiscal.Flag_Cliente := True; //Lauro 23.01.17
    if(dmCupomFiscal.dbTemp_NFCECLIFOR.Value > 0)Then
    begin
      edtCliente.Text := dmCupomFiscal.dbTemp_NFCECLIFOR.AsString;
      edtClienteNome.Text := dmCupomFiscal.dbTemp_NFCECF_NOME.AsString;
      //Lauro 23.01.17
      dmCupomFiscal.Flag_Cliente := dmCupomFiscal.ClientePossuiPrecoEsp('');
      edtCliente.Enabled :=(dmCupomFiscal.Flag_Cliente);
    end;
    getTemp_NFCE_Item;
{ ver para Excluir ????????
    //Centraliza aqui e no form resize
    //pnlFundo.Left:=Trunc((frmPosto.Width/2)-(pnlFundo.Width/2));
    //pnlBotoesOrganiza.Left:=Trunc((frmPosto.Width/2)-(pnlBotoesOrganiza.Width/2)+(btnPedido.Width/2));
    //pnlDadosIFOrganiza.Left:=Trunc((frmPosto.Width/2)-(pnlDadosIFOrganiza.Width/2));
}
    Application.ProcessMessages;
    if(pnlAbastecidas.Visible)then
    begin
      pnlAbastecidas.Left := Trunc((pnlFundo.Width/2)-(pnlBackground.Width/2)-(pnlAbastecidas.Width/2));
      pnlBackground.Left:= Trunc((pnlFundo.Width/2)-(pnlBackground.Width/2)+(pnlAbastecidas.Width/2))
    end
    else
      pnlBackground.Left:= Trunc((pnlFundo.Width/2)-(pnlBackground.Width/2));
  except
    on e:Exception do
    begin
      if e is EDatabaseError then
        logErros(Self, caminhoLog,'Erro ao abrir Frente de Caixa'+#13+E.Message, 'Erro ao abrir Frente de Caixa','S',e);
      PostMessage(Handle, WM_CLOSE, 0, 0);  // Fechar o Form
    end;
  end;
end;

procedure TfrmPosto.FormCreate(Sender: TObject);
begin
  setCores;
  setMensagemDisplay;
  arrendarCampos;
  limpaCampos;

  if (POSTO.Abastecidas_Venda) then //Quando usar Exibi�ao rapida abastecidas
  begin
    pnlAbastecidas.Visible:=True;
    btnLerAbastecidas.Click;
  end
  else
  begin
    pnlAbastecidas.Visible:=False;
    //pnlFundo.Width:=pnlBackground.Width;
    //pnlBackground.Align:=alLeft;
  end;

  //Centralizar Panel
{  pnlFundo.Left:=Trunc((frmPosto.Width/2)-(pnlFundo.Width/2));
  pnlBotoesOrganiza.Left:=Trunc((frmPosto.Width/2)-(pnlBotoesOrganiza.Width/2));
  pnlDadosIFOrganiza.Left:=Trunc((frmPosto.Width/2)-(pnlDadosIFOrganiza.Width/2));
  }
  reVlrUnitario.Enabled:=FRENTE_CAIXA.Altera_Valor;
  //reVlrUnitario.ReadOnly := not FRENTE_CAIXA.Altera_Valor;

  POSTO.Turno := frmPrincipal.getTurnoFuncionario(LOGIN.usuarioCod);
  if POSTO.Turno <> '' then
    lblTurno.Visible:=True
  else
  begin
    lblTurno.Visible:=False;
    logErros(Sender, caminhoLog, '', Format('Funcion�rio %s n�o possui Turno definido',[LOGIN.usuarioFuncNome]), 'S', nil);
  end;
end;

procedure TfrmPosto.btnConsultarCRClick(Sender: TObject);
begin
  frmConsultaLimiteCR:=TfrmConsultaLimiteCR.create(Application);
  frmConsultaLimiteCR.ShowModal;
  frmConsultaLimiteCR.Free;
end;

procedure TfrmPosto.btnConsumidorClick(Sender: TObject);
begin
  edtMercadoria.Clear; // Para n�o executar o onexit
  edtBico.Clear;
  if(dmCupomFiscal.dbTemp_NFCECLIFOR.Value > 0)then //Se ja vinculou um cliente abre a tela
  begin
    frmClifor:=TfrmClifor.create(Application);
    try
      case frmClifor.ShowModal of
      mrOk:begin
          dmCupomFiscal.Transaction.CommitRetaining;
        end;
      mrCancel:begin
          dmCupomFiscal.Transaction.RollbackRetaining;
        end;
      end;
    finally
      frmClifor.Free;
    end;
  end
  else //Se n�o s� foca no campo
    if(edtCliente.Enabled)then
    begin
      Sender_Libera := 'edtCliente';
      edtCliente.SetFocus;
    end;
end;

procedure TfrmPosto.btnConsultaPrecoClick(Sender: TObject);
begin
  frmPesquisaCodigo := TfrmPesquisaCodigo.Create(Application);
{  if(trim(edtMercadoria.Text)<> '')Then
  begin
    frmPesquisaCodigo.edtCodigo.Text := MERCADORIA.codigo;
    //frmPesquisaCodigo.edtCodigoExit(Action);
  end;}
  frmPesquisaCodigo.ShowModal;
  frmPesquisaCodigo.Free;
end;

procedure TfrmPosto.btnPesquisaClick(Sender: TObject);
begin
  //Pesquisa Mercadoria
  if (edtMercadoria.Focused) then
    Label1Click(Action);

  //Pesquisa Cliente
  If  (edtCliente.Focused) then
    Label11Click(Action);
end;

procedure TfrmPosto.reQuantidadeExit(Sender: TObject);
begin
  Cor_Campo(Sender, HexToTColor(PERSONALIZAR.corFundoCampo));
  if (Trim(reQuantidade.Text)='') then reQuantidade.Value:=FRENTE_CAIXA.Qtd_Inicial;
  if quantidadeMaximaExcedeu(reQuantidade.Value) then
    reQuantidade.SetFocus
  else
    SubTotal;
end;

procedure TfrmPosto.setCores;
begin
  pnlMensagem.Color:=HexToTColor(PERSONALIZAR.corFundoCampo);
  pnlMensagem.Font.Color:=HexToTColor(PERSONALIZAR.corFonte);
  txtMensagem.Color:=HexToTColor(PERSONALIZAR.corFundoCampo);
  txtMensagem.Font.Color:=HexToTColor(PERSONALIZAR.corFonte);
  pnlDadosIF.Color:=HexToTColor(PERSONALIZAR.corFundoCampo);
  pnlDadosIF.Font.Color:=HexToTColor(PERSONALIZAR.corFonte);
  pnlBotoes.Color:=HexToTColor(PERSONALIZAR.corFundoCampo);
  edtMercadoria.Color:=HexToTColor(PERSONALIZAR.corFundoCampo);
  edtMercadoria.Font.Color:=HexToTColor(PERSONALIZAR.corFonte);
  edtBico.Color:=HexToTColor(PERSONALIZAR.corFundoCampo);
  edtBico.Font.Color:=HexToTColor(PERSONALIZAR.corFonte);
  reQuantidade.Color:=HexToTColor(PERSONALIZAR.corFundoCampo);
  reQuantidade.Font.Color:=HexToTColor(PERSONALIZAR.corFonte);
  reVlrUnitario.Color:=HexToTColor(PERSONALIZAR.corFundoCampo);
  reVlrUnitario.Font.Color:=HexToTColor(PERSONALIZAR.corFonte);
  reVlrDesconto.Color:=HexToTColor(PERSONALIZAR.corFundoCampo);
  reVlrDesconto.Font.Color:=HexToTColor(PERSONALIZAR.corFonte);
  reSubtotal.Color:=HexToTColor(PERSONALIZAR.corFundoCampo);
  reSubtotal.Font.Color:=HexToTColor(PERSONALIZAR.corFonte);
  reTotal.Color:=HexToTColor(PERSONALIZAR.corFundoCampo);
  reTotal.Font.Color:=HexToTColor(PERSONALIZAR.corFonte);
  alterarCorFonteLabel(pnlDadosIF, HexToTColor(PERSONALIZAR.corFonte));
end;

procedure TfrmPosto.edtBicoEnter(Sender: TObject);
begin
  Cor_Campo(Sender, HexToTColor(PERSONALIZAR.corFundoFoco));
end;

procedure TfrmPosto.reVlrUnitarioEnter(Sender: TObject);
begin
  Cor_Campo(Sender, HexToTColor(PERSONALIZAR.corFundoFoco));
end;

procedure TfrmPosto.reVlrDescontoEnter(Sender: TObject);
begin
  Cor_Campo(Sender, HexToTColor(PERSONALIZAR.corFundoFoco));
  lblDescPerc.Caption:='0,00%';
  vlrDesconto := reVlrDesconto.Value;
  application.ProcessMessages;
end;

procedure TfrmPosto.reSubtotalEnter(Sender: TObject);
begin
  Cor_Campo(Sender, HexToTColor(PERSONALIZAR.corFundoFoco));
end;

procedure TfrmPosto.btnPedidoClick(Sender: TObject);
begin
  { Somente para Loja nao para Posto

  if (reTotal.Value > 0) then
  begin
    msgInformacao('N�o � poss�vel importar Pedido com Cupom Fiscal Pendente','');
  end
  else
  begin
    Application.CreateForm(TfrmImportaPedido, frmImportaPedido);
    if(trim(edtCliente.Text) <> '')then
    begin
      frmImportaPedido.edtCliente.Text:=edtCliente.Text;
      frmImportaPedido.edtClienteNome.Text:=edtClienteNome.Text;
    end
    else
    begin
      frmImportaPedido.edtCliente.Text:='0';
      frmImportaPedido.edtClienteNome.Text:='TODOS OS CLIENTES...';
    end;
    frmImportaPedido.ShowModal;
    frmImportaPedido.Free;
  end;    }
end;

procedure TfrmPosto.btnGavetaClick(Sender: TObject);
var
  F: textfile;
  iRetorno: Integer;
begin
  if(trim(FRENTE_CAIXA.GAV_Caminho) = '')then
  begin
    application.ProcessMessages;
    frmPrincipal.mEnviado.Lines.add(DateTimeToStr(Now)+'   Le_Status()');
    iRetorno := Le_Status();
    frmPrincipal.mResp.Lines.add(DateTimeToStr(Now)+'   Le_Status(): '+IntToStr(iRetorno));
    if iRetorno = 0 then //Erro ao comunicar com a impressora, tenta abrir a porta
    begin
      frmPrincipal.mEnviado.Lines.add(DateTimeToStr(Now)+'   IniciaPorta(USB)');
      iRetorno := IniciaPorta('USB');
      frmPrincipal.mResp.Lines.add(DateTimeToStr(Now)+'   IniciaPorta(USB): '+IntToStr(iRetorno));
      if(iRetorno <> 1) then
      begin
        ShowMessage('Erro ao abrir a porta de comunica��o.');
        Abort;
      end;
    end;
    application.ProcessMessages;
    frmPrincipal.mEnviado.Lines.add(DateTimeToStr(Now)+'   AcionaGaveta()');
    iRetorno := AcionaGaveta();
    frmPrincipal.mResp.Lines.add(DateTimeToStr(Now)+'   AcionaGaveta(): '+IntToStr(iRetorno));
    if(iRetorno <> 1) then
      ShowMessage( 'Erro ao acionar a gaveta.' );
  end
  else
  begin                             {
    AssignFile(F,Trim(FRENTE_CAIXA.GAV_Caminho));
    Rewrite(F);
    Writeln(F,#027+#112+#000+#100'');
    CloseFile(F);                  }
    dmCupomFiscal.AcionarPeriferico(FRENTE_CAIXA.GAV_Caminho,FRENTE_CAIXA.GAV_Comando);
  end;
end;


//Carregar abastecidas que est�o com status "X"
procedure TfrmPosto.getAbastecidas;
begin
  qurAbastecidas.Active:=False;
  qurAbastecidas.Active:=True;
  qurAbastecidas.First;
  cdsAbastecidas.EmptyDataSet;
  cdsAbastecidas.Open;
  while not (qurAbastecidas.Eof) do
  begin
    cdsAbastecidas.Insert;
    cdsAbastecidasID.Value          := qurAbastecidasID.Value;
    cdsAbastecidasMERCADORIA.Value  := qurAbastecidasMERCADORIA.Value;
    cdsAbastecidasPRECO.Value       := qurAbastecidasPRECO.Value;
    cdsAbastecidasLITRO.Value       := qurAbastecidasLITRO.Value;
    cdsAbastecidasDINHEIRO.Value    := qurAbastecidasDINHEIRO.Value;
    cdsAbastecidasBICO.Value        := qurAbastecidasBICO.Value;
    cdsAbastecidasMERC_DESCRICAO.Value:= qurAbastecidasMERC_DESCRICAO.Value;
    cdsAbastecidasTEMP_ECF_ITEM.Value := (qurAbastecidasABASTECIDA.Value > 0);
    cdsAbastecidasPAGO.Value        := (qurAbastecidasPAGO.Value = 'S');
    cdsAbastecidasCLIFOR.Value      := qurAbastecidasCLIFOR.Value;
    cdsAbastecidasDATA_HORA.Value   := qurAbastecidasDATA_HORA.Value;
    cdsAbastecidasFRENTISTA.Value   := qurAbastecidasFRENTISTA.Value;
    cdsAbastecidas.Post;
    qurAbastecidas.Next;
  end;
  pnlCabecalho.Caption:='Abastecidas ('+IntToStr(cdsAbastecidas.RecordCount)+')';
end;

procedure TfrmPosto.grdAbastecidasCellClick(Column: TColumn);
var id_abastecida, id_clifor : Integer;
begin
  if(cdsAbastecidas.IsEmpty)then
    abort;
  id_Abastecida   := cdsAbastecidasID.Value;
  case AnsiIndexStr(LowerCase(Column.FieldName), ['excluir', 'pago', 'clifor']) of
    0:begin  //Coluna Excluir
      if msgPergunta('Deseja lan�ar abastecida  '+IntToStr(id_abastecida)+' como aferi��o ?'+sLineBreak+
                     'Combust�vel : '+cdsAbastecidasMERC_DESCRICAO.AsString+sLineBreak+
                     'Bico : '+cdsAbastecidasBICO.AsString+sLineBreak+
                     'Litros : '+cdsAbastecidasLITRO.AsString,'.::AFERI��O::.') then
      begin
        IF(FRENTE_CAIXA.Autorizacao_Abast)then
        begin
          if not getAutorizacaoIncondicional('Exclui Abastecida', 'Autoriza��o para exclus�o de uma abastecida',
          cdsAbastecidasDINHEIRO.AsCurrency) then
          begin
            Abort;
          end;
        end;
        try
          try
            if not dmCupomFiscal.IBSQL1.Transaction.InTransaction then
              dmCupomFiscal.IBSQL1.Transaction.StartTransaction;
            dmCupomFiscal.IBSQL1.Close;
            dmCupomFiscal.IBSQL1.SQL.Clear;
            dmCupomFiscal.IBSQL1.SQL.Add('Update est_abastecimentos Set Status=''A'' where id = :pID');
            dmCupomFiscal.IBSQL1.ParamByName('pID').AsInteger := id_abastecida;
            dmCupomFiscal.IBSQL1.ExecQuery;
            dmCupomFiscal.IBSQL1.Close;
            dmCupomFiscal.IBSQL1.Transaction.Commit;
          except
            on E: Exception do
            begin
              if dmCupomFiscal.IBSQL1.Transaction.InTransaction then
                dmCupomFiscal.IBSQL1.Transaction.Rollback;
              logErros(Self, caminhoLog, 'Erro ao excluir abastecida. '+e.Message, 'Erro ao excluir abastecida', 'S', e);
            end;
          end;
        finally
          getTemp_NFCe_Item;
          Mer_Bico;
        end;
      end
      else
      begin
        IF(FRENTE_CAIXA.Autorizacao_Abast)then
        begin
          if getAutorizacaoIncondicional('EXCLUIR_ABASTECIDA', 'AUTORIZA��O PARA EXCLUIR UMA ABASTECIDA',
          cdsAbastecidasDINHEIRO.AsCurrency) then
          begin
            if msgPergunta('Confirmar exclus�o da abastecida '+IntToStr(id_abastecida)+' ?'+sLineBreak+
                           'Combust�vel : '+cdsAbastecidasMERC_DESCRICAO.AsString+sLineBreak+
                           'Bico : '+cdsAbastecidasBICO.AsString+sLineBreak+
                           'Litros : '+cdsAbastecidasLITRO.AsString,'.::EXCLUS�O::.') then
            begin
              try
                try
                  if not dmCupomFiscal.IBSQL1.Transaction.InTransaction then
                    dmCupomFiscal.IBSQL1.Transaction.StartTransaction;
                  dmCupomFiscal.IBSQL1.Close;
                  dmCupomFiscal.IBSQL1.SQL.Clear;
                  dmCupomFiscal.IBSQL1.SQL.Add('Update est_abastecimentos Set Status=''E'' where id = :pID');
                  dmCupomFiscal.IBSQL1.ParamByName('pID').AsInteger := id_abastecida;
                  dmCupomFiscal.IBSQL1.ExecQuery;
                  dmCupomFiscal.IBSQL1.Close;
                  dmCupomFiscal.IBSQL1.Transaction.Commit;
                except
                  on E: Exception do
                  begin
                    if dmCupomFiscal.IBSQL1.Transaction.InTransaction then
                      dmCupomFiscal.IBSQL1.Transaction.Rollback;
                    logErros(Self, caminhoLog, 'Erro ao excluir abastecida. '+e.Message, 'Erro ao excluir abastecida', 'S', e);
                  end;
                end;
              finally
                getTemp_NFCe_Item;
                //getAbastecidas;  //IBEvents vai fazer o que precisa
                Mer_Bico;
              end;
            end;
          end;
        end
        else
        begin
          if msgPergunta('Confirmar exclus�o da abastecida '+IntToStr(id_abastecida)+' ?'+sLineBreak+
                         'Combust�vel : '+cdsAbastecidasMERC_DESCRICAO.AsString+sLineBreak+
                         'Bico : '+cdsAbastecidasBICO.AsString+sLineBreak+
                         'Litros : '+cdsAbastecidasLITRO.AsString,'.::EXCLUS�O::.') then
          begin
            try
              try
                if not dmCupomFiscal.IBSQL1.Transaction.InTransaction then
                  dmCupomFiscal.IBSQL1.Transaction.StartTransaction;
                dmCupomFiscal.IBSQL1.Close;
                dmCupomFiscal.IBSQL1.SQL.Clear;
                dmCupomFiscal.IBSQL1.SQL.Add('Update est_abastecimentos Set Status=''E'' where id = :pID');
                dmCupomFiscal.IBSQL1.ParamByName('pID').AsInteger := id_abastecida;
                dmCupomFiscal.IBSQL1.ExecQuery;
                dmCupomFiscal.IBSQL1.Close;
                dmCupomFiscal.IBSQL1.Transaction.Commit;
              except
                on E: Exception do
                begin
                  if dmCupomFiscal.IBSQL1.Transaction.InTransaction then
                    dmCupomFiscal.IBSQL1.Transaction.Rollback;
                  logErros(Self, caminhoLog, 'Erro ao excluir abastecida. '+e.Message, 'Erro ao excluir abastecida', 'S', e);
                end;
              end;
            finally
              getTemp_NFCe_Item;
              //getAbastecidas;  //IBEvents vai fazer o que precisa
              Mer_Bico;
            end;
          end;
        end;
      end;
    end;
    1:begin  //Coluna Pago
      try
        try
          if not dmCupomFiscal.IBSQL1.Transaction.InTransaction then
            dmCupomFiscal.IBSQL1.Transaction.StartTransaction;
          dmCupomFiscal.IBSQL1.Close;
          dmCupomFiscal.IBSQL1.SQL.Clear;
          if not(cdsAbastecidasPAGO.Value)then //Faz processo contrario se tava pago seta nao pago
            dmCupomFiscal.IBSQL1.SQL.Add('Update est_abastecimentos Set Pago=''S'' where id = :pID')
          else
            dmCupomFiscal.IBSQL1.SQL.Add('Update est_abastecimentos Set Pago=''N'' where id = :pID');
          dmCupomFiscal.IBSQL1.ParamByName('pID').AsInteger := id_abastecida;
          dmCupomFiscal.IBSQL1.ExecQuery;
          dmCupomFiscal.IBSQL1.Close;
          dmCupomFiscal.IBSQL1.Transaction.Commit;
          //dmCupomFiscal.Transaction.CommitRetaining;
        except
          on E: Exception do
          begin
            if dmCupomFiscal.IBSQL1.Transaction.InTransaction then
              dmCupomFiscal.IBSQL1.Transaction.Rollback;
            logErros(Self, caminhoLog, 'Erro ao alterar situa��o "PAGO" da abastecida. '+e.Message, 'Erro ao alterar situa��o "PAGO" da abastecida', 'S', e);
          end;
        end;
      finally
        getTemp_NFCe_Item;
        //getAbastecidas;
        Mer_Bico;
      end;
    end;
    2:begin  //Coluna Clifor
      frmAbastecidaCliente := TfrmAbastecidaCliente.Create(Self, cdsAbastecidasCLIFOR.Value, cdsAbastecidasBICO.Value, cdsAbastecidasMERC_DESCRICAO.Value, cdsAbastecidasLITRO.Value);
      try
        try
          case frmAbastecidaCliente.ShowModal of
            mrOk:begin
              id_clifor := frmAbastecidaCliente.qurClifor.FieldByName('id').AsInteger;
              if not dmCupomFiscal.IBSQL1.Transaction.InTransaction then
                dmCupomFiscal.IBSQL1.Transaction.StartTransaction;
              dmCupomFiscal.IBSQL1.Close;
              dmCupomFiscal.IBSQL1.SQL.Clear;
              dmCupomFiscal.IBSQL1.SQL.Add('Update est_abastecimentos Set clifor=:pCLifor where id = :pID');
              dmCupomFiscal.IBSQL1.ParamByName('pID').AsInteger     := id_abastecida;
              dmCupomFiscal.IBSQL1.ParamByName('pClifor').AsInteger := id_clifor;
              dmCupomFiscal.IBSQL1.ExecQuery;
              dmCupomFiscal.IBSQL1.Close;
              dmCupomFiscal.IBSQL1.Transaction.Commit;
              //dmCupomFiscal.Transaction.CommitRetaining;
            end;
            mrIgnore:begin
              if not dmCupomFiscal.IBSQL1.Transaction.InTransaction then
                dmCupomFiscal.IBSQL1.Transaction.StartTransaction;
              dmCupomFiscal.IBSQL1.Close;
              dmCupomFiscal.IBSQL1.SQL.Clear;
              dmCupomFiscal.IBSQL1.SQL.Add('Update est_abastecimentos Set clifor=null where id = :pID');
              dmCupomFiscal.IBSQL1.ParamByName('pID').AsInteger     := id_abastecida;
              dmCupomFiscal.IBSQL1.ExecQuery;
              dmCupomFiscal.IBSQL1.Close;
              dmCupomFiscal.IBSQL1.Transaction.Commit;
              //dmCupomFiscal.Transaction.CommitRetaining;
            end;
          end;
        except
          on E: Exception do
          begin
            if dmCupomFiscal.IBSQL1.Transaction.InTransaction then
              dmCupomFiscal.IBSQL1.Transaction.Rollback;
            logErros(Self, caminhoLog, 'Erro ao identificar cliente da abastecida. '+e.Message, 'Erro ao identificar cliente da abastecida', 'S', e);
          end;
        end;
      finally
        frmAbastecidaCliente.Free;
        //getAbastecidas;
        getTemp_NFCe_Item;
        Mer_Bico;
      end;
    end;
  else
    if not(cdsAbastecidas.IsEmpty) and not(cdsAbastecidasTEMP_ECF_ITEM.Value) then
    begin
      try
        btnVende.Enabled:=True;
        btnCancela.Enabled:=True;
        if reSubtotal.Value > 0 then btnCancela.Click; //Devido colina condi��o que tem no BIco

        if cdsAbastecidasCLIFOR.Value > 0 then
        begin
          edtClienteEnter(edtCliente);
          edtCliente.Text:=cdsAbastecidasCLIFOR.AsString;
          edtClienteExit(edtCliente);
        end
        else
        begin
            edtCliente.Clear;
            edtClienteExit(edtCliente);
        end;

        if(cdsAbastecidasID.Value > 0) and (not(POSTO.AlterarQuantidade)) then
          reQuantidade.Enabled := false
        else
          reQuantidade.Enabled := True;

        Abastecimento:=cdsAbastecidasID.Value;
        edtBicoEnter(edtBico);
        edtBico.Text:=cdsAbastecidasBICO.AsString;
        edtBicoExit(edtBico);
        edtMercadoriaEnter(edtMercadoria);
        edtMercadoria.Text:=cdsAbastecidasMERCADORIA.AsString;
        edtMercadoriaExit(edtMercadoria);
        reQuantidadeEnter(reQuantidade);
        reQuantidade.Value:=cdsAbastecidasLITRO.Value;
        reQuantidadeExit(reQuantidade);
        reVlrUnitarioEnter(reVlrUnitario);
        //reVlrUnitario.Value:=cdsAbastecidasPRECO.Value;
        reVlrUnitarioExit(reVlrUnitario);
        reSubtotalEnter(reSubtotal);
        reSubtotalExit(reSubtotal);
      except
        Mer_Bico;
      end;
    end
    else
    begin
      msgInformacao('Abastecida j� esta registrada no Cupom Fiscal atual','Aten��o')
    end;
  end;
end;

procedure TfrmPosto.grdAbastecidasDblClick(Sender: TObject);
begin
  if not(cdsAbastecidas.IsEmpty)then
  begin
    msgAviso(Format(
    'Bico : %s'+sLineBreak+
    'Combust�vel : %s'+sLineBreak+
    'Data/Hora : %s'+sLineBreak+
    'Litros : %.3f'+sLineBreak+
    'Dinheiro : %.3f'+sLineBreak+
    'Frentista : %s',
    [cdsAbastecidasBICO.AsString,
    cdsAbastecidasMERC_DESCRICAO.AsString,
    cdsAbastecidasDATA_HORA.AsString,
    cdsAbastecidasLITRO.AsCurrency,
    cdsAbastecidasDINHEIRO.AsCurrency,
    cdsAbastecidasFRENTISTA.AsString]),'Informa��es da abastecida '+cdsAbastecidasID.AsString);
  end;
end;

procedure TfrmPosto.grdAbastecidasDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  //ShowScrollBar(grdAbastecidas.Handle,SB_VERT,True);

  if LowerCase(Column.FieldName) = 'excluir' then
    Column.Title.Caption:=''
  else if LowerCase(Column.FieldName) = 'clifor' then
    Column.Title.Caption:='';

  if not (cdsAbastecidas.IsEmpty) then
  begin
    If (cdsAbastecidasTEMP_ECF_ITEM.Value) then
    begin
      grdAbastecidas.Canvas.Brush.Color := HexToTColor('88B8FF');
      grdAbastecidas.Canvas.FillRect(Rect);
      grdAbastecidas.DefaultDrawColumnCell(Rect, DataCol, Column, State);
    end;

    if LowerCase(Column.FieldName) = 'excluir' then //Coluna para DELETAR abastecida
    begin
      grdAbastecidas.Canvas.FillRect(Rect);
      imgListAbastecidas.Draw(grdAbastecidas.Canvas,Rect.Left+01,Rect.Top+1,4);
    end;

    if LowerCase(Column.FieldName) = 'clifor' then //Coluna CLIFOR
    begin
      grdAbastecidas.Canvas.FillRect(Rect);
      if cdsAbastecidasCLIFOR.Value > 0 then
        imgListAbastecidas.Draw(grdAbastecidas.Canvas,Rect.Left+01,Rect.Top+1,0) //Identificado Cliente
      else
        imgListAbastecidas.Draw(grdAbastecidas.Canvas,Rect.Left+01,Rect.Top+1,1);
    end;

    if LowerCase(Column.FieldName) = 'pago' then
    begin
      grdAbastecidas.Canvas.FillRect(Rect);
      if cdsAbastecidasPAGO.Value then
        imgListAbastecidas.Draw(grdAbastecidas.Canvas,Rect.Left+05,Rect.Top+1,2)
      else
        imgListAbastecidas.Draw(grdAbastecidas.Canvas,Rect.Left+05,Rect.Top+1,3);
    end;
  end;
end;

procedure TfrmPosto.edtBicoKeyPress(Sender: TObject; var Key: Char);
begin
  if not (somenteNumeros(Key)) then Key := #0;
end;

procedure TfrmPosto.BuscaMercadoria();
begin
  DMCupomFiscal.dbQuery1.Active:=False;
  DMCupomFiscal.dbQuery1.SQL.Clear;
  DMCupomFiscal.dbQuery1.SQL.Add('SELECT M.ID, M.Unidade, M.CST_Pis, M.CST_Cofins, M.Descricao, M.Sucinto, M.Venda, M.Ativo, M.servico,'+
                          ' M.ECF_CST_ICM, M.ECF_CFOP, M.NCM, M.SUBTIPO_ITEM, M.Desconto,'+
                          ' M.SM_Venda_Promocao, M.Custo_Ultimo, M.Custo_Medio, M.ICM_ECF, M.ISSQN, ME.Est_Atual, MCF.Venda MCF_Venda'+
                          ' FROM Est_Mercadorias M'+
                          ' Left Outer Join Est_Mercadorias_Estoque ME on ME.Mercadoria=M.Id and Me.CCusto=:pCC'+
                          ' Left Outer Join Est_Mercadorias_CliFor MCF on MCF.Mercadoria=M.Id and MCF.CliFor=:pCF'+
                          ' WHERE (M.ID=:pCodigo)');
  DMCupomFiscal.dbQuery1.ParamByName('pCodigo').AsString:=Trim(edtMercadoria.Text);
  DMCupomFiscal.dbQuery1.ParamByName('pCC').AsInteger:=CCUSTO.codigo;
  DMCupomFiscal.dbQuery1.ParamByName('pCF').AsInteger:=StrToIntDef(edtCliente.Text,0);
  DMCupomFiscal.dbQuery1.Active:=True;
  DMCupomFiscal.dbQuery1.First;
  If not(DMCupomFiscal.dbQuery1.Eof) then
  begin
    //Verifica se mercadoria est� Ativa
    If DMCupomFiscal.dbQuery1.FieldByName('Ativo').AsString='N' then
    begin
      msgInformacao('Mercadoria est� Inativa. Favor verificar cadastro','Aten��o');
      edtMercadoria.Clear;
      Mer_Bico;
    end
  end;
end;

procedure TfrmPosto.UsoeConsumo1Click(Sender: TObject);
var
  sPlaca: String;
begin
  if(msgPergunta('Lan�ar abastecida como Uso e Consumo?'+sLineBreak+
    'N�mero: '+cdsAbastecidasID.AsString+sLineBreak+
    'Produto: '+cdsAbastecidasMERCADORIA.AsString+'-'+cdsAbastecidasMERC_DESCRICAO.AsString+sLineBreak+
    'Data: '+cdsAbastecidasDATA_HORA.AsString+sLineBreak+
    'Valor: '+ FormatFloat('R$ ###,##0.00',cdsAbastecidasDINHEIRO.AsCurrency),Application.Title))then
  begin
    if(getAutorizacao('ABASTECIDA_USO_CONSUMO','LAN�AMENTO DE ABASTECIDA COMO USO E CONSUMO',
    cdsAbastecidasDINHEIRO.AsCurrency))Then
    begin
      if(btnCancela.Enabled)then btnCancelaClick(Action);
      try
        sPlaca := '';
        InputQuery('Vincular Placa', 'Informe a Placa:', sPlaca);
        dmCupomFiscal.IBSQL1.Close;
        dmCupomFiscal.IBSQL1.SQL.Clear;
        dmCupomFiscal.IBSQL1.SQL.Add('Update Est_Abastecimentos set Status = ''U'' ');
        if(sPlaca <> '')Then
          dmCupomFiscal.IBSQL1.SQL.Add(',Placa=:pPlaca');
        dmCupomFiscal.IBSQL1.SQL.Add(' where ID = :pID');
        if(sPlaca <> '')Then
          dmCupomFiscal.IBSQL1.ParamByName('pPlaca').AsString := UpperCase(sPlaca);
        dmCupomFiscal.IBSQL1.ParamByName('pID').AsInteger := cdsAbastecidasID.AsInteger;
        dmCupomFiscal.IBSQL1.ExecQuery;
        dmCupomFiscal.IBSQL1.Close;
        dmCupomFiscal.Transaction.CommitRetaining;
      except
        on E:Exception do
          logErros(Sender, caminhoLog,'Erro ao lan�ar abastecida como uso e consumo',
            'Erro ao lan�ar abastecida como uso e consumo','S',E);
      end;
    end;
  end;
end;

end.
