object frmPesquisaCliente: TfrmPesquisaCliente
  Left = 159
  Top = 122
  BorderIcons = []
  Caption = 'Pesquisa Cliente'
  ClientHeight = 406
  ClientWidth = 567
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDesktopCenter
  OnKeyDown = FormKeyDown
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object DBGrid1: TDBGrid
    Left = 12
    Top = 70
    Width = 549
    Height = 307
    Color = clWhite
    Ctl3D = False
    DataSource = DataSource1
    DrawingStyle = gdsClassic
    FixedColor = clNavy
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    Options = [dgTitles, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
    ParentCtl3D = False
    ParentFont = False
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWhite
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnEnter = DBGrid1Enter
    OnKeyDown = DBGrid1KeyDown
    Columns = <
      item
        Expanded = False
        FieldName = 'ID'
        Title.Caption = 'C'#243'digo'
        Width = 45
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NOME'
        Title.Caption = 'Nome/Raz'#227'o Social'
        Width = 290
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'FONE'
        Title.Alignment = taCenter
        Title.Caption = 'Fone'
        Width = 95
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CNPJCPF'
        Title.Alignment = taCenter
        Title.Caption = 'CPF/CNPJ'
        Width = 100
        Visible = True
      end>
  end
  object GroupBox1: TGroupBox
    Left = 12
    Top = 11
    Width = 549
    Height = 54
    Caption = '[  Filtro para Pesquisa  ]'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object Edit1: TEdit
      Left = 8
      Top = 20
      Width = 441
      Height = 19
      CharCase = ecUpperCase
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 0
      OnKeyDown = Edit1KeyDown
    end
    object CheckBox1: TCheckBox
      Left = 464
      Top = 23
      Width = 73
      Height = 17
      Caption = 'Inicie por'
      Checked = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 1
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 387
    Width = 567
    Height = 19
    Panels = <
      item
        Alignment = taCenter
        Text = 'Esc - Sair sem confirmar'
        Width = 130
      end
      item
        Text = 'F12 - Refaz pesquisa'
        Width = 115
      end
      item
        BiDiMode = bdLeftToRight
        ParentBiDiMode = False
        Text = 'Enter - Confirma'
        Width = 100
      end
      item
        Text = 'F1 - Valor de Custo'
        Width = 50
      end>
  end
  object DataSource1: TDataSource
    DataSet = qCliFor
    Left = 152
    Top = 128
  end
  object qCliFor: TIBQuery
    Database = dmCupomFiscal.DataBase
    Transaction = dmCupomFiscal.Transaction
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    SQL.Strings = (
      'select ID,Nome,CNPJCPF,Fone'
      'From CliFor'
      'Where Nome>=:pDes and (CF='#39'C'#39') and (Ativo='#39'S'#39')'
      'Order By Nome'
      '')
    Left = 104
    Top = 128
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'pDes'
        ParamType = ptUnknown
      end>
    object qCliForNOME: TIBStringField
      FieldName = 'NOME'
      Origin = '"CLIENTES_FORNECEDORES"."NOME"'
      Required = True
      Size = 40
    end
    object qCliForFONE: TIBStringField
      FieldName = 'FONE'
      Origin = '"CLIENTES_FORNECEDORES"."FONE"'
      Size = 15
    end
    object qCliForID: TIntegerField
      FieldName = 'ID'
      Origin = '"CLIFOR"."ID"'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object qCliForCNPJCPF: TIBStringField
      FieldName = 'CNPJCPF'
      Origin = '"CLIFOR"."CNPJCPF"'
      Size = 18
    end
  end
end
