object frmCancelaItem: TfrmCancelaItem
  Left = 400
  Top = 273
  BorderIcons = []
  Caption = 'Cancelar Item'
  ClientHeight = 137
  ClientWidth = 245
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDesktopCenter
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 245
    Height = 41
    Align = alTop
    Caption = 'N'#250'mero do Item'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
  end
  object Panel2: TPanel
    Left = 0
    Top = 41
    Width = 245
    Height = 96
    Align = alClient
    Color = clBtnHighlight
    ParentBackground = False
    TabOrder = 0
    object btnOK: TBitBtn
      Left = 49
      Top = 59
      Width = 75
      Height = 25
      Caption = 'OK'
      TabOrder = 1
      OnClick = btnOKClick
    end
    object btnCancelar: TBitBtn
      Left = 129
      Top = 59
      Width = 75
      Height = 25
      Caption = 'Cancelar'
      TabOrder = 2
      OnClick = btnCancelarClick
    end
    object edItem: TEdit
      Left = 93
      Top = 14
      Width = 65
      Height = 28
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      MaxLength = 3
      ParentFont = False
      TabOrder = 0
      OnExit = edItemExit
    end
  end
end
