unit uFrmContingencia;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, Grids, DBGrids, ExtCtrls, StdCtrls, Buttons,
  IBCustomDataSet, IBQuery, DBCtrls, ComCtrls, pngimage;

type
  TfrmContingencia = class(TForm)
    pnlFundo: TPanel;
    qurCredenciadora: TIBQuery;
    qurCredenciadoraCNPJ: TIBStringField;
    qurCredenciadoraDESCRICAO: TIBStringField;
    Image2: TImage;
    edtJustificativa: TRichEdit;
    Image1: TImage;
    Label1: TLabel;
    btnOK: TBitBtn;
    btnCancelar: TBitBtn;
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btnOKClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmContingencia: TfrmContingencia;

implementation

uses UFrmFim, uDMCupomFiscal, uFuncoes;

{$R *.dfm}

procedure TfrmContingencia.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (key = vk_Escape) then
    ModalResult := mrCancel;
end;

procedure TfrmContingencia.btnOKClick(Sender: TObject);
begin
  if Trim(edtJustificativa.Text) = '' then
  begin
    edtJustificativa.SetFocus;
    msgAviso('Favor informar a justificativa para entrada em contingência','');
    Abort;
  end;

  ModalResult := mrOK;
end;

procedure TfrmContingencia.btnCancelarClick(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

end.
